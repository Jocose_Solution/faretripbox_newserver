﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COMN_SHARED;
using COMN_SHARED.Flight;
using System.Data;
using COMN_SHARED.Payment;
using EXCEPTION_LOG;

namespace COMN
{
    public class Flt //: ÍFlt
    {
        public string Constr { get; set; }
        COMN_API.Flight.IFltPNRAPI IfltPNR;
        COMN_BAL.Flight.FlighBal objBal = null;
        public Flt(string connectionString)
        {
            Constr = connectionString;
            objBal = new COMN_BAL.Flight.FlighBal(Constr);
        }

        public FltBookResShrd CreateBooking(FltBookReqShrd bookShared)
        {
             FltBookResShrd resp = new FltBookResShrd();
            try
            {
                FltPNR objPNRO = new FltPNR();
                FltPNR objPNRR = new FltPNR();
                DataSet AgencyDs = objBal.GetAgencyDetails(bookShared.UserID);
                DataSet PaxDs = objBal.GetPaxDetailsByOrderId(bookShared.OrderID);
                DataSet FltHdrDs = objBal.GetHdrDetails(bookShared.OrderID);
                DataSet FltDs = new DataSet();
                FltDs = objBal.GetFlightDetailsByOrderId(bookShared.OrderID, bookShared.UserID);

                objPNRO = CreatePNR(PaxDs, FltHdrDs, FltDs, AgencyDs, bookShared, bookShared.OrderID);
                resp.PNRO = objPNRO;

                if (!string.IsNullOrEmpty(bookShared.OrderIDR) && bookShared.OrderIDR.Length>7)
                {
                    PaxDs = objBal.GetPaxDetailsByOrderId(bookShared.OrderIDR);
                    FltHdrDs = objBal.GetHdrDetails(bookShared.OrderIDR);
                    FltDs = objBal.GetFlightDetailsByOrderId(bookShared.OrderIDR, bookShared.UserID);
                    objPNRR = CreatePNR(PaxDs, FltHdrDs, FltDs, AgencyDs, bookShared, bookShared.OrderIDR);
                    resp.PNRR = objPNRR;
                }

            }
            catch (Exception ex)
            {
                ErrorLog.FileHandling("Flight", "Error_103", ex, "COMN.Flight.Flt.CreateBooking");
                try
                {
                    System.IO.File.AppendAllText("C:\\\\TBO_Req_Res\\\\HOLD_BY_AGENT_" + bookShared.OrderID + ".txt", System.DateTime.Now.ToString() + Environment.NewLine +
                                          "OrderID: " + bookShared.OrderID + Environment.NewLine +
                                          "AgentId: " + bookShared.UserID + Environment.NewLine +
                                          "Exception_Message: " + ex.Message + Environment.NewLine +
                                          "Exception_StackTrace: " + ex.StackTrace.ToString() + Environment.NewLine + Environment.NewLine +
                                          Environment.NewLine + Environment.NewLine + Environment.NewLine);
                }
                catch (Exception ex1)
                { }                
            }

           





            PaymentResShrd payObj = new PaymentResShrd();
            // payObj.DebitAmount(PaymentReq);

            // PNR Mehod
            // ticket  Method

            // out result Class

            return resp;
        }


        public FltPNR CreatePNR(DataSet PaxDs, DataSet FltHdrDs, DataSet FltDs, DataSet AgencyDs, FltBookReqShrd bookShared, string OrderId)
        {
            FltPNR objPnrResp = new FltPNR();

            try
            {
                if (FltHdrDs.Tables[0].Rows[0]["Status"].ToString().Trim().ToUpper() != "TICKETED" & FltHdrDs.Tables[0].Rows[0]["Status"].ToString().Trim().ToUpper() != "CONFIRM")
                {
                    if (AgencyDs.Tables[0].Rows[0]["Agent_Status"].ToString().Trim() != "NOT ACTIVE" & AgencyDs.Tables[0].Rows[0]["Online_tkt"].ToString().Trim() != "NOT ACTIVE")
                    {

                        if ((Convert.ToBoolean(FltHdrDs.Tables[0].Rows[0]["IsHoldByAgent"])== true && (Convert.ToDouble(FltHdrDs.Tables[0].Rows[0]["HoldCharge"]) <= Convert.ToDouble(AgencyDs.Tables[0].Rows[0]["Crd_Limit"].ToString().Trim()))) || (Convert.ToDouble(FltHdrDs.Tables[0].Rows[0]["TotalAfterDis"]) <= Convert.ToDouble(AgencyDs.Tables[0].Rows[0]["Crd_Limit"].ToString().Trim())))
                        {
                            bool PyamentStatus = false;
                            string provider = "";
                            string trip = Convert.ToString(FltDs.Tables[0].Rows[0]["Trip"]).Trim().ToUpper();
                            string vc = Convert.ToString(FltHdrDs.Tables[0].Rows[0]["VC"]).Trim().ToUpper();
                            string ProjectId = "";
                            string BillNoCorp = "";
                            if (Convert.ToBoolean(FltHdrDs.Tables[0].Rows[0]["IsHoldByAgent"]) == true)
                            {
                                int r = objBal.Ledgerandcreditlimit_Transaction_Insert(bookShared.UserID, Convert.ToDouble(FltHdrDs.Tables[0].Rows[0]["HoldCharge"]), OrderId, vc, "", Convert.ToString(AgencyDs.Tables[0].Rows[0]["Agency_Name"]), bookShared.IP, ProjectId, bookShared.UserID, BillNoCorp, Convert.ToString(AgencyDs.Tables[0].Rows[0]["Crd_Limit"].ToString().Trim()), "", FltPNRSatus.PreConfirmByAgent.ToString());
                                if (r == 1 || r == -1)
                                {
                                    PyamentStatus = true;
                                }
                                else
                                {
                                    objPnrResp.Message = "Unable to deduct Payment";
                                }

                            }
                            if (PyamentStatus == false)
                            {
                                //// code for deduction  of payment Payment
                            }
                            if (PyamentStatus == true)
                            {
                                #region Create PNR Code

                                provider = Convert.ToString(FltDs.Tables[0].Rows[0]["Provider"]).Trim().ToUpper();

                                if (provider == "1A" || provider == "1G" || provider == "1B")
                                {
                                    string serviceCode = objBal.GetGDSServiceCodeForPNRCreation(vc, trip);

                                    /// Gal PNR Creation
                                    #region GAL PNR Creation
                                    if (serviceCode == "1G")
                                    {
                                        IfltPNR = new COMN_API.Flight.GalApi(Constr);
                                        objPnrResp = IfltPNR.GetPNR(PaxDs, FltHdrDs, FltDs, OrderId);

                                    }

                                    #endregion

                                }
                                else
                                {
                                    objPnrResp.Message = "Hold is  not allowed for this provider";
                                }


                                #endregion
                            }
                        }
                        else
                        {
                            objPnrResp.Message = "Low Balance.";

                        }
                    }
                    else
                    {

                        objPnrResp.Message = "Agent not active";
                    }
                }
                else
                {
                    objPnrResp.Message = "Invalid orderId.";

                }

               
            }
            catch (Exception ex)
            {

                ErrorLog.FileHandling("Flight", "Error_103", ex, "COMN.Flight.Flt.CreatePNR");
                try
                {
                    System.IO.File.AppendAllText("C:\\\\TBO_Req_Res\\\\HOLD_BOOKING_" + OrderId + ".txt", System.DateTime.Now.ToString() + Environment.NewLine +
                                          "OrderID: " + OrderId + Environment.NewLine +
                                          "AgentId: " + bookShared.UserID + Environment.NewLine +
                                          "Trip: " + Convert.ToString(FltDs.Tables[0].Rows[0]["Trip"]).Trim().ToUpper() + Environment.NewLine +
                                           "Provider: " + Convert.ToString(FltDs.Tables[0].Rows[0]["Provider"]).Trim().ToUpper() + Environment.NewLine +
                                           "VC: " + Convert.ToString(FltHdrDs.Tables[0].Rows[0]["VC"]).Trim().ToUpper() + Environment.NewLine +
                                           "ResponseMessage: " + objPnrResp.Message + Environment.NewLine +
                                          "Exception_Message: " + ex.Message + Environment.NewLine +
                                          "Exception_StackTrace: " + ex.StackTrace.ToString() + Environment.NewLine + Environment.NewLine +
                                          Environment.NewLine + Environment.NewLine + Environment.NewLine);
                }
                catch (Exception ex1)
                { }
            }
            finally
            {
                if (objPnrResp.GdsPNR.Contains("-FQ") )
                {
                    objPnrResp.Status = FltPNRSatus.Failed.ToString();

                }
                else
                {
                    objPnrResp.Status = FltPNRSatus.ConfirmByAgent.ToString();
                }

                if(! string.IsNullOrEmpty( objPnrResp.GdsPNR))
                {
                    objBal.UpdateFltHeader(OrderId, Convert.ToString(AgencyDs.Tables[0].Rows[0]["Agency_Name"]), objPnrResp.GdsPNR, objPnrResp.AirlinePNR, objPnrResp.Status);
                }

              
            }


            return objPnrResp;
        }




        public void Mybooking()
        {



        }

        public void Yourbooking()
        {

        }
    }




    public class FltTicket
    {

        public string GetTicket()
        {
            return "";
        }
    }

    //public interface ÍFlt
    //{

    //    public FltBookResShrd CreateBooking(FltBookReqShrd bookShared);
    //    public void Yourbooking();
    //}


    //public class callCls
    //{

    //    public void getb()
    //    {
    //        ÍFlt oo = new Flt();

    //    }


    //}
}
