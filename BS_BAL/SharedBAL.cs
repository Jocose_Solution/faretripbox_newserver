﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Xml;
using System.Configuration;
using System.Collections;
using System.Threading;
using BS_DAL;
using BS_SHARED;
using Newtonsoft.Json.Linq;
namespace BS_BAL
{
    public class SharedBAL
    {
        #region For Set Predefind Value
        SharedDAL shareddal;
        AbhiBusService abhsrv;
        RedBusService rbdsrv;
        EXCEPTION_LOG.ErrorLog erlog;
        List<SHARED> ABList;
        List<SHARED> RBList;
        List<SHARED> GSList;
        List<SHARED> TyList;
        List<SHARED> ESList;
        SHARED RBBshared;
        SHARED TYBshared;
        SHARED ABBshared;
        SHARED GSBshared;
        SHARED ESBshared;     
        bool AllowRB=false; bool AllowTY = false;bool AllowAB = false;bool AllowGS = false;bool AllowES = false;     
        Travelyaari objtravelYari;
        clsSearchRequest2 objclsSearchRequest2;
        GsrtcService gstsrv;
        EtsService ObjEtsService;
        string srcplacecode = "";
        string Destplacecode = "";
        string gSAuthuSERiD = "";//ConfigurationManager.AppSettings["gSAuthuSERiD"].ToString();
        string gSAuthupASS = "";//ConfigurationManager.AppSettings["gSAuthupASS"].ToString();
        string GSUSERID = "";//ConfigurationManager.AppSettings["USER_ID"].ToString();
        string GSUSERNAME = "";//ConfigurationManager.AppSettings["USERNAME"].ToString();
        string GSCOUNTER_CODE = "";//ConfigurationManager.AppSettings["COUNTER_CODE"].ToString();
        #endregion
        #region[Insert source list]
        public void InsertSource(List<BS_SHARED.SHARED> list, string provider)
        {
            try
            {
                shareddal = new BS_DAL.SharedDAL();
                shareddal.InsertSrcList(list, provider);
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
        }
        #endregion
        #region[Insert destination list]

        public void InsertDestination(BS_SHARED.SHARED shared)
        {
            DataTable dt = new DataTable();
            List<BS_SHARED.SHARED> getABList = new List<BS_SHARED.SHARED>();
            List<BS_SHARED.SHARED> getRBList = new List<BS_SHARED.SHARED>();
            shareddal = new BS_DAL.SharedDAL();
            try
            {
                dt = getSrcID(shared);
               //getdestDestination();
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["Provider_Name"].ToString().ToUpper() == "AB")
                    {
                        if (shareddal.getBusProvider_Enable(dr["Provider_Name"].ToString()))
                        {
                            getABList = getABDestList(dr["city_id"].ToString(), shared.src.Trim(), dr["Provider_Name"].ToString().ToUpper());
                            //--------------------Insert Abhi bus destination list------------------------------//
                            if (getRBList.Count != 0)
                            {
                                shareddal.InsertDestList(getABList, dr["city_id"].ToString().Trim().Trim(), dr["Provider_Name"].ToString().ToUpper());
                            }
                            //----------------------end-------------------------------------------------//
                        }
                    }
                    else if (dr["Provider_Name"].ToString().ToUpper() == "RB")
                    {
                        if (shareddal.getBusProvider_Enable(dr["Provider_Name"].ToString()))
                        {
                            getRBList = getRBDestList(dr["city_id"].ToString().Trim(), shared.src.Trim(), dr["Provider_Name"].ToString().ToUpper());
                            //---------------------Get Redbus destination list-------------------------//
                            if (getRBList.Count != 0)
                            {
                                shareddal.InsertDestList(getRBList, dr["city_id"].ToString().Trim().Trim(), dr["Provider_Name"].ToString().ToUpper());
                            }
                        }
                        //-----------------------end----------------------------------------------//

                    }
                    else if (dr["Provider_Name"].ToString().ToUpper() == "GS")
                    {
                        if (shareddal.getBusProvider_Enable(dr["Provider_Name"].ToString()))
                        {
                            GsrtcService gsrtcDervice = new GsrtcService();
                            // gsrtcDervice.getGScityList();
                        }
                        //-----------------------end----------------------------------------------//

                    }
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
        }
        #endregion
        #region[get source list]


        public List<BS_SHARED.SHARED> getsrcList(string prefix)
        {
            List<BS_SHARED.SHARED> srclist = new List<BS_SHARED.SHARED>();
            try
            {
                shareddal = new BS_DAL.SharedDAL();
                srclist = shareddal.getSourceList(prefix);
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return srclist;
        }
        #endregion
        #region[get destination list]
        public List<BS_SHARED.SHARED> getdestList(string prefix,string source)
        {
            List<BS_SHARED.SHARED> destlist = new List<BS_SHARED.SHARED>();
            try
            {
                shareddal = new BS_DAL.SharedDAL();
                destlist = shareddal.getDestList(prefix, source);
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return destlist;
        }
        #endregion
        #region[Get Source ID]
        public DataTable getSrcID(BS_SHARED.SHARED shared)
        {

            DataTable dt = new DataTable();
            shareddal = new BS_DAL.SharedDAL();
            try
            {
                dt = shareddal.getSRCDEST_ID(shared);
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
                return dt;
            }
            return dt;
        }
        #endregion
        #region[Mersge Bus Result]
        

        public List<SHARED> getMergeJourneyList(SHARED shared)
        {
            List<SHARED> mergeList = new List<SHARED>(); DataTable dtt = new DataTable();
            //SHARED sharedd;
            ABBshared = new SHARED(); RBBshared = new SHARED(); GSBshared = new SHARED(); TYBshared = new SHARED(); ESBshared = new SHARED();
            AllowRB = false; AllowTY = false; AllowAB = false; AllowGS = false; AllowES = false;     
            try
            {
                //-----------------------get src id and dest id-------------------------------//

                dtt = getSrcID(shared);
                foreach (DataRow dr in dtt.Rows)
                {
                    if (dr["Provider_Name"].ToString().ToUpper() == "AB")
                    {
                        if (shareddal.getBusProvider_Enable(dr["Provider_Name"].ToString()))
                        {
                           // ABBshared = new SHARED();
                            ABBshared.srcID = dr["sourceid"].ToString().Trim();
                            ABBshared.destID = dr["destinationid"].ToString().Trim();
                            ABBshared.journeyDate = shared.journeyDate;
                            ABBshared.NoOfPax = shared.NoOfPax;
                            ABBshared.SeatType = shared.SeatType;
                            ABBshared.agentID = shared.agentID.Trim();
                            ABBshared.provider_name = dr["Provider_Name"].ToString().ToUpper();
                            AllowAB = true;
                            // ABList = abhsrv.getABJourneyList(sharedd);
                        }
                    }
                    else if (dr["Provider_Name"].ToString().ToUpper() == "RB")
                    {
                        if (shareddal.getBusProvider_Enable(dr["Provider_Name"].ToString()))
                        {
                          //  RBBshared = new SHARED();
                            RBBshared.srcID = dr["sourceid"].ToString().Trim();
                            RBBshared.destID = dr["destinationid"].ToString().Trim();
                            RBBshared.journeyDate = shared.journeyDate;
                            RBBshared.NoOfPax = shared.NoOfPax;
                            RBBshared.SeatType = shared.SeatType;
                            RBBshared.provider_name = dr["Provider_Name"].ToString().ToUpper();
                            RBBshared.agentID = shared.agentID.Trim();
                            AllowRB = true;
                            //RBList = rbdsrv.getRBJourneyList(RBBshared); 
                        }
                    }
                    else if (dr["Provider_Name"].ToString().ToUpper() == "GS")
                    {
                        if (shareddal.getBusProvider_Enable(dr["Provider_Name"].ToString()))
                        {
                           // GSBshared = new SHARED();
                            GSBshared.srcID = dr["sourceid"].ToString().Trim();
                            GSBshared.destID = dr["destinationid"].ToString().Trim();
                            GSBshared.src = shared.src;
                            GSBshared.dest = shared.dest;
                            GSBshared.journeyDate = shared.journeyDate;
                            GSBshared.NoOfPax = shared.NoOfPax;
                            GSBshared.SeatType = shared.SeatType;
                            GSBshared.agentID = shared.agentID.Trim();
                            srcplacecode = dr["SrcPlaceCode"].ToString().ToUpper();
                            Destplacecode = dr["DestPlaceCode"].ToString().ToUpper();
                            GSBshared.provider_name = dr["Provider_Name"].ToString().ToUpper();
                            AllowGS = true;
                            // GSList = gstsrv.GetAvailableServiceDetails(sharedd, srcplacecode, Destplacecode);
                        }
                    }
                    else if (dr["Provider_Name"].ToString().ToUpper() == "TY")
                    {
                        if (shareddal.getBusProvider_Enable(dr["Provider_Name"].ToString()))
                        {
                            //TYBshared = new SHARED();
                            objclsSearchRequest2 = new clsSearchRequest2();
                            objclsSearchRequest2.FromCityId = Convert.ToInt32(dr["sourceid"].ToString().Trim());
                            objclsSearchRequest2.JourneyDate = shared.journeyDate;
                            objclsSearchRequest2.NoOfSeats = Convert.ToInt32(shared.NoOfPax);
                            objclsSearchRequest2.SearchId = 0;
                            objclsSearchRequest2.ToCityId = Convert.ToInt32(dr["destinationid"].ToString().Trim());

                            TYBshared.srcID = dr["sourceid"].ToString().Trim();
                            TYBshared.destID = dr["destinationid"].ToString().Trim();
                            TYBshared.journeyDate = shared.journeyDate;
                            TYBshared.NoOfPax = shared.NoOfPax;
                            TYBshared.SeatType = shared.SeatType;
                            TYBshared.agentID = shared.agentID.Trim();
                            string srcplacecode = dr["SrcPlaceCode"].ToString().ToUpper();
                            string Destplacecode = dr["DestPlaceCode"].ToString().ToUpper();
                            TYBshared.provider_name = dr["Provider_Name"].ToString().ToUpper();
                            // TyList = objtravelYari.GetSearchresult(objclsSearchRequest2,sharedd);
                            AllowTY = true;
                        }
                    }
                    else if (dr["Provider_Name"].ToString().ToUpper() == "ES")
                    {
                        if (shareddal.getBusProvider_Enable(dr["Provider_Name"].ToString()))
                        {
                           // ESBshared = new SHARED();
                            ESBshared.srcID = dr["sourceid"].ToString().Trim();
                            ESBshared.destID = dr["destinationid"].ToString().Trim();
                            ESBshared.journeyDate = shared.journeyDate;
                            ESBshared.NoOfPax = shared.NoOfPax;
                            ESBshared.SeatType = shared.SeatType;
                            ESBshared.provider_name = dr["Provider_Name"].ToString().ToUpper();
                            ESBshared.agentID = shared.agentID.Trim();
                            ESBshared.src = shared.src.Trim();
                            ESBshared.dest = shared.dest.Trim();
                            AllowES = true;
                            // RBList = rbdsrv.getRBJourneyList(sharedd);
                        }
                    }
                }

                ArrayList vlist = new ArrayList(); ArrayList vlist1 = new ArrayList();
                #region Add in Thread
                Thread RB = new Thread(new ThreadStart(RBAvailability));
                RB.Start();
                vlist.Add(RB);
                vlist1.Add(DateTime.Now);

                Thread GS = new Thread(new ThreadStart(GSAvailability));
                GS.Start();
                vlist.Add(GS);
                vlist1.Add(DateTime.Now);

                Thread AB = new Thread(new ThreadStart(ABAvailability));
                AB.Start();
                vlist.Add(AB);
                vlist1.Add(DateTime.Now);

                Thread TY = new Thread(new ThreadStart(TYAvailability));
                TY.Start();
                vlist.Add(TY);
                vlist1.Add(DateTime.Now);

                Thread ES = new Thread(new ThreadStart(ESAvailability));
                ES.Start();
                vlist.Add(ES);
                vlist1.Add(DateTime.Now);

                #endregion

                #region Wait for Thread
                int counter = 0;
                while ((counter < vlist.Count))
                {
                    Thread TH = (Thread)vlist[counter];
                    if ((TH.ThreadState == ThreadState.Stopped))
                    {
                        counter += 1;
                    }
                    //Thread TH = (Thread)vlist[counter];

                    //if (TH.ThreadState == ThreadState.WaitSleepJoin)
                    //{
                    //    TimeSpan DIFF = DateTime.Now.Subtract((DateTime)vlist1[counter]);
                    //    if ((DIFF.Seconds > 70))
                    //    {
                    //        TH.Abort();
                    //        counter += 1;
                    //    }
                    //}
                    //else if ((TH.ThreadState == ThreadState.Stopped))
                    //{
                    //    counter += 1;
                    //}
                }
                #endregion







                //------------------------end------------------------------------------------//               
                //mergeList = returnFinalList(RBList, ABList);
                // mergeList = RBList.Concat(ABList).ToList();
               // mergeList = ((RBList.Concat(ABList).ToList()).Concat(GSList).ToList()).Concat(TyList).ToList();
                mergeList = (((RBList.Concat(ABList).ToList()).Concat(GSList).ToList()).Concat(TyList).ToList()).Concat(ESList).ToList();
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return mergeList;
        }



        #endregion
        protected void RBAvailability()
        {
            RBList = new List<SHARED>();
            rbdsrv = new RedBusService();
            if (AllowRB == true)
                RBList = RBList = rbdsrv.getRBJourneyList(RBBshared);
        }
        protected void GSAvailability()
        {
            GSList = new List<SHARED>();
            gstsrv = new GsrtcService();
            if (AllowGS == true)
                GSList = gstsrv.GetAvailableServiceDetails(GSBshared, srcplacecode, Destplacecode);
        }
        protected void TYAvailability()
        {
            TyList = new List<SHARED>();
            objtravelYari = new Travelyaari();
            if (AllowTY ==true)
                TyList = objtravelYari.GetSearchresult(objclsSearchRequest2, TYBshared);
        }
        protected void ABAvailability()
        {
            ABList = new List<SHARED>();
            abhsrv = new AbhiBusService();
            if (AllowAB == true)
                ABList = abhsrv.getABJourneyList(ABBshared);
        }
        protected void ESAvailability()
        {
            ESList = new List<SHARED>();
            ObjEtsService = new EtsService();
            if (AllowES == true)
                ESList = ObjEtsService.GetAllAvlBus(ESBshared);
        }
        #region[Merge Seat Layout]
        public string getBusSeatLayout(BS_SHARED.SHARED shared )
        {
            DataTable dtt = new DataTable();
            BS_SHARED.SHARED sharedd; abhsrv = new AbhiBusService(); GsrtcService gstsrv = new GsrtcService();
            rbdsrv = new RedBusService(); string mergeLayout = "";
            EtsService objEtsService = new EtsService();
            Travelyaari objTravelyaari = new Travelyaari();
            try
            {
                //-----------------------get src id and dest id-------------------------------//
                dtt = getSrcID(shared);
                foreach (DataRow dr in dtt.Rows)
                {
                    sharedd = new BS_SHARED.SHARED();
                    sharedd.srcID = dr["sourceid"].ToString().Trim();
                    sharedd.destID = dr["destinationid"].ToString().Trim();
                    sharedd.journeyDate = shared.journeyDate;
                    sharedd.serviceID = shared.serviceID;
                    sharedd.NoOfPax = shared.NoOfPax;
                    sharedd.SeatType = shared.SeatType;
                    sharedd.seatfare = shared.seatfare;
                    sharedd.seatfarewithMarkp = shared.seatfarewithMarkp.Trim();
                    sharedd.src = shared.src.Trim();
                    sharedd.dest = shared.dest.Trim();
                    sharedd.traveler = shared.traveler.Trim();
                    sharedd.agentID = shared.agentID;
                    sharedd.provider_name = shared.provider_name;
                    if (dr["Provider_Name"].ToString().ToUpper() == "AB" && shared.provider_name == "AB")
                    {
                        mergeLayout = abhsrv.ABSeatLayOut(sharedd);

                    }
                    else if (dr["Provider_Name"].ToString().ToUpper() == "RB" && shared.provider_name == "RB")
                    {
                        mergeLayout = rbdsrv.RBSeatLayout(sharedd);
                    }
                    else if (dr["Provider_Name"].ToString().ToUpper() == "GS" && shared.provider_name == "GS")
                    {

                        mergeLayout = gstsrv.murgeGSSeatLayout(sharedd);
                    }
                    else if (dr["Provider_Name"].ToString().ToUpper() == "TY" && shared.provider_name == "TY")
                    {
                        mergeLayout = objTravelyaari.murgeTYSeatLayOut(sharedd);
                    }
                    else if (dr["Provider_Name"].ToString().ToUpper() == "ES" && shared.provider_name == "ES")
                    {
                        mergeLayout = objEtsService.ESSeatLayout(sharedd);
                    }
                }

            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return mergeLayout;
        }
        #endregion
        #region[Get abhibus source list]
        public List<BS_SHARED.SHARED> getABList()
        {
            List<BS_SHARED.SHARED> list = new List<BS_SHARED.SHARED>();
            abhsrv = new AbhiBusService();
            try
            {
                list = abhsrv.getABSourceList();
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return list;
        }
        #endregion
        #region[Get abhibus dest list]

        public List<BS_SHARED.SHARED> getABDestList(string srcid, string src, string providerName)
        {
            List<BS_SHARED.SHARED> list = new List<BS_SHARED.SHARED>();
            abhsrv = new AbhiBusService();
            try
            {
                list = abhsrv.getABDestList(srcid.Trim(), src, providerName);
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return list;
        }
        #endregion
        #region[Get redbus source list]
        public List<BS_SHARED.SHARED> getRBList()
        {
            List<BS_SHARED.SHARED> list = new List<BS_SHARED.SHARED>();
            rbdsrv = new RedBusService();
            try
            {
                list = rbdsrv.getRBSrcList();
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return list;
        }
        #endregion
        #region[Get redbus dest list]

        public List<BS_SHARED.SHARED> getRBDestList(string srcid, string src, string providername)
        {
            List<BS_SHARED.SHARED> list = new List<BS_SHARED.SHARED>();
            rbdsrv = new RedBusService();
            try
            {
                list = rbdsrv.getRBDestList(srcid.Trim(), src, providername);
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return list;
        }
        #endregion
        #region[Get random num]
        public string getRand_NUM()
        {
            string num = "";
            shareddal = new BS_DAL.SharedDAL();
            try
            {
                num = shareddal.getRandom_Num();
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return num;
        }
        #endregion
        #region[Get Commission]
        public List<BS_SHARED.SHARED> getCommissionList(BS_SHARED.SHARED shared)
        {
            List<BS_SHARED.SHARED> list = new List<BS_SHARED.SHARED>();
            shareddal = new BS_DAL.SharedDAL();
            try
            {
                list = shareddal.getComList(shared);
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return list;
        }
        #endregion
        #region[Get Calculated Markup]
        public List<BS_SHARED.SHARED> getMarkUp(string agentid, string fare)
        {
            DataTable dt = new DataTable(); shareddal = new BS_DAL.SharedDAL();
            List<BS_SHARED.SHARED> list = new List<BS_SHARED.SHARED>();
            decimal adMrkp = 0; decimal agMrkp = 0; decimal mrkpfare = 0;
            try
            {
                dt = shareddal.getMarkup(agentid.Trim());
                if (fare.IndexOf(",") > 0)
                {
                    string[] strfare = fare.Split(',');
                    for (int f = 0; f <= strfare.Length - 1; f++)
                    {
                        switch (dt.Rows[0]["AdMrkupType"].ToString().ToUpper())
                        {
                            case "P":
                                adMrkp = Convert.ToDecimal(dt.Rows[0]["AdMrkupVal"].ToString());

                                break;
                            case "F":
                                adMrkp = Convert.ToDecimal(dt.Rows[0]["AdMrkupVal"].ToString());

                                break;
                            default:
                                break;
                        }
                        switch (dt.Rows[0]["AgMrkupType"].ToString().ToUpper())
                        {
                            case "P":
                                agMrkp = Convert.ToDecimal(dt.Rows[0]["AgMrkupVal"].ToString());

                                break;
                            case "F":
                                agMrkp = Convert.ToDecimal(dt.Rows[0]["AgMrkupVal"].ToString());

                                break;
                            default:
                                break;
                        }
                        mrkpfare = Convert.ToDecimal(strfare[f].Trim()) + adMrkp + agMrkp;
                        list.Add(new BS_SHARED.SHARED { admrkp = adMrkp, agmrkp = agMrkp, mrkpFare = mrkpfare });
                    }

                }
                else
                {
                    switch (dt.Rows[0]["AdMrkupType"].ToString().ToUpper())
                    {
                        case "P":
                            adMrkp = Convert.ToDecimal(dt.Rows[0]["AdMrkupVal"].ToString());

                            break;
                        case "F":
                            adMrkp = Convert.ToDecimal(dt.Rows[0]["AdMrkupVal"].ToString());

                            break;
                        default:
                            break;
                    }
                    switch (dt.Rows[0]["AgMrkupType"].ToString().ToUpper())
                    {
                        case "P":
                            agMrkp = Convert.ToDecimal(dt.Rows[0]["AgMrkupVal"].ToString());

                            break;
                        case "F":
                            agMrkp = Convert.ToDecimal(dt.Rows[0]["AgMrkupVal"].ToString());

                            break;
                        default:
                            break;
                    }
                    mrkpfare = Convert.ToDecimal(fare) + adMrkp + agMrkp;
                    list.Add(new BS_SHARED.SHARED { admrkp = adMrkp, agmrkp = agMrkp, mrkpFare = mrkpfare });
                }

            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return list;
        }
        #endregion
        #region[insert selected seat details]
        public void insertselected_seatDetails(BS_SHARED.SHARED shared)
        {
            try 
            {
                shareddal = new BS_DAL.SharedDAL();
                shareddal.insertselected_seatDetrails_Dal(shared);
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
        }
        #endregion
        #region[Get selected seat Details]
        public List<BS_SHARED.SHARED> getSelected_SeatDetails(BS_SHARED.SHARED shared)
        {
            List<BS_SHARED.SHARED> getList = new List<BS_SHARED.SHARED>();
            try
            {
                shareddal = new BS_DAL.SharedDAL();
                getList = shareddal.getSelectedSeatList(shared);

            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return getList;
        }
        #endregion
        #region[Gender Check]
        private object[] chkGender(BS_SHARED.SHARED shared)
        {
            abhsrv = new AbhiBusService(); object[] checkgender = null;
            try
            {
                checkgender = abhsrv.check_Gender(shared);
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return checkgender;
        }
        #endregion
        #region[Insert selected seat detials for book]
        public void insertselected_seatforbook(BS_SHARED.SHARED shared, List<BS_SHARED.SHARED> list)
        {
            shareddal = new BS_DAL.SharedDAL();
            try
            {
                shareddal.insertSelectedSeatForBooking(shared, list);
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
        }

        #endregion
        #region[Blocking Seat]
        public string getSelectedSeat_BlockKey(BS_SHARED.SHARED shared)
        {
            string Key = "";
            string list = ""; string[] name = null;
            shareddal = new BS_DAL.SharedDAL();
            BS_SHARED.SHARED sharedd = new BS_SHARED.SHARED();
            Travelyaari objTravelyaari = new Travelyaari();
            EtsService objEtsService = new EtsService();
            try
            {
                if (shared.provider_name == "AB")
                {
                    //---------------------------check for gender --------------------------//
                    object[] strchk = chkGender(shared);
                    if (strchk.Count() == 0)
                    {
                        abhsrv = new AbhiBusService();
                        Key = abhsrv.getBlockSeat_Details(shared);
                        sharedd.blockKey = Key;

                    }
                    //-----------------------------end--------------------------------------//

                }
                if (shared.provider_name == "GS")
                {
                    GsrtcService objGSRTC = new GsrtcService();
                    Key = objGSRTC.CreateTentativeBooking(shared);
                   
                }

                else if (shared.provider_name == "RB")
                {
                    rbdsrv = new RedBusService();
                    list = "{\"availableTripId\":\"" + shared.serviceID.Trim() + "\",\"boardingPointId\":\"" + shared.boardpointid.Trim() + "\",\"destination\":\"" + shared.destID.Trim() + "\",\"inventoryItems\":[";
                    for (int a = 0; a <= shared.paxseat.Count - 1; a++)
                    {
                        name = shared.paxname[a].ToString().Split(',');
                        if (shared.idproofReq == "true")
                        {
                            if (a == 0)
                                list += "{\"fare\":\"" + shared.perOriginalFare[a].ToString().Trim() + "\",\"ladiesSeat\":\"" + shared.ladiesSeat.Trim().Split(',')[a] + "\",\"passenger\":{\"address\":\"" + shared.paxaddress + "\",\"age\":\"" + shared.paxage[a].ToString().Trim() + "\",\"email\":\"" + shared.paxemail + "\",\"gender\":\"" + shared.gender[a].ToString().Trim() + "\",\"idNumber\":\"" + shared.idnumber + "\",\"idType\":\"" + shared.idtype + "\",\"mobile\":\"" + shared.paxmob + "\",\"name\":\"" + shared.paxname[a].ToString().Trim().Split(',')[0] + "\",\"primary\":\"" + shared.Isprimary.Trim() + "\",\"title\":\"" + shared.title[a].ToString().Trim() + "\"},\"seatName\":\"" + shared.paxseat[a].ToString().Trim() + "\"}";
                            else
                                list += ",{\"fare\":\"" + shared.perOriginalFare[a].ToString().Trim() + "\",\"ladiesSeat\":\"" + shared.ladiesSeat.Trim().Split(',')[a] + "\",\"passenger\":{\"age\":\"" + shared.paxage[a].ToString().Trim() + "\",\"gender\":\"" + shared.gender[a].ToString().Trim() + "\",\"name\":\"" + shared.paxname[a].ToString().Trim().Split(',')[0] + "\",\"primary\":\"" + "false".Trim() + "\",\"title\":\"" + shared.title[a].ToString().Trim() + "\"},\"seatName\":\"" + shared.paxseat[a].ToString().Trim() + "\"}";
                        }
                        else
                        {
                            if (a == 0)
                                list += "{\"fare\":\"" + shared.perOriginalFare[a].ToString().Trim() + "\",\"ladiesSeat\":\"" + shared.ladiesSeat.Trim().Split(',')[a] + "\",\"passenger\":{\"address\":\"" + shared.paxaddress + "\",\"age\":\"" + shared.paxage[a].ToString().Trim() + "\",\"email\":\"" + shared.paxemail + "\",\"gender\":\"" + shared.gender[a].ToString().Trim() + "\",\"idNumber\":\"" + shared.idnumber + "\",\"idType\":\"" + shared.idtype + "\",\"mobile\":\"" + shared.paxmob + "\",\"name\":\"" + shared.paxname[a].ToString().Trim().Split(',')[0] + "\",\"primary\":\"" + shared.Isprimary.Trim() + "\",\"title\":\"" + shared.title[a].ToString().Trim() + "\"},\"seatName\":\"" + shared.paxseat[a].ToString().Trim() + "\"}";
                            else
                                list += ",{\"fare\":\"" + shared.perOriginalFare[a].ToString().Trim() + "\",\"ladiesSeat\":\"" + shared.ladiesSeat.Trim().Split(',')[a] + "\",\"passenger\":{\"age\":\"" + shared.paxage[a].ToString().Trim() + "\",\"gender\":\"" + shared.gender[a].ToString().Trim() + "\",\"name\":\"" + shared.paxname[a].ToString().Trim().Split(',')[0] + "\",\"primary\":\"" + "false".Trim() + "\",\"title\":\"" + shared.title[a].ToString().Trim() + "\"},\"seatName\":\"" + shared.paxseat[a].ToString().Trim() + "\"}";

                        }
                    }

                    list += "],\"source\":\"" + shared.srcID.Trim() + "\"}";

                    //------------------------------call method for seat block-----------------------//
                    Key = rbdsrv.getBlockKey(list.Trim());
                    //Temp Value
                    //------------------------------------end-------------------------------------//

                    //----------------------------save request and block key into database--------------//
                    sharedd.blockKey = Key;
                    sharedd.tin = "";
                    sharedd.pnr = "";
                    sharedd.bookreq = list;
                    sharedd.agentID = shared.agentID.Trim();
                    sharedd.orderID = shared.orderID.Trim();
                    sharedd.provider_name = shared.provider_name.Trim();
                    shareddal.insertBooking_Request_Response(sharedd);
                    //-----------------------------------end------------------------------------------//                  
                }
                else if (shared.provider_name == "TY")
                {
                  sharedd= objTravelyaari.getBlockKeyRes(shared);
                  Key = sharedd.blockKey;
                }
                else if (shared.provider_name == "ES")
                {

                    sharedd = objEtsService.GetESblockKey(shared);
                    Key = sharedd.blockKey;
                    shareddal.insertBooking_Request_Response(sharedd);
                }
                
                //----------------------------Ledger----------------------------------//
                //if ((Key != "" && Key != "Error") || Key.IndexOf("Error") < 0)
                //if ((Key != "" && Key.ToString().Contains("Error") == false && Key.ToString().Contains("error") == false))     
                //{
                //    if (shared.provider_name != "GS")
                //    {
                //        shareddal.updateTicketno(shared);
                //        shared.avalBal = shareddal.deductAndaddfareAmt(shared, "subtract");
                //        shareddal.insertLedgerDetails(shared, "subtract");
                //    }
                //}
                //else
                //{
                    if (Key == "")
                        Key = "Error";
                    
                //}
                //----------------------------end------------------------------------//
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return Key;
        }
        #endregion
        #region[Book Seat]
        public List<BS_SHARED.SHARED> getSelectedSeat_TicketNo(BS_SHARED.SHARED shared)
        {
            List<BS_SHARED.SHARED> list = new List<BS_SHARED.SHARED>();
            YatraBilling.BUS_YATRA bs = new YatraBilling.BUS_YATRA();
            BS_SHARED.SHARED sharedd = new BS_SHARED.SHARED();
            GsrtcService objBusIndia = new GsrtcService();
            shareddal = new BS_DAL.SharedDAL();
            Travelyaari objTravelyaari = new Travelyaari();
            bool flg = false;
            try
            {
                if (shared.provider_name.Trim() == "AB")
                {
                    abhsrv = new AbhiBusService();
                    list = abhsrv.getFinalTicket(shared);
                    if (list.Count > 0)
                    {
                        if (list[0].status.ToUpper() != "FAIL")
                        {
                            //---------------update book response----------------//
                            sharedd.bookres = list[0].bookres;
                            sharedd.blockKey = shared.blockKey;
                            sharedd.tin = list[0].tin;
                            sharedd.pnr = list[0].tin;
                            sharedd.orderID = shared.orderID.Trim();
                            sharedd.agentID = shared.agentID.Trim();
                            shareddal.updateBookResponse(sharedd);
                            shareddal.updateTicketno(sharedd);
                            shareddal.updateTicketandPnr(sharedd);
                            //-------------------end----------------------------//
                            //------------------------integrate into yatra DB here---------------------------------//
                           
                           // flg = bs.IntegrateYatra(sharedd.orderID, sharedd.agentID, sharedd.tin, "B");
                            //-------------------------------end-------------------------------------------------//
                        }
                    }
                }
                else if (shared.provider_name.Trim() == "RB")
                {
                    rbdsrv = new RedBusService();
                    list = rbdsrv.getFinalTicket_RB(shared);
                    if (list.Count > 0)
                    {
                        if (list[0].status.ToUpper() != "FAIL")
                        {
                            //---------------update book response----------------//
                            sharedd.orderID = shared.orderID.Trim();
                            sharedd.agentID = shared.agentID.Trim();
                            sharedd.bookres = list[0].bookres;
                            sharedd.blockKey = list[0].blockKey;
                            sharedd.tin = list[0].tin;
                            sharedd.pnr = list[0].pnr;
                            sharedd.partialCancel = list[0].partialCancel;
                            shareddal.updateBookResponse(sharedd);
                            shareddal.updateTicketno(sharedd);
                            shareddal.updateTicketandPnr(sharedd);
                            //-------------------end----------------------------//
                            //------------------------integrate into yatra DB here---------------------------------//
                        
                           //flg = bs.IntegrateYatra(sharedd.orderID, sharedd.agentID, sharedd.tin, "B");
                            //-------------------------------end-------------------------------------------------//
                        }
                    }
                }
                else if (shared.provider_name.Trim() == "GS")
                {
                    list = objBusIndia.CreateConfirmBooking(shared);
                    if (list.Count > 0)
                    {
                        if (list[0].status.ToUpper() != "FAIL")
                        {
                            sharedd.orderID = shared.orderID.Trim();
                            sharedd.agentID = shared.agentID.Trim();
                            sharedd.bookres = list[0].bookres;
                            sharedd.bookreq = list[0].bookreq;
                            sharedd.blockKey = list[0].blockKey;
                            sharedd.tin = list[0].tin;
                            sharedd.pnr = list[0].pnr;
                            sharedd.partialCancel = list[0].partialCancel;                         
                            shareddal.updateBookResponse(sharedd);
                            shareddal.updateTicketno(sharedd);
                            shareddal.updateTicketandPnr(sharedd);
                            try
                            {
                                flg = bs.IntegrateYatra(sharedd.orderID, sharedd.agentID, sharedd.tin, "B");
                            }
                            catch (Exception ex)
                            {
                                erlog = new EXCEPTION_LOG.ErrorLog();
                                erlog.writeErrorLog(ex, "SHARED_BAL");
                            }
                        }
                    }
                }
                else if (shared.provider_name.Trim() == "TY")
                {
                    list = objTravelyaari.FinalTyBooking(shared);
                    if (list.Count > 0)
                    {
                        if (list[0].status.ToUpper() != "FAIL")
                        {
                            //---------------update book response----------------//
                            sharedd.orderID = shared.orderID.Trim();
                            sharedd.agentID = shared.agentID.Trim();
                            sharedd.bookres = list[0].bookres;
                            sharedd.blockKey = list[0].blockKey;
                            sharedd.tin = list[0].tin;
                            sharedd.pnr = list[0].pnr;
                            sharedd.partialCancel = list[0].partialCancel;
                            shareddal.updateBookResponse(sharedd);
                            shareddal.updateTicketno(sharedd);
                            shareddal.updateTicketandPnr(sharedd);
                            //-------------------end----------------------------//
                            //------------------------integrate into yatra DB here---------------------------------//

                            flg = bs.IntegrateYatra(sharedd.orderID, sharedd.agentID, sharedd.tin, "B");
                            //-------------------------------end-------------------------------------------------//
                        }
                    }
                }
                else if (shared.provider_name.Trim() == "ES")
                {
                    EtsService objEtsService = new EtsService();
                    list = objEtsService.GetFinalBookingES(shared);
                    if (list.Count > 0)
                    {
                        if (list[0].status.ToUpper() != "FAIL")
                        {
                            //---------------update book response----------------//
                            sharedd.orderID = shared.orderID.Trim();
                            sharedd.agentID = shared.agentID.Trim();
                            sharedd.bookres = list[0].bookres;
                            sharedd.blockKey = list[0].blockKey;
                            sharedd.tin = list[0].tin;
                            sharedd.pnr = list[0].pnr;
                            sharedd.partialCancel = list[0].partialCancel;
                            shareddal.updateBookResponse(sharedd);
                            shareddal.updateTicketno(sharedd);
                            shareddal.updateTicketandPnr(sharedd);
                            //-------------------end----------------------------//
                            //------------------------integrate into yatra DB here---------------------------------//

                            flg = bs.IntegrateYatra(sharedd.orderID, sharedd.agentID, sharedd.tin, "B");
                            //-------------------------------end-------------------------------------------------//
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return list;
        }
        
        #endregion
        #region[Get Ticket copy]
        public List<BS_SHARED.SHARED> getticketcopy(BS_SHARED.SHARED shared)
        {
            List<BS_SHARED.SHARED> ticketlist = new List<BS_SHARED.SHARED>();
            shareddal = new BS_DAL.SharedDAL();
            try
            {
                ticketlist = shareddal.getticketcopyDetails(shared);
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return ticketlist;
        }
        #endregion
        #region[Check Credit Limit]
        public decimal getCrdLimit(string agentid)
        {
            decimal crdlimit = 0;
            shareddal = new BS_DAL.SharedDAL();
            try
            {
                crdlimit = shareddal.getCreditLimit(agentid.Trim());
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return crdlimit;
        }
        #endregion
        #region[get partial cancel]
        
        public List<BS_SHARED.SHARED> getcancellist(BS_SHARED.SHARED shared)
        {

            GsrtcService objGsrtc = new GsrtcService();
            List<BS_SHARED.SHARED> getlist = new List<BS_SHARED.SHARED>();
            List<BS_SHARED.SHARED> getlist1 = new List<BS_SHARED.SHARED>();
            List<BS_SHARED.SHARED> getlistGS = new List<BS_SHARED.SHARED>();
            shareddal = new BS_DAL.SharedDAL(); rbdsrv = new RedBusService();
            DataSet ds = new DataSet();
            try
            {
                getlist = shareddal.getpartialcancellist(shared);

                for (int i = 0; i <= getlist.Count - 1; i++)
                {
                    if (getlist[i].provider_name == "AB")
                    {
                        getlist1.Add(new BS_SHARED.SHARED { agentID = getlist[i].agentID.Trim(), AgencyName = getlist[i].AgencyName.Trim(), orderID = getlist[i].orderID.Trim(), bookres = getlist[i].bookres.Trim(), pnr = getlist[i].pnr.Trim(), tin = getlist[i].tin.Trim(), src = getlist[i].src.Trim(), dest = getlist[i].dest.Trim(), busoperator = getlist[i].busoperator.Trim(), journeyDate = getlist[i].journeyDate.Trim(), Passengername = getlist[i].Passengername.Trim(), taNetFare = Convert.ToDecimal(getlist[i].taNetFare), originalfare = getlist[i].originalfare.Trim(), fare = getlist[i].fare.Trim(), partialCanAllowed = getlist[i].partialCanAllowed.Trim(), seat = getlist[i].seat.Trim(), canTime = getlist[i].canTime.Trim(), primaryPax = getlist[i].primaryPax.Trim(), idproofReq = getlist[i].idproofReq.Trim(), provider_name = getlist[i].provider_name.Trim(), canPolicy = getlist[i].canPolicy.Trim() });
                    }
                    else if (getlist[i].provider_name == "RB")
                    {
                        ds = rbdsrv.convertJsonStringToDataSet(getlist[0].bookres);
                        getlist1.Add(new BS_SHARED.SHARED { agentID = getlist[i].agentID.Trim(), AgencyName = getlist[i].AgencyName.Trim(), orderID = getlist[i].orderID.Trim(), bookres = getlist[i].bookres.Trim(), pnr = getlist[i].pnr.Trim(), tin = getlist[i].tin.Trim(), src = getlist[i].src.Trim(), dest = getlist[i].dest.Trim(), busoperator = getlist[i].busoperator.Trim(), journeyDate = getlist[i].journeyDate.Trim(), Passengername = getlist[i].Passengername.Trim(), taNetFare = Convert.ToDecimal(getlist[i].taNetFare), originalfare = getlist[i].originalfare.Trim(), fare = getlist[i].fare.Trim(), partialCanAllowed = getlist[i].partialCanAllowed.Trim(), seat = getlist[i].seat.Trim(), canTime = getlist[i].canTime.Trim(), primaryPax = getlist[i].primaryPax.Trim(), idproofReq = getlist[i].idproofReq.Trim(), provider_name = getlist[i].provider_name.Trim(), canPolicy = ds.Tables[0].Rows[0]["cancellationPolicy"].ToString().Trim(), tripId = getlist[i].tripId.Trim(), paymentmode = getlist[i].paymentmode.Trim(), status = getlist[i].status.Trim() });
                    }
                    //if (getlist[0].provider_name == "GS" || getlist[0].provider_name == "TY")
                    if (getlist[0].provider_name == "GS" || getlist[0].provider_name == "TY" || getlist[0].provider_name == "ES")
                    {
                        getlist1.Add(new BS_SHARED.SHARED { admrkp = getlist[i].admrkp, adcomm = getlist[i].adcomm, agmrkp = getlist[i].agmrkp, taTotFare = getlist[i].taTotFare, TA_tds = getlist[i].TA_tds, agentID = getlist[i].agentID.Trim(), AgencyName = getlist[i].AgencyName.Trim(), orderID = getlist[i].orderID.Trim(), bookres = getlist[i].bookres.Trim(), pnr = getlist[i].pnr.Trim(), tin = getlist[i].tin.Trim(), src = getlist[i].src.Trim(), dest = getlist[i].dest.Trim(), busoperator = getlist[i].busoperator.Trim(), journeyDate = getlist[i].journeyDate.Trim(), Passengername = getlist[i].Passengername.Trim(), taNetFare = Convert.ToDecimal(getlist[i].taNetFare), originalfare = getlist[i].originalfare.Trim(), fare = getlist[i].fare.Trim(), partialCanAllowed = getlist[i].partialCanAllowed.Trim(), seat = getlist[i].seat.Trim(), canTime = getlist[i].canTime.Trim(), primaryPax = getlist[i].primaryPax.Trim(), idproofReq = getlist[i].idproofReq.Trim(), provider_name = getlist[i].provider_name.Trim(), canPolicy = getlist[i].canPolicy.Trim(),serviceID=getlist[i].serviceID,canrequest=getlist[i].canrequest });

                    }
                }
                if (getlist[0].provider_name == "GS")
                {
                    string BiCanLayout = "";
                    
                    BiCanLayout += "<div>";
                    BiCanLayout += "<div class='lft w50'><span class='bld padding1 lft'>PNR. No</span><span class='padding1 lft'>" + getlist[0].pnr + "</span></div>";
                    BiCanLayout += "<div class='rgt w50'><span class='bld padding1 lft'>Ticket No</span><span class='padding1 lft'>" + getlist[0].tin + "</span></div>";
                    BiCanLayout += "</div>";
                    BiCanLayout += "<div class='clear'></div>";
                    BiCanLayout += "<hr/>";
                    BiCanLayout += "<div class='clear'></div>";
                    BiCanLayout += "<div>";
                    if (getlist.Count == 1 && getlist[0].status.ToUpper().Trim()=="BOOKRED")
                        BiCanLayout += "<div class='lft padding1s bld w30'>Pax Name</div><div class='lft padding1s bld w20'>Seat No</div><div class='lft padding1s bld w20'>Fare</div><div class='lft padding1s w20'><span><input type='checkbox' id='BiChkAllSelect' checked='checked' onclick='return false;' /></span><span class='bld'>All</span></div>";
                    else
                        BiCanLayout += "<div class='lft padding1s bld w30'>Pax Name</div><div class='lft padding1s bld w20'>Seat No</div><div class='lft padding1s bld w20'>Fare</div><div class='lft padding1s w20'><span><input type='checkbox' id='BiChkAllSelect'/></span><span class='bld'>All</span></div>";
                    BiCanLayout += "</div>";                    
                    BiCanLayout += "<div class='clear'></div>";
                    BiCanLayout += "<hr/>";
                    BiCanLayout += "<div class='clear'></div>";
                    BiCanLayout += "<div>";
                    for (int p = 0; p < getlist.Count; p++)
                    {
                        string sttatus = getlist[p].status.ToUpper().Trim();

                        if (sttatus == "BOOKED")
                        {
                            if (getlist.Count == 1)
                                BiCanLayout += "<div class='lft padding1s w30'>" + getlist[p].Passengername.Split(',')[0] + "</div><div class='lft padding1s w20 sseat'>" + getlist[p].seat + "</div><div class='lft padding1s w20'>" + getlist[p].taTotFare + "</div><div class='lft padding1s w20'><span><input checked='checked' class='BiChkSelect' type='checkbox' onclick='return false;' id='BiChkSelect" + p + "' /></span></div>";
                            else
                                BiCanLayout += "<div class='lft padding1s w30'>" + getlist[p].Passengername.Split(',')[0] + "</div><div class='lft padding1s w20 sseat'>" + getlist[p].seat + "</div><div class='lft padding1s w20'>" + getlist[p].taTotFare + "</div><div class='lft padding1s w20'><span><input  class='BiChkSelect' type='checkbox' id='BiChkSelect" + p + "' /></span></div>";
                        }
                        else
                        {
                            
                                BiCanLayout += "<div class='lft padding1s w30'>" + getlist[p].Passengername.Split(',')[0] + "</div><div class='lft padding1s w20 sseat'>" + getlist[p].seat + "</div><div class='lft padding1s w20'>" + getlist[p].taTotFare + "</div><div class='lft padding1s w20'><span>"+sttatus.ToString() +"</span></div>";

                        }
                        BiCanLayout += "<div class='clear'></div>";
                        BiCanLayout += "<hr/>";
                        BiCanLayout += "<div class='clear'></div>";
                    }                 
                    BiCanLayout += "</div>";
                   
                    //string busCalcelType = "";
                    //busCalcelType += " Cancelation Type <br><br>";
                    //busCalcelType += " <input onclick='chkcancelationType(this.id)' id='FC' type='radio' name='FC' value='FC'>Full Cancellation <input onclick='chkcancelationType(this.id)' id='Motkt' type='radio' name='Motkt' value='Motkt'>Modified Ticket<br><br>";
                    //busCalcelType += "<div id='FCT'style='display:none'>Please Select Full Cancelation Type <br><br>";
                    //busCalcelType += " <input onclick='chkcancelationType(this.id)' id='NFC' type='radio' name='NFC' value='NFC'>Normal Full Cancellation";
                    //busCalcelType += " <input onclick='chkcancelationType(this.id)' id='OMC' type='radio' name='OMC' value='OMC'>Operator Mistake Full Cancellation";
                    //busCalcelType += " <input onclick='chkcancelationType(this.id)' id='WR' type='radio' name='WR' value='WR'>Manage Decision Full Cancelltion<br><br>";
                    //busCalcelType += "<div id='WRID'style='display:none'>Please Select Management Decision Full Cancelltion Type <br><br><input onclick='chkcancelationType(this.id)' id='WRC' type='radio' name='WRC' value='WRC'>With Reservation Fee";
                    //busCalcelType += "<input onclick='chkcancelationType(this.id)' id='WOC' type='radio' name='WOC' value='WOC'>With Out Reservation Fee<br></div></div>";
                    //
                    getlist1[0].bookreq = BiCanLayout;
                    getlist1[0].status = "success";
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return getlist1;
        }
        #endregion
        #region[Cancel Ticket]
        public string[] cancelTicket(ref BS_SHARED.SHARED shared)
        {
            string[] canStr = null; abhsrv = new AbhiBusService();
            string canStr_RB = ""; string[] canResp = new string[5]; ;
            rbdsrv = new RedBusService();
            Travelyaari objTravelyaari = new Travelyaari();
            EtsService objEtsService = new EtsService();
            try
            {
                if (shared.provider_name == "AB")
                {
                    canStr = abhsrv.getCancelConfrimation(shared);
                    if (canStr[0].ToUpper() == "FAIL")
                    {
                        canResp[0] = canStr[0].Trim();
                        canResp[1] = "This Ticket is not cancellable:Invalid Ticket";
                    }
                    else
                    {
                        canResp = abhsrv.getcancelTicketStatus(shared);
                    }
                }
                else if (shared.provider_name == "RB")
                {
                    canStr_RB = rbdsrv.getcancelData(shared.tin.Trim());
                    if (canStr_RB.ToUpper() == "TRUE")
                    {
                        canResp = rbdsrv.getcancelTicketStatus(ref shared);
                    }
                    else
                    {
                        //update canceldetail to field as api cancel status as failed
                        canResp[0] = "Fail";
                        canResp[1] = "This Ticket is not cancellable:Invalid Ticket";
                    }



                }
                else if (shared.provider_name == "GS")
                {
                    canStr_RB = rbdsrv.getcancelData(shared.tin.Trim());
                    if (canStr_RB.ToUpper() == "TRUE")
                    {
                        canResp = rbdsrv.getcancelTicketStatus(ref shared);
                    }
                    else
                    {
                        canResp[0] = "Fail";
                        canResp[1] = "This Ticket is not cancellable:Invalid Ticket";
                    }
                }
                else if (shared.provider_name == "TY")
                {
                    canResp = objTravelyaari.cancelTyTicket(shared);
                }
                else if (shared.provider_name == "ES")
                {
                    canResp = objEtsService.ESGetCancelTicket(shared);

                }

            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return canResp;
        }
        #endregion
        #region[Return Final Result List]
        private List<BS_SHARED.SHARED> returnFinalList(List<BS_SHARED.SHARED> rblist, List<BS_SHARED.SHARED> ablist)
        {
            List<BS_SHARED.SHARED> finalresultList = new List<BS_SHARED.SHARED>();
            string[] strfare = null; string[] strfare1 = null;
            try
            {
                if (rblist.Count > 0 && ablist.Count > 0)
                {
                    for (int a = 0; a <= ablist.Select(c => c.traveler).Distinct().ToList().Count - 1; a++)
                    {
                        for (int r = 0; r <= rblist.Select(c => c.traveler).Distinct().ToList().Count - 1; r++)
                        {
                            strfare = ablist[a].seat_Originalfare;
                            strfare1 = rblist[r].seat_Originalfare;
                            if (ablist[a].traveler.Replace(" ", "").Trim().ToUpper() == rblist[r].traveler.Replace(" ", "").Trim().ToUpper())
                            {
                                if (Convert.ToInt32(strfare[0]) <= Convert.ToInt32(strfare1[0]))
                                {
                                    finalresultList.Add(ablist[a]);
                                }
                            }
                            else
                            {
                                if (finalresultList.Exists(x => x.traveler.Replace(" ", "").Trim().ToUpper() == rblist[r].traveler.Replace(" ", "").Trim().ToUpper()) == false)
                                {
                                    finalresultList.Add(rblist[r]);
                                }
                                else if (finalresultList.Exists(x => x.departTime.Trim() == rblist[r].departTime.Trim()) == false)
                                {
                                    finalresultList.Add(rblist[r]);
                                }
                            }
                        }
                        if (finalresultList.Exists(x => x.traveler.Replace(" ", "").Trim().ToUpper() == ablist[a].traveler.Replace(" ", "").Trim().ToUpper()) == false)
                        {
                            finalresultList.Add(ablist[a]);
                        }
                        else if (finalresultList.Exists(x => x.departTime.Trim() == ablist[a].departTime.Trim()) == false)
                        {
                            finalresultList.Add(ablist[a]);
                        }
                    }
                }
                else if (rblist.Count == 0 && ablist.Count > 0)
                {
                    finalresultList = ablist;
                }
                else if (rblist.Count > 0 && ablist.Count == 0)
                {
                    finalresultList = rblist;
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return finalresultList;
        }
        #endregion
        public void getdestDestination_ETS()
        {
            List<SHARED> getESListsrc = new List<SHARED>();
            List<SHARED> getESListDest = new List<SHARED>();
            EtsService objEtsService = new EtsService();
            shareddal = new SharedDAL();
            try
            {
                getESListsrc = objEtsService.GetAllEtsSource();

                if (getESListsrc.Count != 0)
                {
                    for (int srno = 0; srno < getESListsrc.Count; srno++)
                    {
                        getESListDest = objEtsService.GetAllEtsDest(getESListsrc[srno].src, getESListsrc[srno].srcID);
                        if (getESListDest.Count != 0)
                            shareddal.InsertDestList(getESListDest, getESListsrc[srno].srcID, "ES");
                    }
                }
            }

            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }

        }
        public void getdestDestination()
        {
            //List<BS_SHARED.SHARED> getABListsrc = new List<BS_SHARED.SHARED>();
            // List<BS_SHARED.SHARED> getRBListsrc = new List<BS_SHARED.SHARED>();
         //   List<BS_SHARED.SHARED> getTyListsrc = new List<BS_SHARED.SHARED>();
            //List<BS_SHARED.SHARED> getABListDest = new List<BS_SHARED.SHARED>();
            //List<BS_SHARED.SHARED> getRBListDest = new List<BS_SHARED.SHARED>();
          //    List<BS_SHARED.SHARED> getTyListDest = new List<BS_SHARED.SHARED>();
         
            //AbhiBusService abhi = new AbhiBusService();
            //RedBusService redbus = new RedBusService();
            GsrtcService gsrtcDervice = new GsrtcService();   
           // Travelyaari objTravelyaari=new Travelyaari();
           // clsCities clsCitieslist = new clsCities();
            shareddal = new BS_DAL.SharedDAL();
            try
            {
                //getABListsrc = abhi.getABSourceList();
               // getRBListsrc = redbus.getRBSrcList();              
                gsrtcDervice.getGScityList();
                //clsCitieslist = objTravelyaari.GetAllTySource();

                //if (clsCitieslist.Cities.Length != 0)
                //{
                //    for (int srno = 0; srno < clsCitieslist.Cities.Length; srno++)
                //    {
                //        getTyListDest = objTravelyaari.GetAllTyDest(Convert.ToInt32(clsCitieslist.Cities[srno].CityID), clsCitieslist.Cities[srno].CityName, "TY");
                //        shareddal.InsertDestList(getTyListDest, clsCitieslist.Cities[srno].CityID.ToString(), "TY");
                //    }
                //}
         
                //if (getABListsrc.Count != 0)
                //{
                //    for (int srcno = 0; srcno < getABListsrc.Count; srcno++)
                //    {
                //        getABListDest = abhi.getABDestList(getABListsrc[srcno].srcID.ToString(), getABListsrc[srcno].src.ToString(), getABListsrc[srcno].provider_name.ToString());
                //        shareddal.InsertDestList(getABListDest, getABListsrc[srcno].srcID.ToString(), getABListsrc[srcno].provider_name.ToString());
                //    }
                //}
                //if (getRBListsrc.Count != 0)
                //{
                //    for (int srcno = 0; srcno < getRBListsrc.Count; srcno++)
                //    {
                //        getRBListDest = redbus.getRBDestList(getRBListsrc[srcno].srcID.ToString(), getRBListsrc[srcno].src.ToString(), getRBListsrc[srcno].provider_name.ToString());
                //        shareddal.InsertDestList(getRBListDest, getRBListsrc[srcno].srcID.ToString(), getRBListsrc[srcno].provider_name.ToString());
                //    }
                //
            }
            
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
        }
        public List<BS_SHARED.SHARED> SetPaxInformationGS(BS_SHARED.SHARED shared)
        {
            List<BS_SHARED.SHARED> SetList = new List<BS_SHARED.SHARED>();
            try
            {
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
                SetList = new List<BS_SHARED.SHARED>();
                SetList.Add(new BS_SHARED.SHARED { orderID = "Error_" + ex.Message + "" });
            }
            return SetList;
        }
        public BS_SHARED.SHARED getSelectedSeatPaxInfo(string BOOKING_REF, string ORDERID)
        {
            DataSet ds = new DataSet();
            BS_SHARED.SHARED shared = new BS_SHARED.SHARED();
            BS_DAL.SharedDAL sheareddal1 = new BS_DAL.SharedDAL();
            try
            {
                ds = sheareddal1.getSelectedSeatPaxDetails(BOOKING_REF, ORDERID);

                shared.arrTime = Convert.ToString(ds.Tables[0].Rows[0]["Arr_Time"]);
                shared.blockKey = Convert.ToString(ds.Tables[0].Rows[0]["Blockkey"]);
                shared.Phone = Convert.ToString(ds.Tables[0].Rows[0]["pickUpContactNo"]);
                shared.departTime = Convert.ToString(ds.Tables[0].Rows[0]["Dept_Time"]);
                shared.Dur_Time = Convert.ToString(ds.Tables[0].Rows[0]["Dur_Time"]);
                shared.orderID = Convert.ToString(ds.Tables[0].Rows[0]["ORDERID"]);
                shared.journeyDate = Convert.ToString(ds.Tables[0].Rows[0]["JOURNEYDATE"]);
                shared.BookingDate = Convert.ToString(ds.Tables[0].Rows[0]["CREATEDDATE"]);
                shared.src = Convert.ToString(ds.Tables[0].Rows[0]["SOURCE"]).Split('(')[0]; //id
                shared.srcID = Convert.ToString(ds.Tables[0].Rows[0]["SOURCE"]).Split('(')[1].Split(')')[0]; //id
                shared.dest = Convert.ToString(ds.Tables[0].Rows[0]["DESTINATION"]).Split('(')[0]; //id
                shared.destID = Convert.ToString(ds.Tables[0].Rows[0]["DESTINATION"].ToString().Split('(')[1].Split(')')[0]); //id
                shared.serviceID = Convert.ToString(ds.Tables[0].Rows[0]["TRIPID"]);
                shared.busoperator = Convert.ToString(ds.Tables[0].Rows[0]["BUSOPERATOR"]);
                var ladies = Convert.ToString(ds.Tables[0].Rows[0]["LADIESSEAT"]).Trim().Split(',');
                string ladie = "";
                for (int ld = 0; ld < ladies.Length; ld++)
                {
                    //ladie = ladie + ladies[ld].Split(':')[1].Trim().ToLower() + ",";
                    ladie = ladie + ladies[ld].Trim().ToLower() + ",";

                }
                shared.ladiesSeat = ladie.Substring(0, ladie.LastIndexOf(','));//Convert.ToString(ds.Tables[0].Rows[0]["LADIESSEAT"]);          
                shared.fare = Convert.ToString(Convert.ToDecimal(ds.Tables[0].Rows[0]["FARE"].ToString().Split(',')[0]));
                decimal basefare = 0;
                for (int f = 0; f < ds.Tables[0].Rows[0]["ORIGINAL_FARE"].ToString().Split(',').Length; f++)
                {
                    basefare += Convert.ToDecimal(ds.Tables[0].Rows[0]["ORIGINAL_FARE"].ToString().Split(',')[f]);
                }
                shared.originalfare = Convert.ToString(basefare);
                shared.boardpoint = Convert.ToString(ds.Tables[0].Rows[0]["BOARDINGPOINT"]).Split('&')[0];
                shared.boardpointid = Convert.ToString(ds.Tables[0].Rows[0]["BOARDINGPOINT"].ToString().Split('&')[1]);
                shared.droppoint = Convert.ToString(ds.Tables[0].Rows[0]["DROPINGPOINT"]).Split('&')[0];
                shared.droppointid = Convert.ToString(ds.Tables[0].Rows[0]["DROPINGPOINT"].ToString().Split('&')[1]);
                shared.agentID = Convert.ToString(ds.Tables[0].Rows[0]["AGENTID"]);
                shared.status = Convert.ToString(ds.Tables[0].Rows[0]["STATUS"]);
                shared.admrkp = Convert.ToDecimal(ds.Tables[0].Rows[0]["ADMIN_MARKUP"]);
                shared.agmrkp = Convert.ToDecimal(ds.Tables[0].Rows[0]["AGENT_MARKUP"]);
                shared.adcomm = Convert.ToDecimal(ds.Tables[0].Rows[0]["ADMIN_COMM"]);
                shared.taTds = Convert.ToDecimal(ds.Tables[0].Rows[0]["TA_TDS"]);
                shared.totFare = Convert.ToDecimal(ds.Tables[0].Rows[0]["TOTAL_FARE"]);
                shared.taTotFare = Convert.ToDecimal(ds.Tables[0].Rows[0]["TA_TOT_FARE"]);
                shared.taNetFare = Convert.ToDecimal(ds.Tables[0].Rows[0]["TA_NET_FARE"]);
                shared.idproofReq = Convert.ToString(ds.Tables[0].Rows[0]["ISIDPROOF_REQUIRED"]);
                shared.NoOfPax = Convert.ToString(ds.Tables[0].Rows[0]["NOOF_PAX"]);
                shared.busType = Convert.ToString(ds.Tables[0].Rows[0]["BUS_TYPE"]);
                shared.provider_name = Convert.ToString(ds.Tables[0].Rows[0]["PROVIDER_NAME"]);
                shared.canPolicy = Convert.ToString(ds.Tables[0].Rows[0]["CANCEL_POLICY"]);
                shared.partialCancel = Convert.ToString(ds.Tables[0].Rows[0]["PARTIAL_CANCEL"]);
                shared.AgencyName = "";
                shared.perOriginalFare = new List<string>();
                shared.gender = new List<string>();
                shared.paxseat = new List<string>();
                shared.paxname = new List<string>();
                shared.title = new List<string>();
                shared.paxage = new List<string>();
                foreach (DataRow dr in ds.Tables[1].Rows)
                {
                    // shared.gender.Add(Convert.ToString(dr["GENDER"]));
                    if (Convert.ToString(dr["GENDER"]) == "MALE")
                    {
                        shared.title.Add("Mr");
                        shared.gender.Add("MALE");
                    }
                    else
                    {
                        shared.title.Add("Ms");
                        shared.gender.Add("FEMALE");
                    }
                    shared.paxseat.Add(Convert.ToString(dr["SEATNO"]));
                    shared.paxname.Add(Convert.ToString(dr["PAXNAME"]));
                    shared.idnumber = Convert.ToString(dr["PRIMARY_PAX_IDNUMBER"]);
                    shared.idtype = Convert.ToString(dr["PRIMARY_PAX_IDTYPE"]);
                    // shared.primaryPax = Convert.ToString(dr["PRIMARY_PAX_NAME"]);
                    shared.paxmob = Convert.ToString(dr["PRIMARY_PAX_PAXMOB"]);
                    shared.paxemail = Convert.ToString(dr["PRIMARY_PAX_PAXEMAIL"]);
                    shared.paxaddress = Convert.ToString(dr["PRIMARYPAX_ADDRESS"]);
                    shared.perOriginalFare.Add(Convert.ToString(dr["ORIGINAL_FARE"]));
                    shared.paxage.Add(Convert.ToString(dr["PAX_AGE"]));
                    //  shared.title.Add(Convert.ToString(dr["PAX_TITLE"]));
                    shared.pnr = Convert.ToString(dr["PNR"]);
                    shared.ticketno = Convert.ToString(dr["TICKETNO"]);
                    if ((Convert.ToString(dr["PRIMARY_PAX_NAME"])) != "")
                    {
                        shared.primaryPax = Convert.ToString(dr["PRIMARY_PAX_NAME"]);
                    }

                    if (Convert.ToString(dr["ISPRIMARY"]) == "primary")
                        shared.Isprimary = shared.Isprimary + "true" + ",";
                    else
                        shared.Isprimary = shared.Isprimary + "false" + ",";
                }
                if (shared.provider_name == "GS")
                {
                    if(ds.Tables.Count==3)
                    shared.bookres = Convert.ToString(ds.Tables[2].Rows[0]["BOOK_RESPONSE"]);
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return shared;
        }
        public string SerializeAnObject(object obj)
        {
            System.Xml.XmlDocument doc = new XmlDocument();
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(obj.GetType());
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            try
            {
                serializer.Serialize(stream, obj);
                stream.Position = 0;
                doc.Load(stream);
                return doc.InnerXml;
            }
            catch
            {
                throw;
            }
            finally
            {
                stream.Close();
                stream.Dispose();
            }
        }
        public string GetBoardingPoints(string tripId, string boardingId)
        {
            RedBusService objrb = new RedBusService();
            string GetBoardingPointsres = "";
            GetBoardingPointsres = objrb.GetBoardingPoints(tripId, boardingId);
            string htmllayout = "";
            if (GetBoardingPointsres != "" && GetBoardingPointsres.Contains("Error"))
            {
                htmllayout += "<span>Not Available</span>";
            }
            else
            {
                JObject Pointarray = JObject.Parse(GetBoardingPointsres);
                htmllayout += "<span class='bld'>Boarding point Address : </span><span>" + Convert.ToString(Pointarray["address"]) + "</span><br/><span class='bld'>Contact Number :</span><span>" + Convert.ToString(Pointarray["contactnumber"]) + "</span><br/><span class='bld'>Landmark :</span><span>" + Convert.ToString(Pointarray["landmark"]) + "</span><br/><span class='bld'>Location :</span><span>" + Convert.ToString(Pointarray["locationName"]) + "</span><br/><span class='bld'>Name :</span><span>" + Convert.ToString(Pointarray["name"]) + "</span>";
            }
            return htmllayout;
        }


        public string GetServiceCharges(string fare, string providername, DataSet dsSrvCharge)
        {
            string type = "";
            string canchrg = "0";

            try
            {


                DataRow[] dtRow = dsSrvCharge.Tables[0].Select("Provider_Name='" + providername + "'");
                if (dtRow != null && dtRow.Count() > 0)
                {
                    type = dtRow[0]["Type"].ToString();
                    canchrg = dtRow[0]["Cancellation_Chrg"].ToString();
                    if (type.ToUpper().Trim() == "P")
                    {
                        canchrg = Convert.ToString(Convert.ToDouble(fare) * Convert.ToDouble(canchrg) / 100);
                    }

                }
            }
            catch (Exception ex)
            {
                canchrg = "0";

            }
            return canchrg ;
        }
    }
}
public class paxlistitem
{
    public string paxname { get; set; }
    public int paxAge { get; set; }
    public string paxGender { get; set; }
}
