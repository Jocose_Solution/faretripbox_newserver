﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
namespace HotelShared
{
   public class HotelInformation
   {
       public string Address { get; set; }
       public string ThumbImg { get; set; }
       public string HotelDiscription { get; set; }
       public string HotelServices { get; set; }
       public string Latitude { get; set; }
       public string Longitude { get; set; }
       public string StarRating { get; set; } 
   }
   public class MarkupList
   {
       public decimal TotelAmt { get; set; }
       public string AdminMrkType { get; set; }
       public decimal AdminMrkAmt { get; set; }
       public decimal AdminMrkPercent { get; set; }
       public string AgentMrkType { get; set; }
       public decimal AgentMrkAmt { get; set; }
       public decimal AgentMrkPercent { get; set; }
       public string DistrMrkType { get; set; }
       public decimal DistrMrkPercent { get; set; }
       public decimal DistrMrkAmt { get; set; }
       public decimal VenderServiceTaxAmt { get; set; }
       public decimal AgentServiceTaxAmt { get; set; }
   }

   public class HotelRatePlan
   {
       public string RatePlanCode { get; set; }
       public string discountMsg { get; set; }
       public string inclusions { get; set; }
   }
   public class Markups
   {
       public string AdminMrkType { get; set; }
       public decimal AdminMrkPer { get; set; }
       public string AgentMrkType { get; set; }
       public decimal AgentMrkPer { get; set; }
       public string DistrMrkType { get; set; }
       public decimal DistrMrkPer { get; set; }
       public decimal MrkAmt { get; set; }
   }
   public class RoomCatgryDetails
   {
       public string Id { get; set; }
       public string Description { get; set; }
       public string RoomDescription { get; set; }
   }
   public enum HotelStatus
   {
       Request, Hold, Confirm, Cancelled, CancelPending, InProcess, Rejected, Domestic, International, Fixed, Percentage
   }
}
