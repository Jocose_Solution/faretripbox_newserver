﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
namespace HotelShared
{
   public class RoomList
   {
        public string HotelCode { get; set; }
        public string RoomName { get; set; }
        public string RoomTypeCode { get; set; }
        public string RatePlanCode { get; set; }
        public string discountMsg { get; set; }
        public string inclusions { get; set; }
        public string CancelationPolicy { get; set; }
        public string EssentialInformation { get; set; }
        public string Smoking { get; set; }
        public string RoomDescription { get; set; }
        public string RoomImage { get; set; }
        public string maxAdult { get; set; }
        public string maxChild { get; set; }
        public string maxGuest { get; set; }
        public string Refundable { get; set; }
        public decimal AmountBeforeTax { get; set; }
        public decimal Taxes { get; set; }
        public decimal DiscountAMT { get; set; }
        public decimal ExtraGuest_Charge { get; set; }
        public decimal Total_Org_Roomrate { get; set; }
        public decimal TotalRoomrate { get; set; }
        public decimal MrkTaxes { get; set; }
        public decimal AdminMarkupPer { get; set; }
        public decimal AdminMarkupAmt { get; set; }
        public string AdminMarkupType { get; set; }
        public decimal AgentMarkupPer { get; set; }
        public decimal AgentMarkupAmt { get; set; }
        public string AgentMarkupType { get; set; }
        public decimal distriMarkupPer { get; set; }
        public decimal distriMarkupAmt { get; set; }
        public string distriMarkupType { get; set; }
        public decimal V_ServiseTaxAmt { get; set; }
        public decimal AgentServiseTaxAmt { get; set; }

        public string OrgRateBreakups { get; set; }
        public string MrkRateBreakups { get; set; }
        public string DiscRoomrateBreakups { get; set; }
        public string HtlError { get; set; }
        public string Provider { get; set; }
        public decimal EXHotelFee { get; set; }
        public string EXRateKey { get; set; }
    }
    public class RoomType
    {
        public string RoomTypeName { get; set; }
        public string RoomTypeCode { get; set; }
        public string Smoking { get; set; }
        public string RoomDescription { get; set; }
        public string RoomImage { get; set; }
        public string maxAdult { get; set; }
        public string maxChild { get; set; }
        public string maxGuest { get; set; }
    }
    public class RatePlan
    {
        public string RatePlanName { get; set; }
        public string RatePlanCode { get; set; }
        public string discountMsg { get; set; }
        public string inclusions { get; set; }
        public string CancelationPolicy { get; set; }
    }
    public class RoomRate
    {
        public string RatePlanCode { get; set; }
        public string RoomTypeCode { get; set; }
        public decimal DiscountAMT { get; set; }
        public decimal AmountBeforeTax { get; set; }
        public decimal Taxes { get; set; }        
        public decimal MrkTaxes { get; set; }
        public decimal ExtraGuest_Charge { get; set; }
        public decimal Total_Org_Roomrate { get; set; }
        public decimal TotalRoomrate { get; set; }
        public decimal AdminMarkupPer { get; set; }
        public decimal AdminMarkupAmt { get; set; }
        public string AdminMarkupType { get; set; }
        public decimal AgentMarkupPer { get; set; }
        public decimal AgentMarkupAmt { get; set; }
        public string AgentMarkupType { get; set; }
        public decimal distriMarkupPer { get; set; }
        public decimal distriMarkupAmt { get; set; }
        public string distriMarkupType { get; set; }
        public decimal V_ServiseTaxAmt { get; set; }
        public decimal ServiseTaxAmt { get; set; }
        public string Org_RoomrateBreakups { get; set; }
        public string Mrk_RoomrateBreakups { get; set; }
        public string Disc_RoomrateBreakups { get; set; }
    }
    public class SelectedHotel
    {
        public string HotelName { get; set; }
        public string HotelCode { get; set; }
        public string StarRating { get; set; }
        public string Lati_Longi { get; set; }
        public string Location { get; set; }
        public string HotelAddress { get; set; }
        public string HotelContactNo { get; set; }
        public string ThumbnailUrl { get; set; }
        public string HotelDescription { get; set; }
        public string AmenityDescription { get; set; }
        public string CheckInTime { get; set; }
        public string CheckOutTime { get; set; }
        public string FlexibleCheckIn { get; set; }
        public string NumberOfRooms { get; set; }
        public string ReviewComment { get; set; }
        public string ReviewPostDate { get; set; }
        public string ReviewRating { get; set; }
        public string ReviewerName { get; set; }
        public ArrayList RoomcatList { get; set; }
        public string HotelImage { get; set; }
        public string Attraction { get; set; }
        public string HotelAmenities { get; set; }
        public string RoomAmenities { get; set; }
        public string HtlError { get; set; }
        public string EXHotelPolicy { get; set; }
    }
    public class CitySearch
    {
        public string CityName { get; set; }
        public string CityCode { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string SearchType { get; set; }
        public string RegionID { get; set; }
        public string NoOfHotel { get; set; }
     //   public bool Availability_0Star { get; set; }
    }
}
