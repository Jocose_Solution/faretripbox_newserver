﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMN_BAL.Flight
{
    public class FlighBal
    {
        public string Constr { get; set; }
        public COMN_DAL.Flight.FlightDal ObjFltDal = null;
        public FlighBal(string connectionString)
        {
            Constr = connectionString;
            ObjFltDal = new COMN_DAL.Flight.FlightDal(connectionString);
        }

        public DataSet GetFlightDetailsByOrderId(string orderId, string agentId)
        {
            return ObjFltDal.GetFlightDetailsByOrderId(orderId, agentId);
        }

        public DataSet GetPaxDetailsByOrderId(string orderId)
        {
            return ObjFltDal.GetPaxDetailsByOrderId(orderId);
        }

        public DataSet GetHdrDetails(string orderId)
        {
            return ObjFltDal.GetHdrDetails(orderId);
        }

        public DataSet GetFltFareDtl(string orderId)
        {
            return ObjFltDal.GetFltFareDtl(orderId);
        }

        public DataSet GetTktCredentials_GAL(string vc, string trip, string CrdType)
        {
            return ObjFltDal.GetTktCredentials_GAL(vc, trip, CrdType);
        }

        public int InsertGdsBkgLogs(string OrderId, Hashtable PnrHT)
        {
            return ObjFltDal.InsertGdsBkgLogs(OrderId, PnrHT);
        }



        public string GetGDSServiceCodeForPNRCreation(string vc, string trip)
        {
            return ObjFltDal.GetGDSServiceCodeForPNRCreation(vc, trip);
        }

        public int Ledgerandcreditlimit_Transaction_Insert(string AGENTID, double TOTALAFETRDIS, string TRACKID, string VC, string GDSPNR, string AGENCYNAME, string IP, string ProjectId, string BookedBy, string BillNo, string AvailBal, string EasyID, string status = "")
        {
            return ObjFltDal.Ledgerandcreditlimit_Transaction_Insert(AGENTID, TOTALAFETRDIS, TRACKID, VC, GDSPNR, AGENCYNAME, IP, ProjectId, BookedBy, BillNo, AvailBal, EasyID, status);
        }

        public DataSet GetAgencyDetails(string UserId)
        {
            return ObjFltDal.GetAgencyDetails(UserId);
        }

        public int UpdateFltHeader(string trackid, string agname, string gdspnr, string airpnr, string status)
        {
            return ObjFltDal.UpdateFltHeader(trackid, agname, gdspnr, airpnr, status);
        }

        public DataSet GetFltHoldBookingCharge(string GroupType, string agentId, string aircode, string tripType)
        {
            return ObjFltDal.GetFltHoldBookingCharge(GroupType, agentId, aircode, tripType);
        }
        public int UpdateHoldBookingCharge(string OrderIDO, string holdBokingStatusO, decimal holdBookingChargeO, string OrderIDR, string holdBokingStatusR, decimal holdBookingChargeR)
        {
            return ObjFltDal.UpdateHoldBookingCharge(OrderIDO, holdBokingStatusO, holdBookingChargeO, OrderIDR, holdBokingStatusR, holdBookingChargeR);
        }
    }
}
