﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using STD.Shared;
using LCC;
using System.Data;
using System.Collections;
using schemas.navitaire.com.WebServices.DataContracts.Booking;
//using ExceptionLog;
//using STD.WEB;

namespace STD.BAL
{
    /// <summary>
    /// Summary description for LCCResult
    /// </summary>
    public class LCCResult
    {
        string ConnStr = "";
        public LCCResult()
        { }
        public LCCResult(string ConnectionString)
        {
            ConnStr = ConnectionString;
        }
        public string LccDepDate { get; set; }
        public string LccRetDate { get; set; }
        FltCalc objMarkup = new FltCalc();

        public ArrayList IndigoAvilability(FlightSearch searchInputs, CredentialList objIndigoCrd, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, float srvCharge, string crdType, string SearchId)
        {

            LCC.clsIndigo obj6E = new clsIndigo(objIndigoCrd.UserID, objIndigoCrd.APISource, objIndigoCrd.Password, objIndigoCrd.CarrierAcc, "Indigo", objIndigoCrd.AvailabilityURL, "");
            ArrayList objIndigoFltResultList = new ArrayList();
            if (searchInputs.Trip.ToString() == "D")
            {
                System.Data.DataTable IndigoAvailDT = new System.Data.DataTable();
                IndigoAvailDT = obj6E.getAvailabilty(searchInputs.Trip.ToString(), searchInputs.TripType.ToString(), Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3), LccDepDate, LccRetDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, objIndigoCrd.INFBasic, objIndigoCrd.INFTax, "", "", "", 1, "OutBound", "");
                objIndigoFltResultList.Add(IndigoConvertToList(searchInputs, IndigoAvailDT, SrvChargeList, CityList, Airlist, MarkupDs, srvCharge, crdType, SearchId));
                if (searchInputs.TripType.ToString() == "RoundTrip")
                {
                    System.Data.DataTable IndigoAvailDT_R = new System.Data.DataTable();
                    IndigoAvailDT_R = obj6E.getAvailabilty(searchInputs.Trip.ToString(), searchInputs.TripType.ToString(), Utility.Left(searchInputs.HidTxtArrCity, 3), Utility.Left(searchInputs.HidTxtDepCity, 3), LccRetDate, LccDepDate, searchInputs.Adult, searchInputs.Child, searchInputs.Infant, objIndigoCrd.INFBasic, objIndigoCrd.INFTax, "", "", "", 1, "InBound", "");
                    objIndigoFltResultList.Add(IndigoConvertToList(searchInputs, IndigoAvailDT_R, SrvChargeList, CityList, Airlist, MarkupDs, srvCharge, crdType, SearchId));
                }
            }
            else
            { }
            return objIndigoFltResultList;
        }
        public List<FlightSearchResults> IndigoConvertToList(FlightSearch searchInputs, DataTable IndigoAvailDT, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, float srvCharge, string CrdType, string SearchId)
        {
            DataTable IndigoDT = new DataTable();
            List<FlightSearchResults> IndigoList = new List<FlightSearchResults>();
            try
            {
                #region "IndigoList"
                if (IndigoAvailDT.Rows.Count > 0)
                {
                    DataTable CommDt = new DataTable();
                    Hashtable STTFTDS = new Hashtable();
                    FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
                    IndigoDT = IndigoAvailDT.DefaultView.ToTable(true, "LineItemNumber");
                    for (int i = 0; i <= IndigoDT.Rows.Count - 1; i++)
                    {
                        DataRow[] IndigoArray;
                        IndigoArray = IndigoAvailDT.Select("LineItemNumber='" + IndigoDT.Rows[i]["LineItemNumber"] + "'", "");
                        for (int j = 0; j <= IndigoArray.Length - 1; j++)
                        {
                            //ResListItem objlist = new ResListItem();
                            FlightSearchResults objlist = new FlightSearchResults();
                            objlist.Adult = Convert.ToInt16(IndigoArray[j]["Adult"]);
                            objlist.Child = Convert.ToInt16(IndigoArray[j]["Child"]);
                            objlist.Infant = Convert.ToInt16(IndigoArray[j]["Infant"]);

                            #region FlightDetails

                            objlist.TotDur = IndigoArray[j]["Tot_Dur"].ToString();
                            objlist.DepartureDate = IndigoArray[j]["DepartureDate"].ToString();
                            objlist.Departure_Date = IndigoArray[j]["Departure_Date"].ToString();
                            objlist.DepartureTime = IndigoArray[j]["DepartureTime"].ToString();// Utility.Left(IndigoArray[j]["DepartureTime"].ToString().Replace(":", ""), 4);
                            objlist.ArrivalDate = IndigoArray[j]["ArrivalDate"].ToString();
                            objlist.Arrival_Date = IndigoArray[j]["Arrival_Date"].ToString();
                            objlist.ArrivalTime = IndigoArray[j]["ArrivalTime"].ToString(); //Utility.Left(IndigoArray[j]["ArrivalTime"].ToString().Replace(":", ""), 4);
                            objlist.OrgDestFrom = IndigoArray[j]["OrgDestFrom"].ToString();
                            objlist.OrgDestTo = IndigoArray[j]["OrgDestTo"].ToString();
                            objlist.Sector = IndigoArray[j]["Sector"].ToString();
                            objlist.DepartureLocation = IndigoArray[j]["DepartureLocation"].ToString();
                            objlist.DepartureCityName = ((from ct in CityList where ct.AirportCode == objlist.DepartureLocation select ct).ToList())[0].City;// ResultInfoDS.Tables["Airport"].Select("airportid='" + objListItem.DepCityCode + "'", "")[0]["city"].ToString();
                            objlist.DepartureAirportName = ((from ct in CityList where ct.AirportCode == objlist.DepartureLocation select ct).ToList())[0].AirportName; //ResultInfoDS.Tables["Airport"].Select("airportid='" + objListItem.DepCityCode + "'", "")[0]["name"].ToString();
                            objlist.DepartureTerminal = "";
                            objlist.ArrivalLocation = IndigoArray[j]["ArrivalLocation"].ToString();
                            objlist.ArrivalCityName = ((from ct in CityList where ct.AirportCode == objlist.ArrivalLocation select ct).ToList())[0].City; //ResultInfoDS.Tables["Airport"].Select("airportid='" + objListItem.ArrCityCode + "'", "")[0]["city"].ToString();
                            objlist.ArrivalAirportName = ((from ct in CityList where ct.AirportCode == objlist.ArrivalLocation select ct).ToList())[0].AirportName; // ResultInfoDS.Tables["Airport"].Select("airportid='" + objListItem.ArrCityCode + "'", "")[0]["name"].ToString();
                            objlist.ArrivalTerminal = "";

                            objlist.MarketingCarrier = IndigoArray[j]["MarketingCarrier"].ToString();
                            objlist.ValiDatingCarrier = IndigoArray[j]["ValiDatingCarrier"].ToString();
                            objlist.AirLineName = ((from al in Airlist where al.AirlineCode == objlist.MarketingCarrier select al).ToList())[0].AlilineName; //ResultInfoDS.Tables["Airline"].Select("AL_Code='" + objListItem.MarketingCarrier + "'", "")[0]["AL_Name"].ToString();
                            objlist.FlightIdentification = IndigoArray[j]["FlightIdentification"].ToString();
                            objlist.EQ = IndigoArray[j]["EQ"].ToString();

                            //objListItem.OperatingCarrier = Flt_Info[iii].ItemArray[12].ToString();
                            //objListItem.ElectronicTicketing = Flt_Info[iii].ItemArray[15].ToString();
                            //objListItem.ProductDetailQualifier = Flt_Info[iii].ItemArray[16].ToString();
                            //objListItem.FareDet = FareList;
                            //objListItem.AdtFar = PtcAdt;
                            //objListItem.ChdFar = PtcChd;
                            //objListItem.InfFar = PtcInf;

                            #endregion

                            #region Fare Details

                            ////SMS charge calc
                            //float srvCharge = 0;
                            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), objlist.ValiDatingCarrier, searchInputs.UID);
                            ////SMS charge calc end

                            #region ADT
                            objlist.RBD = IndigoArray[j]["RBD"].ToString();
                            objlist.AvailableSeats = IndigoArray[j]["AvailableSeats"].ToString();
                            objlist.AvailableSeats1 = IndigoArray[j]["AvailableSeats"].ToString();
                            objlist.fareBasis = IndigoArray[j]["fareBasis"].ToString();
                            objlist.FBPaxType = IndigoArray[j]["FBPaxType"].ToString();
                            objlist.AdtFareType = "Refundable";
                            objlist.AdtFare = float.Parse(IndigoArray[j]["AdtFare"].ToString());
                            objlist.AdtTax = float.Parse(IndigoArray[j]["AdtTax"].ToString());
                            objlist.AdtBfare = float.Parse(IndigoArray[j]["AdtBfare"].ToString());
                            objlist.AdtFSur = float.Parse(IndigoArray[j]["AdtFSur"].ToString());
                            objlist.AdtWO = 0;
                            objlist.AdtIN = 0;
                            objlist.AdtJN = 0;
                            objlist.AdtYR = 0;
                            objlist.AdtOT = objlist.AdtTax - objlist.AdtFSur;
                            //SMS charge add 
                            objlist.AdtOT = objlist.AdtOT + srvCharge;
                            objlist.AdtTax = objlist.AdtTax + srvCharge;
                            objlist.AdtFare = objlist.AdtFare + srvCharge;
                            //SMS charge add end
                            //Hashtable Markup = new Hashtable();
                            //Markup = objMarkup.Fligt_Markup_Calc(MarkupDs, objlist.AdtBfare, objlist.AdtTax, objlist.AdtFare, objlist.ValiDatingCarrier, searchInputs.UID, searchInputs.DISTRID, searchInputs.UserType, searchInputs.OwnerId);
                            CommDt.Clear();
                            STTFTDS.Clear();
                            objlist.ADTAdminMrk = 0;// CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objlist.ValiDatingCarrier, objlist.AdtFare, searchInputs.Trip.ToString()); // float.Parse(Markup["Admin_Markup"].ToString());
                            objlist.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objlist.ValiDatingCarrier, objlist.AdtFare, searchInputs.Trip.ToString());  //float.Parse(Markup["Dist_Markup"].ToString());
                            objlist.ADTDistMrk = 0; //float.Parse(Markup["Agnt_Markup"].ToString());                                                       
                            CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objlist.ValiDatingCarrier, decimal.Parse(objlist.AdtBfare.ToString()), decimal.Parse(objlist.AdtFSur.ToString()), 1, "", objlist.AdtCabin.ToString(), searchInputs.DepDate, objlist.OrgDestFrom + "-" + objlist.OrgDestTo, searchInputs.RetDate, objlist.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), objlist.FlightIdentification, objlist.OperatingCarrier, objlist.MarketingCarrier, CrdType, "");
                            objlist.AdtDiscount = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                            objlist.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                            STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objlist.ValiDatingCarrier, objlist.AdtDiscount, objlist.AdtBfare, objlist.AdtFSur, searchInputs.TDS);
                            objlist.AdtSrvTax = float.Parse(STTFTDS["STax"].ToString());
                            objlist.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                            objlist.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                            objlist.AdtEduCess = 0;
                            objlist.AdtHighEduCess = 0;
                            if (searchInputs.IsCorp == true)
                            {
                                try
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objlist.ValiDatingCarrier, decimal.Parse(objlist.AdtBfare.ToString()), decimal.Parse(objlist.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objlist.AdtFare.ToString()));
                                    objlist.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    objlist.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                                catch { }
                            }

                            objlist.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());
                            #endregion

                            #region CHD
                            if (objlist.Child > 0)
                            {
                                objlist.ChdFare = float.Parse(IndigoArray[j]["ChdFare"].ToString());
                                objlist.ChdTax = float.Parse(IndigoArray[j]["ChdTax"].ToString());
                                objlist.ChdFSur = float.Parse(IndigoArray[j]["ChdFSur"].ToString());
                                objlist.ChdBFare = float.Parse(IndigoArray[j]["ChdBfare"].ToString()); // ChdBfare -ResListItem
                                objlist.ChdWO = 0;
                                objlist.ChdIN = 0;
                                objlist.ChdJN = 0;
                                objlist.ChdYR = 0;
                                objlist.ChdOT = objlist.ChdTax - objlist.ChdFSur;
                                //SMS charge add 
                                objlist.ChdOT = objlist.ChdOT + srvCharge;
                                objlist.ChdTax = objlist.ChdTax + srvCharge;
                                objlist.ChdFare = objlist.ChdFare + srvCharge;
                                //SMS charge add end
                                CommDt.Clear();
                                STTFTDS.Clear();
                                objlist.CHDAdminMrk = 0;// CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objlist.ValiDatingCarrier, objlist.ChdFare, searchInputs.Trip.ToString());
                                objlist.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objlist.ValiDatingCarrier, objlist.ChdFare, searchInputs.Trip.ToString());
                                objlist.CHDDistMrk = 0;
                                CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objlist.ValiDatingCarrier, decimal.Parse(objlist.ChdBFare.ToString()), decimal.Parse(objlist.ChdFSur.ToString()), 1, "", objlist.ChdCabin, searchInputs.DepDate, objlist.OrgDestFrom + "-" + objlist.OrgDestTo, searchInputs.RetDate, objlist.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objlist.FlightIdentification, objlist.OperatingCarrier, objlist.MarketingCarrier, "NRM", "");
                                objlist.ChdDiscount = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                objlist.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objlist.ValiDatingCarrier, objlist.ChdDiscount, objlist.ChdBFare, objlist.ChdFSur, searchInputs.TDS);
                                objlist.ChdSrvTax = float.Parse(STTFTDS["STax"].ToString());
                                objlist.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                objlist.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                objlist.ChdEduCess = 0;
                                objlist.ChdHighEduCess = 0;
                                if (searchInputs.IsCorp == true)
                                {
                                    try
                                    {
                                        DataTable MGDT = new DataTable();
                                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objlist.ValiDatingCarrier, decimal.Parse(objlist.ChdBFare.ToString()), decimal.Parse(objlist.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objlist.ChdFare.ToString()));
                                        objlist.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                        objlist.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                    }
                                    catch { }
                                }
                            }
                            #endregion

                            #region INF
                            if (objlist.Infant > 0)
                            {
                                objlist.InfFare = float.Parse(IndigoArray[j]["InfFare"].ToString());
                                objlist.InfFSur = float.Parse(IndigoArray[j]["InfFSur"].ToString());
                                objlist.InfTax = float.Parse(IndigoArray[j]["InfTax"].ToString());
                                objlist.InfBfare = float.Parse(IndigoArray[j]["InfBfare"].ToString());
                                objlist.InfWO = 0;
                                objlist.InfIN = 0;
                                objlist.InfJN = 0;
                                objlist.InfYR = 0;
                                objlist.InfOT = objlist.InfTax - objlist.InfFSur;
                                CommDt.Clear();
                                STTFTDS.Clear();
                                objlist.InfAdminMrk = 0;// CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objlist.ValiDatingCarrier, objlist.InfFare, searchInputs.Trip.ToString());
                                objlist.InfAgentMrk = 0;// CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objlist.ValiDatingCarrier, objlist.InfFare, searchInputs.Trip.ToString());
                                objlist.InfDistMrk = 0;
                                //CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objlist.ValiDatingCarrier, decimal.Parse(objlist.ChdBFare.ToString()), decimal.Parse(objlist.ChdFSur.ToString()), 1);
                                objlist.InfDiscount = 0;// float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                objlist.InfCB = 0;// float.Parse(CommDt.Rows[0]["CB"].ToString());
                                //STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objlist.ValiDatingCarrier, objlist.ChdDiscount, objlist.ChdBFare, objlist.ChdFSur, searchInputs.TDS);
                                objlist.InfSrvTax = 0;// float.Parse(STTFTDS["STax"].ToString());
                                objlist.InfTF = 0;// float.Parse(STTFTDS["TFee"].ToString());
                                objlist.InfTds = 0;// float.Parse(STTFTDS["Tds"].ToString());
                                objlist.InfEduCess = 0;
                                objlist.InfHighEduCess = 0;
                                if (searchInputs.IsCorp == true)
                                {
                                    try
                                    {
                                        DataTable MGDT = new DataTable();
                                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objlist.ValiDatingCarrier, decimal.Parse(objlist.InfBfare.ToString()), decimal.Parse(objlist.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objlist.InfFare.ToString()));
                                        objlist.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                        objlist.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                    }
                                    catch { }
                                }
                            }
                            #endregion

                            objlist.FareRule = "";//Tqt_Det;
                            objlist.TotalFuelSur = float.Parse(IndigoArray[j]["TotalFuelSur"].ToString());
                            objlist.TotBfare = float.Parse(IndigoArray[j]["TotalBfare"].ToString()); //TotalBfare -ResListItem
                            objlist.TotalTax = float.Parse(IndigoArray[j]["TotalTax"].ToString());
                            objlist.TotalFare = objlist.TotBfare + objlist.TotalTax;
                            objlist.STax = (objlist.AdtSrvTax * objlist.Adult) + (objlist.ChdSrvTax * objlist.Child) + (objlist.InfSrvTax * objlist.Infant);
                            objlist.TFee = (objlist.AdtTF * objlist.Adult) + (objlist.ChdTF * objlist.Child);// +(objlist.InfTF * objlist.Infant);
                            objlist.TotDis = (objlist.AdtDiscount * objlist.Adult) + (objlist.ChdDiscount * objlist.Child);
                            objlist.TotCB = (objlist.AdtCB * objlist.Adult) + (objlist.ChdCB * objlist.Child);
                            objlist.TotTds = (objlist.AdtTds * objlist.Adult) + (objlist.ChdTds * objlist.Child);// +objFS.InfTds;
                            objlist.TotMrkUp = (objlist.ADTAdminMrk * objlist.Adult) + (objlist.ADTAgentMrk * objlist.Adult) + (objlist.CHDAdminMrk * objlist.Child) + (objlist.CHDAgentMrk * objlist.Child);
                            objlist.TotMgtFee = (objlist.AdtMgtFee * objlist.Adult) + (objlist.ChdMgtFee * objlist.Child) + (objlist.InfMgtFee * objlist.Infant);
                            objlist.TotalFare = objlist.TotalFare + objlist.TotMrkUp + objlist.STax + objlist.TFee + objlist.TotMgtFee + (srvCharge * objlist.Adult) + (srvCharge * objlist.Child);
                            objlist.NetFare = (objlist.TotalFare + objlist.TotTds) - (objlist.TotDis + objlist.TotCB + (objlist.ADTAgentMrk * objlist.Adult) + (objlist.CHDAgentMrk * objlist.Child));
                            #endregion


                            #region Other Details
                            objlist.Flight = IndigoArray[j]["Flight"].ToString();
                            objlist.FType = IndigoArray[j]["FType"].ToString();
                            objlist.Leg = int.Parse(IndigoArray[j]["Leg"].ToString());
                            objlist.LineNumber = int.Parse(IndigoArray[j]["LineItemNumber"].ToString());
                            objlist.sno = IndigoArray[j]["sno"].ToString();
                            objlist.Stops = IndigoArray[j]["Stops"].ToString();
                            objlist.Trip = IndigoArray[j]["Trip"].ToString();
                            objlist.TripCnt = IndigoArray[j]["TripCnt"].ToString();
                            objlist.TripType = IndigoArray[j]["TripType"].ToString();
                            objlist.Track_id = IndigoArray[j]["Track_id"].ToString();
                            objlist.Searchvalue = IndigoArray[j]["Searchvalue"].ToString();
                            objlist.OriginalTF = float.Parse(IndigoArray[j]["OriginalTF"].ToString());
                            objlist.OriginalTT = srvCharge; //float.Parse(IndigoArray[j]["OriginalTT"].ToString());
                            //objlist.TotDis = float.Parse(IndigoArray[j]["DisCount"].ToString()); //DisCount
                            objlist.depdatelcc = IndigoArray[j]["depdatelcc"].ToString();
                            objlist.arrdatelcc = IndigoArray[j]["arrdatelcc"].ToString();
                            objlist.IsCorp = searchInputs.IsCorp;
                            objlist.Provider = "LCC";
                            objlist.SearchId = SearchId;
                            #endregion

                            IndigoList.Add(objlist);

                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightSearch");
            }
            return IndigoList;
        }

        public ArrayList GoairAvilability(FlightSearch searchInputs, CredentialList objGoairCrd, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, float srvCharge, string CrdType, string SearchId)
        {
            ArrayList objGoairFltResultList = new ArrayList();
            LCC.clsGoAir objGoair = new LCC.clsGoAir("", "Goair");

            string gCabin = "";
            if (searchInputs.Cabin == "Y")
            {
                gCabin = "ECONOMY";
            }
            else if (searchInputs.Cabin == "C")
            {
                gCabin = "BUSINESS";
            }
            else if (searchInputs.Cabin == "F")
            {
                gCabin = "BUSINESS";
            }
            if (searchInputs.Trip.ToString() == "D" && searchInputs.RTF == false)
            {
                System.Data.DataTable GoairAvailDT = new System.Data.DataTable();

                string guid = objGoair.GetSecurityGUID(objGoairCrd.LoginID, objGoairCrd.LoginPWD);
                if (objGoair.ValidateGUID(guid))
                {
                    GoairAvailDT = objGoair.GetAvailability(guid, objGoairCrd.CorporateID, objGoairCrd.UserID, objGoairCrd.Password, searchInputs.Trip.ToString(), searchInputs.TripType.ToString(), Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3), LccDepDate, "1", searchInputs.Adult, searchInputs.Child, searchInputs.Infant, gCabin, objGoairCrd.ServerIP, searchInputs.UID, searchInputs.DISTRID, "", 1, "OutBound", "");
                    objGoairFltResultList.Add(GoairConvertToList(searchInputs, GoairAvailDT, SrvChargeList, CityList, Airlist, MarkupDs, srvCharge, CrdType, SearchId));
                }
                if (searchInputs.TripType.ToString() == "RoundTrip")
                {
                    string guidR = objGoair.GetSecurityGUID(objGoairCrd.LoginID, objGoairCrd.LoginPWD);
                    System.Data.DataTable GoairAvailDT_R = new System.Data.DataTable();
                    if (objGoair.ValidateGUID(guidR))
                    {
                        GoairAvailDT_R = objGoair.GetAvailability(guidR, objGoairCrd.CorporateID, objGoairCrd.UserID, objGoairCrd.Password, searchInputs.Trip.ToString(), searchInputs.TripType.ToString(), Utility.Left(searchInputs.HidTxtArrCity, 3), Utility.Left(searchInputs.HidTxtDepCity, 3), LccRetDate, "1", searchInputs.Adult, searchInputs.Child, searchInputs.Infant, gCabin, objGoairCrd.ServerIP, searchInputs.UID, searchInputs.DISTRID, "", 1, "InBound", "");
                        objGoairFltResultList.Add(GoairConvertToList(searchInputs, GoairAvailDT_R, SrvChargeList, CityList, Airlist, MarkupDs, srvCharge, CrdType, SearchId));
                    }
                }
            }
            else if (searchInputs.Trip.ToString() == "D" && searchInputs.RTF == true)
            {

                GoAirSearchFlightsBAL GoairF = new GoAirSearchFlightsBAL(objGoairCrd.ServerIP, objGoairCrd.LoginID, objGoairCrd.LoginPWD, objGoairCrd.CorporateID, objGoairCrd.UserID, objGoairCrd.Password);
                objGoairFltResultList.Add(GetGoAirResultListWithMarkup((List<FlightSearchResults>)GoairF.GetFlightAvailability(searchInputs, CityList)[0], searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, srvCharge, CrdType, SearchId));

            }
            else
            { }
            return objGoairFltResultList;
        }
        private List<FlightSearchResults> GoairConvertToList(FlightSearch searchInputs, DataTable GoairAvailDT, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, float srvCharge, string CrdType, string SearchId)
        {
            DataTable GoairDT = new DataTable();
            List<FlightSearchResults> GoairList = new List<FlightSearchResults>();
            try
            {
                #region "GoairList"
                if (GoairAvailDT.Rows.Count > 0)
                {
                    DataTable CommDt = new DataTable();
                    Hashtable STTFTDS = new Hashtable();
                    FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
                    List<BaggageList> BagInfoList = new List<BaggageList>();
                    BagInfoList = objFltComm.GetBaggage(searchInputs.Trip.ToString(), "G8",false);
                    GoairDT = GoairAvailDT.DefaultView.ToTable(true, "LineItemNumber");
                    for (int i = 0; i <= GoairDT.Rows.Count - 1; i++)
                    {
                        DataRow[] GoairArray;
                        GoairArray = GoairAvailDT.Select("LineItemNumber='" + GoairDT.Rows[i]["LineItemNumber"] + "'", "");
                        for (int j = 0; j <= GoairArray.Length - 1; j++)
                        {
                            //ResListItem objlist = new ResListItem();
                            FlightSearchResults objlist = new FlightSearchResults();
                            //Hashtable Markup = new Hashtable();
                            objlist.Adult = Convert.ToInt16(GoairArray[j]["Adult"]);
                            objlist.Child = Convert.ToInt16(GoairArray[j]["Child"]);
                            objlist.Infant = Convert.ToInt16(GoairArray[j]["Infant"]);

                            #region FlightDetails

                            objlist.TotDur = GetTimeInHrsAndMin(Convert.ToInt16(GoairArray[j]["Tot_Dur"].ToString().Trim()));
                            objlist.DepartureDate = GoairArray[j]["DepartureDate"].ToString();
                            objlist.Departure_Date = GoairArray[j]["Departure_Date"].ToString();
                            objlist.DepartureTime = GoairArray[j]["DepartureTime"].ToString();// Utility.Left(GoairArray[j]["DepartureTime"].ToString().Replace(":", ""), 4);
                            objlist.ArrivalDate = GoairArray[j]["ArrivalDate"].ToString();
                            objlist.Arrival_Date = GoairArray[j]["Arrival_Date"].ToString();
                            objlist.ArrivalTime = GoairArray[j]["ArrivalTime"].ToString();// Utility.Left(GoairArray[j]["ArrivalTime"].ToString().Replace(":", ""), 4);
                            objlist.OrgDestFrom = GoairArray[j]["OrgDestFrom"].ToString();
                            objlist.OrgDestTo = GoairArray[j]["OrgDestTo"].ToString();
                            objlist.Sector = GoairArray[j]["Sector"].ToString();
                            objlist.DepartureLocation = GoairArray[j]["DepartureLocation"].ToString();
                            objlist.DepartureCityName = ((from ct in CityList where ct.AirportCode == objlist.DepartureLocation select ct).ToList())[0].City;// ResultInfoDS.Tables["Airport"].Select("airportid='" + objListItem.DepCityCode + "'", "")[0]["city"].ToString();
                            objlist.DepartureAirportName = ((from ct in CityList where ct.AirportCode == objlist.DepartureLocation select ct).ToList())[0].AirportName; //ResultInfoDS.Tables["Airport"].Select("airportid='" + objListItem.DepCityCode + "'", "")[0]["name"].ToString();
                            objlist.DepartureTerminal = "";
                            objlist.ArrivalLocation = GoairArray[j]["ArrivalLocation"].ToString();
                            objlist.ArrivalCityName = ((from ct in CityList where ct.AirportCode == objlist.ArrivalLocation select ct).ToList())[0].City; //ResultInfoDS.Tables["Airport"].Select("airportid='" + objListItem.ArrCityCode + "'", "")[0]["city"].ToString();
                            objlist.ArrivalAirportName = ((from ct in CityList where ct.AirportCode == objlist.ArrivalLocation select ct).ToList())[0].AirportName; // ResultInfoDS.Tables["Airport"].Select("airportid='" + objListItem.ArrCityCode + "'", "")[0]["name"].ToString();
                            objlist.ArrivalTerminal = "";

                            objlist.MarketingCarrier = GoairArray[j]["MarketingCarrier"].ToString();
                            objlist.ValiDatingCarrier = GoairArray[j]["ValiDatingCarrier"].ToString();
                            objlist.AirLineName = ((from al in Airlist where al.AirlineCode == objlist.MarketingCarrier select al).ToList())[0].AlilineName; //ResultInfoDS.Tables["Airline"].Select("AL_Code='" + objListItem.MarketingCarrier + "'", "")[0]["AL_Name"].ToString();
                            objlist.FlightIdentification = GoairArray[j]["FlightIdentification"].ToString();
                            objlist.EQ = GoairArray[j]["EQ"].ToString();
                            if (GoairArray[j]["sno"].ToString().ToUpper().Contains("GOBUSINESS"))
                                objlist.BagInfo = ((from bg in BagInfoList where bg.Class == "Business" select bg).ToList())[0].Weight;
                            else
                                objlist.BagInfo = ((from bg in BagInfoList where bg.Class == "Economy" select bg).ToList())[0].Weight;
                            //objListItem.OperatingCarrier = Flt_Info[iii].ItemArray[12].ToString();
                            //objListItem.ElectronicTicketing = Flt_Info[iii].ItemArray[15].ToString();
                            //objListItem.ProductDetailQualifier = Flt_Info[iii].ItemArray[16].ToString();
                            //objListItem.FareDet = FareList;
                            //objListItem.AdtFar = PtcAdt;
                            //objListItem.ChdFar = PtcChd;
                            //objListItem.InfFar = PtcInf;

                            #endregion

                            #region Fare Details

                            ////SMS charge calc
                            //float srvCharge = 0;
                            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), objlist.ValiDatingCarrier, searchInputs.UID);
                            ////SMS charge calc end


                            #region ADT
                            objlist.RBD = GoairArray[j]["RBD"].ToString();
                            objlist.AvailableSeats = GoairArray[j]["AvailableSeats"].ToString();
                            objlist.AvailableSeats1 = GoairArray[j]["AvailableSeats"].ToString();
                            objlist.fareBasis = GoairArray[j]["fareBasis"].ToString();
                            objlist.FBPaxType = GoairArray[j]["FBPaxType"].ToString();
                            objlist.AdtFareType = "Refundable";
                            if (GoairArray[j]["sno"].ToString().ToUpper().Contains("GOBUSINESS"))
                                objlist.AdtCabin = "B";
                            else
                                objlist.AdtCabin = "E";
                            objlist.AdtFare = float.Parse(GoairArray[j]["AdtFare"].ToString());
                            objlist.AdtTax = float.Parse(GoairArray[j]["AdtTax"].ToString());
                            objlist.AdtBfare = float.Parse(GoairArray[j]["AdtBfare"].ToString());
                            objlist.AdtFSur = float.Parse(GoairArray[j]["AdtFSur"].ToString());
                            objlist.AdtWO = 0;
                            objlist.AdtIN = 0;
                            objlist.AdtJN = 0;
                            objlist.AdtYR = 0;
                            objlist.AdtOT = objlist.AdtTax - objlist.AdtFSur;
                            //SMS charge add 
                            objlist.AdtOT = objlist.AdtOT + srvCharge;
                            objlist.AdtTax = objlist.AdtTax + srvCharge;
                            objlist.AdtFare = objlist.AdtFare + srvCharge;
                            //SMS charge add end
                            //Hashtable Markup = new Hashtable();
                            //Markup = objMarkup.Fligt_Markup_Calc(MarkupDs, objlist.AdtBfare, objlist.AdtTax, objlist.AdtFare, objlist.ValiDatingCarrier, searchInputs.UID, searchInputs.DISTRID, searchInputs.UserType, searchInputs.OwnerId);
                            CommDt.Clear();
                            STTFTDS.Clear();
                            objlist.ADTAdminMrk = 0;// CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objlist.ValiDatingCarrier, objlist.AdtFare, searchInputs.Trip.ToString()); // float.Parse(Markup["Admin_Markup"].ToString());
                            objlist.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objlist.ValiDatingCarrier, objlist.AdtFare, searchInputs.Trip.ToString());  //float.Parse(Markup["Dist_Markup"].ToString());
                            objlist.ADTDistMrk = 0; //float.Parse(Markup["Agnt_Markup"].ToString());                                                       
                            CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objlist.ValiDatingCarrier, decimal.Parse(objlist.AdtBfare.ToString()), decimal.Parse(objlist.AdtFSur.ToString()), 1, "", objlist.AdtCabin, searchInputs.DepDate, objlist.OrgDestFrom + "-" + objlist.OrgDestTo, searchInputs.RetDate, objlist.AdtFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), objlist.FlightIdentification, objlist.OperatingCarrier, objlist.MarketingCarrier, CrdType, "");
                            objlist.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                            objlist.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                            STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objlist.ValiDatingCarrier, objlist.AdtDiscount1, objlist.AdtBfare, objlist.AdtFSur, searchInputs.TDS);
                            objlist.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                            objlist.AdtDiscount = objlist.AdtDiscount1 - objlist.AdtSrvTax1;
                            objlist.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                            objlist.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                            objlist.AdtEduCess = 0;
                            objlist.AdtHighEduCess = 0;
                            objlist.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());
                            if (searchInputs.IsCorp == true)
                            {
                                try
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objlist.ValiDatingCarrier, decimal.Parse(objlist.AdtBfare.ToString()), decimal.Parse(objlist.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objlist.AdtFare.ToString()));
                                    objlist.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    objlist.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                                catch { }
                            }
                            #endregion

                            #region CHD
                            if (objlist.Child > 0)
                            {
                                objlist.ChdFare = float.Parse(GoairArray[j]["ChdFare"].ToString());
                                objlist.ChdTax = float.Parse(GoairArray[j]["ChdTax"].ToString());
                                objlist.ChdFSur = float.Parse(GoairArray[j]["ChdFSur"].ToString());
                                objlist.ChdBFare = float.Parse(GoairArray[j]["ChdBfare"].ToString()); //ChdBfare
                                objlist.ChdWO = 0;
                                objlist.ChdIN = 0;
                                objlist.ChdJN = 0;
                                objlist.ChdYR = 0;
                                objlist.ChdOT = objlist.ChdTax - objlist.ChdFSur;
                                //SMS charge add 
                                objlist.ChdOT = objlist.ChdOT + srvCharge;
                                objlist.ChdTax = objlist.ChdTax + srvCharge;
                                objlist.ChdFare = objlist.ChdFare + srvCharge;
                                //SMS charge add end
                                CommDt.Clear();
                                STTFTDS.Clear();
                                objlist.CHDAdminMrk = 0;// CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objlist.ValiDatingCarrier, objlist.ChdFare, searchInputs.Trip.ToString()); 
                                objlist.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objlist.ValiDatingCarrier, objlist.ChdFare, searchInputs.Trip.ToString());
                                objlist.CHDDistMrk = 0;
                                CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objlist.ValiDatingCarrier, decimal.Parse(objlist.ChdBFare.ToString()), decimal.Parse(objlist.ChdFSur.ToString()), 1, "", objlist.ChdCabin, searchInputs.DepDate, objlist.OrgDestFrom + "-" + objlist.OrgDestTo, searchInputs.RetDate, objlist.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), objlist.FlightIdentification, objlist.OperatingCarrier, objlist.MarketingCarrier, CrdType, "");
                                objlist.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                objlist.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objlist.ValiDatingCarrier, objlist.ChdDiscount1, objlist.ChdBFare, objlist.ChdFSur, searchInputs.TDS);
                                objlist.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                objlist.ChdDiscount = objlist.ChdDiscount1 - objlist.ChdSrvTax1;
                                objlist.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                objlist.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                objlist.ChdEduCess = 0;
                                objlist.ChdHighEduCess = 0;
                                if (searchInputs.IsCorp == true)
                                {
                                    try
                                    {
                                        DataTable MGDT = new DataTable();
                                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objlist.ValiDatingCarrier, decimal.Parse(objlist.ChdBFare.ToString()), decimal.Parse(objlist.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objlist.ChdFare.ToString()));
                                        objlist.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                        objlist.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                    }
                                    catch { }
                                }
                            }
                            #endregion

                            #region INF
                            if (objlist.Infant > 0)
                            {
                                objlist.InfFare = float.Parse(GoairArray[j]["InfFare"].ToString());
                                objlist.InfFSur = float.Parse(GoairArray[j]["InfFSur"].ToString());
                                objlist.InfTax = float.Parse(GoairArray[j]["InfTax"].ToString());
                                objlist.InfBfare = float.Parse(GoairArray[j]["InfBfare"].ToString());
                                objlist.InfWO = 0;
                                objlist.InfIN = 0;
                                objlist.InfJN = 0;
                                objlist.InfYR = 0;
                                objlist.InfOT = objlist.InfTax - objlist.InfFSur;
                                CommDt.Clear();
                                STTFTDS.Clear();
                                objlist.InfAdminMrk = 0;// CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objlist.ValiDatingCarrier, objlist.InfFare, searchInputs.Trip.ToString());
                                objlist.InfAgentMrk = 0;// CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objlist.ValiDatingCarrier, objlist.InfFare, searchInputs.Trip.ToString());
                                objlist.InfDistMrk = 0;
                                //CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objlist.ValiDatingCarrier, decimal.Parse(objlist.ChdBFare.ToString()), decimal.Parse(objlist.ChdFSur.ToString()), 1);
                                objlist.InfDiscount = 0;// float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                objlist.InfCB = 0;// float.Parse(CommDt.Rows[0]["CB"].ToString());
                                //STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objlist.ValiDatingCarrier, objlist.ChdDiscount, objlist.ChdBFare, objlist.ChdFSur, searchInputs.TDS);
                                objlist.InfSrvTax = 0;// float.Parse(STTFTDS["STax"].ToString());
                                objlist.InfTF = 0;// float.Parse(STTFTDS["TFee"].ToString());
                                objlist.InfTds = 0;// float.Parse(STTFTDS["Tds"].ToString());
                                objlist.InfEduCess = 0;
                                objlist.InfHighEduCess = 0;
                                if (searchInputs.IsCorp == true)
                                {
                                    try
                                    {
                                        DataTable MGDT = new DataTable();
                                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objlist.ValiDatingCarrier, decimal.Parse(objlist.InfBfare.ToString()), decimal.Parse(objlist.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objlist.InfFare.ToString()));
                                        objlist.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                        objlist.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                    }
                                    catch { }
                                }
                            }
                            #endregion

                            objlist.FareRule = "";//Tqt_Det;
                            objlist.TotalFuelSur = float.Parse(GoairArray[j]["TotalFuelSur"].ToString());
                            objlist.TotBfare = float.Parse(GoairArray[j]["TotalBfare"].ToString()); //TotalBfare
                            objlist.TotalTax = float.Parse(GoairArray[j]["TotalTax"].ToString());
                            objlist.TotalFare = objlist.TotBfare + objlist.TotalTax;
                            objlist.STax = (objlist.AdtSrvTax * objlist.Adult) + (objlist.ChdSrvTax * objlist.Child) + (objlist.InfSrvTax * objlist.Infant);
                            objlist.TFee = (objlist.AdtTF * objlist.Adult) + (objlist.ChdTF * objlist.Child);// +(objlist.InfTF * objlist.Infant);
                            objlist.TotDis = (objlist.AdtDiscount * objlist.Adult) + (objlist.ChdDiscount * objlist.Child);
                            objlist.TotCB = (objlist.AdtCB * objlist.Adult) + (objlist.ChdCB * objlist.Child);
                            objlist.TotTds = (objlist.AdtTds * objlist.Adult) + (objlist.ChdTds * objlist.Child);// +objFS.InfTds;
                            objlist.TotMrkUp = (objlist.ADTAdminMrk * objlist.Adult) + (objlist.ADTAgentMrk * objlist.Adult) + (objlist.CHDAdminMrk * objlist.Child) + (objlist.CHDAgentMrk * objlist.Child);
                            objlist.TotMgtFee = (objlist.AdtMgtFee * objlist.Adult) + (objlist.ChdMgtFee * objlist.Child) + (objlist.InfMgtFee * objlist.Infant);
                            objlist.TotalFare = objlist.TotalFare + objlist.TotMrkUp + objlist.STax + objlist.TFee + objlist.TotMgtFee + (srvCharge * objlist.Adult) + (srvCharge * objlist.Child);
                            objlist.NetFare = (objlist.TotalFare + objlist.TotTds) - (objlist.TotDis + objlist.TotCB + (objlist.ADTAgentMrk * objlist.Adult) + (objlist.CHDAgentMrk * objlist.Child));

                            //try
                            //{
                            //    objlist.TotalFare = (objlist.AdtFare * objlist.Adult + objlist.ChdFare * objlist.Child + objlist.InfFare * objlist.Infant);
                            //    objlist.TotalFare += objlist.TFee + objlist.STax + TotMarkup;
                            //    objlist.IATAComm = 0;//((from sc in SrvchargeList where sc.AirlineCode == objListItem.ValidatingCarrier select sc).ToList())[0].IATACommissiom;
                            //}
                            //catch (Exception ex)
                            //{
                            //    //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightSearch");
                            //}
                            #endregion


                            #region Other Details
                            objlist.Flight = GoairArray[j]["Flight"].ToString();
                            objlist.FType = GoairArray[j]["FType"].ToString();
                            objlist.Leg = int.Parse(GoairArray[j]["Leg"].ToString());
                            objlist.LineNumber = int.Parse(GoairArray[j]["LineItemNumber"].ToString());
                            objlist.sno = GoairArray[j]["sno"].ToString();
                            objlist.Stops = GoairArray[j]["Stops"].ToString();
                            objlist.Trip = GoairArray[j]["Trip"].ToString();
                            objlist.TripCnt = GoairArray[j]["TripCnt"].ToString();
                            objlist.TripType = GoairArray[j]["TripType"].ToString();
                            objlist.Track_id = GoairArray[j]["Track_id"].ToString();
                            objlist.Searchvalue = GoairArray[j]["Searchvalue"].ToString();
                            objlist.OriginalTF = float.Parse(GoairArray[j]["OriginalTF"].ToString());
                            objlist.OriginalTT = srvCharge; //float.Parse(GoairArray[j]["OriginalTT"].ToString());
                            //objlist.TotDis = float.Parse(GoairArray[j]["DisCount"].ToString());  //DisCount
                            objlist.depdatelcc = GoairArray[j]["depdatelcc"].ToString();
                            objlist.arrdatelcc = GoairArray[j]["arrdatelcc"].ToString();
                            objlist.IsCorp = searchInputs.IsCorp;
                            objlist.Provider = "LCC";
                            objlist.SearchId = SearchId;
                            #endregion

                            GoairList.Add(objlist);

                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightSearch");
            }
            return GoairList;
        }

        #region Goair RTF
        public List<FlightSearchResults> GetGoAirResultListWithMarkup(List<FlightSearchResults> inputList, FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, float srvCharge, string CrdType, string SearchId)
        {
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            List<FlightSearchResults> newList = new List<FlightSearchResults>();
            //ArrayList test = (ArrayList)inputList[0];
            ////SMS charge calc
            //float srvCharge = 0;
            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), "G8", searchInputs.UID);
            ////SMS charge calc end
            List<BaggageList> BagInfoList = new List<BaggageList>();
            BagInfoList = objFltComm.GetBaggage(searchInputs.Trip.ToString(), "G8",false);
            for (int i = 0; i < inputList.Count; i++)
            {

                newList.Add(GoAirGetNewObject((FlightSearchResults)inputList[i], searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, objFltComm, srvCharge, BagInfoList, CrdType, SearchId));
            }

            return newList;
        }

        public FlightSearchResults GoAirGetNewObject(FlightSearchResults fsr, FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, FlightCommonBAL objFltComm, float srvCharge, List<BaggageList> BagInfoList, string CrdType, string SearchId)
        {
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            var objNew = (FlightSearchResults)fsr.Clone();

            if (fsr.sno.ToUpper().Contains("GOBUSINESS"))
            {
                objNew.BagInfo = ((from bg in BagInfoList where bg.Class == "Business" select bg).ToList())[0].Weight;
                objNew.AdtCabin = "B";
            }
            else
            {
                objNew.BagInfo = ((from bg in BagInfoList where bg.Class == "Economy" select bg).ToList())[0].Weight;
                objNew.AdtCabin = "E";
            }

            #region Fare Calculation

            #region Adult
            if (objNew.Adult > 0)
            {
                objNew.AdtOT = objNew.AdtTax - objNew.AdtFSur;
                //SMS charge add 
                objNew.AdtOT = objNew.AdtOT + srvCharge;
                objNew.AdtTax = objNew.AdtTax + srvCharge;
                objNew.AdtFare = objNew.AdtFare + srvCharge;
                //SMS charge add end
                objNew.ADTAdminMrk = 0;//CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, AdtFare, searchInputs.Trip.ToString()); 
                objNew.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objNew.ValiDatingCarrier, objNew.AdtFare, searchInputs.Trip.ToString());
                objNew.ADTDistMrk = fsr.ADTDistMrk;
                CommDt.Clear();
                CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.AdtBfare.ToString()), decimal.Parse(objNew.AdtFSur.ToString()), 1, "", objNew.AdtCabin, searchInputs.DepDate, objNew.OrgDestFrom + "-" + objNew.OrgDestTo, searchInputs.RetDate, objNew.AdtFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objNew.FlightIdentification, objNew.OperatingCarrier, objNew.MarketingCarrier, CrdType, "");
                objNew.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                objNew.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                STTFTDS.Clear();
                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objNew.ValiDatingCarrier, objNew.AdtDiscount1, objNew.AdtBfare, objNew.AdtFSur, searchInputs.TDS);
                objNew.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                objNew.AdtDiscount = objNew.AdtDiscount1 - objNew.AdtSrvTax1;
                objNew.AdtEduCess = 0;
                objNew.AdtHighEduCess = 0;
                objNew.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                objNew.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                if (searchInputs.IsCorp == true)
                {
                    try
                    {
                        DataTable MGDT = new DataTable();
                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.AdtBfare.ToString()), decimal.Parse(objNew.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objNew.AdtFare.ToString()));
                        objNew.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                        objNew.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                    }
                    catch { }
                }
            }
            #endregion

            #region Child
            if (objNew.Child > 0)
            {
                objNew.ChdOT = objNew.ChdTax - objNew.ChdFSur;
                //SMS charge add 
                objNew.ChdOT = objNew.ChdOT + srvCharge;
                objNew.ChdTax = objNew.ChdTax + srvCharge;
                objNew.ChdFare = objNew.ChdFare + srvCharge;
                //SMS charge add end
                objNew.CHDAdminMrk = 0;//CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, ChdFare, searchInputs.Trip.ToString());
                objNew.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objNew.ValiDatingCarrier, objNew.ChdFare, searchInputs.Trip.ToString());
                objNew.CHDDistMrk = fsr.CHDDistMrk;
                CommDt.Clear();
                CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.ChdBFare.ToString()), decimal.Parse(objNew.ChdFSur.ToString()), 1, "", objNew.ChdCabin, searchInputs.DepDate, objNew.OrgDestFrom + "-" + objNew.OrgDestTo, searchInputs.RetDate, objNew.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objNew.FlightIdentification, objNew.OperatingCarrier, objNew.MarketingCarrier, CrdType, "");
                objNew.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());//-ChdComm
                objNew.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                STTFTDS.Clear();
                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objNew.ValiDatingCarrier, objNew.ChdDiscount1, objNew.ChdBFare, objNew.ChdFSur, searchInputs.TDS);
                objNew.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE 
                objNew.ChdDiscount = objNew.ChdDiscount1 - objNew.ChdSrvTax1;
                objNew.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                objNew.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                objNew.ChdEduCess = 0;
                objNew.ChdHighEduCess = 0;
                if (searchInputs.IsCorp == true)
                {
                    try
                    {
                        DataTable MGDT = new DataTable();
                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.ChdBFare.ToString()), decimal.Parse(objNew.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objNew.ChdFare.ToString()));
                        objNew.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                        objNew.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                    }
                    catch { }
                }
            }
            #endregion

            #region Infant
            if (objNew.Infant > 0)
            {
                objNew.InfOT = objNew.InfTax - objNew.InfFSur;
                if (searchInputs.IsCorp == true)
                {
                    try
                    {
                        DataTable MGDT = new DataTable();
                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.InfBfare.ToString()), decimal.Parse(objNew.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objNew.InfFare.ToString()));
                        objNew.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                        objNew.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                    }
                    catch { }
                }
            }

            #endregion

            objNew.STax = (objNew.AdtSrvTax * objNew.Adult) + (objNew.ChdSrvTax * objNew.Child) + (objNew.InfSrvTax * objNew.Infant);
            objNew.TFee = (objNew.AdtTF * objNew.Adult) + (objNew.ChdTF * objNew.Child);// +(objlist.InfTF * objlist.Infant);
            objNew.TotDis = (objNew.AdtDiscount * objNew.Adult) + (objNew.ChdDiscount * objNew.Child);
            objNew.TotCB = (objNew.AdtCB * objNew.Adult) + (objNew.ChdCB * objNew.Child);
            objNew.TotTds = (objNew.AdtTds * objNew.Adult) + (objNew.ChdTds * objNew.Child);// +objFS.InfTds;
            objNew.TotMrkUp = (objNew.ADTAdminMrk * objNew.Adult) + (objNew.ADTAgentMrk * objNew.Adult) + (objNew.CHDAdminMrk * objNew.Child) + (objNew.CHDAgentMrk * objNew.Child);
            objNew.TotMgtFee = (objNew.AdtMgtFee * objNew.Adult) + (objNew.ChdMgtFee * objNew.Child) + (objNew.InfMgtFee * objNew.Infant);
            objNew.TotalTax = objNew.TotalTax + (srvCharge * objNew.Adult) + (srvCharge * objNew.Child);
            objNew.TotalFare = objNew.TotalFare + objNew.TotMrkUp + objNew.STax + objNew.TFee + objNew.TotMgtFee + (srvCharge * objNew.Adult) + (srvCharge * objNew.Child);
            objNew.NetFare = (objNew.TotalFare + objNew.TotTds) - (objNew.TotDis + objNew.TotCB + (objNew.ADTAgentMrk * objNew.Adult) + (objNew.CHDAgentMrk * objNew.Child));
            objNew.Provider = "LCC";
            objNew.IsCorp = searchInputs.IsCorp;
            objNew.OriginalTT = srvCharge;
            objNew.SearchId = SearchId;
            #endregion


            return objNew;



        }
        #endregion

        #region Flydubai
        public ArrayList FlydubaiAvilability(FlightSearch searchInputs, CredentialList objFZCrd, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, float srvCharge)
        {
            ArrayList objFZFltResultList = new ArrayList();
            string gCabin = "";
            gCabin = searchInputs.Cabin == "" ? "" : searchInputs.Cabin == "Y" ? "ECONOMY" : "BUSINESS";
            searchInputs.Cabin = gCabin;
            FZSearchBAL objFzSearch = new FZSearchBAL(objFZCrd.LoginID, objFZCrd.LoginPWD, objFZCrd.CorporateID, objFZCrd.UserID, objFZCrd.Password, objFZCrd.ServerIP);

            objFZFltResultList = objFzSearch.GetFlightAvailability(searchInputs, CityList, srvCharge, MarkupDs);


            return objFZFltResultList;
        }

        #endregion


        #region G8CPAPI
        public ArrayList G8Avilability(FlightSearch searchInputs, CredentialList objG8Crd, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, float srvCharge, bool IsNRTP)
        {
            ArrayList objG8FltResultList = new ArrayList();
            string gCabin = "";
            gCabin = searchInputs.Cabin == "" ? "" : searchInputs.Cabin == "Y" ? "ECONOMY" : "BUSINESS";
            searchInputs.Cabin = gCabin;
            G8SearchBAL objG8Search = new G8SearchBAL(ConnStr, objG8Crd.LoginID, objG8Crd.LoginPWD, objG8Crd.CorporateID, objG8Crd.UserID, objG8Crd.Password, objG8Crd.ServerIP);
            if (searchInputs.HidTxtAirLine=="IX")
            {
                objG8FltResultList = objG8Search.GetIXFlightAvailability(searchInputs, CityList, SrvChargeList, srvCharge, MarkupDs, IsNRTP, objG8Crd.CrdType);
            }
            else
            {
                objG8FltResultList = objG8Search.GetFlightAvailability(searchInputs, CityList, SrvChargeList, srvCharge, MarkupDs, IsNRTP, objG8Crd.CrdType);
            }

            return objG8FltResultList;
        }

        public List<FlightSearchResults> GetG8ResultListWithMarkup(List<FlightSearchResults> inputList, FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, float srvCharge, string CrdType, string SearchId)
        {
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            List<FlightSearchResults> newList = new List<FlightSearchResults>();

            for (int i = 0; i < inputList.Count; i++)
            {

                newList.Add(GetG8NewObject((FlightSearchResults)inputList[i], searchInputs, SrvChargeList, CityList, Airlist, MarkupDs, objFltComm, srvCharge, CrdType, SearchId));
            }

            return newList;
        }

        public FlightSearchResults GetG8NewObject(FlightSearchResults fsr, FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, FlightCommonBAL objFltComm, float srvCharge, string CrdType, string SearchId)
        {
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            var objNew = (FlightSearchResults)fsr.Clone();

            objNew.BagInfo = fsr.BagInfo;

            #region Fare Calculation

            #region Adult
            if (objNew.Adult > 0)
            {
                objNew.AdtOT = objNew.AdtTax - objNew.AdtFSur;
                //SMS charge add 
                objNew.AdtOT = objNew.AdtOT + srvCharge;
                objNew.AdtTax = objNew.AdtTax + srvCharge;
                objNew.AdtFare = objNew.AdtFare + srvCharge;
                //SMS charge add end
                objNew.ADTAdminMrk = 0;//CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, AdtFare, searchInputs.Trip.ToString()); 
                objNew.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objNew.ValiDatingCarrier, objNew.AdtFare, searchInputs.Trip.ToString());
                objNew.ADTDistMrk = fsr.ADTDistMrk;
                CommDt.Clear();
                CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.AdtBfare.ToString()), decimal.Parse(objNew.AdtFSur.ToString()), 1, objNew.AdtRbd, objNew.AdtCabin, objNew.DepartureDate, objNew.Sector, searchInputs.RetDate, objNew.AdtFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objNew.FlightIdentification, objNew.OperatingCarrier, objNew.MarketingCarrier, CrdType, "");
                objNew.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                objNew.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                STTFTDS.Clear();
                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objNew.ValiDatingCarrier, objNew.AdtDiscount1, objNew.AdtBfare, objNew.AdtFSur, searchInputs.TDS);
                objNew.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                objNew.AdtDiscount = objNew.AdtDiscount1 - objNew.AdtSrvTax1;
                objNew.AdtEduCess = 0;
                objNew.AdtHighEduCess = 0;
                objNew.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                objNew.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                if (searchInputs.IsCorp == true)
                {
                    try
                    {
                        DataTable MGDT = new DataTable();
                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.AdtBfare.ToString()), decimal.Parse(objNew.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objNew.AdtFare.ToString()));
                        objNew.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                        objNew.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                    }
                    catch { }
                }
            }
            #endregion

            #region Child
            if (objNew.Child > 0)
            {
                objNew.ChdOT = objNew.ChdTax - objNew.ChdFSur;
                //SMS charge add 
                objNew.ChdOT = objNew.ChdOT + srvCharge;
                objNew.ChdTax = objNew.ChdTax + srvCharge;
                objNew.ChdFare = objNew.ChdFare + srvCharge;
                //SMS charge add end
                objNew.CHDAdminMrk = 0;//CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, ChdFare, searchInputs.Trip.ToString());
                objNew.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objNew.ValiDatingCarrier, objNew.ChdFare, searchInputs.Trip.ToString());
                objNew.CHDDistMrk = fsr.CHDDistMrk;
                CommDt.Clear();
                CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.ChdBFare.ToString()), decimal.Parse(objNew.ChdFSur.ToString()), 1, objNew.ChdRbd, objNew.ChdCabin, objNew.DepartureDate, objNew.Sector, searchInputs.RetDate, objNew.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objNew.FlightIdentification, objNew.OperatingCarrier, objNew.MarketingCarrier, CrdType, "");
                objNew.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());//-ChdComm
                objNew.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                STTFTDS.Clear();
                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objNew.ValiDatingCarrier, objNew.ChdDiscount1, objNew.ChdBFare, objNew.ChdFSur, searchInputs.TDS);
                objNew.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE 
                objNew.ChdDiscount = objNew.ChdDiscount1 - objNew.ChdSrvTax1;
                objNew.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                objNew.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                objNew.ChdEduCess = 0;
                objNew.ChdHighEduCess = 0;
                if (searchInputs.IsCorp == true)
                {
                    try
                    {
                        DataTable MGDT = new DataTable();
                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.ChdBFare.ToString()), decimal.Parse(objNew.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objNew.ChdFare.ToString()));
                        objNew.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                        objNew.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                    }
                    catch { }
                }
            }
            #endregion

            #region Infant
            if (objNew.Infant > 0)
            {
                objNew.InfOT = objNew.InfTax - objNew.InfFSur;
                if (searchInputs.IsCorp == true)
                {
                    try
                    {
                        DataTable MGDT = new DataTable();
                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.InfBfare.ToString()), decimal.Parse(objNew.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objNew.InfFare.ToString()));
                        objNew.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                        objNew.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                    }
                    catch { }
                }
            }

            #endregion

            objNew.STax = (objNew.AdtSrvTax * objNew.Adult) + (objNew.ChdSrvTax * objNew.Child) + (objNew.InfSrvTax * objNew.Infant);
            objNew.TFee = (objNew.AdtTF * objNew.Adult) + (objNew.ChdTF * objNew.Child);// +(objlist.InfTF * objlist.Infant);
            objNew.TotDis = (objNew.AdtDiscount * objNew.Adult) + (objNew.ChdDiscount * objNew.Child);
            objNew.TotCB = (objNew.AdtCB * objNew.Adult) + (objNew.ChdCB * objNew.Child);
            objNew.TotTds = (objNew.AdtTds * objNew.Adult) + (objNew.ChdTds * objNew.Child);// +objFS.InfTds;
            objNew.TotMrkUp = (objNew.ADTAdminMrk * objNew.Adult) + (objNew.ADTAgentMrk * objNew.Adult) + (objNew.CHDAdminMrk * objNew.Child) + (objNew.CHDAgentMrk * objNew.Child);
            objNew.TotMgtFee = (objNew.AdtMgtFee * objNew.Adult) + (objNew.ChdMgtFee * objNew.Child) + (objNew.InfMgtFee * objNew.Infant);
            objNew.TotalTax = objNew.TotalTax + (srvCharge * objNew.Adult) + (srvCharge * objNew.Child);
            objNew.TotalFare = objNew.TotalFare + objNew.TotMrkUp + objNew.STax + objNew.TFee + objNew.TotMgtFee + (srvCharge * objNew.Adult) + (srvCharge * objNew.Child);
            objNew.NetFare = (objNew.TotalFare + objNew.TotTds) - (objNew.TotDis + objNew.TotCB + (objNew.ADTAgentMrk * objNew.Adult) + (objNew.CHDAgentMrk * objNew.Child));
            objNew.Provider = "LCC";
            objNew.IsCorp = searchInputs.IsCorp;
            objNew.OriginalTT = srvCharge;
            objNew.SearchId = SearchId;
            #endregion


            return objNew;



        }

        #endregion
        //private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        //{
        //    DataRow[] airMrkArray;
        //    double mrkamt = 0;
        //    try
        //    {
        //        airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");
        //        if (!(airMrkArray != null && airMrkArray.Length > 0))
        //        {
        //            airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

        //        }
        //        if (airMrkArray.Length > 0)
        //        {
        //            if (Trip == "I")
        //            {
        //                if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
        //                {
        //                    mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
        //                }
        //                else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
        //                {
        //                    mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
        //                }
        //            }
        //            else
        //            {
        //                mrkamt = Convert.ToDouble(airMrkArray[0]["MarkUp"].ToString());
        //            }
        //        }
        //        else
        //        {
        //            mrkamt = 0;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        mrkamt = 0;
        //    }
        //    return float.Parse(mrkamt.ToString());
        //}

        public float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray;
            double mrkamt = 0;
            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");

                if (!(airMrkArray != null && airMrkArray.Length > 0))
                {
                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

                }

                if (airMrkArray.Length > 0)
                {

                    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    {
                        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    }
                    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    }

                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return float.Parse(mrkamt.ToString());
        }

        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;            
            decimal originalDis = 0;
            Hashtable STHT = new Hashtable();
            try
            {
                try
                {
                    List<FltSrvChargeList> StNew = (from st in SrvchargeList where st.AirlineCode == VC select st).ToList();
                    if (StNew.Count > 0)
                    {
                        STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                        TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                        IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                    }
                }
                catch
                { }
                //STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                //TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                //IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                originalDis = decimal.Parse(Dis.ToString()) - decimal.Parse(STHT["TFee"].ToString());
                STHT.Add("STax", Math.Round(((originalDis * STaxP) / 100), 0));
                STHT.Add("Tds", Math.Round((((originalDis - decimal.Parse(STHT["STax"].ToString())) * decimal.Parse(TDS)) / 100), 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }
        //public List<FlightSearchResults> Spice_GetFltResult(List<FarePriceJourney> FareBrk, List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet MrkupDS, FlightSearch searchInputs, int schd, decimal InfantBFare, decimal InfTax, string IdType, float srvCharge, string VC, string CrdType, DataRow[] PCRow, string SearchId, List<FareTypeSettings> objFareTypeSettings, bool Bag = false)

        public List<FlightSearchResults> Spice_GetFltResult(List<FarePriceJourney> FareBrk, List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet MrkupDS, FlightSearch searchInputs, int schd, decimal InfantBFare, decimal InfTax, string IdType, List<MISCCharges> MiscList, string VC, string CrdType, DataRow[] PCRow, string SearchId, List<FareTypeSettings> objFareTypeSettings,  bool isRTF, bool Bag = false, bool SMEFare = false)
        {
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            string arrCity = "", depCity = "";
            if (schd == 0)
            {
                depCity = Utility.Left(searchInputs.HidTxtDepCity, 3);
                arrCity = Utility.Left(searchInputs.HidTxtArrCity, 3);
            }
            else
            {
                depCity = Utility.Left(searchInputs.HidTxtArrCity, 3);
                arrCity = Utility.Left(searchInputs.HidTxtDepCity, 3);
            }
            List<FlightSearchResults> FlightList = new List<FlightSearchResults>();

            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            List<BaggageList> BagInfoList = new List<BaggageList>();
            BagInfoList = objFltComm.GetBaggage(searchInputs.Trip.ToString(), VC, Bag);

            string strFlightNo = "";
            string strValidatingCarrier = "";
            string FlttmDur = "";//added by abhilash
            int LNO = 1;

            try
            {
                //if (VC == "SG" && IdType == "SGNRML")
                //    srvCharge = srvCharge - 200;
                //foreach (FarePriceJourney t in FareBrk)
                #region OutBOund -Inbound
                for (int jrny = 0; jrny < FareBrk.Count; jrny++)
                {
                    FarePriceJourney t = new FarePriceJourney();
                    t = FareBrk[jrny];
                    try
                    {
                        #region Get TotalViaSector for Commission PPPSector and PPPSegment
                        int TotalViaSector = 1;
                        try
                        {
                            TotalViaSector = t.Leg.Select(x => x["FlightNumber"]).Distinct().Count();
                        }
                        catch (Exception ex)
                        {
                            TotalViaSector = 1;
                        }
                        #endregion

                        int legs = t.Leg.Length;
                        for (int i = 0; i < legs; i++)
                        {
                            FlightSearchResults objFS = new FlightSearchResults();
                            decimal AdtTF = 0, ChdTF = 0, InfTF = 0;//For Total Fare Per Pax Type
                            #region Flight Details
                            objFS.OrgDestFrom = depCity;
                            objFS.OrgDestTo = arrCity;
                            objFS.DepartureLocation = t.Leg[i]["DepartureStation"];
                            objFS.DepAirportCode = t.Leg[i]["DepartureStation"];
                            objFS.DepartureTerminal = t.Leg[i]["DepartureTerminal"];
                            objFS.DepartureCityName = getCityName(objFS.DepartureLocation, CityList);
                            objFS.ArrivalLocation = t.Leg[i]["ArrivalStation"];
                            objFS.ArrAirportCode = t.Leg[i]["ArrivalStation"];
                            objFS.ArrivalTerminal = t.Leg[i]["ArrivalTerminal"];
                            objFS.ArrivalCityName = getCityName(objFS.ArrivalLocation, CityList);
                            try
                            {
                                objFS.DepartureAirportName = ((from ct in CityList where ct.AirportCode == objFS.DepartureLocation select ct).ToList())[0].AirportName;
                                objFS.ArrivalAirportName = ((from ct in CityList where ct.AirportCode == objFS.ArrivalLocation select ct).ToList())[0].AirportName;
                            }
                            catch (Exception ex)
                            {
                                objFS.DepartureAirportName = t.Leg[i]["DepartureStation"];
                                objFS.ArrivalAirportName = t.Leg[i]["ArrivalStation"];
                            }
                            string[] Dep = Utility.Split(Utility.Split(t.Leg[i]["STD"], "T")[0], "-");
                            objFS.DepartureDate = Dep[2] + Dep[1] + Utility.Right(Dep[0], 2);
                            objFS.Departure_Date = Dep[2] + " " + Utility.datecon(Dep[1]);
                            objFS.DepartureTime = Utility.Left(Utility.Split(t.Leg[i]["STD"], "T")[1], 5);

                            string[] Arr = Utility.Split(Utility.Split(t.Leg[i]["STA"], "T")[0], "-");
                            objFS.ArrivalDate = Arr[2] + Arr[1] + Utility.Right(Arr[0], 2);
                            objFS.Arrival_Date = Arr[2] + " " + Utility.datecon(Arr[1]);
                            objFS.ArrivalTime = Utility.Left(Utility.Split(t.Leg[i]["STA"], "T")[1], 5);
                            try
                            {
                                if (t.Leg.Length > 1)
                                {
                                    //DateTime DDTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[0]["STD"], "T")[1], 5));
                                    //DateTime ADTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[legs - 1]["STA"], "T")[1], 5));

                                    //TimeSpan value = ADTime - DDTime;
                                    //string label = string.Format("{0:hh\\:mm\\:ss}", value);
                                    //objFS.TotDur = Utility.Left(label, 5);
                                    objFS.TotDur = Utility.Left(Convert.ToDateTime(t.Leg[legs - 1]["STA"]).Subtract(Convert.ToDateTime(t.Leg[0]["STD"])).ToString(), 5);
                                }
                                else
                                {

                                    //DateTime DDTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[i]["STD"], "T")[1], 5));
                                    //DateTime ADTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[i]["STA"], "T")[1], 5));

                                    //TimeSpan value = ADTime - DDTime;
                                    //string label = string.Format("{0:hh\\:mm\\:ss}", value);
                                    //objFS.TotDur = Utility.Left(label, 5);
                                    objFS.TotDur = objFS.TotDur = Utility.Left(Convert.ToDateTime(t.Leg[i]["STA"]).Subtract(Convert.ToDateTime(t.Leg[i]["STD"])).ToString(), 5);

                                }
                            }
                            catch { objFS.TotDur = ""; }
                            objFS.Adult = searchInputs.Adult;
                            objFS.Child = searchInputs.Child;
                            objFS.Infant = searchInputs.Infant;
                            objFS.TotPax = searchInputs.Adult + searchInputs.Child;
                            objFS.MarketingCarrier = VC;
                            objFS.OperatingCarrier = VC;
                            objFS.FlightIdentification = t.Leg[i]["FlightNumber"];
                            objFS.ValiDatingCarrier = VC;
                            if (VC == "SG")
                                objFS.AirLineName = "SpiceJet";
                            else
                                objFS.AirLineName = "Indigo";
                            objFS.AvailableSeats = IdType;
                            objFS.AvailableSeats1 = t.AVLCNT;
                            if ((t.PCS == "HB") && (VC == "SG") && (("P1,P2").Contains(t.FCS)))
                                objFS.BagInfo = "7kg Hand Bag Only";
                            else
                                objFS.BagInfo = ((from bg in BagInfoList where bg.Class == "Economy" select bg).ToList())[0].Weight;

                            #region Baggage
                            objFS.IsBagFare = Bag;
                            if (objFareTypeSettings != null && objFareTypeSettings.Count > 0 && objFareTypeSettings[0].SSRCode != null)
                                objFS.SSRCode = objFareTypeSettings[0].SSRCode;
                            #endregion
                            #region SME FARE        
                            objFS.IsSMEFare = SMEFare;
                            #endregion
                            //objFS.TotDur = "";
                            objFS.arrdatelcc = t.Leg[i]["STA"];
                            objFS.depdatelcc = t.Leg[i]["STD"];

                            if (searchInputs.Trip == Trip.I)
                            {
                                if (schd == 0)
                                {
                                    if (searchInputs.TripType == STD.Shared.TripType.RoundTrip)
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo + ":" + objFS.OrgDestFrom;
                                    else
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                }
                                else
                                {
                                    if (searchInputs.TripType == STD.Shared.TripType.RoundTrip)
                                        objFS.Sector = objFS.OrgDestTo + ":" + objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                    else
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                }
                            }
                            else
                            {
                                if (schd == 0)
                                {
                                    if (searchInputs.RTF == true)
                                    { objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo + ":" + objFS.OrgDestFrom; }
                                    else
                                    { objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo; }
                                }
                                else
                                {
                                    if (searchInputs.RTF == true)
                                        objFS.Sector = objFS.OrgDestTo + ":" + objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                    else
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                }
                            }

                            objFS.EQ = t.Leg[i]["EQType"];
                            objFS.LineNumber = LNO;
                            // objFS.Ln = LNO;
                            objFS.Leg = (i + 1);
                            objFS.fareBasis = t.FBC;
                            objFS.RBD = t.FCS;
                            objFS.FareRule = t.FSK; //Fare Sell Key
                            objFS.Searchvalue = t.FSK; //Fare Sell Key
                            objFS.sno = t.JSK; //Journey Sell Key
                            #endregion

                            ////SMS charge calc
                            float srvCharge = 0;
                            float srvChargeAdt = 0;
                            float srvChargeChd = 0;

                            //float srvCharge = 0;
                            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID);
                            ////SMS charge calc end
                            try
                            {
                            #region Adult
                            //objFS.AdtFar = IdType;
                            objFS.AdtFar = CrdType;
                            objFS.AdtCabin = "E";// t.COS;
                            objFS.AdtFarebasis = t.FBC;
                            objFS.AdtRbd = t.FCS;
                            #region ADTFR
                            if (IdType != "SGTBF")
                            {
                                if ((t.PCS == "NN") && (VC == "SG") && (("F,G,H,I,J,K").Contains(t.FCS)))
                                    objFS.AdtFareType = "Non Refundable";
                                else if ((t.PCS == "AB") && (VC == "SG") && (("A,B").Contains(t.FCS)))
                                    objFS.AdtFareType = "Non Refundable";
                                else if ((VC == "6E") && (t.PCS == "S"))//(t.FBC == "P4SALE") &&
                                    objFS.AdtFareType = "Non Refundable";
                                else if (VC == "SG" && (t.FSK.Contains("AP7") || t.FSK.Contains("AP14")))
                                    objFS.AdtFareType = "Non Refundable";
                                else if ((t.PCS == "HB") && (VC == "SG") && (("P1,P2").Contains(t.FCS)))
                                    objFS.AdtFareType = "Non Refundable";
                                else if ((t.PCS == "NF") && (VC == "SG") && (("P1,P2").Contains(t.FCS)))
                                    objFS.AdtFareType = "Non Refundable";
                                else
                                    objFS.AdtFareType = "Refundable";
                            }
                            else if (IdType == "SGSTR")
                            { objFS.AdtFareType = "Spl. Fare, No Commission<br/>Refundable, Meal Included"; }
                            else
                            { objFS.AdtFareType = "Spl. Fare, No Commission<br/>Refundable"; }

                            //FareSellKey , JSK
                            #endregion

                            float PROMODISCHD = 0;
                            float PROMODIS = 0;
                            //Adt Fare Details 
                            if (searchInputs.Adult > 0)
                            {
                                int m = 0;
                                var AdtCharges = from ServiceCharges in t.PaxFare[0].ServiceCharges
                                                 where t.PaxFare[0].PaxType == "ADT"
                                                 select ServiceCharges;




                                foreach (var svc in AdtCharges)
                                {
                                    BookingServiceCharge adtd = new BookingServiceCharge();
                                    adtd = svc;
                                        // if (adtd.ChargeType.ToString().ToUpper() != "PROMOTIONDISCOUNT")
                                        if (!adtd.ChargeType.ToString().ToUpper().Contains("DISCOUNT"))
                                        {

                                        if (adtd.ChargeCode.ToString() == "YQ")
                                            objFS.AdtFSur = objFS.AdtFSur + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString() == "WO")
                                            objFS.AdtWO = objFS.AdtWO + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString() == "IN")
                                            objFS.AdtIN = objFS.AdtIN + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        //else if (adtd.ChargeCode.ToString() == "JN")
                                        //    objFS.AdtJN = objFS.AdtJN + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString().ToUpper().Contains("CGST") || adtd.ChargeCode.ToString().ToUpper().Contains("SGST") || adtd.ChargeCode.ToString().ToUpper().Contains("UTGST") || adtd.ChargeCode.ToString().ToUpper().Contains("IGST"))
                                            objFS.AdtJN = objFS.AdtJN + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString() == "YM")
                                            objFS.AdtYR = objFS.AdtYR + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString() == String.Empty)
                                            objFS.AdtBfare = objFS.AdtBfare + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else
                                            objFS.AdtOT = float.Parse(Math.Round(decimal.Parse(objFS.AdtOT.ToString()) + decimal.Parse(adtd.Amount.ToString()), 0).ToString());

                                        objFS.AdtFare = float.Parse(Math.Round(decimal.Parse(objFS.AdtFare.ToString()) + decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        AdtTF = AdtTF + decimal.Parse(adtd.Amount.ToString());
                                    }
                                    else
                                    {
                                        PROMODIS += float.Parse(adtd.Amount.ToString());
                                    }
                                }

                                objFS.AdtBfare = objFS.AdtBfare - PROMODIS;
                                objFS.AdtFare = objFS.AdtFare - PROMODIS;
                                AdtTF = AdtTF - decimal.Parse(PROMODIS.ToString());

                                objFS.AdtTax = objFS.AdtFare - objFS.AdtBfare;

                                // float srvChargeAdt = 0;
                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    srvChargeAdt = objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType, Convert.ToString(objFS.AdtBfare), Convert.ToString(objFS.AdtFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                }
                                catch { srvChargeAdt = 0; }
                                #endregion


                                //SMS charge add 
                                objFS.AdtOT = objFS.AdtOT + srvChargeAdt;//srvCharge;
                                objFS.AdtTax = objFS.AdtTax + srvChargeAdt;//srvCharge;
                                objFS.AdtFare = objFS.AdtFare + srvChargeAdt; //srvCharge;
                                //SMS charge add end
                                objFS.ADTAdminMrk = 0;// CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                //objFS.ADTAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.AdtFare, searchInputs.Trip.ToString());
                                objFS.ADTAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                {
                                    //objFS.ADTAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.AdtFare, searchInputs.Trip.ToString());
                                    objFS.ADTAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                    objFS.AdtBfare = objFS.AdtBfare + objFS.ADTAdminMrk;
                                    objFS.AdtFare = objFS.AdtFare + objFS.ADTAdminMrk;
                                }
                                CommDt.Clear();
                                STTFTDS.Clear();
                                if ((searchInputs.Trip.ToString() == JourneyType.D.ToString()) && (searchInputs.RTF != true)) //&& (searchInputs.RTF != true)
                                {
                                    //if ((IdType != "SGTBF") && (IdType != "SGSTR"))
                                    //{
                                            try
                                            {
                                            #region Show hide Net Fare and Commission Calculation-Adult Pax
                                            if (objFS.Leg == 1)
                                            {
                                                CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), 1, objFS.AdtRbd, objFS.AdtCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, CrdType, Convert.ToString(TotalViaSector));
                                                if (CommDt != null && CommDt.Rows.Count > 0)
                                                {                                                    
                                                    objFS.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                                    objFS.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                    STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.AdtDiscount1, objFS.AdtBfare, objFS.AdtFSur, searchInputs.TDS);
                                                    objFS.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                                    objFS.AdtDiscount = objFS.AdtDiscount1 - objFS.AdtSrvTax1;
                                                    objFS.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                                    objFS.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                                    objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());

                                                    //objFS.AdtDiscount = 0;
                                                    //objFS.AdtCB = 0;
                                                    //objFS.AdtSrvTax = 0;
                                                    //objFS.AdtTF = 0;
                                                    //objFS.AdtTds = 0;
                                                    //objFS.IATAComm = 0;
                                                }
                                                else
                                                {
                                                    objFS.AdtDiscount = 0;
                                                    objFS.AdtCB = 0;
                                                    objFS.AdtSrvTax = 0;
                                                    objFS.AdtTF = 0;
                                                    objFS.AdtTds = 0;
                                                    objFS.IATAComm = 0;
                                                }

                                            }
                                            else
                                            {
                                                objFS.AdtDiscount = 0;
                                                objFS.AdtCB = 0;
                                                objFS.AdtSrvTax = 0;
                                                objFS.AdtTF = 0;
                                                objFS.AdtTds = 0;
                                                objFS.IATAComm = 0;
                                            }
                                            #endregion                                           
                                            }
                                            catch (Exception ex)
                                            {
                                                objFS.AdtDiscount = 0;
                                                objFS.AdtCB = 0;
                                                objFS.AdtSrvTax = 0;
                                                objFS.AdtTF = 0;
                                                objFS.AdtTds = 0;
                                                objFS.IATAComm = 0;
                                            }
                                        //}
                                }
                                else
                                {
                                    objFS.AdtDiscount = 0;
                                    objFS.AdtCB = 0;
                                    objFS.AdtSrvTax = 0;
                                    objFS.AdtTF = 0;
                                    objFS.AdtTds = 0;
                                    objFS.IATAComm = 0;
                                }
                            }
                            if ((searchInputs.IsCorp == true) && (searchInputs.RTF != true))
                            {
                                try
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.AdtFare.ToString()));
                                    objFS.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    objFS.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                                catch { }
                            }
                            objFS.AdtEduCess = 0;
                            objFS.AdtHighEduCess = 0;
                            #endregion

                            #region child
                            objFS.ChdCabin = t.COS;
                            objFS.ChdFarebasis = t.FBC;
                            objFS.ChdRbd = t.FCS;
                            //objFS.ChdfareType
                            objFS.ChdTax = 0;
                            if (searchInputs.Child > 0)
                            {
                                var ChdCharges = from pxfare in t.PaxFare[1].ServiceCharges
                                                 where t.PaxFare[1].PaxType == "CHD"
                                                 select pxfare;
                                #region CHDFR
                                foreach (var svc in ChdCharges)
                                {
                                    BookingServiceCharge chdd = new BookingServiceCharge();
                                    chdd = svc;
                                        //if (chdd.ChargeType.ToString().ToUpper() != "PROMOTIONDISCOUNT")
                                        if (!chdd.ChargeType.ToString().ToUpper().Contains("DISCOUNT"))
                                        {
                                        if (chdd.ChargeCode.ToString() == "YQ")
                                            objFS.ChdFSur = objFS.ChdFSur + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString() == "WO")
                                            objFS.ChdWO = objFS.ChdWO + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString() == "IN")
                                            objFS.ChdIN = objFS.ChdIN + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        //else if (chdd.ChargeCode.ToString() == "JN")
                                        //    objFS.ChdJN = objFS.ChdJN + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString().ToUpper().Contains("CGST") || chdd.ChargeCode.ToString().ToUpper().Contains("SGST") || chdd.ChargeCode.ToString().ToUpper().Contains("UTGST") || chdd.ChargeCode.ToString().ToUpper().Contains("IGST"))
                                            objFS.ChdJN = objFS.ChdJN + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString() == "YM")
                                            objFS.ChdYR = objFS.ChdYR + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString() == String.Empty)
                                            objFS.ChdBFare = objFS.ChdBFare + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else
                                            objFS.ChdOT = float.Parse(Math.Round(decimal.Parse(objFS.ChdOT.ToString()) + decimal.Parse(chdd.Amount.ToString()), 0).ToString());

                                        objFS.ChdFare = float.Parse(Math.Round(decimal.Parse(objFS.ChdFare.ToString()) + decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        ChdTF = ChdTF + decimal.Parse(chdd.Amount.ToString());
                                    }
                                    else
                                    {
                                        PROMODISCHD += float.Parse(chdd.Amount.ToString());
                                    }
                                }
                                #endregion
                                objFS.ChdBFare = objFS.ChdBFare - PROMODISCHD;
                                objFS.ChdFare = objFS.ChdFare - PROMODISCHD;
                                ChdTF = ChdTF - decimal.Parse(PROMODISCHD.ToString());


                                objFS.ChdTax = objFS.ChdFare - objFS.ChdBFare;
                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    srvChargeChd = objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType, Convert.ToString(objFS.ChdBFare), Convert.ToString(objFS.ChdFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                }
                                catch { srvChargeChd = 0; }
                                #endregion


                                //SMS charge add 
                                objFS.ChdOT = objFS.ChdOT + srvChargeChd;//srvCharge;
                                objFS.ChdTax = objFS.ChdTax + srvChargeChd;//srvCharge;
                                objFS.ChdFare = objFS.ChdFare + srvChargeChd; //srvCharge;
                                //SMS charge add end

                                objFS.CHDAdminMrk = 0;// CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                                      // objFS.CHDAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.ChdFare, searchInputs.Trip.ToString());
                                objFS.CHDAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                CommDt.Clear();
                                STTFTDS.Clear();
                                if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                {
                                    //.CHDAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.ChdFare, searchInputs.Trip.ToString());
                                    objFS.CHDAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                    objFS.ChdBFare = objFS.ChdBFare + objFS.CHDAdminMrk;
                                    objFS.ChdFare = objFS.ChdFare + objFS.CHDAdminMrk;
                                }
                                if ((searchInputs.Trip.ToString() == JourneyType.D.ToString()) && (searchInputs.RTF != true))
                                {
                                    //if ((IdType != "SGTBF") && (IdType != "SGSTR"))
                                    //{
                                            try
                                            {
                                            #region Show hide Net Fare and Commission Calculation-Child Pax
                                            if (objFS.Leg == 1)
                                            {
                                                CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), 1, objFS.ChdRbd, objFS.ChdCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, CrdType, Convert.ToString(TotalViaSector));
                                                if (CommDt != null && CommDt.Rows.Count > 0)
                                                {                                                    
                                                    objFS.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                                    objFS.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                    STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.ChdDiscount1, objFS.ChdBFare, objFS.ChdFSur, searchInputs.TDS);
                                                    objFS.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                                    objFS.ChdDiscount = objFS.ChdDiscount1 - objFS.ChdSrvTax1;
                                                    objFS.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                                    objFS.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                                    //objFS.ChdDiscount = 0;
                                                    //objFS.ChdCB = 0;
                                                    //objFS.ChdSrvTax = 0;
                                                    //objFS.ChdTF = 0;
                                                    //objFS.ChdTds = 0;
                                                }
                                                else
                                                {
                                                    objFS.ChdDiscount = 0;
                                                    objFS.ChdCB = 0;
                                                    objFS.ChdSrvTax = 0;
                                                    objFS.ChdTF = 0;
                                                    objFS.ChdTds = 0;
                                                }

                                            }
                                            else
                                            {
                                                objFS.ChdDiscount = 0;
                                                objFS.ChdCB = 0;
                                                objFS.ChdSrvTax = 0;
                                                objFS.ChdTF = 0;
                                                objFS.ChdTds = 0;
                                            }
                                            #endregion                                            
                                            }
                                            catch(Exception ex)
                                            {
                                                objFS.ChdDiscount = 0;
                                                objFS.ChdCB = 0;
                                                objFS.ChdSrvTax = 0;
                                                objFS.ChdTF = 0;
                                                objFS.ChdTds = 0;
                                            }
                                   // }
                                }
                                else
                                {
                                    objFS.ChdDiscount = 0;
                                    objFS.ChdCB = 0;
                                    objFS.ChdSrvTax = 0;
                                    objFS.ChdTF = 0;
                                    objFS.ChdTds = 0;
                                }
                                if ((searchInputs.IsCorp == true) && (searchInputs.RTF != true))
                                {
                                    try
                                    {
                                        DataTable MGDT = new DataTable();
                                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier + CrdType, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.ChdFare.ToString()));
                                        objFS.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                        objFS.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                    }
                                    catch { }
                                }
                                objFS.ChdEduCess = 0;
                                objFS.ChdHighEduCess = 0;
                                    //objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());
                                }
                                else
                            {
                                objFS.ChdBFare = 0;
                                objFS.ChdFare = 0;
                                objFS.ChdFSur = 0;
                                objFS.ChdWO = 0;
                                objFS.ChdIN = 0;
                                objFS.ChdJN = 0;
                                objFS.ChdYR = 0;
                                objFS.ChdOT = 0;
                                objFS.ChdTax = 0;
                                objFS.ChdDiscount = 0;
                                objFS.ChdCB = 0;
                                objFS.ChdSrvTax = 0;
                                objFS.ChdTF = 0;
                                objFS.ChdTds = 0;
                                objFS.ChdEduCess = 0;
                                objFS.ChdHighEduCess = 0;

                            }

                            #endregion

                            #region PROMOCODE  Devesh
                            try
                            {
                                if (PCRow.Count() > 0)
                                {
                                    objFS.ElectronicTicketing = PCRow[0]["D_T_Code"].ToString() + "/" + PROMODIS + "/" + PROMODISCHD + "/" + PCRow[0]["AppliedOn"].ToString() + "/" + PCRow[0]["CodeType"].ToString();
                                }
                                else
                                {
                                    objFS.ElectronicTicketing = "";
                                }
                            }
                            catch (Exception ex)
                            { }
                            #endregion

                            #region Infant
                            if (searchInputs.Infant > 0)
                            {
                                if (t.InfFare > 0)
                                {
                                    if (t.InfFare > t.InfTax)
                                        objFS.InfBfare = float.Parse(Math.Round(t.InfFare - t.InfTax).ToString());
                                    else
                                        objFS.InfBfare = float.Parse(Math.Round(t.InfTax - t.InfFare).ToString());
                                    objFS.InfFare = float.Parse(Math.Round(t.InfFare).ToString());
                                    objFS.InfTax = float.Parse(Math.Round(t.InfTax).ToString());
                                    objFS.InfOT = objFS.InfTax;
                                    InfTF = decimal.Parse(Math.Round(t.InfFare).ToString());
                                }
                                else
                                {
                                    objFS.InfFare = float.Parse(Math.Round(InfantBFare + InfTax).ToString());
                                    objFS.InfBfare = float.Parse(Math.Round(InfantBFare).ToString());
                                    objFS.InfTax = float.Parse(Math.Round(InfTax).ToString());
                                    objFS.InfOT = objFS.InfTax;
                                    InfTF = decimal.Parse(Math.Round(objFS.InfFare).ToString());
                                }
                                objFS.InfFSur = 0;
                                objFS.InfIN = 0;
                                objFS.InfJN = 0;
                                //objFS.InfOT = 0;
                                objFS.InfQ = 0;

                                if (searchInputs.IsCorp == true)
                                {
                                    try
                                    {
                                        DataTable MGDT = new DataTable();
                                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier + CrdType, decimal.Parse(objFS.InfBfare.ToString()), decimal.Parse(objFS.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.InfFare.ToString()));
                                        objFS.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                        objFS.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                    }
                                    catch { }
                                }
                            }
                            else
                            {
                                objFS.InfFare = 0;
                                objFS.InfBfare = 0;
                                objFS.InfFSur = 0;
                                objFS.InfIN = 0;
                                objFS.InfJN = 0;
                                objFS.InfOT = 0;
                                objFS.InfQ = 0;
                            }
                                #endregion
                            }
                            catch (Exception ex)
                            {

                            }

                            objFS.OriginalTF = float.Parse((AdtTF * objFS.Adult + ChdTF * objFS.Child).ToString());//Without Infant Fare
                            objFS.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
                            objFS.TotalTax = (objFS.AdtTax * objFS.Adult) + (objFS.ChdTax * objFS.Child) + (objFS.InfTax * objFS.Infant);
                            objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
                            objFS.TotalFare = (objFS.AdtFare * objFS.Adult) + (objFS.ChdFare * objFS.Child) + (objFS.InfFare * objFS.Infant);
                            objFS.STax = (objFS.AdtSrvTax * objFS.Adult) + (objFS.ChdSrvTax * objFS.Child) + (objFS.InfSrvTax * objFS.Infant);
                            objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
                            objFS.TotDis = (objFS.AdtDiscount * objFS.Adult) + (objFS.ChdDiscount * objFS.Child);
                            objFS.TotCB = (objFS.AdtCB * objFS.Adult) + (objFS.ChdCB * objFS.Child);
                            objFS.TotTds = (objFS.AdtTds * objFS.Adult) + (objFS.ChdTds * objFS.Child);// +objFS.InfTds;
                            objFS.TotMrkUp = (objFS.ADTAdminMrk * objFS.Adult) + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAdminMrk * objFS.Child) + (objFS.CHDAgentMrk * objFS.Child);
                            objFS.TotMgtFee = (objFS.AdtMgtFee * objFS.Adult) + (objFS.ChdMgtFee * objFS.Child) + (objFS.InfMgtFee * objFS.Infant);
                            if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                objFS.TotalFare = objFS.TotalFare + objFS.STax + objFS.TFee + objFS.TotMgtFee + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child);
                            else
                                objFS.TotalFare = objFS.TotalFare + objFS.TotMrkUp + objFS.STax + objFS.TFee + objFS.TotMgtFee;
                            objFS.NetFare = (objFS.TotalFare + objFS.TotTds) - (objFS.TotDis + objFS.TotCB + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child));
                            //objFS.OperatingCarrier = p.opreatingcarrier;
                            objFS.NetFareSRTF = objFS.NetFare;
                            //int hr = int.Parse(p.jrnyTime) / 60;
                            //int min = int.Parse(p.jrnyTime) % 60;
                            //objFS.TotDur = hr + " hrs " + min + " min";

                            objFS.Stops = legs - 1 + "-Stop";

                            objFS.Trip = searchInputs.Trip.ToString();
                            // objFS.FType = searchInputs.TripType.ToString();
                            if ((searchInputs.RTF == true) || (searchInputs.Trip == Trip.I))
                            {
                                if (schd == 0)
                                {
                                    objFS.Flight = "1";
                                    if (searchInputs.RTF == true)
                                        objFS.FType = "RTF";
                                    else
                                        objFS.FType = "OutBound";
                                    objFS.TripType = "O";
                                }
                                else
                                {
                                    objFS.Flight = "2";
                                    if (searchInputs.RTF == true)
                                        objFS.FType = "RTF";
                                    else
                                        objFS.FType = "InBound";
                                    objFS.TripType = "R";
                                }
                            }
                            else
                            {
                                objFS.Flight = "1";
                                objFS.FType = "OutBound";
                                objFS.TripType = "O";
                            }
                            objFS.IsCorp = searchInputs.IsCorp;
                            objFS.Provider = "LCC";
                            srvCharge = srvChargeAdt + srvChargeChd;  //06-03-2018  Add MISC CHARGES PAX WISE
                            objFS.OriginalTT = srvCharge;
                            objFS.SearchId = SearchId;
                            FlightList.Add(objFS);
                        }
                        LNO = LNO + 1;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return FlightList;
        }
        public List<FlightSearchResults> Spice_GetFltResult(List<FarePriceJourney> FareBrk, List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet MrkupDS, FlightSearch searchInputs, int schd, decimal InfantBFare, decimal InfTax, string IdType, List<MISCCharges> MiscList, string VC, string CrdType, DataRow[] PCRow, string SearchId, List<FareTypeSettings> objFareTypeSettings, bool Bag = false, bool SMEFare = false)
        {
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            string arrCity = "", depCity = "";
            if (schd == 0)
            {
                depCity = Utility.Left(searchInputs.HidTxtDepCity, 3);
                arrCity = Utility.Left(searchInputs.HidTxtArrCity, 3);
            }
            else
            {
                depCity = Utility.Left(searchInputs.HidTxtArrCity, 3);
                arrCity = Utility.Left(searchInputs.HidTxtDepCity, 3);
            }
            List<FlightSearchResults> FlightList = new List<FlightSearchResults>();

            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            List<BaggageList> BagInfoList = new List<BaggageList>();
            BagInfoList = objFltComm.GetBaggage(searchInputs.Trip.ToString(), VC, Bag);
            
            string strFlightNo = "";
            string strValidatingCarrier = "";
            string FlttmDur = "";//added by abhilash
            int LNO = 1;

            try
            {
                //if (VC == "SG" && IdType == "SGNRML")
                //    srvCharge = srvCharge - 200;
                //foreach (FarePriceJourney t in FareBrk)
                #region OutBOund -Inbound
                for (int jrny = 0; jrny < FareBrk.Count; jrny++)
                {
                    FarePriceJourney t = new FarePriceJourney();
                    t = FareBrk[jrny];
                    try
                    {
                        #region Get TotalViaSector for Commission PPPSector and PPPSegment
                        int TotalViaSector = 1;
                        try
                        {
                            TotalViaSector = t.Leg.Select(x => x["FlightNumber"]).Distinct().Count();
                        }
                        catch (Exception ex)
                        {
                            TotalViaSector = 1;
                        }
                        #endregion
                        int legs = t.Leg.Length;
                        for (int i = 0; i < legs; i++)
                        {
                            FlightSearchResults objFS = new FlightSearchResults();
                            decimal AdtTF = 0, ChdTF = 0, InfTF = 0;//For Total Fare Per Pax Type
                            #region Flight Details
                            objFS.OrgDestFrom = depCity;
                            objFS.OrgDestTo = arrCity;
                            objFS.DepartureLocation = t.Leg[i]["DepartureStation"];
                            objFS.DepAirportCode = t.Leg[i]["DepartureStation"];
                            objFS.DepartureTerminal = t.Leg[i]["DepartureTerminal"];
                            objFS.DepartureCityName = getCityName(objFS.DepartureLocation, CityList);
                            objFS.ArrivalLocation = t.Leg[i]["ArrivalStation"];
                            objFS.ArrAirportCode = t.Leg[i]["ArrivalStation"];
                            objFS.ArrivalTerminal = t.Leg[i]["ArrivalTerminal"];
                            objFS.ArrivalCityName = getCityName(objFS.ArrivalLocation, CityList);
                            try
                            {
                                objFS.DepartureAirportName = ((from ct in CityList where ct.AirportCode == objFS.DepartureLocation select ct).ToList())[0].AirportName;
                                objFS.ArrivalAirportName = ((from ct in CityList where ct.AirportCode == objFS.ArrivalLocation select ct).ToList())[0].AirportName;
                            }
                            catch (Exception ex)
                            {
                                objFS.DepartureAirportName = t.Leg[i]["DepartureStation"];
                                objFS.ArrivalAirportName = t.Leg[i]["ArrivalStation"];
                            }
                            string[] Dep = Utility.Split(Utility.Split(t.Leg[i]["STD"], "T")[0], "-");
                            objFS.DepartureDate = Dep[2] + Dep[1] + Utility.Right(Dep[0], 2);
                            objFS.Departure_Date = Dep[2] + " " + Utility.datecon(Dep[1]);
                            objFS.DepartureTime = Utility.Left(Utility.Split(t.Leg[i]["STD"], "T")[1], 5);

                            string[] Arr = Utility.Split(Utility.Split(t.Leg[i]["STA"], "T")[0], "-");
                            objFS.ArrivalDate = Arr[2] + Arr[1] + Utility.Right(Arr[0], 2);
                            objFS.Arrival_Date = Arr[2] + " " + Utility.datecon(Arr[1]);
                            objFS.ArrivalTime = Utility.Left(Utility.Split(t.Leg[i]["STA"], "T")[1], 5);
                            try
                            {
                                if (t.Leg.Length > 1)
                                {
                                    //DateTime DDTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[0]["STD"], "T")[1], 5));
                                    //DateTime ADTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[legs - 1]["STA"], "T")[1], 5));

                                    //TimeSpan value = ADTime - DDTime;
                                    //string label = string.Format("{0:hh\\:mm\\:ss}", value);
                                    //objFS.TotDur = Utility.Left(label, 5);
                                    objFS.TotDur = Utility.Left(Convert.ToDateTime(t.Leg[legs - 1]["STA"]).Subtract(Convert.ToDateTime(t.Leg[0]["STD"])).ToString(), 5);
                                }
                                else
                                {

                                    //DateTime DDTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[i]["STD"], "T")[1], 5));
                                    //DateTime ADTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[i]["STA"], "T")[1], 5));

                                    //TimeSpan value = ADTime - DDTime;
                                    //string label = string.Format("{0:hh\\:mm\\:ss}", value);
                                    //objFS.TotDur = Utility.Left(label, 5);
                                    objFS.TotDur = objFS.TotDur = Utility.Left(Convert.ToDateTime(t.Leg[i]["STA"]).Subtract(Convert.ToDateTime(t.Leg[i]["STD"])).ToString(), 5);

                                }
                            }
                            catch { objFS.TotDur = ""; }
                            objFS.Adult = searchInputs.Adult;
                            objFS.Child = searchInputs.Child;
                            objFS.Infant = searchInputs.Infant;
                            objFS.TotPax = searchInputs.Adult + searchInputs.Child;
                            objFS.MarketingCarrier = VC;
                            objFS.OperatingCarrier = VC;
                            objFS.FlightIdentification = t.Leg[i]["FlightNumber"];
                            objFS.ValiDatingCarrier = VC;
                            if (VC == "SG")
                                objFS.AirLineName = "SpiceJet";
                            else
                                objFS.AirLineName = "Indigo";
                            objFS.AvailableSeats = IdType;
                            objFS.AvailableSeats1 = t.AVLCNT;
                            if ((t.PCS == "HB") && (VC == "SG") && (("P1,P2").Contains(t.FCS)))
                                objFS.BagInfo = "7kg Hand Bag Only";
                            else
                                objFS.BagInfo = ((from bg in BagInfoList where bg.Class == "Economy" select bg).ToList())[0].Weight;

                            #region Baggage
                            objFS.IsBagFare = Bag;
                            if (objFareTypeSettings != null && objFareTypeSettings.Count > 0 && objFareTypeSettings[0].SSRCode != null)
                                objFS.SSRCode = objFareTypeSettings[0].SSRCode;
                            #endregion
                            #region SME FARE        
                            objFS.IsSMEFare = SMEFare;
                            #endregion
                            //objFS.TotDur = "";
                            objFS.arrdatelcc = t.Leg[i]["STA"];
                            objFS.depdatelcc = t.Leg[i]["STD"];

                            if (searchInputs.Trip == Trip.I)
                            {
                                if (schd == 0)
                                {
                                    if (searchInputs.TripType == STD.Shared.TripType.RoundTrip)
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo + ":" + objFS.OrgDestFrom;
                                    else
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                }
                                else
                                {
                                    if (searchInputs.TripType == STD.Shared.TripType.RoundTrip)
                                        objFS.Sector = objFS.OrgDestTo + ":" + objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                    else
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                }
                            }
                            else
                            {
                                if (schd == 0)
                                {
                                    if (searchInputs.RTF == true)
                                    { objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo + ":" + objFS.OrgDestFrom; }
                                    else
                                    { objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo; }
                                }
                                else
                                {
                                    if (searchInputs.RTF == true)
                                        objFS.Sector = objFS.OrgDestTo + ":" + objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                    else
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                }
                            }

                            objFS.EQ = t.Leg[i]["EQType"];
                            objFS.LineNumber = LNO;
                            // objFS.Ln = LNO;
                            objFS.Leg = (i + 1);
                            objFS.fareBasis = t.FBC;
                            objFS.RBD = t.FCS;
                            objFS.FareRule = t.FSK; //Fare Sell Key
                            objFS.Searchvalue = t.FSK; //Fare Sell Key
                            objFS.sno = t.JSK; //Journey Sell Key
                            #endregion

                            ////SMS charge calc
                            float srvCharge = 0;
                            float srvChargeAdt = 0;
                            float srvChargeChd = 0;

                            //float srvCharge = 0;
                            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID);
                            ////SMS charge calc end


                            #region Adult
                            //objFS.AdtFar = IdType;
                            objFS.AdtFar = CrdType;
                            objFS.AdtCabin = "E";// t.COS;
                            objFS.AdtFarebasis = t.FBC;
                            objFS.AdtRbd = t.FCS;
                            #region ADTFR
                            if (IdType != "SGTBF")
                            {
                                if ((t.PCS == "NN") && (VC == "SG") && (("F,G,H,I,J,K").Contains(t.FCS)))
                                    objFS.AdtFareType = "Non Refundable";
                                else if ((t.PCS == "AB") && (VC == "SG") && (("A,B").Contains(t.FCS)))
                                    objFS.AdtFareType = "Non Refundable";
                                else if ((VC == "6E") && (t.PCS == "S"))//(t.FBC == "P4SALE") &&
                                    objFS.AdtFareType = "Non Refundable";
                                else if (VC == "SG" && (t.FSK.Contains("AP7") || t.FSK.Contains("AP14")))
                                    objFS.AdtFareType = "Non Refundable";
                                else if ((t.PCS == "HB") && (VC == "SG") && (("P1,P2").Contains(t.FCS)))
                                    objFS.AdtFareType = "Non Refundable";
                                else if ((t.PCS == "NF") && (VC == "SG") && (("P1,P2").Contains(t.FCS)))
                                    objFS.AdtFareType = "Non Refundable";
                                else
                                    objFS.AdtFareType = "Refundable";
                            }
                            else if (IdType == "SGSTR")
                            { objFS.AdtFareType = "Spl. Fare, No Commission<br/>Refundable, Meal Included"; }
                            else
                            { objFS.AdtFareType = "Spl. Fare, No Commission<br/>Refundable"; }

                            //FareSellKey , JSK
                            #endregion

                            float PROMODISCHD = 0;
                            float PROMODIS = 0;
                            //Adt Fare Details 
                            if (searchInputs.Adult > 0)
                            {
                                int m = 0;
                                var AdtCharges = from ServiceCharges in t.PaxFare[0].ServiceCharges
                                                 where t.PaxFare[0].PaxType == "ADT"
                                                 select ServiceCharges;




                                foreach (var svc in AdtCharges)
                                {
                                    BookingServiceCharge adtd = new BookingServiceCharge();
                                    adtd = svc;
                                    //if (adtd.ChargeType.ToString().ToUpper() != "PROMOTIONDISCOUNT")
                                        if (!adtd.ChargeType.ToString().ToUpper().Contains("DISCOUNT"))
                                        {

                                        if (adtd.ChargeCode.ToString() == "YQ")
                                            objFS.AdtFSur = objFS.AdtFSur + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString() == "WO")
                                            objFS.AdtWO = objFS.AdtWO + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString() == "IN")
                                            objFS.AdtIN = objFS.AdtIN + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        //else if (adtd.ChargeCode.ToString() == "JN")
                                        //    objFS.AdtJN = objFS.AdtJN + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString().ToUpper().Contains("CGST") || adtd.ChargeCode.ToString().ToUpper().Contains("SGST") || adtd.ChargeCode.ToString().ToUpper().Contains("UTGST") || adtd.ChargeCode.ToString().ToUpper().Contains("IGST"))
                                            objFS.AdtJN = objFS.AdtJN + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString() == "YM")
                                            objFS.AdtYR = objFS.AdtYR + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString() == String.Empty)
                                            objFS.AdtBfare = objFS.AdtBfare + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else
                                            objFS.AdtOT = float.Parse(Math.Round(decimal.Parse(objFS.AdtOT.ToString()) + decimal.Parse(adtd.Amount.ToString()), 0).ToString());

                                        objFS.AdtFare = float.Parse(Math.Round(decimal.Parse(objFS.AdtFare.ToString()) + decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        AdtTF = AdtTF + decimal.Parse(adtd.Amount.ToString());
                                    }
                                    else
                                    {
                                        PROMODIS += float.Parse(adtd.Amount.ToString());
                                    }
                                }

                                objFS.AdtBfare = objFS.AdtBfare - PROMODIS;
                                objFS.AdtFare = objFS.AdtFare - PROMODIS;
                                AdtTF = AdtTF - decimal.Parse(PROMODIS.ToString());

                                objFS.AdtTax = objFS.AdtFare - objFS.AdtBfare;

                                // float srvChargeAdt = 0;
                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    srvChargeAdt = objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType, Convert.ToString(objFS.AdtBfare), Convert.ToString(objFS.AdtFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                }
                                catch { srvChargeAdt = 0; }
                                #endregion
                               

                                //SMS charge add 
                                objFS.AdtOT = objFS.AdtOT + srvChargeAdt;//srvCharge;
                                objFS.AdtTax = objFS.AdtTax + srvChargeAdt;//srvCharge;
                                objFS.AdtFare = objFS.AdtFare + srvChargeAdt; //srvCharge;
                                //SMS charge add end
                                objFS.ADTAdminMrk = 0;// CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                //objFS.ADTAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.AdtFare, searchInputs.Trip.ToString());
                                objFS.ADTAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                {
                                    //objFS.ADTAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.AdtFare, searchInputs.Trip.ToString());
                                    objFS.ADTAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                    objFS.AdtBfare = objFS.AdtBfare + objFS.ADTAdminMrk;
                                    objFS.AdtFare = objFS.AdtFare + objFS.ADTAdminMrk;
                                }
                                CommDt.Clear();
                                STTFTDS.Clear();
                                if ((searchInputs.Trip.ToString() == JourneyType.D.ToString()) && (searchInputs.RTF != true)) //&& (searchInputs.RTF != true)
                                {
                                    //if ((IdType != "SGTBF") && (IdType != "SGSTR"))
                                    //{
                                        try
                                        {
                                        #region Show hide Net Fare and Commission Calculation-Adult Pax
                                        if (objFS.Leg == 1)
                                        {
                                            CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), 1, objFS.AdtRbd, objFS.AdtCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, CrdType, Convert.ToString(TotalViaSector));
                                            if (CommDt != null && CommDt.Rows.Count > 0)
                                            {                                                
                                                objFS.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                                objFS.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.AdtDiscount1, objFS.AdtBfare, objFS.AdtFSur, searchInputs.TDS);
                                                objFS.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                                objFS.AdtDiscount = objFS.AdtDiscount1 - objFS.AdtSrvTax1;
                                                objFS.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                                objFS.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                                objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());

                                                //objFS.AdtDiscount = 0;
                                                //objFS.AdtCB = 0;
                                                //objFS.AdtSrvTax = 0;
                                                //objFS.AdtTF = 0;
                                                //objFS.AdtTds = 0;
                                                //objFS.IATAComm = 0;
                                            }
                                            else
                                            {
                                                objFS.AdtDiscount = 0;
                                                objFS.AdtCB = 0;
                                                objFS.AdtSrvTax = 0;
                                                objFS.AdtTF = 0;
                                                objFS.AdtTds = 0;
                                                objFS.IATAComm = 0;
                                            }

                                        }
                                        else
                                        {
                                            objFS.AdtDiscount = 0;
                                            objFS.AdtCB = 0;
                                            objFS.AdtSrvTax = 0;
                                            objFS.AdtTF = 0;
                                            objFS.AdtTds = 0;
                                            objFS.IATAComm = 0;
                                        }
                                        #endregion                                       
                                    }
                                    catch (Exception ex)
                                        {
                                            objFS.AdtDiscount = 0;
                                            objFS.AdtCB = 0;
                                            objFS.AdtSrvTax = 0;
                                            objFS.AdtTF = 0;
                                            objFS.AdtTds = 0;
                                            objFS.IATAComm = 0;
                                        }                                        
                                    //}
                                }
                                else
                                {
                                    objFS.AdtDiscount = 0;
                                    objFS.AdtCB = 0;
                                    objFS.AdtSrvTax = 0;
                                    objFS.AdtTF = 0;
                                    objFS.AdtTds = 0;
                                    objFS.IATAComm = 0;
                                }
                            }
                            if ((searchInputs.IsCorp == true) && (searchInputs.RTF != true))
                            {
                                try
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.AdtFare.ToString()));
                                    objFS.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    objFS.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                                catch { }
                            }
                            objFS.AdtEduCess = 0;
                            objFS.AdtHighEduCess = 0;
                            #endregion

                            #region child
                            objFS.ChdCabin = t.COS;
                            objFS.ChdFarebasis = t.FBC;
                            objFS.ChdRbd = t.FCS;
                            //objFS.ChdfareType
                            objFS.ChdTax = 0;
                            if (searchInputs.Child > 0)
                            {
                                var ChdCharges = from pxfare in t.PaxFare[1].ServiceCharges
                                                 where t.PaxFare[1].PaxType == "CHD"
                                                 select pxfare;
                                #region CHDFR
                                foreach (var svc in ChdCharges)
                                {
                                    BookingServiceCharge chdd = new BookingServiceCharge();
                                    chdd = svc;
                                    //if (chdd.ChargeType.ToString().ToUpper() != "PROMOTIONDISCOUNT")
                                        if (!chdd.ChargeType.ToString().ToUpper().Contains("DISCOUNT"))
                                        {
                                        if (chdd.ChargeCode.ToString() == "YQ")
                                            objFS.ChdFSur = objFS.ChdFSur + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString() == "WO")
                                            objFS.ChdWO = objFS.ChdWO + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString() == "IN")
                                            objFS.ChdIN = objFS.ChdIN + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        //else if (chdd.ChargeCode.ToString() == "JN")
                                        //    objFS.ChdJN = objFS.ChdJN + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString().ToUpper().Contains("CGST") || chdd.ChargeCode.ToString().ToUpper().Contains("SGST") || chdd.ChargeCode.ToString().ToUpper().Contains("UTGST") || chdd.ChargeCode.ToString().ToUpper().Contains("IGST"))
                                            objFS.ChdJN = objFS.ChdJN + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString() == "YM")
                                            objFS.ChdYR = objFS.ChdYR + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString() == String.Empty)
                                            objFS.ChdBFare = objFS.ChdBFare + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else
                                            objFS.ChdOT = float.Parse(Math.Round(decimal.Parse(objFS.ChdOT.ToString()) + decimal.Parse(chdd.Amount.ToString()), 0).ToString());

                                        objFS.ChdFare = float.Parse(Math.Round(decimal.Parse(objFS.ChdFare.ToString()) + decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        ChdTF = ChdTF + decimal.Parse(chdd.Amount.ToString());
                                    }
                                    else
                                    {
                                        PROMODISCHD += float.Parse(chdd.Amount.ToString());
                                    }
                                }
                                #endregion
                                objFS.ChdBFare = objFS.ChdBFare - PROMODISCHD;
                                objFS.ChdFare = objFS.ChdFare - PROMODISCHD;
                                ChdTF = ChdTF - decimal.Parse(PROMODISCHD.ToString());


                                objFS.ChdTax = objFS.ChdFare - objFS.ChdBFare;
                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    srvChargeChd = objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType, Convert.ToString(objFS.ChdBFare), Convert.ToString(objFS.ChdFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                }
                                catch { srvChargeChd = 0; }
                                #endregion

                               
                                //SMS charge add 
                                objFS.ChdOT = objFS.ChdOT + srvChargeChd;//srvCharge;
                                objFS.ChdTax = objFS.ChdTax + srvChargeChd;//srvCharge;
                                objFS.ChdFare = objFS.ChdFare + srvChargeChd; //srvCharge;
                                //SMS charge add end

                                objFS.CHDAdminMrk = 0;// CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                               // objFS.CHDAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.ChdFare, searchInputs.Trip.ToString());
                                objFS.CHDAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                CommDt.Clear();
                                STTFTDS.Clear();
                                if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                {
                                    //.CHDAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.ChdFare, searchInputs.Trip.ToString());
                                    objFS.CHDAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                    objFS.ChdBFare = objFS.ChdBFare + objFS.CHDAdminMrk;
                                    objFS.ChdFare = objFS.ChdFare + objFS.CHDAdminMrk;
                                }
                                if ((searchInputs.Trip.ToString() == JourneyType.D.ToString()) && (searchInputs.RTF != true))
                                {
                                    try
                                    {
                                        #region Show hide Net Fare and Commission Calculation-Child Pax
                                        if (objFS.Leg == 1)
                                        {
                                            CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), 1, objFS.ChdRbd, objFS.ChdCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, CrdType, Convert.ToString(TotalViaSector));
                                            if (CommDt != null && CommDt.Rows.Count > 0)
                                            {                                                
                                                objFS.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                                objFS.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.ChdDiscount1, objFS.ChdBFare, objFS.ChdFSur, searchInputs.TDS);
                                                objFS.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                                objFS.ChdDiscount = objFS.ChdDiscount1 - objFS.ChdSrvTax1;
                                                objFS.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                                objFS.ChdTds = float.Parse(STTFTDS["Tds"].ToString());

                                                //objFS.ChdDiscount = 0;
                                                //objFS.ChdCB = 0;
                                                //objFS.ChdSrvTax = 0;
                                                //objFS.ChdTF = 0;
                                                //objFS.ChdTds = 0;
                                            }
                                            else
                                            {
                                                objFS.ChdDiscount = 0;
                                                objFS.ChdCB = 0;
                                                objFS.ChdSrvTax = 0;
                                                objFS.ChdTF = 0;
                                                objFS.ChdTds = 0;
                                            }

                                        }
                                        else
                                        {
                                            objFS.ChdDiscount = 0;
                                            objFS.ChdCB = 0;
                                            objFS.ChdSrvTax = 0;
                                            objFS.ChdTF = 0;
                                            objFS.ChdTds = 0;
                                        }
                                        #endregion

                                        
                                    }
                                    catch(Exception ex)
                                    {
                                        objFS.ChdDiscount = 0;
                                        objFS.ChdCB = 0;
                                        objFS.ChdSrvTax = 0;
                                        objFS.ChdTF = 0;
                                        objFS.ChdTds = 0;
                                    }
                                }
                                else
                                {
                                    objFS.ChdDiscount = 0;
                                    objFS.ChdCB = 0;
                                    objFS.ChdSrvTax = 0;
                                    objFS.ChdTF = 0;
                                    objFS.ChdTds = 0;
                                }
                                if ((searchInputs.IsCorp == true) && (searchInputs.RTF != true))
                                {
                                    try
                                    {
                                        DataTable MGDT = new DataTable();
                                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier + CrdType, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.ChdFare.ToString()));
                                        objFS.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                        objFS.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                    }
                                    catch { }
                                }
                                objFS.ChdEduCess = 0;
                                objFS.ChdHighEduCess = 0;
                                //objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());
                            }
                            else
                            {
                                objFS.ChdBFare = 0;
                                objFS.ChdFare = 0;
                                objFS.ChdFSur = 0;
                                objFS.ChdWO = 0;
                                objFS.ChdIN = 0;
                                objFS.ChdJN = 0;
                                objFS.ChdYR = 0;
                                objFS.ChdOT = 0;
                                objFS.ChdTax = 0;
                                objFS.ChdDiscount = 0;
                                objFS.ChdCB = 0;
                                objFS.ChdSrvTax = 0;
                                objFS.ChdTF = 0;
                                objFS.ChdTds = 0;
                                objFS.ChdEduCess = 0;
                                objFS.ChdHighEduCess = 0;

                            }

                            #endregion

                            #region PROMOCODE  Devesh
                            try
                            {
                                if (PCRow.Count() > 0)
                                {
                                    objFS.ElectronicTicketing = PCRow[0]["D_T_Code"].ToString() + "/" + PROMODIS + "/" + PROMODISCHD + "/" + PCRow[0]["AppliedOn"].ToString() + "/" + PCRow[0]["CodeType"].ToString();
                                }
                                else
                                {
                                    objFS.ElectronicTicketing = "";
                                }
                            }
                            catch (Exception ex)
                            { }
                            #endregion

                            #region Infant
                            if (searchInputs.Infant > 0)
                            {
                                if (t.InfFare > 0)
                                {
                                    if (t.InfFare > t.InfTax)
                                        objFS.InfBfare = float.Parse(Math.Round(t.InfFare - t.InfTax).ToString());
                                    else
                                        objFS.InfBfare = float.Parse(Math.Round(t.InfTax - t.InfFare).ToString());
                                    objFS.InfFare = float.Parse(Math.Round(t.InfFare).ToString());
                                    objFS.InfTax = float.Parse(Math.Round(t.InfTax).ToString());
                                    objFS.InfOT = objFS.InfTax;
                                    InfTF = decimal.Parse(Math.Round(t.InfFare).ToString());
                                }
                                else
                                {
                                    objFS.InfFare = float.Parse(Math.Round(InfantBFare + InfTax).ToString());
                                    objFS.InfBfare = float.Parse(Math.Round(InfantBFare).ToString());
                                    objFS.InfTax = float.Parse(Math.Round(InfTax).ToString());
                                    objFS.InfOT = objFS.InfTax;
                                    InfTF = decimal.Parse(Math.Round(objFS.InfFare).ToString());
                                }
                                objFS.InfFSur = 0;
                                objFS.InfIN = 0;
                                objFS.InfJN = 0;
                                //objFS.InfOT = 0;
                                objFS.InfQ = 0;

                                if (searchInputs.IsCorp == true)
                                {
                                    try
                                    {
                                        DataTable MGDT = new DataTable();
                                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier + CrdType, decimal.Parse(objFS.InfBfare.ToString()), decimal.Parse(objFS.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.InfFare.ToString()));
                                        objFS.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                        objFS.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                    }
                                    catch { }
                                }
                            }
                            else
                            {
                                objFS.InfFare = 0;
                                objFS.InfBfare = 0;
                                objFS.InfFSur = 0;
                                objFS.InfIN = 0;
                                objFS.InfJN = 0;
                                objFS.InfOT = 0;
                                objFS.InfQ = 0;
                            }
                            #endregion

                            objFS.OriginalTF = float.Parse((AdtTF * objFS.Adult + ChdTF * objFS.Child).ToString());//Without Infant Fare
                            objFS.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
                            objFS.TotalTax = (objFS.AdtTax * objFS.Adult) + (objFS.ChdTax * objFS.Child) + (objFS.InfTax * objFS.Infant);
                            objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
                            objFS.TotalFare = (objFS.AdtFare * objFS.Adult) + (objFS.ChdFare * objFS.Child) + (objFS.InfFare * objFS.Infant);
                            objFS.STax = (objFS.AdtSrvTax * objFS.Adult) + (objFS.ChdSrvTax * objFS.Child) + (objFS.InfSrvTax * objFS.Infant);
                            objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
                            objFS.TotDis = (objFS.AdtDiscount * objFS.Adult) + (objFS.ChdDiscount * objFS.Child);
                            objFS.TotCB = (objFS.AdtCB * objFS.Adult) + (objFS.ChdCB * objFS.Child);
                            objFS.TotTds = (objFS.AdtTds * objFS.Adult) + (objFS.ChdTds * objFS.Child);// +objFS.InfTds;
                            objFS.TotMrkUp = (objFS.ADTAdminMrk * objFS.Adult) + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAdminMrk * objFS.Child) + (objFS.CHDAgentMrk * objFS.Child);
                            objFS.TotMgtFee = (objFS.AdtMgtFee * objFS.Adult) + (objFS.ChdMgtFee * objFS.Child) + (objFS.InfMgtFee * objFS.Infant);
                            if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                objFS.TotalFare = objFS.TotalFare + objFS.STax + objFS.TFee + objFS.TotMgtFee + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child);
                            else
                                objFS.TotalFare = objFS.TotalFare + objFS.TotMrkUp + objFS.STax + objFS.TFee + objFS.TotMgtFee;
                            objFS.NetFare = (objFS.TotalFare + objFS.TotTds) - (objFS.TotDis + objFS.TotCB + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child));
                            //objFS.OperatingCarrier = p.opreatingcarrier;
                            objFS.NetFareSRTF = objFS.NetFare;
                            //int hr = int.Parse(p.jrnyTime) / 60;
                            //int min = int.Parse(p.jrnyTime) % 60;
                            //objFS.TotDur = hr + " hrs " + min + " min";

                            objFS.Stops = legs - 1 + "-Stop";

                            objFS.Trip = searchInputs.Trip.ToString();
                            // objFS.FType = searchInputs.TripType.ToString();
                            if ((searchInputs.RTF == true) || (searchInputs.Trip == Trip.I))
                            {
                                if (schd == 0)
                                {
                                    objFS.Flight = "1";
                                    if (searchInputs.RTF == true)
                                        objFS.FType = "RTF";
                                    else
                                        objFS.FType = "OutBound";
                                    objFS.TripType = "O";
                                }
                                else
                                {
                                    objFS.Flight = "2";
                                    if (searchInputs.RTF == true)
                                        objFS.FType = "RTF";
                                    else
                                        objFS.FType = "InBound";
                                    objFS.TripType = "R";
                                }
                            }
                            else
                            {
                                objFS.Flight = "1";
                                objFS.FType = "OutBound";
                                objFS.TripType = "O";
                            }
                            objFS.IsCorp = searchInputs.IsCorp;
                            objFS.Provider = "LCC";
                            srvCharge = srvChargeAdt + srvChargeChd;  //06-03-2018  Add MISC CHARGES PAX WISE
                            objFS.OriginalTT = srvCharge;
                            objFS.SearchId = SearchId;
                            FlightList.Add(objFS);
                        }
                        LNO = LNO + 1;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return FlightList;
        }
        //public List<FlightSearchResults> Spice_GetFltResult(List<FarePriceJourney_V4> FareBrk, List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet MrkupDS, FlightSearch searchInputs, int schd, decimal InfantBFare, decimal InfTax, string IdType, float srvCharge, string VC, string CrdType, DataRow[] PCRow, string SearchId, List<FareTypeSettings> objFareTypeSettings, bool Bag = false)
        public List<FlightSearchResults> Spice_GetFltResult(List<FarePriceJourney_V4> FareBrk, List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet MrkupDS, FlightSearch searchInputs, int schd, decimal InfantBFare, decimal InfTax, string IdType, List<MISCCharges> MiscList, string VC, string CrdType, DataRow[] PCRow, string SearchId, List<FareTypeSettings> objFareTypeSettings, bool Bag = false, bool SMEFare = false)
        {
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            string arrCity = "", depCity = "";
            if (schd == 0)
            {
                depCity = Utility.Left(searchInputs.HidTxtDepCity, 3);
                arrCity = Utility.Left(searchInputs.HidTxtArrCity, 3);
            }
            else
            {
                depCity = Utility.Left(searchInputs.HidTxtArrCity, 3);
                arrCity = Utility.Left(searchInputs.HidTxtDepCity, 3);
            }
            List<FlightSearchResults> FlightList = new List<FlightSearchResults>();

            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            List<BaggageList> BagInfoList = new List<BaggageList>();
            BagInfoList = objFltComm.GetBaggage(searchInputs.Trip.ToString(), VC, Bag);
            string strFlightNo = "";
            string strValidatingCarrier = "";
            string FlttmDur = "";//added by abhilash
            int LNO = 1;

            try
            {
                //if (VC == "SG" && IdType == "SGNRML")
                //    srvCharge = srvCharge - 200;
                //foreach (FarePriceJourney t in FareBrk)
                #region OutBOund -Inbound
                for (int jrny = 0; jrny < FareBrk.Count; jrny++)
                {
                    FarePriceJourney_V4 t = new FarePriceJourney_V4();
                    t = FareBrk[jrny];
                    try
                    {
                        #region Get TotalViaSector for Commission PPPSector and PPPSegment
                        int TotalViaSector = 1;
                        try
                        {
                            TotalViaSector = t.Leg.Select(x => x["FlightNumber"]).Distinct().Count();
                        }
                        catch (Exception ex)
                        {
                            TotalViaSector = 1;
                        }
                        #endregion
                        int legs = t.Leg.Length;
                        for (int i = 0; i < legs; i++)
                        {
                            FlightSearchResults objFS = new FlightSearchResults();
                            decimal AdtTF = 0, ChdTF = 0, InfTF = 0;//For Total Fare Per Pax Type
                            #region Flight Details
                            objFS.OrgDestFrom = depCity;
                            objFS.OrgDestTo = arrCity;
                            objFS.DepartureLocation = t.Leg[i]["DepartureStation"];
                            objFS.DepAirportCode = t.Leg[i]["DepartureStation"];
                            objFS.DepartureTerminal = t.Leg[i]["DepartureTerminal"];
                            objFS.DepartureCityName = getCityName(objFS.DepartureLocation, CityList);
                            objFS.ArrivalLocation = t.Leg[i]["ArrivalStation"];
                            objFS.ArrAirportCode = t.Leg[i]["ArrivalStation"];
                            objFS.ArrivalTerminal = t.Leg[i]["ArrivalTerminal"];
                            objFS.ArrivalCityName = getCityName(objFS.ArrivalLocation, CityList);
                            try
                            {
                                objFS.DepartureAirportName = ((from ct in CityList where ct.AirportCode == objFS.DepartureLocation select ct).ToList())[0].AirportName;
                                objFS.ArrivalAirportName = ((from ct in CityList where ct.AirportCode == objFS.ArrivalLocation select ct).ToList())[0].AirportName;
                            }
                            catch (Exception ex)
                            {
                                objFS.DepartureAirportName = t.Leg[i]["DepartureStation"];
                                objFS.ArrivalAirportName = t.Leg[i]["ArrivalStation"];
                            }
                            string[] Dep = Utility.Split(Utility.Split(t.Leg[i]["STD"], "T")[0], "-");
                            objFS.DepartureDate = Dep[2] + Dep[1] + Utility.Right(Dep[0], 2);
                            objFS.Departure_Date = Dep[2] + " " + Utility.datecon(Dep[1]);
                            objFS.DepartureTime = Utility.Left(Utility.Split(t.Leg[i]["STD"], "T")[1], 5);

                            string[] Arr = Utility.Split(Utility.Split(t.Leg[i]["STA"], "T")[0], "-");
                            objFS.ArrivalDate = Arr[2] + Arr[1] + Utility.Right(Arr[0], 2);
                            objFS.Arrival_Date = Arr[2] + " " + Utility.datecon(Arr[1]);
                            objFS.ArrivalTime = Utility.Left(Utility.Split(t.Leg[i]["STA"], "T")[1], 5);
                            try
                            {
                                if (t.Leg.Length > 1)
                                {
                                    //DateTime DDTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[0]["STD"], "T")[1], 5));
                                    //DateTime ADTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[legs - 1]["STA"], "T")[1], 5));

                                    //TimeSpan value = ADTime - DDTime;
                                    //string label = string.Format("{0:hh\\:mm\\:ss}", value);
                                    //objFS.TotDur = Utility.Left(label, 5);
                                    objFS.TotDur = Utility.Left(Convert.ToDateTime(t.Leg[legs - 1]["STA"]).Subtract(Convert.ToDateTime(t.Leg[0]["STD"])).ToString(), 5);
                                }
                                else
                                {

                                    //DateTime DDTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[i]["STD"], "T")[1], 5));
                                    //DateTime ADTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[i]["STA"], "T")[1], 5));

                                    //TimeSpan value = ADTime - DDTime;
                                    //string label = string.Format("{0:hh\\:mm\\:ss}", value);
                                    //objFS.TotDur = Utility.Left(label, 5);
                                    objFS.TotDur = objFS.TotDur = Utility.Left(Convert.ToDateTime(t.Leg[i]["STA"]).Subtract(Convert.ToDateTime(t.Leg[i]["STD"])).ToString(), 5);

                                }
                            }
                            catch { objFS.TotDur = ""; }
                            objFS.Adult = searchInputs.Adult;
                            objFS.Child = searchInputs.Child;
                            objFS.Infant = searchInputs.Infant;
                            objFS.TotPax = searchInputs.Adult + searchInputs.Child;
                            objFS.MarketingCarrier = VC;
                            objFS.OperatingCarrier = VC;
                            objFS.FlightIdentification = t.Leg[i]["FlightNumber"];
                            objFS.ValiDatingCarrier = VC;
                            objFS.AirLineName = "Goair";
                            objFS.AvailableSeats = IdType;
                            objFS.AvailableSeats1 = t.AVLCNT;
                            string cbn = "";
                            if ((t.PCS == "HB") && (VC == "SG") && (("P1,P2").Contains(t.FCS)))
                                objFS.BagInfo = "7kg Hand Bag Only";
                            else
                            //objFS.BagInfo = ((from bg in BagInfoList where bg.Class == "Economy" select bg).ToList())[0].Weight;
                            {

                               // cbn = t.PCS == "GB" ? "BUSINESS" : "ECONOMY";
                                cbn = ("GB,GC").ToString().Contains(t.PCS.Trim()) ? "BUSINESS" : "E";
                                objFS.BagInfo = ((from bg in BagInfoList where bg.Class.ToUpper() == cbn select bg).ToList())[0].Weight;
                            }

                            #region Baggage
                            objFS.IsBagFare = Bag;
                            if (objFareTypeSettings != null && objFareTypeSettings.Count > 0 && objFareTypeSettings[0].SSRCode != null)
                                objFS.SSRCode = objFareTypeSettings[0].SSRCode;
                            #endregion
                            #region SME FARE        
                            objFS.IsSMEFare = SMEFare;
                            #endregion
                            //objFS.TotDur = "";
                            objFS.arrdatelcc = t.Leg[i]["STA"];
                            objFS.depdatelcc = t.Leg[i]["STD"];

                            if (searchInputs.Trip == Trip.I)
                            {
                                if (schd == 0)
                                {
                                    if (searchInputs.TripType == STD.Shared.TripType.RoundTrip)
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo + ":" + objFS.OrgDestFrom;
                                    else
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                }
                                else
                                {
                                    if (searchInputs.TripType == STD.Shared.TripType.RoundTrip)
                                        objFS.Sector = objFS.OrgDestTo + ":" + objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                    else
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                }
                            }
                            else
                            {
                                if (schd == 0)
                                {
                                    if (searchInputs.RTF == true)
                                    { objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo + ":" + objFS.OrgDestFrom; }
                                    else
                                    { objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo; }
                                }
                                else
                                {
                                    if (searchInputs.RTF == true)
                                        objFS.Sector = objFS.OrgDestTo + ":" + objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                    else
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                }
                            }

                            objFS.EQ = t.Leg[i]["EQType"];
                            objFS.LineNumber = LNO;
                            // objFS.Ln = LNO;
                            objFS.Leg = (i + 1);
                            objFS.fareBasis = t.FBC;
                            objFS.RBD = t.FCS;
                            objFS.FareRule = t.FSK; //Fare Sell Key
                            objFS.Searchvalue = t.FSK; //Fare Sell Key
                            objFS.sno = t.JSK; //Journey Sell Key
                            #endregion

                            ////SMS charge calc
                            float srvCharge = 0;
                            float srvChargeAdt = 0;
                            float srvChargeChd = 0;
                            
                            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID);
                            ////SMS charge calc end


                            #region Adult
                           // objFS.AdtFar = IdType;
                            objFS.AdtFar = CrdType;
                            objFS.AdtCabin = cbn;// "E";// t.COS;
                            objFS.AdtFarebasis = t.FBC;
                            objFS.AdtRbd = t.FCS;
                            #region ADTFR
                            if (IdType != "SGTBF")
                            {
                                if ((t.PCS == "NN") && (VC == "SG") && (("F,G,H,I,J,K").Contains(t.FCS)))
                                    objFS.AdtFareType = "Non Refundable";
                                else if ((t.PCS == "AB") && (VC == "SG") && (("A,B").Contains(t.FCS)))
                                    objFS.AdtFareType = "Non Refundable";
                                else if ((VC == "6E") && (t.PCS == "S"))//(t.FBC == "P4SALE") &&
                                    objFS.AdtFareType = "Non Refundable";
                                else if (VC == "SG" && (t.FSK.Contains("AP7") || t.FSK.Contains("AP14")))
                                    objFS.AdtFareType = "Non Refundable";
                                else if ((t.PCS == "HB") && (VC == "SG") && (("P1,P2").Contains(t.FCS)))
                                    objFS.AdtFareType = "Non Refundable";
                                else if ((t.PCS == "NF") && (VC == "SG") && (("P1,P2").Contains(t.FCS)))
                                    objFS.AdtFareType = "Non Refundable";
                                else
                                    objFS.AdtFareType = "Refundable";
                            }
                            else if (IdType == "SGSTR")
                            { objFS.AdtFareType = "Spl. Fare, No Commission<br/>Refundable, Meal Included"; }
                            else
                            { objFS.AdtFareType = "Spl. Fare, No Commission<br/>Refundable"; }

                            //FareSellKey , JSK
                            #endregion

                            float PROMODISCHD = 0;
                            float PROMODIS = 0;
                            //Adt Fare Details 
                            if (searchInputs.Adult > 0)
                            {
                                int m = 0;
                                var AdtCharges = from ServiceCharges in t.PaxFare[0].ServiceCharges
                                                 where t.PaxFare[0].PaxType == "ADT"
                                                 select ServiceCharges;




                                foreach (var svc in AdtCharges)
                                {
                                    navitaire.bm.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge adtd = new navitaire.bm.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge();
                                    adtd = svc;
                                    //if (adtd.ChargeType.ToString().ToUpper() != "PROMOTIONDISCOUNT")
                                    if (!adtd.ChargeType.ToString().ToUpper().Contains("DISCOUNT"))
                                    {

                                        if (adtd.ChargeCode.ToString() == "YQ")
                                            objFS.AdtFSur = objFS.AdtFSur + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString() == "WO")
                                            objFS.AdtWO = objFS.AdtWO + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString() == "IN")
                                            objFS.AdtIN = objFS.AdtIN + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        //else if (adtd.ChargeCode.ToString() == "JN")
                                        //    objFS.AdtJN = objFS.AdtJN + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (String.IsNullOrEmpty(adtd.ChargeCode.ToString()) == false && (Utility.Left(adtd.ChargeCode.ToString().ToUpper(), 2) == "CT" || Utility.Left(adtd.ChargeCode.ToString().ToUpper(), 2) == "ST" || Utility.Left(adtd.ChargeCode.ToString().ToUpper(), 2) == "UT" || Utility.Left(adtd.ChargeCode.ToString().ToUpper(), 2) == "IT"))
                                            objFS.AdtJN = objFS.AdtJN + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString() == "YM")
                                            objFS.AdtYR = objFS.AdtYR + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString() == String.Empty)
                                            objFS.AdtBfare = objFS.AdtBfare + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else
                                            objFS.AdtOT = float.Parse(Math.Round(decimal.Parse(objFS.AdtOT.ToString()) + decimal.Parse(adtd.Amount.ToString()), 0).ToString());

                                        objFS.AdtFare = float.Parse(Math.Round(decimal.Parse(objFS.AdtFare.ToString()) + decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        AdtTF = AdtTF + decimal.Parse(adtd.Amount.ToString());
                                    }
                                    else
                                    {
                                        PROMODIS += float.Parse(adtd.Amount.ToString());
                                    }
                                }

                                objFS.AdtBfare = objFS.AdtBfare - PROMODIS;
                                objFS.AdtFare = objFS.AdtFare - PROMODIS;
                                AdtTF = AdtTF - decimal.Parse(PROMODIS.ToString());

                                objFS.AdtTax = objFS.AdtFare - objFS.AdtBfare;

                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    srvChargeAdt = objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType, Convert.ToString(objFS.AdtBfare), Convert.ToString(objFS.AdtFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                }
                                catch { srvChargeAdt = 0; }
                                #endregion
                              
                                //SMS charge add 
                                objFS.AdtOT = objFS.AdtOT + srvChargeAdt;//srvCharge;
                                objFS.AdtTax = objFS.AdtTax +srvChargeAdt; //srvCharge;
                                objFS.AdtFare = objFS.AdtFare + srvChargeAdt; //srvCharge;
                                //SMS charge add end
                                objFS.ADTAdminMrk = 0;// CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                //objFS.ADTAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.AdtFare, searchInputs.Trip.ToString());
                                objFS.ADTAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                {
                                    //objFS.ADTAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.AdtFare, searchInputs.Trip.ToString());
                                    objFS.ADTAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                    objFS.AdtBfare = objFS.AdtBfare + objFS.ADTAdminMrk;
                                    objFS.AdtFare = objFS.AdtFare + objFS.ADTAdminMrk;
                                }
                                CommDt.Clear();
                                STTFTDS.Clear();
                                if ((searchInputs.Trip.ToString() == JourneyType.D.ToString()) && (searchInputs.RTF != true))
                                {
                                    //if ((IdType != "SGTBF") && (IdType != "SGSTR"))
                                    //{
                                    try {
                                        #region Show hide Net Fare and Commission Calculation-Adult Pax
                                        if (objFS.Leg == 1)
                                        {
                                            //Get Commission -Devesh
                                            CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), 1, objFS.AdtRbd, objFS.AdtCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, CrdType, Convert.ToString(TotalViaSector));
                                            if (CommDt != null && CommDt.Rows.Count > 0)
                                            {                                               
                                                objFS.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                                objFS.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.AdtDiscount1, objFS.AdtBfare, objFS.AdtFSur, searchInputs.TDS);
                                                objFS.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                                objFS.AdtDiscount = objFS.AdtDiscount1 - objFS.AdtSrvTax1;
                                                objFS.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                                objFS.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                                objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());

                                                //objFS.AdtDiscount = 0;
                                                //objFS.AdtCB = 0;
                                                //objFS.AdtSrvTax = 0;
                                                //objFS.AdtTF = 0;
                                                //objFS.AdtTds = 0;
                                                //objFS.IATAComm = 0;
                                            }
                                            else
                                            {
                                                objFS.AdtDiscount = 0;
                                                objFS.AdtCB = 0;
                                                objFS.AdtSrvTax = 0;
                                                objFS.AdtTF = 0;
                                                objFS.AdtTds = 0;
                                                objFS.IATAComm = 0;
                                            }

                                        }
                                        else
                                        {
                                            objFS.AdtDiscount = 0;
                                            objFS.AdtCB = 0;
                                            objFS.AdtSrvTax = 0;
                                            objFS.AdtTF = 0;
                                            objFS.AdtTds = 0;
                                            objFS.IATAComm = 0;
                                        }
                                        #endregion                                        
                                    }
                                    catch (Exception ex) {
                                        objFS.AdtDiscount = 0;
                                        objFS.AdtCB = 0;
                                        objFS.AdtSrvTax = 0;
                                        objFS.AdtTF = 0;
                                        objFS.AdtTds = 0;
                                        objFS.IATAComm = 0;
                                    }                                        
                                   // }
                                }
                                else
                                {
                                    objFS.AdtDiscount = 0;
                                    objFS.AdtCB = 0;
                                    objFS.AdtSrvTax = 0;
                                    objFS.AdtTF = 0;
                                    objFS.AdtTds = 0;
                                    objFS.IATAComm = 0;
                                }
                            }
                            if ((searchInputs.IsCorp == true) && (searchInputs.RTF != true))
                            {
                                try
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.AdtFare.ToString()));
                                    objFS.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    objFS.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                                catch { }
                            }
                            objFS.AdtEduCess = 0;
                            objFS.AdtHighEduCess = 0;
                            #endregion

                            #region child
                            objFS.ChdCabin = cbn;// t.COS;
                            objFS.ChdFarebasis = t.FBC;
                            objFS.ChdRbd = t.FCS;
                            //objFS.ChdfareType
                            objFS.ChdTax = 0;
                            if (searchInputs.Child > 0)
                            {
                                var ChdCharges = from pxfare in t.PaxFare[1].ServiceCharges
                                                 where t.PaxFare[1].PaxType == "CHD"
                                                 select pxfare;
                                #region CHDFR
                                foreach (var svc in ChdCharges)
                                {
                                    navitaire.bm.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge chdd = new navitaire.bm.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge();
                                    chdd = svc;
                                    // if (chdd.ChargeType.ToString().ToUpper() != "PROMOTIONDISCOUNT")
                                    if (!chdd.ChargeType.ToString().ToUpper().Contains("DISCOUNT"))
                                    {
                                        if (chdd.ChargeCode.ToString() == "YQ")
                                            objFS.ChdFSur = objFS.ChdFSur + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString() == "WO")
                                            objFS.ChdWO = objFS.ChdWO + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString() == "IN")
                                            objFS.ChdIN = objFS.ChdIN + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        //else if (chdd.ChargeCode.ToString() == "JN")
                                        //    objFS.ChdJN = objFS.ChdJN + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (String.IsNullOrEmpty(chdd.ChargeCode.ToString()) == false && (Utility.Left(chdd.ChargeCode.ToString().ToUpper(), 2) == "CT" || Utility.Left(chdd.ChargeCode.ToString().ToUpper(), 2) == "ST" || Utility.Left(chdd.ChargeCode.ToString().ToUpper(), 2) == "UT" || Utility.Left(chdd.ChargeCode.ToString().ToUpper(), 2) == "IT"))
                                            objFS.ChdJN = objFS.ChdJN + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString() == "YM")
                                            objFS.ChdYR = objFS.ChdYR + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString() == String.Empty)
                                            objFS.ChdBFare = objFS.ChdBFare + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else
                                            objFS.ChdOT = float.Parse(Math.Round(decimal.Parse(objFS.ChdOT.ToString()) + decimal.Parse(chdd.Amount.ToString()), 0).ToString());

                                        objFS.ChdFare = float.Parse(Math.Round(decimal.Parse(objFS.ChdFare.ToString()) + decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        ChdTF = ChdTF + decimal.Parse(chdd.Amount.ToString());
                                    }
                                    else
                                    {
                                        PROMODISCHD += float.Parse(chdd.Amount.ToString());
                                    }
                                }
                                #endregion
                                objFS.ChdBFare = objFS.ChdBFare - PROMODISCHD;
                                objFS.ChdFare = objFS.ChdFare - PROMODISCHD;
                                ChdTF = ChdTF - decimal.Parse(PROMODISCHD.ToString());


                                objFS.ChdTax = objFS.ChdFare - objFS.ChdBFare;
                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    srvChargeChd = objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType, Convert.ToString(objFS.ChdBFare), Convert.ToString(objFS.ChdFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                }
                                catch { srvChargeChd = 0; }
                                #endregion
                                
                                //SMS charge add 
                                objFS.ChdOT = objFS.ChdOT + srvChargeChd;//srvCharge;
                                objFS.ChdTax = objFS.ChdTax + srvChargeChd;//srvCharge;
                                objFS.ChdFare = objFS.ChdFare + srvChargeChd;//srvCharge;
                                //SMS charge add end

                                objFS.CHDAdminMrk = 0;// CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                //objFS.CHDAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.ChdFare, searchInputs.Trip.ToString());
                                objFS.CHDAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                CommDt.Clear();
                                STTFTDS.Clear();
                                if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                {
                                    //objFS.CHDAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.ChdFare, searchInputs.Trip.ToString());
                                    objFS.CHDAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                    objFS.ChdBFare = objFS.ChdBFare + objFS.CHDAdminMrk;
                                    objFS.ChdFare = objFS.ChdFare + objFS.CHDAdminMrk;
                                }
                                if ((searchInputs.Trip.ToString() == JourneyType.D.ToString()) && (searchInputs.RTF != true))
                                {
                                    //if ((IdType != "SGTBF") && (IdType != "SGSTR"))
                                    //{
                                    try
                                    {                                       

                                        #region Show hide Net Fare and Commission Calculation-Child Pax
                                        if (objFS.Leg == 1)
                                        {
                                            CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), 1, objFS.ChdRbd, objFS.ChdCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, CrdType, Convert.ToString(TotalViaSector));
                                            if (CommDt != null && CommDt.Rows.Count > 0)
                                            {
                                                objFS.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                                objFS.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.ChdDiscount1, objFS.ChdBFare, objFS.ChdFSur, searchInputs.TDS);
                                                objFS.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                                objFS.ChdDiscount = objFS.ChdDiscount1 - objFS.ChdSrvTax1;
                                                objFS.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                                objFS.ChdTds = float.Parse(STTFTDS["Tds"].ToString());

                                                //objFS.ChdDiscount = 0;
                                                //objFS.ChdCB = 0;
                                                //objFS.ChdSrvTax = 0;
                                                //objFS.ChdTF = 0;
                                                //objFS.ChdTds = 0;
                                            }
                                            else
                                            {
                                                objFS.ChdDiscount = 0;
                                                objFS.ChdCB = 0;
                                                objFS.ChdSrvTax = 0;
                                                objFS.ChdTF = 0;
                                                objFS.ChdTds = 0;
                                            }

                                        }
                                        else
                                        {
                                            objFS.ChdDiscount = 0;
                                            objFS.ChdCB = 0;
                                            objFS.ChdSrvTax = 0;
                                            objFS.ChdTF = 0;
                                            objFS.ChdTds = 0;
                                        }
                                        #endregion                                        
                                    }
                                    catch (Exception ex)
                                        {
                                        objFS.ChdDiscount = 0;
                                        objFS.ChdCB = 0;
                                        objFS.ChdSrvTax = 0;
                                        objFS.ChdTF = 0;
                                        objFS.ChdTds = 0;
                                    }                                       
                                    //}
                                }
                                else
                                {
                                    objFS.ChdDiscount = 0;
                                    objFS.ChdCB = 0;
                                    objFS.ChdSrvTax = 0;
                                    objFS.ChdTF = 0;
                                    objFS.ChdTds = 0;
                                }
                                if ((searchInputs.IsCorp == true) && (searchInputs.RTF != true))
                                {
                                    try
                                    {
                                        DataTable MGDT = new DataTable();
                                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier + CrdType, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.ChdFare.ToString()));
                                        objFS.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                        objFS.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                    }
                                    catch { }
                                }
                                objFS.ChdEduCess = 0;
                                objFS.ChdHighEduCess = 0;
                                //objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());
                            }
                            else
                            {
                                objFS.ChdBFare = 0;
                                objFS.ChdFare = 0;
                                objFS.ChdFSur = 0;
                                objFS.ChdWO = 0;
                                objFS.ChdIN = 0;
                                objFS.ChdJN = 0;
                                objFS.ChdYR = 0;
                                objFS.ChdOT = 0;
                                objFS.ChdTax = 0;
                                objFS.ChdDiscount = 0;
                                objFS.ChdCB = 0;
                                objFS.ChdSrvTax = 0;
                                objFS.ChdTF = 0;
                                objFS.ChdTds = 0;
                                objFS.ChdEduCess = 0;
                                objFS.ChdHighEduCess = 0;

                            }

                            #endregion

                            #region PROMOCODE  Devesh
                            try
                            {
                                if (PCRow.Count() > 0)
                                {
                                    objFS.ElectronicTicketing = PCRow[0]["D_T_Code"].ToString() + "/" + PROMODIS + "/" + PROMODISCHD + "/" + PCRow[0]["AppliedOn"].ToString() + "/" + PCRow[0]["CodeType"].ToString();
                                }
                                else
                                {
                                    objFS.ElectronicTicketing = "";
                                }
                            }
                            catch (Exception ex)
                            { }
                            #endregion

                            #region Infant
                            if (searchInputs.Infant > 0)
                            {
                                if (t.InfFare > 0)
                                {
                                    if (t.InfFare > t.InfTax)
                                        objFS.InfBfare = float.Parse(Math.Round(t.InfFare - t.InfTax).ToString());
                                    else
                                        objFS.InfBfare = float.Parse(Math.Round(t.InfTax - t.InfFare).ToString());
                                    objFS.InfFare = float.Parse(Math.Round(t.InfFare).ToString());
                                    objFS.InfTax = float.Parse(Math.Round(t.InfTax).ToString());
                                    objFS.InfOT = objFS.InfTax;
                                    InfTF = decimal.Parse(Math.Round(t.InfFare).ToString());
                                }
                                else
                                {
                                    objFS.InfFare = float.Parse(Math.Round(InfantBFare + InfTax).ToString());
                                    objFS.InfBfare = float.Parse(Math.Round(InfantBFare).ToString());
                                    objFS.InfTax = float.Parse(Math.Round(InfTax).ToString());
                                    objFS.InfOT = objFS.InfTax;
                                    InfTF = decimal.Parse(Math.Round(objFS.InfFare).ToString());
                                }
                                objFS.InfFSur = 0;
                                objFS.InfIN = 0;
                                objFS.InfJN = 0;
                                //objFS.InfOT = 0;
                                objFS.InfQ = 0;

                                if (searchInputs.IsCorp == true)
                                {
                                    try
                                    {
                                        DataTable MGDT = new DataTable();
                                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier + CrdType, decimal.Parse(objFS.InfBfare.ToString()), decimal.Parse(objFS.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.InfFare.ToString()));
                                        objFS.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                        objFS.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                    }
                                    catch { }
                                }
                            }
                            else
                            {
                                objFS.InfFare = 0;
                                objFS.InfBfare = 0;
                                objFS.InfFSur = 0;
                                objFS.InfIN = 0;
                                objFS.InfJN = 0;
                                objFS.InfOT = 0;
                                objFS.InfQ = 0;
                            }
                            #endregion
                            objFS.OriginalTF = float.Parse((AdtTF * objFS.Adult + ChdTF * objFS.Child).ToString());//Without Infant Fare
                            objFS.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
                            objFS.TotalTax = (objFS.AdtTax * objFS.Adult) + (objFS.ChdTax * objFS.Child) + (objFS.InfTax * objFS.Infant);
                            objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
                            objFS.TotalFare = (objFS.AdtFare * objFS.Adult) + (objFS.ChdFare * objFS.Child) + (objFS.InfFare * objFS.Infant);
                            objFS.STax = (objFS.AdtSrvTax * objFS.Adult) + (objFS.ChdSrvTax * objFS.Child) + (objFS.InfSrvTax * objFS.Infant);
                            objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
                            objFS.TotDis = (objFS.AdtDiscount * objFS.Adult) + (objFS.ChdDiscount * objFS.Child);
                            objFS.TotCB = (objFS.AdtCB * objFS.Adult) + (objFS.ChdCB * objFS.Child);
                            objFS.TotTds = (objFS.AdtTds * objFS.Adult) + (objFS.ChdTds * objFS.Child);// +objFS.InfTds;
                            objFS.TotMrkUp = (objFS.ADTAdminMrk * objFS.Adult) + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAdminMrk * objFS.Child) + (objFS.CHDAgentMrk * objFS.Child);
                            objFS.TotMgtFee = (objFS.AdtMgtFee * objFS.Adult) + (objFS.ChdMgtFee * objFS.Child) + (objFS.InfMgtFee * objFS.Infant);
                            if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                objFS.TotalFare = objFS.TotalFare + objFS.STax + objFS.TFee + objFS.TotMgtFee + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child);
                            else
                                objFS.TotalFare = objFS.TotalFare + objFS.TotMrkUp + objFS.STax + objFS.TFee + objFS.TotMgtFee;
                            objFS.NetFare = (objFS.TotalFare + objFS.TotTds) - (objFS.TotDis + objFS.TotCB + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child));
                            //objFS.OperatingCarrier = p.opreatingcarrier;
                            objFS.NetFareSRTF = objFS.NetFare;
                            //int hr = int.Parse(p.jrnyTime) / 60;
                            //int min = int.Parse(p.jrnyTime) % 60;
                            //objFS.TotDur = hr + " hrs " + min + " min";

                            objFS.Stops = legs - 1 + "-Stop";

                            objFS.Trip = searchInputs.Trip.ToString();
                            // objFS.FType = searchInputs.TripType.ToString();
                            if ((searchInputs.RTF == true) || (searchInputs.Trip == Trip.I))
                            {
                                if (schd == 0)
                                {
                                    objFS.Flight = "1";
                                    if (searchInputs.RTF == true)
                                        objFS.FType = "RTF";
                                    else
                                        objFS.FType = "OutBound";
                                    objFS.TripType = "O";
                                }
                                else
                                {
                                    objFS.Flight = "2";
                                    if (searchInputs.RTF == true)
                                        objFS.FType = "RTF";
                                    else
                                        objFS.FType = "InBound";
                                    objFS.TripType = "R";
                                }
                            }
                            else
                            {
                                objFS.Flight = "1";
                                objFS.FType = "OutBound";
                                objFS.TripType = "O";
                            }
                            objFS.IsCorp = searchInputs.IsCorp;
                            objFS.Provider = "LCC";
                            srvCharge = srvChargeAdt + srvChargeChd;  //06-03-2018  Add MISC CHARGES PAX WISE
                            objFS.OriginalTT = srvCharge;
                            objFS.SearchId = SearchId;
                            FlightList.Add(objFS);
                        }
                        LNO = LNO + 1;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return FlightList;
        }


        #region 6E Version4
        public List<FlightSearchResults> Spice_GetFltResult(List<FarePriceJourney_6EV4> FareBrk, List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet MrkupDS, FlightSearch searchInputs, int schd, decimal InfantBFare, decimal InfTax, string IdType, List<MISCCharges> MiscList, string VC, string CrdType, DataRow[] PCRow, string SearchId, List<FareTypeSettings> objFareTypeSettings, bool isRTF, bool Bag = false, bool SMEFare = false)
        {
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            string arrCity = "", depCity = "";
            if (schd == 0)
            {
                depCity = Utility.Left(searchInputs.HidTxtDepCity, 3);
                arrCity = Utility.Left(searchInputs.HidTxtArrCity, 3);
            }
            else
            {
                depCity = Utility.Left(searchInputs.HidTxtArrCity, 3);
                arrCity = Utility.Left(searchInputs.HidTxtDepCity, 3);
            }
            List<FlightSearchResults> FlightList = new List<FlightSearchResults>();

            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            List<BaggageList> BagInfoList = new List<BaggageList>();
            BagInfoList = objFltComm.GetBaggages(searchInputs.Trip.ToString(), VC, Bag, Utility.Right(searchInputs.HidTxtDepCity, 2), Utility.Right(searchInputs.HidTxtArrCity, 2));
            //BagInfoList = objFltComm.GetBaggage(searchInputs.Trip.ToString(), VC, Bag);

            string strFlightNo = "";
            string strValidatingCarrier = "";
            string FlttmDur = "";//added by abhilash
            int LNO = 1;

            try
            {
                //if (VC == "SG" && IdType == "SGNRML")
                //    srvCharge = srvCharge - 200;
                //foreach (FarePriceJourney t in FareBrk)
                #region OutBOund -Inbound
                for (int jrny = 0; jrny < FareBrk.Count; jrny++)
                {
                    FarePriceJourney_6EV4 t = new FarePriceJourney_6EV4();
                    t = FareBrk[jrny];
                    try
                    {
                        #region Get TotalViaSector for Commission PPPSector and PPPSegment
                        int TotalViaSector = 1;
                        try
                        {
                            TotalViaSector = t.Leg.Select(x => x["FlightNumber"]).Distinct().Count();
                        }
                        catch (Exception ex)
                        {
                            TotalViaSector = 1;
                        }
                        #endregion

                        int legs = t.Leg.Length;
                        for (int i = 0; i < legs; i++)
                        {
                            FlightSearchResults objFS = new FlightSearchResults();
                            decimal AdtTF = 0, ChdTF = 0, InfTF = 0;//For Total Fare Per Pax Type
                            #region Flight Details
                            objFS.OrgDestFrom = depCity;
                            objFS.OrgDestTo = arrCity;
                            objFS.DepartureLocation = t.Leg[i]["DepartureStation"];
                            objFS.DepAirportCode = t.Leg[i]["DepartureStation"];
                            objFS.DepartureTerminal = t.Leg[i]["DepartureTerminal"];
                            objFS.DepartureCityName = getCityName(objFS.DepartureLocation, CityList);
                            objFS.ArrivalLocation = t.Leg[i]["ArrivalStation"];
                            objFS.ArrAirportCode = t.Leg[i]["ArrivalStation"];
                            objFS.ArrivalTerminal = t.Leg[i]["ArrivalTerminal"];
                            objFS.ArrivalCityName = getCityName(objFS.ArrivalLocation, CityList);
                            try
                            {
                                objFS.DepartureAirportName = ((from ct in CityList where ct.AirportCode == objFS.DepartureLocation select ct).ToList())[0].AirportName;
                                objFS.ArrivalAirportName = ((from ct in CityList where ct.AirportCode == objFS.ArrivalLocation select ct).ToList())[0].AirportName;
                            }
                            catch (Exception ex)
                            {
                                objFS.DepartureAirportName = t.Leg[i]["DepartureStation"];
                                objFS.ArrivalAirportName = t.Leg[i]["ArrivalStation"];
                            }
                            string[] Dep = Utility.Split(Utility.Split(t.Leg[i]["STD"], "T")[0], "-");
                            objFS.DepartureDate = Dep[2] + Dep[1] + Utility.Right(Dep[0], 2);
                            objFS.Departure_Date = Dep[2] + " " + Utility.datecon(Dep[1]);
                            objFS.DepartureTime = Utility.Left(Utility.Split(t.Leg[i]["STD"], "T")[1], 5);

                            string[] Arr = Utility.Split(Utility.Split(t.Leg[i]["STA"], "T")[0], "-");
                            objFS.ArrivalDate = Arr[2] + Arr[1] + Utility.Right(Arr[0], 2);
                            objFS.Arrival_Date = Arr[2] + " " + Utility.datecon(Arr[1]);
                            objFS.ArrivalTime = Utility.Left(Utility.Split(t.Leg[i]["STA"], "T")[1], 5);
                            try
                            {
                                if (t.Leg.Length > 1)
                                {
                                    //DateTime DDTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[0]["STD"], "T")[1], 5));
                                    //DateTime ADTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[legs - 1]["STA"], "T")[1], 5));

                                    //TimeSpan value = ADTime - DDTime;
                                    //string label = string.Format("{0:hh\\:mm\\:ss}", value);
                                    //objFS.TotDur = Utility.Left(label, 5);
                                    objFS.TotDur = Utility.Left(Convert.ToDateTime(t.Leg[legs - 1]["STA"]).Subtract(Convert.ToDateTime(t.Leg[0]["STD"])).ToString(), 5);
                                }
                                else
                                {

                                    //DateTime DDTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[i]["STD"], "T")[1], 5));
                                    //DateTime ADTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[i]["STA"], "T")[1], 5));

                                    //TimeSpan value = ADTime - DDTime;
                                    //string label = string.Format("{0:hh\\:mm\\:ss}", value);
                                    //objFS.TotDur = Utility.Left(label, 5);
                                    objFS.TotDur = objFS.TotDur = Utility.Left(Convert.ToDateTime(t.Leg[i]["STA"]).Subtract(Convert.ToDateTime(t.Leg[i]["STD"])).ToString(), 5);

                                }
                            }
                            catch { objFS.TotDur = ""; }
                            objFS.Adult = searchInputs.Adult;
                            objFS.Child = searchInputs.Child;
                            objFS.Infant = searchInputs.Infant;
                            objFS.TotPax = searchInputs.Adult + searchInputs.Child;
                            objFS.MarketingCarrier = VC;
                            objFS.OperatingCarrier = VC;
                            objFS.FlightIdentification = t.Leg[i]["FlightNumber"];
                            objFS.ValiDatingCarrier = VC;
                            if (VC == "SG")
                                objFS.AirLineName = "SpiceJet";
                            else
                                objFS.AirLineName = "Indigo";
                            objFS.AvailableSeats = IdType;
                            objFS.AvailableSeats1 = t.AVLCNT;
                            if ((t.PCS == "HB") && (VC == "SG") && (("P1,P2").Contains(t.FCS)))
                                objFS.BagInfo = "7kg Hand Bag Only";
                            else
                                objFS.BagInfo = ((from bg in BagInfoList where bg.Class == "Economy" select bg).ToList())[0].Weight;

                            #region Baggage
                            objFS.IsBagFare = Bag;
                            if (objFareTypeSettings != null && objFareTypeSettings.Count > 0 && objFareTypeSettings[0].SSRCode != null)
                                objFS.SSRCode = objFareTypeSettings[0].SSRCode;
                            #endregion
                            #region SME FARE        
                            objFS.IsSMEFare = SMEFare;
                            #endregion
                            //objFS.TotDur = "";
                            objFS.arrdatelcc = t.Leg[i]["STA"];
                            objFS.depdatelcc = t.Leg[i]["STD"];

                            if (searchInputs.Trip == Trip.I)
                            {
                                if (schd == 0)
                                {
                                    if (searchInputs.TripType == STD.Shared.TripType.RoundTrip)
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo + ":" + objFS.OrgDestFrom;
                                    else
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                }
                                else
                                {
                                    if (searchInputs.TripType == STD.Shared.TripType.RoundTrip)
                                        objFS.Sector = objFS.OrgDestTo + ":" + objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                    else
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                }
                            }
                            else
                            {
                                if (schd == 0)
                                {
                                    if (searchInputs.RTF == true)
                                    { objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo + ":" + objFS.OrgDestFrom; }
                                    else
                                    { objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo; }
                                }
                                else
                                {
                                    if (searchInputs.RTF == true)
                                        objFS.Sector = objFS.OrgDestTo + ":" + objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                    else
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                }
                            }

                            objFS.EQ = t.Leg[i]["EQType"];
                            objFS.LineNumber = LNO;
                            // objFS.Ln = LNO;
                            objFS.Leg = (i + 1);
                            objFS.fareBasis = t.FBC;
                            objFS.RBD = t.FCS;
                            objFS.FareRule = t.FSK; //Fare Sell Key
                            objFS.Searchvalue = t.FSK; //Fare Sell Key
                            objFS.sno = t.JSK; //Journey Sell Key
                            #endregion

                            ////SMS charge calc
                            float srvCharge = 0;
                            float srvChargeAdt = 0;
                            float srvChargeChd = 0;

                            //float srvCharge = 0;
                            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID);
                            ////SMS charge calc end
                            try
                            {
                                #region Adult
                                //objFS.AdtFar = IdType;
                                objFS.AdtFar = CrdType;
                                objFS.AdtCabin = "E";// t.COS;
                                objFS.AdtFarebasis = t.FBC;
                                objFS.AdtRbd = t.FCS;
                                #region ADTFR
                                if (IdType != "SGTBF")
                                {
                                    if ((t.PCS == "NN") && (VC == "SG") && (("F,G,H,I,J,K").Contains(t.FCS)))
                                        objFS.AdtFareType = "Non Refundable";
                                    else if ((t.PCS == "AB") && (VC == "SG") && (("A,B").Contains(t.FCS)))
                                        objFS.AdtFareType = "Non Refundable";
                                    else if ((VC == "6E") && (t.PCS == "S"))//(t.FBC == "P4SALE") &&
                                        objFS.AdtFareType = "Non Refundable";
                                    else if (VC == "SG" && (t.FSK.Contains("AP7") || t.FSK.Contains("AP14")))
                                        objFS.AdtFareType = "Non Refundable";
                                    else if ((t.PCS == "HB") && (VC == "SG") && (("P1,P2").Contains(t.FCS)))
                                        objFS.AdtFareType = "Non Refundable";
                                    else if ((t.PCS == "NF") && (VC == "SG") && (("P1,P2").Contains(t.FCS)))
                                        objFS.AdtFareType = "Non Refundable";
                                    else
                                        objFS.AdtFareType = "Refundable";
                                }
                                else if (IdType == "SGSTR")
                                { objFS.AdtFareType = "Spl. Fare, No Commission<br/>Refundable, Meal Included"; }
                                else
                                { objFS.AdtFareType = "Spl. Fare, No Commission<br/>Refundable"; }

                                //FareSellKey , JSK
                                #endregion

                                float PROMODISCHD = 0;
                                float PROMODIS = 0;
                                //Adt Fare Details 
                                if (searchInputs.Adult > 0)
                                {
                                    int m = 0;
                                    var AdtCharges = from ServiceCharges in t.PaxFare[0].ServiceCharges
                                                     where t.PaxFare[0].PaxType == "ADT"
                                                     select ServiceCharges;




                                    foreach (var svc in AdtCharges)
                                    {
                                        navitaire.indigo.bm.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge adtd = new navitaire.indigo.bm.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge();
                                        adtd = svc;
                                        //if (adtd.ChargeType.ToString().ToUpper() != "PROMOTIONDISCOUNT")
                                        if (!adtd.ChargeType.ToString().ToUpper().Contains("DISCOUNT"))
                                        {

                                            if (adtd.ChargeCode.ToString() == "YQ")
                                                objFS.AdtFSur = objFS.AdtFSur + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                            else if (adtd.ChargeCode.ToString() == "WO")
                                                objFS.AdtWO = objFS.AdtWO + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                            else if (adtd.ChargeCode.ToString() == "IN")
                                                objFS.AdtIN = objFS.AdtIN + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                            //else if (adtd.ChargeCode.ToString() == "JN")
                                            //    objFS.AdtJN = objFS.AdtJN + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                            else if (adtd.ChargeCode.ToString().ToUpper().Contains("CGST") || adtd.ChargeCode.ToString().ToUpper().Contains("SGST") || adtd.ChargeCode.ToString().ToUpper().Contains("UTGST") || adtd.ChargeCode.ToString().ToUpper().Contains("IGST"))
                                                objFS.AdtJN = objFS.AdtJN + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                            else if (adtd.ChargeCode.ToString() == "YM")
                                                objFS.AdtYR = objFS.AdtYR + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                            else if (adtd.ChargeCode.ToString() == String.Empty)
                                                objFS.AdtBfare = objFS.AdtBfare + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                            else
                                                objFS.AdtOT = float.Parse(Math.Round(decimal.Parse(objFS.AdtOT.ToString()) + decimal.Parse(adtd.Amount.ToString()), 0).ToString());

                                            objFS.AdtFare = float.Parse(Math.Round(decimal.Parse(objFS.AdtFare.ToString()) + decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                            AdtTF = AdtTF + decimal.Parse(adtd.Amount.ToString());
                                        }
                                        else
                                        {
                                            PROMODIS += float.Parse(adtd.Amount.ToString());
                                        }
                                    }

                                    objFS.AdtBfare = objFS.AdtBfare - PROMODIS;
                                    objFS.AdtFare = objFS.AdtFare - PROMODIS;
                                    AdtTF = AdtTF - decimal.Parse(PROMODIS.ToString());

                                    objFS.AdtTax = objFS.AdtFare - objFS.AdtBfare;

                                    // float srvChargeAdt = 0;
                                    #region Get MISC Markup Charges Date 06-03-2018
                                    try
                                    {
                                        srvChargeAdt = objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType, Convert.ToString(objFS.AdtBfare), Convert.ToString(objFS.AdtFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                    }
                                    catch { srvChargeAdt = 0; }
                                    #endregion


                                    //SMS charge add 
                                    objFS.AdtOT = objFS.AdtOT + srvChargeAdt;//srvCharge;
                                    objFS.AdtTax = objFS.AdtTax + srvChargeAdt;//srvCharge;
                                    objFS.AdtFare = objFS.AdtFare + srvChargeAdt; //srvCharge;
                                                                                  //SMS charge add end
                                    objFS.ADTAdminMrk = 0;// CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                                          //objFS.ADTAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.AdtFare, searchInputs.Trip.ToString());
                                    objFS.ADTAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                    if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                    {
                                        //objFS.ADTAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.AdtFare, searchInputs.Trip.ToString());
                                        objFS.ADTAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                        objFS.AdtBfare = objFS.AdtBfare + objFS.ADTAdminMrk;
                                        objFS.AdtFare = objFS.AdtFare + objFS.ADTAdminMrk;
                                    }
                                    CommDt.Clear();
                                    STTFTDS.Clear();
                                    if ((searchInputs.Trip.ToString() == JourneyType.D.ToString()) && (searchInputs.RTF != true)) //&& (searchInputs.RTF != true)
                                    {
                                        //if ((IdType != "SGTBF") && (IdType != "SGSTR"))
                                        //{
                                        try
                                        {

                                            #region Show hide Net Fare and Commission Calculation-Adult Pax
                                            if (objFS.Leg == 1)
                                            {
                                                CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), 1, objFS.AdtRbd, objFS.AdtCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, CrdType, Convert.ToString(TotalViaSector));
                                                if (CommDt != null && CommDt.Rows.Count > 0)
                                                {                                                   
                                                    objFS.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                                    objFS.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                    STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.AdtDiscount1, objFS.AdtBfare, objFS.AdtFSur, searchInputs.TDS);
                                                    objFS.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                                    objFS.AdtDiscount = objFS.AdtDiscount1 - objFS.AdtSrvTax1;
                                                    objFS.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                                    objFS.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                                    objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());

                                                    //objFS.AdtDiscount = 0;
                                                    //objFS.AdtCB = 0;
                                                    //objFS.AdtSrvTax = 0;
                                                    //objFS.AdtTF = 0;
                                                    //objFS.AdtTds = 0;
                                                    //objFS.IATAComm = 0;
                                                }
                                                else
                                                {
                                                    objFS.AdtDiscount = 0;
                                                    objFS.AdtCB = 0;
                                                    objFS.AdtSrvTax = 0;
                                                    objFS.AdtTF = 0;
                                                    objFS.AdtTds = 0;
                                                    objFS.IATAComm = 0;
                                                }

                                            }
                                            else
                                            {
                                                objFS.AdtDiscount = 0;
                                                objFS.AdtCB = 0;
                                                objFS.AdtSrvTax = 0;
                                                objFS.AdtTF = 0;
                                                objFS.AdtTds = 0;
                                                objFS.IATAComm = 0;
                                            }
                                            #endregion
                                            
                                        }
                                        catch (Exception ex)
                                        {
                                            objFS.AdtDiscount = 0;
                                            objFS.AdtCB = 0;
                                            objFS.AdtSrvTax = 0;
                                            objFS.AdtTF = 0;
                                            objFS.AdtTds = 0;
                                            objFS.IATAComm = 0;
                                        }
                                        //}
                                    }
                                    else
                                    {
                                        objFS.AdtDiscount = 0;
                                        objFS.AdtCB = 0;
                                        objFS.AdtSrvTax = 0;
                                        objFS.AdtTF = 0;
                                        objFS.AdtTds = 0;
                                        objFS.IATAComm = 0;
                                    }
                                }
                                if ((searchInputs.IsCorp == true) && (searchInputs.RTF != true))
                                {
                                    try
                                    {
                                        DataTable MGDT = new DataTable();
                                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.AdtFare.ToString()));
                                        objFS.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                        objFS.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                    }
                                    catch { }
                                }
                                objFS.AdtEduCess = 0;
                                objFS.AdtHighEduCess = 0;
                                #endregion

                                #region child
                                objFS.ChdCabin = t.COS;
                                objFS.ChdFarebasis = t.FBC;
                                objFS.ChdRbd = t.FCS;
                                //objFS.ChdfareType
                                objFS.ChdTax = 0;
                                if (searchInputs.Child > 0)
                                {
                                    var ChdCharges = from pxfare in t.PaxFare[1].ServiceCharges
                                                     where t.PaxFare[1].PaxType == "CHD"
                                                     select pxfare;
                                    #region CHDFR
                                    foreach (var svc in ChdCharges)
                                    {
                                        navitaire.indigo.bm.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge chdd = new navitaire.indigo.bm.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge();
                                        chdd = svc;
                                        //if (chdd.ChargeType.ToString().ToUpper() != "PROMOTIONDISCOUNT")
                                        if (!chdd.ChargeType.ToString().ToUpper().Contains("DISCOUNT"))
                                        {
                                            if (chdd.ChargeCode.ToString() == "YQ")
                                                objFS.ChdFSur = objFS.ChdFSur + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                            else if (chdd.ChargeCode.ToString() == "WO")
                                                objFS.ChdWO = objFS.ChdWO + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                            else if (chdd.ChargeCode.ToString() == "IN")
                                                objFS.ChdIN = objFS.ChdIN + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                            //else if (chdd.ChargeCode.ToString() == "JN")
                                            //    objFS.ChdJN = objFS.ChdJN + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                            else if (chdd.ChargeCode.ToString().ToUpper().Contains("CGST") || chdd.ChargeCode.ToString().ToUpper().Contains("SGST") || chdd.ChargeCode.ToString().ToUpper().Contains("UTGST") || chdd.ChargeCode.ToString().ToUpper().Contains("IGST"))
                                                objFS.ChdJN = objFS.ChdJN + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                            else if (chdd.ChargeCode.ToString() == "YM")
                                                objFS.ChdYR = objFS.ChdYR + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                            else if (chdd.ChargeCode.ToString() == String.Empty)
                                                objFS.ChdBFare = objFS.ChdBFare + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                            else
                                                objFS.ChdOT = float.Parse(Math.Round(decimal.Parse(objFS.ChdOT.ToString()) + decimal.Parse(chdd.Amount.ToString()), 0).ToString());

                                            objFS.ChdFare = float.Parse(Math.Round(decimal.Parse(objFS.ChdFare.ToString()) + decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                            ChdTF = ChdTF + decimal.Parse(chdd.Amount.ToString());
                                        }
                                        else
                                        {
                                            PROMODISCHD += float.Parse(chdd.Amount.ToString());
                                        }
                                    }
                                    #endregion
                                    objFS.ChdBFare = objFS.ChdBFare - PROMODISCHD;
                                    objFS.ChdFare = objFS.ChdFare - PROMODISCHD;
                                    ChdTF = ChdTF - decimal.Parse(PROMODISCHD.ToString());


                                    objFS.ChdTax = objFS.ChdFare - objFS.ChdBFare;
                                    #region Get MISC Markup Charges Date 06-03-2018
                                    try
                                    {
                                        srvChargeChd = objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType, Convert.ToString(objFS.ChdBFare), Convert.ToString(objFS.ChdFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                    }
                                    catch { srvChargeChd = 0; }
                                    #endregion


                                    //SMS charge add 
                                    objFS.ChdOT = objFS.ChdOT + srvChargeChd;//srvCharge;
                                    objFS.ChdTax = objFS.ChdTax + srvChargeChd;//srvCharge;
                                    objFS.ChdFare = objFS.ChdFare + srvChargeChd; //srvCharge;
                                                                                  //SMS charge add end

                                    objFS.CHDAdminMrk = 0;// CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                                          // objFS.CHDAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.ChdFare, searchInputs.Trip.ToString());
                                    objFS.CHDAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                    CommDt.Clear();
                                    STTFTDS.Clear();
                                    if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                    {
                                        //.CHDAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.ChdFare, searchInputs.Trip.ToString());
                                        objFS.CHDAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                        objFS.ChdBFare = objFS.ChdBFare + objFS.CHDAdminMrk;
                                        objFS.ChdFare = objFS.ChdFare + objFS.CHDAdminMrk;
                                    }
                                    if ((searchInputs.Trip.ToString() == JourneyType.D.ToString()) && (searchInputs.RTF != true))
                                    {
                                        //if ((IdType != "SGTBF") && (IdType != "SGSTR"))
                                        //{
                                        try
                                        {
                                            #region Show hide Net Fare and Commission Calculation-Child Pax
                                            if (objFS.Leg == 1)
                                            {
                                                CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), 1, objFS.ChdRbd, objFS.ChdCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, CrdType, Convert.ToString(TotalViaSector));
                                                if (CommDt != null && CommDt.Rows.Count > 0)
                                                {                                                    
                                                    objFS.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                                    objFS.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                    STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.ChdDiscount1, objFS.ChdBFare, objFS.ChdFSur, searchInputs.TDS);
                                                    objFS.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                                    objFS.ChdDiscount = objFS.ChdDiscount1 - objFS.ChdSrvTax1;
                                                    objFS.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                                    objFS.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                                    //objFS.ChdDiscount = 0;
                                                    //objFS.ChdCB = 0;
                                                    //objFS.ChdSrvTax = 0;
                                                    //objFS.ChdTF = 0;
                                                    //objFS.ChdTds = 0;
                                                }
                                                else
                                                {
                                                    objFS.ChdDiscount = 0;
                                                    objFS.ChdCB = 0;
                                                    objFS.ChdSrvTax = 0;
                                                    objFS.ChdTF = 0;
                                                    objFS.ChdTds = 0;
                                                }

                                            }
                                            else
                                            {
                                                objFS.ChdDiscount = 0;
                                                objFS.ChdCB = 0;
                                                objFS.ChdSrvTax = 0;
                                                objFS.ChdTF = 0;
                                                objFS.ChdTds = 0;
                                            }
                                            #endregion                                           
                                        }
                                        catch (Exception ex)
                                        {
                                            objFS.ChdDiscount = 0;
                                            objFS.ChdCB = 0;
                                            objFS.ChdSrvTax = 0;
                                            objFS.ChdTF = 0;
                                            objFS.ChdTds = 0;
                                        }
                                        // }
                                    }
                                    else
                                    {
                                        objFS.ChdDiscount = 0;
                                        objFS.ChdCB = 0;
                                        objFS.ChdSrvTax = 0;
                                        objFS.ChdTF = 0;
                                        objFS.ChdTds = 0;
                                    }
                                    if ((searchInputs.IsCorp == true) && (searchInputs.RTF != true))
                                    {
                                        try
                                        {
                                            DataTable MGDT = new DataTable();
                                            MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier + CrdType, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.ChdFare.ToString()));
                                            objFS.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                            objFS.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                        }
                                        catch { }
                                    }
                                    objFS.ChdEduCess = 0;
                                    objFS.ChdHighEduCess = 0;
                                    //objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());
                                }
                                else
                                {
                                    objFS.ChdBFare = 0;
                                    objFS.ChdFare = 0;
                                    objFS.ChdFSur = 0;
                                    objFS.ChdWO = 0;
                                    objFS.ChdIN = 0;
                                    objFS.ChdJN = 0;
                                    objFS.ChdYR = 0;
                                    objFS.ChdOT = 0;
                                    objFS.ChdTax = 0;
                                    objFS.ChdDiscount = 0;
                                    objFS.ChdCB = 0;
                                    objFS.ChdSrvTax = 0;
                                    objFS.ChdTF = 0;
                                    objFS.ChdTds = 0;
                                    objFS.ChdEduCess = 0;
                                    objFS.ChdHighEduCess = 0;

                                }

                                #endregion

                                #region PROMOCODE  Devesh
                                try
                                {
                                    if (PCRow.Count() > 0)
                                    {
                                        objFS.ElectronicTicketing = PCRow[0]["D_T_Code"].ToString() + "/" + PROMODIS + "/" + PROMODISCHD + "/" + PCRow[0]["AppliedOn"].ToString() + "/" + PCRow[0]["CodeType"].ToString();
                                    }
                                    else
                                    {
                                        objFS.ElectronicTicketing = "";
                                    }
                                }
                                catch (Exception ex)
                                { }
                                #endregion

                                #region Infant
                                if (searchInputs.Infant > 0)
                                {
                                    if (t.InfFare > 0)
                                    {
                                        if (t.InfFare > t.InfTax)
                                            objFS.InfBfare = float.Parse(Math.Round(t.InfFare - t.InfTax).ToString());
                                        else
                                            objFS.InfBfare = float.Parse(Math.Round(t.InfTax - t.InfFare).ToString());
                                        objFS.InfFare = float.Parse(Math.Round(t.InfFare).ToString());
                                        objFS.InfTax = float.Parse(Math.Round(t.InfTax).ToString());
                                        objFS.InfOT = objFS.InfTax;
                                        InfTF = decimal.Parse(Math.Round(t.InfFare).ToString());
                                    }
                                    else
                                    {
                                        objFS.InfFare = float.Parse(Math.Round(InfantBFare + InfTax).ToString());
                                        objFS.InfBfare = float.Parse(Math.Round(InfantBFare).ToString());
                                        objFS.InfTax = float.Parse(Math.Round(InfTax).ToString());
                                        objFS.InfOT = objFS.InfTax;
                                        InfTF = decimal.Parse(Math.Round(objFS.InfFare).ToString());
                                    }
                                    objFS.InfFSur = 0;
                                    objFS.InfIN = 0;
                                    objFS.InfJN = 0;
                                    //objFS.InfOT = 0;
                                    objFS.InfQ = 0;

                                    if (searchInputs.IsCorp == true)
                                    {
                                        try
                                        {
                                            DataTable MGDT = new DataTable();
                                            MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier + CrdType, decimal.Parse(objFS.InfBfare.ToString()), decimal.Parse(objFS.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.InfFare.ToString()));
                                            objFS.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                            objFS.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                        }
                                        catch { }
                                    }
                                }
                                else
                                {
                                    objFS.InfFare = 0;
                                    objFS.InfBfare = 0;
                                    objFS.InfFSur = 0;
                                    objFS.InfIN = 0;
                                    objFS.InfJN = 0;
                                    objFS.InfOT = 0;
                                    objFS.InfQ = 0;
                                }
                                #endregion
                            }
                            catch (Exception ex)
                            {

                            }

                            objFS.OriginalTF = float.Parse((AdtTF * objFS.Adult + ChdTF * objFS.Child).ToString());//Without Infant Fare
                            objFS.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
                            objFS.TotalTax = (objFS.AdtTax * objFS.Adult) + (objFS.ChdTax * objFS.Child) + (objFS.InfTax * objFS.Infant);
                            objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
                            objFS.TotalFare = (objFS.AdtFare * objFS.Adult) + (objFS.ChdFare * objFS.Child) + (objFS.InfFare * objFS.Infant);
                            objFS.STax = (objFS.AdtSrvTax * objFS.Adult) + (objFS.ChdSrvTax * objFS.Child) + (objFS.InfSrvTax * objFS.Infant);
                            objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
                            objFS.TotDis = (objFS.AdtDiscount * objFS.Adult) + (objFS.ChdDiscount * objFS.Child);
                            objFS.TotCB = (objFS.AdtCB * objFS.Adult) + (objFS.ChdCB * objFS.Child);
                            objFS.TotTds = (objFS.AdtTds * objFS.Adult) + (objFS.ChdTds * objFS.Child);// +objFS.InfTds;
                            objFS.TotMrkUp = (objFS.ADTAdminMrk * objFS.Adult) + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAdminMrk * objFS.Child) + (objFS.CHDAgentMrk * objFS.Child);
                            objFS.TotMgtFee = (objFS.AdtMgtFee * objFS.Adult) + (objFS.ChdMgtFee * objFS.Child) + (objFS.InfMgtFee * objFS.Infant);
                            if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                objFS.TotalFare = objFS.TotalFare + objFS.STax + objFS.TFee + objFS.TotMgtFee + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child);
                            else
                                objFS.TotalFare = objFS.TotalFare + objFS.TotMrkUp + objFS.STax + objFS.TFee + objFS.TotMgtFee;
                            objFS.NetFare = (objFS.TotalFare + objFS.TotTds) - (objFS.TotDis + objFS.TotCB + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child));
                            //objFS.OperatingCarrier = p.opreatingcarrier;
                            objFS.NetFareSRTF = objFS.NetFare;
                            //int hr = int.Parse(p.jrnyTime) / 60;
                            //int min = int.Parse(p.jrnyTime) % 60;
                            //objFS.TotDur = hr + " hrs " + min + " min";

                            objFS.Stops = legs - 1 + "-Stop";

                            objFS.Trip = searchInputs.Trip.ToString();
                            // objFS.FType = searchInputs.TripType.ToString();
                            if ((searchInputs.RTF == true) || (searchInputs.Trip == Trip.I))
                            {
                                if (schd == 0)
                                {
                                    objFS.Flight = "1";
                                    if (searchInputs.RTF == true)
                                        objFS.FType = "RTF";
                                    else
                                        objFS.FType = "OutBound";
                                    objFS.TripType = "O";
                                }
                                else
                                {
                                    objFS.Flight = "2";
                                    if (searchInputs.RTF == true)
                                        objFS.FType = "RTF";
                                    else
                                        objFS.FType = "InBound";
                                    objFS.TripType = "R";
                                }
                            }
                            else
                            {
                                objFS.Flight = "1";
                                objFS.FType = "OutBound";
                                objFS.TripType = "O";
                            }
                            objFS.IsCorp = searchInputs.IsCorp;
                            objFS.Provider = "LCC";
                            srvCharge = srvChargeAdt + srvChargeChd;  //06-03-2018  Add MISC CHARGES PAX WISE
                            objFS.OriginalTT = srvCharge;
                            objFS.SearchId = SearchId;
                            FlightList.Add(objFS);
                        }
                        LNO = LNO + 1;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return FlightList;
        }

        public List<FlightSearchResults> Spice_GetFltResult(List<FarePriceJourney_6EV4> FareBrk, List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet MrkupDS, FlightSearch searchInputs, int schd, decimal InfantBFare, decimal InfTax, string IdType, List<MISCCharges> MiscList, string VC, string CrdType, DataRow[] PCRow, string SearchId, List<FareTypeSettings> objFareTypeSettings, bool Bag = false, bool SMEFare = false)
        {
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            string arrCity = "", depCity = "";
            if (schd == 0)
            {
                depCity = Utility.Left(searchInputs.HidTxtDepCity, 3);
                arrCity = Utility.Left(searchInputs.HidTxtArrCity, 3);
            }
            else
            {
                depCity = Utility.Left(searchInputs.HidTxtArrCity, 3);
                arrCity = Utility.Left(searchInputs.HidTxtDepCity, 3);
            }
            List<FlightSearchResults> FlightList = new List<FlightSearchResults>();

            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            List<BaggageList> BagInfoList = new List<BaggageList>();
            BagInfoList = objFltComm.GetBaggages(searchInputs.Trip.ToString(), VC, Bag, Utility.Right(searchInputs.HidTxtDepCity, 2), Utility.Right(searchInputs.HidTxtArrCity, 2));
            //BagInfoList = objFltComm.GetBaggage(searchInputs.Trip.ToString(), VC, Bag);

            string strFlightNo = "";
            string strValidatingCarrier = "";
            string FlttmDur = "";//added by abhilash
            int LNO = 1;

            try
            {
                //if (VC == "SG" && IdType == "SGNRML")
                //    srvCharge = srvCharge - 200;
                //foreach (FarePriceJourney t in FareBrk)
                #region OutBOund -Inbound
                for (int jrny = 0; jrny < FareBrk.Count; jrny++)
                {
                    FarePriceJourney_6EV4 t = new FarePriceJourney_6EV4();
                    t = FareBrk[jrny];
                    try
                    {
                        #region Get TotalViaSector for Commission PPPSector and PPPSegment
                        int TotalViaSector = 1;
                        try
                        {
                            TotalViaSector = t.Leg.Select(x => x["FlightNumber"]).Distinct().Count();
                        }
                        catch (Exception ex)
                        {
                            TotalViaSector = 1;
                        }
                        #endregion
                        int legs = t.Leg.Length;
                        for (int i = 0; i < legs; i++)
                        {
                            FlightSearchResults objFS = new FlightSearchResults();
                            decimal AdtTF = 0, ChdTF = 0, InfTF = 0;//For Total Fare Per Pax Type
                            #region Flight Details
                            objFS.OrgDestFrom = depCity;
                            objFS.OrgDestTo = arrCity;
                            objFS.DepartureLocation = t.Leg[i]["DepartureStation"];
                            objFS.DepAirportCode = t.Leg[i]["DepartureStation"];
                            objFS.DepartureTerminal = t.Leg[i]["DepartureTerminal"];
                            objFS.DepartureCityName = getCityName(objFS.DepartureLocation, CityList);
                            objFS.ArrivalLocation = t.Leg[i]["ArrivalStation"];
                            objFS.ArrAirportCode = t.Leg[i]["ArrivalStation"];
                            objFS.ArrivalTerminal = t.Leg[i]["ArrivalTerminal"];
                            objFS.ArrivalCityName = getCityName(objFS.ArrivalLocation, CityList);
                            try
                            {
                                objFS.DepartureAirportName = ((from ct in CityList where ct.AirportCode == objFS.DepartureLocation select ct).ToList())[0].AirportName;
                                objFS.ArrivalAirportName = ((from ct in CityList where ct.AirportCode == objFS.ArrivalLocation select ct).ToList())[0].AirportName;
                            }
                            catch (Exception ex)
                            {
                                objFS.DepartureAirportName = t.Leg[i]["DepartureStation"];
                                objFS.ArrivalAirportName = t.Leg[i]["ArrivalStation"];
                            }
                            string[] Dep = Utility.Split(Utility.Split(t.Leg[i]["STD"], "T")[0], "-");
                            objFS.DepartureDate = Dep[2] + Dep[1] + Utility.Right(Dep[0], 2);
                            objFS.Departure_Date = Dep[2] + " " + Utility.datecon(Dep[1]);
                            objFS.DepartureTime = Utility.Left(Utility.Split(t.Leg[i]["STD"], "T")[1], 5);

                            string[] Arr = Utility.Split(Utility.Split(t.Leg[i]["STA"], "T")[0], "-");
                            objFS.ArrivalDate = Arr[2] + Arr[1] + Utility.Right(Arr[0], 2);
                            objFS.Arrival_Date = Arr[2] + " " + Utility.datecon(Arr[1]);
                            objFS.ArrivalTime = Utility.Left(Utility.Split(t.Leg[i]["STA"], "T")[1], 5);
                            try
                            {
                                if (t.Leg.Length > 1)
                                {
                                    //DateTime DDTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[0]["STD"], "T")[1], 5));
                                    //DateTime ADTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[legs - 1]["STA"], "T")[1], 5));

                                    //TimeSpan value = ADTime - DDTime;
                                    //string label = string.Format("{0:hh\\:mm\\:ss}", value);
                                    //objFS.TotDur = Utility.Left(label, 5);
                                    objFS.TotDur = Utility.Left(Convert.ToDateTime(t.Leg[legs - 1]["STA"]).Subtract(Convert.ToDateTime(t.Leg[0]["STD"])).ToString(), 5);
                                }
                                else
                                {

                                    //DateTime DDTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[i]["STD"], "T")[1], 5));
                                    //DateTime ADTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[i]["STA"], "T")[1], 5));

                                    //TimeSpan value = ADTime - DDTime;
                                    //string label = string.Format("{0:hh\\:mm\\:ss}", value);
                                    //objFS.TotDur = Utility.Left(label, 5);
                                    objFS.TotDur = objFS.TotDur = Utility.Left(Convert.ToDateTime(t.Leg[i]["STA"]).Subtract(Convert.ToDateTime(t.Leg[i]["STD"])).ToString(), 5);

                                }
                            }
                            catch { objFS.TotDur = ""; }
                            objFS.Adult = searchInputs.Adult;
                            objFS.Child = searchInputs.Child;
                            objFS.Infant = searchInputs.Infant;
                            objFS.TotPax = searchInputs.Adult + searchInputs.Child;
                            objFS.MarketingCarrier = VC;
                            objFS.OperatingCarrier = VC;
                            objFS.FlightIdentification = t.Leg[i]["FlightNumber"];
                            objFS.ValiDatingCarrier = VC;
                            if (VC == "SG")
                                objFS.AirLineName = "SpiceJet";
                            else
                                objFS.AirLineName = "Indigo";
                            objFS.AvailableSeats = IdType;
                            objFS.AvailableSeats1 = t.AVLCNT;
                            if ((t.PCS == "HB") && (VC == "SG") && (("P1,P2").Contains(t.FCS)))
                                objFS.BagInfo = "7kg Hand Bag Only";
                            else
                                objFS.BagInfo = ((from bg in BagInfoList where bg.Class == "Economy" select bg).ToList())[0].Weight;

                            #region Baggage
                            objFS.IsBagFare = Bag;
                            if (objFareTypeSettings != null && objFareTypeSettings.Count > 0 && objFareTypeSettings[0].SSRCode != null)
                                objFS.SSRCode = objFareTypeSettings[0].SSRCode;
                            #endregion
                            #region SME FARE        
                            objFS.IsSMEFare = SMEFare;
                            #endregion
                            //objFS.TotDur = "";
                            objFS.arrdatelcc = t.Leg[i]["STA"];
                            objFS.depdatelcc = t.Leg[i]["STD"];

                            if (searchInputs.Trip == Trip.I)
                            {
                                if (schd == 0)
                                {
                                    if (searchInputs.TripType == STD.Shared.TripType.RoundTrip)
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo + ":" + objFS.OrgDestFrom;
                                    else
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                }
                                else
                                {
                                    if (searchInputs.TripType == STD.Shared.TripType.RoundTrip)
                                        objFS.Sector = objFS.OrgDestTo + ":" + objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                    else
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                }
                            }
                            else
                            {
                                if (schd == 0)
                                {
                                    if (searchInputs.RTF == true)
                                    { objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo + ":" + objFS.OrgDestFrom; }
                                    else
                                    { objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo; }
                                }
                                else
                                {
                                    if (searchInputs.RTF == true)
                                        objFS.Sector = objFS.OrgDestTo + ":" + objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                    else
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                }
                            }

                            objFS.EQ = t.Leg[i]["EQType"];
                            objFS.LineNumber = LNO;
                            // objFS.Ln = LNO;
                            objFS.Leg = (i + 1);
                            objFS.fareBasis = t.FBC;
                            objFS.RBD = t.FCS;
                            objFS.FareRule = t.FSK; //Fare Sell Key
                            objFS.Searchvalue = t.FSK; //Fare Sell Key
                            objFS.sno = t.JSK; //Journey Sell Key
                            #endregion

                            ////SMS charge calc
                            float srvCharge = 0;
                            float srvChargeAdt = 0;
                            float srvChargeChd = 0;

                            //float srvCharge = 0;
                            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID);
                            ////SMS charge calc end


                            #region Adult
                            //objFS.AdtFar = IdType;
                            objFS.AdtFar = CrdType;
                            objFS.AdtCabin = "E";// t.COS;
                            objFS.AdtFarebasis = t.FBC;
                            objFS.AdtRbd = t.FCS;
                            #region ADTFR
                            if (IdType != "SGTBF")
                            {
                                if ((t.PCS == "NN") && (VC == "SG") && (("F,G,H,I,J,K").Contains(t.FCS)))
                                    objFS.AdtFareType = "Non Refundable";
                                else if ((t.PCS == "AB") && (VC == "SG") && (("A,B").Contains(t.FCS)))
                                    objFS.AdtFareType = "Non Refundable";
                                else if ((VC == "6E") && (t.PCS == "S"))//(t.FBC == "P4SALE") &&
                                    objFS.AdtFareType = "Non Refundable";
                                else if (VC == "SG" && (t.FSK.Contains("AP7") || t.FSK.Contains("AP14")))
                                    objFS.AdtFareType = "Non Refundable";
                                else if ((t.PCS == "HB") && (VC == "SG") && (("P1,P2").Contains(t.FCS)))
                                    objFS.AdtFareType = "Non Refundable";
                                else if ((t.PCS == "NF") && (VC == "SG") && (("P1,P2").Contains(t.FCS)))
                                    objFS.AdtFareType = "Non Refundable";
                                else
                                    objFS.AdtFareType = "Refundable";
                            }
                            else if (IdType == "SGSTR")
                            { objFS.AdtFareType = "Spl. Fare, No Commission<br/>Refundable, Meal Included"; }
                            else
                            { objFS.AdtFareType = "Spl. Fare, No Commission<br/>Refundable"; }

                            //FareSellKey , JSK
                            #endregion

                            float PROMODISCHD = 0;
                            float PROMODIS = 0;
                            //Adt Fare Details 
                            if (searchInputs.Adult > 0)
                            {
                                int m = 0;
                                var AdtCharges = from ServiceCharges in t.PaxFare[0].ServiceCharges
                                                 where t.PaxFare[0].PaxType == "ADT"
                                                 select ServiceCharges;




                                foreach (var svc in AdtCharges)
                                {
                                    navitaire.indigo.bm.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge adtd = new navitaire.indigo.bm.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge();
                                    adtd = svc;
                                    //if (adtd.ChargeType.ToString().ToUpper() != "PROMOTIONDISCOUNT")
                                    if (!adtd.ChargeType.ToString().ToUpper().Contains("DISCOUNT"))
                                    {

                                        if (adtd.ChargeCode.ToString() == "YQ")
                                            objFS.AdtFSur = objFS.AdtFSur + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString() == "WO")
                                            objFS.AdtWO = objFS.AdtWO + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString() == "IN")
                                            objFS.AdtIN = objFS.AdtIN + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        //else if (adtd.ChargeCode.ToString() == "JN")
                                        //    objFS.AdtJN = objFS.AdtJN + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString().ToUpper().Contains("CGST") || adtd.ChargeCode.ToString().ToUpper().Contains("SGST") || adtd.ChargeCode.ToString().ToUpper().Contains("UTGST") || adtd.ChargeCode.ToString().ToUpper().Contains("IGST"))
                                            objFS.AdtJN = objFS.AdtJN + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString() == "YM")
                                            objFS.AdtYR = objFS.AdtYR + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString() == String.Empty)
                                            objFS.AdtBfare = objFS.AdtBfare + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else
                                            objFS.AdtOT = float.Parse(Math.Round(decimal.Parse(objFS.AdtOT.ToString()) + decimal.Parse(adtd.Amount.ToString()), 0).ToString());

                                        objFS.AdtFare = float.Parse(Math.Round(decimal.Parse(objFS.AdtFare.ToString()) + decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        AdtTF = AdtTF + decimal.Parse(adtd.Amount.ToString());
                                    }
                                    else
                                    {
                                        PROMODIS += float.Parse(adtd.Amount.ToString());
                                    }
                                }

                                objFS.AdtBfare = objFS.AdtBfare - PROMODIS;
                                objFS.AdtFare = objFS.AdtFare - PROMODIS;
                                AdtTF = AdtTF - decimal.Parse(PROMODIS.ToString());

                                objFS.AdtTax = objFS.AdtFare - objFS.AdtBfare;

                                // float srvChargeAdt = 0;
                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    srvChargeAdt = objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType, Convert.ToString(objFS.AdtBfare), Convert.ToString(objFS.AdtFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                }
                                catch { srvChargeAdt = 0; }
                                #endregion


                                //SMS charge add 
                                objFS.AdtOT = objFS.AdtOT + srvChargeAdt;//srvCharge;
                                objFS.AdtTax = objFS.AdtTax + srvChargeAdt;//srvCharge;
                                objFS.AdtFare = objFS.AdtFare + srvChargeAdt; //srvCharge;
                                //SMS charge add end
                                objFS.ADTAdminMrk = 0;// CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                //objFS.ADTAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.AdtFare, searchInputs.Trip.ToString());
                                objFS.ADTAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                {
                                    //objFS.ADTAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.AdtFare, searchInputs.Trip.ToString());
                                    objFS.ADTAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                    objFS.AdtBfare = objFS.AdtBfare + objFS.ADTAdminMrk;
                                    objFS.AdtFare = objFS.AdtFare + objFS.ADTAdminMrk;
                                }
                                CommDt.Clear();
                                STTFTDS.Clear();
                                if ((searchInputs.Trip.ToString() == JourneyType.D.ToString()) && (searchInputs.RTF != true)) //&& (searchInputs.RTF != true)
                                {
                                    //if ((IdType != "SGTBF") && (IdType != "SGSTR"))
                                    //{
                                    try
                                    {
                                        #region Show hide Net Fare and Commission Calculation-Adult Pax
                                        if (objFS.Leg == 1)
                                        {
                                            CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), 1, objFS.AdtRbd, objFS.AdtCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, CrdType, Convert.ToString(TotalViaSector));
                                            if (CommDt != null && CommDt.Rows.Count > 0)
                                            {                                                
                                                objFS.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                                objFS.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.AdtDiscount1, objFS.AdtBfare, objFS.AdtFSur, searchInputs.TDS);
                                                objFS.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                                objFS.AdtDiscount = objFS.AdtDiscount1 - objFS.AdtSrvTax1;
                                                objFS.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                                objFS.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                                objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());

                                                //objFS.AdtDiscount = 0;
                                                //objFS.AdtCB = 0;
                                                //objFS.AdtSrvTax = 0;
                                                //objFS.AdtTF = 0;
                                                //objFS.AdtTds = 0;
                                                //objFS.IATAComm = 0;
                                            }
                                            else
                                            {
                                                objFS.AdtDiscount = 0;
                                                objFS.AdtCB = 0;
                                                objFS.AdtSrvTax = 0;
                                                objFS.AdtTF = 0;
                                                objFS.AdtTds = 0;
                                                objFS.IATAComm = 0;
                                            }

                                        }
                                        else
                                        {
                                            objFS.AdtDiscount = 0;
                                            objFS.AdtCB = 0;
                                            objFS.AdtSrvTax = 0;
                                            objFS.AdtTF = 0;
                                            objFS.AdtTds = 0;
                                            objFS.IATAComm = 0;
                                        }
                                        #endregion                                       
                                    }
                                    catch (Exception ex)
                                    {
                                        objFS.AdtDiscount = 0;
                                        objFS.AdtCB = 0;
                                        objFS.AdtSrvTax = 0;
                                        objFS.AdtTF = 0;
                                        objFS.AdtTds = 0;
                                        objFS.IATAComm = 0;
                                    }
                                    //}
                                }
                                else
                                {
                                    objFS.AdtDiscount = 0;
                                    objFS.AdtCB = 0;
                                    objFS.AdtSrvTax = 0;
                                    objFS.AdtTF = 0;
                                    objFS.AdtTds = 0;
                                    objFS.IATAComm = 0;
                                }
                            }
                            if ((searchInputs.IsCorp == true) && (searchInputs.RTF != true))
                            {
                                try
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.AdtFare.ToString()));
                                    objFS.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    objFS.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                                catch { }
                            }
                            objFS.AdtEduCess = 0;
                            objFS.AdtHighEduCess = 0;
                            #endregion

                            #region child
                            objFS.ChdCabin = t.COS;
                            objFS.ChdFarebasis = t.FBC;
                            objFS.ChdRbd = t.FCS;
                            //objFS.ChdfareType
                            objFS.ChdTax = 0;
                            if (searchInputs.Child > 0)
                            {
                                var ChdCharges = from pxfare in t.PaxFare[1].ServiceCharges
                                                 where t.PaxFare[1].PaxType == "CHD"
                                                 select pxfare;
                                #region CHDFR
                                foreach (var svc in ChdCharges)
                                {
                                    navitaire.indigo.bm.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge chdd = new navitaire.indigo.bm.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge();
                                    chdd = svc;
                                    //if (chdd.ChargeType.ToString().ToUpper() != "PROMOTIONDISCOUNT")
                                    if (!chdd.ChargeType.ToString().ToUpper().Contains("DISCOUNT"))
                                    {
                                        if (chdd.ChargeCode.ToString() == "YQ")
                                            objFS.ChdFSur = objFS.ChdFSur + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString() == "WO")
                                            objFS.ChdWO = objFS.ChdWO + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString() == "IN")
                                            objFS.ChdIN = objFS.ChdIN + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        //else if (chdd.ChargeCode.ToString() == "JN")
                                        //    objFS.ChdJN = objFS.ChdJN + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString().ToUpper().Contains("CGST") || chdd.ChargeCode.ToString().ToUpper().Contains("SGST") || chdd.ChargeCode.ToString().ToUpper().Contains("UTGST") || chdd.ChargeCode.ToString().ToUpper().Contains("IGST"))
                                            objFS.ChdJN = objFS.ChdJN + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString() == "YM")
                                            objFS.ChdYR = objFS.ChdYR + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString() == String.Empty)
                                            objFS.ChdBFare = objFS.ChdBFare + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else
                                            objFS.ChdOT = float.Parse(Math.Round(decimal.Parse(objFS.ChdOT.ToString()) + decimal.Parse(chdd.Amount.ToString()), 0).ToString());

                                        objFS.ChdFare = float.Parse(Math.Round(decimal.Parse(objFS.ChdFare.ToString()) + decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        ChdTF = ChdTF + decimal.Parse(chdd.Amount.ToString());
                                    }
                                    else
                                    {
                                        PROMODISCHD += float.Parse(chdd.Amount.ToString());
                                    }
                                }
                                #endregion
                                objFS.ChdBFare = objFS.ChdBFare - PROMODISCHD;
                                objFS.ChdFare = objFS.ChdFare - PROMODISCHD;
                                ChdTF = ChdTF - decimal.Parse(PROMODISCHD.ToString());


                                objFS.ChdTax = objFS.ChdFare - objFS.ChdBFare;
                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    srvChargeChd = objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType, Convert.ToString(objFS.ChdBFare), Convert.ToString(objFS.ChdFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                }
                                catch { srvChargeChd = 0; }
                                #endregion


                                //SMS charge add 
                                objFS.ChdOT = objFS.ChdOT + srvChargeChd;//srvCharge;
                                objFS.ChdTax = objFS.ChdTax + srvChargeChd;//srvCharge;
                                objFS.ChdFare = objFS.ChdFare + srvChargeChd; //srvCharge;
                                //SMS charge add end

                                objFS.CHDAdminMrk = 0;// CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                                      // objFS.CHDAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.ChdFare, searchInputs.Trip.ToString());
                                objFS.CHDAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                CommDt.Clear();
                                STTFTDS.Clear();
                                if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                {
                                    //.CHDAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.ChdFare, searchInputs.Trip.ToString());
                                    objFS.CHDAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                    objFS.ChdBFare = objFS.ChdBFare + objFS.CHDAdminMrk;
                                    objFS.ChdFare = objFS.ChdFare + objFS.CHDAdminMrk;
                                }
                                if ((searchInputs.Trip.ToString() == JourneyType.D.ToString()) && (searchInputs.RTF != true))
                                {
                                    try
                                    {
                                        #region Show hide Net Fare and Commission Calculation-Child Pax
                                        if (objFS.Leg == 1)
                                        {
                                            CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), 1, objFS.ChdRbd, objFS.ChdCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, CrdType, Convert.ToString(TotalViaSector));
                                            if (CommDt != null && CommDt.Rows.Count > 0)
                                            {                                               
                                                objFS.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                                objFS.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.ChdDiscount1, objFS.ChdBFare, objFS.ChdFSur, searchInputs.TDS);
                                                objFS.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                                objFS.ChdDiscount = objFS.ChdDiscount1 - objFS.ChdSrvTax1;
                                                objFS.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                                objFS.ChdTds = float.Parse(STTFTDS["Tds"].ToString());

                                                //objFS.ChdDiscount = 0;
                                                //objFS.ChdCB = 0;
                                                //objFS.ChdSrvTax = 0;
                                                //objFS.ChdTF = 0;
                                                //objFS.ChdTds = 0;
                                            }
                                            else
                                            {
                                                objFS.ChdDiscount = 0;
                                                objFS.ChdCB = 0;
                                                objFS.ChdSrvTax = 0;
                                                objFS.ChdTF = 0;
                                                objFS.ChdTds = 0;
                                            }

                                        }
                                        else
                                        {
                                            objFS.ChdDiscount = 0;
                                            objFS.ChdCB = 0;
                                            objFS.ChdSrvTax = 0;
                                            objFS.ChdTF = 0;
                                            objFS.ChdTds = 0;
                                        }
                                        #endregion                                       
                                    }
                                    catch (Exception ex)
                                    {
                                        objFS.ChdDiscount = 0;
                                        objFS.ChdCB = 0;
                                        objFS.ChdSrvTax = 0;
                                        objFS.ChdTF = 0;
                                        objFS.ChdTds = 0;
                                    }
                                }
                                else
                                {
                                    objFS.ChdDiscount = 0;
                                    objFS.ChdCB = 0;
                                    objFS.ChdSrvTax = 0;
                                    objFS.ChdTF = 0;
                                    objFS.ChdTds = 0;
                                }
                                if ((searchInputs.IsCorp == true) && (searchInputs.RTF != true))
                                {
                                    try
                                    {
                                        DataTable MGDT = new DataTable();
                                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier + CrdType, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.ChdFare.ToString()));
                                        objFS.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                        objFS.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                    }
                                    catch { }
                                }
                                objFS.ChdEduCess = 0;
                                objFS.ChdHighEduCess = 0;
                                //objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());
                            }
                            else
                            {
                                objFS.ChdBFare = 0;
                                objFS.ChdFare = 0;
                                objFS.ChdFSur = 0;
                                objFS.ChdWO = 0;
                                objFS.ChdIN = 0;
                                objFS.ChdJN = 0;
                                objFS.ChdYR = 0;
                                objFS.ChdOT = 0;
                                objFS.ChdTax = 0;
                                objFS.ChdDiscount = 0;
                                objFS.ChdCB = 0;
                                objFS.ChdSrvTax = 0;
                                objFS.ChdTF = 0;
                                objFS.ChdTds = 0;
                                objFS.ChdEduCess = 0;
                                objFS.ChdHighEduCess = 0;

                            }

                            #endregion

                            #region PROMOCODE  Devesh
                            try
                            {
                                if (PCRow.Count() > 0)
                                {
                                    objFS.ElectronicTicketing = PCRow[0]["D_T_Code"].ToString() + "/" + PROMODIS + "/" + PROMODISCHD + "/" + PCRow[0]["AppliedOn"].ToString() + "/" + PCRow[0]["CodeType"].ToString();
                                }
                                else
                                {
                                    objFS.ElectronicTicketing = "";
                                }
                            }
                            catch (Exception ex)
                            { }
                            #endregion

                            #region Infant
                            if (searchInputs.Infant > 0)
                            {
                                if (t.InfFare > 0)
                                {
                                    if (t.InfFare > t.InfTax)
                                        objFS.InfBfare = float.Parse(Math.Round(t.InfFare - t.InfTax).ToString());
                                    else
                                        objFS.InfBfare = float.Parse(Math.Round(t.InfTax - t.InfFare).ToString());
                                    objFS.InfFare = float.Parse(Math.Round(t.InfFare).ToString());
                                    objFS.InfTax = float.Parse(Math.Round(t.InfTax).ToString());
                                    objFS.InfOT = objFS.InfTax;
                                    InfTF = decimal.Parse(Math.Round(t.InfFare).ToString());
                                }
                                else
                                {
                                    objFS.InfFare = float.Parse(Math.Round(InfantBFare + InfTax).ToString());
                                    objFS.InfBfare = float.Parse(Math.Round(InfantBFare).ToString());
                                    objFS.InfTax = float.Parse(Math.Round(InfTax).ToString());
                                    objFS.InfOT = objFS.InfTax;
                                    InfTF = decimal.Parse(Math.Round(objFS.InfFare).ToString());
                                }
                                objFS.InfFSur = 0;
                                objFS.InfIN = 0;
                                objFS.InfJN = 0;
                                //objFS.InfOT = 0;
                                objFS.InfQ = 0;

                                if (searchInputs.IsCorp == true)
                                {
                                    try
                                    {
                                        DataTable MGDT = new DataTable();
                                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier + CrdType, decimal.Parse(objFS.InfBfare.ToString()), decimal.Parse(objFS.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.InfFare.ToString()));
                                        objFS.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                        objFS.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                    }
                                    catch { }
                                }
                            }
                            else
                            {
                                objFS.InfFare = 0;
                                objFS.InfBfare = 0;
                                objFS.InfFSur = 0;
                                objFS.InfIN = 0;
                                objFS.InfJN = 0;
                                objFS.InfOT = 0;
                                objFS.InfQ = 0;
                            }
                            #endregion

                            objFS.OriginalTF = float.Parse((AdtTF * objFS.Adult + ChdTF * objFS.Child).ToString());//Without Infant Fare
                            objFS.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
                            objFS.TotalTax = (objFS.AdtTax * objFS.Adult) + (objFS.ChdTax * objFS.Child) + (objFS.InfTax * objFS.Infant);
                            objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
                            objFS.TotalFare = (objFS.AdtFare * objFS.Adult) + (objFS.ChdFare * objFS.Child) + (objFS.InfFare * objFS.Infant);
                            objFS.STax = (objFS.AdtSrvTax * objFS.Adult) + (objFS.ChdSrvTax * objFS.Child) + (objFS.InfSrvTax * objFS.Infant);
                            objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
                            objFS.TotDis = (objFS.AdtDiscount * objFS.Adult) + (objFS.ChdDiscount * objFS.Child);
                            objFS.TotCB = (objFS.AdtCB * objFS.Adult) + (objFS.ChdCB * objFS.Child);
                            objFS.TotTds = (objFS.AdtTds * objFS.Adult) + (objFS.ChdTds * objFS.Child);// +objFS.InfTds;
                            objFS.TotMrkUp = (objFS.ADTAdminMrk * objFS.Adult) + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAdminMrk * objFS.Child) + (objFS.CHDAgentMrk * objFS.Child);
                            objFS.TotMgtFee = (objFS.AdtMgtFee * objFS.Adult) + (objFS.ChdMgtFee * objFS.Child) + (objFS.InfMgtFee * objFS.Infant);
                            if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                objFS.TotalFare = objFS.TotalFare + objFS.STax + objFS.TFee + objFS.TotMgtFee + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child);
                            else
                                objFS.TotalFare = objFS.TotalFare + objFS.TotMrkUp + objFS.STax + objFS.TFee + objFS.TotMgtFee;
                            objFS.NetFare = (objFS.TotalFare + objFS.TotTds) - (objFS.TotDis + objFS.TotCB + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child));
                            //objFS.OperatingCarrier = p.opreatingcarrier;
                            objFS.NetFareSRTF = objFS.NetFare;
                            //int hr = int.Parse(p.jrnyTime) / 60;
                            //int min = int.Parse(p.jrnyTime) % 60;
                            //objFS.TotDur = hr + " hrs " + min + " min";

                            objFS.Stops = legs - 1 + "-Stop";

                            objFS.Trip = searchInputs.Trip.ToString();
                            // objFS.FType = searchInputs.TripType.ToString();
                            if ((searchInputs.RTF == true) || (searchInputs.Trip == Trip.I))
                            {
                                if (schd == 0)
                                {
                                    objFS.Flight = "1";
                                    if (searchInputs.RTF == true)
                                        objFS.FType = "RTF";
                                    else
                                        objFS.FType = "OutBound";
                                    objFS.TripType = "O";
                                }
                                else
                                {
                                    objFS.Flight = "2";
                                    if (searchInputs.RTF == true)
                                        objFS.FType = "RTF";
                                    else
                                        objFS.FType = "InBound";
                                    objFS.TripType = "R";
                                }
                            }
                            else
                            {
                                objFS.Flight = "1";
                                objFS.FType = "OutBound";
                                objFS.TripType = "O";
                            }
                            objFS.IsCorp = searchInputs.IsCorp;
                            objFS.Provider = "LCC";
                            srvCharge = srvChargeAdt + srvChargeChd;  //06-03-2018  Add MISC CHARGES PAX WISE
                            objFS.OriginalTT = srvCharge;
                            objFS.SearchId = SearchId;
                            FlightList.Add(objFS);
                        }
                        LNO = LNO + 1;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return FlightList;
        }


        #endregion
        #region SG NAV Version 4
        public List<FlightSearchResults> Spice_GetFltResultSGNav4(List<FarePriceJourney_SGV4> FareBrk, List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet MrkupDS, FlightSearch searchInputs, int schd, decimal InfantBFare, decimal InfTax, string IdType, List<MISCCharges> MiscList, string VC, string CrdType, DataRow[] PCRow, string SearchId, List<FareTypeSettings> objFareTypeSettings, bool Bag = false, bool SMEFare = false)
        {
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            string arrCity = "", depCity = "";
            if (schd == 0)
            {
                depCity = Utility.Left(searchInputs.HidTxtDepCity, 3);
                arrCity = Utility.Left(searchInputs.HidTxtArrCity, 3);
            }
            else
            {
                depCity = Utility.Left(searchInputs.HidTxtArrCity, 3);
                arrCity = Utility.Left(searchInputs.HidTxtDepCity, 3);
            }
            List<FlightSearchResults> FlightList = new List<FlightSearchResults>();

            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            List<BaggageList> BagInfoList = new List<BaggageList>();
            BagInfoList = objFltComm.GetBaggages(searchInputs.Trip.ToString(), VC, Bag, Utility.Right(searchInputs.HidTxtDepCity, 2), Utility.Right(searchInputs.HidTxtArrCity, 2));
            //BagInfoList = objFltComm.GetBaggage(searchInputs.Trip.ToString(), VC, Bag);

            string strFlightNo = "";
            string strValidatingCarrier = "";
            string FlttmDur = "";//added by abhilash
            int LNO = 1;

            try
            {
                //if (VC == "SG" && IdType == "SGNRML")
                //    srvCharge = srvCharge - 200;
                //foreach (FarePriceJourney t in FareBrk)
                #region OutBOund -Inbound
                for (int jrny = 0; jrny < FareBrk.Count; jrny++)
                {
                    FarePriceJourney_SGV4 t = new FarePriceJourney_SGV4();
                    t = FareBrk[jrny];
                    try
                    {
                        #region Get TotalViaSector for Commission PPPSector and PPPSegment
                        int TotalViaSector = 1;
                        try
                        {
                            TotalViaSector = t.Leg.Select(x => x["FlightNumber"]).Distinct().Count();
                        }
                        catch (Exception ex)
                        {
                            TotalViaSector = 1;
                        }
                        #endregion
                        int legs = t.Leg.Length;
                        for (int i = 0; i < legs; i++)
                        {
                            FlightSearchResults objFS = new FlightSearchResults();
                            decimal AdtTF = 0, ChdTF = 0, InfTF = 0;//For Total Fare Per Pax Type
                            #region Flight Details
                            objFS.OrgDestFrom = depCity;
                            objFS.OrgDestTo = arrCity;
                            objFS.DepartureLocation = t.Leg[i]["DepartureStation"];
                            objFS.DepAirportCode = t.Leg[i]["DepartureStation"];
                            objFS.DepartureTerminal = t.Leg[i]["DepartureTerminal"];
                            objFS.DepartureCityName = getCityName(objFS.DepartureLocation, CityList);
                            objFS.ArrivalLocation = t.Leg[i]["ArrivalStation"];
                            objFS.ArrAirportCode = t.Leg[i]["ArrivalStation"];
                            objFS.ArrivalTerminal = t.Leg[i]["ArrivalTerminal"];
                            objFS.ArrivalCityName = getCityName(objFS.ArrivalLocation, CityList);
                            try
                            {
                                objFS.DepartureAirportName = ((from ct in CityList where ct.AirportCode == objFS.DepartureLocation select ct).ToList())[0].AirportName;
                                objFS.ArrivalAirportName = ((from ct in CityList where ct.AirportCode == objFS.ArrivalLocation select ct).ToList())[0].AirportName;
                            }
                            catch (Exception ex)
                            {
                                objFS.DepartureAirportName = t.Leg[i]["DepartureStation"];
                                objFS.ArrivalAirportName = t.Leg[i]["ArrivalStation"];
                            }
                            string[] Dep = Utility.Split(Utility.Split(t.Leg[i]["STD"], "T")[0], "-");
                            objFS.DepartureDate = Dep[2] + Dep[1] + Utility.Right(Dep[0], 2);
                            objFS.Departure_Date = Dep[2] + " " + Utility.datecon(Dep[1]);
                            objFS.DepartureTime = Utility.Left(Utility.Split(t.Leg[i]["STD"], "T")[1], 5);

                            string[] Arr = Utility.Split(Utility.Split(t.Leg[i]["STA"], "T")[0], "-");
                            objFS.ArrivalDate = Arr[2] + Arr[1] + Utility.Right(Arr[0], 2);
                            objFS.Arrival_Date = Arr[2] + " " + Utility.datecon(Arr[1]);
                            objFS.ArrivalTime = Utility.Left(Utility.Split(t.Leg[i]["STA"], "T")[1], 5);
                            try
                            {
                                if (t.Leg.Length > 1)
                                {
                                    //DateTime DDTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[0]["STD"], "T")[1], 5));
                                    //DateTime ADTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[legs - 1]["STA"], "T")[1], 5));

                                    //TimeSpan value = ADTime - DDTime;
                                    //string label = string.Format("{0:hh\\:mm\\:ss}", value);
                                    //objFS.TotDur = Utility.Left(label, 5);
                                    objFS.TotDur = Utility.Left(Convert.ToDateTime(t.Leg[legs - 1]["STA"]).Subtract(Convert.ToDateTime(t.Leg[0]["STD"])).ToString(), 5);
                                }
                                else
                                {

                                    //DateTime DDTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[i]["STD"], "T")[1], 5));
                                    //DateTime ADTime = DateTime.Parse(Utility.Left(Utility.Split(t.Leg[i]["STA"], "T")[1], 5));

                                    //TimeSpan value = ADTime - DDTime;
                                    //string label = string.Format("{0:hh\\:mm\\:ss}", value);
                                    //objFS.TotDur = Utility.Left(label, 5);
                                    objFS.TotDur = objFS.TotDur = Utility.Left(Convert.ToDateTime(t.Leg[i]["STA"]).Subtract(Convert.ToDateTime(t.Leg[i]["STD"])).ToString(), 5);

                                }
                            }
                            catch { objFS.TotDur = ""; }
                            objFS.Adult = searchInputs.Adult;
                            objFS.Child = searchInputs.Child;
                            objFS.Infant = searchInputs.Infant;
                            objFS.TotPax = searchInputs.Adult + searchInputs.Child;
                            objFS.MarketingCarrier = VC;
                            objFS.OperatingCarrier = VC;
                            objFS.FlightIdentification = t.Leg[i]["FlightNumber"];
                            objFS.ValiDatingCarrier = VC;
                            if (VC == "SG")
                                objFS.AirLineName = "SpiceJet";
                            else
                                objFS.AirLineName = "Indigo";
                            objFS.AvailableSeats = IdType;
                            objFS.AvailableSeats1 = t.AVLCNT;
                            string cbn = "";
                            if ((t.PCS == "HB") && (VC == "SG") && (("P1,P2").Contains(t.FCS)))
                            {
                                objFS.BagInfo = "7kg Hand Bag Only";
                            }                                
                            else
                            {
                                //cbn = ("BC,GC").ToString().Contains(t.PCS.Trim()) ? "BUSINESS" : "ECONOMY";
                                cbn = ("BC,GC").ToString().Contains(t.PCS.Trim()) ? "BUSINESS" : "ECONOMY";
                                objFS.BagInfo = ((from bg in BagInfoList where bg.Class.ToUpper() == cbn select bg).ToList())[0].Weight;
                               // objFS.BagInfo = ((from bg in BagInfoList where bg.Class == "Economy" select bg).ToList())[0].Weight;
                            }                          

                            #region Baggage
                            objFS.IsBagFare = Bag;
                            if (objFareTypeSettings != null && objFareTypeSettings.Count > 0 && objFareTypeSettings[0].SSRCode != null)
                                objFS.SSRCode = objFareTypeSettings[0].SSRCode;
                            #endregion
                            #region SME FARE        
                            objFS.IsSMEFare = SMEFare;
                            #endregion
                            //objFS.TotDur = "";
                            objFS.arrdatelcc = t.Leg[i]["STA"];
                            objFS.depdatelcc = t.Leg[i]["STD"];

                            if (searchInputs.Trip == Trip.I)
                            {
                                if (schd == 0)
                                {
                                    if (searchInputs.TripType == STD.Shared.TripType.RoundTrip)
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo + ":" + objFS.OrgDestFrom;
                                    else
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                }
                                else
                                {
                                    if (searchInputs.TripType == STD.Shared.TripType.RoundTrip)
                                        objFS.Sector = objFS.OrgDestTo + ":" + objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                    else
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                }
                            }
                            else
                            {
                                if (schd == 0)
                                {
                                    if (searchInputs.RTF == true)
                                    { objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo + ":" + objFS.OrgDestFrom; }
                                    else
                                    { objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo; }
                                }
                                else
                                {
                                    if (searchInputs.RTF == true)
                                        objFS.Sector = objFS.OrgDestTo + ":" + objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                    else
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                }
                            }

                            objFS.EQ = t.Leg[i]["EQType"];
                            objFS.LineNumber = LNO;
                            // objFS.Ln = LNO;
                            objFS.Leg = (i + 1);
                            objFS.fareBasis = t.FBC;
                            objFS.RBD = t.FCS;
                            objFS.FareRule = t.FSK; //Fare Sell Key
                            objFS.Searchvalue = t.FSK; //Fare Sell Key
                            objFS.sno = t.JSK; //Journey Sell Key
                            #endregion

                            ////SMS charge calc
                            float srvCharge = 0;
                            float srvChargeAdt = 0;
                            float srvChargeChd = 0;

                            //float srvCharge = 0;
                            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID);
                            ////SMS charge calc end


                            #region Adult
                            //objFS.AdtFar = IdType;
                            objFS.AdtFar = CrdType;
                            objFS.AdtCabin = cbn;// "E";// t.COS;
                            objFS.AdtFarebasis = t.FBC;
                            objFS.AdtRbd = t.FCS;
                            #region ADTFR
                            if (IdType != "SGTBF")
                            {
                                if ((t.PCS == "NN") && (VC == "SG") && (("F,G,H,I,J,K").Contains(t.FCS)))
                                    objFS.AdtFareType = "Non Refundable";
                                else if ((t.PCS == "AB") && (VC == "SG") && (("A,B").Contains(t.FCS)))
                                    objFS.AdtFareType = "Non Refundable";
                                else if ((VC == "6E") && (t.PCS == "S"))//(t.FBC == "P4SALE") &&
                                    objFS.AdtFareType = "Non Refundable";
                                else if (VC == "SG" && (t.FSK.Contains("AP7") || t.FSK.Contains("AP14")))
                                    objFS.AdtFareType = "Non Refundable";
                                else if ((t.PCS == "HB") && (VC == "SG") && (("P1,P2").Contains(t.FCS)))
                                    objFS.AdtFareType = "Non Refundable";
                                else if ((t.PCS == "NF") && (VC == "SG") && (("P1,P2").Contains(t.FCS)))
                                    objFS.AdtFareType = "Non Refundable";
                                else
                                    objFS.AdtFareType = "Refundable";
                            }
                            else if (IdType == "SGSTR")
                            { objFS.AdtFareType = "Spl. Fare, No Commission<br/>Refundable, Meal Included"; }
                            else
                            { objFS.AdtFareType = "Spl. Fare, No Commission<br/>Refundable"; }

                            //FareSellKey , JSK
                            #endregion

                            float PROMODISCHD = 0;
                            float PROMODIS = 0;
                            //Adt Fare Details 
                            if (searchInputs.Adult > 0)
                            {
                                int m = 0;
                                var AdtCharges = from ServiceCharges in t.PaxFare[0].ServiceCharges
                                                 where t.PaxFare[0].PaxType == "ADT"
                                                 select ServiceCharges;




                                foreach (var svc in AdtCharges)
                                {
                                    navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge adtd = new navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge();
                                    adtd = svc;
                                    //if (adtd.ChargeType.ToString().ToUpper() != "PROMOTIONDISCOUNT")
                                    if (!adtd.ChargeType.ToString().ToUpper().Contains("DISCOUNT"))
                                    {

                                        if (adtd.ChargeCode.ToString() == "YQ")
                                            objFS.AdtFSur = objFS.AdtFSur + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString() == "WO")
                                            objFS.AdtWO = objFS.AdtWO + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString() == "IN")
                                            objFS.AdtIN = objFS.AdtIN + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        //else if (adtd.ChargeCode.ToString() == "JN")
                                        //    objFS.AdtJN = objFS.AdtJN + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString().ToUpper().Contains("CGST") || adtd.ChargeCode.ToString().ToUpper().Contains("SGST") || adtd.ChargeCode.ToString().ToUpper().Contains("UTGST") || adtd.ChargeCode.ToString().ToUpper().Contains("IGST"))
                                            objFS.AdtJN = objFS.AdtJN + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString() == "YM")
                                            objFS.AdtYR = objFS.AdtYR + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else if (adtd.ChargeCode.ToString() == String.Empty)
                                            objFS.AdtBfare = objFS.AdtBfare + float.Parse(Math.Round(decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        else
                                            objFS.AdtOT = float.Parse(Math.Round(decimal.Parse(objFS.AdtOT.ToString()) + decimal.Parse(adtd.Amount.ToString()), 0).ToString());

                                        objFS.AdtFare = float.Parse(Math.Round(decimal.Parse(objFS.AdtFare.ToString()) + decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                        AdtTF = AdtTF + decimal.Parse(adtd.Amount.ToString());
                                    }
                                    else
                                    {
                                        PROMODIS += float.Parse(adtd.Amount.ToString());
                                    }
                                }

                                objFS.AdtBfare = objFS.AdtBfare - PROMODIS;
                                objFS.AdtFare = objFS.AdtFare - PROMODIS;
                                AdtTF = AdtTF - decimal.Parse(PROMODIS.ToString());

                                objFS.AdtTax = objFS.AdtFare - objFS.AdtBfare;

                                // float srvChargeAdt = 0;
                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    srvChargeAdt = objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType, Convert.ToString(objFS.AdtBfare), Convert.ToString(objFS.AdtFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                }
                                catch { srvChargeAdt = 0; }
                                #endregion


                                //SMS charge add 
                                objFS.AdtOT = objFS.AdtOT + srvChargeAdt;//srvCharge;
                                objFS.AdtTax = objFS.AdtTax + srvChargeAdt;//srvCharge;
                                objFS.AdtFare = objFS.AdtFare + srvChargeAdt; //srvCharge;
                                //SMS charge add end
                                objFS.ADTAdminMrk = 0;// CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                //objFS.ADTAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.AdtFare, searchInputs.Trip.ToString());
                                objFS.ADTAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                {
                                    //objFS.ADTAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.AdtFare, searchInputs.Trip.ToString());
                                    objFS.ADTAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                    objFS.AdtBfare = objFS.AdtBfare + objFS.ADTAdminMrk;
                                    objFS.AdtFare = objFS.AdtFare + objFS.ADTAdminMrk;
                                }
                                CommDt.Clear();
                                STTFTDS.Clear();
                                if ((searchInputs.Trip.ToString() == JourneyType.D.ToString()) && (searchInputs.RTF != true)) //&& (searchInputs.RTF != true)
                                {
                                    //if ((IdType != "SGTBF") && (IdType != "SGSTR"))
                                    //{
                                    try
                                    {
                                        #region Show hide Net Fare and Commission Calculation-Adult Pax
                                        if (objFS.Leg == 1)
                                        {
                                            CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), 1, objFS.AdtRbd, objFS.AdtCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, CrdType, Convert.ToString(TotalViaSector));
                                            if (CommDt != null && CommDt.Rows.Count > 0)
                                            {                                               
                                                objFS.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                                objFS.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.AdtDiscount1, objFS.AdtBfare, objFS.AdtFSur, searchInputs.TDS);
                                                objFS.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                                objFS.AdtDiscount = objFS.AdtDiscount1 - objFS.AdtSrvTax1;
                                                objFS.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                                objFS.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                                objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());

                                                //objFS.AdtDiscount = 0;
                                                //objFS.AdtCB = 0;
                                                //objFS.AdtSrvTax = 0;
                                                //objFS.AdtTF = 0;
                                                //objFS.AdtTds = 0;
                                                //objFS.IATAComm = 0;
                                            }
                                            else
                                            {
                                                objFS.AdtDiscount = 0;
                                                objFS.AdtCB = 0;
                                                objFS.AdtSrvTax = 0;
                                                objFS.AdtTF = 0;
                                                objFS.AdtTds = 0;
                                                objFS.IATAComm = 0;
                                            }

                                        }
                                        else
                                        {
                                            objFS.AdtDiscount = 0;
                                            objFS.AdtCB = 0;
                                            objFS.AdtSrvTax = 0;
                                            objFS.AdtTF = 0;
                                            objFS.AdtTds = 0;
                                            objFS.IATAComm = 0;
                                        }
                                        #endregion

                                        
                                    }
                                    catch (Exception ex)
                                    {
                                        objFS.AdtDiscount = 0;
                                        objFS.AdtCB = 0;
                                        objFS.AdtSrvTax = 0;
                                        objFS.AdtTF = 0;
                                        objFS.AdtTds = 0;
                                        objFS.IATAComm = 0;
                                    }
                                    //}
                                }
                                else
                                {
                                    objFS.AdtDiscount = 0;
                                    objFS.AdtCB = 0;
                                    objFS.AdtSrvTax = 0;
                                    objFS.AdtTF = 0;
                                    objFS.AdtTds = 0;
                                    objFS.IATAComm = 0;
                                }
                            }
                            if ((searchInputs.IsCorp == true) && (searchInputs.RTF != true))
                            {
                                try
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.AdtFare.ToString()));
                                    objFS.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    objFS.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                                catch { }
                            }
                            objFS.AdtEduCess = 0;
                            objFS.AdtHighEduCess = 0;
                            #endregion

                            #region child
                            objFS.ChdCabin = t.COS;
                            objFS.ChdFarebasis = t.FBC;
                            objFS.ChdRbd = t.FCS;
                            //objFS.ChdfareType
                            objFS.ChdTax = 0;
                            if (searchInputs.Child > 0)
                            {
                                var ChdCharges = from pxfare in t.PaxFare[1].ServiceCharges
                                                 where t.PaxFare[1].PaxType == "CHD"
                                                 select pxfare;
                                #region CHDFR
                                foreach (var svc in ChdCharges)
                                {
                                    navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge chdd = new navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge();
                                    chdd = svc;
                                    // if (chdd.ChargeType.ToString().ToUpper() != "PROMOTIONDISCOUNT")
                                    if (!chdd.ChargeType.ToString().ToUpper().Contains("DISCOUNT"))
                                    {
                                        if (chdd.ChargeCode.ToString() == "YQ")
                                            objFS.ChdFSur = objFS.ChdFSur + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString() == "WO")
                                            objFS.ChdWO = objFS.ChdWO + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString() == "IN")
                                            objFS.ChdIN = objFS.ChdIN + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        //else if (chdd.ChargeCode.ToString() == "JN")
                                        //    objFS.ChdJN = objFS.ChdJN + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString().ToUpper().Contains("CGST") || chdd.ChargeCode.ToString().ToUpper().Contains("SGST") || chdd.ChargeCode.ToString().ToUpper().Contains("UTGST") || chdd.ChargeCode.ToString().ToUpper().Contains("IGST"))
                                            objFS.ChdJN = objFS.ChdJN + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString() == "YM")
                                            objFS.ChdYR = objFS.ChdYR + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else if (chdd.ChargeCode.ToString() == String.Empty)
                                            objFS.ChdBFare = objFS.ChdBFare + float.Parse(Math.Round(decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        else
                                            objFS.ChdOT = float.Parse(Math.Round(decimal.Parse(objFS.ChdOT.ToString()) + decimal.Parse(chdd.Amount.ToString()), 0).ToString());

                                        objFS.ChdFare = float.Parse(Math.Round(decimal.Parse(objFS.ChdFare.ToString()) + decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                        ChdTF = ChdTF + decimal.Parse(chdd.Amount.ToString());
                                    }
                                    else
                                    {
                                        PROMODISCHD += float.Parse(chdd.Amount.ToString());
                                    }
                                }
                                #endregion
                                objFS.ChdBFare = objFS.ChdBFare - PROMODISCHD;
                                objFS.ChdFare = objFS.ChdFare - PROMODISCHD;
                                ChdTF = ChdTF - decimal.Parse(PROMODISCHD.ToString());


                                objFS.ChdTax = objFS.ChdFare - objFS.ChdBFare;
                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    srvChargeChd = objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType, Convert.ToString(objFS.ChdBFare), Convert.ToString(objFS.ChdFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                }
                                catch { srvChargeChd = 0; }
                                #endregion


                                //SMS charge add 
                                objFS.ChdOT = objFS.ChdOT + srvChargeChd;//srvCharge;
                                objFS.ChdTax = objFS.ChdTax + srvChargeChd;//srvCharge;
                                objFS.ChdFare = objFS.ChdFare + srvChargeChd; //srvCharge;
                                //SMS charge add end

                                objFS.CHDAdminMrk = 0;// CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                                      // objFS.CHDAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.ChdFare, searchInputs.Trip.ToString());
                                objFS.CHDAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                CommDt.Clear();
                                STTFTDS.Clear();
                                if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                {
                                    //.CHDAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.ChdFare, searchInputs.Trip.ToString());
                                    objFS.CHDAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                    objFS.ChdBFare = objFS.ChdBFare + objFS.CHDAdminMrk;
                                    objFS.ChdFare = objFS.ChdFare + objFS.CHDAdminMrk;
                                }
                                if ((searchInputs.Trip.ToString() == JourneyType.D.ToString()) && (searchInputs.RTF != true))
                                {
                                    try
                                    {
                                        #region Show hide Net Fare and Commission Calculation-Child Pax
                                        if (objFS.Leg == 1)
                                        {
                                            CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), 1, objFS.ChdRbd, objFS.ChdCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, CrdType, Convert.ToString(TotalViaSector));
                                            if (CommDt != null && CommDt.Rows.Count > 0)
                                            {                                                
                                                objFS.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                                objFS.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.ChdDiscount1, objFS.ChdBFare, objFS.ChdFSur, searchInputs.TDS);
                                                objFS.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                                objFS.ChdDiscount = objFS.ChdDiscount1 - objFS.ChdSrvTax1;
                                                objFS.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                                objFS.ChdTds = float.Parse(STTFTDS["Tds"].ToString());

                                                //objFS.ChdDiscount = 0;
                                                //objFS.ChdCB = 0;
                                                //objFS.ChdSrvTax = 0;
                                                //objFS.ChdTF = 0;
                                                //objFS.ChdTds = 0;
                                            }
                                            else
                                            {
                                                objFS.ChdDiscount = 0;
                                                objFS.ChdCB = 0;
                                                objFS.ChdSrvTax = 0;
                                                objFS.ChdTF = 0;
                                                objFS.ChdTds = 0;
                                            }

                                        }
                                        else
                                        {
                                            objFS.ChdDiscount = 0;
                                            objFS.ChdCB = 0;
                                            objFS.ChdSrvTax = 0;
                                            objFS.ChdTF = 0;
                                            objFS.ChdTds = 0;
                                        }
                                        #endregion                                       
                                    }
                                    catch (Exception ex)
                                    {
                                        objFS.ChdDiscount = 0;
                                        objFS.ChdCB = 0;
                                        objFS.ChdSrvTax = 0;
                                        objFS.ChdTF = 0;
                                        objFS.ChdTds = 0;
                                    }
                                }
                                else
                                {
                                    objFS.ChdDiscount = 0;
                                    objFS.ChdCB = 0;
                                    objFS.ChdSrvTax = 0;
                                    objFS.ChdTF = 0;
                                    objFS.ChdTds = 0;
                                }
                                if ((searchInputs.IsCorp == true) && (searchInputs.RTF != true))
                                {
                                    try
                                    {
                                        DataTable MGDT = new DataTable();
                                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier + CrdType, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.ChdFare.ToString()));
                                        objFS.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                        objFS.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                    }
                                    catch { }
                                }
                                objFS.ChdEduCess = 0;
                                objFS.ChdHighEduCess = 0;
                                //objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());
                            }
                            else
                            {
                                objFS.ChdBFare = 0;
                                objFS.ChdFare = 0;
                                objFS.ChdFSur = 0;
                                objFS.ChdWO = 0;
                                objFS.ChdIN = 0;
                                objFS.ChdJN = 0;
                                objFS.ChdYR = 0;
                                objFS.ChdOT = 0;
                                objFS.ChdTax = 0;
                                objFS.ChdDiscount = 0;
                                objFS.ChdCB = 0;
                                objFS.ChdSrvTax = 0;
                                objFS.ChdTF = 0;
                                objFS.ChdTds = 0;
                                objFS.ChdEduCess = 0;
                                objFS.ChdHighEduCess = 0;

                            }

                            #endregion

                            #region PROMOCODE  Devesh
                            try
                            {
                                if (PCRow.Count() > 0)
                                {
                                    objFS.ElectronicTicketing = PCRow[0]["D_T_Code"].ToString() + "/" + PROMODIS + "/" + PROMODISCHD + "/" + PCRow[0]["AppliedOn"].ToString() + "/" + PCRow[0]["CodeType"].ToString();
                                }
                                else
                                {
                                    objFS.ElectronicTicketing = "";
                                }
                            }
                            catch (Exception ex)
                            { }
                            #endregion

                            #region Infant
                            if (searchInputs.Infant > 0)
                            {
                                if (t.InfFare > 0)
                                {
                                    if (t.InfFare > t.InfTax)
                                        objFS.InfBfare = float.Parse(Math.Round(t.InfFare - t.InfTax).ToString());
                                    else
                                        objFS.InfBfare = float.Parse(Math.Round(t.InfTax - t.InfFare).ToString());
                                    objFS.InfFare = float.Parse(Math.Round(t.InfFare).ToString());
                                    objFS.InfTax = float.Parse(Math.Round(t.InfTax).ToString());
                                    objFS.InfOT = objFS.InfTax;
                                    InfTF = decimal.Parse(Math.Round(t.InfFare).ToString());
                                }
                                else
                                {
                                    objFS.InfFare = float.Parse(Math.Round(InfantBFare + InfTax).ToString());
                                    objFS.InfBfare = float.Parse(Math.Round(InfantBFare).ToString());
                                    objFS.InfTax = float.Parse(Math.Round(InfTax).ToString());
                                    objFS.InfOT = objFS.InfTax;
                                    InfTF = decimal.Parse(Math.Round(objFS.InfFare).ToString());
                                }
                                objFS.InfFSur = 0;
                                objFS.InfIN = 0;
                                objFS.InfJN = 0;
                                //objFS.InfOT = 0;
                                objFS.InfQ = 0;

                                if (searchInputs.IsCorp == true)
                                {
                                    try
                                    {
                                        DataTable MGDT = new DataTable();
                                        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier + CrdType, decimal.Parse(objFS.InfBfare.ToString()), decimal.Parse(objFS.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.InfFare.ToString()));
                                        objFS.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                        objFS.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                    }
                                    catch { }
                                }
                            }
                            else
                            {
                                objFS.InfFare = 0;
                                objFS.InfBfare = 0;
                                objFS.InfFSur = 0;
                                objFS.InfIN = 0;
                                objFS.InfJN = 0;
                                objFS.InfOT = 0;
                                objFS.InfQ = 0;
                            }
                            #endregion

                            objFS.OriginalTF = float.Parse((AdtTF * objFS.Adult + ChdTF * objFS.Child).ToString());//Without Infant Fare
                            objFS.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
                            objFS.TotalTax = (objFS.AdtTax * objFS.Adult) + (objFS.ChdTax * objFS.Child) + (objFS.InfTax * objFS.Infant);
                            objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
                            objFS.TotalFare = (objFS.AdtFare * objFS.Adult) + (objFS.ChdFare * objFS.Child) + (objFS.InfFare * objFS.Infant);
                            objFS.STax = (objFS.AdtSrvTax * objFS.Adult) + (objFS.ChdSrvTax * objFS.Child) + (objFS.InfSrvTax * objFS.Infant);
                            objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
                            objFS.TotDis = (objFS.AdtDiscount * objFS.Adult) + (objFS.ChdDiscount * objFS.Child);
                            objFS.TotCB = (objFS.AdtCB * objFS.Adult) + (objFS.ChdCB * objFS.Child);
                            objFS.TotTds = (objFS.AdtTds * objFS.Adult) + (objFS.ChdTds * objFS.Child);// +objFS.InfTds;
                            objFS.TotMrkUp = (objFS.ADTAdminMrk * objFS.Adult) + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAdminMrk * objFS.Child) + (objFS.CHDAgentMrk * objFS.Child);
                            objFS.TotMgtFee = (objFS.AdtMgtFee * objFS.Adult) + (objFS.ChdMgtFee * objFS.Child) + (objFS.InfMgtFee * objFS.Infant);
                            if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                objFS.TotalFare = objFS.TotalFare + objFS.STax + objFS.TFee + objFS.TotMgtFee + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child);
                            else
                                objFS.TotalFare = objFS.TotalFare + objFS.TotMrkUp + objFS.STax + objFS.TFee + objFS.TotMgtFee;
                            objFS.NetFare = (objFS.TotalFare + objFS.TotTds) - (objFS.TotDis + objFS.TotCB + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child));
                            //objFS.OperatingCarrier = p.opreatingcarrier;
                            objFS.NetFareSRTF = objFS.NetFare;
                            //int hr = int.Parse(p.jrnyTime) / 60;
                            //int min = int.Parse(p.jrnyTime) % 60;
                            //objFS.TotDur = hr + " hrs " + min + " min";

                            objFS.Stops = legs - 1 + "-Stop";

                            objFS.Trip = searchInputs.Trip.ToString();
                            // objFS.FType = searchInputs.TripType.ToString();
                            if ((searchInputs.RTF == true) || (searchInputs.Trip == Trip.I))
                            {
                                if (schd == 0)
                                {
                                    objFS.Flight = "1";
                                    if (searchInputs.RTF == true)
                                        objFS.FType = "RTF";
                                    else
                                        objFS.FType = "OutBound";
                                    objFS.TripType = "O";
                                }
                                else
                                {
                                    objFS.Flight = "2";
                                    if (searchInputs.RTF == true)
                                        objFS.FType = "RTF";
                                    else
                                        objFS.FType = "InBound";
                                    objFS.TripType = "R";
                                }
                            }
                            else
                            {
                                objFS.Flight = "1";
                                objFS.FType = "OutBound";
                                objFS.TripType = "O";
                            }
                            objFS.IsCorp = searchInputs.IsCorp;
                            objFS.Provider = "LCC";
                            srvCharge = srvChargeAdt + srvChargeChd;  //06-03-2018  Add MISC CHARGES PAX WISE
                            objFS.OriginalTT = srvCharge;
                            objFS.SearchId = SearchId;
                            FlightList.Add(objFS);
                        }
                        LNO = LNO + 1;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return FlightList;
        }

        #endregion
        private string getCityName(string city, List<FlightCityList> CityList)
        {
            string city1 = "";
            try
            {
                city1 = ((from ct in CityList where ct.AirportCode == city select ct).ToList())[0].City;
            }
            catch { city1 = city; }
            return city1;
        }

        private string GetTimeInHrsAndMin(int min)
        {
            string rslt;
            if (min < 60)
            {
                rslt = "00:" + min.ToString("00");
            }
            else
            {
                int hrs = min / 60;
                int rmin = min % 60;

                rslt = hrs.ToString("00") + ":" + rmin.ToString("00");
            }

            return rslt;

        }
    }
}