﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;
using LCC;
using LccCouponResult;
using STD.Shared;
using STD.BAL;
using System.Linq;
using System.Xml;
using System.IO;
using System.Text;
using System.Net;
namespace STD.BAL
{
    public class ClsLccCouponAvailability
    {
        string ConnectionString;
        public ClsLccCouponAvailability(string strCon)
        {
            ConnectionString = strCon;
        }
        public string LccDepDate { get; set; }
        public string LccRetDate { get; set; }

        #region Indigo
        public List<FlightSearchResults> GetLccCpnResult(FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, string TType, string FType, float srvCharge)
        {
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnectionString);
            List<BaggageList> BagInfoList = new List<BaggageList>();
            BagInfoList = objFltComm.GetBaggage(searchInputs.Trip.ToString(), "6E",false);
            //DataTable SpiceCpnDt = new DataTable();
            DataTable IndigoCpnDt = new DataTable();
            //DataTable CouponResTbl = new DataTable();
            CouponFare objCpnRes = new CouponFare();
            //clsDbSpiceJet objclsDT = new clsDbSpiceJet();
            //LCCResult objLCCMrk = new LCCResult();
            List<FlightSearchResults> RetList = new List<FlightSearchResults>();
            string arrCity, depCity = "";
            string st = "True";
            if (searchInputs.UID.ToString().Trim() == "III2101")
                st = "False";
            if (FType == "InBound")
            {
                arrCity = searchInputs.HidTxtDepCity;
                depCity = searchInputs.HidTxtArrCity;

                IndigoCpnDt = objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), searchInputs.RetDate.ToString(), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "907", "93", "6E", "True", ""); //Inf_Basic.ToString(), Inf_Tax.ToString()
                //IndigoCpnDt = GetResult(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), LccRetDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "907", "93", "6E", st, ""); //Inf_Basic.ToString(), Inf_Tax.ToString()
            }
            else
            {
                arrCity = searchInputs.HidTxtArrCity;
                depCity = searchInputs.HidTxtDepCity;
                IndigoCpnDt = objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), searchInputs.DepDate.ToString(), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "907", "93", "6E", "True", ""); //Inf_Basic.ToString(), Inf_Tax.ToString()
                //IndigoCpnDt = GetResult(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), LccDepDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "907", "93", "6E", st, ""); //Inf_Basic.ToString(), Inf_Tax.ToString()
            }
            try
            {

                if (IndigoCpnDt.Rows.Count > 0)
                {
                    for (int iCtr = 0; iCtr <= IndigoCpnDt.Rows.Count - 1; iCtr++)
                    {
                        try
                        {
                            FlightSearchResults obj = new FlightSearchResults();
                            #region Flight Details
                            obj.OrgDestFrom = Utility.Left(depCity, 3);
                            obj.OrgDestTo = Utility.Left(arrCity, 3);
                            obj.TripType = "O";  //obj.TripType"] = "O";
                            obj.Sector = obj.OrgDestFrom + ":" + obj.OrgDestTo;
                            obj.DepartureLocation = IndigoCpnDt.Rows[iCtr]["DEPARTURECITY"].ToString().Trim(); //obj.OrgDestFrom;// searchInputs.DepartureCity;	//obj.DepartureLocation"] = Origin;
                            obj.ArrivalLocation = IndigoCpnDt.Rows[iCtr]["ARRIVALCITY"].ToString().Trim(); //obj.OrgDestTo;//searchInputs.ArrivalCity; //obj.ArrivalLocation"] = Destination;
                            //obj.DepartureCityName = IndigoCpnDt.Rows[iCtr]["DEPARTURECITY"].ToString().Trim(); //obj.DepartureCityName"] = IndigoCpnDt.Rows[iCtr]["DEPARTURECITY"].ToString().Trim();
                            //obj.ArrivalCityName = IndigoCpnDt.Rows[iCtr]["ARRIVALCITY"].ToString().Trim();        //row["ArrivalCityName"] 
                            try
                            { obj.DepartureCityName = ((from ct in CityList where ct.AirportCode == obj.DepartureLocation select ct).ToList())[0].City; }
                            catch
                            { obj.DepartureCityName = IndigoCpnDt.Rows[iCtr]["DEPARTURECITY"].ToString().Trim(); }
                            try
                            { obj.ArrivalCityName = ((from ct in CityList where ct.AirportCode == obj.ArrivalLocation select ct).ToList())[0].City; }
                            catch
                            { obj.ArrivalCityName = IndigoCpnDt.Rows[iCtr]["ARRIVALCITY"].ToString().Trim(); }


                            obj.depdatelcc = "";
                            obj.arrdatelcc = "";
                            obj.DepartureDate = Utility.Left(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 3, 2) + Utility.Right(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2); //row["DepartureDate"]
                            obj.Departure_Date = Utility.Left(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + " " + Utility.datecon(Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 3, 2)); //  datecon(Utility.Mid(obj.DepartureDate.ToString().Trim(), 3, 2))
                            obj.DepartureTime = convertTimeFormat(IndigoCpnDt.Rows[iCtr]["DEPARTURETIME"].ToString().Trim());
                            obj.ArrivalDate = Utility.Left(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 3, 2) + Utility.Right(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2); //  row["ArrivalDate"] 
                            obj.Arrival_Date = Utility.Left(obj.ArrivalDate.ToString().Trim(), 2) + " " + Utility.datecon(Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 3, 2)); //datecon(Utility.Mid(obj.ArrivalDate.ToString().Trim(), 3, 2))
                            obj.ArrivalTime = convertTimeFormat(IndigoCpnDt.Rows[iCtr]["ARRIVALTIME"].ToString().Trim());
                            obj.MarketingCarrier = Utility.Left(IndigoCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim(), 2);
                            obj.AirLineName = "Indigo";
                            obj.FlightIdentification = IndigoCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim().Replace("6E", "");
                            obj.RBD = "";
                            obj.AvailableSeats = "0";
                            obj.ValiDatingCarrier = Utility.Left(IndigoCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim(), 2);
                            obj.EQ = "";
                            obj.Stops = IndigoCpnDt.Select("LINENO='" + IndigoCpnDt.Rows[iCtr]["LINENO"].ToString() + "'", "").Length - 1 + "-Stop";
                            obj.fareBasis = "";
                            obj.FBPaxType = "";
                            obj.TotPax = searchInputs.Adult + searchInputs.Child;
                            obj.Adult = searchInputs.Adult;
                            obj.Child = searchInputs.Child;
                            obj.Infant = searchInputs.Infant;
                            obj.TotDur = "";
                            obj.Trip = searchInputs.Trip.ToString();
                            obj.sno = "INDIGOSPECIAL/" + IndigoCpnDt.Rows[iCtr]["SESSIONID"];
                            obj.BagInfo = ((from bg in BagInfoList where bg.Class == "Economy" select bg).ToList())[0].Weight;
                            #endregion

                            #region Fare Details

                            ////SMS charge calc
                            //float srvCharge = 0;
                            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), obj.ValiDatingCarrier, searchInputs.UID);
                            ////SMS charge calc end                                                        

                            #region ADT Fare
                            obj.AdtFareType = "Special Fare";// "Spl. Fare, No Commission";
                            obj.AdtBfare = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTBASEFARE"].ToString());
                            obj.AdtFSur = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTYQ"].ToString());
                            obj.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(IndigoCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());
                            obj.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(IndigoCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());

                            obj.AdtTax = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur + obj.ADTAdminMrk;
                            obj.AdtOT = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.ADTAdminMrk;
                            obj.AdtFare = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString()) + obj.ADTAdminMrk;

                            //SMS charge add 
                            obj.AdtOT = obj.AdtOT + srvCharge;
                            obj.AdtTax = obj.AdtTax + srvCharge;
                            obj.AdtFare = obj.AdtFare + srvCharge;
                            //SMS charge add end

                            //Calculate Commission
                            CommDt.Clear();
                            STTFTDS.Clear();
                            string VTCar = "6ECPN";
                            //VTCar = GetCPNValidatingCarrier(obj.sno);
                            CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, VTCar, decimal.Parse(obj.AdtBfare.ToString()), decimal.Parse(obj.AdtFSur.ToString()), 1, "", obj.AdtCabin, searchInputs.DepDate, obj.OrgDestFrom + "-" + obj.OrgDestTo, searchInputs.RetDate, obj.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), obj.FlightIdentification, obj.OperatingCarrier, obj.MarketingCarrier, "CPN", "");
                            obj.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                            obj.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                            STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, obj.ValiDatingCarrier, obj.AdtDiscount1, obj.AdtBfare, obj.AdtFSur, searchInputs.TDS);
                            obj.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                            obj.AdtDiscount = obj.AdtDiscount1 - obj.AdtSrvTax1;
                            obj.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                            obj.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                            //Calculate Commission


                            if (searchInputs.IsCorp == true)
                            {
                                DataTable MGDT = new DataTable();
                                MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.AdtBfare.ToString()), decimal.Parse(obj.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.AdtFare.ToString()));
                                obj.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                obj.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                            }
                            else
                            {
                                //obj.AdtTax = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur;
                                //obj.AdtOT = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString());
                                //obj.AdtFare = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString());

                                ////SMS charge add 
                                //obj.AdtOT = obj.AdtOT + srvCharge;
                                //obj.AdtTax = obj.AdtTax + srvCharge;
                                //obj.AdtFare = obj.AdtFare + srvCharge;
                                ////SMS charge add end
                            }
                            #endregion

                            #region CHD Fare
                            if (obj.Child > 0)
                            {
                                obj.ChdBFare = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDBASEFARE"].ToString());
                                obj.ChdFSur = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDYQ"].ToString());
                                obj.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(IndigoCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());
                                obj.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(IndigoCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());

                                obj.ChdOT = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.CHDAdminMrk;
                                obj.ChdTax = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur + obj.CHDAdminMrk;
                                obj.ChdFare = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString()) + obj.CHDAdminMrk;

                                //SMS charge add 
                                obj.ChdOT = obj.ChdOT + srvCharge;
                                obj.ChdTax = obj.ChdTax + srvCharge;
                                obj.ChdFare = obj.ChdFare + srvCharge;
                                //SMS charge add end

                                //Calculate Commission Child
                                CommDt.Clear();
                                STTFTDS.Clear();
                                CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, VTCar, decimal.Parse(obj.ChdBFare.ToString()), decimal.Parse(obj.ChdFSur.ToString()), 1, "", obj.ChdCabin, searchInputs.DepDate, obj.OrgDestFrom + "-" + obj.OrgDestTo, searchInputs.RetDate, obj.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), obj.FlightIdentification, obj.OperatingCarrier, obj.MarketingCarrier, "CPN", "");
                                obj.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                obj.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, obj.ValiDatingCarrier, obj.ChdDiscount1, obj.ChdBFare, obj.ChdFSur, searchInputs.TDS);
                                obj.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                obj.ChdDiscount = obj.ChdDiscount1 - obj.ChdSrvTax1;
                                obj.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                obj.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                //Calculate Commssion Child



                                if (searchInputs.IsCorp == true)
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.ChdBFare.ToString()), decimal.Parse(obj.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.ChdFare.ToString()));
                                    obj.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    obj.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                                else
                                {
                                    //obj.ChdOT = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString());
                                    //obj.ChdTax = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur;
                                    //obj.ChdFare = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString());

                                    ////SMS charge add 
                                    //obj.ChdOT = obj.ChdOT + srvCharge;
                                    //obj.ChdTax = obj.ChdTax + srvCharge;
                                    //obj.ChdFare = obj.ChdFare + srvCharge;
                                    ////SMS charge add end
                                }
                            }
                            #endregion

                            #region INF Fare
                            if (obj.Infant > 0)
                            {
                                obj.InfBfare = float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTBASEFARE"].ToString());
                                obj.InfFSur = float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTYQ"].ToString());
                                obj.InfOT = float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString());
                                obj.InfTax = float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString()) + float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTYQ"].ToString());
                                obj.InfFare = float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTTOTALFARE"].ToString());
                                if (searchInputs.IsCorp == true)
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.InfBfare.ToString()), decimal.Parse(obj.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.InfFare.ToString()));
                                    obj.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    obj.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                            }
                            #endregion

                            //objlist.TotDis = (objlist.AdtDiscount * objlist.Adult) + (objlist.ChdDiscount * objlist.Child);
                            //objlist.TotTds = (objlist.AdtTds * objlist.Adult) + (objlist.ChdTds * objlist.Child);// +objFS.InfTds;
                            //objlist.TotCB = (objlist.AdtCB * objlist.Adult) + (objlist.ChdCB * objlist.Child);


                            obj.STax = (obj.AdtSrvTax * obj.Adult) + (obj.ChdSrvTax * obj.Child) + (obj.InfSrvTax * obj.Infant);
                            obj.TotMrkUp = (obj.ADTAdminMrk * obj.Adult) + (obj.CHDAdminMrk * obj.Child) + (obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child);
                            obj.TotMgtFee = (obj.AdtMgtFee * obj.Adult) + (obj.ChdMgtFee * obj.Child) + (obj.InfMgtFee * obj.Infant);
                            obj.TotBfare = (obj.AdtBfare * obj.Adult) + (obj.ChdBFare * obj.Child) + (obj.InfBfare * obj.Infant);
                            obj.TotalFuelSur = (obj.AdtFSur * obj.Adult) + (obj.ChdFSur * obj.Child) + (obj.InfFSur * obj.Infant);
                            obj.TotalTax = (obj.AdtTax * obj.Adult) + (obj.ChdTax * obj.Child) + (obj.InfTax * obj.Infant);
                            obj.TotalFare = (obj.AdtFare * obj.Adult) + (obj.ChdFare * obj.Child) + (obj.InfFare * obj.Infant);


                            //objlist.STax = (objlist.AdtSrvTax * objlist.Adult) + (objlist.ChdSrvTax * objlist.Child) + (objlist.InfSrvTax * objlist.Infant);
                            obj.TFee = (obj.AdtTF * obj.Adult) + (obj.ChdTF * obj.Child);// +(objlist.InfTF * objlist.Infant);
                            obj.TotDis = (obj.AdtDiscount * obj.Adult) + (obj.ChdDiscount * obj.Child);
                            obj.TotCB = (obj.AdtCB * obj.Adult) + (obj.ChdCB * obj.Child);
                            obj.TotTds = (obj.AdtTds * obj.Adult) + (obj.ChdTds * obj.Child);// +objFS.InfTds;


                            //obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + obj.TotMrkUp + obj.TotMgtFee;
                            obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + obj.TotMrkUp + obj.TotMgtFee + obj.STax;
                            obj.NetFare = (obj.TotalFare + obj.TotTds) - ((obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child) + obj.TotDis + obj.TotCB);
                            //obj.TotDis = 0;
                            obj.OriginalTF = float.Parse(IndigoCpnDt.Rows[iCtr]["TOTALPACKAGECOST"].ToString());
                            //obj.OriginalTT = (float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur) * obj.Adult;
                            //obj.OriginalTT = (float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur) * obj.Child;
                            //obj.OriginalTT = (float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString()) + obj.InfFSur) * obj.Infant;
                            obj.OriginalTT = srvCharge;
                            //changes by abhilash 31-aug-2013
                            obj.ADTAdminMrk = 0;
                            obj.CHDAdminMrk = 0;
                            //end
                            #endregion

                            obj.Track_id = "";
                            obj.LineNumber = Convert.ToInt32(IndigoCpnDt.Rows[iCtr]["LINENO"].ToString());
                            obj.Searchvalue = ""; // row["Searchvalue"] 
                            obj.FType = FType;
                            obj.Leg = iCtr + 1;
                            obj.Flight = "1";
                            obj.Provider = "LCC";
                            obj.IsCorp = searchInputs.IsCorp;
                            RetList.Add(obj);
                        }
                        catch
                        { }
                    }

                }


            }
            catch (Exception ex)
            {
            }
            //return CouponResTbl;
            return RetList;
        }

        public List<FlightSearchResults> GetLccCpnResultCorp(FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, string TType, string FType, float srvCharge)
        {
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnectionString);
            DataTable IndigoCpnDt = new DataTable();
            //DataTable CouponResTbl = new DataTable();
            CouponFare objCpnRes = new CouponFare();
            //clsDbSpiceJet objclsDT = new clsDbSpiceJet();
            //LCCResult objLCCMrk = new LCCResult();
            List<FlightSearchResults> RetList = new List<FlightSearchResults>();
            string arrCity, depCity = "";
            string st = "True";
            if (searchInputs.UID.ToString().Trim() == "III2101")
                st = "False";
            if (FType == "InBound")
            {
                arrCity = searchInputs.HidTxtDepCity;
                depCity = searchInputs.HidTxtArrCity;
                IndigoCpnDt = objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), searchInputs.RetDate.ToString(), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "907", "93", "6ECORP", "True", ""); //Inf_Basic.ToString(), Inf_Tax.ToString()
                //IndigoCpnDt = GetResult(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), LccRetDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "907", "93", "6ECORP", st, ""); //Inf_Basic.ToString(), Inf_Tax.ToString()

            }
            else
            {
                arrCity = searchInputs.HidTxtArrCity;
                depCity = searchInputs.HidTxtDepCity;
                IndigoCpnDt = objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), searchInputs.DepDate.ToString(), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "907", "93", "6ECORP", "True", ""); //Inf_Basic.ToString(), Inf_Tax.ToString()
                //IndigoCpnDt = GetResult(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), LccDepDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "907", "93", "6ECORP", st, ""); //Inf_Basic.ToString(), Inf_Tax.ToString()
            }
            try
            {

                if (IndigoCpnDt.Rows.Count > 0)
                {
                    for (int iCtr = 0; iCtr <= IndigoCpnDt.Rows.Count - 1; iCtr++)
                    {
                        try
                        {
                            FlightSearchResults obj = new FlightSearchResults();
                            #region Flight Details
                            obj.OrgDestFrom = Utility.Left(depCity, 3);
                            obj.OrgDestTo = Utility.Left(arrCity, 3);
                            obj.TripType = "O";  //obj.TripType"] = "O";
                            obj.Sector = obj.OrgDestFrom + ":" + obj.OrgDestTo;
                            obj.DepartureLocation = IndigoCpnDt.Rows[iCtr]["DEPARTURECITY"].ToString().Trim(); //obj.OrgDestFrom;// searchInputs.DepartureCity;	//obj.DepartureLocation"] = Origin;
                            obj.ArrivalLocation = IndigoCpnDt.Rows[iCtr]["ARRIVALCITY"].ToString().Trim();// obj.OrgDestTo;//searchInputs.ArrivalCity; //obj.ArrivalLocation"] = Destination;
                            //obj.DepartureCityName = IndigoCpnDt.Rows[iCtr]["DEPARTURECITY"].ToString().Trim(); //obj.DepartureCityName"] = IndigoCpnDt.Rows[iCtr]["DEPARTURECITY"].ToString().Trim();
                            //obj.ArrivalCityName = IndigoCpnDt.Rows[iCtr]["ARRIVALCITY"].ToString().Trim();        //row["ArrivalCityName"]                            
                            try
                            { obj.DepartureCityName = ((from ct in CityList where ct.AirportCode == obj.DepartureLocation select ct).ToList())[0].City; }
                            catch
                            { obj.DepartureCityName = IndigoCpnDt.Rows[iCtr]["DEPARTURECITY"].ToString().Trim(); }
                            try
                            { obj.ArrivalCityName = ((from ct in CityList where ct.AirportCode == obj.ArrivalLocation select ct).ToList())[0].City; }
                            catch
                            { obj.ArrivalCityName = IndigoCpnDt.Rows[iCtr]["ARRIVALCITY"].ToString().Trim(); }

                            obj.depdatelcc = "";
                            obj.arrdatelcc = "";
                            //obj.DepartureDate = Utility.Right(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2) + Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2, 2); //row["DepartureDate"]
                            //obj.Departure_Date = Utility.Right(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + " " + Utility.datecon(Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2)); //  datecon(Utility.Mid(obj.DepartureDate.ToString().Trim(), 3, 2))
                            //obj.DepartureTime = convertTimeFormat(IndigoCpnDt.Rows[iCtr]["DEPARTURETIME"].ToString().Trim());
                            //obj.ArrivalDate = Utility.Right(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2) + Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2, 2); //  row["ArrivalDate"] 
                            //obj.Arrival_Date = Utility.Left(obj.ArrivalDate.ToString().Trim(), 2) + " " + Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2); //datecon(Utility.Mid(obj.ArrivalDate.ToString().Trim(), 3, 2))
                            obj.DepartureDate = Utility.Left(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 3, 2) + Utility.Right(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2); //row["DepartureDate"]
                            obj.Departure_Date = Utility.Left(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + " " + Utility.datecon(Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 3, 2)); //  datecon(Utility.Mid(obj.DepartureDate.ToString().Trim(), 3, 2))
                            obj.DepartureTime = convertTimeFormat(IndigoCpnDt.Rows[iCtr]["DEPARTURETIME"].ToString().Trim());
                            obj.ArrivalDate = Utility.Left(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 3, 2) + Utility.Right(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2); //  row["ArrivalDate"] 
                            obj.Arrival_Date = Utility.Left(obj.ArrivalDate.ToString().Trim(), 2) + " " + Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 3, 2); //datecon(Utility.Mid(obj.ArrivalDate.ToString().Trim(), 3, 2))
                            obj.ArrivalTime = convertTimeFormat(IndigoCpnDt.Rows[iCtr]["ARRIVALTIME"].ToString().Trim());
                            obj.MarketingCarrier = Utility.Left(IndigoCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim(), 2);
                            obj.AirLineName = "Indigo";
                            obj.FlightIdentification = IndigoCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim().Replace("6E", "");
                            obj.RBD = "";
                            obj.AvailableSeats = "0";
                            obj.ValiDatingCarrier = Utility.Left(IndigoCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim(), 2);
                            obj.EQ = "";
                            obj.Stops = IndigoCpnDt.Select("LINENO='" + IndigoCpnDt.Rows[iCtr]["LINENO"].ToString() + "'", "").Length - 1 + "-Stop";
                            obj.fareBasis = "";
                            obj.FBPaxType = "";
                            obj.TotPax = searchInputs.Adult + searchInputs.Child;
                            obj.Adult = searchInputs.Adult;
                            obj.Child = searchInputs.Child;
                            obj.Infant = searchInputs.Infant;
                            obj.TotDur = "";
                            obj.Trip = searchInputs.Trip.ToString();
                            obj.sno = "INDIGOCORP/" + IndigoCpnDt.Rows[iCtr]["SESSIONID"];
                            #endregion

                            #region Fare Details

                            ////SMS charge calc
                            //float srvCharge = 0;
                            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), obj.ValiDatingCarrier, searchInputs.UID);
                            ////SMS charge calc end                                                        

                            #region ADT Fare
                            obj.AdtFareType = "Special Fare";// "Spl. Fare, No Commission";
                            obj.AdtBfare = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTBASEFARE"].ToString());
                            obj.AdtFSur = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTYQ"].ToString());
                            obj.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(IndigoCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());
                            obj.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(IndigoCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());

                            obj.AdtTax = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur + obj.ADTAdminMrk;
                            obj.AdtOT = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.ADTAdminMrk;
                            obj.AdtFare = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString()) + obj.ADTAdminMrk;

                            //SMS charge add 
                            obj.AdtOT = obj.AdtOT + srvCharge;
                            obj.AdtTax = obj.AdtTax + srvCharge;
                            obj.AdtFare = obj.AdtFare + srvCharge;
                            //SMS charge add end

                            //Calculate Commission
                            CommDt.Clear();
                            STTFTDS.Clear();
                            string VTCar = "6ECORP";
                            //VTCar = GetCPNValidatingCarrier(obj.sno);
                            CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, VTCar, decimal.Parse(obj.AdtBfare.ToString()), decimal.Parse(obj.AdtFSur.ToString()), 1, "", obj.AdtCabin, searchInputs.DepDate, obj.OrgDestFrom + "-" + obj.OrgDestTo, searchInputs.RetDate, obj.AdtFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), obj.FlightIdentification, obj.OperatingCarrier, obj.MarketingCarrier, "CRP", "");
                            obj.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                            obj.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                            STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, obj.ValiDatingCarrier, obj.AdtDiscount1, obj.AdtBfare, obj.AdtFSur, searchInputs.TDS);
                            obj.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                            obj.AdtDiscount = obj.AdtDiscount1 - obj.AdtSrvTax1;
                            obj.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                            obj.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                            //Calculate Commission



                            if (searchInputs.IsCorp == true)
                            {
                                DataTable MGDT = new DataTable();
                                MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.AdtBfare.ToString()), decimal.Parse(obj.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.AdtFare.ToString()));
                                obj.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                obj.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                            }
                            else
                            {
                                //obj.AdtTax = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur;
                                //obj.AdtOT = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString());
                                //obj.AdtFare = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString());

                                ////SMS charge add 
                                //obj.AdtOT = obj.AdtOT + srvCharge;
                                //obj.AdtTax = obj.AdtTax + srvCharge;
                                //obj.AdtFare = obj.AdtFare + srvCharge;
                                ////SMS charge add end
                            }
                            #endregion

                            #region CHD Fare
                            if (obj.Child > 0)
                            {
                                obj.ChdBFare = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDBASEFARE"].ToString());
                                obj.ChdFSur = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDYQ"].ToString());
                                obj.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(IndigoCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());
                                obj.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(IndigoCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());

                                obj.ChdOT = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.CHDAdminMrk;
                                obj.ChdTax = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur + obj.CHDAdminMrk;
                                obj.ChdFare = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString()) + obj.CHDAdminMrk;

                                //SMS charge add 
                                obj.ChdOT = obj.ChdOT + srvCharge;
                                obj.ChdTax = obj.ChdTax + srvCharge;
                                obj.ChdFare = obj.ChdFare + srvCharge;
                                //SMS charge add end



                                //Calculate Commission Child
                                CommDt.Clear();
                                STTFTDS.Clear();
                                CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, VTCar, decimal.Parse(obj.ChdBFare.ToString()), decimal.Parse(obj.ChdFSur.ToString()), 1, "", obj.ChdCabin, searchInputs.DepDate, obj.OrgDestFrom + "-" + obj.OrgDestTo, searchInputs.RetDate, obj.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), obj.FlightIdentification, obj.OperatingCarrier, obj.MarketingCarrier, "CPN", "");
                                obj.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                obj.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, obj.ValiDatingCarrier, obj.ChdDiscount1, obj.ChdBFare, obj.ChdFSur, searchInputs.TDS);
                                obj.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                obj.ChdDiscount = obj.ChdDiscount1 - obj.ChdSrvTax1;
                                obj.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                obj.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                //Calculate Commssion Child



                                if (searchInputs.IsCorp == true)
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.ChdBFare.ToString()), decimal.Parse(obj.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.ChdFare.ToString()));
                                    obj.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    obj.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                                else
                                {
                                    //obj.ChdOT = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString());
                                    //obj.ChdTax = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur;
                                    //obj.ChdFare = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString());

                                    ////SMS charge add 
                                    //obj.ChdOT = obj.ChdOT + srvCharge;
                                    //obj.ChdTax = obj.ChdTax + srvCharge;
                                    //obj.ChdFare = obj.ChdFare + srvCharge;
                                    ////SMS charge add end
                                }
                            }
                            #endregion

                            #region INF Fare
                            if (obj.Infant > 0)
                            {
                                obj.InfBfare = float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTBASEFARE"].ToString());
                                obj.InfFSur = float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTYQ"].ToString());
                                obj.InfOT = float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString());
                                obj.InfTax = float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString()) + float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTYQ"].ToString());
                                obj.InfFare = float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTTOTALFARE"].ToString());
                                if (searchInputs.IsCorp == true)
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.InfBfare.ToString()), decimal.Parse(obj.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.InfFare.ToString()));
                                    obj.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    obj.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                            }
                            #endregion

                            obj.STax = (obj.AdtSrvTax * obj.Adult) + (obj.ChdSrvTax * obj.Child) + (obj.InfSrvTax * obj.Infant);
                            obj.TotMrkUp = (obj.ADTAdminMrk * obj.Adult) + (obj.CHDAdminMrk * obj.Child) + (obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child);
                            obj.TotMgtFee = (obj.AdtMgtFee * obj.Adult) + (obj.ChdMgtFee * obj.Child) + (obj.InfMgtFee * obj.Infant);
                            obj.TotBfare = (obj.AdtBfare * obj.Adult) + (obj.ChdBFare * obj.Child) + (obj.InfBfare * obj.Infant);
                            obj.TotalFuelSur = (obj.AdtFSur * obj.Adult) + (obj.ChdFSur * obj.Child) + (obj.InfFSur * obj.Infant);
                            obj.TotalTax = (obj.AdtTax * obj.Adult) + (obj.ChdTax * obj.Child) + (obj.InfTax * obj.Infant);
                            obj.TotalFare = (obj.AdtFare * obj.Adult) + (obj.ChdFare * obj.Child) + (obj.InfFare * obj.Infant);
                            //obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + obj.TotMrkUp + obj.TotMgtFee;


                            obj.TFee = (obj.AdtTF * obj.Adult) + (obj.ChdTF * obj.Child);// +(objlist.InfTF * objlist.Infant);
                            obj.TotDis = (obj.AdtDiscount * obj.Adult) + (obj.ChdDiscount * obj.Child);
                            obj.TotCB = (obj.AdtCB * obj.Adult) + (obj.ChdCB * obj.Child);
                            obj.TotTds = (obj.AdtTds * obj.Adult) + (obj.ChdTds * obj.Child);// +objFS.InfTds;


                            //obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + obj.TotMrkUp + obj.TotMgtFee;
                            obj.TotalFare = obj.TotalFare + obj.TFee + obj.TotMrkUp + obj.TotMgtFee + obj.STax;
                            obj.NetFare = (obj.TotalFare + obj.TotTds) - ((obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child) + obj.TotDis + obj.TotCB);
                            
                            
                            
                            //obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + (obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child) + obj.TotMgtFee;
                            //obj.NetFare = obj.TotalFare - ((obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child));
                            //obj.TotDis = 0;
                            obj.OriginalTF = float.Parse(IndigoCpnDt.Rows[iCtr]["TOTALPACKAGECOST"].ToString());
                            //obj.OriginalTT = (float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur) * obj.Adult;
                            //obj.OriginalTT = (float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur) * obj.Child;
                            //obj.OriginalTT = (float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString()) + obj.InfFSur) * obj.Infant;
                            obj.OriginalTT = srvCharge;
                            //changes by abhilash 31-aug-2013
                            obj.ADTAdminMrk = 0;
                            obj.CHDAdminMrk = 0;
                            //end
                            #endregion

                            obj.LineNumber = Convert.ToInt32(IndigoCpnDt.Rows[iCtr]["LINENO"].ToString());
                            obj.Searchvalue = ""; // row["Searchvalue"] 
                            obj.Track_id = "";
                            obj.FType = FType;
                            obj.Leg = iCtr + 1;
                            obj.Flight = "1";
                            obj.Provider = "LCC";
                            obj.IsCorp = searchInputs.IsCorp;
                            RetList.Add(obj);
                        }
                        catch
                        { }
                    }

                }


            }
            catch (Exception ex)
            {
            }
            //return CouponResTbl;
            return RetList;
        }

        public List<FlightSearchResults> GetLccCpnResultTBF(FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, string TType, string FType, float srvCharge)
        {
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnectionString);
            DataTable IndigoCpnDt = new DataTable();
            //DataTable CouponResTbl = new DataTable();
            CouponFare objCpnRes = new CouponFare();
            //clsDbSpiceJet objclsDT = new clsDbSpiceJet();
            //LCCResult objLCCMrk = new LCCResult();
            List<FlightSearchResults> RetList = new List<FlightSearchResults>();
            string arrCity, depCity = "";
            string st = "True";
            if (searchInputs.UID.ToString().Trim() == "III2101")
                st = "False";
            if (FType == "InBound")
            {
                arrCity = searchInputs.HidTxtDepCity;
                depCity = searchInputs.HidTxtArrCity;
                //IndigoCpnDt = objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), LccRetDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "907", "93", "6ETBF", "True", ""); //Inf_Basic.ToString(), Inf_Tax.ToString()
                IndigoCpnDt = GetResult(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), LccRetDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "907", "93", "6ETBF", st, ""); //Inf_Basic.ToString(), Inf_Tax.ToString()

            }
            else
            {
                arrCity = searchInputs.HidTxtArrCity;
                depCity = searchInputs.HidTxtDepCity;
                //IndigoCpnDt = objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), LccDepDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "907", "93", "6ETBF", "True", ""); //Inf_Basic.ToString(), Inf_Tax.ToString()
                IndigoCpnDt = GetResult(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), LccDepDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "907", "93", "6ETBF", st, ""); //Inf_Basic.ToString(), Inf_Tax.ToString()
            }
            try
            {

                if (IndigoCpnDt.Rows.Count > 0)
                {
                    for (int iCtr = 0; iCtr <= IndigoCpnDt.Rows.Count - 1; iCtr++)
                    {
                        try
                        {
                            FlightSearchResults obj = new FlightSearchResults();
                            #region Flight Details
                            obj.OrgDestFrom = Utility.Left(depCity, 3);
                            obj.OrgDestTo = Utility.Left(arrCity, 3);
                            obj.TripType = "O";  //obj.TripType"] = "O";
                            obj.Sector = obj.OrgDestFrom + ":" + obj.OrgDestTo;
                            obj.DepartureLocation = IndigoCpnDt.Rows[iCtr]["DEPARTURECITY"].ToString().Trim(); //obj.OrgDestFrom;// searchInputs.DepartureCity;	//obj.DepartureLocation"] = Origin;
                            obj.ArrivalLocation = IndigoCpnDt.Rows[iCtr]["ARRIVALCITY"].ToString().Trim(); //obj.OrgDestTo;//searchInputs.ArrivalCity; //obj.ArrivalLocation"] = Destination;
                            obj.DepartureCityName = IndigoCpnDt.Rows[iCtr]["DEPARTURECITY"].ToString().Trim(); //obj.DepartureCityName"] = IndigoCpnDt.Rows[iCtr]["DEPARTURECITY"].ToString().Trim();
                            obj.ArrivalCityName = IndigoCpnDt.Rows[iCtr]["ARRIVALCITY"].ToString().Trim();        //row["ArrivalCityName"]                            
                            obj.depdatelcc = "";
                            obj.arrdatelcc = "";
                            //obj.DepartureDate = Utility.Right(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2) + Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2, 2); //row["DepartureDate"]
                            //obj.Departure_Date = Utility.Right(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + " " + Utility.datecon(Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2)); //  datecon(Utility.Mid(obj.DepartureDate.ToString().Trim(), 3, 2))
                            //obj.DepartureTime = convertTimeFormat(IndigoCpnDt.Rows[iCtr]["DEPARTURETIME"].ToString().Trim());
                            //obj.ArrivalDate = Utility.Right(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2) + Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2, 2); //  row["ArrivalDate"] 
                            //obj.Arrival_Date = Utility.Left(obj.ArrivalDate.ToString().Trim(), 2) + " " + Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2); //datecon(Utility.Mid(obj.ArrivalDate.ToString().Trim(), 3, 2))
                            obj.DepartureDate = Utility.Left(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 3, 2) + Utility.Right(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2); //row["DepartureDate"]
                            obj.Departure_Date = Utility.Left(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + " " + Utility.datecon(Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 3, 2)); //  datecon(Utility.Mid(obj.DepartureDate.ToString().Trim(), 3, 2))
                            obj.DepartureTime = convertTimeFormat(IndigoCpnDt.Rows[iCtr]["DEPARTURETIME"].ToString().Trim());
                            obj.ArrivalDate = Utility.Left(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 3, 2) + Utility.Right(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2); //  row["ArrivalDate"] 
                            obj.Arrival_Date = Utility.Left(obj.ArrivalDate.ToString().Trim(), 2) + " " + Utility.Mid(IndigoCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 3, 2); //datecon(Utility.Mid(obj.ArrivalDate.ToString().Trim(), 3, 2))
                            obj.ArrivalTime = convertTimeFormat(IndigoCpnDt.Rows[iCtr]["ARRIVALTIME"].ToString().Trim());
                            obj.MarketingCarrier = Utility.Left(IndigoCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim(), 2);
                            obj.AirLineName = "Indigo";
                            obj.FlightIdentification = IndigoCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim().Replace("6E", "");
                            obj.RBD = "";
                            obj.AvailableSeats = "0";
                            obj.ValiDatingCarrier = Utility.Left(IndigoCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim(), 2);
                            obj.EQ = "";
                            obj.Stops = IndigoCpnDt.Select("LINENO='" + IndigoCpnDt.Rows[iCtr]["LINENO"].ToString() + "'", "").Length - 1 + "-Stop";
                            obj.fareBasis = "";
                            obj.FBPaxType = "";
                            obj.TotPax = searchInputs.Adult + searchInputs.Child;
                            obj.Adult = searchInputs.Adult;
                            obj.Child = searchInputs.Child;
                            obj.Infant = searchInputs.Infant;
                            obj.TotDur = "";
                            obj.Trip = searchInputs.Trip.ToString();
                            obj.sno = "INDIGOTBF/" + IndigoCpnDt.Rows[iCtr]["SESSIONID"];
                            #endregion

                            #region Fare Details

                            ////SMS charge calc
                            //float srvCharge = 0;
                            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), obj.ValiDatingCarrier, searchInputs.UID);
                            ////SMS charge calc end                                                        

                            #region ADT Fare
                            obj.AdtFareType = "Special Fare";//"Spl. Fare, No Commission";
                            obj.AdtBfare = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTBASEFARE"].ToString());
                            obj.AdtFSur = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTYQ"].ToString());
                            obj.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(IndigoCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());
                            obj.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(IndigoCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());

                            obj.AdtTax = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur + obj.ADTAdminMrk;
                            obj.AdtOT = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.ADTAdminMrk;
                            obj.AdtFare = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString()) + obj.ADTAdminMrk;

                            //SMS charge add 
                            obj.AdtOT = obj.AdtOT + srvCharge;
                            obj.AdtTax = obj.AdtTax + srvCharge;
                            obj.AdtFare = obj.AdtFare + srvCharge;
                            //SMS charge add end

                            //Calculate Commission
                            CommDt.Clear();
                            STTFTDS.Clear();
                            string VTCar = "6ETBF";
                            //VTCar = GetCPNValidatingCarrier(obj.sno);
                            CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, VTCar, decimal.Parse(obj.AdtBfare.ToString()), decimal.Parse(obj.AdtFSur.ToString()), 1, "", obj.AdtCabin, searchInputs.DepDate, obj.OrgDestFrom + "-" + obj.OrgDestTo, searchInputs.RetDate, obj.AdtFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), obj.FlightIdentification, obj.OperatingCarrier, obj.MarketingCarrier, "CPN", "");
                            obj.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                            obj.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                            STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, obj.ValiDatingCarrier, obj.AdtDiscount1, obj.AdtBfare, obj.AdtFSur, searchInputs.TDS);
                            obj.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                            obj.AdtDiscount = obj.AdtDiscount1 - obj.AdtSrvTax1;
                            obj.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                            obj.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                            //Calculate Commission

                            if (searchInputs.IsCorp == true)
                            {
                                DataTable MGDT = new DataTable();
                                MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.AdtBfare.ToString()), decimal.Parse(obj.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.AdtFare.ToString()));
                                obj.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                obj.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                            }
                            else
                            {
                                //obj.AdtTax = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur;
                                //obj.AdtOT = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString());
                                //obj.AdtFare = float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString());

                                ////SMS charge add 
                                //obj.AdtOT = obj.AdtOT + srvCharge;
                                //obj.AdtTax = obj.AdtTax + srvCharge;
                                //obj.AdtFare = obj.AdtFare + srvCharge;
                                ////SMS charge add end
                            }
                            #endregion

                            #region CHD Fare
                            if (obj.Child > 0)
                            {
                                obj.ChdBFare = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDBASEFARE"].ToString());
                                obj.ChdFSur = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDYQ"].ToString());
                                obj.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(IndigoCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());
                                obj.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(IndigoCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());

                                obj.ChdOT = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.CHDAdminMrk;
                                obj.ChdTax = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur + obj.CHDAdminMrk;
                                obj.ChdFare = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString()) + obj.CHDAdminMrk;

                                //SMS charge add 
                                obj.ChdOT = obj.ChdOT + srvCharge;
                                obj.ChdTax = obj.ChdTax + srvCharge;
                                obj.ChdFare = obj.ChdFare + srvCharge;
                                //SMS charge add end


                                //Calculate Commission Child
                                CommDt.Clear();
                                STTFTDS.Clear();
                                CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, VTCar, decimal.Parse(obj.ChdBFare.ToString()), decimal.Parse(obj.ChdFSur.ToString()), 1, "", obj.ChdCabin, searchInputs.DepDate, obj.OrgDestFrom + "-" + obj.OrgDestTo, searchInputs.RetDate, obj.AdtFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), obj.FlightIdentification, obj.OperatingCarrier, obj.MarketingCarrier, "CPN", "");
                                obj.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                obj.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, obj.ValiDatingCarrier, obj.ChdDiscount1, obj.ChdBFare, obj.ChdFSur, searchInputs.TDS);
                                obj.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                obj.ChdDiscount = obj.ChdDiscount1 - obj.ChdSrvTax1;
                                obj.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                obj.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                //Calculate Commssion Child



                                if (searchInputs.IsCorp == true)
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.ChdBFare.ToString()), decimal.Parse(obj.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.ChdFare.ToString()));
                                    obj.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    obj.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                                else
                                {
                                    //obj.ChdOT = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString());
                                    //obj.ChdTax = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur;
                                    //obj.ChdFare = float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString());

                                    ////SMS charge add 
                                    //obj.ChdOT = obj.ChdOT + srvCharge;
                                    //obj.ChdTax = obj.ChdTax + srvCharge;
                                    //obj.ChdFare = obj.ChdFare + srvCharge;
                                    ////SMS charge add end
                                }
                            }
                            #endregion

                            #region INF Fare
                            if (obj.Infant > 0)
                            {
                                obj.InfBfare = float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTBASEFARE"].ToString());
                                obj.InfFSur = float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTYQ"].ToString());
                                obj.InfOT = float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString());
                                obj.InfTax = float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString()) + float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTYQ"].ToString());
                                obj.InfFare = float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTTOTALFARE"].ToString());
                                if (searchInputs.IsCorp == true)
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.InfBfare.ToString()), decimal.Parse(obj.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.InfFare.ToString()));
                                    obj.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    obj.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                            }
                            #endregion

                            obj.STax = (obj.AdtSrvTax * obj.Adult) + (obj.ChdSrvTax * obj.Child) + (obj.InfSrvTax * obj.Infant);
                            obj.TotMrkUp = (obj.ADTAdminMrk * obj.Adult) + (obj.CHDAdminMrk * obj.Child) + (obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child);
                            obj.TotMgtFee = (obj.AdtMgtFee * obj.Adult) + (obj.ChdMgtFee * obj.Child) + (obj.InfMgtFee * obj.Infant);
                            obj.TotBfare = (obj.AdtBfare * obj.Adult) + (obj.ChdBFare * obj.Child) + (obj.InfBfare * obj.Infant);
                            obj.TotalFuelSur = (obj.AdtFSur * obj.Adult) + (obj.ChdFSur * obj.Child) + (obj.InfFSur * obj.Infant);
                            obj.TotalTax = (obj.AdtTax * obj.Adult) + (obj.ChdTax * obj.Child) + (obj.InfTax * obj.Infant);
                            obj.TotalFare = (obj.AdtFare * obj.Adult) + (obj.ChdFare * obj.Child) + (obj.InfFare * obj.Infant);
                            //obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + obj.TotMrkUp + obj.TotMgtFee;


                            obj.TFee = (obj.AdtTF * obj.Adult) + (obj.ChdTF * obj.Child);// +(objlist.InfTF * objlist.Infant);
                            obj.TotDis = (obj.AdtDiscount * obj.Adult) + (obj.ChdDiscount * obj.Child);
                            obj.TotCB = (obj.AdtCB * obj.Adult) + (obj.ChdCB * obj.Child);
                            obj.TotTds = (obj.AdtTds * obj.Adult) + (obj.ChdTds * obj.Child);// +objFS.InfTds;


                            //obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + obj.TotMrkUp + obj.TotMgtFee;
                            obj.TotalFare = obj.TotalFare + obj.TFee + obj.TotMrkUp + obj.TotMgtFee + obj.STax;
                            obj.NetFare = (obj.TotalFare + obj.TotTds) - ((obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child) + obj.TotDis + obj.TotCB);
                            





                            //obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + (obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child) + obj.TotMgtFee;
                            //obj.NetFare = obj.TotalFare - ((obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child));
                            //obj.TotDis = 0;
                            obj.OriginalTF = float.Parse(IndigoCpnDt.Rows[iCtr]["TOTALPACKAGECOST"].ToString());
                            //obj.OriginalTT = (float.Parse(IndigoCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur) * obj.Adult;
                            //obj.OriginalTT = (float.Parse(IndigoCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur) * obj.Child;
                            //obj.OriginalTT = (float.Parse(IndigoCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString()) + obj.InfFSur) * obj.Infant;
                            obj.OriginalTT = srvCharge;
                            //changes by abhilash 31-aug-2013
                            obj.ADTAdminMrk = 0;
                            obj.CHDAdminMrk = 0;
                            //end
                            #endregion

                            obj.LineNumber = Convert.ToInt32(IndigoCpnDt.Rows[iCtr]["LINENO"].ToString());
                            obj.Searchvalue = ""; // row["Searchvalue"] 
                            obj.Track_id = "";
                            obj.FType = FType;
                            obj.Leg = iCtr + 1;
                            obj.Flight = "1";
                            obj.Provider = "LCC";
                            obj.IsCorp = searchInputs.IsCorp;
                            RetList.Add(obj);
                        }
                        catch
                        { }
                    }

                }


            }
            catch (Exception ex)
            {
            }
            //return CouponResTbl;
            return RetList;
        }


        #endregion

        #region Spcejet

        public List<FlightSearchResults> GetLccCpnResultSG(FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, string TType, string FType, float srvCharge, string IdType)
        {
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnectionString);
            DataTable SpiceCpnDt = new DataTable();
            //DataTable CouponResTbl = new DataTable();
            CouponFare objCpnRes = new CouponFare();
            clsDbIndigo objclsDT = new clsDbIndigo();
            List<BaggageList> BagInfoList = new List<BaggageList>();
            BagInfoList = objFltComm.GetBaggage(searchInputs.Trip.ToString(), "SG",false);
            List<FlightSearchResults> RetList = new List<FlightSearchResults>();
            string arrCity, depCity = "";
            if (FType == "InBound")
            {
                arrCity = searchInputs.HidTxtDepCity;
                depCity = searchInputs.HidTxtArrCity;
                SpiceCpnDt = objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), LccRetDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "907", "93", "SG", "True", ""); //Inf_Basic.ToString(), Inf_Tax.ToString()
            }
            else
            {
                arrCity = searchInputs.HidTxtArrCity;
                depCity = searchInputs.HidTxtDepCity;
                SpiceCpnDt = objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), LccDepDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "907", "93", "SG", "True", ""); //Inf_Basic.ToString(), Inf_Tax.ToString()
            }
            try
            {

                if (SpiceCpnDt.Rows.Count > 0)
                {

                    for (int iCtr = 0; iCtr <= SpiceCpnDt.Rows.Count - 1; iCtr++)
                    {
                        try
                        {
                            FlightSearchResults obj = new FlightSearchResults();
                            #region Flight Details
                            obj.OrgDestFrom = Utility.Left(depCity, 3);
                            obj.OrgDestTo = Utility.Left(arrCity, 3);
                            obj.TripType = "O";  //obj.TripType"] = "O";
                            obj.Sector = obj.OrgDestFrom + ":" + obj.OrgDestTo;
                            obj.DepartureLocation = SpiceCpnDt.Rows[iCtr]["DEPARTURECITY"].ToString().Trim();
                            obj.ArrivalLocation = SpiceCpnDt.Rows[iCtr]["ARRIVALCITY"].ToString().Trim();//obj.ArrivalLocation"]                                         
                            obj.DepartureCityName = ((from ct in CityList where ct.AirportCode == obj.DepartureLocation select ct).ToList())[0].City;
                            obj.ArrivalCityName = ((from ct in CityList where ct.AirportCode == obj.ArrivalLocation select ct).ToList())[0].City;
                            obj.depdatelcc = "";
                            obj.arrdatelcc = "";
                            obj.DepartureDate = Utility.Right(SpiceCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(SpiceCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2) + Utility.Mid(SpiceCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2, 2); //row["DepartureDate"]
                            obj.Departure_Date = Utility.Right(SpiceCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + " " + Utility.datecon(Utility.Mid(SpiceCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2));
                            obj.DepartureTime = convertTimeFormat(SpiceCpnDt.Rows[iCtr]["DEPARTURETIME"].ToString().Trim()); // row["DepartureTime"] 
                            obj.ArrivalDate = Utility.Right(SpiceCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(SpiceCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2) + Utility.Mid(SpiceCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2, 2); //  row["ArrivalDate"] 
                            obj.Arrival_Date = Utility.Right(SpiceCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + " " + Utility.datecon(Utility.Mid(SpiceCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2));
                            obj.ArrivalTime = convertTimeFormat(SpiceCpnDt.Rows[iCtr]["ARRIVALTIME"].ToString().Trim()); // row["ArrivalTime"] 
                            obj.MarketingCarrier = Utility.Left(SpiceCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim(), 2); //obj.MarketingCarrier"]                     
                            obj.AirLineName = "SpiceJet";
                            obj.FlightIdentification = SpiceCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim().Replace("SG", "");// Utility.Mid(SpiceCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim(), 3, SpiceCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim().Length - 1); // row["FlightIdentification"]
                            obj.RBD = "";
                            obj.AvailableSeats = "0";
                            obj.ValiDatingCarrier = Utility.Left(SpiceCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim(), 2);
                            obj.EQ = "";
                            obj.Stops = SpiceCpnDt.Select("LINENO='" + SpiceCpnDt.Rows[iCtr]["LINENO"].ToString() + "'", "").Length - 1 + "-Stop";
                            obj.fareBasis = "";
                            obj.FBPaxType = "";
                            obj.TotPax = searchInputs.Adult + searchInputs.Child;
                            obj.Adult = searchInputs.Adult;
                            obj.Child = searchInputs.Child;
                            obj.Infant = searchInputs.Infant;
                            obj.TotDur = "";
                            obj.Trip = searchInputs.Trip.ToString();
                            obj.sno = "SPICEJETSPECIAL/" + SpiceCpnDt.Rows[iCtr]["SESSIONID"];
                            obj.BagInfo = ((from bg in BagInfoList where bg.Class == "Economy" select bg).ToList())[0].Weight;
                            #endregion

                            #region Fare Details

                            ////SMS charge calc
                            //float srvCharge = 0;
                            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), obj.ValiDatingCarrier, searchInputs.UID);
                            ////SMS charge calc end                                                        

                            #region ADT Fare
                            //if (IdType == "SGCPNHTL")
                            //    obj.AdtFareType = "Spl. Fare, No Commission<br/><a href='javascript:;' class='inttt'>You could get a Special Free Hotel Voucher with this Flight Purchase</a>";
                            //else
                            obj.AdtFareType = "Special Fare";//"Spl. Fare, No Commission";
                            obj.AdtBfare = float.Parse(SpiceCpnDt.Rows[iCtr]["ADULTBASEFARE"].ToString());
                            obj.AdtFSur = float.Parse(SpiceCpnDt.Rows[iCtr]["ADULTYQ"].ToString());
                            obj.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(SpiceCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());
                            obj.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(SpiceCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());

                            obj.AdtTax = float.Parse(SpiceCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur + obj.ADTAdminMrk;
                            obj.AdtOT = float.Parse(SpiceCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.ADTAdminMrk;
                            obj.AdtFare = float.Parse(SpiceCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString()) + obj.ADTAdminMrk;

                            //SMS charge add 
                            obj.AdtOT = obj.AdtOT + srvCharge;
                            obj.AdtTax = obj.AdtTax + srvCharge;
                            obj.AdtFare = obj.AdtFare + srvCharge;
                            //SMS charge add end


                            //Calculate Commission
                            CommDt.Clear();
                            STTFTDS.Clear();
                            string VTCar = "SGCPN";
                            //VTCar = GetCPNValidatingCarrier(obj.sno);
                            CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, VTCar, decimal.Parse(obj.AdtBfare.ToString()), decimal.Parse(obj.AdtFSur.ToString()), 1, "", obj.AdtCabin, searchInputs.DepDate, obj.OrgDestFrom + "-" + obj.OrgDestTo, searchInputs.RetDate, obj.AdtFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), obj.FlightIdentification, obj.OperatingCarrier, obj.MarketingCarrier, "CPN", "");
                            obj.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                            obj.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                            STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, obj.ValiDatingCarrier, obj.AdtDiscount1, obj.AdtBfare, obj.AdtFSur, searchInputs.TDS);
                            obj.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                            obj.AdtDiscount = obj.AdtDiscount1 - obj.AdtSrvTax1;
                            obj.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                            obj.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                            //Calculate Commission



                            if (searchInputs.IsCorp == true)
                            {
                                DataTable MGDT = new DataTable();
                                MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.AdtBfare.ToString()), decimal.Parse(obj.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.AdtFare.ToString()));
                                obj.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                obj.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                            }
                            else
                            {
                                //obj.AdtTax = float.Parse(SpiceCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur;
                                //obj.AdtOT = float.Parse(SpiceCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString());
                                //obj.AdtFare = float.Parse(SpiceCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString());

                                ////SMS charge add 
                                //obj.AdtOT = obj.AdtOT + srvCharge;
                                //obj.AdtTax = obj.AdtTax + srvCharge;
                                //obj.AdtFare = obj.AdtFare + srvCharge;
                                ////SMS charge add end
                            }
                            #endregion

                            #region CHD Fare
                            if (obj.Child > 0)
                            {
                                obj.ChdBFare = float.Parse(SpiceCpnDt.Rows[iCtr]["CHILDBASEFARE"].ToString());
                                obj.ChdFSur = float.Parse(SpiceCpnDt.Rows[iCtr]["CHILDYQ"].ToString());
                                obj.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(SpiceCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());
                                obj.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(SpiceCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());

                                obj.ChdOT = float.Parse(SpiceCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.CHDAdminMrk;
                                obj.ChdTax = float.Parse(SpiceCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur + obj.CHDAdminMrk;
                                obj.ChdFare = float.Parse(SpiceCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString()) + obj.CHDAdminMrk;

                                //SMS charge add 
                                obj.ChdOT = obj.ChdOT + srvCharge;
                                obj.ChdTax = obj.ChdTax + srvCharge;
                                obj.ChdFare = obj.ChdFare + srvCharge;
                                //SMS charge add end

                                //Calculate Commission Child
                                CommDt.Clear();
                                STTFTDS.Clear();
                                CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, VTCar, decimal.Parse(obj.ChdBFare.ToString()), decimal.Parse(obj.ChdFSur.ToString()), 1, "", obj.ChdCabin, searchInputs.DepDate, obj.OrgDestFrom + "-" + obj.OrgDestTo, searchInputs.RetDate, obj.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), obj.FlightIdentification, obj.OperatingCarrier, obj.MarketingCarrier, "CPN", "");
                                obj.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                obj.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, obj.ValiDatingCarrier, obj.ChdDiscount1, obj.ChdBFare, obj.ChdFSur, searchInputs.TDS);
                                obj.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                obj.ChdDiscount = obj.ChdDiscount1 - obj.ChdSrvTax1;
                                obj.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                obj.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                //Calculate Commssion Child

                                if (searchInputs.IsCorp == true)
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.ChdBFare.ToString()), decimal.Parse(obj.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.ChdFare.ToString()));
                                    obj.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    obj.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                                else
                                {
                                    //obj.ChdOT = float.Parse(SpiceCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString());
                                    //obj.ChdTax = float.Parse(SpiceCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur;
                                    //obj.ChdFare = float.Parse(SpiceCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString());

                                    ////SMS charge add 
                                    //obj.ChdOT = obj.ChdOT + srvCharge;
                                    //obj.ChdTax = obj.ChdTax + srvCharge;
                                    //obj.ChdFare = obj.ChdFare + srvCharge;
                                    ////SMS charge add end
                                }
                            }
                            #endregion

                            #region INF Fare
                            if (obj.Infant > 0)
                            {
                                obj.InfBfare = float.Parse(SpiceCpnDt.Rows[iCtr]["INFANTBASEFARE"].ToString());
                                obj.InfFSur = float.Parse(SpiceCpnDt.Rows[iCtr]["INFANTYQ"].ToString());
                                obj.InfOT = float.Parse(SpiceCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString());
                                obj.InfTax = float.Parse(SpiceCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString()) + float.Parse(SpiceCpnDt.Rows[iCtr]["INFANTYQ"].ToString());
                                obj.InfFare = float.Parse(SpiceCpnDt.Rows[iCtr]["INFANTTOTALFARE"].ToString());
                                if (searchInputs.IsCorp == true)
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.InfBfare.ToString()), decimal.Parse(obj.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.InfFare.ToString()));
                                    obj.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    obj.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                            }
                            #endregion

                            obj.STax = (obj.AdtSrvTax * obj.Adult) + (obj.ChdSrvTax * obj.Child) + (obj.InfSrvTax * obj.Infant);
                            obj.TotMrkUp = (obj.ADTAdminMrk * obj.Adult) + (obj.CHDAdminMrk * obj.Child) + (obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child);
                            obj.TotMgtFee = (obj.AdtMgtFee * obj.Adult) + (obj.ChdMgtFee * obj.Child) + (obj.InfMgtFee * obj.Infant);
                            obj.TotBfare = (obj.AdtBfare * obj.Adult) + (obj.ChdBFare * obj.Child) + (obj.InfBfare * obj.Infant);
                            obj.TotalFuelSur = (obj.AdtFSur * obj.Adult) + (obj.ChdFSur * obj.Child) + (obj.InfFSur * obj.Infant);
                            obj.TotalTax = (obj.AdtTax * obj.Adult) + (obj.ChdTax * obj.Child) + (obj.InfTax * obj.Infant);
                            obj.TotalFare = (obj.AdtFare * obj.Adult) + (obj.ChdFare * obj.Child) + (obj.InfFare * obj.Infant);
                            
                            obj.TFee = (obj.AdtTF * obj.Adult) + (obj.ChdTF * obj.Child);// +(objlist.InfTF * objlist.Infant);
                            obj.TotDis = (obj.AdtDiscount * obj.Adult) + (obj.ChdDiscount * obj.Child);
                            obj.TotCB = (obj.AdtCB * obj.Adult) + (obj.ChdCB * obj.Child);
                            obj.TotTds = (obj.AdtTds * obj.Adult) + (obj.ChdTds * obj.Child);// +objFS.InfTds;


                            //obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + obj.TotMrkUp + obj.TotMgtFee;
                            obj.TotalFare = obj.TotalFare + obj.TFee + obj.TotMrkUp + obj.TotMgtFee + obj.STax;
                            obj.NetFare = (obj.TotalFare + obj.TotTds) - ((obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child) + obj.TotDis + obj.TotCB);

                            //obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + (obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child) + obj.TotMgtFee;
                            //obj.NetFare = obj.TotalFare - ((obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child));
                            //obj.TotDis = 0;
                            obj.OriginalTF = float.Parse(SpiceCpnDt.Rows[iCtr]["TOTALPACKAGECOST"].ToString());
                            //obj.OriginalTT = (float.Parse(SpiceCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur) * obj.Adult;
                            //obj.OriginalTT = (float.Parse(SpiceCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur) * obj.Child;
                            //obj.OriginalTT = (float.Parse(SpiceCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString()) + obj.InfFSur) * obj.Infant;
                            obj.OriginalTT = srvCharge;
                            //changes by abhilash 31-aug-2013
                            obj.ADTAdminMrk = 0;
                            obj.CHDAdminMrk = 0;
                            //end
                            #endregion

                            obj.LineNumber = Convert.ToInt32(SpiceCpnDt.Rows[iCtr]["LINENO"].ToString());
                            obj.Searchvalue = "";
                            obj.Track_id = "";/// SpiceCpnDt.Rows[iCtr]["Track_id"].ToString();
                            obj.FType = FType;
                            obj.Leg = iCtr + 1;
                            obj.Flight = "1";
                            obj.Provider = "LCC";
                            obj.IsCorp = searchInputs.IsCorp;
                            RetList.Add(obj);
                        }
                        catch { }
                    }

                }


            }
            catch (Exception ex)
            {
            }
            //return CouponResTbl;
            return RetList;
        }

        

        #endregion
        #region Go
        //public List<FlightSearchResults> GetLccCpnResultGO(FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, string TType, string FType, float srvCharge)
        public List<FlightSearchResults> GetLccCpnResultGO(FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, string TType, string FType, List<MISCCharges> MiscList)
        {

            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnectionString);
            DataTable GoairCpnDt = new DataTable();
            //DataTable CouponResTbl = new DataTable();
            CouponFare objCpnRes = new CouponFare();
            //clsDbIndigo objclsDT = new clsDbIndigo();
            List<BaggageList> BagInfoList = new List<BaggageList>();
            BagInfoList = objFltComm.GetBaggage(searchInputs.Trip.ToString(), "G8",false);
            List<FlightSearchResults> RetList = new List<FlightSearchResults>();
            string arrCity, depCity = "";
            string arrdate, depdate = "";
            if (FType == "InBound")
            {
                arrCity = searchInputs.HidTxtDepCity;
                depCity = searchInputs.HidTxtArrCity;
                depdate = LccRetDate;
                arrdate = LccDepDate;
                GoairCpnDt = objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), LccRetDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "907", "93", "G8", "True", ""); //Inf_Basic.ToString(), Inf_Tax.ToString()
            }
            else
            {
                arrCity = searchInputs.HidTxtArrCity;
                depCity = searchInputs.HidTxtDepCity;
                depdate = LccDepDate;
                arrdate = LccRetDate;
                GoairCpnDt = objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), LccDepDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "907", "93", "G8", "True", ""); //Inf_Basic.ToString(), Inf_Tax.ToString()
            }
            try
            {

                if (GoairCpnDt.Rows.Count > 0)
                {

                    for (int iCtr = 0; iCtr <= GoairCpnDt.Rows.Count - 1; iCtr++)
                    {
                        try
                        {
                            FlightSearchResults obj = new FlightSearchResults();
                            #region Flight Details
                            obj.OrgDestFrom = Utility.Left(depCity, 3);
                            obj.OrgDestTo = Utility.Left(arrCity, 3);
                            obj.TripType = "O";  //obj.TripType"] = "O";
                            obj.Sector = obj.OrgDestFrom + ":" + obj.OrgDestTo;

                            obj.DepartureLocation = GoairCpnDt.Rows[iCtr]["DEPARTURECITY"].ToString().Trim();
                            obj.ArrivalLocation = GoairCpnDt.Rows[iCtr]["ARRIVALCITY"].ToString().Trim();
                            obj.DepartureCityName = ((from ct in CityList where ct.AirportCode == obj.DepartureLocation select ct).ToList())[0].City;
                            obj.ArrivalCityName = ((from ct in CityList where ct.AirportCode == obj.ArrivalLocation select ct).ToList())[0].City;
                            obj.depdatelcc = "";
                            obj.arrdatelcc = "";
                            obj.DepartureDate = Utility.Right(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2) + Utility.Mid(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2, 2);
                            obj.Departure_Date = Utility.Right(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + " " + Utility.datecon(Utility.Mid(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2)); //datecon(
                            obj.DepartureTime = convertTimeFormat(GoairCpnDt.Rows[iCtr]["DEPARTURETIME"].ToString().Trim());
                            obj.ArrivalDate = Utility.Right(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2) + Utility.Mid(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2, 2);
                            obj.Arrival_Date = Utility.Right(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + " " + Utility.datecon(Utility.Mid(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2)); //datecon(
                            obj.ArrivalTime = convertTimeFormat(GoairCpnDt.Rows[iCtr]["ARRIVALTIME"].ToString().Trim());
                            obj.MarketingCarrier = Utility.Left(GoairCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim(), 2);
                            obj.AirLineName = "GoAir";
                            obj.FlightIdentification = GoairCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim().Replace("G8-", "");// Utility.Mid(GoairCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim(), 3, GoairCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim().Length - 1);
                            obj.RBD = "";
                            //GoairCpnDt.Rows(iCtr).Item("RBD").ToString
                            obj.AvailableSeats = "0";
                            obj.ValiDatingCarrier = Utility.Left(GoairCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim(), 2);
                            obj.EQ = "";
                            obj.Stops = GoairCpnDt.Select("LINENO='" + GoairCpnDt.Rows[iCtr]["LINENO"].ToString() + "'", "").Length - 1 + "-Stop";
                            obj.fareBasis = "";
                            obj.FBPaxType = "";
                            obj.TotPax = searchInputs.Adult + searchInputs.Child;
                            obj.Adult = searchInputs.Adult;
                            obj.Child = searchInputs.Child;
                            obj.Infant = searchInputs.Infant;
                            obj.TotDur = "";
                            obj.Trip = searchInputs.Trip.ToString();
                            obj.sno = "GOAIRSPECIAL/" + GoairCpnDt.Rows[iCtr]["BOOKINGID"];//SESSIONID
                            obj.BagInfo = ((from bg in BagInfoList where bg.Class == "Economy" select bg).ToList())[0].Weight;
                            #endregion

                            #region Fare Details

                            ////SMS charge calc
                            float srvCharge = 0;
                            float srvChargeAdt = 0;
                            float srvChargeChd = 0;
                            //float srvCharge = 0;
                            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), obj.ValiDatingCarrier, searchInputs.UID);
                            ////SMS charge calc end                                                        

                            #region ADT Fare
                            obj.AdtFareType = "Special Fare";// "Spl. Fare, No Commission";
                            obj.AdtBfare = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTBASEFARE"].ToString());
                            obj.AdtFSur = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTYQ"].ToString());
                            obj.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(GoairCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());
                            obj.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(GoairCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());

                            obj.AdtTax = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur + obj.ADTAdminMrk;
                            obj.AdtOT = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.ADTAdminMrk;
                            obj.AdtFare = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString()) + obj.ADTAdminMrk;

                            #region Get MISC Markup Charges Date 06-03-2018
                            try
                            {
                                srvChargeAdt = objFltComm.MISCServiceFee(MiscList, obj.ValiDatingCarrier, "CPN", Convert.ToString(obj.AdtBfare), Convert.ToString(obj.AdtFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                            }
                            catch { srvChargeAdt = 0; }
                            #endregion

                            //SMS charge add 
                            obj.AdtOT = obj.AdtOT + srvChargeAdt;//srvCharge;
                            obj.AdtTax = obj.AdtTax + srvChargeAdt;//srvCharge;
                            obj.AdtFare = obj.AdtFare + srvChargeAdt;//srvCharge;
                            //SMS charge add end

                            //Calculate Commission
                            CommDt.Clear();
                            STTFTDS.Clear();
                            string VTCar = "G8CPN";
                            //VTCar = GetCPNValidatingCarrier(obj.sno);
                            CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, VTCar, decimal.Parse(obj.AdtBfare.ToString()), decimal.Parse(obj.AdtFSur.ToString()), 1, "", obj.AdtCabin, searchInputs.DepDate, obj.OrgDestFrom + "-" + obj.OrgDestTo, searchInputs.RetDate, obj.AdtFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), obj.FlightIdentification, obj.OperatingCarrier, obj.MarketingCarrier, "CPN", "");
                            obj.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                            obj.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                            STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, obj.ValiDatingCarrier, obj.AdtDiscount1, obj.AdtBfare, obj.AdtFSur, searchInputs.TDS);
                            obj.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                            obj.AdtDiscount = obj.AdtDiscount1 - obj.AdtSrvTax1;
                            obj.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                            obj.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                            //Calculate Commission



                            if (searchInputs.IsCorp == true)
                            {
                                DataTable MGDT = new DataTable();
                                MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.AdtBfare.ToString()), decimal.Parse(obj.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.AdtFare.ToString()));
                                obj.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                obj.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                            }
                            else
                            {
                                //obj.AdtTax = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur;
                                //obj.AdtOT = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString());
                                //obj.AdtFare = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString());

                                ////SMS charge add 
                                //obj.AdtOT = obj.AdtOT + srvCharge;
                                //obj.AdtTax = obj.AdtTax + srvCharge;
                                //obj.AdtFare = obj.AdtFare + srvCharge;
                                ////SMS charge add end
                            }
                            #endregion

                            #region CHD Fare
                            if (obj.Child > 0)
                            {
                                obj.ChdBFare = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDBASEFARE"].ToString());
                                obj.ChdFSur = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDYQ"].ToString());
                                obj.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(GoairCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());
                                obj.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(GoairCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());

                                obj.ChdOT = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.CHDAdminMrk;
                                obj.ChdTax = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur + obj.CHDAdminMrk;
                                obj.ChdFare = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString()) + obj.CHDAdminMrk;


                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    srvChargeChd = objFltComm.MISCServiceFee(MiscList, obj.ValiDatingCarrier, "CPN", Convert.ToString(obj.ChdBFare), Convert.ToString(obj.ChdFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                }
                                catch { srvChargeChd = 0; }
                                #endregion

                                //SMS charge add 
                                obj.ChdOT = obj.ChdOT + srvChargeChd;//srvCharge;
                                obj.ChdTax = obj.ChdTax + srvChargeChd;//srvCharge;
                                obj.ChdFare = obj.ChdFare + srvChargeChd;//srvCharge;
                                //SMS charge add end

                                //Calculate Commission Child
                                CommDt.Clear();
                                STTFTDS.Clear();
                                CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, VTCar, decimal.Parse(obj.ChdBFare.ToString()), decimal.Parse(obj.ChdFSur.ToString()), 1, "", obj.ChdCabin, searchInputs.DepDate, obj.OrgDestFrom + "-" + obj.OrgDestTo, searchInputs.RetDate, obj.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), obj.FlightIdentification, obj.OperatingCarrier, obj.MarketingCarrier, "CPN", "");
                                obj.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                obj.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, obj.ValiDatingCarrier, obj.ChdDiscount1, obj.ChdBFare, obj.ChdFSur, searchInputs.TDS);
                                obj.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                obj.ChdDiscount = obj.ChdDiscount1 - obj.ChdSrvTax1;
                                obj.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                obj.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                //Calculate Commssion Child





                                if (searchInputs.IsCorp == true)
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.ChdBFare.ToString()), decimal.Parse(obj.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.ChdFare.ToString()));
                                    obj.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    obj.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                                else
                                {
                                    //obj.ChdOT = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString());
                                    //obj.ChdTax = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur;
                                    //obj.ChdFare = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString());

                                    ////SMS charge add 
                                    //obj.ChdOT = obj.ChdOT + srvCharge;
                                    //obj.ChdTax = obj.ChdTax + srvCharge;
                                    //obj.ChdFare = obj.ChdFare + srvCharge;
                                    ////SMS charge add end
                                }
                            }
                            #endregion

                            #region INF Fare
                            if (obj.Infant > 0)
                            {
                                obj.InfBfare = float.Parse(GoairCpnDt.Rows[iCtr]["INFANTBASEFARE"].ToString());
                                obj.InfFSur = float.Parse(GoairCpnDt.Rows[iCtr]["INFANTYQ"].ToString());
                                obj.InfOT = float.Parse(GoairCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString());
                                obj.InfTax = float.Parse(GoairCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString()) + float.Parse(GoairCpnDt.Rows[iCtr]["INFANTYQ"].ToString());
                                obj.InfFare = float.Parse(GoairCpnDt.Rows[iCtr]["INFANTTOTALFARE"].ToString());

                                if (searchInputs.IsCorp == true)
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.InfBfare.ToString()), decimal.Parse(obj.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.InfFare.ToString()));
                                    obj.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    obj.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                            }
                            #endregion

                            obj.STax = (obj.AdtSrvTax * obj.Adult) + (obj.ChdSrvTax * obj.Child) + (obj.InfSrvTax * obj.Infant);
                            obj.TotMrkUp = (obj.ADTAdminMrk * obj.Adult) + (obj.CHDAdminMrk * obj.Child) + (obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child);
                            obj.TotMgtFee = (obj.AdtMgtFee * obj.Adult) + (obj.ChdMgtFee * obj.Child) + (obj.InfMgtFee * obj.Infant);
                            obj.TotBfare = (obj.AdtBfare * obj.Adult) + (obj.ChdBFare * obj.Child) + (obj.InfBfare * obj.Infant);
                            obj.TotalFuelSur = (obj.AdtFSur * obj.Adult) + (obj.ChdFSur * obj.Child) + (obj.InfFSur * obj.Infant);
                            obj.TotalTax = (obj.AdtTax * obj.Adult) + (obj.ChdTax * obj.Child) + (obj.InfTax * obj.Infant);
                            obj.TotalFare = (obj.AdtFare * obj.Adult) + (obj.ChdFare * obj.Child) + (obj.InfFare * obj.Infant);
                            //obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + obj.TotMrkUp + obj.TotMgtFee;


                            obj.TFee = (obj.AdtTF * obj.Adult) + (obj.ChdTF * obj.Child);// +(objlist.InfTF * objlist.Infant);
                            obj.TotDis = (obj.AdtDiscount * obj.Adult) + (obj.ChdDiscount * obj.Child);
                            obj.TotCB = (obj.AdtCB * obj.Adult) + (obj.ChdCB * obj.Child);
                            obj.TotTds = (obj.AdtTds * obj.Adult) + (obj.ChdTds * obj.Child);// +objFS.InfTds;


                            //obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + obj.TotMrkUp + obj.TotMgtFee;
                            obj.TotalFare = obj.TotalFare + obj.TFee + obj.TotMrkUp + obj.TotMgtFee + obj.STax;
                            obj.NetFare = (obj.TotalFare + obj.TotTds) - ((obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child) + obj.TotDis + obj.TotCB);
                            





                            //obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + (obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child) + obj.TotMgtFee;
                            //obj.NetFare = obj.TotalFare - ((obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child));
                            //obj.TotDis = 0;
                            obj.OriginalTF = float.Parse(GoairCpnDt.Rows[iCtr]["TOTALPACKAGECOST"].ToString());
                            //obj.OriginalTT = (float.Parse(GoairCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur) * obj.Adult;
                            //obj.OriginalTT = (float.Parse(GoairCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur) * obj.Child;
                            //obj.OriginalTT = (float.Parse(GoairCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString()) + obj.InfFSur) * obj.Infant;
                            srvCharge = srvChargeAdt + srvChargeChd;
                            obj.OriginalTT = srvCharge;
                            //changes by abhilash 31-aug-2013
                            obj.ADTAdminMrk = 0;
                            obj.CHDAdminMrk = 0;
                            //end
                            #endregion

                            obj.Track_id = "";/// SpiceCpnDt.Rows[iCtr]["Track_id"].ToString();
                            obj.FType = FType;
                            obj.LineNumber = Convert.ToInt32(GoairCpnDt.Rows[iCtr]["LINENO"].ToString());
                            obj.Searchvalue = "";
                            obj.Leg = iCtr + 1;
                            obj.Flight = "1";
                            obj.Provider = "LCC";
                            obj.IsCorp = searchInputs.IsCorp;
                            obj.depdatelcc = depdate;
                            obj.arrdatelcc = arrdate;
                            RetList.Add(obj);
                        }
                        catch { }
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return RetList;//CouponResTbl;
        }
        // public List<FlightSearchResults> GetLccFlxResultGO(FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, string TType, string FType, float srvCharge)
        public List<FlightSearchResults> GetLccFlxResultGO(FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, string TType, string FType, List<MISCCharges> MiscList)
        {
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnectionString);
            DataTable GoairCpnDt = new DataTable();
            //DataTable CouponResTbl = new DataTable();
            CouponFare objCpnRes = new CouponFare();
            //clsDbIndigo objclsDT = new clsDbIndigo();
            List<FlightSearchResults> RetList = new List<FlightSearchResults>();
            string arrCity, depCity = "";
            string arrdate, depdate = "";
            if (FType == "InBound")
            {
                arrCity = searchInputs.HidTxtDepCity;
                depCity = searchInputs.HidTxtArrCity;
                depdate = LccRetDate;
                arrdate = LccDepDate;
                GoairCpnDt = objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), LccRetDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "907", "93", "G8FLX", "True", ""); //Inf_Basic.ToString(), Inf_Tax.ToString()
            }
            else
            {
                arrCity = searchInputs.HidTxtArrCity;
                depCity = searchInputs.HidTxtDepCity;
                depdate = LccDepDate;
                arrdate = LccRetDate;
                GoairCpnDt = objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), LccDepDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "907", "93", "G8FLX", "True", ""); //Inf_Basic.ToString(), Inf_Tax.ToString()
            }
            try
            {

                if (GoairCpnDt.Rows.Count > 0)
                {

                    for (int iCtr = 0; iCtr <= GoairCpnDt.Rows.Count - 1; iCtr++)
                    {
                        try
                        {
                            FlightSearchResults obj = new FlightSearchResults();
                            #region Flight Details
                            obj.OrgDestFrom = Utility.Left(depCity, 3);
                            obj.OrgDestTo = Utility.Left(arrCity, 3);
                            obj.TripType = "O";  //obj.TripType"] = "O";
                            obj.Sector = obj.OrgDestFrom + ":" + obj.OrgDestTo;

                            obj.DepartureLocation = GoairCpnDt.Rows[iCtr]["DEPARTURECITY"].ToString().Trim();
                            obj.ArrivalLocation = GoairCpnDt.Rows[iCtr]["ARRIVALCITY"].ToString().Trim();
                            obj.DepartureCityName = ((from ct in CityList where ct.AirportCode == obj.DepartureLocation select ct).ToList())[0].City;
                            obj.ArrivalCityName = ((from ct in CityList where ct.AirportCode == obj.ArrivalLocation select ct).ToList())[0].City;
                            obj.depdatelcc = "";
                            obj.arrdatelcc = "";
                            obj.DepartureDate = Utility.Right(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2) + Utility.Mid(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2, 2);
                            obj.Departure_Date = Utility.Right(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + " " + Utility.datecon(Utility.Mid(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2)); //datecon(
                            obj.DepartureTime = convertTimeFormat(GoairCpnDt.Rows[iCtr]["DEPARTURETIME"].ToString().Trim());
                            obj.ArrivalDate = Utility.Right(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2) + Utility.Mid(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2, 2);
                            obj.Arrival_Date = Utility.Right(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + " " + Utility.datecon(Utility.Mid(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2)); //datecon(
                            obj.ArrivalTime = convertTimeFormat(GoairCpnDt.Rows[iCtr]["ARRIVALTIME"].ToString().Trim());
                            obj.MarketingCarrier = Utility.Left(GoairCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim(), 2);
                            obj.AirLineName = "GoAir";
                            obj.FlightIdentification = GoairCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim().Replace("G8-", "");// Utility.Mid(GoairCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim(), 3, GoairCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim().Length - 1);
                            obj.RBD = "";
                            //GoairCpnDt.Rows(iCtr).Item("RBD").ToString
                            obj.AvailableSeats = "0";
                            obj.ValiDatingCarrier = Utility.Left(GoairCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim(), 2);
                            obj.EQ = "";
                            obj.Stops = GoairCpnDt.Select("LINENO='" + GoairCpnDt.Rows[iCtr]["LINENO"].ToString() + "'", "").Length - 1 + "-Stop";
                            obj.fareBasis = "";
                            obj.FBPaxType = "";
                            obj.TotPax = searchInputs.Adult + searchInputs.Child;
                            obj.Adult = searchInputs.Adult;
                            obj.Child = searchInputs.Child;
                            obj.Infant = searchInputs.Infant;
                            obj.TotDur = "";
                            obj.Trip = searchInputs.Trip.ToString();
                            obj.sno = "GOAIRSPECIAL/" + GoairCpnDt.Rows[iCtr]["BOOKINGID"];//SESSIONID
                            #endregion

                            #region Fare Details

                            float srvCharge = 0;
                            float srvChargeAdt = 0;
                            float srvChargeChd = 0;
                            ////SMS charge calc
                            //float srvCharge = 0;
                            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), obj.ValiDatingCarrier, searchInputs.UID);
                            ////SMS charge calc end                                                        

                            #region ADT Fare
                            obj.AdtFareType = "Special Fare";// "Spl. Fare, No Commission.";
                            obj.AdtBfare = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTBASEFARE"].ToString());
                            obj.AdtFSur = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTYQ"].ToString());
                            obj.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(GoairCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());
                            obj.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(GoairCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());

                            obj.AdtTax = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur + obj.ADTAdminMrk;
                            obj.AdtOT = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.ADTAdminMrk;
                            obj.AdtFare = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString()) + obj.ADTAdminMrk;


                            #region Get MISC Markup Charges Date 06-03-2018
                            try
                            {
                                srvChargeAdt = objFltComm.MISCServiceFee(MiscList, obj.ValiDatingCarrier, "CPN", Convert.ToString(obj.AdtBfare), Convert.ToString(obj.AdtFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                            }
                            catch { srvChargeAdt = 0; }
                            #endregion  
                            //SMS charge add 
                            obj.AdtOT = obj.AdtOT + srvChargeAdt;//srvCharge;
                            obj.AdtTax = obj.AdtTax + srvChargeAdt;//srvCharge;
                            obj.AdtFare = obj.AdtFare + srvChargeAdt;//srvCharge;
                            //SMS charge add end


                            //Calculate Commission
                            CommDt.Clear();
                            STTFTDS.Clear();
                            string VTCar = "G8FLX";
                            //VTCar = GetCPNValidatingCarrier(obj.sno);
                            CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, VTCar, decimal.Parse(obj.AdtBfare.ToString()), decimal.Parse(obj.AdtFSur.ToString()), 1, "", obj.AdtCabin, searchInputs.DepDate, obj.OrgDestFrom + "-" + obj.OrgDestTo, searchInputs.RetDate, obj.AdtFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), obj.FlightIdentification, obj.OperatingCarrier, obj.MarketingCarrier, "CPN", "");
                            obj.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                            obj.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                            STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, obj.ValiDatingCarrier, obj.AdtDiscount1, obj.AdtBfare, obj.AdtFSur, searchInputs.TDS);
                            obj.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                            obj.AdtDiscount = obj.AdtDiscount1 - obj.AdtSrvTax1;
                            obj.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                            obj.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                            //Calculate Commission



                            if (searchInputs.IsCorp == true)
                            {
                                DataTable MGDT = new DataTable();
                                MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.AdtBfare.ToString()), decimal.Parse(obj.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.AdtFare.ToString()));
                                obj.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                obj.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                            }
                            else
                            {
                                //obj.AdtTax = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur;
                                //obj.AdtOT = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString());
                                //obj.AdtFare = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString());

                                ////SMS charge add 
                                //obj.AdtOT = obj.AdtOT + srvCharge;
                                //obj.AdtTax = obj.AdtTax + srvCharge;
                                //obj.AdtFare = obj.AdtFare + srvCharge;
                                ////SMS charge add end
                            }
                            #endregion

                            #region CHD Fare
                            if (obj.Child > 0)
                            {
                                obj.ChdBFare = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDBASEFARE"].ToString());
                                obj.ChdFSur = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDYQ"].ToString());
                                obj.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(GoairCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());
                                obj.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(GoairCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());

                                obj.ChdOT = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.CHDAdminMrk;
                                obj.ChdTax = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur + obj.CHDAdminMrk;
                                obj.ChdFare = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString()) + obj.CHDAdminMrk;

                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    srvChargeChd = objFltComm.MISCServiceFee(MiscList, obj.ValiDatingCarrier, "CPN", Convert.ToString(obj.ChdBFare), Convert.ToString(obj.ChdFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                }
                                catch { srvChargeChd = 0; }
                                #endregion
                                //SMS charge add 
                                obj.ChdOT = obj.ChdOT + srvChargeChd;//srvCharge;
                                obj.ChdTax = obj.ChdTax + srvChargeChd;//srvCharge;
                                obj.ChdFare = obj.ChdFare + srvChargeChd;//srvCharge;
                                //SMS charge add end

                                //Calculate Commission Child
                                CommDt.Clear();
                                STTFTDS.Clear();
                                CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, VTCar, decimal.Parse(obj.ChdBFare.ToString()), decimal.Parse(obj.ChdFSur.ToString()), 1, "", obj.ChdCabin, searchInputs.DepDate, obj.OrgDestFrom + "-" + obj.OrgDestTo, searchInputs.RetDate, obj.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), obj.FlightIdentification, obj.OperatingCarrier, obj.MarketingCarrier, "CPN", "");
                                obj.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                obj.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, obj.ValiDatingCarrier, obj.ChdDiscount1, obj.ChdBFare, obj.ChdFSur, searchInputs.TDS);
                                obj.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                obj.ChdDiscount = obj.ChdDiscount1 - obj.ChdSrvTax1;
                                obj.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                obj.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                //Calculate Commssion Child


                                if (searchInputs.IsCorp == true)
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.ChdBFare.ToString()), decimal.Parse(obj.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.ChdFare.ToString()));
                                    obj.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    obj.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                                else
                                {
                                    //obj.ChdOT = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString());
                                    //obj.ChdTax = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur;
                                    //obj.ChdFare = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString());

                                    ////SMS charge add 
                                    //obj.ChdOT = obj.ChdOT + srvCharge;
                                    //obj.ChdTax = obj.ChdTax + srvCharge;
                                    //obj.ChdFare = obj.ChdFare + srvCharge;
                                    ////SMS charge add end
                                }
                            }
                            #endregion

                            #region INF Fare
                            if (obj.Infant > 0)
                            {
                                obj.InfBfare = float.Parse(GoairCpnDt.Rows[iCtr]["INFANTBASEFARE"].ToString());
                                obj.InfFSur = float.Parse(GoairCpnDt.Rows[iCtr]["INFANTYQ"].ToString());
                                obj.InfOT = float.Parse(GoairCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString());
                                obj.InfTax = float.Parse(GoairCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString()) + float.Parse(GoairCpnDt.Rows[iCtr]["INFANTYQ"].ToString());
                                obj.InfFare = float.Parse(GoairCpnDt.Rows[iCtr]["INFANTTOTALFARE"].ToString());

                                if (searchInputs.IsCorp == true)
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.InfBfare.ToString()), decimal.Parse(obj.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.InfFare.ToString()));
                                    obj.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    obj.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                            }
                            #endregion

                            obj.STax = (obj.AdtSrvTax * obj.Adult) + (obj.ChdSrvTax * obj.Child) + (obj.InfSrvTax * obj.Infant);
                            obj.TotMrkUp = (obj.ADTAdminMrk * obj.Adult) + (obj.CHDAdminMrk * obj.Child) + (obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child);
                            obj.TotMgtFee = (obj.AdtMgtFee * obj.Adult) + (obj.ChdMgtFee * obj.Child) + (obj.InfMgtFee * obj.Infant);
                            obj.TotBfare = (obj.AdtBfare * obj.Adult) + (obj.ChdBFare * obj.Child) + (obj.InfBfare * obj.Infant);
                            obj.TotalFuelSur = (obj.AdtFSur * obj.Adult) + (obj.ChdFSur * obj.Child) + (obj.InfFSur * obj.Infant);
                            obj.TotalTax = (obj.AdtTax * obj.Adult) + (obj.ChdTax * obj.Child) + (obj.InfTax * obj.Infant);
                            obj.TotalFare = (obj.AdtFare * obj.Adult) + (obj.ChdFare * obj.Child) + (obj.InfFare * obj.Infant);
                            //obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + obj.TotMrkUp + obj.TotMgtFee;


                            obj.TFee = (obj.AdtTF * obj.Adult) + (obj.ChdTF * obj.Child);// +(objlist.InfTF * objlist.Infant);
                            obj.TotDis = (obj.AdtDiscount * obj.Adult) + (obj.ChdDiscount * obj.Child);
                            obj.TotCB = (obj.AdtCB * obj.Adult) + (obj.ChdCB * obj.Child);
                            obj.TotTds = (obj.AdtTds * obj.Adult) + (obj.ChdTds * obj.Child);// +objFS.InfTds;


                            //obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + obj.TotMrkUp + obj.TotMgtFee;
                            obj.TotalFare = obj.TotalFare + obj.TFee + obj.TotMrkUp + obj.TotMgtFee + obj.STax;
                            obj.NetFare = (obj.TotalFare + obj.TotTds) - ((obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child) + obj.TotDis + obj.TotCB);
                            




                            //obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + (obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child) + obj.TotMgtFee;
                            //obj.NetFare = obj.TotalFare - ((obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child));
                            //obj.TotDis = 0;
                            obj.OriginalTF = float.Parse(GoairCpnDt.Rows[iCtr]["TOTALPACKAGECOST"].ToString());
                            //obj.OriginalTT = (float.Parse(GoairCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur) * obj.Adult;
                            //obj.OriginalTT = (float.Parse(GoairCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur) * obj.Child;
                            //obj.OriginalTT = (float.Parse(GoairCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString()) + obj.InfFSur) * obj.Infant;
                            srvCharge = srvChargeAdt + srvChargeChd;
                            obj.OriginalTT = srvCharge;
                            //changes by abhilash 31-aug-2013
                            obj.ADTAdminMrk = 0;
                            obj.CHDAdminMrk = 0;
                            //end
                            #endregion

                            obj.Track_id = "";/// SpiceCpnDt.Rows[iCtr]["Track_id"].ToString();
                            obj.FType = FType;
                            obj.LineNumber = Convert.ToInt32(GoairCpnDt.Rows[iCtr]["LINENO"].ToString());
                            obj.Searchvalue = "GoAirFlexi";
                            obj.Leg = iCtr + 1;
                            obj.Flight = "1";
                            obj.Provider = "LCC";
                            obj.IsCorp = searchInputs.IsCorp;
                            obj.depdatelcc = depdate;
                            obj.arrdatelcc = arrdate;
                            RetList.Add(obj);
                        }
                        catch { }
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return RetList;//CouponResTbl;
        }
        //public List<FlightSearchResults> GetLccCorpResultGO(FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, string TType, string FType, float srvCharge)
        public List<FlightSearchResults> GetLccCorpResultGO(FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, string TType, string FType, List<MISCCharges> MiscList)
        {
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnectionString);
            DataTable GoairCpnDt = new DataTable();
            //DataTable CouponResTbl = new DataTable();
            CouponFare objCpnRes = new CouponFare();
            //clsDbIndigo objclsDT = new clsDbIndigo();
            List<FlightSearchResults> RetList = new List<FlightSearchResults>();
            string arrCity, depCity = "";
            string arrdate, depdate = "";
            if (FType == "InBound")
            {
                arrCity = searchInputs.HidTxtDepCity;
                depCity = searchInputs.HidTxtArrCity;
                depdate = LccRetDate;
                arrdate = LccDepDate;
                GoairCpnDt = objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), LccRetDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "907", "93", "G8CORP", "True", ""); //Inf_Basic.ToString(), Inf_Tax.ToString()
            }
            else
            {
                arrCity = searchInputs.HidTxtArrCity;
                depCity = searchInputs.HidTxtDepCity;
                depdate = LccDepDate;
                arrdate = LccRetDate;
                GoairCpnDt = objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), LccDepDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "907", "93", "G8CORP", "True", ""); //Inf_Basic.ToString(), Inf_Tax.ToString()
            }
            try
            {

                if (GoairCpnDt.Rows.Count > 0)
                {

                    for (int iCtr = 0; iCtr <= GoairCpnDt.Rows.Count - 1; iCtr++)
                    {
                        try
                        {
                            FlightSearchResults obj = new FlightSearchResults();
                            #region Flight Details
                            obj.OrgDestFrom = Utility.Left(depCity, 3);
                            obj.OrgDestTo = Utility.Left(arrCity, 3);
                            obj.TripType = "O";  //obj.TripType"] = "O";
                            obj.Sector = obj.OrgDestFrom + ":" + obj.OrgDestTo;

                            obj.DepartureLocation = GoairCpnDt.Rows[iCtr]["DEPARTURECITY"].ToString().Trim();
                            obj.ArrivalLocation = GoairCpnDt.Rows[iCtr]["ARRIVALCITY"].ToString().Trim();
                            obj.DepartureCityName = ((from ct in CityList where ct.AirportCode == obj.DepartureLocation select ct).ToList())[0].City;
                            obj.ArrivalCityName = ((from ct in CityList where ct.AirportCode == obj.ArrivalLocation select ct).ToList())[0].City;
                            obj.depdatelcc = "";
                            obj.arrdatelcc = "";
                            obj.DepartureDate = Utility.Right(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2) + Utility.Mid(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2, 2);
                            obj.Departure_Date = Utility.Right(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + " " + Utility.datecon(Utility.Mid(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2)); //datecon(
                            obj.DepartureTime = convertTimeFormat(GoairCpnDt.Rows[iCtr]["DEPARTURETIME"].ToString().Trim());
                            obj.ArrivalDate = Utility.Right(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2) + Utility.Mid(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2, 2);
                            obj.Arrival_Date = Utility.Right(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 2) + " " + Utility.datecon(Utility.Mid(GoairCpnDt.Rows[iCtr]["DEPARTUREDATE"].ToString().Trim(), 5, 2)); //datecon(
                            obj.ArrivalTime = convertTimeFormat(GoairCpnDt.Rows[iCtr]["ARRIVALTIME"].ToString().Trim());
                            obj.MarketingCarrier = Utility.Left(GoairCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim(), 2);
                            obj.AirLineName = "GoAir";
                            obj.FlightIdentification = GoairCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim().Replace("G8-", "");// Utility.Mid(GoairCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim(), 3, GoairCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim().Length - 1);
                            obj.RBD = "";
                            //GoairCpnDt.Rows(iCtr).Item("RBD").ToString
                            obj.AvailableSeats = "0";
                            obj.ValiDatingCarrier = Utility.Left(GoairCpnDt.Rows[iCtr]["FLIGHT"].ToString().Trim(), 2);
                            obj.EQ = "";
                            obj.Stops = GoairCpnDt.Select("LINENO='" + GoairCpnDt.Rows[iCtr]["LINENO"].ToString() + "'", "").Length - 1 + "-Stop";
                            obj.fareBasis = "";
                            obj.FBPaxType = "";
                            obj.TotPax = searchInputs.Adult + searchInputs.Child;
                            obj.Adult = searchInputs.Adult;
                            obj.Child = searchInputs.Child;
                            obj.Infant = searchInputs.Infant;
                            obj.TotDur = "";
                            obj.Trip = searchInputs.Trip.ToString();
                            obj.sno = "GOAIRCORP/" + GoairCpnDt.Rows[iCtr]["BOOKINGID"];//SESSIONID
                            #endregion

                            #region Fare Details

                            ////SMS charge calc
                            float srvCharge = 0;
                            float srvChargeAdt = 0;
                            float srvChargeChd = 0;
                            //float srvCharge = 0;
                            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), obj.ValiDatingCarrier, searchInputs.UID);
                            ////SMS charge calc end                                                        

                            #region ADT Fare
                            obj.AdtFareType = "Special Fare";// "Spl. Fare, No Commission";
                            obj.AdtBfare = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTBASEFARE"].ToString());
                            obj.AdtFSur = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTYQ"].ToString());
                            obj.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(GoairCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());
                            obj.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(GoairCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());

                            obj.AdtTax = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur + obj.ADTAdminMrk;
                            obj.AdtOT = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.ADTAdminMrk;
                            obj.AdtFare = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString()) + obj.ADTAdminMrk;


                            #region Get MISC Markup Charges Date 06-03-2018
                            try
                            {
                                srvChargeAdt = objFltComm.MISCServiceFee(MiscList, obj.ValiDatingCarrier, "CRP", Convert.ToString(obj.AdtBfare), Convert.ToString(obj.AdtFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                            }
                            catch { srvChargeAdt = 0; }
                            #endregion     

                            //SMS charge add 
                            obj.AdtOT = obj.AdtOT + srvChargeAdt;//srvCharge;
                            obj.AdtTax = obj.AdtTax + srvChargeAdt;//srvCharge;
                            obj.AdtFare = obj.AdtFare + srvChargeAdt;//srvCharge;
                            //SMS charge add end

                            //Calculate Commission
                            CommDt.Clear();
                            STTFTDS.Clear();
                            string VTCar = "G8CORP";
                            //VTCar = GetCPNValidatingCarrier(obj.sno);
                            CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, VTCar, decimal.Parse(obj.AdtBfare.ToString()), decimal.Parse(obj.AdtFSur.ToString()), 1, "", obj.AdtCabin, searchInputs.DepDate, obj.OrgDestFrom + "-" + obj.OrgDestTo, searchInputs.RetDate, obj.AdtFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), obj.FlightIdentification, obj.OperatingCarrier, obj.MarketingCarrier, "CPN", "");
                            obj.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                            obj.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                            STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, obj.ValiDatingCarrier, obj.AdtDiscount1, obj.AdtBfare, obj.AdtFSur, searchInputs.TDS);
                            obj.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                            obj.AdtDiscount = obj.AdtDiscount1 - obj.AdtSrvTax1;
                            obj.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                            obj.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                            //Calculate Commission



                            if (searchInputs.IsCorp == true)
                            {
                                DataTable MGDT = new DataTable();
                                MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.AdtBfare.ToString()), decimal.Parse(obj.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.AdtFare.ToString()));
                                obj.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                obj.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                            }
                            else
                            {
                                //obj.AdtTax = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur;
                                //obj.AdtOT = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString());
                                //obj.AdtFare = float.Parse(GoairCpnDt.Rows[iCtr]["ADULTTOTALFARE"].ToString());

                                ////SMS charge add 
                                //obj.AdtOT = obj.AdtOT + srvCharge;
                                //obj.AdtTax = obj.AdtTax + srvCharge;
                                //obj.AdtFare = obj.AdtFare + srvCharge;
                                ////SMS charge add end
                            }
                            #endregion

                            #region CHD Fare
                            if (obj.Child > 0)
                            {
                                obj.ChdBFare = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDBASEFARE"].ToString());
                                obj.ChdFSur = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDYQ"].ToString());
                                obj.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(GoairCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());
                                obj.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(GoairCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString().Trim()), searchInputs.Trip.ToString());

                                obj.ChdOT = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.CHDAdminMrk;
                                obj.ChdTax = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur + obj.CHDAdminMrk;
                                obj.ChdFare = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString()) + obj.CHDAdminMrk;


                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    srvChargeChd = objFltComm.MISCServiceFee(MiscList, obj.ValiDatingCarrier, "CRP", Convert.ToString(obj.ChdBFare), Convert.ToString(obj.ChdFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                }
                                catch { srvChargeChd = 0; }
                                #endregion
                                //SMS charge add 
                                obj.ChdOT = obj.ChdOT + srvChargeChd;//srvCharge;
                                obj.ChdTax = obj.ChdTax + srvChargeChd;//srvCharge;
                                obj.ChdFare = obj.ChdFare + srvChargeChd;//srvCharge;
                                //SMS charge add end

                                //Calculate Commission Child
                                CommDt.Clear();
                                STTFTDS.Clear();
                                CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, VTCar, decimal.Parse(obj.ChdBFare.ToString()), decimal.Parse(obj.ChdFSur.ToString()), 1, "", obj.ChdCabin, searchInputs.DepDate, obj.OrgDestFrom + "-" + obj.OrgDestTo, searchInputs.RetDate, obj.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), obj.FlightIdentification, obj.OperatingCarrier, obj.MarketingCarrier, "CPN", "");
                                obj.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                obj.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, obj.ValiDatingCarrier, obj.ChdDiscount1, obj.ChdBFare, obj.ChdFSur, searchInputs.TDS);
                                obj.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                obj.ChdDiscount = obj.ChdDiscount1 - obj.ChdSrvTax1;
                                obj.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                obj.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                //Calculate Commssion Child




                                if (searchInputs.IsCorp == true)
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.ChdBFare.ToString()), decimal.Parse(obj.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.ChdFare.ToString()));
                                    obj.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    obj.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                                else
                                {
                                    //obj.ChdOT = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString());
                                    //obj.ChdTax = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur;
                                    //obj.ChdFare = float.Parse(GoairCpnDt.Rows[iCtr]["CHILDTOTALFARE"].ToString());

                                    ////SMS charge add 
                                    //obj.ChdOT = obj.ChdOT + srvCharge;
                                    //obj.ChdTax = obj.ChdTax + srvCharge;
                                    //obj.ChdFare = obj.ChdFare + srvCharge;
                                    ////SMS charge add end
                                }
                            }
                            #endregion

                            #region INF Fare
                            if (obj.Infant > 0)
                            {
                                obj.InfBfare = float.Parse(GoairCpnDt.Rows[iCtr]["INFANTBASEFARE"].ToString());
                                obj.InfFSur = float.Parse(GoairCpnDt.Rows[iCtr]["INFANTYQ"].ToString());
                                obj.InfOT = float.Parse(GoairCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString());
                                obj.InfTax = float.Parse(GoairCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString()) + float.Parse(GoairCpnDt.Rows[iCtr]["INFANTYQ"].ToString());
                                obj.InfFare = float.Parse(GoairCpnDt.Rows[iCtr]["INFANTTOTALFARE"].ToString());
                                if (searchInputs.IsCorp == true)
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.InfBfare.ToString()), decimal.Parse(obj.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.InfFare.ToString()));
                                    obj.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    obj.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                            }
                            #endregion

                            obj.STax = (obj.AdtSrvTax * obj.Adult) + (obj.ChdSrvTax * obj.Child) + (obj.InfSrvTax * obj.Infant);
                            obj.TotMrkUp = (obj.ADTAdminMrk * obj.Adult) + (obj.CHDAdminMrk * obj.Child) + (obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child);
                            obj.TotMgtFee = (obj.AdtMgtFee * obj.Adult) + (obj.ChdMgtFee * obj.Child) + (obj.InfMgtFee * obj.Infant);
                            obj.TotBfare = (obj.AdtBfare * obj.Adult) + (obj.ChdBFare * obj.Child) + (obj.InfBfare * obj.Infant);
                            obj.TotalFuelSur = (obj.AdtFSur * obj.Adult) + (obj.ChdFSur * obj.Child) + (obj.InfFSur * obj.Infant);
                            obj.TotalTax = (obj.AdtTax * obj.Adult) + (obj.ChdTax * obj.Child) + (obj.InfTax * obj.Infant);
                            obj.TotalFare = (obj.AdtFare * obj.Adult) + (obj.ChdFare * obj.Child) + (obj.InfFare * obj.Infant);
                            //obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + obj.TotMrkUp + obj.TotMgtFee;

                            obj.TFee = (obj.AdtTF * obj.Adult) + (obj.ChdTF * obj.Child);// +(objlist.InfTF * objlist.Infant);
                            obj.TotDis = (obj.AdtDiscount * obj.Adult) + (obj.ChdDiscount * obj.Child);
                            obj.TotCB = (obj.AdtCB * obj.Adult) + (obj.ChdCB * obj.Child);
                            obj.TotTds = (obj.AdtTds * obj.Adult) + (obj.ChdTds * obj.Child);// +objFS.InfTds;


                            //obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + obj.TotMrkUp + obj.TotMgtFee;
                            obj.TotalFare = obj.TotalFare + obj.TFee + obj.TotMrkUp + obj.TotMgtFee + obj.STax;
                            obj.NetFare = (obj.TotalFare + obj.TotTds) - ((obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child) + obj.TotDis + obj.TotCB);
                            


                            //obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + (obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child) + obj.TotMgtFee;
                            //obj.NetFare = obj.TotalFare - ((obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child));
                            //obj.TotDis = 0;
                            obj.OriginalTF = float.Parse(GoairCpnDt.Rows[iCtr]["TOTALPACKAGECOST"].ToString());
                            //obj.OriginalTT = (float.Parse(GoairCpnDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur) * obj.Adult;
                            //obj.OriginalTT = (float.Parse(GoairCpnDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur) * obj.Child;
                            //obj.OriginalTT = (float.Parse(GoairCpnDt.Rows[iCtr]["INFANTOTHERTAX"].ToString()) + obj.InfFSur) * obj.Infant;
                            srvCharge = srvChargeAdt + srvChargeChd;
                            obj.OriginalTT = srvCharge;
                            //changes by abhilash 31-aug-2013
                            obj.ADTAdminMrk = 0;
                            obj.CHDAdminMrk = 0;
                            //end
                            #endregion

                            obj.LineNumber = Convert.ToInt32(GoairCpnDt.Rows[iCtr]["LINENO"].ToString());
                            obj.Searchvalue = "";
                            obj.Track_id = "";/// SpiceCpnDt.Rows[iCtr]["Track_id"].ToString();
                            obj.FType = FType;
                            obj.Leg = iCtr + 1;
                            obj.Flight = "1";
                            obj.Provider = "LCC";
                            obj.IsCorp = searchInputs.IsCorp;
                            obj.depdatelcc = depdate;
                            obj.arrdatelcc = arrdate;
                            RetList.Add(obj);
                        }
                        catch { }
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return RetList;//CouponResTbl;
        }
        #endregion        

        public List<FlightSearchResults> GetLccCpnResultIX(FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, string TType, string FType, float srvCharge)
        {
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnectionString);
            DataTable IXCpnDt = new DataTable();
            DataTable CouponResTbl = new DataTable();
            CouponFare objCpnRes = new CouponFare();
            clsDbIndigo objclsDT = new clsDbIndigo();
            List<FlightSearchResults> RetList = new List<FlightSearchResults>();
            string arrCity, depCity = "";
            if (searchInputs.Trip.ToString() == "I")
            {
                depCity = searchInputs.HidTxtDepCity;
                arrCity = searchInputs.HidTxtArrCity;
                IXCpnDt = GetIXResult(objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), LccDepDate.Replace("-", "/"), LccRetDate.Replace("-", "/"), searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "0", "0", "IX", "True", searchInputs.TripType.ToString()), searchInputs.TripType.ToString());
            }
            else
            {
                if (FType == "InBound")
                {
                    arrCity = searchInputs.HidTxtDepCity;
                    depCity = searchInputs.HidTxtArrCity;
                    IXCpnDt = GetIXResult(objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), LccRetDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "0", "0", "IX", "True", "ONEWAY"), "ONEWAY");
                }
                else
                {
                    arrCity = searchInputs.HidTxtArrCity;
                    depCity = searchInputs.HidTxtDepCity;
                    IXCpnDt = GetIXResult(objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), LccDepDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "0", "0", "IX", "True", "ONEWAY"), "ONEWAY");
                }
            }
            try
            {

                DataTable dtIX = null;
                dtIX = IXCpnDt.DefaultView.ToTable(true, "LineItemNumber");
                for (int iCtr = 0; iCtr <= dtIX.Rows.Count - 1; iCtr++)
                {
                    DataRow[] IXArray = null;
                    IXArray = IXCpnDt.Select("LineItemNumber='" + dtIX.Rows[iCtr]["LineItemNumber"] + "'", "");
                    Hashtable IXFareADTHT = null;
                    Hashtable IXFareCHDHT = new Hashtable();
                    IXFareADTHT = GetCalcFareIX(IXArray, "ADULT");
                    IXFareCHDHT = GetCalcFareIX(IXArray, "CHILD");
                    for (int ictr1 = 0; ictr1 <= IXArray.Length - 1; ictr1++)
                    {

                        FlightSearchResults obj = new FlightSearchResults();

                        obj.OrgDestFrom = Utility.Left(depCity, 3);
                        obj.OrgDestTo = Utility.Left(arrCity, 3);
                        obj.TripType = "O";  //obj.TripType"] = "O";
                        obj.Sector = obj.OrgDestFrom + ":" + obj.OrgDestTo;
                        obj.DepartureLocation = Utility.Left(depCity, 3); //IndigoCpnDt.Rows[iCtr]["DEPARTURECITY"].ToString().Trim();
                        obj.ArrivalLocation = Utility.Left(arrCity, 3); //IndigoCpnDt.Rows[iCtr]["ARRIVALCITY"].ToString().Trim();//obj.ArrivalLocation"]                     
                        obj.DepartureCityName = IXArray[ictr1]["DEPARTURECITY"].ToString().Trim(); //obj.DepartureCityName"] = IndigoCpnDt.Rows[iCtr]["DEPARTURECITY"].ToString().Trim();                        
                        obj.ArrivalCityName = IXArray[ictr1]["ARRIVALCITY"].ToString().Trim();        //row["ArrivalCityName"]
                        obj.TripType = IXArray[ictr1]["TRIPTYPE"].ToString();
                        obj.DepartureCityName = IXArray[ictr1]["DEPARTURECITY"].ToString().Trim();
                        obj.ArrivalCityName = IXArray[ictr1]["ARRIVALCITY"].ToString().Trim();
                        obj.depdatelcc = "";
                        obj.arrdatelcc = "";
                        obj.DepartureDate = Utility.Right(IXArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(IXArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 5, 2) + Utility.Mid(IXArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 2, 2);
                        obj.Departure_Date = Utility.Right(IXArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 2) + " " + Utility.datecon(Utility.Mid(IXArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 5, 2));
                        obj.DepartureTime = convertTimeFormat(IXArray[ictr1]["DEPARTURETIME"].ToString().Trim());
                        obj.ArrivalDate = Utility.Right(IXArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(IXArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 5, 2) + Utility.Mid(IXArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 2, 2);
                        obj.Arrival_Date = Utility.Right(IXArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 2) + " " + Utility.datecon(Utility.Mid(IXArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 5, 2));
                        obj.ArrivalTime = convertTimeFormat(IXArray[ictr1]["ARRIVALTIME"].ToString().Trim());
                        obj.MarketingCarrier = Utility.Left(IXArray[ictr1]["FLIGHT"].ToString().Trim(), 2);
                        obj.AirLineName = "Air IndiaExpress";
                        obj.FlightIdentification = IXArray[ictr1]["FLIGHT"].ToString().Trim().Replace("IX-", "");
                        obj.RBD = "";
                        obj.AdtRbd = "";
                        obj.ChdRbd = "";
                        obj.InfRbd = "";
                        //IXArray[ictr1]["RBD").ToString
                        obj.AvailableSeats = "0";
                        obj.ValiDatingCarrier = "IX";
                        obj.EQ = "";

                        obj.Stops = IXArray[ictr1]["STOPS"].ToString() + "-Stop";
                        obj.fareBasis = IXArray[ictr1]["TOTALPACKAGECOST"].ToString();
                        obj.FBPaxType = "";
                        obj.LineNumber = Convert.ToInt32(IXArray[ictr1]["LineItemNumber"].ToString());
                        // iCtr + 1
                        obj.Searchvalue = "";
                        obj.TotPax = searchInputs.Adult + searchInputs.Child + searchInputs.Infant;
                        obj.Adult = searchInputs.Adult;
                        obj.Child = searchInputs.Child;
                        obj.Infant = searchInputs.Infant;
                        obj.Leg = Convert.ToInt32(IXArray[ictr1]["Leg"].ToString());
                        obj.Flight = IXArray[ictr1]["Flight1"].ToString();
                        obj.TotDur = "";
                        obj.Trip = searchInputs.Trip.ToString();
                        obj.sno = "AIINDIAEXPRESS/";

                        //Calculation of Transaction details
                        ////SMS charge calc
                        //float srvCharge = 0;
                        //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), obj.ValiDatingCarrier, searchInputs.UID);
                        ////SMS charge calc end
                        float totBFWInf = 0;
                        float totBFWOInf = 0;
                        float totFS = 0;
                        float totTax = 0;
                        double ADTAgentMrk = 0;
                        double CHDAgentMrk = 0;
                        double ADTAdminMrk = 0;
                        double CHDAdminMrk = 0;
                        int AStax = 0;
                        int CStax = 0;
                        int IStax = 0;
                        int ATF = 0;
                        int CTF = 0;
                        int ITF = 0;


                        totBFWInf = (float.Parse(IXFareADTHT["BF"].ToString()) * searchInputs.Adult) + (float.Parse(IXFareCHDHT["BF"].ToString()) * searchInputs.Child);
                        //+ (IXArray[ictr1]["INFANTBASEFARE")) * Infant)
                        totBFWOInf = totBFWInf;
                        //(IXArray[ictr1]["ADULTBASEFARE")) * Adult) + (IXArray[ictr1]["CHILDBASEFARE"] * Child) '(IXArray[ictr1]["BaseFareAmt_Adt") * dTotalNo_Adt) + (IXArray[ictr1]["BaseFareAmt_Chd") * dTotalNo_Chd)
                        totFS = (float.Parse(IXFareADTHT["YQ"].ToString()) * searchInputs.Adult) + (float.Parse(IXFareCHDHT["YQ"].ToString()) * searchInputs.Child);
                        totTax = (float.Parse(IXFareADTHT["OT"].ToString()) * searchInputs.Adult) + (float.Parse(IXFareCHDHT["OT"].ToString()) * searchInputs.Child);
                        AStax = 0;
                        ATF = 0;
                        CStax = 0;
                        CTF = 0;
                        IStax = 0;
                        ITF = 0;
                        // HsTblSTax("TF") * Infant

                        ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Utility.Left(IXArray[ictr1]["FLIGHT"].ToString().Trim(), 2), (Convert.ToDouble(IXFareADTHT["BF"].ToString()) + Convert.ToDouble(IXFareADTHT["YQ"].ToString()) + Convert.ToDouble(IXFareADTHT["OT"].ToString())), searchInputs.Trip.ToString());
                        if (searchInputs.Child > 0)
                            CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], Utility.Left(IXArray[ictr1]["FLIGHT"].ToString().Trim(), 2), (Convert.ToDouble(IXFareCHDHT["BF"].ToString()) + Convert.ToDouble(IXFareCHDHT["YQ"].ToString()) + Convert.ToDouble(IXFareCHDHT["OT"].ToString())), searchInputs.Trip.ToString());
                        else
                            CHDAgentMrk = 0;
                        ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], Utility.Left(IXArray[ictr1]["FLIGHT"].ToString().Trim(), 2), (Convert.ToDouble(IXFareADTHT["BF"].ToString()) + Convert.ToDouble(IXFareADTHT["YQ"].ToString()) + Convert.ToDouble(IXFareADTHT["OT"].ToString())), searchInputs.Trip.ToString());
                        if (searchInputs.Child > 0)
                            CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], Utility.Left(IXArray[ictr1]["FLIGHT"].ToString().Trim(), 2), (Convert.ToDouble(IXFareCHDHT["BF"].ToString()) + Convert.ToDouble(IXFareCHDHT["YQ"].ToString()) + Convert.ToDouble(IXFareCHDHT["OT"].ToString())), searchInputs.Trip.ToString());
                        else
                            CHDAgentMrk = 0;
                        double totMrk = 0;
                        ADTAdminMrk = Convert.ToDouble(IXFareADTHT["TC"].ToString());
                        CHDAdminMrk = Convert.ToDouble(IXFareCHDHT["TC"].ToString());
                        totMrk = ADTAdminMrk * searchInputs.Adult;
                        totMrk = totMrk + ADTAgentMrk * searchInputs.Adult;
                        totMrk = totMrk + CHDAdminMrk * searchInputs.Child;
                        totMrk = totMrk + CHDAgentMrk * searchInputs.Child;
                        //Calculation of Transaction details end

                        obj.AdtFareType = "Refundable";
                        obj.AdtBfare = float.Parse(IXFareADTHT["BF"].ToString());
                        obj.AdtFSur = float.Parse(IXFareADTHT["YQ"].ToString());
                        obj.AdtOT = float.Parse(IXFareADTHT["OT"].ToString());
                        obj.AdtTax = float.Parse(IXFareADTHT["OT"].ToString()) + float.Parse(IXFareADTHT["YQ"].ToString());
                        obj.AdtFare = float.Parse(IXFareADTHT["TF"].ToString());

                        //SMS charge add 
                        obj.AdtOT = obj.AdtOT + srvCharge;
                        obj.AdtTax = obj.AdtTax + srvCharge;
                        obj.AdtFare = obj.AdtFare + srvCharge;
                        //SMS charge add end
                        if (searchInputs.IsCorp == true)
                        {
                            obj.AdtBfare = obj.AdtBfare + float.Parse(ADTAdminMrk.ToString());
                            obj.AdtFare = obj.AdtFare + float.Parse(ADTAdminMrk.ToString());

                            DataTable MGDT = new DataTable();
                            MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.AdtBfare.ToString()), decimal.Parse(obj.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.AdtFare.ToString()));
                            obj.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                            obj.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                        }
                        if (obj.Child > 0)
                        {
                            obj.ChdBFare = float.Parse(IXFareCHDHT["BF"].ToString());
                            obj.ChdFSur = float.Parse(IXFareCHDHT["YQ"].ToString());
                            obj.ChdOT = float.Parse(IXFareCHDHT["OT"].ToString());
                            obj.ChdTax = float.Parse(IXFareCHDHT["OT"].ToString()) + float.Parse(IXFareCHDHT["YQ"].ToString());
                            obj.ChdFare = float.Parse(IXFareCHDHT["TF"].ToString());

                            //SMS charge add 
                            obj.ChdOT = obj.ChdOT + srvCharge;
                            obj.ChdTax = obj.ChdTax + srvCharge;
                            obj.ChdFare = obj.ChdFare + srvCharge;
                            //SMS charge add end
                            if (searchInputs.IsCorp == true)
                            {
                                obj.ChdBFare = obj.ChdBFare + float.Parse(CHDAdminMrk.ToString());
                                obj.ChdFare = obj.ChdFare + float.Parse(CHDAdminMrk.ToString());

                                DataTable MGDT = new DataTable();
                                MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.ChdBFare.ToString()), decimal.Parse(obj.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.ChdFare.ToString()));
                                obj.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                obj.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                            }
                        }

                        if (obj.Infant > 0)
                        {
                            obj.InfBfare = 0;
                            obj.InfFSur = 0;
                            obj.InfOT = 0;
                            obj.InfTax = 0;
                            obj.InfFare = 0;
                        }

                        obj.STax = (obj.AdtSrvTax * obj.Adult) + (obj.ChdSrvTax * obj.Child) + (obj.InfSrvTax * obj.Infant);//AStax + CStax + IStax;
                        obj.TFee = ATF + CTF + ITF;
                        obj.IATAComm = 0;
                        obj.ADTAgentMrk = float.Parse(ADTAgentMrk.ToString());
                        obj.CHDAgentMrk = float.Parse(CHDAgentMrk.ToString());
                        obj.ADTAdminMrk = float.Parse(ADTAdminMrk.ToString());
                        obj.CHDAdminMrk = float.Parse(CHDAdminMrk.ToString());
                        obj.TotMrkUp = float.Parse(totMrk.ToString());
                        obj.TotMgtFee = (obj.AdtMgtFee * obj.Adult) + (obj.ChdMgtFee * obj.Child) + (obj.InfMgtFee * obj.Infant);
                        //obj.TotBfare = float.Parse(totBFWInf.ToString());
                        //obj.TotalTax = totTax + totFS;
                        //obj.TotalFare = (totBFWInf + totTax + obj.STax + obj.TFee + float.Parse(totMrk.ToString()) + totFS) + (srvCharge * obj.Adult) + (srvCharge * obj.Child);

                        obj.TotBfare = (obj.AdtBfare * obj.Adult) + (obj.ChdBFare * obj.Child) + (obj.InfBfare * obj.Infant);
                        obj.TotalFuelSur = (obj.AdtFSur * obj.Adult) + (obj.ChdFSur * obj.Child) + (obj.InfFSur * obj.Infant);
                        obj.TotalTax = (obj.AdtTax * obj.Adult) + (obj.ChdTax * obj.Child) + (obj.InfTax * obj.Infant);
                        obj.TotalFare = (obj.AdtFare * obj.Adult) + (obj.ChdFare * obj.Child) + (obj.InfFare * obj.Infant);
                        if (searchInputs.IsCorp == true)
                            obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + (obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child) + obj.TotMgtFee;
                        else
                            obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + obj.TotMrkUp;

                        obj.NetFare = obj.TotalFare - ((obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child));

                        obj.TotDis = 0;
                        obj.OriginalTF = float.Parse(totBFWInf.ToString()) + totTax + totFS;//obj.TotalFare;
                        obj.OriginalTT = srvCharge;// totTax + totFS;
                        obj.Track_id = "";// IXArray[ictr1]["Track_id"].ToString();
                        obj.FType = FType;
                        obj.Flight = IXArray[ictr1]["Flight1"].ToString();
                        obj.Provider = "LCC";
                        obj.TotDis = 0;
                        obj.IsCorp = searchInputs.IsCorp;
                        //obj.NetFare = obj.TotalFare - ((obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child));
                        RetList.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return RetList;

        }

        //No Changes in These
        private DataTable GetIXResult(DataTable IXCpnDt, string TourType)
        {
            DataTable IXDt = new DataTable();
            DataRow[] IXResArrayO = null;
            DataRow[] IXResArrayR = null;
            List<FlightSearchResults> RetList = new List<FlightSearchResults>();
            if (IXCpnDt.Rows.Count > 0)
            {
                IXResArrayO = IXCpnDt.Select("TRIPTYPE='O' and ADULTTOTALFARE<>'0'", "");
                IXResArrayR = IXCpnDt.Select("TRIPTYPE='R' and ADULTTOTALFARE<>'0'", "");
                IXDt = IXCpnDt.Clone();
                IXDt.Clear();
                DataColumn AirDataColumn = null;
                AirDataColumn = new DataColumn();
                AirDataColumn.DataType = Type.GetType("System.String");
                AirDataColumn.ColumnName = "Leg";
                IXDt.Columns.Add(AirDataColumn);

                AirDataColumn = new DataColumn();
                AirDataColumn.DataType = Type.GetType("System.String");
                AirDataColumn.ColumnName = "Flight1";
                IXDt.Columns.Add(AirDataColumn);

                AirDataColumn = new DataColumn();
                AirDataColumn.DataType = Type.GetType("System.String");
                AirDataColumn.ColumnName = "LineItemNumber";
                IXDt.Columns.Add(AirDataColumn);
                int cnt = 1;
                if (TourType == "RoundTrip")
                {
                    if (IXResArrayO.Length > 0 & IXResArrayR.Length > 0)
                    {
                        for (int i = 0; i <= IXResArrayO.Length - 1; i++)
                        {

                            for (int ii = 0; ii <= IXResArrayR.Length - 1; ii++)
                            {
                                IXDt.ImportRow(IXResArrayO[i]);
                                IXDt.Rows[IXDt.Rows.Count - 1]["Leg"] = "1";
                                IXDt.Rows[IXDt.Rows.Count - 1]["Flight1"] = "1";
                                IXDt.Rows[IXDt.Rows.Count - 1]["LineItemNumber"] = cnt.ToString();
                                IXDt.ImportRow(IXResArrayR[ii]);
                                IXDt.Rows[IXDt.Rows.Count - 1]["Leg"] = "1";
                                IXDt.Rows[IXDt.Rows.Count - 1]["Flight1"] = "2";
                                IXDt.Rows[IXDt.Rows.Count - 1]["LineItemNumber"] = cnt.ToString();
                                cnt += 1;
                            }
                        }
                    }
                }
                else
                {
                    if (IXResArrayO.Length > 0)
                    {
                        for (int i = 0; i <= IXResArrayO.Length - 1; i++)
                        {
                            IXDt.ImportRow(IXResArrayO[i]);
                            IXDt.Rows[IXDt.Rows.Count - 1]["Leg"] = "1";
                            IXDt.Rows[IXDt.Rows.Count - 1]["Flight1"] = "1";
                            IXDt.Rows[IXDt.Rows.Count - 1]["LineItemNumber"] = cnt.ToString();
                            cnt += 1;
                        }
                    }
                }
            }
            return IXDt;
        }

        private Hashtable GetCalcFareIX(DataRow[] IXArray, string PaxType)
        {
            double BF = 0;
            double YQ = 0;
            double OT = 0;
            double TC = 0;
            double TF = 0;
            Hashtable IXFareHT = new Hashtable();
            for (int i = 0; i <= IXArray.Length - 1; i++)
            {
                BF += Convert.ToDouble((IXArray[i][PaxType + "BASEFARE"]));
                YQ += Convert.ToDouble((IXArray[i][PaxType + "YQ"]));
                OT += Convert.ToDouble((IXArray[i][PaxType + "OTHERTAX"]));
                TC += Convert.ToDouble((IXArray[i][PaxType + "TC"]));
                TF += Convert.ToDouble((IXArray[i][PaxType + "TOTALFARE"]));
            }
            IXFareHT.Add("BF", BF);
            IXFareHT.Add("YQ", YQ);
            IXFareHT.Add("OT", OT);
            IXFareHT.Add("TC", TC);
            IXFareHT.Add("TF", TF);

            return IXFareHT;
        }

        public List<FlightSearchResults> GetOfflineFlightResult(FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, string TType, string FType, List<MISCCharges> MiscList)
        {
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnectionString);
            DataTable GoairOfflineDt = new DataTable();
            List<FlightSearchResults> RetList = new List<FlightSearchResults>();
            //List<BaggageList> BagInfoList = new List<BaggageList>();
            //BagInfoList = objFltComm.GetBaggage(searchInputs.Trip.ToString(), "G8");
            string arrCity, depCity = "";
            string arrdate, depdate = "", airl = "";
            if (searchInputs.HidTxtAirLine.Length > 1) { airl = Utility.Right(searchInputs.HidTxtAirLine, 2); }
            if (FType == "InBound")
            {
                arrCity = searchInputs.HidTxtDepCity;
                depCity = searchInputs.HidTxtArrCity;
                depdate = Utility.Left(searchInputs.RetDate, 2) + Utility.Mid(searchInputs.RetDate, 3, 2) + Utility.Right(searchInputs.RetDate, 2);
                arrdate = Utility.Left(searchInputs.DepDate, 2) + Utility.Mid(searchInputs.DepDate, 3, 2) + Utility.Right(searchInputs.DepDate, 2);
                GoairOfflineDt = objFltComm.GetofflineResult(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), depdate, airl, searchInputs.Trip.ToString());
            }
            else
            {
                arrCity = searchInputs.HidTxtArrCity;
                depCity = searchInputs.HidTxtDepCity;
                depdate = Utility.Left(searchInputs.DepDate, 2) + Utility.Mid(searchInputs.DepDate, 3, 2) + Utility.Right(searchInputs.DepDate, 2);
                arrdate = Utility.Left(searchInputs.RetDate, 2) + Utility.Mid(searchInputs.RetDate, 3, 2) + Utility.Right(searchInputs.RetDate, 2);
                GoairOfflineDt = objFltComm.GetofflineResult(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), depdate, airl, searchInputs.Trip.ToString());
            }
            try
            {

                if (GoairOfflineDt.Rows.Count > 0)
                {

                    for (int iCtr = 0; iCtr <= GoairOfflineDt.Rows.Count - 1; iCtr++)
                    {
                        try
                        {
                            float srvCharge = 0;
                            // if (searchInputs.Trip.ToString() == "I")
                            //srvCharge = objFltComm.MISCServiceFee(MiscList, GoairOfflineDt.Rows[iCtr]["ValiDatingCarrier"].ToString().Trim());

                            srvCharge = float.Parse(GoairOfflineDt.Rows[iCtr]["SrvCharge"].ToString());
                            FlightSearchResults obj = new FlightSearchResults();
                            #region Flight Details
                            obj.OrgDestFrom = Utility.Left(depCity, 3);
                            obj.OrgDestTo = Utility.Left(arrCity, 3);
                            obj.TripType = "O";  //obj.TripType"] = "O";
                            obj.Sector = obj.OrgDestFrom + ":" + obj.OrgDestTo;

                            obj.DepartureLocation = GoairOfflineDt.Rows[iCtr]["DepartureLocation"].ToString().Trim();
                            obj.ArrivalLocation = GoairOfflineDt.Rows[iCtr]["ArrivalLocation"].ToString().Trim();
                            obj.DepartureCityName = ((from ct in CityList where ct.AirportCode == obj.DepartureLocation select ct).ToList())[0].City;
                            obj.ArrivalCityName = ((from ct in CityList where ct.AirportCode == obj.ArrivalLocation select ct).ToList())[0].City;
                            obj.depdatelcc = "";
                            obj.arrdatelcc = "";
                            obj.DepartureDate = GoairOfflineDt.Rows[iCtr]["DepartureDate"].ToString().Trim();
                            obj.Departure_Date = Utility.Left(GoairOfflineDt.Rows[iCtr]["DepartureDate"].ToString().Trim(), 2) + " " + Utility.datecon(Utility.Mid(GoairOfflineDt.Rows[iCtr]["DepartureDate"].ToString().Trim(), 2, 2)); //datecon(
                            obj.DepartureTime = GoairOfflineDt.Rows[iCtr]["DepartureTime"].ToString().Trim();
                            obj.ArrivalDate = GoairOfflineDt.Rows[iCtr]["ArrivalDate"].ToString().Trim();
                            obj.Arrival_Date = Utility.Left(GoairOfflineDt.Rows[iCtr]["ArrivalDate"].ToString().Trim(), 2) + " " + Utility.datecon(Utility.Mid(GoairOfflineDt.Rows[iCtr]["ArrivalDate"].ToString().Trim(), 2, 2)); //datecon(
                            obj.ArrivalTime = GoairOfflineDt.Rows[iCtr]["ArrivalTime"].ToString().Trim();
                            obj.MarketingCarrier = GoairOfflineDt.Rows[iCtr]["MarketingCarrier"].ToString().Trim();
                            obj.AirLineName = GoairOfflineDt.Rows[iCtr]["AirlineName"].ToString().Trim();
                            obj.FlightIdentification = GoairOfflineDt.Rows[iCtr]["FlightNo"].ToString().Trim();// Utility.Mid(GoairOfflineDt.Rows[iCtr]["FLIGHT"].ToString().Trim(), 3, GoairOfflineDt.Rows[iCtr]["FLIGHT"].ToString().Trim().Length - 1);
                            obj.RBD = "";
                            obj.AdtRbd = "";
                            obj.ChdRbd = "";
                            //GoairOfflineDt.Rows(iCtr).Item("RBD").ToString
                            obj.AvailableSeats = "0";
                            obj.ValiDatingCarrier = GoairOfflineDt.Rows[iCtr]["ValiDatingCarrier"].ToString().Trim();
                            obj.EQ = "";
                            obj.Stops = GoairOfflineDt.Rows[iCtr]["Stops"].ToString().Trim() + "-Stop";
                            obj.fareBasis = "";
                            obj.FBPaxType = "";
                            obj.TotPax = searchInputs.Adult + searchInputs.Child;
                            obj.Adult = searchInputs.Adult;
                            obj.Child = searchInputs.Child;
                            obj.Infant = searchInputs.Infant;
                            obj.TotDur = "";
                            obj.Trip = searchInputs.Trip.ToString();
                            obj.sno = GoairOfflineDt.Rows[iCtr]["AirlineName"].ToString().Trim().ToUpper() + "SPECIAL/OFFLINERESULT";
                            //obj.BagInfo = ((from bg in BagInfoList where bg.Class == "Economy" select bg).ToList())[0].Weight;
                            #endregion

                            #region Fare Details

                            ////SMS charge calc
                            //float srvCharge = 0;
                            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), obj.ValiDatingCarrier, searchInputs.UID);
                            ////SMS charge calc end                                                        

                            #region ADT Fare
                            obj.AdtFareType = GoairOfflineDt.Rows[iCtr]["FareType"].ToString(); //"Spl. Fare, No Commission";
                            obj.AdtBfare = float.Parse(GoairOfflineDt.Rows[iCtr]["BaseFare"].ToString());
                            obj.AdtFSur = float.Parse(GoairOfflineDt.Rows[iCtr]["YQ"].ToString());
                            obj.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(GoairOfflineDt.Rows[iCtr]["TotalFare"].ToString().Trim()), searchInputs.Trip.ToString());
                            obj.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(GoairOfflineDt.Rows[iCtr]["TotalFare"].ToString().Trim()), searchInputs.Trip.ToString());

                            obj.AdtTax = float.Parse(GoairOfflineDt.Rows[iCtr]["OT"].ToString()) + obj.AdtFSur + obj.ADTAdminMrk;
                            obj.AdtOT = float.Parse(GoairOfflineDt.Rows[iCtr]["OT"].ToString()) + obj.ADTAdminMrk;
                            obj.AdtFare = float.Parse(GoairOfflineDt.Rows[iCtr]["TotalFare"].ToString()) + obj.ADTAdminMrk;

                            //SMS charge add 
                            obj.AdtOT = obj.AdtOT + srvCharge;
                            obj.AdtTax = obj.AdtTax + srvCharge;
                            obj.AdtFare = obj.AdtFare + srvCharge;
                            //SMS charge add end
                            if (searchInputs.IsCorp == true)
                            {
                                DataTable MGDT = new DataTable();
                                MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.AdtBfare.ToString()), decimal.Parse(obj.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.AdtFare.ToString()));
                                obj.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                obj.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                            }
                            else
                            {
                                //obj.AdtTax = float.Parse(GoairOfflineDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur;
                                //obj.AdtOT = float.Parse(GoairOfflineDt.Rows[iCtr]["ADULTOTHERTAX"].ToString());
                                //obj.AdtFare = float.Parse(GoairOfflineDt.Rows[iCtr]["ADULTTOTALFARE"].ToString());

                                ////SMS charge add 
                                //obj.AdtOT = obj.AdtOT + srvCharge;
                                //obj.AdtTax = obj.AdtTax + srvCharge;
                                //obj.AdtFare = obj.AdtFare + srvCharge;
                                ////SMS charge add end
                            }
                            #endregion

                            #region CHD Fare
                            if (obj.Child > 0)
                            {
                                obj.ChdBFare = float.Parse(GoairOfflineDt.Rows[iCtr]["BaseFare"].ToString());
                                obj.ChdFSur = float.Parse(GoairOfflineDt.Rows[iCtr]["YQ"].ToString());
                                obj.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(GoairOfflineDt.Rows[iCtr]["TotalFare"].ToString().Trim()), searchInputs.Trip.ToString());
                                obj.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(GoairOfflineDt.Rows[iCtr]["TotalFare"].ToString().Trim()), searchInputs.Trip.ToString());

                                obj.ChdOT = float.Parse(GoairOfflineDt.Rows[iCtr]["OT"].ToString()) + obj.CHDAdminMrk;
                                obj.ChdTax = float.Parse(GoairOfflineDt.Rows[iCtr]["OT"].ToString()) + obj.ChdFSur + obj.CHDAdminMrk;
                                obj.ChdFare = float.Parse(GoairOfflineDt.Rows[iCtr]["TotalFare"].ToString()) + obj.CHDAdminMrk;

                                //SMS charge add 
                                obj.ChdOT = obj.ChdOT + srvCharge;
                                obj.ChdTax = obj.ChdTax + srvCharge;
                                obj.ChdFare = obj.ChdFare + srvCharge;
                                //SMS charge add end
                                if (searchInputs.IsCorp == true)
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.ChdBFare.ToString()), decimal.Parse(obj.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.ChdFare.ToString()));
                                    obj.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    obj.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                                else
                                {
                                    //obj.ChdOT = float.Parse(GoairOfflineDt.Rows[iCtr]["CHILDOTHERTAX"].ToString());
                                    //obj.ChdTax = float.Parse(GoairOfflineDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur;
                                    //obj.ChdFare = float.Parse(GoairOfflineDt.Rows[iCtr]["CHILDTOTALFARE"].ToString());

                                    ////SMS charge add 
                                    //obj.ChdOT = obj.ChdOT + srvCharge;
                                    //obj.ChdTax = obj.ChdTax + srvCharge;
                                    //obj.ChdFare = obj.ChdFare + srvCharge;
                                    ////SMS charge add end
                                }
                            }
                            #endregion

                            #region INF Fare
                            if (obj.Infant > 0)
                            {
                                obj.InfBfare = float.Parse(GoairOfflineDt.Rows[iCtr]["InfBaseFare"].ToString());
                                obj.InfFSur = float.Parse(GoairOfflineDt.Rows[iCtr]["InfYQ"].ToString());
                                obj.InfOT = float.Parse(GoairOfflineDt.Rows[iCtr]["InfOT"].ToString());
                                obj.InfTax = float.Parse(GoairOfflineDt.Rows[iCtr]["InfOT"].ToString()) + float.Parse(GoairOfflineDt.Rows[iCtr]["InfYQ"].ToString());
                                obj.InfFare = float.Parse(GoairOfflineDt.Rows[iCtr]["InfTotalFare"].ToString());

                                if (searchInputs.IsCorp == true)
                                {
                                    DataTable MGDT = new DataTable();
                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.InfBfare.ToString()), decimal.Parse(obj.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.InfFare.ToString()));
                                    obj.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                    obj.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                }
                            }
                            #endregion

                            obj.STax = (obj.AdtSrvTax * obj.Adult) + (obj.ChdSrvTax * obj.Child) + (obj.InfSrvTax * obj.Infant);
                            obj.TotMrkUp = (obj.ADTAdminMrk * obj.Adult) + (obj.CHDAdminMrk * obj.Child) + (obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child);
                            obj.TotMgtFee = (obj.AdtMgtFee * obj.Adult) + (obj.ChdMgtFee * obj.Child) + (obj.InfMgtFee * obj.Infant);
                            obj.TotBfare = (obj.AdtBfare * obj.Adult) + (obj.ChdBFare * obj.Child) + (obj.InfBfare * obj.Infant);
                            obj.TotalFuelSur = (obj.AdtFSur * obj.Adult) + (obj.ChdFSur * obj.Child) + (obj.InfFSur * obj.Infant);
                            obj.TotalTax = (obj.AdtTax * obj.Adult) + (obj.ChdTax * obj.Child) + (obj.InfTax * obj.Infant);
                            obj.TotalFare = (obj.AdtFare * obj.Adult) + (obj.ChdFare * obj.Child) + (obj.InfFare * obj.Infant);
                            //obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + obj.TotMrkUp + obj.TotMgtFee;
                            obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + (obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child) + obj.TotMgtFee;
                            obj.NetFare = obj.TotalFare - ((obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child));
                            obj.TotDis = 0;
                            obj.OriginalTF = obj.NetFare;
                            //obj.OriginalTT = (float.Parse(GoairOfflineDt.Rows[iCtr]["ADULTOTHERTAX"].ToString()) + obj.AdtFSur) * obj.Adult;
                            //obj.OriginalTT = (float.Parse(GoairOfflineDt.Rows[iCtr]["CHILDOTHERTAX"].ToString()) + obj.ChdFSur) * obj.Child;
                            //obj.OriginalTT = (float.Parse(GoairOfflineDt.Rows[iCtr]["INFANTOTHERTAX"].ToString()) + obj.InfFSur) * obj.Infant;
                            obj.OriginalTT = srvCharge;
                            //changes by abhilash 31-aug-2013
                            obj.ADTAdminMrk = 0;
                            obj.CHDAdminMrk = 0;
                            //end
                            #endregion

                            obj.Track_id = "";/// SpiceCpnDt.Rows[iCtr]["Track_id"].ToString();
                            obj.FType = FType;
                            obj.LineNumber = iCtr + 1;
                            obj.Searchvalue = "";
                            obj.Leg = 1;
                            obj.Flight = "1";
                            obj.Provider = searchInputs.Provider;
                            obj.IsCorp = searchInputs.IsCorp;
                            obj.depdatelcc = depdate;
                            obj.arrdatelcc = arrdate;
                            RetList.Add(obj);
                        }
                        catch { }
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return RetList;
        }


        #region Air Asia

        private DataTable GetAirAsiaResult(DataTable AKCpnDt, string TourType)
        {
            DataTable AKDt = new DataTable();
            DataRow[] AKResArrayO = null;
            DataRow[] AKResArrayR = null;
            List<FlightSearchResults> RetList = new List<FlightSearchResults>();
            if (AKCpnDt.Rows.Count > 0)
            {

                AKDt = AKCpnDt.Clone();
                AKDt.Clear();
                DataColumn AirDataColumn = null;
                AirDataColumn = new DataColumn();
                AirDataColumn.DataType = Type.GetType("System.String");
                AirDataColumn.ColumnName = "Leg";
                AKDt.Columns.Add(AirDataColumn);

                AirDataColumn = new DataColumn();
                AirDataColumn.DataType = Type.GetType("System.String");
                AirDataColumn.ColumnName = "Flight1";
                AKDt.Columns.Add(AirDataColumn);

                AirDataColumn = new DataColumn();
                AirDataColumn.DataType = Type.GetType("System.String");
                AirDataColumn.ColumnName = "LineItemNumber";
                AKDt.Columns.Add(AirDataColumn);
                int cnt = 1;
                if (TourType == "RoundTrip")
                {
                    DataTable aDt = new DataTable();
                    aDt = AKCpnDt.DefaultView.ToTable(true, "LINENO");
                    for (int k = 0; k <= aDt.Rows.Count - 1; k++)
                    {
                        AKResArrayO = AKCpnDt.Select("TRIPTYPE='O' and ADULTTOTALFARE<>'0' and LINENO='" + (k + 1) + "'", "");
                        AKResArrayR = AKCpnDt.Select("TRIPTYPE='R' and ADULTTOTALFARE<>'0' and LINENO='" + (k + 1) + "'", "");
                        if (AKResArrayO.Length > 0)
                        {
                            for (int i = 0; i <= AKResArrayO.Length - 1; i++)
                            {
                                AKDt.ImportRow(AKResArrayO[i]);
                                AKDt.Rows[AKDt.Rows.Count - 1]["Leg"] = (i + 1).ToString();
                                AKDt.Rows[AKDt.Rows.Count - 1]["Flight1"] = "1";
                                AKDt.Rows[AKDt.Rows.Count - 1]["LineItemNumber"] = k + 1;
                                AKDt.Rows[AKDt.Rows.Count - 1]["Stops"] = AKResArrayO.Count() - 1;
                            }
                        }
                        if (AKResArrayR.Length > 0)
                        {
                            for (int ii = 0; ii <= AKResArrayR.Length - 1; ii++)
                            {
                                AKDt.ImportRow(AKResArrayR[ii]);
                                AKDt.Rows[AKDt.Rows.Count - 1]["Leg"] = (ii + 1).ToString();
                                AKDt.Rows[AKDt.Rows.Count - 1]["Flight1"] = "2";
                                AKDt.Rows[AKDt.Rows.Count - 1]["LineItemNumber"] = k + 1;
                                AKDt.Rows[AKDt.Rows.Count - 1]["Stops"] = AKResArrayR.Count() - 1;
                            }
                        }
                    }

                }
                else
                {
                    DataTable aDt = new DataTable();
                    aDt = AKCpnDt.DefaultView.ToTable(true, "LINENO");
                    for (int k = 0; k <= aDt.Rows.Count - 1; k++)
                    {
                        AKResArrayO = AKCpnDt.Select("TRIPTYPE='O' and ADULTTOTALFARE<>'0' and LINENO='" + (k + 1) + "'", "");
                        if (AKResArrayO.Length > 0)
                        {
                            for (int i = 0; i <= AKResArrayO.Length - 1; i++)
                            {
                                AKDt.ImportRow(AKResArrayO[i]);
                                AKDt.Rows[AKDt.Rows.Count - 1]["Leg"] = (i + 1).ToString();
                                AKDt.Rows[AKDt.Rows.Count - 1]["Flight1"] = "1";
                                AKDt.Rows[AKDt.Rows.Count - 1]["LineItemNumber"] = k + 1;
                                AKDt.Rows[AKDt.Rows.Count - 1]["Stops"] = AKResArrayO.Count() - 1;
                            }
                        }
                    }
                }
            }
            return AKDt;
        }

        private Hashtable GetCalcFareAirAsia(DataRow[] AKArray, string PaxType)
        {
            double BF = 0;
            double YQ = 0;
            double OT = 0;
            double TC = 0;
            double TF = 0;
            Hashtable AKFareHT = new Hashtable();
            bool isOfr = false;
            bool isRfr = false;
            for (int i = 0; i <= AKArray.Length - 1; i++)
            {
                if (AKArray[i]["TRIPTYPE"].ToString() == "O" && isOfr == false)
                {
                    isOfr = true;
                    BF += Convert.ToDouble((AKArray[i][PaxType + "BASEFARE"]));
                    YQ += Convert.ToDouble((AKArray[i][PaxType + "YQ"]));
                    OT += Convert.ToDouble((AKArray[i][PaxType + "OTHERTAX"]));
                    TC += Convert.ToDouble((AKArray[i][PaxType + "TC"]));
                    TF += Convert.ToDouble((AKArray[i][PaxType + "TOTALFARE"]));
                }
                if (AKArray[i]["TRIPTYPE"].ToString() == "R" && isRfr == false)
                {
                    isRfr = true;
                    BF += Convert.ToDouble((AKArray[i][PaxType + "BASEFARE"]));
                    YQ += Convert.ToDouble((AKArray[i][PaxType + "YQ"]));
                    OT += Convert.ToDouble((AKArray[i][PaxType + "OTHERTAX"]));
                    TC += Convert.ToDouble((AKArray[i][PaxType + "TC"]));
                    TF += Convert.ToDouble((AKArray[i][PaxType + "TOTALFARE"]));
                }
            }
            AKFareHT.Add("BF", BF);
            AKFareHT.Add("YQ", YQ);
            AKFareHT.Add("OT", OT);
            AKFareHT.Add("TC", TC);
            AKFareHT.Add("TF", TF);

            return AKFareHT;
        }

        public List<FlightSearchResults> GetLccCpnResultAirAsia(FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, List<FlightCityList> CityList, List<AirlineList> Airlist, DataSet MarkupDs, string TType, string FType, float srvCharge)
        {
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnectionString);
            DataTable AKCpnDt = null;
            DataTable CouponResTbl = new DataTable();
            CouponFare objCpnRes = new CouponFare();
            clsDbIndigo objclsDT = new clsDbIndigo();
            List<FlightSearchResults> RetList = new List<FlightSearchResults>();
            string arrCity, depCity = "";
            try
            {
                if (searchInputs.Trip.ToString() == "D")
                {
                    if (searchInputs.GDSRTF == false)
                    {
                        if (FType == "InBound")
                        {
                            arrCity = searchInputs.HidTxtDepCity;
                            depCity = searchInputs.HidTxtArrCity;
                            AKCpnDt = GetAirAsiaResult(objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), searchInputs.RetDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "0", "0", "AK", "True", "ONEWAY"), ""); //TourType - last
                        }
                        else
                        {
                            arrCity = searchInputs.HidTxtArrCity;
                            depCity = searchInputs.HidTxtDepCity;
                            AKCpnDt = GetAirAsiaResult(objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), searchInputs.DepDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "0", "0", "AK", "True", "ONEWAY"), ""); //TourType - last
                        }
                    }
                    else
                    {
                        arrCity = searchInputs.HidTxtArrCity;
                        depCity = searchInputs.HidTxtDepCity;
                        AKCpnDt = GetAirAsiaResult(objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), searchInputs.DepDate.Replace("-", "/"), searchInputs.RetDate.Replace("-", "/"), searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "0", "0", "AK", "True", "ROUNDTRIP"), "RoundTrip"); //TourType - last
                    }
                }
                else
                {
                    if (FType == "InBound")
                    {
                        arrCity = searchInputs.HidTxtArrCity;
                        depCity = searchInputs.HidTxtDepCity;
                        AKCpnDt = GetAirAsiaResult(objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), searchInputs.DepDate.Replace("-", "/"), searchInputs.RetDate.Replace("-", "/"), searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "0", "0", "AK", "True", "ROUNDTRIP"), "RoundTrip"); //TourType - last
                    }
                    else
                    {
                        arrCity = searchInputs.HidTxtArrCity;
                        depCity = searchInputs.HidTxtDepCity;
                        AKCpnDt = GetAirAsiaResult(objCpnRes.getFlightResultTable(Utility.Left(depCity, 3), Utility.Left(arrCity, 3), searchInputs.DepDate.Replace("-", "/"), "", searchInputs.Adult.ToString(), searchInputs.Child.ToString(), searchInputs.Infant.ToString(), "0", "0", "AK", "True", "ONEWAY"), ""); //TourType - last
                    }
                }

                CouponResTbl = objclsDT.ResultTable();
                DataRow row = null;
                //Conn.Open();
                DataTable dtAK = null;
                dtAK = AKCpnDt.DefaultView.ToTable(true, "LineItemNumber");

                for (int iCtr = 0; iCtr <= dtAK.Rows.Count - 1; iCtr++)
                {
                    DataRow[] AKArray = null;
                    AKArray = AKCpnDt.Select("LineItemNumber='" + dtAK.Rows[iCtr]["LineItemNumber"] + "'", "");
                    Hashtable AKFareADTHT = new Hashtable();
                    Hashtable AKFareCHDHT = new Hashtable();
                    Hashtable AKFareINFHT = new Hashtable();
                    AKFareADTHT = GetCalcFareAirAsia(AKArray, "ADULT");
                    AKFareCHDHT = GetCalcFareAirAsia(AKArray, "CHILD");
                    AKFareINFHT = GetCalcFareAirAsia(AKArray, "INFANT");
                    for (int ictr1 = 0; ictr1 <= AKArray.Length - 1; ictr1++)
                    {
                        row = CouponResTbl.NewRow();
                        FlightSearchResults obj = new FlightSearchResults();
                        #region Flight Details
                        obj.OrgDestFrom = Utility.Left(AKArray[ictr1]["SECTOR"].ToString().Trim(), 3);
                        obj.OrgDestTo = Utility.Right(AKArray[ictr1]["SECTOR"].ToString().Trim(), 3);
                        if (searchInputs.Trip.ToString() == "I")
                        {
                            if (searchInputs.TripType == TripType.OneWay)
                            {
                                obj.Sector = Utility.Left(searchInputs.HidTxtDepCity, 3) + ":" + Utility.Left(searchInputs.HidTxtArrCity, 3);
                            }
                            else
                            {
                                obj.Sector = Utility.Left(searchInputs.HidTxtDepCity, 3) + ":" + Utility.Left(searchInputs.HidTxtArrCity, 3) + ":" + Utility.Left(searchInputs.HidTxtDepCity, 3);
                            }
                        }
                        else
                        {
                            if (searchInputs.RTF == true)
                            { obj.Sector = obj.OrgDestFrom + ":" + obj.OrgDestTo + ":" + obj.OrgDestFrom; }
                            else
                            { obj.Sector = obj.OrgDestFrom + ":" + obj.OrgDestTo; }
                        }
                        obj.DepartureLocation = AKArray[ictr1]["DEPARTURECITY"].ToString().Trim();
                        obj.ArrivalLocation = AKArray[ictr1]["ARRIVALCITY"].ToString().Trim();
                        obj.TripType = AKArray[ictr1]["TRIPTYPE"].ToString();
                        try
                        {
                            obj.DepartureCityName = ((from ct in CityList where ct.AirportCode == obj.DepartureLocation select ct).ToList())[0].City;
                        }
                        catch (Exception ex)
                        {
                            obj.DepartureCityName = searchInputs.DepartureCity;
                        }
                        try
                        {
                            obj.ArrivalCityName = ((from ct in CityList where ct.AirportCode == obj.ArrivalLocation select ct).ToList())[0].City;
                        }
                        catch (Exception ex)
                        {
                            obj.ArrivalCityName = searchInputs.ArrivalCity;
                        }
                        obj.depdatelcc = "";
                        obj.arrdatelcc = "";
                        //obj.DepartureDate = Utility.Left(AKArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(AKArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 3, 2) + Utility.Right(AKArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 2);
                        //obj.Departure_Date = Utility.Left(obj.DepartureDate, 2) + " " + Utility.datecon(Utility.Mid(AKArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 3, 2));
                        //obj.DepartureTime = AKArray[ictr1]["DEPARTURETIME"].ToString().Trim();
                        //obj.ArrivalDate = Utility.Left(AKArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(AKArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 3, 2) + Utility.Right(AKArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 2);
                        //obj.Arrival_Date = Utility.Left(obj.ArrivalDate, 2) + " " + Utility.datecon(Utility.Mid(AKArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 3, 2));
                        //obj.ArrivalTime = AKArray[ictr1]["ARRIVALTIME"].ToString().Trim();
                        obj.DepartureDate = Utility.Left(AKArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 2) + Utility.Mid(AKArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 3, 2) + Utility.Right(AKArray[ictr1]["DEPARTUREDATE"].ToString().Trim(), 2);
                        obj.Departure_Date = AKArray[ictr1]["DEP_DATE"].ToString().Trim();
                        obj.DepartureTime = AKArray[ictr1]["DEPARTURETIME"].ToString().Trim();
                        //Changed Arrival
                        obj.ArrivalDate = Utility.Left(AKArray[ictr1]["ARRIVALDATE"].ToString().Trim(), 2) + Utility.Mid(AKArray[ictr1]["ARRIVALDATE"].ToString().Trim(), 3, 2) + Utility.Right(AKArray[ictr1]["ARRIVALDATE"].ToString().Trim(), 2);
                        obj.Arrival_Date = AKArray[ictr1]["ARR_DATE"].ToString().Trim();
                        obj.ArrivalTime = AKArray[ictr1]["ARRIVALTIME"].ToString().Trim();

                        obj.MarketingCarrier = Utility.Left(AKArray[ictr1]["FLIGHT"].ToString().Trim(), 2);
                        obj.AirLineName = "AirAsia";
                        obj.FlightIdentification = AKArray[ictr1]["FLIGHT"].ToString().Trim().Replace(Utility.Left(AKArray[ictr1]["FLIGHT"].ToString(), 2), "");
                        obj.RBD = "";
                        obj.AdtRbd = "";
                        obj.ChdRbd = "";
                        obj.InfRbd = "";
                        //IXArray[ictr1]["RBD").ToString
                        obj.AvailableSeats = "0";
                        obj.ValiDatingCarrier = "AK";
                        obj.EQ = "";
                        obj.Stops = AKArray[ictr1]["STOPS"].ToString() + "-Stop";
                        obj.fareBasis = "";
                        obj.FBPaxType = "";
                        obj.TotPax = searchInputs.Adult + searchInputs.Child;
                        obj.Adult = searchInputs.Adult;
                        obj.Child = searchInputs.Child;
                        obj.Infant = searchInputs.Infant;

                        obj.TotDur = "";
                        obj.Trip = searchInputs.Trip.ToString();
                        //obj.TripCnt = tCnt;
                        obj.sno = "AIRASIA/";
                        #endregion

                        #region Fare Details
                        #region ADT Fare
                        obj.AdtFareType = "Non Refundable";
                        obj.AdtBfare = float.Parse(AKFareADTHT["BF"].ToString());
                        obj.AdtFSur = float.Parse(AKFareADTHT["YQ"].ToString());
                        obj.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(AKFareADTHT["TF"].ToString()), searchInputs.Trip.ToString());
                        obj.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(AKFareADTHT["TF"].ToString()), searchInputs.Trip.ToString());
                        obj.AdtOT = float.Parse(AKFareADTHT["OT"].ToString()) + srvCharge + obj.ADTAdminMrk;
                        obj.AdtTax = obj.AdtFSur + obj.AdtOT;
                        obj.AdtFare = float.Parse(AKFareADTHT["TF"].ToString()) + srvCharge + obj.ADTAdminMrk;
                        if (searchInputs.IsCorp == true)
                        {
                            DataTable MGDT = new DataTable();
                            MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.AdtBfare.ToString()), decimal.Parse(obj.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.AdtFare.ToString()));
                            obj.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                            obj.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                        }
                        #endregion

                        #region CHD Fare
                        if (searchInputs.Child > 0)
                        {
                            obj.ChdBFare = float.Parse(AKFareCHDHT["BF"].ToString());
                            obj.ChdFSur = float.Parse(AKFareCHDHT["YQ"].ToString());
                            obj.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(AKFareCHDHT["TF"].ToString()), searchInputs.Trip.ToString());
                            obj.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], obj.ValiDatingCarrier, Convert.ToDouble(AKFareCHDHT["TF"].ToString()), searchInputs.Trip.ToString());

                            obj.ChdOT = float.Parse(AKFareCHDHT["OT"].ToString()) + srvCharge + obj.CHDAdminMrk;
                            obj.ChdTax = obj.ChdFSur + obj.ChdOT;
                            obj.ChdFare = float.Parse(AKFareCHDHT["TF"].ToString()) + srvCharge + obj.CHDAdminMrk;

                            if (searchInputs.IsCorp == true)
                            {
                                DataTable MGDT = new DataTable();
                                MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.ChdBFare.ToString()), decimal.Parse(obj.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.ChdFare.ToString()));
                                obj.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                obj.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                            }
                        }
                        #endregion

                        #region INF Fare
                        if (searchInputs.Infant > 0)
                        {
                            obj.InfBfare = float.Parse(AKFareINFHT["BF"].ToString());
                            obj.InfFSur = float.Parse(AKFareINFHT["YQ"].ToString());
                            obj.InfOT = float.Parse(AKFareINFHT["OT"].ToString());
                            obj.InfTax = obj.InfFSur + obj.InfOT;
                            obj.InfFare = float.Parse(AKFareINFHT["TF"].ToString());

                            if (searchInputs.IsCorp == true)
                            {
                                DataTable MGDT = new DataTable();
                                MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, obj.ValiDatingCarrier, decimal.Parse(obj.InfBfare.ToString()), decimal.Parse(obj.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(obj.InfFare.ToString()));
                                obj.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                obj.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                            }
                        }
                        #endregion
                        obj.STax = (obj.AdtSrvTax * obj.Adult) + (obj.ChdSrvTax * obj.Child) + (obj.InfSrvTax * obj.Infant);
                        obj.TotMrkUp = (obj.ADTAdminMrk * obj.Adult) + (obj.CHDAdminMrk * obj.Child) + (obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child);
                        obj.TotMgtFee = (obj.AdtMgtFee * obj.Adult) + (obj.ChdMgtFee * obj.Child) + (obj.InfMgtFee * obj.Infant);
                        obj.TotBfare = (obj.AdtBfare * obj.Adult) + (obj.ChdBFare * obj.Child) + (obj.InfBfare * obj.Infant);
                        obj.TotalFuelSur = (obj.AdtFSur * obj.Adult) + (obj.ChdFSur * obj.Child) + (obj.InfFSur * obj.Infant);
                        obj.TotalTax = (obj.AdtTax * obj.Adult) + (obj.ChdTax * obj.Child) + (obj.InfTax * obj.Infant);
                        obj.TotalFare = (obj.AdtFare * obj.Adult) + (obj.ChdFare * obj.Child) + (obj.InfFare * obj.Infant);
                        obj.OriginalTF = obj.TotalFare;
                        //obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + obj.TotMrkUp + obj.TotMgtFee;
                        obj.TotalFare = obj.TotalFare + obj.STax + obj.TFee + (obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child) + obj.TotMgtFee;
                        obj.NetFare = obj.TotalFare - ((obj.ADTAgentMrk * obj.Adult) + (obj.CHDAgentMrk * obj.Child));
                        obj.TotDis = 0;
                        obj.OriginalTT = srvCharge; //totTax + totFS;
                        obj.ADTAdminMrk = 0;
                        obj.CHDAdminMrk = 0;
                        #endregion

                        obj.LineNumber = Convert.ToInt32(AKArray[ictr1]["LineItemNumber"]);
                        obj.Leg = Convert.ToInt32(AKArray[ictr1]["Leg"].ToString());
                        obj.Flight = AKArray[ictr1]["Flight1"].ToString();
                        obj.Searchvalue = AKArray[ictr1]["FARETYPE"].ToString();
                        obj.Provider = "LCC";
                        obj.Track_id = "";
                        obj.FType = FType;
                        obj.IsCorp = searchInputs.IsCorp;
                        RetList.Add(obj);
                    }
                }




            }
            catch (Exception ex)
            {
            }
            return RetList;

        }

        #endregion

        private string convertTimeFormat(string time)
        {
            string mm_str = null;
            if (Utility.Right(time, 2) == "PM")
            {
                switch (Utility.Left(time, 2))
                {
                    case "01":
                        mm_str = time.Replace(Utility.Left(time, 3), "13:");
                        break;
                    case "02":
                        mm_str = time.Replace(Utility.Left(time, 3), "14:");
                        break;
                    case "03":
                        mm_str = time.Replace(Utility.Left(time, 3), "15:");
                        break;
                    case "04":
                        mm_str = time.Replace(Utility.Left(time, 3), "16:");
                        break;
                    case "05":
                        mm_str = time.Replace(Utility.Left(time, 3), "17:");
                        break;
                    case "06":
                        mm_str = time.Replace(Utility.Left(time, 3), "18:");
                        break;
                    case "07":
                        mm_str = time.Replace(Utility.Left(time, 3), "19:");
                        break;
                    case "08":
                        mm_str = time.Replace(Utility.Left(time, 3), "20:");
                        break;
                    case "09":
                        mm_str = time.Replace(Utility.Left(time, 3), "21:");
                        break;
                    case "10":
                        mm_str = time.Replace(Utility.Left(time, 3), "22:");
                        break;
                    case "11":
                        mm_str = time.Replace(Utility.Left(time, 3), "23:");
                        break;
                    case "12":
                        mm_str = time.Replace(Utility.Left(time, 3), "12:");
                        break;
                    default:
                        mm_str = time;
                        break;
                }
            }
            else
            {
                mm_str = time;
            }
            if (mm_str.Length == 4)
                mm_str = "0" + mm_str;
            if (mm_str.Length >= 5)
                mm_str = Utility.Left(mm_str, 5);
            return mm_str;
        }

        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray;
            double mrkamt = 0;
            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");
                if (!(airMrkArray != null && airMrkArray.Length > 0))
                {
                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

                }

                if (airMrkArray.Length > 0)
                {
                    if (Trip == "I")
                    {
                        if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                        {
                            mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                        }
                        else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                        {
                            mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                        }
                    }
                    else
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkUp"].ToString());
                    }
                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return float.Parse(mrkamt.ToString());
        }

        //private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        //{
        //    decimal STaxP = 0;
        //    decimal TFeeP = 0;
        //    int IATAComm = 0;
        //    decimal originalDis = 0;
        //    Hashtable STHT = new Hashtable();
        //    try
        //    {
        //        STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
        //        TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
        //        IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
        //        STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
        //        originalDis = decimal.Parse(Dis.ToString()) - decimal.Parse(STHT["TFee"].ToString());
        //        STHT.Add("STax", Math.Round(((originalDis * STaxP) / 100), 0));
        //        STHT.Add("Tds", Math.Round(((originalDis * decimal.Parse(TDS)) / 100), 0));
        //        STHT.Add("IATAComm", IATAComm);
        //    }
        //    catch
        //    {
        //        STHT.Add("STax", 0);
        //        STHT.Add("TFee", 0);
        //        STHT.Add("Tds", 0);
        //        STHT.Add("IATAComm", 0);
        //    }
        //    return STHT;
        //}
        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;
            //int IATAComm = 0;
            decimal originalDis = 0;
            Hashtable STHT = new Hashtable();
            try
            {
                STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                originalDis = decimal.Parse(Dis.ToString()) - decimal.Parse(STHT["TFee"].ToString());
                STHT.Add("STax", Math.Round(((originalDis * STaxP) / 100), 0));
                STHT.Add("Tds", Math.Round((((originalDis - decimal.Parse(STHT["STax"].ToString())) * decimal.Parse(TDS)) / 100), 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }

        public DataTable GetResult(string src, string dest, string deptdate, string returndate, string adult, string child, string infant, string infantbase, string infanttax, string flight, string status, string trip)
        {
            string strReq = "";
            string strRes = "";

            strReq = strReq + "<?xml version='1.0' encoding='utf-8'?>";
            strReq = strReq + "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>";
            strReq = strReq + "<soap:Body>";
            strReq = strReq + "<Get_FlightResult  xmlns='http://tempuri.org/'>";

            strReq = strReq + "<src>" + src + "</src>";
            strReq = strReq + "<dest>" + dest + "</dest>";
            strReq = strReq + "<deptdate>" + deptdate + "</deptdate>";
            strReq = strReq + "<returndate>" + returndate + "</returndate>";
            strReq = strReq + "<adult>" + adult + "</adult>";
            strReq = strReq + "<child>" + child + "</child>";
            strReq = strReq + "<infant>" + infant + "</infant>";
            strReq = strReq + "<infantBase>" + infantbase + "</infantBase>";
            strReq = strReq + "<infantTax>" + infanttax + "</infantTax>";
            strReq = strReq + "<flight>" + flight + "</flight>";
            strReq = strReq + "<Status>" + status + "</Status>";
            strReq = strReq + "<trip>" + trip + "</trip>";
            strReq = strReq + "</Get_FlightResult>";
            strReq = strReq + "</soap:Body>";
            strReq = strReq + "</soap:Envelope>";
            string strresult = PostXml("http://180.179.206.77/lcc/LccCouFlightService.asmx", strReq);
            DataTable Dt = new DataTable();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(strresult);
                DataSet ds = new DataSet();
                ds.ReadXml(new StringReader(xd.InnerText));
                if (ds.Tables.Count > 0)
                    Dt = ds.Tables[0];
            }
            catch (Exception ex)
            {

            }

            return Dt;
        }
        private string PostXml(string url, string xml)
        {
            byte[] bytes = UTF8Encoding.UTF8.GetBytes(xml);
            string strResult = string.Empty;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "text/xml";
                request.KeepAlive = false;
                request.Timeout = 3600 * 1000;
                request.ReadWriteTimeout = 3600 * 1000;

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        StreamReader reader = null;
                        Stream responseStream = response.GetResponseStream();
                        reader = new StreamReader(responseStream);
                        strResult = reader.ReadToEnd();
                        response.Close();
                        responseStream.Close();
                        reader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return strResult;
        }

    }
}