﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using STD.Shared;
using STD.DAL;

namespace STD.BAL
{
    public class Data
    {
        /// <summary>
        /// Service Tax,Tran.,IATAComm
        /// </summary>
        /// <param name="jrnyType">Domestic or Internation(D/I)</param>
        /// <param name="connectionString">connectionString</param>
        /// <returns>List of charges</returns>
        public static List<FltSrvChargeList> GetSrvChargeInfo(string jrnyType, string connectionString)
        {
            FltCharges objFltCharges = new FltCharges(connectionString);
            return objFltCharges.GetSrvCharges(jrnyType);
        }
        /// <summary>
        /// Credentiials 
        /// </summary>
        /// <param name="connectionString">connectionString</param>
        /// <returns>list</returns>
        public static List<CredentialList> GetProviderCrd(string connectionString)
        {
            Credentials objCrd = new Credentials(connectionString);
            return objCrd.GetServiceCredentials("");
        }
        public static List<CredentialList> GetGalBookingCrd(string connectionString, string Provider, string Trip, string VC,string crdType)
        {
            Credentials objCrd = new Credentials(connectionString);
            return objCrd.GetGALBookingCredentials(Provider, Trip, VC, crdType);
        }

        public static List<CredentialList> GetGalTicketingCrd(string connectionString, string Provider, string Trip, string VC, string crdType)
        {
            Credentials objCrd = new Credentials(connectionString);
            return objCrd.GetGalTicketingCrd(Provider, Trip, VC, crdType);
        }
        /// <summary>
        /// Get All City List Trip Wise
        /// </summary>
        /// <param name="jrnyType">D/I</param>
        /// <param name="connectionString"></param>
        /// <returns>Cityname,citycode,airport name airportcode</returns>
        public static List<FlightCityList> GetCityList(string jrnyType, string connectionString)
        {
            FlightCommonDAL objFltCommon = new FlightCommonDAL(connectionString);
            return objFltCommon.FetchCityName(jrnyType, "");
        }
        /// <summary>
        /// Get All Airline list
        /// </summary>
        /// <param name="connectionString"></param>
        /// <returns>airline name,airline code</returns>
        public static List<AirlineList> GetAirlineList(string connectionString)
        {
            FlightCommonDAL objFltCommon = new FlightCommonDAL(connectionString);
            return objFltCommon.FetchAirlineName("");
        }
        ///// <summary>
        ///// Get markup details
        ///// </summary>
        ///// <param name="connectionString"></param>
        ///// <param name="AgentId"></param>
        ///// <param name="DistrId"></param>
        ///// <param name="UserType">Admin/agent/distr</param>
        ///// <param name="jrnyType">D/I</param>
        ///// <returns>AIRLINECODE, MARKUPTYPE, MARKUPVALUE, SHOW</returns>
        //public static DataSet GetMarkup(string connectionString, string AgentId, string DistrId, string UserType, string jrnyType)
        //{
        //    FlightCommonDAL objFltCommon = new FlightCommonDAL(connectionString);
        //    return objFltCommon.GetMarkupDetails(AgentId, DistrId, UserType, jrnyType);
        //}
        /// <summary>
        /// Get markup details
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="AgentId"></param>
        /// <param name="DistrId"></param>
        /// <param name="UserType">Admin/agent/distr</param>
        /// <param name="jrnyType">D/I</param>
        /// <returns>AIRLINECODE, MARKUPTYPE, MARKUPVALUE, SHOW</returns>
        public static DataTable GetMarkup(string connectionString, string AgentId, string DistrId, string UserType, string jrnyType)
        {
            FlightCommonDAL objFltCommon = new FlightCommonDAL(connectionString);
            return objFltCommon.GetMarkupDetails_Gal(AgentId, DistrId, UserType, jrnyType);
        }

        public static DataTable Calc_ComCB(string connectionString, string GPType, string VC, decimal BaseFare, decimal YQ, int PaxCnt,string cabinclass,string Depdate,string sector
            , string returnDate, string farebasis, string orgCountry, string destCountry, string fltNum, string OPC, string MktC, string fareType, string bkgChannel)
        {
            FlightCommonDAL objFltCommon = new FlightCommonDAL(connectionString);
            return objFltCommon.Cal_Comm_cb_Gal(GPType, VC, BaseFare, YQ, PaxCnt, "", cabinclass, Depdate,sector, returnDate,  farebasis,  orgCountry,  destCountry,  fltNum, OPC, MktC, fareType, bkgChannel);
        }

        public static string Calc_TDS(string connectionString, string UserId)
        {
            FlightCommonDAL objFltCommon = new FlightCommonDAL(connectionString);
            return objFltCommon.GetTDSPercent_Gal(UserId);
        }
        /// <summary>
        /// get service list block for agent/all
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="AgentId">userid</param>
        /// <param name="jrnyType">D/I</param>
        /// <returns>not available list of service</returns>
        public static string GetBlockServices(string connectionString, string AgentId, String jrnyType)
        {
            FlightCommonDAL objFltCommon = new FlightCommonDAL(connectionString);
            return objFltCommon.GetBlockSrvcFromBD(AgentId, jrnyType);
        }
        public static FlightSearch GetFltSearchDetails_API(string connectionString, FlightSearch objSearch)
        {
            FlightCommonDAL objFltCommon = new FlightCommonDAL(connectionString);
            return objFltCommon.GetFltSearchDetailsDal_API(objSearch);
        }

        /// <summary>
        /// Form of Payment
        /// </summary>
        /// <param name="VC">Ticketing Carrier :length 2</param>
        /// <param name="Trip">D/I</param>
        /// <param name="Provider">1G/1A/1B</param>
        /// <returns>fop with details</returns>
        public static DataTable GetFOP(string connectionString, string VC, string Trip, string Provider)
        {
            FlightCommonDAL objFltCommon = new FlightCommonDAL(connectionString);
            return objFltCommon.GetFOPFromDB(VC, Trip, Provider);
        }
    }
}
