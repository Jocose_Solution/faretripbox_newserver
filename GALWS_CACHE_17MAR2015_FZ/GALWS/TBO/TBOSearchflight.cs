﻿using GALWS;
using Newtonsoft.Json;
using STD.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace STD.BAL.TBO
{
    class TBOSearchflight
    {

        String ClientID;
        string UserName;
        string Pass;
        string IP;
        public TBOSearchflight(string clientId, string Username, string Pwd, string ip)
        {

            ClientID = clientId;
            UserName = Username;
            Pass = Pwd;
            IP = ip;




        }

        public List<FlightSearchResults> GetTBOAvailibility(FlightSearch objFlt, bool isSplFare, string fareType, bool isLCC, List<FltSrvChargeList> SrvchargeList, DataSet MarkupDs, string constr, float srvCharge)
        {
            TBOAuthProcess objTbProcess = new TBOAuthProcess();

            string strTokenID = objTbProcess.GetTBOToken(ServiceUrl.loginUrl, ClientID, UserName, Pass, IP);

            searchinput objSearch = new searchinput();
            objSearch.EndUserIp = IP;//"125.22.194.74";
            objSearch.TokenId = strTokenID;
            objSearch.AdultCount = objFlt.Adult.ToString();
            objSearch.ChildCount = objFlt.Child.ToString();
            objSearch.InfantCount = objFlt.Infant.ToString();
            objSearch.DirectFlight = "false";
            objSearch.OneStopFlight = "false";
            objSearch.PreferredAirlines = null;// objFlt.HidTxtAirLine;//null;

            List<SegmentsInput> objLSeg = new List<SegmentsInput>();
            SegmentsInput objseg = new SegmentsInput();

            if (fareType == "INB")
            {
                objseg.Origin = objFlt.HidTxtDepCity.Split(',')[0];
                objseg.Destination = objFlt.HidTxtArrCity.Split(',')[0];
                objseg.PreferredDepartureTime = Utility.Right(objFlt.DepDate, 4) + "-" + Utility.Mid(objFlt.DepDate, 3, 2) + "-" + Utility.Left(objFlt.DepDate, 2) + "T00: 00: 00";//objFlt.DepDate;
            }
            else
            {

                objseg.Origin = objFlt.HidTxtArrCity.Split(',')[0];
                objseg.Destination = objFlt.HidTxtDepCity.Split(',')[0];
                objseg.PreferredDepartureTime = Utility.Right(objFlt.RetDate, 4) + "-" + Utility.Mid(objFlt.RetDate, 3, 2) + "-" + Utility.Left(objFlt.RetDate, 2) + "T00: 00: 00"; //objFlt.RetDate;
            }
            objseg.FlightCabinClass = "1";

            //objseg.PreferredArrivalTime = objFlt
            objLSeg.Add(objseg);

            if (isSplFare)
            {
                SegmentsInput objsegret = new SegmentsInput();

                objsegret.Origin = objFlt.HidTxtArrCity.Split(',')[0];
                objsegret.Destination = objFlt.HidTxtDepCity.Split(',')[0];

                string cabin = "1";

                if (objFlt.Cabin.ToUpper().Trim() == "Y")
                {
                    cabin = "2";
                }
                else if (objFlt.Cabin.ToUpper().Trim() == "W")
                {
                    cabin = "3";
                }
                else if (objFlt.Cabin.ToUpper().Trim() == "C")
                {
                    cabin = "4";
                }
                else if (objFlt.Cabin.ToUpper().Trim() == "F")
                {
                    cabin = "6";
                }


                objsegret.FlightCabinClass = cabin;
                objsegret.PreferredDepartureTime = Utility.Right(objFlt.RetDate, 4) + "-" + Utility.Mid(objFlt.RetDate, 3, 2) + "-" + Utility.Left(objFlt.RetDate, 2) + "T00: 00: 00"; //objFlt.RetDate;
                objLSeg.Add(objsegret);
                objSearch.JourneyType = "5";
            }
            else
            {
                objSearch.JourneyType = "1";
            }



            // objsegret.PreferredArrivalTime = "2016-08-05T00: 00: 00";



            objSearch.Segments = objLSeg;

            List<string> strSource = new List<string>();

            //strSource.Add(objFlt.HidTxtAirLine);
            if (isLCC)
            {
                if (objFlt.HidTxtAirLine=="")
                {
                    strSource.Add("2T");
                    strSource.Add("LB");
                    strSource.Add("VA");
                    //strSource.Add("OD");
                    strSource.Add("KB");
                    strSource.Add("OP");
                    strSource.Add("G9");
                    strSource.Add("FZ");
                    strSource.Add("AK");
                    strSource.Add("IX");
                    strSource.Add("I5");
                   
                
                }
                else{
                strSource.Add(objFlt.HidTxtAirLine);
                }
               
            }
            else
            {
                string[] ail = { objFlt.HidTxtAirLine };
                objSearch.PreferredAirlines = ail;
                strSource.Add("GDS");
            }


            objSearch.Sources = strSource;


            string strJsonData = JsonConvert.SerializeObject(objSearch);
            TBOUtitlity.SaveFile(strJsonData, "SearchReq_" + objFlt.HidTxtAirLine);
            SaveResponse.SAVElOGFILE(strJsonData, "REQ", "txt", "", objFlt.HidTxtAirLine, fareType);
            string strJsonResponse = "";
            FlightResult items = new FlightResult();
            //strJsonResponse = "{\"Response\":{\"ResponseStatus\":1,\"Error\":{\"ErrorCode\":0,\"ErrorMessage\":\"\"},\"TraceId\":\"93c4ca1d-7592-41b6-bd62-cd05e8ba7960\",\"Origin\":\"DEL\",\"Destination\":\"BLR\",\"Results\":[{\"ResultIndex\":\"OB1\",\"Source\":10,\"IsLCC\":true,\"IsRefundable\":true,\"AirlineRemark\":null,\"Fare\":{\"Currency\":\"INR\",\"BaseFare\":2371,\"Tax\":910.03,\"YQTax\":0,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0,\"OtherCharges\":15.00,\"ChargeBU\":[{\"key\":\"TBOMARKUP\",\"value\":0},{\"key\":\"CONVENIENCECHARGE\",\"value\":0},{\"key\":\"OTHERCHARGE\",\"value\":15.00}],\"Discount\":0,\"PublishedFare\":3296.03,\"CommissionEarned\":115.23,\"PLBEarned\":0.00,\"IncentiveEarned\":0.00,\"OfferedFare\":3180.80,\"TdsOnCommission\":5.76,\"TdsOnPLB\":0.00,\"TdsOnIncentive\":0.00,\"ServiceFee\":0,\"TotalBaggageCharges\":0,\"TotalMealCharges\":0,\"TotalSeatCharges\":0},\"FareBreakdown\":[{\"Currency\":\"INR\",\"PassengerType\":1,\"PassengerCount\":1,\"BaseFare\":2371,\"Tax\":910.03,\"YQTax\":0,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0}],\"Segments\":[{\"TripIndicator\":1,\"SegmentIndicator\":1,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"113\",\"FareClass\":\"J\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":9,\"Origin\":{\"Airport\":{\"AirportCode\":\"DEL\",\"AirportName\":\"Indira Gandhi Airport\",\"Terminal\":\"1D\",\"CityCode\":\"DEL\",\"CityName\":\"Delhi\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T05:45:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BLR\",\"AirportName\":\"Bengaluru Intl\",\"Terminal\":\"\",\"CityCode\":\"BLR\",\"CityName\":\"Bangalore\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T08:25:00\"},\"Duration\":160,\"GroundTime\":0,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T08:25:00\",\"StopPointDepartureTime\":\"2016-07-21T05:45:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"}],\"LastTicketDate\":\"2016-07-04T11:55:08\",\"TicketAdvisory\":null,\"FareRules\":[{\"Origin\":\"DEL\",\"Destination\":\"BLR\",\"Airline\":\"G8\",\"FareBasisCode\":\"JGOSALE\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"}],\"AirlineCode\":\"G8\",\"ValidatingAirline\":\"G8\",\"BaggageAllowance\":\"15kg\"},{\"ResultIndex\":\"OB3\",\"Source\":32,\"IsLCC\":true,\"IsRefundable\":true,\"AirlineRemark\":\"G8 Pvt Fare \",\"Fare\":{\"Currency\":\"INR\",\"BaseFare\":1,\"Tax\":3575,\"YQTax\":2650,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0,\"OtherCharges\":15.00,\"ChargeBU\":[{\"key\":\"TBOMARKUP\",\"value\":0},{\"key\":\"CONVENIENCECHARGE\",\"value\":0},{\"key\":\"OTHERCHARGE\",\"value\":15.00}],\"Discount\":0,\"PublishedFare\":3591,\"CommissionEarned\":0.00,\"PLBEarned\":0.00,\"IncentiveEarned\":0.00,\"OfferedFare\":3591.00,\"TdsOnCommission\":0.00,\"TdsOnPLB\":0.00,\"TdsOnIncentive\":0.00,\"ServiceFee\":0,\"TotalBaggageCharges\":0,\"TotalMealCharges\":0,\"TotalSeatCharges\":0},\"FareBreakdown\":[{\"Currency\":\"INR\",\"PassengerType\":1,\"PassengerCount\":1,\"BaseFare\":1,\"Tax\":3575,\"YQTax\":2650,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0}],\"Segments\":[{\"TripIndicator\":1,\"SegmentIndicator\":1,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"117\",\"FareClass\":\"G1\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":9,\"Origin\":{\"Airport\":{\"AirportCode\":\"DEL\",\"AirportName\":\"Indira Gandhi Airport\",\"Terminal\":\"1D\",\"CityCode\":\"DEL\",\"CityName\":\"Delhi\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T20:35:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BLR\",\"AirportName\":\"Bengaluru Intl\",\"Terminal\":\"\",\"CityCode\":\"BLR\",\"CityName\":\"Bangalore\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T23:30:00\"},\"Duration\":175,\"GroundTime\":0,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T23:30:00\",\"StopPointDepartureTime\":\"2016-07-21T20:35:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"}],\"LastTicketDate\":\"2016-07-04T11:55:08\",\"TicketAdvisory\":null,\"FareRules\":[{\"Origin\":\"DEL\",\"Destination\":\"BLR\",\"Airline\":\"G8\",\"FareBasisCode\":\"G1TRIP\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"}],\"AirlineCode\":\"G8\",\"ValidatingAirline\":\"G8\",\"BaggageAllowance\":\"15kg\"},{\"ResultIndex\":\"OB2\",\"Source\":32,\"IsLCC\":true,\"IsRefundable\":true,\"AirlineRemark\":\"G8 Pvt Fare \",\"Fare\":{\"Currency\":\"INR\",\"BaseFare\":1,\"Tax\":3575,\"YQTax\":2650,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0,\"OtherCharges\":15.00,\"ChargeBU\":[{\"key\":\"TBOMARKUP\",\"value\":0},{\"key\":\"CONVENIENCECHARGE\",\"value\":0},{\"key\":\"OTHERCHARGE\",\"value\":15.00}],\"Discount\":0,\"PublishedFare\":3591,\"CommissionEarned\":0.00,\"PLBEarned\":0.00,\"IncentiveEarned\":0.00,\"OfferedFare\":3591.00,\"TdsOnCommission\":0.00,\"TdsOnPLB\":0.00,\"TdsOnIncentive\":0.00,\"ServiceFee\":0,\"TotalBaggageCharges\":0,\"TotalMealCharges\":0,\"TotalSeatCharges\":0},\"FareBreakdown\":[{\"Currency\":\"INR\",\"PassengerType\":1,\"PassengerCount\":1,\"BaseFare\":1,\"Tax\":3575,\"YQTax\":2650,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0}],\"Segments\":[{\"TripIndicator\":1,\"SegmentIndicator\":1,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"113\",\"FareClass\":\"G1\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":9,\"Origin\":{\"Airport\":{\"AirportCode\":\"DEL\",\"AirportName\":\"Indira Gandhi Airport\",\"Terminal\":\"1D\",\"CityCode\":\"DEL\",\"CityName\":\"Delhi\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T05:45:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BLR\",\"AirportName\":\"Bengaluru Intl\",\"Terminal\":\"\",\"CityCode\":\"BLR\",\"CityName\":\"Bangalore\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T08:25:00\"},\"Duration\":160,\"GroundTime\":0,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T08:25:00\",\"StopPointDepartureTime\":\"2016-07-21T05:45:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"}],\"LastTicketDate\":\"2016-07-04T11:55:08\",\"TicketAdvisory\":null,\"FareRules\":[{\"Origin\":\"DEL\",\"Destination\":\"BLR\",\"Airline\":\"G8\",\"FareBasisCode\":\"G1TRIP\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"}],\"AirlineCode\":\"G8\",\"ValidatingAirline\":\"G8\",\"BaggageAllowance\":\"15kg\"},{\"ResultIndex\":\"OB5\",\"Source\":10,\"IsLCC\":true,\"IsRefundable\":true,\"AirlineRemark\":null,\"Fare\":{\"Currency\":\"INR\",\"BaseFare\":80,\"Tax\":3672.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0,\"OtherCharges\":15.00,\"ChargeBU\":[{\"key\":\"TBOMARKUP\",\"value\":0},{\"key\":\"CONVENIENCECHARGE\",\"value\":0},{\"key\":\"OTHERCHARGE\",\"value\":15.00}],\"Discount\":0,\"PublishedFare\":3767.03,\"CommissionEarned\":136.95,\"PLBEarned\":0.00,\"IncentiveEarned\":0.00,\"OfferedFare\":3630.08,\"TdsOnCommission\":6.85,\"TdsOnPLB\":0.00,\"TdsOnIncentive\":0.00,\"ServiceFee\":0,\"TotalBaggageCharges\":0,\"TotalMealCharges\":0,\"TotalSeatCharges\":0},\"FareBreakdown\":[{\"Currency\":\"INR\",\"PassengerType\":1,\"PassengerCount\":1,\"BaseFare\":80,\"Tax\":3672.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0}],\"Segments\":[{\"TripIndicator\":1,\"SegmentIndicator\":1,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"117\",\"FareClass\":\"M\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":9,\"Origin\":{\"Airport\":{\"AirportCode\":\"DEL\",\"AirportName\":\"Indira Gandhi Airport\",\"Terminal\":\"1D\",\"CityCode\":\"DEL\",\"CityName\":\"Delhi\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T20:35:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BLR\",\"AirportName\":\"Bengaluru Intl\",\"Terminal\":\"\",\"CityCode\":\"BLR\",\"CityName\":\"Bangalore\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T23:30:00\"},\"Duration\":175,\"GroundTime\":0,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T23:30:00\",\"StopPointDepartureTime\":\"2016-07-21T20:35:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"}],\"LastTicketDate\":\"2016-07-04T11:55:08\",\"TicketAdvisory\":null,\"FareRules\":[{\"Origin\":\"DEL\",\"Destination\":\"BLR\",\"Airline\":\"G8\",\"FareBasisCode\":\"MGOSAVE\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"}],\"AirlineCode\":\"G8\",\"ValidatingAirline\":\"G8\",\"BaggageAllowance\":\"15kg\"},{\"ResultIndex\":\"OB4\",\"Source\":26,\"IsLCC\":true,\"IsRefundable\":true,\"AirlineRemark\":\"Special Non Commissionable Fare \",\"Fare\":{\"Currency\":\"INR\",\"BaseFare\":2,\"Tax\":3668.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0,\"OtherCharges\":0,\"ChargeBU\":[{\"key\":\"TBOMARKUP\",\"value\":0},{\"key\":\"CONVENIENCECHARGE\",\"value\":0},{\"key\":\"OTHERCHARGE\",\"value\":0}],\"Discount\":0,\"PublishedFare\":3670.03,\"CommissionEarned\":0.00,\"PLBEarned\":0.00,\"IncentiveEarned\":0.00,\"OfferedFare\":3670.03,\"TdsOnCommission\":0.00,\"TdsOnPLB\":0.00,\"TdsOnIncentive\":0.00,\"ServiceFee\":0,\"TotalBaggageCharges\":0,\"TotalMealCharges\":0,\"TotalSeatCharges\":0},\"FareBreakdown\":[{\"Currency\":\"INR\",\"PassengerType\":1,\"PassengerCount\":1,\"BaseFare\":2,\"Tax\":3668.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0}],\"Segments\":[{\"TripIndicator\":1,\"SegmentIndicator\":1,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"117\",\"FareClass\":\"M\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":9,\"Origin\":{\"Airport\":{\"AirportCode\":\"DEL\",\"AirportName\":\"Indira Gandhi Airport\",\"Terminal\":\"1D\",\"CityCode\":\"DEL\",\"CityName\":\"Delhi\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T20:35:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BLR\",\"AirportName\":\"Bengaluru Intl\",\"Terminal\":\"\",\"CityCode\":\"BLR\",\"CityName\":\"Bangalore\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T23:30:00\"},\"Duration\":175,\"GroundTime\":0,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T23:30:00\",\"StopPointDepartureTime\":\"2016-07-21T20:35:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"}],\"LastTicketDate\":\"2016-07-04T11:55:08\",\"TicketAdvisory\":null,\"FareRules\":[{\"Origin\":\"DEL\",\"Destination\":\"BLR\",\"Airline\":\"G8\",\"FareBasisCode\":\"MGOSPLIP\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"}],\"AirlineCode\":\"G8\",\"ValidatingAirline\":\"G8\",\"BaggageAllowance\":\"15kg\"},{\"ResultIndex\":\"OB7\",\"Source\":10,\"IsLCC\":true,\"IsRefundable\":true,\"AirlineRemark\":null,\"Fare\":{\"Currency\":\"INR\",\"BaseFare\":1125,\"Tax\":3735.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0,\"OtherCharges\":15.00,\"ChargeBU\":[{\"key\":\"TBOMARKUP\",\"value\":0},{\"key\":\"CONVENIENCECHARGE\",\"value\":0},{\"key\":\"OTHERCHARGE\",\"value\":15.00}],\"Discount\":0,\"PublishedFare\":4875.03,\"CommissionEarned\":187.74,\"PLBEarned\":0.00,\"IncentiveEarned\":0.00,\"OfferedFare\":4687.29,\"TdsOnCommission\":9.39,\"TdsOnPLB\":0.00,\"TdsOnIncentive\":0.00,\"ServiceFee\":0,\"TotalBaggageCharges\":0,\"TotalMealCharges\":0,\"TotalSeatCharges\":0},\"FareBreakdown\":[{\"Currency\":\"INR\",\"PassengerType\":1,\"PassengerCount\":1,\"BaseFare\":1125,\"Tax\":3735.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0}],\"Segments\":[{\"TripIndicator\":1,\"SegmentIndicator\":1,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"336\",\"FareClass\":\"K\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":6,\"Origin\":{\"Airport\":{\"AirportCode\":\"DEL\",\"AirportName\":\"Indira Gandhi Airport\",\"Terminal\":\"1D\",\"CityCode\":\"DEL\",\"CityName\":\"Delhi\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T14:20:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BOM\",\"AirportName\":\"Mumbai\",\"Terminal\":\"1A\",\"CityCode\":\"BOM\",\"CityName\":\"Mumbai\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T16:15:00\"},\"Duration\":115,\"GroundTime\":0,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T16:15:00\",\"StopPointDepartureTime\":\"2016-07-21T14:20:00\",\"Craft\":\"32N\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"},{\"TripIndicator\":1,\"SegmentIndicator\":2,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"325\",\"FareClass\":\"K\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":6,\"Origin\":{\"Airport\":{\"AirportCode\":\"BOM\",\"AirportName\":\"Mumbai\",\"Terminal\":\"1B\",\"CityCode\":\"BOM\",\"CityName\":\"Mumbai\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T21:45:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BLR\",\"AirportName\":\"Bengaluru Intl\",\"Terminal\":\"\",\"CityCode\":\"BLR\",\"CityName\":\"Bangalore\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T23:15:00\"},\"AccumulatedDuration\":535,\"Duration\":90,\"GroundTime\":330,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T23:15:00\",\"StopPointDepartureTime\":\"2016-07-21T21:45:00\",\"Craft\":\"32N\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"}],\"LastTicketDate\":\"2016-07-04T11:55:08\",\"TicketAdvisory\":null,\"FareRules\":[{\"Origin\":\"DEL\",\"Destination\":\"BOM\",\"Airline\":\"G8\",\"FareBasisCode\":\"KGOSAVE\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"},{\"Origin\":\"BOM\",\"Destination\":\"BLR\",\"Airline\":\"G8\",\"FareBasisCode\":\"KGOSAVE\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"}],\"AirlineCode\":\"G8\",\"ValidatingAirline\":\"G8\",\"BaggageAllowance\":\"15kg\"},{\"ResultIndex\":\"OB6\",\"Source\":10,\"IsLCC\":true,\"IsRefundable\":true,\"AirlineRemark\":null,\"Fare\":{\"Currency\":\"INR\",\"BaseFare\":1125,\"Tax\":3735.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0,\"OtherCharges\":15.00,\"ChargeBU\":[{\"key\":\"TBOMARKUP\",\"value\":0},{\"key\":\"CONVENIENCECHARGE\",\"value\":0},{\"key\":\"OTHERCHARGE\",\"value\":15.00}],\"Discount\":0,\"PublishedFare\":4875.03,\"CommissionEarned\":187.74,\"PLBEarned\":0.00,\"IncentiveEarned\":0.00,\"OfferedFare\":4687.29,\"TdsOnCommission\":9.39,\"TdsOnPLB\":0.00,\"TdsOnIncentive\":0.00,\"ServiceFee\":0,\"TotalBaggageCharges\":0,\"TotalMealCharges\":0,\"TotalSeatCharges\":0},\"FareBreakdown\":[{\"Currency\":\"INR\",\"PassengerType\":1,\"PassengerCount\":1,\"BaseFare\":1125,\"Tax\":3735.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0}],\"Segments\":[{\"TripIndicator\":1,\"SegmentIndicator\":1,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"336\",\"FareClass\":\"K\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":6,\"Origin\":{\"Airport\":{\"AirportCode\":\"DEL\",\"AirportName\":\"Indira Gandhi Airport\",\"Terminal\":\"1D\",\"CityCode\":\"DEL\",\"CityName\":\"Delhi\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T14:20:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BOM\",\"AirportName\":\"Mumbai\",\"Terminal\":\"1A\",\"CityCode\":\"BOM\",\"CityName\":\"Mumbai\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T16:15:00\"},\"Duration\":115,\"GroundTime\":0,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T16:15:00\",\"StopPointDepartureTime\":\"2016-07-21T14:20:00\",\"Craft\":\"32N\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"},{\"TripIndicator\":1,\"SegmentIndicator\":2,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"396\",\"FareClass\":\"K\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":6,\"Origin\":{\"Airport\":{\"AirportCode\":\"BOM\",\"AirportName\":\"Mumbai\",\"Terminal\":\"1B\",\"CityCode\":\"BOM\",\"CityName\":\"Mumbai\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T20:55:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BLR\",\"AirportName\":\"Bengaluru Intl\",\"Terminal\":\"\",\"CityCode\":\"BLR\",\"CityName\":\"Bangalore\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T22:20:00\"},\"AccumulatedDuration\":480,\"Duration\":85,\"GroundTime\":280,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T22:20:00\",\"StopPointDepartureTime\":\"2016-07-21T20:55:00\",\"Craft\":\"32N\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"}],\"LastTicketDate\":\"2016-07-04T11:55:08\",\"TicketAdvisory\":null,\"FareRules\":[{\"Origin\":\"DEL\",\"Destination\":\"BOM\",\"Airline\":\"G8\",\"FareBasisCode\":\"KGOSAVE\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"},{\"Origin\":\"BOM\",\"Destination\":\"BLR\",\"Airline\":\"G8\",\"FareBasisCode\":\"KGOSAVE\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"}],\"AirlineCode\":\"G8\",\"ValidatingAirline\":\"G8\",\"BaggageAllowance\":\"15kg\"},{\"ResultIndex\":\"OB9\",\"Source\":26,\"IsLCC\":true,\"IsRefundable\":true,\"AirlineRemark\":\"Special Non Commissionable Fare \",\"Fare\":{\"Currency\":\"INR\",\"BaseFare\":1183,\"Tax\":3738.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0,\"OtherCharges\":0,\"ChargeBU\":[{\"key\":\"TBOMARKUP\",\"value\":0},{\"key\":\"CONVENIENCECHARGE\",\"value\":0},{\"key\":\"OTHERCHARGE\",\"value\":0}],\"Discount\":0,\"PublishedFare\":4921.03,\"CommissionEarned\":0.00,\"PLBEarned\":0.00,\"IncentiveEarned\":0.00,\"OfferedFare\":4921.03,\"TdsOnCommission\":0.00,\"TdsOnPLB\":0.00,\"TdsOnIncentive\":0.00,\"ServiceFee\":0,\"TotalBaggageCharges\":0,\"TotalMealCharges\":0,\"TotalSeatCharges\":0},\"FareBreakdown\":[{\"Currency\":\"INR\",\"PassengerType\":1,\"PassengerCount\":1,\"BaseFare\":1183,\"Tax\":3738.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0}],\"Segments\":[{\"TripIndicator\":1,\"SegmentIndicator\":1,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"334\",\"FareClass\":\"Y\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":9,\"Origin\":{\"Airport\":{\"AirportCode\":\"DEL\",\"AirportName\":\"Indira Gandhi Airport\",\"Terminal\":\"1D\",\"CityCode\":\"DEL\",\"CityName\":\"Delhi\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T08:05:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BOM\",\"AirportName\":\"Mumbai\",\"Terminal\":\"1A\",\"CityCode\":\"BOM\",\"CityName\":\"Mumbai\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T10:15:00\"},\"Duration\":130,\"GroundTime\":0,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T10:15:00\",\"StopPointDepartureTime\":\"2016-07-21T08:05:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"},{\"TripIndicator\":1,\"SegmentIndicator\":2,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"323\",\"FareClass\":\"Y\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":9,\"Origin\":{\"Airport\":{\"AirportCode\":\"BOM\",\"AirportName\":\"Mumbai\",\"Terminal\":\"1B\",\"CityCode\":\"BOM\",\"CityName\":\"Mumbai\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T14:30:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BLR\",\"AirportName\":\"Bengaluru Intl\",\"Terminal\":\"\",\"CityCode\":\"BLR\",\"CityName\":\"Bangalore\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T16:10:00\"},\"AccumulatedDuration\":485,\"Duration\":100,\"GroundTime\":255,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T16:10:00\",\"StopPointDepartureTime\":\"2016-07-21T14:30:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"}],\"LastTicketDate\":\"2016-07-04T11:55:08\",\"TicketAdvisory\":null,\"FareRules\":[{\"Origin\":\"DEL\",\"Destination\":\"BOM\",\"Airline\":\"G8\",\"FareBasisCode\":\"YGOSPLIP\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"},{\"Origin\":\"BOM\",\"Destination\":\"BLR\",\"Airline\":\"G8\",\"FareBasisCode\":\"YGOSPLIP\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"}],\"AirlineCode\":\"G8\",\"ValidatingAirline\":\"G8\",\"BaggageAllowance\":\"15kg\"},{\"ResultIndex\":\"OB8\",\"Source\":26,\"IsLCC\":true,\"IsRefundable\":true,\"AirlineRemark\":\"Special Non Commissionable Fare \",\"Fare\":{\"Currency\":\"INR\",\"BaseFare\":1183,\"Tax\":3738.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0,\"OtherCharges\":0,\"ChargeBU\":[{\"key\":\"TBOMARKUP\",\"value\":0},{\"key\":\"CONVENIENCECHARGE\",\"value\":0},{\"key\":\"OTHERCHARGE\",\"value\":0}],\"Discount\":0,\"PublishedFare\":4921.03,\"CommissionEarned\":0.00,\"PLBEarned\":0.00,\"IncentiveEarned\":0.00,\"OfferedFare\":4921.03,\"TdsOnCommission\":0.00,\"TdsOnPLB\":0.00,\"TdsOnIncentive\":0.00,\"ServiceFee\":0,\"TotalBaggageCharges\":0,\"TotalMealCharges\":0,\"TotalSeatCharges\":0},\"FareBreakdown\":[{\"Currency\":\"INR\",\"PassengerType\":1,\"PassengerCount\":1,\"BaseFare\":1183,\"Tax\":3738.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0}],\"Segments\":[{\"TripIndicator\":1,\"SegmentIndicator\":1,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"330\",\"FareClass\":\"Y\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":9,\"Origin\":{\"Airport\":{\"AirportCode\":\"DEL\",\"AirportName\":\"Indira Gandhi Airport\",\"Terminal\":\"1D\",\"CityCode\":\"DEL\",\"CityName\":\"Delhi\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T07:05:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BOM\",\"AirportName\":\"Mumbai\",\"Terminal\":\"1A\",\"CityCode\":\"BOM\",\"CityName\":\"Mumbai\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T09:10:00\"},\"Duration\":125,\"GroundTime\":0,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T09:10:00\",\"StopPointDepartureTime\":\"2016-07-21T07:05:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"},{\"TripIndicator\":1,\"SegmentIndicator\":2,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"323\",\"FareClass\":\"Y\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":9,\"Origin\":{\"Airport\":{\"AirportCode\":\"BOM\",\"AirportName\":\"Mumbai\",\"Terminal\":\"1B\",\"CityCode\":\"BOM\",\"CityName\":\"Mumbai\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T14:30:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BLR\",\"AirportName\":\"Bengaluru Intl\",\"Terminal\":\"\",\"CityCode\":\"BLR\",\"CityName\":\"Bangalore\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T16:10:00\"},\"AccumulatedDuration\":545,\"Duration\":100,\"GroundTime\":320,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T16:10:00\",\"StopPointDepartureTime\":\"2016-07-21T14:30:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"}],\"LastTicketDate\":\"2016-07-04T11:55:08\",\"TicketAdvisory\":null,\"FareRules\":[{\"Origin\":\"DEL\",\"Destination\":\"BOM\",\"Airline\":\"G8\",\"FareBasisCode\":\"YGOSPLIP\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"},{\"Origin\":\"BOM\",\"Destination\":\"BLR\",\"Airline\":\"G8\",\"FareBasisCode\":\"YGOSPLIP\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"}],\"AirlineCode\":\"G8\",\"ValidatingAirline\":\"G8\",\"BaggageAllowance\":\"15kg\"},{\"ResultIndex\":\"OB11\",\"Source\":10,\"IsLCC\":true,\"IsRefundable\":true,\"AirlineRemark\":null,\"Fare\":{\"Currency\":\"INR\",\"BaseFare\":1534,\"Tax\":3760.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0,\"OtherCharges\":15.00,\"ChargeBU\":[{\"key\":\"TBOMARKUP\",\"value\":0},{\"key\":\"CONVENIENCECHARGE\",\"value\":0},{\"key\":\"OTHERCHARGE\",\"value\":15.00}],\"Discount\":0,\"PublishedFare\":5309.03,\"CommissionEarned\":207.62,\"PLBEarned\":0.00,\"IncentiveEarned\":0.00,\"OfferedFare\":5101.41,\"TdsOnCommission\":10.38,\"TdsOnPLB\":0.00,\"TdsOnIncentive\":0.00,\"ServiceFee\":0,\"TotalBaggageCharges\":0,\"TotalMealCharges\":0,\"TotalSeatCharges\":0},\"FareBreakdown\":[{\"Currency\":\"INR\",\"PassengerType\":1,\"PassengerCount\":1,\"BaseFare\":1534,\"Tax\":3760.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0}],\"Segments\":[{\"TripIndicator\":1,\"SegmentIndicator\":1,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"334\",\"FareClass\":\"Y\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":9,\"Origin\":{\"Airport\":{\"AirportCode\":\"DEL\",\"AirportName\":\"Indira Gandhi Airport\",\"Terminal\":\"1D\",\"CityCode\":\"DEL\",\"CityName\":\"Delhi\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T08:05:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BOM\",\"AirportName\":\"Mumbai\",\"Terminal\":\"1A\",\"CityCode\":\"BOM\",\"CityName\":\"Mumbai\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T10:15:00\"},\"Duration\":130,\"GroundTime\":0,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T10:15:00\",\"StopPointDepartureTime\":\"2016-07-21T08:05:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"},{\"TripIndicator\":1,\"SegmentIndicator\":2,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"323\",\"FareClass\":\"Y\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":9,\"Origin\":{\"Airport\":{\"AirportCode\":\"BOM\",\"AirportName\":\"Mumbai\",\"Terminal\":\"1B\",\"CityCode\":\"BOM\",\"CityName\":\"Mumbai\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T14:30:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BLR\",\"AirportName\":\"Bengaluru Intl\",\"Terminal\":\"\",\"CityCode\":\"BLR\",\"CityName\":\"Bangalore\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T16:10:00\"},\"AccumulatedDuration\":485,\"Duration\":100,\"GroundTime\":255,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T16:10:00\",\"StopPointDepartureTime\":\"2016-07-21T14:30:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"}],\"LastTicketDate\":\"2016-07-04T11:55:08\",\"TicketAdvisory\":null,\"FareRules\":[{\"Origin\":\"DEL\",\"Destination\":\"BOM\",\"Airline\":\"G8\",\"FareBasisCode\":\"YGOSAVE\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"},{\"Origin\":\"BOM\",\"Destination\":\"BLR\",\"Airline\":\"G8\",\"FareBasisCode\":\"YGOSAVE\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"}],\"AirlineCode\":\"G8\",\"ValidatingAirline\":\"G8\",\"BaggageAllowance\":\"15kg\"},{\"ResultIndex\":\"OB10\",\"Source\":10,\"IsLCC\":true,\"IsRefundable\":true,\"AirlineRemark\":null,\"Fare\":{\"Currency\":\"INR\",\"BaseFare\":1534,\"Tax\":3760.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0,\"OtherCharges\":15.00,\"ChargeBU\":[{\"key\":\"TBOMARKUP\",\"value\":0},{\"key\":\"CONVENIENCECHARGE\",\"value\":0},{\"key\":\"OTHERCHARGE\",\"value\":15.00}],\"Discount\":0,\"PublishedFare\":5309.03,\"CommissionEarned\":207.62,\"PLBEarned\":0.00,\"IncentiveEarned\":0.00,\"OfferedFare\":5101.41,\"TdsOnCommission\":10.38,\"TdsOnPLB\":0.00,\"TdsOnIncentive\":0.00,\"ServiceFee\":0,\"TotalBaggageCharges\":0,\"TotalMealCharges\":0,\"TotalSeatCharges\":0},\"FareBreakdown\":[{\"Currency\":\"INR\",\"PassengerType\":1,\"PassengerCount\":1,\"BaseFare\":1534,\"Tax\":3760.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0}],\"Segments\":[{\"TripIndicator\":1,\"SegmentIndicator\":1,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"330\",\"FareClass\":\"Y\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":9,\"Origin\":{\"Airport\":{\"AirportCode\":\"DEL\",\"AirportName\":\"Indira Gandhi Airport\",\"Terminal\":\"1D\",\"CityCode\":\"DEL\",\"CityName\":\"Delhi\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T07:05:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BOM\",\"AirportName\":\"Mumbai\",\"Terminal\":\"1A\",\"CityCode\":\"BOM\",\"CityName\":\"Mumbai\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T09:10:00\"},\"Duration\":125,\"GroundTime\":0,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T09:10:00\",\"StopPointDepartureTime\":\"2016-07-21T07:05:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"},{\"TripIndicator\":1,\"SegmentIndicator\":2,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"323\",\"FareClass\":\"Y\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":9,\"Origin\":{\"Airport\":{\"AirportCode\":\"BOM\",\"AirportName\":\"Mumbai\",\"Terminal\":\"1B\",\"CityCode\":\"BOM\",\"CityName\":\"Mumbai\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T14:30:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BLR\",\"AirportName\":\"Bengaluru Intl\",\"Terminal\":\"\",\"CityCode\":\"BLR\",\"CityName\":\"Bangalore\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T16:10:00\"},\"AccumulatedDuration\":545,\"Duration\":100,\"GroundTime\":320,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T16:10:00\",\"StopPointDepartureTime\":\"2016-07-21T14:30:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"}],\"LastTicketDate\":\"2016-07-04T11:55:08\",\"TicketAdvisory\":null,\"FareRules\":[{\"Origin\":\"DEL\",\"Destination\":\"BOM\",\"Airline\":\"G8\",\"FareBasisCode\":\"YGO14OW\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"},{\"Origin\":\"BOM\",\"Destination\":\"BLR\",\"Airline\":\"G8\",\"FareBasisCode\":\"YGO14OW\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"}],\"AirlineCode\":\"G8\",\"ValidatingAirline\":\"G8\",\"BaggageAllowance\":\"15kg\"},{\"ResultIndex\":\"OB12\",\"Source\":10,\"IsLCC\":true,\"IsRefundable\":true,\"AirlineRemark\":null,\"Fare\":{\"Currency\":\"INR\",\"BaseFare\":2582,\"Tax\":3823.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0,\"OtherCharges\":15.00,\"ChargeBU\":[{\"key\":\"TBOMARKUP\",\"value\":0},{\"key\":\"CONVENIENCECHARGE\",\"value\":0},{\"key\":\"OTHERCHARGE\",\"value\":15.00}],\"Discount\":0,\"PublishedFare\":6420.03,\"CommissionEarned\":258.55,\"PLBEarned\":0.00,\"IncentiveEarned\":0.00,\"OfferedFare\":6161.48,\"TdsOnCommission\":12.93,\"TdsOnPLB\":0.00,\"TdsOnIncentive\":0.00,\"ServiceFee\":0,\"TotalBaggageCharges\":0,\"TotalMealCharges\":0,\"TotalSeatCharges\":0},\"FareBreakdown\":[{\"Currency\":\"INR\",\"PassengerType\":1,\"PassengerCount\":1,\"BaseFare\":2582,\"Tax\":3823.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0}],\"Segments\":[{\"TripIndicator\":1,\"SegmentIndicator\":1,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"344\",\"FareClass\":\"Q\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":6,\"Origin\":{\"Airport\":{\"AirportCode\":\"DEL\",\"AirportName\":\"Indira Gandhi Airport\",\"Terminal\":\"1D\",\"CityCode\":\"DEL\",\"CityName\":\"Delhi\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T22:30:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BOM\",\"AirportName\":\"Mumbai\",\"Terminal\":\"1A\",\"CityCode\":\"BOM\",\"CityName\":\"Mumbai\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-22T00:35:00\"},\"Duration\":125,\"GroundTime\":0,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-22T00:35:00\",\"StopPointDepartureTime\":\"2016-07-21T22:30:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"},{\"TripIndicator\":1,\"SegmentIndicator\":2,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"317\",\"FareClass\":\"Q\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":6,\"Origin\":{\"Airport\":{\"AirportCode\":\"BOM\",\"AirportName\":\"Mumbai\",\"Terminal\":\"1B\",\"CityCode\":\"BOM\",\"CityName\":\"Mumbai\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-22T06:00:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BLR\",\"AirportName\":\"Bengaluru Intl\",\"Terminal\":\"\",\"CityCode\":\"BLR\",\"CityName\":\"Bangalore\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-22T07:40:00\"},\"AccumulatedDuration\":550,\"Duration\":100,\"GroundTime\":325,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-22T07:40:00\",\"StopPointDepartureTime\":\"2016-07-22T06:00:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"}],\"LastTicketDate\":\"2016-07-04T11:55:08\",\"TicketAdvisory\":null,\"FareRules\":[{\"Origin\":\"DEL\",\"Destination\":\"BOM\",\"Airline\":\"G8\",\"FareBasisCode\":\"QGO7OW\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"},{\"Origin\":\"BOM\",\"Destination\":\"BLR\",\"Airline\":\"G8\",\"FareBasisCode\":\"QGO7OW\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"}],\"AirlineCode\":\"G8\",\"ValidatingAirline\":\"G8\",\"BaggageAllowance\":\"15kg\"},{\"ResultIndex\":\"OB13\",\"Source\":26,\"IsLCC\":true,\"IsRefundable\":true,\"AirlineRemark\":\"Special Non Commissionable Fare \",\"Fare\":{\"Currency\":\"INR\",\"BaseFare\":2721,\"Tax\":3830.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0,\"OtherCharges\":0,\"ChargeBU\":[{\"key\":\"TBOMARKUP\",\"value\":0},{\"key\":\"CONVENIENCECHARGE\",\"value\":0},{\"key\":\"OTHERCHARGE\",\"value\":0}],\"Discount\":0,\"PublishedFare\":6551.03,\"CommissionEarned\":0.00,\"PLBEarned\":0.00,\"IncentiveEarned\":0.00,\"OfferedFare\":6551.03,\"TdsOnCommission\":0.00,\"TdsOnPLB\":0.00,\"TdsOnIncentive\":0.00,\"ServiceFee\":0,\"TotalBaggageCharges\":0,\"TotalMealCharges\":0,\"TotalSeatCharges\":0},\"FareBreakdown\":[{\"Currency\":\"INR\",\"PassengerType\":1,\"PassengerCount\":1,\"BaseFare\":2721,\"Tax\":3830.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0}],\"Segments\":[{\"TripIndicator\":1,\"SegmentIndicator\":1,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"145\",\"FareClass\":\"W\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":9,\"Origin\":{\"Airport\":{\"AirportCode\":\"DEL\",\"AirportName\":\"Indira Gandhi Airport\",\"Terminal\":\"1D\",\"CityCode\":\"DEL\",\"CityName\":\"Delhi\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T09:25:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"IXR\",\"AirportName\":\"Ranchi\",\"Terminal\":\"\",\"CityCode\":\"IXR\",\"CityName\":\"Ranchi\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T11:05:00\"},\"Duration\":100,\"GroundTime\":0,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T11:05:00\",\"StopPointDepartureTime\":\"2016-07-21T09:25:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"},{\"TripIndicator\":1,\"SegmentIndicator\":2,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"272\",\"FareClass\":\"W\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":9,\"Origin\":{\"Airport\":{\"AirportCode\":\"IXR\",\"AirportName\":\"Ranchi\",\"Terminal\":\"\",\"CityCode\":\"IXR\",\"CityName\":\"Ranchi\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T13:25:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BLR\",\"AirportName\":\"Bengaluru Intl\",\"Terminal\":\"\",\"CityCode\":\"BLR\",\"CityName\":\"Bangalore\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T15:55:00\"},\"AccumulatedDuration\":390,\"Duration\":150,\"GroundTime\":140,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T15:55:00\",\"StopPointDepartureTime\":\"2016-07-21T13:25:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"}],\"LastTicketDate\":\"2016-07-04T11:55:08\",\"TicketAdvisory\":null,\"FareRules\":[{\"Origin\":\"DEL\",\"Destination\":\"IXR\",\"Airline\":\"G8\",\"FareBasisCode\":\"WGOSPLIP\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"},{\"Origin\":\"IXR\",\"Destination\":\"BLR\",\"Airline\":\"G8\",\"FareBasisCode\":\"WGOSPLIP\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"}],\"AirlineCode\":\"G8\",\"ValidatingAirline\":\"G8\",\"BaggageAllowance\":\"15kg\"},{\"ResultIndex\":\"OB14\",\"Source\":10,\"IsLCC\":true,\"IsRefundable\":true,\"AirlineRemark\":null,\"Fare\":{\"Currency\":\"INR\",\"BaseFare\":3044,\"Tax\":3850.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0,\"OtherCharges\":15.00,\"ChargeBU\":[{\"key\":\"TBOMARKUP\",\"value\":0},{\"key\":\"CONVENIENCECHARGE\",\"value\":0},{\"key\":\"OTHERCHARGE\",\"value\":15.00}],\"Discount\":0,\"PublishedFare\":6909.03,\"CommissionEarned\":281.01,\"PLBEarned\":0.00,\"IncentiveEarned\":0.00,\"OfferedFare\":6628.02,\"TdsOnCommission\":14.05,\"TdsOnPLB\":0.00,\"TdsOnIncentive\":0.00,\"ServiceFee\":0,\"TotalBaggageCharges\":0,\"TotalMealCharges\":0,\"TotalSeatCharges\":0},\"FareBreakdown\":[{\"Currency\":\"INR\",\"PassengerType\":1,\"PassengerCount\":1,\"BaseFare\":3044,\"Tax\":3850.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0}],\"Segments\":[{\"TripIndicator\":1,\"SegmentIndicator\":1,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"145\",\"FareClass\":\"R\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":8,\"Origin\":{\"Airport\":{\"AirportCode\":\"DEL\",\"AirportName\":\"Indira Gandhi Airport\",\"Terminal\":\"1D\",\"CityCode\":\"DEL\",\"CityName\":\"Delhi\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T09:25:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"IXR\",\"AirportName\":\"Ranchi\",\"Terminal\":\"\",\"CityCode\":\"IXR\",\"CityName\":\"Ranchi\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T11:05:00\"},\"Duration\":100,\"GroundTime\":0,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T11:05:00\",\"StopPointDepartureTime\":\"2016-07-21T09:25:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"},{\"TripIndicator\":1,\"SegmentIndicator\":2,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"272\",\"FareClass\":\"R\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":8,\"Origin\":{\"Airport\":{\"AirportCode\":\"IXR\",\"AirportName\":\"Ranchi\",\"Terminal\":\"\",\"CityCode\":\"IXR\",\"CityName\":\"Ranchi\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T13:25:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BLR\",\"AirportName\":\"Bengaluru Intl\",\"Terminal\":\"\",\"CityCode\":\"BLR\",\"CityName\":\"Bangalore\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T15:55:00\"},\"AccumulatedDuration\":390,\"Duration\":150,\"GroundTime\":140,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T15:55:00\",\"StopPointDepartureTime\":\"2016-07-21T13:25:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"}],\"LastTicketDate\":\"2016-07-04T11:55:08\",\"TicketAdvisory\":null,\"FareRules\":[{\"Origin\":\"DEL\",\"Destination\":\"IXR\",\"Airline\":\"G8\",\"FareBasisCode\":\"RGOOW\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"},{\"Origin\":\"IXR\",\"Destination\":\"BLR\",\"Airline\":\"G8\",\"FareBasisCode\":\"RGOOW\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"}],\"AirlineCode\":\"G8\",\"ValidatingAirline\":\"G8\",\"BaggageAllowance\":\"15kg\"},{\"ResultIndex\":\"OB15\",\"Source\":10,\"IsLCC\":true,\"IsRefundable\":true,\"AirlineRemark\":null,\"Fare\":{\"Currency\":\"INR\",\"BaseFare\":4604,\"Tax\":3944.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0,\"OtherCharges\":15.00,\"ChargeBU\":[{\"key\":\"TBOMARKUP\",\"value\":0},{\"key\":\"CONVENIENCECHARGE\",\"value\":0},{\"key\":\"OTHERCHARGE\",\"value\":15.00}],\"Discount\":0,\"PublishedFare\":8563.03,\"CommissionEarned\":356.82,\"PLBEarned\":0.00,\"IncentiveEarned\":0.00,\"OfferedFare\":8206.21,\"TdsOnCommission\":17.84,\"TdsOnPLB\":0.00,\"TdsOnIncentive\":0.00,\"ServiceFee\":0,\"TotalBaggageCharges\":0,\"TotalMealCharges\":0,\"TotalSeatCharges\":0},\"FareBreakdown\":[{\"Currency\":\"INR\",\"PassengerType\":1,\"PassengerCount\":1,\"BaseFare\":4604,\"Tax\":3944.03,\"YQTax\":2738,\"AdditionalTxnFeeOfrd\":0,\"AdditionalTxnFeePub\":0}],\"Segments\":[{\"TripIndicator\":1,\"SegmentIndicator\":1,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"286\",\"FareClass\":\"U\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":9,\"Origin\":{\"Airport\":{\"AirportCode\":\"DEL\",\"AirportName\":\"Indira Gandhi Airport\",\"Terminal\":\"1D\",\"CityCode\":\"DEL\",\"CityName\":\"Delhi\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T10:40:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"GOI\",\"AirportName\":\"Dabolim\",\"Terminal\":\"\",\"CityCode\":\"GOI\",\"CityName\":\"Goa\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T13:20:00\"},\"Duration\":160,\"GroundTime\":0,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T13:20:00\",\"StopPointDepartureTime\":\"2016-07-21T10:40:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"},{\"TripIndicator\":1,\"SegmentIndicator\":2,\"Airline\":{\"AirlineCode\":\"G8\",\"AirlineName\":\"GoAir\",\"FlightNumber\":\"286\",\"FareClass\":\"U\",\"OperatingCarrier\":\"\"},\"NoOfSeatAvailable\":9,\"Origin\":{\"Airport\":{\"AirportCode\":\"GOI\",\"AirportName\":\"Dabolim\",\"Terminal\":\"\",\"CityCode\":\"GOI\",\"CityName\":\"Goa\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"DepTime\":\"2016-07-21T13:50:00\"},\"Destination\":{\"Airport\":{\"AirportCode\":\"BLR\",\"AirportName\":\"Bengaluru Intl\",\"Terminal\":\"\",\"CityCode\":\"BLR\",\"CityName\":\"Bangalore\",\"CountryCode\":\"IN\",\"CountryName\":\"India\"},\"ArrTime\":\"2016-07-21T14:55:00\"},\"AccumulatedDuration\":255,\"Duration\":65,\"GroundTime\":30,\"Mile\":0,\"StopOver\":false,\"StopPoint\":\"\",\"StopPointArrivalTime\":\"2016-07-21T14:55:00\",\"StopPointDepartureTime\":\"2016-07-21T13:50:00\",\"Craft\":\"320\",\"Remark\":null,\"IsETicketEligible\":true,\"FlightStatus\":\"Confirmed\",\"Status\":\"\"}],\"LastTicketDate\":\"2016-07-04T11:55:08\",\"TicketAdvisory\":null,\"FareRules\":[{\"Origin\":\"DEL\",\"Destination\":\"GOI\",\"Airline\":\"G8\",\"FareBasisCode\":\"UGOOW\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"},{\"Origin\":\"GOI\",\"Destination\":\"BLR\",\"Airline\":\"G8\",\"FareBasisCode\":\"UGOOW\",\"FareRuleDetail\":\"\",\"FareRestriction\":\"\"}],\"AirlineCode\":\"G8\",\"ValidatingAirline\":\"G8\",\"BaggageAllowance\":\"15kg\"}]}}";
            try
            {
                strJsonResponse = TBOAuthProcess.GetResponse(ServiceUrl.SearchUrl, strJsonData);
                // Response items = new Response();

                items = JsonConvert.DeserializeObject<FlightResult>(strJsonResponse);
            }
            catch (Exception ex1)
            {

                strJsonResponse = strJsonResponse + "error: " + ex1.Message + "stack:" + ex1.StackTrace.ToString();// +"" + Convert.ToString(ex1.InnerException.Message);
            }

            TBOUtitlity.SaveFile(strJsonResponse, "SearchRes_" + objFlt.HidTxtAirLine);
            SaveResponse.SAVElOGFILE(strJsonResponse, "REQ", "txt", "", objFlt.HidTxtAirLine, fareType);

            List<FlightSearchResults> fsrList = new List<FlightSearchResults>();
            List<FlightSearchResults> RfsrList = new List<FlightSearchResults>();
            List<FlightSearchResults> FinalList = new List<FlightSearchResults>();
            //string rrr = GetEntityXml(items.Response.Results);

            FlightCommonBAL objFltComm = new FlightCommonBAL(constr);
            //List<BaggageList> BagInfoList = new List<BaggageList>();
            //BagInfoList = objFltComm.GetBaggage(objFlt.Trip.ToString(), objFlt.HidTxtAirLine);

            int flight = 1;
            foreach (var trip in items.Response.Results)
            {
                int lineNum = 1;
                foreach (var flt in trip)
                {


                    //List<Segments> seglist =  flt.Segments.Select(x=>x.)
                    foreach (var seglist in flt.Segments)
                    {
                        int leg = 1;
                        foreach (var seg in seglist)
                        {
                            FlightSearchResults fsr = new FlightSearchResults();
                            fsr.Leg = leg;
                            fsr.LineNumber = lineNum;

                            if (objFlt.Trip == Trip.I)
                            {
                                flight = seg.TripIndicator;
                            }
                            fsr.Flight = flight.ToString();
                            fsr.Stops = (seglist.Count - 1).ToString() + "-Stop";

                            fsr.Adult = objFlt.Adult;
                            fsr.Child = objFlt.Child;
                            fsr.Infant = objFlt.Infant;


                            fsr.depdatelcc = seg.Origin.DepTime.Trim();
                            fsr.arrdatelcc = seg.Destination.ArrTime.Trim();
                            fsr.Departure_Date = Convert.ToDateTime(seg.Origin.DepTime.Trim()).ToString("dd MMM"); // legdetailsM.DepartureDate[8].ToString() + legdetailsM.DepartureDate[9].ToString() + " " + GetMonthName(Convert.ToInt16(legdetailsM.DepartureDate[5].ToString() + legdetailsM.DepartureDate[6].ToString()));
                            fsr.Arrival_Date = Convert.ToDateTime(seg.Destination.ArrTime.Trim()).ToString("dd MMM");// legdetailsM.ArrivalDate[8].ToString() + legdetailsM.ArrivalDate[9].ToString() + " " + GetMonthName(Convert.ToInt16(legdetailsM.ArrivalDate[5].ToString() + legdetailsM.ArrivalDate[6].ToString()));
                            fsr.DepartureDate = Convert.ToDateTime(seg.Origin.DepTime.Trim()).ToString("ddMMyy");
                            fsr.ArrivalDate = Convert.ToDateTime(seg.Destination.ArrTime.Trim()).ToString("ddMMyy");


                            fsr.FlightIdentification = seg.Airline.FlightNumber;
                            fsr.AirLineName = seg.Airline.AirlineName;

                            fsr.ValiDatingCarrier = flt.ValidatingAirline.Trim();
                            fsr.OperatingCarrier = seg.Airline.OperatingCarrier.Trim();
                            fsr.MarketingCarrier = flt.AirlineCode.Trim();//seg.Airline.OperatingCarrier.Trim();
                            fsr.fareBasis = seg.Airline.FareClass;


                            fsr.DepartureLocation = seg.Origin.Airport.CityCode.Trim();
                            fsr.DepartureCityName = seg.Origin.Airport.CityName.Trim();
                            fsr.DepartureTime = Convert.ToDateTime(seg.Origin.DepTime.Trim()).ToString("HHmm");
                            fsr.DepartureAirportName = seg.Origin.Airport.AirportName.Trim();
                            fsr.DepartureTerminal = seg.Origin.Airport.Terminal.Trim();
                            fsr.DepAirportCode = seg.Origin.Airport.AirportCode.Trim();

                            fsr.ArrivalLocation = seg.Destination.Airport.CityCode.Trim();
                            fsr.ArrivalCityName = seg.Destination.Airport.CityName.Trim();
                            fsr.ArrivalTime = Convert.ToDateTime(seg.Destination.ArrTime.Trim()).ToString("HHmm");
                            fsr.ArrivalAirportName = seg.Destination.Airport.AirportName.Trim();
                            fsr.ArrivalTerminal = seg.Destination.Airport.Terminal.Trim();
                            fsr.ArrAirportCode = seg.Destination.Airport.AirportCode.Trim();
                            fsr.Trip = objFlt.Trip.ToString();

                            fsr.TotDur = GetTimeInHrsAndMin(seg.Duration);
                            fsr.TripCnt= GetTimeInHrsAndMin(seg.Duration);

                            #region Fare
                            float ChargeBU = 0;

                            foreach (var objChargBU in flt.Fare.ChargeBU)
                            {
                                ChargeBU += float.Parse(objChargBU.value.ToString());

                            }
                            DataTable CommDt = new DataTable();
                            Hashtable STTFTDS = new Hashtable();
                            //FlightCommonBAL objFltComm = new FlightCommonBAL(constr);
                            foreach (var paxFare in flt.FareBreakdown)
                            {

                                //fsr.AdtAvlStatus = fareInfo.SeatsAvailable.Trim();

                                if (paxFare.PassengerType == 1)
                                {

                                    fsr.AdtBfare = (float)Math.Ceiling(float.Parse((paxFare.BaseFare / paxFare.PassengerCount).ToString()));
                                    fsr.AdtCabin = "Y";
                                    fsr.AdtFSur = (float)Math.Ceiling(float.Parse((paxFare.YQTax / paxFare.PassengerCount).ToString()));
                                    fsr.AdtTax = (float)Math.Ceiling(float.Parse((paxFare.Tax / paxFare.PassengerCount).ToString()) + float.Parse( paxFare.AdditionalTxnFeePub.ToString()) + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                    fsr.AdtOT = (float)Math.Ceiling(fsr.AdtTax - fsr.AdtFSur);// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                    fsr.AdtRbd = seg.Airline.FareClass.ToString();
                                    fsr.AdtFarebasis = flt.FareRules[0].FareBasisCode.ToString();//; fareInfo.FBCode.Trim();
                                    fsr.AdtFareType = flt.IsRefundable == true ? "Refundable" : "Non Refundable";// fareInfo.FareTypeID.Trim();
                                    // fsr.AdtFareTypeName = fareInfo.FareTypeName.Trim();
                                    fsr.AdtFare = (float)Math.Ceiling(float.Parse((fsr.AdtTax + fsr.AdtBfare).ToString())); //  + srvCharge
                                    fsr.sno = flt.ResultIndex + ":" + items.Response.TraceId + ":" + flt.IsLCC + ":" + flt.Source;//fareInfo.FareID.Trim() + ":" + securityGUID + ":" + fareInfo.FareTypeName.Trim();
                                    fsr.fareBasis = fsr.AdtFarebasis;
                                    fsr.FBPaxType = "ADT";


                                    fsr.AdtFar = string.IsNullOrEmpty(flt.AirlineRemark) ? "" : flt.AirlineRemark.ToLower();
                                    fsr.BagInfo = fsr.AdtFar;
                                    if (!string.IsNullOrEmpty(flt.AirlineRemark) && fsr.AdtFar.Trim().Contains("special non commissionable fare"))
                                    {
                                        fsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier + "S", fsr.AdtFare, objFlt.Trip.ToString());
                                        fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier + "S", fsr.AdtFare, objFlt.Trip.ToString());

                                        fsr.AdtDiscount1 = 0;  //-AdtComm  
                                        fsr.AdtCB = 0;
                                        fsr.AdtSrvTax1 = 0;// added TO TABLE
                                        fsr.AdtDiscount = 0;
                                        fsr.AdtEduCess = 0;
                                        fsr.AdtHighEduCess = 0;
                                        fsr.AdtTF = 0;
                                        fsr.AdtTds = 0;
                                    }
                                    else
                                    {
                                        fsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, objFlt.Trip.ToString());
                                        fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, objFlt.Trip.ToString());

                                        CommDt.Clear();
                                        CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), 1, fsr.AdtRbd, fsr.AdtCabin, objFlt.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, objFlt.RetDate, fsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", "");
                                        fsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                        fsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                        STTFTDS.Clear();
                                        STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, fsr.ValiDatingCarrier, fsr.AdtDiscount1, fsr.AdtBfare, fsr.AdtFSur, objFlt.TDS);
                                        fsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                        fsr.AdtDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                        fsr.AdtEduCess = 0;
                                        fsr.AdtHighEduCess = 0;
                                        fsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                        fsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                    }





                                    if (objFlt.IsCorp == true)
                                    {
                                        try
                                        {
                                            DataTable MGDT = new DataTable();
                                            MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(fsr.AdtFare.ToString()));
                                            fsr.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                            fsr.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                        }
                                        catch { }
                                    }


                                }

                                if (paxFare.PassengerType == 2)
                                {
                                    fsr.ChdBFare = (float)Math.Ceiling(float.Parse((paxFare.BaseFare / paxFare.PassengerCount).ToString()));
                                    fsr.ChdCabin = "Y";
                                    fsr.ChdFSur = (float)Math.Ceiling(float.Parse((paxFare.YQTax / paxFare.PassengerCount).ToString()));
                                    fsr.ChdTax = (float)Math.Ceiling(float.Parse((paxFare.Tax / paxFare.PassengerCount).ToString()) +float.Parse( paxFare.AdditionalTxnFeePub.ToString()) + srvCharge);
                                    fsr.ChdOT = (float)Math.Ceiling(fsr.ChdTax - fsr.ChdFSur);// + paxFare.AdditionalTxnFeePub + srvCharge);
                                    fsr.ChdRbd = seg.Airline.FareClass.ToString();
                                    fsr.ChdFarebasis = flt.FareRules[0].FareBasisCode.ToString();//; fareInfo.FBCode.Trim();
                                    fsr.ChdFare = (float)Math.Ceiling(float.Parse((fsr.ChdTax + fsr.ChdBFare).ToString())); //  + srvCharge

                                    //fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, objFlt.Trip.ToString());



                                    if (!string.IsNullOrEmpty(flt.AirlineRemark) && fsr.AdtFar.Trim().Contains("special non commissionable fare"))
                                    {
                                        fsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier + "S", fsr.ChdFare, fsr.Trip.ToString());
                                        fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier + "S", fsr.ChdFare, objFlt.Trip.ToString());
                                        fsr.ChdDiscount1 = 0;  //-AdtComm  
                                        fsr.ChdCB = 0;
                                        fsr.ChdSrvTax1 = 0;// added TO TABLE
                                        fsr.ChdDiscount = 0;
                                        fsr.ChdEduCess = 0;
                                        fsr.ChdHighEduCess = 0;
                                        fsr.ChdTF = 0;
                                        fsr.ChdTds = 0;
                                    }
                                    else
                                    {
                                        fsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, fsr.Trip.ToString());
                                        fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, objFlt.Trip.ToString());
                                        CommDt.Clear();
                                        CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), 1, fsr.ChdRbd, fsr.ChdCabin, objFlt.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, objFlt.RetDate, fsr.ChdFarebasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", "");
                                        fsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());//-ChdComm
                                        fsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                        STTFTDS.Clear();
                                        STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, fsr.ValiDatingCarrier, fsr.ChdDiscount1, fsr.ChdBFare, fsr.ChdFSur, objFlt.TDS);
                                        fsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE 
                                        fsr.ChdDiscount = fsr.ChdDiscount1 - fsr.ChdSrvTax1;
                                        fsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                        fsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                        fsr.ChdEduCess = 0;
                                        fsr.ChdHighEduCess = 0;
                                    }
                                    if (objFlt.IsCorp == true)
                                    {
                                        try
                                        {
                                            DataTable MGDT = new DataTable();
                                            MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(fsr.ChdFare.ToString()));
                                            fsr.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                            fsr.ChdSrvTax1 = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                        }
                                        catch { }
                                    }

                                }


                                if (paxFare.PassengerType == 3)
                                {
                                    fsr.InfBfare = (float)Math.Ceiling(float.Parse((paxFare.BaseFare / paxFare.PassengerCount).ToString()));
                                    fsr.InfCabin = "";
                                    fsr.InfFSur = (float)Math.Ceiling(float.Parse((paxFare.YQTax / paxFare.PassengerCount).ToString()));
                                    fsr.InfTax = (float)Math.Ceiling(float.Parse((paxFare.Tax / paxFare.PassengerCount).ToString()) + float.Parse(paxFare.AdditionalTxnFeePub.ToString()));
                                    fsr.InfOT = (float)Math.Ceiling(fsr.InfTax - fsr.InfFSur);//+ paxFare.AdditionalTxnFeePub);
                                    fsr.InfRbd = seg.Airline.FareClass.ToString();
                                    fsr.InfFarebasis = flt.FareRules[0].FareBasisCode.ToString();//; fareInfo.FBCode.Trim();
                                    fsr.InfFare = (float)Math.Ceiling(float.Parse((fsr.InfTax + fsr.InfBfare).ToString()));


                                    if (objFlt.IsCorp == true)
                                    {
                                        try
                                        {
                                            DataTable MGDT = new DataTable();
                                            MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.InfBfare.ToString()), decimal.Parse(fsr.InfFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(fsr.InfFare.ToString()));
                                            fsr.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                            fsr.InfSrvTax1 = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                        }
                                        catch { }
                                    }

                                }





                            }


                            fsr.Searchvalue = JsonConvert.SerializeObject(flt.Fare) + "itzDanata420" + JsonConvert.SerializeObject(flt.FareBreakdown);
                            fsr.OriginalTF = float.Parse(flt.Fare.OfferedFare.ToString());
                            fsr.OriginalTT = srvCharge;
                            fsr.Provider = "TB";

                            fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;
                            //fsr.TotalFare =(float)Math.Ceiling( float.Parse(flt.Fare.PublishedFare.ToString()));
                            //fsr.TotalFuelSur = (float)Math.Ceiling(float.Parse(flt.Fare.YQTax.ToString()));//(fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                            //fsr.TotalTax =(float)Math.Ceiling( float.Parse(flt.Fare.Tax.ToString()) + float.Parse(flt.Fare.OtherCharges.ToString()) + float.Parse(flt.Fare.AdditionalTxnFeePub.ToString()));// (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                            //fsr.OriginalTT = fsr.TotalTax;
                            //fsr.TotBfare =(float)Math.Ceiling( float.Parse(flt.Fare.BaseFare.ToString()));// (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);


                            fsr.STax = (fsr.AdtSrvTax * fsr.Adult) + (fsr.ChdSrvTax * fsr.Child) + (fsr.InfSrvTax * fsr.Infant);
                            fsr.TFee = (fsr.AdtTF * fsr.Adult) + (fsr.ChdTF * fsr.Child);// +(objlist.InfTF * objlist.Infant);
                            fsr.TotDis = (fsr.AdtDiscount * fsr.Adult) + (fsr.ChdDiscount * fsr.Child);
                            fsr.TotCB = (fsr.AdtCB * fsr.Adult) + (fsr.ChdCB * fsr.Child);
                            fsr.TotTds = (fsr.AdtTds * fsr.Adult) + (fsr.ChdTds * fsr.Child);// +objFS.InfTds;
                            fsr.TotMgtFee = (fsr.AdtMgtFee * fsr.Adult) + (fsr.ChdMgtFee * fsr.Child) + (fsr.InfMgtFee * fsr.Infant);



                            fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                            fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                            fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                            fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                            fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                            fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                            fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);
                            fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee;
                            fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));






                            if (objFlt.RTF == true || objFlt.GDSRTF == true || isSplFare == true)
                            {
                                fsr.Sector = objFlt.HidTxtDepCity.Split(',')[0] + ":" + objFlt.HidTxtArrCity.Split(',')[0] + ":" + objFlt.HidTxtDepCity.Split(',')[0];
                            }
                            else
                            {
                                if (fareType == "INB")
                                {
                                    fsr.Sector = objFlt.HidTxtDepCity.Split(',')[0] + ":" + objFlt.HidTxtArrCity.Split(',')[0];
                                }
                                else
                                {
                                    fsr.Sector = objFlt.HidTxtArrCity.Split(',')[0] + ":" + objFlt.HidTxtDepCity.Split(',')[0];
                                }

                            }


                            #endregion



                            if (flight == 1)
                            {
                                if (fareType == "INB")
                                {
                                    fsr.OrgDestFrom = objFlt.HidTxtDepCity.Split(',')[0];
                                    fsr.OrgDestTo = objFlt.HidTxtArrCity.Split(',')[0];
                                }
                                else
                                {
                                    fsr.OrgDestFrom = objFlt.HidTxtArrCity.Split(',')[0];
                                    fsr.OrgDestTo = objFlt.HidTxtDepCity.Split(',')[0];
                                }
                                fsr.TripType = TripType.O.ToString();
                            }
                            else if (flight == 2)
                            {
                                fsr.OrgDestFrom = objFlt.HidTxtArrCity.Split(',')[0];
                                fsr.OrgDestTo = objFlt.HidTxtDepCity.Split(',')[0];
                                fsr.TripType = TripType.R.ToString();
                            }

                            if (flight == 1 || objFlt.Trip == Trip.I)
                            {

                                fsrList.Add(fsr);
                            }
                            else if (flight == 2 && objFlt.Trip == Trip.D)
                            {

                                RfsrList.Add(fsr);
                            }


                            leg++;
                        }
                    }


                    lineNum++;


                }

                flight++;
            }

            if (isSplFare && objFlt.Trip == Trip.D)
            {

                FinalList = RoundTripFare(fsrList, RfsrList, srvCharge);
            }
            else
            {
                FinalList = fsrList;
            }

            return objFltComm.AddFlightKey( FinalList,isSplFare);
        }
        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;
            //int IATAComm = 0;
            decimal originalDis = 0;
            Hashtable STHT = new Hashtable();
            if (string.IsNullOrEmpty(TDS))
            {
                TDS = "0";
            }

            try
            {
                STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                originalDis = decimal.Parse(Dis.ToString()) - decimal.Parse(STHT["TFee"].ToString());
                STHT.Add("STax", Math.Round(((originalDis * STaxP) / 100), 0));
                STHT.Add("Tds", Math.Round((((originalDis - decimal.Parse(STHT["STax"].ToString())) * decimal.Parse(TDS)) / 100), 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Clear();
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }

        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray;
            double mrkamt = 0;
            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");

                if (!(airMrkArray != null && airMrkArray.Length > 0))
                {
                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

                }

                if (airMrkArray.Length > 0)
                {

                    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    {
                        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    }
                    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    }

                    //if (Trip == "I")
                    //{
                    //    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    //    {
                    //        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    //    }
                    //    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    //    {
                    //        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    //    }
                    //}
                    //else
                    //{
                    //    mrkamt = Convert.ToDouble(airMrkArray[0]["MarkUp"].ToString());
                    //}
                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return float.Parse(mrkamt.ToString());
        }
        private List<FlightSearchResults> RoundTripFare(List<FlightSearchResults> objO, List<FlightSearchResults> objR, float srvCharge)
        {

            int ln = 1;//For Total Line No.         
            int LnOb = objO[objO.Count - 1].LineNumber;
            int LnIb = objR[objR.Count - 1].LineNumber;
            List<FlightSearchResults> Comb = new List<FlightSearchResults>();
            List<FlightSearchResults> Final = new List<FlightSearchResults>();
            for (int k = 1; k <= LnOb; k++)
            {
                var OB = (from ct in objO where ct.LineNumber == k select ct).ToList();

                for (int l = 1; l <= LnIb; l++)
                {
                    var IB = (from c in objR where c.LineNumber == l select c).ToList();
                    //List<FlightSearchResults> st = new List<FlightSearchResults>();

                    if (OB[OB.Count - 1].DepartureDate.Split('T')[0] == IB[0].DepartureDate.Split('T')[0])
                    {
                        int obtmin = Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(2, 2));
                        int ibtmin = Convert.ToInt16(IB[0].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(IB[0].DepartureTime.Substring(2, 2));

                        if ((obtmin + 120) <= ibtmin)
                        {
                            // st = Merge(OB, IB, ln);
                            Final.AddRange(Merge(OB, IB, ln, srvCharge));
                            ln++;///Increment Total Ln
                        }
                    }
                    else
                    {
                        Final.AddRange(Merge(OB, IB, ln, srvCharge));
                        ln++;
                    }

                }

            }
            Comb.AddRange(Final);
            return Comb;
        }

        private List<FlightSearchResults> Merge(List<FlightSearchResults> OB, List<FlightSearchResults> IB, int Ln, float srvCharge)
        {
            List<FlightSearchResults> Final = new List<FlightSearchResults>();

            float AdtFSur = 0, AdtWO = 0, AdtIN = 0, AdtJN = 0, AdtYR = 0, AdtBfare = 0, AdtOT = 0, AdtFare = 0, AdtTax = 0;//,
            //ADTAdminMrk = 0, ADTAgentMrk = 0, AdtDiscount = 0, AdtCB = 0, AdtSrvTax = 0, AdtTF = 0, AdtTds = 0, IATAComm = 0;
            float ChdFSur = 0, ChdWO = 0, ChdIN = 0, ChdJN = 0, ChdYR = 0, ChdBFare = 0, ChdOT = 0, ChdFare = 0, ChdTax = 0;//,
            //CHDAdminMrk = 0, CHDAgentMrk = 0, ChdDiscount = 0, ChdCB = 0, ChdSrvTax = 0, ChdTF = 0, ChdTds = 0;
            float InfFSur = 0, InfIN = 0, InfJN = 0, InfOT = 0, InfQ = 0, InfFare = 0, InfBfare = 0, InfTax = 0;//,
            //InfSrvTax = 0, InfTF = 0;
            float ADTAgentMrk = 0, CHDAgentMrk = 0;

            float AdtDiscount1 = 0, AdtCB = 0, AdtSrvTax1 = 0, AdtDiscount = 0, AdtTF = 0, AdtTds = 0;
            float ChdDiscount1 = 0, ChdCB = 0, ChdSrvTax1 = 0, ChdDiscount = 0, ChdTF = 0, ChdTds = 0;
            float STax = 0, TFee = 0, TotDis = 0, TotCB = 0, TotTds = 0;
            float ADTAdminMrk = 0, ChdAdminMrk = 0;
            float TotMgtFee = 0, AdtMgtFee = 0, ChdMgtFee = 0, InfMgtFee = 0;







            var ObF = OB.Select(x => (FlightSearchResults)x.Clone()).ToList();
            var IbF = IB.Select(x => (FlightSearchResults)x.Clone()).ToList();
            var item = (FlightSearchResults)OB[0].Clone();
            var itemib = (FlightSearchResults)IB[0].Clone();
            #region ADULT
            int Adult = item.Adult;
            AdtFSur = AdtFSur + item.AdtFSur + itemib.AdtFSur;
            AdtWO = AdtWO + item.AdtWO + itemib.AdtWO;
            AdtIN = AdtIN + item.AdtIN + itemib.AdtIN;
            AdtJN = AdtJN + item.AdtJN + itemib.AdtJN;
            AdtYR = AdtYR + item.AdtYR + itemib.AdtYR;
            AdtBfare = AdtBfare + item.AdtBfare + itemib.AdtBfare;
            AdtOT = AdtOT + item.AdtOT + itemib.AdtOT - srvCharge;
            AdtFare = AdtFare + item.AdtFare + itemib.AdtFare - srvCharge;
            AdtTax = AdtTax + item.AdtTax + itemib.AdtTax - srvCharge;
            ADTAgentMrk = ADTAgentMrk + item.ADTAgentMrk + itemib.ADTAgentMrk;
            ADTAdminMrk = ADTAdminMrk + item.ADTAdminMrk + itemib.ADTAdminMrk;

            AdtDiscount1 = AdtDiscount1 + item.AdtDiscount1 + itemib.AdtDiscount1;
            AdtCB = AdtCB + item.AdtCB + itemib.AdtCB; ;
            AdtSrvTax1 = AdtSrvTax1 + item.AdtSrvTax1 + itemib.AdtSrvTax1; ;
            AdtDiscount = AdtDiscount + item.AdtDiscount + itemib.AdtDiscount; ;
            AdtTF = AdtTF + item.AdtTF + itemib.AdtTF; ;
            AdtTds = AdtTds + item.AdtTds + itemib.AdtTds;
            AdtMgtFee = AdtMgtFee + item.AdtMgtFee + itemib.AdtMgtFee;

            #endregion

            #region CHILD
            int Child = item.Child;
            ChdFSur = ChdFSur + item.ChdFSur + itemib.ChdFSur;
            ChdWO = ChdWO + item.ChdWO + itemib.ChdWO;
            ChdIN = ChdIN + item.ChdIN + itemib.ChdIN;
            ChdJN = ChdJN + item.ChdJN + itemib.ChdJN;
            ChdYR = ChdYR + item.ChdYR + itemib.ChdYR;
            ChdBFare = ChdBFare + item.ChdBFare + itemib.ChdBFare;
            ChdOT = ChdOT + item.ChdOT + itemib.ChdOT - srvCharge;
            ChdFare = ChdFare + item.ChdFare + itemib.ChdFare - srvCharge;
            ChdTax = ChdTax + item.ChdTax + itemib.ChdTax - srvCharge;
            CHDAgentMrk = CHDAgentMrk + item.CHDAgentMrk + itemib.CHDAgentMrk;
            ChdAdminMrk = ChdAdminMrk + item.CHDAdminMrk + itemib.CHDAdminMrk;


            ChdDiscount1 = ChdDiscount1 + item.ChdDiscount1 + itemib.ChdDiscount1;
            ChdCB = ChdCB + item.ChdCB + itemib.ChdCB;
            ChdSrvTax1 = ChdSrvTax1 + item.ChdSrvTax1 + itemib.ChdSrvTax1;
            ChdDiscount = ChdDiscount + item.ChdDiscount + itemib.ChdDiscount;
            ChdTF = ChdTF + item.ChdTF + itemib.ChdTF;
            ChdTds = ChdTds + item.ChdTds + itemib.ChdTds;
            ChdMgtFee = ChdMgtFee + item.ChdMgtFee + itemib.ChdMgtFee;


            #endregion

            #region INFANT
            int Infant = item.Infant;
            InfFare = InfFare + item.InfFare + itemib.InfFare;
            InfBfare = InfBfare + item.InfBfare + itemib.InfBfare;
            InfFSur = InfFSur + item.InfFSur + itemib.InfFSur;
            InfIN = InfIN + item.InfIN + itemib.InfIN;
            InfJN = InfJN + item.InfJN + itemib.InfJN;
            InfOT = InfOT + item.InfOT + itemib.InfOT;
            InfQ = InfQ + item.InfQ + itemib.InfQ;
            InfTax = InfTax + item.InfTax + itemib.InfTax;
            InfMgtFee = InfMgtFee + item.InfMgtFee + itemib.InfMgtFee;

            #endregion

            #region TOTAL

            STax = 0;// (AdtSrvTax1 * Adult) + (ChdSrvTax1 * Child);
            TFee = (AdtTF * Adult) + (ChdTF * Child);// +(objlist.InfTF * objlist.Infant);
            TotDis = (AdtDiscount * Adult) + (ChdDiscount * Child);
            TotCB = (AdtCB * Adult) + (ChdCB * Child);
            TotTds = (AdtTds * Adult) + (ChdTds * Child);
            TotMgtFee = (AdtMgtFee * Adult) + (ChdMgtFee * Child) + (InfMgtFee * Infant);

            float OriginalTF = item.OriginalTF + itemib.OriginalTF;
            float OriginalTT = srvCharge;// item.OriginalTT + itemib.OriginalTT;
            float TotBfare = (AdtBfare * Adult) + (ChdBFare * Child) + (InfBfare * Infant);
            float TotalTax = (AdtTax * Adult) + (ChdTax * Child) + (InfTax * Infant);
            float TotalFuelSur = (AdtFSur * Adult) + (ChdFSur * Child);
            float TotMrkUp = (ADTAgentMrk * Adult) + (CHDAgentMrk * Child) + (ADTAdminMrk * Adult) + (ChdAdminMrk * Child);
            float TotalFare = (float)Math.Ceiling(Convert.ToDecimal((AdtFare * Adult) + (ChdFare * Child) + (InfFare * Infant)));
            TotalFare = TotalFare + TotMrkUp + STax + TFee + TotMgtFee;
            float NetFare = (TotalFare + TotTds) - (TotDis + TotCB + (ADTAgentMrk * Adult) + (CHDAgentMrk * Child));
            #endregion


            ObF.ForEach(y =>
            {
                #region Adult
                y.LineNumber = Ln;
                y.AdtFSur = AdtFSur;
                y.AdtIN = AdtIN;
                y.AdtJN = AdtJN;
                y.AdtYR = AdtYR;
                y.AdtBfare = AdtBfare;
                y.AdtOT = AdtOT;
                y.AdtFare = AdtFare;
                y.AdtTax = AdtTax;
                y.ADTAgentMrk = ADTAgentMrk;


                y.AdtDiscount1 = AdtDiscount1;
                y.AdtCB = AdtCB;
                y.AdtSrvTax1 = AdtSrvTax1;
                y.AdtDiscount = AdtDiscount;
                y.AdtTF = AdtTF;
                y.AdtTds = AdtTds;
                y.AdtMgtFee = AdtMgtFee;
                #endregion

                #region Child
                y.ChdFSur = ChdFSur;
                y.ChdWO = ChdWO;
                y.ChdIN = ChdIN;
                y.ChdJN = ChdJN;
                y.ChdYR = ChdYR;
                y.ChdBFare = ChdBFare;
                y.ChdOT = ChdOT;
                y.ChdFare = ChdFare;
                y.ChdTax = ChdTax;
                y.CHDAgentMrk = CHDAgentMrk;


                y.ChdDiscount1 = ChdDiscount1;
                y.ChdCB = ChdCB;
                y.ChdSrvTax1 = ChdSrvTax1;
                y.ChdDiscount = ChdDiscount;
                y.ChdTF = ChdTF;
                y.ChdTds = ChdTds;
                y.ChdMgtFee = ChdMgtFee;
                #endregion

                #region Infant
                y.InfFare = InfFare;
                y.InfBfare = InfBfare;
                y.InfFSur = InfFSur;
                y.InfIN = InfIN;
                y.InfJN = InfJN;
                y.InfOT = InfOT;
                y.InfQ = InfQ;
                y.InfTax = InfTax;
                y.InfMgtFee = InfMgtFee;
                #endregion

                #region Total
                y.TotBfare = TotBfare;
                y.TotalTax = TotalTax;
                y.TotalFuelSur = TotalFuelSur;
                y.TotalFare = TotalFare;
                y.OriginalTF = OriginalTF;
                y.OriginalTT = OriginalTT;
                y.NetFare = NetFare;


                y.STax = STax;
                y.TFee = TFee;
                y.TotDis = TotDis;
                y.TotCB = TotCB;
                y.TotTds = TotTds;
                y.TotMgtFee = TotMgtFee;
                #endregion
            });
            Final.AddRange(ObF);



            IbF.ForEach(y =>
            {
                #region Adult
                y.LineNumber = Ln;
                y.AdtFSur = AdtFSur;
                y.AdtIN = AdtIN;
                y.AdtJN = AdtJN;
                y.AdtYR = AdtYR;
                y.AdtBfare = AdtBfare;
                y.AdtOT = AdtOT;
                y.AdtFare = AdtFare;
                y.AdtTax = AdtTax;
                y.ADTAgentMrk = ADTAgentMrk;

                y.AdtDiscount1 = AdtDiscount1;
                y.AdtCB = AdtCB;
                y.AdtSrvTax1 = AdtSrvTax1;
                y.AdtDiscount = AdtDiscount;
                y.AdtTF = AdtTF;
                y.AdtTds = AdtTds;
                y.AdtMgtFee = AdtMgtFee;
                #endregion

                #region Child
                y.ChdFSur = ChdFSur;
                y.ChdWO = ChdWO;
                y.ChdIN = ChdIN;
                y.ChdJN = ChdJN;
                y.ChdYR = ChdYR;
                y.ChdBFare = ChdBFare;
                y.ChdOT = ChdOT;
                y.ChdFare = ChdFare;
                y.ChdTax = ChdTax;
                y.CHDAgentMrk = CHDAgentMrk;

                y.ChdDiscount1 = ChdDiscount1;
                y.ChdCB = ChdCB;
                y.ChdSrvTax1 = ChdSrvTax1;
                y.ChdDiscount = ChdDiscount;
                y.ChdTF = ChdTF;
                y.ChdTds = ChdTds;
                y.ChdMgtFee = ChdMgtFee;

                #endregion

                #region Infant
                y.InfFare = InfFare;
                y.InfBfare = InfBfare;
                y.InfFSur = InfFSur;
                y.InfIN = InfIN;
                y.InfJN = InfJN;
                y.InfOT = InfOT;
                y.InfQ = InfQ;
                y.InfTax = InfTax;
                y.InfMgtFee = InfMgtFee;
                #endregion

                #region Total
                y.TotBfare = TotBfare;
                y.TotalTax = TotalTax;
                y.TotalFuelSur = TotalFuelSur;
                y.TotalFare = TotalFare;
                y.OriginalTF = OriginalTF;
                y.OriginalTT = srvCharge;//OriginalTT;
                y.NetFare = NetFare;


                y.STax = STax;
                y.TFee = TFee;
                y.TotDis = TotDis;
                y.TotCB = TotCB;
                y.TotTds = TotTds;
                y.TotMgtFee = TotMgtFee;
                #endregion
            });
            Final.AddRange(IbF);


            return Final;
        }
        private string GetTimeInHrsAndMin(int min)
        {
            string rslt;
            if (min < 60)
            {
                rslt = "00:" + min.ToString("00");
            }
            else
            {
                int hrs = min / 60;
                int rmin = min % 60;

                rslt = hrs.ToString("00") + ":" + rmin.ToString("00");
            }

            return rslt;

        }
        public string GetEntityXml(List<List<Results>> Finallist)
        {
            XmlAttributeOverrides overrides = new XmlAttributeOverrides();
            XmlAttributes attr = new XmlAttributes();
            attr.XmlRoot = new XmlRootAttribute("XML");
            overrides.Add(typeof(List<List<Results>>), attr);

            XmlDocument xmlDoc = new XmlDocument();
            XPathNavigator nav = xmlDoc.CreateNavigator();
            using (XmlWriter writer = nav.AppendChild())
            {
                XmlSerializer ser = new XmlSerializer(typeof(List<List<Results>>), overrides);
                List<List<Results>> parameters = Finallist;
                ser.Serialize(writer, parameters);
            }
            return xmlDoc.OuterXml.ToString();
        }
    }
    public class searchinput
    {
        public string EndUserIp { get; set; }
        public string TokenId { get; set; }
        public string AdultCount { get; set; }
        public string ChildCount { get; set; }
        public string InfantCount { get; set; }
        public string DirectFlight { get; set; }
        public string OneStopFlight { get; set; }
        public string JourneyType { get; set; }
        public string[] PreferredAirlines { get; set; }
        public List<SegmentsInput> Segments { get; set; }
        public List<string> Sources { get; set; }

    }
    public class SegmentsInput
    {
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string FlightCabinClass { get; set; }
        public string PreferredDepartureTime { get; set; }
        public string PreferredArrivalTime { get; set; }
    }

    public class Airline
    {
        public string AirlineCode { get; set; }
        public string AirlineName { get; set; }
        public string FlightNumber { get; set; }
        public string FareClass { get; set; }
        public string OperatingCarrier { get; set; }
    }

    public class Airport
    {
        public string AirportCode { get; set; }
        public string AirportName { get; set; }
        public string Terminal { get; set; }
        public string CityCode { get; set; }
        public string CityName { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
    }

    public class Origin
    {
        public Airport Airport { get; set; }
        public string DepTime { get; set; }
    }

    //public class Airport2
    //{
    //    public string AirportCode { get; set; }
    //    public string AirportName { get; set; }
    //    public string Terminal { get; set; }
    //    public string CityCode { get; set; }
    //    public string CityName { get; set; }
    //    public string CountryCode { get; set; }
    //    public string CountryName { get; set; }
    //}

    public class Segments
    {
        public int TripIndicator { get; set; }
        public int SegmentIndicator { get; set; }
        public Airline Airline { get; set; }
        public Origin Origin { get; set; }
        public Destination Destination { get; set; }
        public int Duration { get; set; }
        public int GroundTime { get; set; }
        public int Mile { get; set; }
        public bool StopOver { get; set; }
        public string StopPoint { get; set; }
        public string StopPointArrivalTime { get; set; }
        public string StopPointDepartureTime { get; set; }
        public string Craft { get; set; }
        public bool IsETicketEligible { get; set; }
        public string FlightStatus { get; set; }
        public string Status { get; set; }
    }
    public class Destination
    {
        public Airport Airport { get; set; }
        public string ArrTime { get; set; }
    }
    public class ChargeBU
    {
        public string key { get; set; }
        public double value { get; set; }
    }

    public class Fare
    {
        public string Currency { get; set; }
        public decimal BaseFare { get; set; }
        public decimal Tax { get; set; }
        public decimal YQTax { get; set; }
        public decimal AdditionalTxnFee { get; set; }
        public decimal AdditionalTxnFeeOfrd { get; set; }
        public decimal AdditionalTxnFeePub { get; set; }
        public double OtherCharges { get; set; }
        public List<ChargeBU> ChargeBU { get; set; }
        public decimal Discount { get; set; }
        public double PublishedFare { get; set; }
        public double CommissionEarned { get; set; }
        public double PLBEarned { get; set; }
        public double IncentiveEarned { get; set; }
        public double OfferedFare { get; set; }
        public double TdsOnCommission { get; set; }
        public double TdsOnPLB { get; set; }
        public double TdsOnIncentive { get; set; }
        public decimal ServiceFee { get; set; }
    }

    public class FareBreakdown
    {
        public string Currency { get; set; }
        public int PassengerType { get; set; }
        public int PassengerCount { get; set; }
        public decimal BaseFare { get; set; }
        public decimal Tax { get; set; }
        public decimal YQTax { get; set; }
        public decimal AdditionalTxnFee { get; set; }
        public decimal AdditionalTxnFeeOfrd { get; set; }
        public decimal AdditionalTxnFeePub { get; set; }
    }

    public class FareRule
    {
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string Airline { get; set; }
        public string FareBasisCode { get; set; }
        public string FareRuleDetail { get; set; }
        public string FareRestriction { get; set; }
    }

    public class Results
    {
        public string ResultIndex { get; set; }
        public int Source { get; set; }
        public bool IsLCC { get; set; }
        public bool IsRefundable { get; set; }
        public string AirlineRemark { get; set; }
        public Fare Fare { get; set; }
        public List<FareBreakdown> FareBreakdown { get; set; }
        public List<List<Segments>> Segments { get; set; }
        public object LastTicketDate { get; set; }
        public object TicketAdvisory { get; set; }
        public List<FareRule> FareRules { get; set; }
        public string AirlineCode { get; set; }
        public string ValidatingAirline { get; set; }
    }
  

    public class Response
    {
        public int ResponseStatus { get; set; }
        public Error Error { get; set; }
        public string TraceId { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public List<List<Results>> Results { get; set; }
    }

    public class FlightResult
    {
        public Response Response { get; set; }
    }
}
