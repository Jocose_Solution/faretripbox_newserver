﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using STD.Shared;
//using ExceptionLog;
using System.Security;
namespace STD.DAL
{
    public class CommonDAL
    {
        private SqlConnection con; private SqlCommand cmd;
        public CommonDAL()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        }
        /// <summary>
        /// Debit Amount from Agent or distributed Account  ( if you want Debit only Distributer or Agent then SET other ID="" and Debit Amount=0)
        /// Returns Agent and Distributed Available balance After Debit
        /// </summary>
        //public CardLimit Subtract_AmountFrom_AGENT_DISTR(CardLimit Balance)
        //{
        //    try
        //    {
        //        cmd = new SqlCommand("SP_DIST_SUBTRACTFROMLIMIT_AGENT_DISTR", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@AgentId", Balance.AgentID);
        //        cmd.Parameters.AddWithValue("@DistrId", Balance.DistrID);
        //        cmd.Parameters.AddWithValue("@AI_AMOUNT", Balance.NetFare);
        //        cmd.Parameters.AddWithValue("@DI_AMOUNT", Balance.Distr_Amt);
        //        con.Open();
        //        SqlDataReader reader = cmd.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            Balance.NetFare = Convert.ToDouble(reader["AVLAGENT"]);
        //            Balance.Distr_Amt = Convert.ToDouble(reader["AVLDISTR"]);
        //        }
        //        reader.Dispose();
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    finally
        //    {
        //        con.Close(); cmd.Dispose();
        //    }
        //    return Balance;
        //}

        /// <summary>
        /// Credit Amount from Agent and distributed Account  ( if you want Credit only Distributer or Agent  then SET other ID="" and Debit Amount=0)
        /// Returns Agent and Distributed Available balance After Credit 
        /// </summary>
        //public CardLimit ADD_AmountFrom_AGENT_DISTR(CardLimit Balance)
        //{
        //    try
        //    {
        //        cmd = new SqlCommand("SP_DIST_ADDTOLIMIT_AGENT_DISTR", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@AgentId", Balance.AgentID);
        //        cmd.Parameters.AddWithValue("@DistrId", Balance.DistrID);
        //        cmd.Parameters.AddWithValue("@AI_AMOUNT", Balance.NetFare);
        //        cmd.Parameters.AddWithValue("@DI_AMOUNT", Balance.Distr_Amt);
        //        con.Open();
        //        SqlDataReader reader = cmd.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            Balance.NetFare = Convert.ToDouble(reader["AVLAGENT"]);
        //            Balance.Distr_Amt = Convert.ToDouble(reader["AVLDISTR"]);
        //        }
        //        reader.Dispose();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        con.Close(); cmd.Dispose();
        //    }
        //    return Balance;
        //}

        /// <summary>
        /// Check Agent Or Distributer Balance ( if you want only Distributer or Agent balance then SET other ID="" and Debit Amount=0)
        /// Return True  if Credit Limit is Greater then or Equal to Debit Amount
        /// Retrun False if Credit Limit is Less then to Debit Amount
        /// </summary>
        //public CardLimit CHECK_AGENT_DISTR_CLIMIT(CardLimit CheckBalance)
        //{
        //    try
        //    {
        //        cmd = new SqlCommand("SP_DIST_CHECK_BALANCE_AGENT_DISTR", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@AGENTID", CheckBalance.AgentID);
        //        cmd.Parameters.AddWithValue("@DISTRID", CheckBalance.DistrID);
        //        cmd.Parameters.AddWithValue("@TA_AMOUNT", CheckBalance.NetFare);
        //        cmd.Parameters.AddWithValue("@DI_AMOUNT", CheckBalance.Distr_Amt);
        //        con.Open();
        //        SqlDataReader reader = cmd.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            CheckBalance.AgentLimit = Convert.ToBoolean(reader["AgtStatus"]); CheckBalance.DistrLimit = Convert.ToBoolean(reader["DistrStatus"]);
        //        }
        //        reader.Dispose();
        //    }
        //    catch (Exception ex)
        //    {
        //        CheckBalance.AgentLimit = false;
        //        CheckBalance.DistrLimit = false;
        //    }
        //    finally
        //    {
        //        con.Close(); cmd.Dispose();
        //    }
        //    return CheckBalance;
        //}

        /// <summary>
        /// Insert Data in Ladger details table
        /// </summary
        //public int InsertLedgerDetails(LedgerDetails objledger)
        //{
        //    int ledger = 0;
        //    try
        //    {
        //        cmd = new SqlCommand("SP_DIST_INSERT_LEDGERDETAILS", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@AGENTID", objledger.AGENTID);
        //        cmd.Parameters.AddWithValue("@DISTRID", objledger.DISTRID);
        //        cmd.Parameters.AddWithValue("@AGENCYNAME", objledger.AGENCYNAME);
        //        cmd.Parameters.AddWithValue("@INVOICENO", objledger.INVOICENO);
        //        cmd.Parameters.AddWithValue("@PNRNO", objledger.PNRNO);
        //        cmd.Parameters.AddWithValue("@TICKETNO", objledger.TICKETNO);
        //        cmd.Parameters.AddWithValue("@TICKETINGCARRIER", objledger.TICKETINGCARRIER);
        //        cmd.Parameters.AddWithValue("@YATRAACCOUNTID", objledger.YATRAACCOUNTID);
        //        cmd.Parameters.AddWithValue("@ACCOUNTID", objledger.ACCOUNTID);
        //        cmd.Parameters.AddWithValue("@EXECUTIVEID", objledger.EXECUTIVEID);
        //        cmd.Parameters.AddWithValue("@IPADDRESS", objledger.IPADDRESS);
        //        cmd.Parameters.AddWithValue("@DEBIT", objledger.DEBIT);
        //        cmd.Parameters.AddWithValue("@CREDIT", objledger.CREDIT);
        //        cmd.Parameters.AddWithValue("@AVALBALANCE", objledger.AVALBALANCE);
        //        cmd.Parameters.AddWithValue("@BOOKINGTYPE", objledger.BOOKINGTYPE);
        //        cmd.Parameters.AddWithValue("@REMARK", objledger.REMARK);
        //        cmd.Parameters.AddWithValue("@PAXID", objledger.PAXID);
        //        con.Open();
        //        ledger = cmd.ExecuteNonQuery();
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    finally
        //    {
        //        con.Close(); cmd.Dispose();
        //    }
        //    return ledger;
        //}

        /// <summary>
        /// Check Owner Type for Credit and Debit amount
        /// </summary
        public string GetOwnerType(string Ownerid)
        {
            string OwnerType = "";
            try
            {
                cmd = new SqlCommand("SP_DIST_CHECK_OWNERTYPE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OWNERID", Ownerid);
                con.Open();
                OwnerType = cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                con.Close(); cmd.Dispose();
            }
            return OwnerType;
        }
        /// <summary>
        /// Get Agency Name in case of Agent's orDistributer's Excutive Ticket Booking
        /// </summary
        public string GetAgencyName(string Ownerid)
        {
            string AgencyName = "";
            try
            {
                cmd = new SqlCommand("SP_DIST_AGENCYNAME", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OWNERID", Ownerid);
                con.Open();
                AgencyName = cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                con.Close(); cmd.Dispose();
            }
            return AgencyName;
        }

        /// <summary>
        /// SELECT SERVICE CHARGE FOR REISSUE AND REFUND
        /// </summary
        public DataTable GetServiceCharge(string AIRLINE, string TRIP, string SRVCHARGETYPE, string DISTRID, string GROUPTYPE, string USERTYPE)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlDataAdapter adap = new SqlDataAdapter("SP_DIST_SERVICECHARGE_SELECT", con);
                adap.SelectCommand.CommandType = CommandType.StoredProcedure;
                adap.SelectCommand.Parameters.AddWithValue("@AIRLINE", AIRLINE);
                adap.SelectCommand.Parameters.AddWithValue("@TRIP", TRIP);
                adap.SelectCommand.Parameters.AddWithValue("@DISTRID", DISTRID);
                adap.SelectCommand.Parameters.AddWithValue("@SRVCHARGETYPE", SRVCHARGETYPE);
                adap.SelectCommand.Parameters.AddWithValue("@GROUPTYPE", GROUPTYPE);
                adap.SelectCommand.Parameters.AddWithValue("@USERTYPE", USERTYPE);
                adap.Fill(dt);
            }
            catch (Exception ex)
            {
                //ExceptionLogger.FileHandling("ReissueAndRefund", "Err_023", ex, "ReissueAndRefund");
            }
            return dt;
        }

        // GET AGENCY DETAILS AOUTO COMPLETE
        //public List<LedgerDetails> GetAgencyList(string AgencyName, string loginid, string reqtype)
        //{
        //    List<LedgerDetails> AgencyList = new List<LedgerDetails>();
        //    try
        //    {
        //        cmd = new SqlCommand("SP_DIST_AGENCY_SEARCH", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@param", AgencyName);
        //        cmd.Parameters.AddWithValue("@LoginId", loginid);
        //        cmd.Parameters.AddWithValue("@Type", reqtype);
        //        con.Open();
        //        SqlDataReader reader = cmd.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            LedgerDetails Agt = new LedgerDetails();
        //            Agt.AGENTID = reader["USERID"].ToString();
        //            Agt.AGENCYNAME = reader["AGENCYNAME"].ToString();
        //            AgencyList.Add(Agt);
        //        }
        //    }
        //    catch
        //    {

        //    }
        //    finally
        //    {
        //        con.Close();
        //        cmd.Dispose();
        //    }
        //    return AgencyList;
        //}

        //// shows all distributor when admin login start-------------------

        //public List<LedgerDetails> GetDistList(string AgencyName, string loginid, string reqtype)
        //{
        //    List<LedgerDetails> AgencyList = new List<LedgerDetails>();
        //    try
        //    {
        //        cmd = new SqlCommand("SP_DIST_DIST_SEARCH", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@param", AgencyName);
        //        cmd.Parameters.AddWithValue("@LoginId", loginid);
        //        cmd.Parameters.AddWithValue("@Type", reqtype);
        //        con.Open();
        //        SqlDataReader reader = cmd.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            LedgerDetails Agt = new LedgerDetails();
        //            Agt.AGENTID = reader["USERID"].ToString();
        //            Agt.AGENCYNAME = reader["AGENCYNAME"].ToString();
        //            AgencyList.Add(Agt);
        //        }
        //    }
        //    catch
        //    {

        //    }
        //    finally
        //    {
        //        con.Close();
        //        cmd.Dispose();
        //    }
        //    return AgencyList;
        //}

        //// shows all distributor when admin login end-------------------
        //public List<string> GetDistrId(string AgentId)
        //{
        //    List<string> Dist = new List<string>();

        //    cmd = new SqlCommand("SP_DIST_GET_DISTRID", con);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@AgentId", AgentId);
        //    con.Open();
        //    IDataReader reader = cmd.ExecuteReader();
        //    while (reader.Read())
        //    {
        //        Dist.Add(reader["DistrId"].ToString());
        //    }

        //    reader.Dispose();
        //    con.Close();
        //    return Dist;

        //}
        //public List<GetAgencyDetailsByDistrId> GetAgencyDetailsByDistrId(GetAgencyDetailsByDistrId objGADBD)
        //{
        //    List<GetAgencyDetailsByDistrId> AgencyList = new List<GetAgencyDetailsByDistrId>();
        //    try
        //    {
        //        cmd = new SqlCommand("SP_DIST_GET_DISTRID", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@DistrId", objGADBD.DISTRID);
        //        cmd.Parameters.AddWithValue("@AgentId", objGADBD.AGENTID);
        //        con.Open();
        //        SqlDataReader reader = cmd.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            GetAgencyDetailsByDistrId GADBD = new GetAgencyDetailsByDistrId();
        //            GADBD.ID = Convert.ToInt32(reader["ID"].ToString());
        //            GADBD.AGENCYNAME = reader["AGENCYNAME"].ToString();
        //            GADBD.AGENTID = reader["USERID"].ToString();
        //            GADBD.DISTRID = reader["DISTRID"].ToString();
        //            GADBD.CREDITLIMIT = Convert.ToDouble(reader["CREDITLIMIT"].ToString());
        //            GADBD.REFFEREDBY = reader["REFFEREDBY"].ToString();
        //            GADBD.LASTTRANSACTIONDATE = reader["LASTTRANSACTIONDATE"].ToString();

        //            // ***** ADDED BY RAVI ****** //

        //            GADBD.FNAME = reader["FIRSTNAME"].ToString();
        //            GADBD.MNAME = reader["MIDDLENAME"].ToString();
        //            GADBD.LNAME = reader["LASTNAME"].ToString();
        //            GADBD.MOBILE = reader["MOBILE"].ToString();
        //            GADBD.EMAIL = reader["EMAIL"].ToString();
        //            GADBD.FAX = reader["FAX"].ToString();
        //            GADBD.PASSWORD = reader["PWD"].ToString();
        //            GADBD.AGENTSTATUS = (bool)reader["AGENTSTATUS"];
        //            GADBD.ONLINETKTSTATUS = (bool)reader["TICKTINGSTATUS"];
        //            GADBD.PAN = reader["PANNO"].ToString();
        //            GADBD.AGENTTYPE = reader["AGENTTYPE"].ToString();
        //            GADBD.REFBY = reader["REFFEREDBY"].ToString();
        //            GADBD.ADDRESS = reader["ADDRESS"].ToString();
        //            GADBD.CITY = reader["CITY"].ToString();
        //            GADBD.STATE = reader["STATE"].ToString();
        //            GADBD.COUNTRY = reader["COUNTRY"].ToString();
        //            GADBD.ZIPCODE = reader["ZIPCODE"].ToString();
        //            GADBD.REGISTRATIONDATE = Convert.ToDateTime(reader["CREATED_DATE"]);
        //            AgencyList.Add(GADBD);
        //        }
        //    }
        //    catch
        //    {

        //    }
        //    finally
        //    {
        //        con.Close();
        //        cmd.Dispose();
        //    }
        //    return AgencyList;
        //}
        public int CheckBalance(string UserId, string Amount)
        {
            con.Open();
            SqlCommand dCmd = new SqlCommand("CheckBalance", con);
            dCmd.CommandType = CommandType.StoredProcedure;
            dCmd.Parameters.AddWithValue("@UserId", UserId);
            dCmd.Parameters.AddWithValue("@Amt", Amount);
            int i = dCmd.ExecuteNonQuery();
            return i;
        }
        public double UpdateBalance(string UserId, string Amount)
        {
            con.Open();
            SqlCommand dCmd = new SqlCommand("SubtractFromLimit", con);
            dCmd.CommandType = CommandType.StoredProcedure;
            //  dCmd.Parameters.AddWithValue("@AgentId",c.AgentID);
            //   dCmd.Parameters.AddWithValue("@);
            dCmd.Parameters.AddWithValue("@userid", UserId);
            dCmd.Parameters.AddWithValue("@Amount", Amount);
            double dblhtlprop = Convert.ToDouble(dCmd.ExecuteScalar());
            return dblhtlprop;
        }


        //public DataSet GetAgencyDetailsDal(GetAgencyDetailsByDistrId objGADBD)
        //{
        //    DataSet AgencyList = new DataSet();
        //    try
        //    {
        //        SqlDataAdapter adap = new SqlDataAdapter("SP_DIST_GET_DISTRID", con);
        //        adap.SelectCommand.CommandType = CommandType.StoredProcedure;
        //        adap.SelectCommand.Parameters.AddWithValue("@DistrId", objGADBD.DISTRID);
        //        adap.Fill(AgencyList);


        //    }
        //    catch
        //    {

        //    }

        //    return AgencyList;

        //}
        //public DataSet GetSearchAgencyDal(GetAgencyDetailsByDistrId objGADBD)
        //{
        //    DataSet AgencyList = new DataSet();
        //    try
        //    {
        //        SqlDataAdapter adap = new SqlDataAdapter("SP_DIST_GET_DISTRID", con);
        //        adap.SelectCommand.CommandType = CommandType.StoredProcedure;
        //        adap.SelectCommand.Parameters.AddWithValue("@AgentId", objGADBD.AGENTID);
        //        adap.Fill(AgencyList);


        //    }
        //    catch
        //    {

        //    }

        //    return AgencyList;

        //}

        //public double SubtractFromLimit(CardLimit objcom)
        //{
        //    con.Open();
        //    cmd = new SqlCommand("SP_DIST_SUBTRACTFROMLIMIT", con);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@AgentId", objcom.AgentID);
        //    cmd.Parameters.AddWithValue("@NetFare", objcom.NetFare);
        //    double NetFare = Convert.ToDouble(cmd.ExecuteScalar());
        //    con.Close();
        //    return NetFare;

        //    //paramHashtable.Clear();
        //    //paramHashtable.Add("@UserId", objcom.AgentID);
        //    //paramHashtable.Add("@NetFare", objcom.NetFare);
        //    //double NetFare=Convert.ToDouble(objDataAcess.ExecuteData<object>(paramHashtable, true, "SP_DIST_SUBTRACTFROMLIMIT", 2));
        //    //return NetFare;


        //}
        //public double AddToLimit(CardLimit objATL)
        //{
        //    con.Open();
        //    cmd = new SqlCommand("SP_DIST_ADDTOLIMIT", con);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@AgentId", objATL.AgentID);
        //    cmd.Parameters.AddWithValue("@Amount", objATL.NetFare);
        //    double NetFare = Convert.ToDouble(cmd.ExecuteScalar());
        //    con.Close();
        //    return NetFare;

        //    //paramHashtable.Clear();
        //    //paramHashtable.Add("@UserId", objcom.AgentID);
        //    //paramHashtable.Add("@NetFare", objcom.NetFare);
        //    //double NetFare=Convert.ToDouble(objDataAcess.ExecuteData<object>(paramHashtable, true, "SP_DIST_SUBTRACTFROMLIMIT", 2));
        //    //return NetFare;


        //}

        //public int UpdateFltHeaderStatusDal(LedgerDetails ObjShared)
        //{
        //    con.Open();
        //    cmd = new SqlCommand("SP_DIST_UPDATE_FLT_HEADER_PAXDETAILS", con);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@orderid", ObjShared.ORDERID);
        //    cmd.Parameters.AddWithValue("@PNR", ObjShared.PNRNO);
        //    cmd.Parameters.AddWithValue("@PaxID", ObjShared.PAXID);
        //    cmd.Parameters.AddWithValue("@Status", ObjShared.STATUS);
        //    int i = cmd.ExecuteNonQuery();
        //    return i;
        //}
        //*Add for menucontrol
        public DataSet GetMenuControlDal(string strMenuID, string typeID)
        {
            DataSet menuList = new DataSet();
            try
            {
                SqlDataAdapter adap = new SqlDataAdapter("sp_menuControl", con);
                adap.SelectCommand.CommandType = CommandType.StoredProcedure;
                adap.SelectCommand.Parameters.AddWithValue("@pageRequestID", strMenuID);
                adap.SelectCommand.Parameters.AddWithValue("@typeid", typeID);
                adap.Fill(menuList);

            }
            catch
            {

            }

            return menuList;

        }
        public DataSet GetPageControlDal(string strPageID)
        {
            DataSet menuList = new DataSet();
            try
            {
                SqlDataAdapter adap = new SqlDataAdapter("sp_PageControl", con);
                adap.SelectCommand.CommandType = CommandType.StoredProcedure;
                adap.SelectCommand.Parameters.AddWithValue("@pageID", strPageID);
                adap.Fill(menuList);

            }
            catch
            {

            }

            return menuList;

        }
        public DataTable GetAgencyAddress(string OWNERID, string DISTRID, string USERTYPE, string TYPE)
        {
            DataTable dtadd = new DataTable();
            try
            {
                SqlDataAdapter adap = new SqlDataAdapter("SP_DIST_GET_AGENCYADDRESS", con);
                adap.SelectCommand.CommandType = CommandType.StoredProcedure;
                adap.SelectCommand.Parameters.AddWithValue("@OWNERID", OWNERID);
                adap.SelectCommand.Parameters.AddWithValue("@DISTRID", DISTRID);
                adap.SelectCommand.Parameters.AddWithValue("@USERTYPE", USERTYPE);
                adap.SelectCommand.Parameters.AddWithValue("@TYPE", TYPE);
                adap.Fill(dtadd);

            }
            catch
            {

            }

            return dtadd;

        }
        public DataTable GetAgencyOwnAddress(string OWNERID, string DISTRID, string AGENTID, string USERTYPE, string TYPE)
        {
            DataTable dtadd = new DataTable();
            try
            {
                SqlDataAdapter adap = new SqlDataAdapter("SP_DIST_GET_AGENCYOWNADDRESS", con);
                adap.SelectCommand.CommandType = CommandType.StoredProcedure;
                adap.SelectCommand.Parameters.AddWithValue("@OWNERID", OWNERID);
                adap.SelectCommand.Parameters.AddWithValue("@DISTRID", DISTRID);
                adap.SelectCommand.Parameters.AddWithValue("@AGENTID", AGENTID);
                adap.SelectCommand.Parameters.AddWithValue("@USERTYPE", USERTYPE);
                adap.SelectCommand.Parameters.AddWithValue("@TYPE", TYPE);
                adap.Fill(dtadd);

            }
            catch
            {

            }

            return dtadd;

        }
        ////for hotel
        //public Double CheckLedgerStatus(string orderid, string AgentID, string Amt, string Status)
        //{
        //    con.Open();
        //    SqlCommand dCmd = new SqlCommand("CheckLedgerBookingType", con);
        //    dCmd.CommandType = CommandType.StoredProcedure;
        //    dCmd.Parameters.AddWithValue("@orderid", orderid);
        //    dCmd.Parameters.AddWithValue("@AgentID", AgentID);
        //    dCmd.Parameters.AddWithValue("@Amt", Amt);
        //    dCmd.Parameters.AddWithValue("@Status", Status);
        //    double dblhtlprop = Convert.ToDouble(dCmd.ExecuteScalar());
        //    return dblhtlprop;
        //}

        //public int UpdateRefundCancellationDal(LedgerDetails ObjShared)
        //{
        //    con.Open();
        //    cmd = new SqlCommand("SP_DIST_UPDATE_REFUND_REISSUE_CANCELLATION", con);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@counter",ObjShared.COUNTER);
        //    cmd.Parameters.AddWithValue("@TaCancellationCharge",ObjShared.TA_CANCELLATIONCHARGE);
        //    cmd.Parameters.AddWithValue("@TaServiceCharge",ObjShared.TA_SERVICECHARGE);
        //    cmd.Parameters.AddWithValue("@TaRefundFair",ObjShared.TA_REFUNDFARE);
        //    cmd.Parameters.AddWithValue("@DiCancellationCharge",ObjShared.DI_CANCELLATIONCHARGE);
        //    cmd.Parameters.AddWithValue("@DiServiceCharge",ObjShared.DI_SERVICECHARGE);
        //    cmd.Parameters.AddWithValue("@DiRefundFair",ObjShared.DI_REFUNDFARE);
        //    cmd.Parameters.AddWithValue("@UpdateRemark",ObjShared.REMARK);
        //    cmd.Parameters.AddWithValue("@Status",ObjShared.STATUS);
        //    int i = cmd.ExecuteNonQuery();
        //    con.Close();
        //    return i;
        //}

        //public int insertLedgerDetails(HtlProperty htlprop)
        //{
        //    con.Open();
        //    SqlCommand dCmd = new SqlCommand("usp_insert_LedgerDetails", con);
        //    dCmd.CommandType = CommandType.StoredProcedure;

        //    dCmd.Parameters.AddWithValue("@AgentID", htlprop.AgentID);
        //    dCmd.Parameters.AddWithValue("@AgencyName", htlprop.AgencyName);
        //    dCmd.Parameters.AddWithValue("@InvoiceNo", htlprop.InvoiceNo);
        //    dCmd.Parameters.AddWithValue("@PnrNo", htlprop.PnrNo);
        //    dCmd.Parameters.AddWithValue("@TicketNo", htlprop.TicketNo);
        //    dCmd.Parameters.AddWithValue("@YatraAccountID", htlprop.YatraAccountID);
        //    dCmd.Parameters.AddWithValue("@AccountID", htlprop.AccountID);
        //    dCmd.Parameters.AddWithValue("@ExecutiveID", htlprop.ExecutiveID);
        //    dCmd.Parameters.AddWithValue("@IPAddress", htlprop.IPAddress);
        //    dCmd.Parameters.AddWithValue("@Debit", htlprop.Debit);
        //    dCmd.Parameters.AddWithValue("@Credit", htlprop.credit);
        //    dCmd.Parameters.AddWithValue("@Aval_Balance", htlprop.avalbal);
        //    dCmd.Parameters.AddWithValue("@BookingType", htlprop.BookingType);
        //    dCmd.Parameters.AddWithValue("@Remark", htlprop.Remarks);
        //    dCmd.Parameters.AddWithValue("@PaxId", htlprop.PaxId);
        //    return dCmd.ExecuteNonQuery();
        //}

        public string strScrapRespone(string strQueryData)
        {
            con.Open();
            SqlCommand dCmd = new SqlCommand("couponRequest", con);
            dCmd.CommandType = CommandType.StoredProcedure;
            dCmd.Parameters.AddWithValue("@requestString", strQueryData);

            string strResponse = Convert.ToString(dCmd.ExecuteScalar());
            con.Close();

            return strResponse;

        }
        public void ScrapResponeChache(string strQueryData, string strScrapResponse)
        {
            con.Open();
            SqlCommand dCmd = new SqlCommand("couponDataChache", con);
            dCmd.CommandType = CommandType.StoredProcedure;

            dCmd.Parameters.AddWithValue("@requestString", strQueryData);
            dCmd.Parameters.AddWithValue("@responsedata", strScrapResponse);


            dCmd.ExecuteNonQuery();
            con.Close();

        }





    }
}
