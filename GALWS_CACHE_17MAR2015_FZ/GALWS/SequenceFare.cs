﻿using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;
using STD.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;


namespace STD.BAL
{
    public class SequeneFare
    {
        string ConnStr = "";
        List<FlightCityList> CityList;
        List<AirlineList> Airlist;
        public SequeneFare()
        { }
        public SequeneFare(string ConnectionString)
        {
            //m
            ConnStr = ConnectionString;
        }
        private string GetCPNValidatingCarrier(string sno)
        {
            string VC = "";
            try
            {
                if (sno.ToUpper().Contains("INDIGOCORP"))
                {
                    VC = "6ECORP";
                }
                else if (sno.ToUpper().Contains("INDIGOTBF"))
                {
                    VC = "6ETBF";
                }
                else if (sno.ToUpper().Contains("GOAIRCORP"))
                {
                    VC = "G8CORP";
                }
                else if (sno.ToUpper().Contains("AIRASIA"))
                {
                    VC = "AKCPN";
                }
                else if (sno.ToUpper().Contains("INDIGOSPECIAL"))
                {
                    VC = "6ECPN";
                }
                else if (sno.ToUpper().Contains("SPICEJETSPECIAL"))
                {
                    VC = "SGCPN";
                }
                else if (sno.ToUpper().Contains("GOAIRSPECIAL"))
                {
                    VC = "G8CPN";
                }
                else if (sno.ToUpper().Contains("AIINDIAEXPRESS"))
                {
                    VC = "IXCPN";
                }
            }
            catch (Exception ex) { VC = ""; }
            return VC;
        }

        private ArrayList AvilabilityListSpl(DataSet FlightDs, List<FltSrvChargeList> SrvChargeList, DataSet MrkupDS, List<MISCCharges> MiscList, string GType, string TDS, bool RTF, FlightSearch searchInputs, string CrdType)
        {

            ICacheManager objCacheManager = CacheFactory.GetCacheManager("MyCacheManager");
            if (objCacheManager.Contains("CityList"))
            {
                CityList = (List<FlightCityList>)objCacheManager.GetData("CityList");

            }
            else
            {
                CityList = Data.GetCityList("I", ConnStr);
                FileDependency _objFileDependency = new FileDependency(HttpContext.Current.Server.MapPath("~/UpdateCache.XML"));
                objCacheManager.Add("CityList", CityList, CacheItemPriority.Normal, null, _objFileDependency);
            }
            if (objCacheManager.Contains("Airlist"))
            {
                Airlist = (List<AirlineList>)objCacheManager.GetData("Airlist");
            }
            else
            {
                Airlist = Data.GetAirlineList(ConnStr);
                FileDependency _objFileDependency = new FileDependency(HttpContext.Current.Server.MapPath("~/UpdateCache.XML"));
                objCacheManager.Add("Airlist", Airlist, CacheItemPriority.Normal, null, _objFileDependency);
            }

            int flight = 1;
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            ArrayList CacheList = new ArrayList(); float srvCharge = 0;
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            float srvChargeAdt = 0;
            float srvChargeChd = 0;
            //FlightCommonBAL obj = new FlightCommonBAL();

            List<BaggageList> BagInfoList = new List<BaggageList>();
            List<FlightSearchResults> FlightList = new List<FlightSearchResults>();


            BagInfoList = null; //objFltComm.GetBaggage(searchInputs.Trip.ToString(), VC, Bag);

            string strFlightNo = "";
            string strValidatingCarrier = "";
            string FlttmDur = "";//added by abhilash
            int LNO = 1;

            try
            {


                foreach (DataRow dr in FlightDs.Tables[0].Rows)
                {
                    string arrCity = "", depCity = "";
                    FlightSearchResults objFS = new FlightSearchResults();
                    FlightSearchResults objFSR = new FlightSearchResults();

                    depCity = dr["DepAirportCode"].ToString().Replace(")", "").Replace("(", "");
                    arrCity = dr["ArrAirportCode"].ToString().Replace(")", "").Replace("(", "");

                    try
                    {
                        #region Get TotalViaSector for Commission PPPSector and PPPSegment
                        int TotalViaSector = 1;
                        #endregion
                        int legs = 1;
                        for (int i = 0; i < legs; i++)
                        {

                            decimal AdtTF = 0, ChdTF = 0, InfTF = 0;//For Total Fare Per Pax Type
                            #region Flight Details
                            objFS.OrgDestFrom = depCity;
                            objFS.OrgDestTo = arrCity;
                            objFS.Flight = flight.ToString();
                            objFS.DepartureLocation = depCity;
                            objFS.DepAirportCode = depCity;
                            objFS.DepartureTerminal = dr["DepartureTerminal"].ToString();
                            objFS.DepartureCityName = getCityName(objFS.DepartureLocation, CityList);
                            objFS.ArrivalLocation = arrCity;
                            objFS.ArrAirportCode = arrCity;
                            objFS.ArrivalTerminal = dr["ArrivalTerminal"].ToString();
                            objFS.ArrivalCityName = getCityName(objFS.ArrivalLocation, CityList);
                            try
                            {
                                objFS.DepartureAirportName = ((from ct in CityList where ct.AirportCode == objFS.DepartureLocation select ct).ToList())[0].AirportName;
                                objFS.ArrivalAirportName = ((from ct in CityList where ct.AirportCode == objFS.ArrivalLocation select ct).ToList())[0].AirportName;
                            }
                            catch (Exception ex)
                            {
                                objFS.DepartureAirportName = depCity;
                                objFS.ArrivalAirportName = arrCity;
                            }
                            string[] Dep = Utility.Split(dr["Departure_Date"].ToString(), "/");
                            objFS.DepartureDate = Dep[0] + Dep[1] + Utility.Right(Dep[2], 2);
                            objFS.Departure_Date = Dep[0] + " " + Utility.datecon(Dep[1]);
                            objFS.DepartureTime = dr["DepartureTime"].ToString();//.Replace(":", "");

                            string[] Arr = Utility.Split(dr["Arrival_Date"].ToString(), "/");
                            objFS.ArrivalDate = Arr[0] + Arr[1] + Utility.Right(Arr[2], 2);
                            objFS.Arrival_Date = Arr[0] + " " + Utility.datecon(Arr[1]);
                            objFS.ArrivalTime = dr["ArrivalTime"].ToString();//.Replace(":", "");
                            try
                            {

                                DateTime DDTime = DateTime.Parse(objFS.DepartureTime);
                                DateTime ADTime = DateTime.Parse(objFS.ArrivalTime);
                                TimeSpan value = ADTime - DDTime;
                                string label = string.Format("{0:hh\\:mm\\:ss}", value);
                                objFS.TotDur = Utility.Left(label, 5);
                                // objFS.TotDur = objFS.TotDur = Utility.Left(Convert.ToDateTime(t.Leg[i]["STA"]).Subtract(Convert.ToDateTime(t.Leg[i]["STD"])).ToString(), 5);


                            }
                            catch { objFS.TotDur = ""; }
                            objFS.Adult = searchInputs.Adult;
                            objFS.Child = searchInputs.Child;
                            objFS.Infant = searchInputs.Infant;
                            objFS.TotPax = searchInputs.Adult + searchInputs.Child;

                            string VC = dr["MarketingCarrier"].ToString().Replace(")", "").Replace("(", "");
                            objFS.MarketingCarrier = VC;
                            objFS.OperatingCarrier = VC;
                            objFS.FlightIdentification = dr["FlightNo"].ToString();

                            objFS.ValiDatingCarrier = VC;

                            objFS.AirLineName = dr["AirLineName"].ToString();
                            objFS.AvailableSeats = dr["Avl_Seat"].ToString();
                            objFS.AvailableSeats1 = dr["Avl_Seat"].ToString();

                            objFS.BagInfo = dr["BagInfo"].ToString();


                            #region Baggage
                            objFS.IsBagFare = false;
                            //if (objFareTypeSettings != null && objFareTypeSettings.Count > 0 && objFareTypeSettings[0].SSRCode != null)
                            objFS.SSRCode = "";
                            #endregion
                            #region SME FARE        
                            objFS.IsSMEFare = false;
                            objFS.IsLTC = false;
                            #endregion
                            //objFS.TotDur = "";
                            string[] Dep1 = Utility.Split(dr["Departure_Date"].ToString(), "/");
                            string[] Arr1 = Utility.Split(dr["Arrival_Date"].ToString(), "/");
                            objFS.depdatelcc = Dep1[2] + '-' + Dep1[1] + '-' + Dep1[0] + "T" + dr["DepartureTime"].ToString() + ":00";
                            objFS.arrdatelcc = Arr1[2] + '-' + Arr1[1] + '-' + Arr1[0] + "T" + dr["ArrivalTime"].ToString() + ":00";

                            int schd = 0;
                            if (searchInputs.Trip == Trip.I)
                            {
                                if (schd == 0)
                                {
                                    if (searchInputs.TripType == STD.Shared.TripType.RoundTrip)
                                        objFS.Sector = depCity + ":" + arrCity + ":" + depCity;
                                    else
                                        objFS.Sector = depCity + ":" + arrCity;
                                }
                                else
                                {
                                    if (searchInputs.TripType == STD.Shared.TripType.RoundTrip)
                                        objFS.Sector = arrCity + ":" + depCity + ":" + arrCity;
                                    else
                                        objFS.Sector = depCity + ":" + arrCity;
                                }
                            }
                            else
                            {
                                if (schd == 0)
                                {

                                    if (searchInputs.RTF == true)
                                    { objFS.Sector = depCity + ":" + arrCity + ":" + depCity; }
                                    else
                                    { objFS.Sector = depCity + ":" + arrCity; }
                                }
                                else
                                {
                                    if (searchInputs.RTF == true)
                                        objFS.Sector = arrCity + ":" + depCity + ":" + arrCity;
                                    else
                                        objFS.Sector = depCity + ":" + arrCity;
                                }
                            }

                            objFS.EQ = dr["FlightNo"].ToString();
                            objFS.LineNumber = LNO;
                            // objFS.Ln = LNO;
                            objFS.Leg = (i + 1);
                            objFS.fareBasis = dr["fareBasis"].ToString();
                            objFS.RBD = dr["RBD"].ToString();
                            objFS.AdtRbd = dr["RBD"].ToString();
                            objFS.ChdRbd = dr["RBD"].ToString();
                            objFS.InfRbd = dr["RBD"].ToString();
                            objFS.ChdCabin = "E";
                            objFS.InfCabin = "E";
                            objFS.FareRule = dr["FareRule"].ToString(); //Fare Sell Key
                            objFS.Searchvalue = "";//t.FSK; //Fare Sell Key
                            objFS.sno = ""; //t.JSK; //Journey Sell Key
                            objFS.SubKey = "";
                            objFS.Track_id = "";
                            objFS.ProviderUserID = "";

                            objFS.Leg = (i + 1);
                            objFS.fareBasis = dr["fareBasis"].ToString();
                            objFS.MainKey = "";

                            objFS.AdtAvlStatus = "";
                            objFS.ChdBreakPoint = "";
                            objFS.ChdAvlStatus = "";
                            objFS.InfBreakPoint = "";
                            objFS.InfAvlStatus = "";
                            objFS.TotalTripDur = objFS.TotDur;
                            objFS.TripCnt = objFS.TotDur;
                            objFS.FBPaxType = "";
                            objFS.InfFar = "";
                            objFS.ChdFar = "";
                            objFS.getVia = "";
                            objFS.ProductDetailQualifier = "";


                            #endregion



                            #region Flight Details Return
                            objFSR.OrgDestFrom = arrCity;
                            objFSR.OrgDestTo = depCity;
                            objFSR.Flight = "2";
                            objFSR.DepartureLocation = arrCity;
                            objFSR.DepAirportCode = arrCity;
                            objFSR.DepartureTerminal = dr["Rt_DepartureTerminal"].ToString();
                            objFSR.DepartureCityName = getCityName(objFSR.DepartureLocation, CityList);
                            objFSR.ArrivalLocation = depCity;
                            objFSR.ArrAirportCode = depCity;
                            objFSR.ArrivalTerminal = dr["Rt_ArrivalTerminal"].ToString();
                            objFSR.ArrivalCityName = getCityName(objFSR.ArrivalLocation, CityList);
                            try
                            {
                                objFSR.DepartureAirportName = ((from ct in CityList where ct.AirportCode == objFSR.DepartureLocation select ct).ToList())[0].AirportName;
                                objFSR.ArrivalAirportName = ((from ct in CityList where ct.AirportCode == objFSR.ArrivalLocation select ct).ToList())[0].AirportName;
                            }
                            catch (Exception ex)
                            {
                                objFSR.DepartureAirportName = arrCity;
                                objFSR.ArrivalAirportName = depCity;
                            }
                            string[] Dep2 = Utility.Split(dr["Rt_Departure_Date"].ToString(), "/");
                            objFSR.DepartureDate = Dep2[0] + Dep2[1] + Utility.Right(Dep2[2], 2);
                            objFSR.Departure_Date = Dep2[0] + " " + Utility.datecon(Dep2[1]);
                            objFSR.DepartureTime = dr["Rt_DepartureTime"].ToString();//.Replace(":", "");

                            string[] Arr2 = Utility.Split(dr["Rt_Arrival_Date"].ToString(), "/");
                            objFSR.ArrivalDate = Arr2[0] + Arr2[1] + Utility.Right(Arr2[2], 2);
                            objFSR.Arrival_Date = Arr2[0] + " " + Utility.datecon(Arr2[1]);
                            objFSR.ArrivalTime = dr["Rt_ArrivalTime"].ToString();//.Replace(":", "");
                            try
                            {

                                DateTime DDTime = DateTime.Parse(objFSR.DepartureTime);
                                DateTime ADTime = DateTime.Parse(objFSR.ArrivalTime);
                                TimeSpan value = ADTime - DDTime;
                                string label = string.Format("{0:hh\\:mm\\:ss}", value);
                                objFSR.TotDur = Utility.Left(label, 5);
                                // objFS.TotDur = objFS.TotDur = Utility.Left(Convert.ToDateTime(t.Leg[i]["STA"]).Subtract(Convert.ToDateTime(t.Leg[i]["STD"])).ToString(), 5);


                            }
                            catch { objFS.TotDur = ""; }
                            objFSR.Adult = searchInputs.Adult;
                            objFSR.Child = searchInputs.Child;
                            objFSR.Infant = searchInputs.Infant;
                            objFSR.TotPax = searchInputs.Adult + searchInputs.Child;

                            string VC1 = dr["Rt_AirLineName"].ToString().Split(',')[1].Replace(")", "").Replace("(", "");
                            objFSR.MarketingCarrier = VC1;
                            objFSR.OperatingCarrier = VC1;
                            objFSR.FlightIdentification = dr["Rt_FlightNo"].ToString();

                            objFSR.ValiDatingCarrier = VC1;

                            objFSR.AirLineName = dr["Rt_AirLineName"].ToString().Split(',')[0];
                            objFSR.AvailableSeats = dr["Avl_Seat"].ToString();
                            objFSR.AvailableSeats1 = dr["Avl_Seat"].ToString();

                            objFSR.BagInfo = dr["BagInfo"].ToString();


                            #region Baggage
                            objFSR.IsBagFare = false;
                            //if (objFareTypeSettings != null && objFareTypeSettings.Count > 0 && objFareTypeSettings[0].SSRCode != null)
                            objFSR.SSRCode = "";
                            #endregion
                            #region SME FARE        
                            objFSR.IsSMEFare = false;
                            objFSR.IsLTC = false;
                            #endregion
                            //objFS.TotDur = "";
                            string[] Dep11 = Utility.Split(dr["Rt_Departure_Date"].ToString(), "/");
                            string[] Arr11 = Utility.Split(dr["Rt_Arrival_Date"].ToString(), "/");
                            objFSR.depdatelcc = Dep11[2] + '-' + Dep11[1] + '-' + Dep11[0] + "T" + dr["Rt_DepartureTime"].ToString() + ":00";
                            objFSR.arrdatelcc = Arr11[2] + '-' + Arr11[1] + '-' + Arr11[0] + "T" + dr["Rt_ArrivalTime"].ToString() + ":00";

                            int schd1 = 1;
                            if (searchInputs.Trip == Trip.I)
                            {
                                if (schd1 == 0)
                                {
                                    if (searchInputs.TripType == STD.Shared.TripType.RoundTrip)
                                        objFSR.Sector = depCity + ":" + arrCity + ":" + depCity;
                                    else
                                        objFSR.Sector = depCity + ":" + arrCity;
                                }
                                else
                                {
                                    if (searchInputs.TripType == STD.Shared.TripType.RoundTrip)
                                        objFSR.Sector = arrCity + ":" + depCity + ":" + arrCity;
                                    else
                                        objFSR.Sector = depCity + ":" + arrCity;
                                }
                            }
                            else
                            {
                                if (schd1 == 0)
                                {

                                    if (searchInputs.RTF == true)
                                    { objFSR.Sector = depCity + ":" + arrCity + ":" + depCity; }
                                    else
                                    { objFSR.Sector = depCity + ":" + arrCity; }
                                }
                                else
                                {
                                    if (searchInputs.RTF == true)
                                        objFSR.Sector = arrCity + ":" + depCity + ":" + arrCity;
                                    else
                                        objFSR.Sector = depCity + ":" + arrCity;
                                }
                            }

                            objFSR.EQ = dr["Rt_FlightNo"].ToString();
                            objFSR.LineNumber = LNO;
                            // objFS.Ln = LNO;
                            objFSR.Leg = (i + 1);
                            objFSR.fareBasis = dr["Rt_fareBasis"].ToString();
                            objFSR.RBD = dr["Rt_RBD"].ToString();
                            objFSR.AdtRbd = dr["Rt_RBD"].ToString();
                            objFSR.ChdRbd = dr["Rt_RBD"].ToString();
                            objFSR.InfRbd = dr["Rt_RBD"].ToString();
                            objFSR.ChdCabin = "E";
                            objFSR.InfCabin = "E";
                            objFSR.FareRule = dr["FareRule"].ToString(); //Fare Sell Key
                            objFSR.Searchvalue = "";//t.FSK; //Fare Sell Key
                            objFSR.sno = ""; //t.JSK; //Journey Sell Key
                            objFSR.SubKey = "";
                            objFSR.Track_id = "";
                            objFSR.ProviderUserID = "";

                            objFSR.Leg = (i + 1);
                            objFSR.fareBasis = dr["Rt_fareBasis"].ToString();
                            objFSR.MainKey = "";

                            objFSR.AdtAvlStatus = "";
                            objFSR.ChdBreakPoint = "";
                            objFSR.ChdAvlStatus = "";
                            objFSR.InfBreakPoint = "";
                            objFSR.InfAvlStatus = "";
                            objFSR.TotalTripDur = objFS.TotDur;
                            objFSR.TripCnt = objFS.TotDur;
                            objFSR.FBPaxType = "";
                            objFSR.InfFar = "";
                            objFSR.ChdFar = "";
                            objFSR.getVia = "";
                            objFSR.ProductDetailQualifier = "";


                            #endregion

                            //11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
                            ////SMS charge calc
                            //float srvCharge = 0;
                            //float srvChargeAdt = 0;
                            //float srvChargeChd = 0;

                            //float srvCharge = 0;
                            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID);
                            ////SMS charge calc end


                            #region Adult
                            //objFS.AdtFar = IdType;
                            objFS.AdtFar = "FDD";// CrdType;
                            objFS.AdtCabin = "E";// "E";// t.COS;
                            objFS.AdtFarebasis = dr["fareBasis"].ToString();
                            objFS.AdtRbd = dr["RBD"].ToString();
                            #region ADTFR

                            objFS.AdtFareType = "Non Refundable";


                            //FareSellKey , JSK
                            #endregion

                            float PROMODISCHD = 0;
                            float PROMODIS = 0;
                            //Adt Fare Details 
                            if (searchInputs.Adult > 0)
                            {
                                int m = 0;
                                var AdtCharges = 0;



                                // navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge adtd = new navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge();
                                // adtd = svc;



                                objFS.AdtFSur = float.Parse(Math.Round(decimal.Parse(dr["YQ"].ToString()), 0).ToString());
                                objFS.AdtWO = float.Parse(Math.Round(decimal.Parse(dr["WO"].ToString()), 0).ToString());
                                objFS.AdtYR = float.Parse(Math.Round(decimal.Parse(dr["YR"].ToString()), 0).ToString());
                                objFS.AdtBfare = float.Parse(Math.Round(decimal.Parse(dr["Basicfare"].ToString()), 0).ToString());
                                objFS.AdtOT = float.Parse(Math.Round(decimal.Parse(dr["OT"].ToString()), 0).ToString());

                                objFS.AdtFare = float.Parse(Math.Round(decimal.Parse(dr["GROSS_Total"].ToString()), 0).ToString());
                                AdtTF = 0;


                                PROMODIS = 0;


                                objFS.AdtBfare = objFS.AdtBfare - PROMODIS;
                                objFS.AdtFare = objFS.AdtFare - PROMODIS;
                                AdtTF = AdtTF - decimal.Parse(PROMODIS.ToString());

                                objFS.AdtTax = objFS.AdtFare - objFS.AdtBfare;

                                // float srvChargeAdt = 0;
                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    srvChargeAdt = 0;//objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType, Convert.ToString(objFS.AdtBfare), Convert.ToString(objFS.AdtFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                }
                                catch { srvChargeAdt = 0; }
                                #endregion


                                //SMS charge add 
                                objFS.AdtOT = objFS.AdtOT + srvChargeAdt;//srvCharge;
                                objFS.AdtTax = objFS.AdtTax + srvChargeAdt;//srvCharge;
                                objFS.AdtFare = objFS.AdtFare + srvChargeAdt; //srvCharge;
                                                                              //SMS charge add end

                                if (dr["Markup_Type"].ToString() == "Flat")
                                {
                                    objFS.ADTAdminMrk = float.Parse(dr["Admin_Markup"].ToString() == "" ? "0" : dr["Admin_Markup"].ToString());
                                }
                                else
                                {
                                    objFS.ADTAdminMrk = (objFS.AdtBfare * Convert.ToInt32(dr["Admin_Markup"].ToString() == "" ? "0" : dr["Admin_Markup"].ToString())) / 100;
                                }

                                objFS.ADTAgentMrk = float.Parse(dr["Markup"].ToString() == "" ? "0" : dr["Markup"].ToString());


                                //CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                {
                                    //objFS.ADTAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.AdtFare, searchInputs.Trip.ToString());
                                    // objFS.ADTAdminMrk = float.Parse(dr["Admin_Markup"].ToString());
                                    objFS.AdtBfare = objFS.AdtBfare + objFS.ADTAdminMrk;
                                    objFS.AdtFare = objFS.AdtFare + objFS.ADTAdminMrk;
                                }
                                CommDt.Clear();
                                STTFTDS.Clear();
                                if ((searchInputs.Trip.ToString() == JourneyType.D.ToString()) && (searchInputs.RTF != true)) //&& (searchInputs.RTF != true)
                                {
                                    //if ((IdType != "SGTBF") && (IdType != "SGSTR"))
                                    //{
                                    try
                                    {
                                        //CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), 1, objFS.AdtRbd, objFS.AdtCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, CrdType, Convert.ToString(TotalViaSector));
                                        //objFS.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                        //objFS.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                        //STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objFS.ValiDatingCarrier, objFS.AdtDiscount1, objFS.AdtBfare, objFS.AdtFSur, searchInputs.TDS);
                                        //objFS.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                        //objFS.AdtDiscount = objFS.AdtDiscount1 - objFS.AdtSrvTax1;
                                        //objFS.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                        //objFS.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                        //objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());
                                        objFS.AdtDiscount1 = 0;
                                        objFS.AdtDiscount = 0;
                                        objFS.AdtCB = 0;
                                        objFS.AdtSrvTax = 0;
                                        objFS.AdtTF = 0;
                                        objFS.AdtTds = 0;
                                        objFS.IATAComm = 0;
                                    }
                                    catch (Exception ex)
                                    {
                                        objFS.AdtDiscount1 = 0;
                                        objFS.AdtDiscount = 0;
                                        objFS.AdtCB = 0;
                                        objFS.AdtSrvTax = 0;
                                        objFS.AdtTF = 0;
                                        objFS.AdtTds = 0;
                                        objFS.IATAComm = 0;
                                    }
                                    //}
                                }
                                else
                                {
                                    objFS.AdtDiscount = 0;
                                    objFS.AdtCB = 0;
                                    objFS.AdtSrvTax = 0;
                                    objFS.AdtTF = 0;
                                    objFS.AdtTds = 0;
                                    objFS.IATAComm = 0;
                                }
                            }
                            if ((searchInputs.IsCorp == true) && (searchInputs.RTF != true))
                            {
                                try
                                {
                                    //DataTable MGDT = new DataTable();
                                    //MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.AdtFare.ToString()));
                                    objFS.AdtMgtFee = 0;
                                    objFS.AdtSrvTax = 0;
                                }
                                catch { }
                            }
                            objFS.AdtEduCess = 0;
                            objFS.AdtHighEduCess = 0;
                            #endregion


                            #region Adult Retrun
                            //objFS.AdtFar = IdType;
                            objFSR.AdtFar = "FDD";// CrdType;
                            objFSR.AdtCabin = "E";// "E";// t.COS;
                            objFSR.AdtFarebasis = dr["Rt_fareBasis"].ToString();
                            objFSR.AdtRbd = dr["RBD"].ToString();
                            #region ADTFR

                            objFSR.AdtFareType = "Non Refundable";


                            //FareSellKey , JSK
                            #endregion

                            float PROMODISCHD1 = 0;
                            float PROMODIS1 = 0;
                            //Adt Fare Details 
                            if (searchInputs.Adult > 0)
                            {
                                int m = 0;
                                var AdtCharges = 0;



                                // navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge adtd = new navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge();
                                // adtd = svc;



                                objFSR.AdtFSur = float.Parse(Math.Round(decimal.Parse(dr["YQ"].ToString()), 0).ToString());
                                objFSR.AdtWO = float.Parse(Math.Round(decimal.Parse(dr["WO"].ToString()), 0).ToString());
                                objFSR.AdtYR = float.Parse(Math.Round(decimal.Parse(dr["YR"].ToString()), 0).ToString());
                                objFSR.AdtBfare = float.Parse(Math.Round(decimal.Parse(dr["Basicfare"].ToString()), 0).ToString());
                                objFSR.AdtOT = float.Parse(Math.Round(decimal.Parse(dr["OT"].ToString()), 0).ToString());

                                objFSR.AdtFare = float.Parse(Math.Round(decimal.Parse(dr["GROSS_Total"].ToString()), 0).ToString());
                                AdtTF = 0;


                                PROMODIS1 = 0;


                                objFSR.AdtBfare = objFS.AdtBfare - PROMODIS;
                                objFSR.AdtFare = objFS.AdtFare - PROMODIS;
                                AdtTF = AdtTF - decimal.Parse(PROMODIS.ToString());

                                objFSR.AdtTax = objFSR.AdtFare - objFSR.AdtBfare;

                                // float srvChargeAdt = 0;
                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    srvChargeAdt = 0;//objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType, Convert.ToString(objFS.AdtBfare), Convert.ToString(objFS.AdtFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                }
                                catch { srvChargeAdt = 0; }
                                #endregion


                                //SMS charge add 
                                objFSR.AdtOT = objFS.AdtOT + srvChargeAdt;//srvCharge;
                                objFSR.AdtTax = objFS.AdtTax + srvChargeAdt;//srvCharge;
                                objFSR.AdtFare = objFS.AdtFare + srvChargeAdt; //srvCharge;
                                                                               //SMS charge add end
                                objFSR.ADTAdminMrk = float.Parse(dr["Admin_Markup"].ToString() == "" ? "0" : dr["Admin_Markup"].ToString()); // CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());




                                if (dr["Markup_Type"].ToString() == "Flat")
                                {
                                    objFSR.ADTAdminMrk = float.Parse(dr["Admin_Markup"].ToString() == "" ? "0" : dr["Admin_Markup"].ToString());
                                }
                                else
                                {
                                    objFSR.ADTAdminMrk = (objFSR.AdtBfare * Convert.ToInt32(dr["Admin_Markup"].ToString() == "" ? "0" : dr["Admin_Markup"].ToString())) / 100;
                                }

                                objFSR.ADTAgentMrk = float.Parse(dr["Markup"].ToString() == "" ? "0" : dr["Markup"].ToString());

                                //objFS.ADTAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.AdtFare, searchInputs.Trip.ToString());
                                //objFSR.ADTAgentMrk = float.Parse(dr["Markup"].ToString() == "" ? "0" : dr["Markup"].ToString());  //CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                {
                                    //objFS.ADTAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.AdtFare, searchInputs.Trip.ToString());
                                    //objFSR.ADTAdminMrk = float.Parse(dr["Admin_Markup"].ToString());
                                    objFSR.AdtBfare = objFSR.AdtBfare + objFSR.ADTAdminMrk;
                                    objFSR.AdtFare = objFSR.AdtFare + objFSR.ADTAdminMrk;
                                }
                                CommDt.Clear();
                                STTFTDS.Clear();
                                if ((searchInputs.Trip.ToString() == JourneyType.D.ToString()) && (searchInputs.RTF != true)) //&& (searchInputs.RTF != true)
                                {
                                    try
                                    {
                                        objFSR.AdtDiscount1 = 0;
                                        objFSR.AdtDiscount = 0;
                                        objFSR.AdtCB = 0;
                                        objFSR.AdtSrvTax = 0;
                                        objFSR.AdtTF = 0;
                                        objFSR.AdtTds = 0;
                                        objFSR.IATAComm = 0;
                                    }
                                    catch (Exception ex)
                                    {
                                        objFSR.AdtDiscount1 = 0;
                                        objFSR.AdtDiscount = 0;
                                        objFSR.AdtCB = 0;
                                        objFSR.AdtSrvTax = 0;
                                        objFSR.AdtTF = 0;
                                        objFSR.AdtTds = 0;
                                        objFSR.IATAComm = 0;
                                    }
                                    //}
                                }
                                else
                                {
                                    objFSR.AdtDiscount = 0;
                                    objFSR.AdtCB = 0;
                                    objFSR.AdtSrvTax = 0;
                                    objFSR.AdtTF = 0;
                                    objFSR.AdtTds = 0;
                                    objFS.IATAComm = 0;
                                }
                            }
                            if ((searchInputs.IsCorp == true) && (searchInputs.RTF != true))
                            {
                                try
                                {
                                    //DataTable MGDT = new DataTable();
                                    //MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.AdtFare.ToString()));
                                    objFSR.AdtMgtFee = 0;
                                    objFSR.AdtSrvTax = 0;
                                }
                                catch { }
                            }
                            objFSR.AdtEduCess = 0;
                            objFSR.AdtHighEduCess = 0;
                            #endregion
                            //222222222222222222222222222222222222222222222



                            #region child
                            objFS.ChdTax = 0;
                            if (searchInputs.Child > 0)
                            {
                                objFS.ChdFar = "";
                                objFS.ChdCabin = "E";// "E";// t.COS;
                                objFS.ChdFarebasis = dr["fareBasis"].ToString();
                                objFS.AdtRbd = dr["RBD"].ToString();
                                objFS.ChdfareType = "";
                                objFS.ChdTax = 0;

                                var ChdCharges = 0;
                                #region CHDFR


                                objFS.ChdFSur = float.Parse(Math.Round(decimal.Parse(dr["YQ"].ToString()), 0).ToString());
                                objFS.ChdWO = float.Parse(Math.Round(decimal.Parse(dr["WO"].ToString()), 0).ToString());
                                objFS.ChdYR = float.Parse(Math.Round(decimal.Parse(dr["YR"].ToString()), 0).ToString());
                                objFS.ChdBFare = float.Parse(Math.Round(decimal.Parse(dr["Basicfare"].ToString()), 0).ToString());
                                objFS.ChdOT = float.Parse(Math.Round(decimal.Parse(dr["OT"].ToString()), 0).ToString());
                                objFS.ChdFare = float.Parse(Math.Round(decimal.Parse(dr["GROSS_Total"].ToString()), 0).ToString());
                                ChdTF = 0;
                                PROMODISCHD = 0;

                                #endregion
                                objFS.ChdBFare = objFS.ChdBFare - PROMODISCHD;
                                objFS.ChdFare = objFS.ChdFare - PROMODISCHD;
                                ChdTF = ChdTF - decimal.Parse(PROMODISCHD.ToString());


                                objFS.ChdTax = objFS.ChdFare - objFS.ChdBFare;
                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    srvChargeChd = 0;
                                }
                                catch { srvChargeChd = 0; }
                                #endregion



                                objFS.ChdOT = objFS.ChdOT + srvChargeChd;//srvCharge;
                                objFS.ChdTax = objFS.ChdTax + srvChargeChd;//srvCharge;
                                objFS.ChdFare = objFS.ChdFare + srvChargeChd; //srvCharge;



                                if (dr["Markup_Type"].ToString() == "Flat")
                                {
                                    objFS.CHDAdminMrk = float.Parse(dr["Admin_Markup"].ToString() == "" ? "0" : dr["Admin_Markup"].ToString());
                                }
                                else
                                {
                                    objFS.CHDAdminMrk = (objFS.ChdBFare * Convert.ToInt32(dr["Admin_Markup"].ToString() == "" ? "0" : dr["Admin_Markup"].ToString())) / 100;
                                }

                                objFS.CHDAgentMrk = float.Parse(dr["Markup"].ToString() == "" ? "0" : dr["Markup"].ToString());


                                //objFS.CHDAgentMrk = float.Parse(dr["Markup"].ToString() == "" ? "0" : dr["Markup"].ToString());   //CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                CommDt.Clear();
                                STTFTDS.Clear();
                                if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                {
                                    // objFS.CHDAdminMrk = float.Parse(dr["Admin_Markup"].ToString() == "" ? "0" : dr["Admin_Markup"].ToString());

                                    objFS.ChdBFare = objFS.ChdBFare + objFS.CHDAdminMrk;
                                    objFS.ChdFare = objFS.ChdFare + objFS.CHDAdminMrk;
                                }
                                if ((searchInputs.Trip.ToString() == JourneyType.D.ToString()) && (searchInputs.RTF != true))
                                {
                                    try
                                    {
                                        objFS.ChdDiscount1 = 0;
                                        objFS.ChdDiscount = 0;
                                        objFS.ChdCB = 0;
                                        objFS.ChdSrvTax = 0;
                                        objFS.ChdTF = 0;
                                        objFS.ChdTds = 0;
                                    }
                                    catch (Exception ex)
                                    {
                                        objFS.ChdDiscount1 = 0;
                                        objFS.ChdDiscount = 0;
                                        objFS.ChdCB = 0;
                                        objFS.ChdSrvTax = 0;
                                        objFS.ChdTF = 0;
                                        objFS.ChdTds = 0;
                                    }
                                }
                                else
                                {
                                    objFS.ChdDiscount1 = 0;
                                    objFS.ChdDiscount = 0;
                                    objFS.ChdCB = 0;
                                    objFS.ChdSrvTax = 0;
                                    objFS.ChdTF = 0;
                                    objFS.ChdTds = 0;
                                }
                                if ((searchInputs.IsCorp == true) && (searchInputs.RTF != true))
                                {
                                    try
                                    {

                                        objFS.ChdMgtFee = 0;
                                        objFS.ChdSrvTax = 0;
                                    }
                                    catch { }
                                }
                                objFS.ChdEduCess = 0;
                                objFS.ChdHighEduCess = 0;
                                objFS.IATAComm = 0;
                            }
                            else
                            {
                                objFS.ChdBFare = 0;
                                objFS.ChdFare = 0;
                                objFS.ChdFSur = 0;
                                objFS.ChdWO = 0;
                                objFS.ChdIN = 0;
                                objFS.ChdJN = 0;
                                objFS.ChdYR = 0;
                                objFS.ChdOT = 0;
                                objFS.ChdTax = 0;
                                objFS.ChdDiscount = 0;
                                objFS.ChdCB = 0;
                                objFS.ChdSrvTax = 0;
                                objFS.ChdTF = 0;
                                objFS.ChdTds = 0;
                                objFS.ChdEduCess = 0;
                                objFS.ChdHighEduCess = 0;

                            }

                            #endregion

                            #region child Return
                            objFSR.ChdTax = 0;
                            if (searchInputs.Child > 0)
                            {
                                objFSR.ChdFar = "";
                                objFSR.ChdCabin = "E";// "E";// t.COS;
                                objFSR.ChdFarebasis = dr["fareBasis"].ToString();
                                objFSR.AdtRbd = dr["Rt_RBD"].ToString();
                                objFSR.ChdfareType = "";
                                objFSR.ChdTax = 0;

                                var ChdCharges = 0;
                                #region CHDFR


                                objFSR.ChdFSur = float.Parse(Math.Round(decimal.Parse(dr["YQ"].ToString()), 0).ToString());
                                objFSR.ChdWO = float.Parse(Math.Round(decimal.Parse(dr["WO"].ToString()), 0).ToString());
                                objFSR.ChdYR = float.Parse(Math.Round(decimal.Parse(dr["YR"].ToString()), 0).ToString());
                                objFSR.ChdBFare = float.Parse(Math.Round(decimal.Parse(dr["Basicfare"].ToString()), 0).ToString());
                                objFSR.ChdOT = float.Parse(Math.Round(decimal.Parse(dr["OT"].ToString()), 0).ToString());
                                objFSR.ChdFare = float.Parse(Math.Round(decimal.Parse(dr["GROSS_Total"].ToString()), 0).ToString());
                                ChdTF = 0;
                                PROMODISCHD = 0;

                                #endregion
                                objFSR.ChdBFare = objFSR.ChdBFare - PROMODISCHD;
                                objFSR.ChdFare = objFSR.ChdFare - PROMODISCHD;
                                ChdTF = ChdTF - decimal.Parse(PROMODISCHD.ToString());


                                objFSR.ChdTax = objFSR.ChdFare - objFSR.ChdBFare;
                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    srvChargeChd = 0;
                                }
                                catch { srvChargeChd = 0; }
                                #endregion



                                objFSR.ChdOT = objFSR.ChdOT + srvChargeChd;//srvCharge;
                                objFSR.ChdTax = objFSR.ChdTax + srvChargeChd;//srvCharge;
                                objFSR.ChdFare = objFSR.ChdFare + srvChargeChd; //srvCharge;




                                if (dr["Markup_Type"].ToString() == "Flat")
                                {
                                    objFSR.CHDAdminMrk = float.Parse(dr["Admin_Markup"].ToString() == "" ? "0" : dr["Admin_Markup"].ToString());
                                }
                                else
                                {
                                    objFSR.CHDAdminMrk = (objFS.ChdBFare * Convert.ToInt32(dr["Admin_Markup"].ToString() == "" ? "0" : dr["Admin_Markup"].ToString())) / 100;
                                }

                                objFSR.CHDAgentMrk = float.Parse(dr["Markup"].ToString() == "" ? "0" : dr["Markup"].ToString());



                                //objFSR.CHDAgentMrk = float.Parse(dr["Markup"].ToString() == "" ? "0" : dr["Markup"].ToString());   //CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                CommDt.Clear();
                                STTFTDS.Clear();
                                if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                {
                                    // objFSR.CHDAdminMrk = float.Parse(dr["Admin_Markup"].ToString() == "" ? "0" : dr["Admin_Markup"].ToString());

                                    objFSR.ChdBFare = objFSR.ChdBFare + objFSR.CHDAdminMrk;
                                    objFSR.ChdFare = objFSR.ChdFare + objFSR.CHDAdminMrk;
                                }
                                if ((searchInputs.Trip.ToString() == JourneyType.D.ToString()) && (searchInputs.RTF != true))
                                {
                                    try
                                    {
                                        objFSR.ChdDiscount1 = 0;
                                        objFSR.ChdDiscount = 0;
                                        objFSR.ChdCB = 0;
                                        objFSR.ChdSrvTax = 0;
                                        objFSR.ChdTF = 0;
                                        objFSR.ChdTds = 0;
                                    }
                                    catch (Exception ex)
                                    {
                                        objFSR.ChdDiscount1 = 0;
                                        objFSR.ChdDiscount = 0;
                                        objFSR.ChdCB = 0;
                                        objFSR.ChdSrvTax = 0;
                                        objFSR.ChdTF = 0;
                                        objFSR.ChdTds = 0;
                                    }
                                }
                                else
                                {
                                    objFSR.ChdDiscount1 = 0;
                                    objFSR.ChdDiscount = 0;
                                    objFSR.ChdCB = 0;
                                    objFSR.ChdSrvTax = 0;
                                    objFSR.ChdTF = 0;
                                    objFSR.ChdTds = 0;
                                }
                                if ((searchInputs.IsCorp == true) && (searchInputs.RTF != true))
                                {
                                    try
                                    {

                                        objFSR.ChdMgtFee = 0;
                                        objFSR.ChdSrvTax = 0;
                                    }
                                    catch { }
                                }
                                objFSR.ChdEduCess = 0;
                                objFSR.ChdHighEduCess = 0;
                                objFSR.IATAComm = 0;
                            }
                            else
                            {
                                objFSR.ChdBFare = 0;
                                objFSR.ChdFare = 0;
                                objFSR.ChdFSur = 0;
                                objFSR.ChdWO = 0;
                                objFSR.ChdIN = 0;
                                objFSR.ChdJN = 0;
                                objFSR.ChdYR = 0;
                                objFSR.ChdOT = 0;
                                objFSR.ChdTax = 0;
                                objFSR.ChdDiscount = 0;
                                objFSR.ChdCB = 0;
                                objFSR.ChdSrvTax = 0;
                                objFSR.ChdTF = 0;
                                objFSR.ChdTds = 0;
                                objFSR.ChdEduCess = 0;
                                objFSR.ChdHighEduCess = 0;

                            }

                            #endregion

                            objFSR.ElectronicTicketing = "";

                            objFSR.InfCabin = "";
                            objFSR.InfFarebasis = "";
                            objFSR.InfRbd = "";
                            objFSR.InfFareType = "";
                            objFSR.InfTax = 0;
                            //333333333333333333333333333333333333333333333333333333333333333333333333333333333
                            objFS.ElectronicTicketing = "";

                            objFS.InfCabin = "";
                            objFS.InfFarebasis = "";
                            objFS.InfRbd = "";
                            objFS.InfFareType = "";
                            objFS.InfTax = 0;




                            #region Infant
                            if (searchInputs.Infant > 0)
                            {


                                objFS.InfBfare = float.Parse(Math.Round(decimal.Parse(dr["Basicfare"].ToString()), 0).ToString());
                                objFS.InfOT = float.Parse(Math.Round(decimal.Parse(dr["OT"].ToString()), 0).ToString());
                                objFS.InfFare = float.Parse(Math.Round(decimal.Parse(dr["GROSS_Total"].ToString()), 0).ToString());

                                objFS.InfTax = objFS.InfFare - objFS.InfBfare;

                                if (objFS.InfBfare > 0)
                                {
                                    if (objFS.InfFare > objFS.InfTax)
                                        objFS.InfBfare = float.Parse(Math.Round(objFS.InfFare - objFS.InfTax).ToString());
                                    else
                                        objFS.InfBfare = float.Parse(Math.Round(objFS.InfTax - objFS.InfFare).ToString());
                                    objFS.InfFare = float.Parse(Math.Round(objFS.InfFare).ToString());
                                    objFS.InfTax = float.Parse(Math.Round(objFS.InfTax).ToString());
                                    objFS.InfOT = objFS.InfTax;
                                    InfTF = decimal.Parse(Math.Round(objFS.InfFare).ToString());
                                }
                                else
                                {
                                    objFS.InfFare = float.Parse(Math.Round(objFS.InfBfare + objFS.InfTax).ToString());
                                    objFS.InfBfare = float.Parse(Math.Round(objFS.InfBfare).ToString());
                                    objFS.InfTax = float.Parse(Math.Round(objFS.InfTax).ToString());
                                    objFS.InfOT = objFS.InfTax;
                                    InfTF = decimal.Parse(Math.Round(objFS.InfFare).ToString());
                                }
                                objFS.InfFSur = 0;
                                objFS.InfIN = 0;
                                objFS.InfJN = 0;
                                //objFS.InfOT = 0;
                                objFS.InfQ = 0;

                                if (searchInputs.IsCorp == true)
                                {
                                    try
                                    {

                                        objFS.InfMgtFee = 0;
                                        objFS.InfSrvTax = 0;
                                    }
                                    catch { }
                                }
                            }
                            else
                            {
                                objFS.InfFare = 0;
                                objFS.InfBfare = 0;
                                objFS.InfFSur = 0;
                                objFS.InfIN = 0;
                                objFS.InfJN = 0;
                                objFS.InfOT = 0;
                                objFS.InfQ = 0;
                            }
                            #endregion



                            #region Infant Retrun
                            if (searchInputs.Infant > 0)
                            {


                                objFSR.InfBfare = float.Parse(Math.Round(decimal.Parse(dr["Basicfare"].ToString()), 0).ToString());
                                objFSR.InfOT = float.Parse(Math.Round(decimal.Parse(dr["OT"].ToString()), 0).ToString());
                                objFSR.InfFare = float.Parse(Math.Round(decimal.Parse(dr["GROSS_Total"].ToString()), 0).ToString());

                                objFSR.InfTax = objFSR.InfFare - objFSR.InfBfare;

                                if (objFSR.InfBfare > 0)
                                {
                                    if (objFSR.InfFare > objFSR.InfTax)
                                        objFSR.InfBfare = float.Parse(Math.Round(objFSR.InfFare - objFSR.InfTax).ToString());
                                    else
                                        objFSR.InfBfare = float.Parse(Math.Round(objFSR.InfTax - objFSR.InfFare).ToString());
                                    objFSR.InfFare = float.Parse(Math.Round(objFSR.InfFare).ToString());
                                    objFSR.InfTax = float.Parse(Math.Round(objFSR.InfTax).ToString());
                                    objFSR.InfOT = objFS.InfTax;
                                    InfTF = decimal.Parse(Math.Round(objFSR.InfFare).ToString());
                                }
                                else
                                {
                                    objFSR.InfFare = float.Parse(Math.Round(objFSR.InfBfare + objFSR.InfTax).ToString());
                                    objFSR.InfBfare = float.Parse(Math.Round(objFSR.InfBfare).ToString());
                                    objFSR.InfTax = float.Parse(Math.Round(objFSR.InfTax).ToString());
                                    objFSR.InfOT = objFSR.InfTax;
                                    InfTF = decimal.Parse(Math.Round(objFSR.InfFare).ToString());
                                }
                                objFSR.InfFSur = 0;
                                objFSR.InfIN = 0;
                                objFSR.InfJN = 0;
                                //objFS.InfOT = 0;
                                objFSR.InfQ = 0;

                                if (searchInputs.IsCorp == true)
                                {
                                    try
                                    {

                                        objFSR.InfMgtFee = 0;
                                        objFSR.InfSrvTax = 0;
                                    }
                                    catch { }
                                }
                            }
                            else
                            {
                                objFSR.InfFare = 0;
                                objFSR.InfBfare = 0;
                                objFSR.InfFSur = 0;
                                objFSR.InfIN = 0;
                                objFSR.InfJN = 0;
                                objFSR.InfOT = 0;
                                objFSR.InfQ = 0;
                            }
                            #endregion
                            ////4444444444444444444444444444444444444444444444444

                            objFS.OriginalTF = float.Parse((AdtTF * objFS.Adult + ChdTF * objFS.Child).ToString());//Without Infant Fare
                            objFS.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
                            objFS.TotalTax = (objFS.AdtTax * objFS.Adult) + (objFS.ChdTax * objFS.Child) + (objFS.InfTax * objFS.Infant);
                            objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
                            objFS.TotalFare = (objFS.AdtFare * objFS.Adult) + (objFS.ChdFare * objFS.Child) + (objFS.InfFare * objFS.Infant);
                            objFS.STax = (objFS.AdtSrvTax * objFS.Adult) + (objFS.ChdSrvTax * objFS.Child) + (objFS.InfSrvTax * objFS.Infant);
                            objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
                            objFS.TotDis = (objFS.AdtDiscount * objFS.Adult) + (objFS.ChdDiscount * objFS.Child);
                            objFS.TotCB = (objFS.AdtCB * objFS.Adult) + (objFS.ChdCB * objFS.Child);
                            objFS.TotTds = (objFS.AdtTds * objFS.Adult) + (objFS.ChdTds * objFS.Child);// +objFS.InfTds;
                            objFS.TotMrkUp = (objFS.ADTAdminMrk * objFS.Adult) + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAdminMrk * objFS.Child) + (objFS.CHDAgentMrk * objFS.Child);
                            objFS.TotMgtFee = (objFS.AdtMgtFee * objFS.Adult) + (objFS.ChdMgtFee * objFS.Child) + (objFS.InfMgtFee * objFS.Infant);
                            if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                objFS.TotalFare = objFS.TotalFare + objFS.STax + objFS.TFee + objFS.TotMgtFee + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child);
                            else
                                objFS.TotalFare = objFS.TotalFare + objFS.TotMrkUp + objFS.STax + objFS.TFee + objFS.TotMgtFee;
                            objFS.NetFare = (objFS.TotalFare + objFS.TotTds) - (objFS.TotDis + objFS.TotCB + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child));
                            //objFS.OperatingCarrier = p.opreatingcarrier;
                            objFS.NetFareSRTF = objFS.NetFare;
                            //int hr = int.Parse(p.jrnyTime) / 60;
                            //int min = int.Parse(p.jrnyTime) % 60;
                            //objFS.TotDur = hr + " hrs " + min + " min";

                            objFS.Stops = legs - 1 + "-Stop";
                            objFS.FareDet = "";
                            objFS.Trip = searchInputs.Trip.ToString();
                            // objFS.FType = searchInputs.TripType.ToString();
                            if ((searchInputs.RTF == true) || (searchInputs.Trip == Trip.I))
                            {
                                if (schd == 0)
                                {
                                    objFS.Flight = "1";
                                    if (searchInputs.RTF == true)
                                        objFS.FType = "RTF";
                                    else
                                        objFS.FType = "OutBound";
                                    objFS.TripType = "O";
                                }
                                else
                                {
                                    objFS.Flight = "2";
                                    if (searchInputs.RTF == true)
                                        objFS.FType = "RTF";
                                    else
                                        objFS.FType = "InBound";
                                    objFS.TripType = "R";
                                }
                            }
                            else
                            {
                                objFS.Flight = "1";
                                objFS.FType = "OutBound";
                                objFS.TripType = "O";
                            }
                            objFS.IsCorp = searchInputs.IsCorp;
                            objFS.Provider = "FDD";
                            srvCharge = srvChargeAdt + srvChargeChd;  //06-03-2018  Add MISC CHARGES PAX WISE
                            objFS.OriginalTT = srvCharge;
                            objFS.SearchId = "FDD";


                            ////////Retrun

                            objFSR.OriginalTF = float.Parse((AdtTF * objFSR.Adult + ChdTF * objFSR.Child).ToString());//Without Infant Fare
                            objFSR.TotBfare = (objFSR.AdtBfare * objFSR.Adult) + (objFSR.ChdBFare * objFSR.Child) + (objFSR.InfBfare * objFSR.Infant);
                            objFSR.TotalTax = (objFSR.AdtTax * objFSR.Adult) + (objFSR.ChdTax * objFSR.Child) + (objFSR.InfTax * objFSR.Infant);
                            objFSR.TotalFuelSur = (objFSR.AdtFSur * objFSR.Adult) + (objFSR.ChdFSur * objFSR.Child);
                            objFSR.TotalFare = (objFSR.AdtFare * objFSR.Adult) + (objFSR.ChdFare * objFSR.Child) + (objFSR.InfFare * objFSR.Infant);
                            objFSR.STax = (objFSR.AdtSrvTax * objFSR.Adult) + (objFSR.ChdSrvTax * objFSR.Child) + (objFSR.InfSrvTax * objFSR.Infant);
                            objFSR.TFee = (objFSR.AdtTF * objFSR.Adult) + (objFSR.ChdTF * objFSR.Child) + (objFSR.InfTF * objFSR.Infant);
                            objFSR.TotDis = (objFSR.AdtDiscount * objFSR.Adult) + (objFSR.ChdDiscount * objFSR.Child);
                            objFSR.TotCB = (objFSR.AdtCB * objFSR.Adult) + (objFSR.ChdCB * objFSR.Child);
                            objFSR.TotTds = (objFSR.AdtTds * objFSR.Adult) + (objFSR.ChdTds * objFSR.Child);// +objFSR.InfTds;
                            objFSR.TotMrkUp = (objFSR.ADTAdminMrk * objFSR.Adult) + (objFSR.ADTAgentMrk * objFSR.Adult) + (objFSR.CHDAdminMrk * objFSR.Child) + (objFSR.CHDAgentMrk * objFSR.Child);
                            objFSR.TotMgtFee = (objFSR.AdtMgtFee * objFSR.Adult) + (objFSR.ChdMgtFee * objFSR.Child) + (objFSR.InfMgtFee * objFSR.Infant);
                            if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                objFSR.TotalFare = objFSR.TotalFare + objFSR.STax + objFSR.TFee + objFSR.TotMgtFee + (objFSR.ADTAgentMrk * objFSR.Adult) + (objFSR.CHDAgentMrk * objFSR.Child);
                            else
                                objFSR.TotalFare = objFSR.TotalFare + objFSR.TotMrkUp + objFSR.STax + objFSR.TFee + objFSR.TotMgtFee;
                            objFSR.NetFare = (objFSR.TotalFare + objFSR.TotTds) - (objFSR.TotDis + objFSR.TotCB + (objFSR.ADTAgentMrk * objFSR.Adult) + (objFSR.CHDAgentMrk * objFSR.Child));
                            //objFSR.OperatingCarrier = p.opreatingcarrier;
                            objFSR.NetFareSRTF = objFSR.NetFare;
                            //int hr = int.Parse(p.jrnyTime) / 60;
                            //int min = int.Parse(p.jrnyTime) % 60;
                            //objFSR.TotDur = hr + " hrs " + min + " min";

                            objFSR.Stops = legs - 1 + "-Stop";
                            objFSR.FareDet = "";
                            objFSR.Trip = searchInputs.Trip.ToString();
                            // objFSR.FType = searchInputs.TripType.ToString();
                            if ((searchInputs.RTF == true) || (searchInputs.Trip == Trip.I))
                            {
                                if (schd1 == 0)
                                {
                                    objFSR.Flight = "1";
                                    if (searchInputs.RTF == true)
                                        objFSR.FType = "RTF";
                                    else
                                        objFSR.FType = "OutBound";
                                    objFSR.TripType = "O";
                                }
                                else
                                {
                                    objFSR.Flight = "2";
                                    if (searchInputs.RTF == true)
                                        objFSR.FType = "RTF";
                                    else
                                        objFSR.FType = "InBound";
                                    objFSR.TripType = "R";
                                }
                            }
                            else
                            {
                                objFSR.Flight = "1";
                                objFSR.FType = "OutBound";
                                objFSR.TripType = "O";
                            }
                            objFSR.IsCorp = searchInputs.IsCorp;
                            objFSR.Provider = "FDD";
                            srvCharge = srvChargeAdt + srvChargeChd;  //06-03-2018  Add MISC CHARGES PAX WISE
                            objFSR.OriginalTT = srvCharge;
                            objFSR.SearchId = "FDD";


                            FlightList.Add(objFS);
                            FlightList.Add(objFSR);
                        }
                        LNO = LNO + 1;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


            CacheList.Add(FlightList);
            //CacheList.Add(objFS);

            return CacheList;
        }


        private ArrayList AvilabilityList(DataSet FlightDs, List<FltSrvChargeList> SrvChargeList, DataSet MrkupDS, List<MISCCharges> MiscList, string GType, string TDS, bool RTF, FlightSearch searchInputs, string CrdType)
        {

            ICacheManager objCacheManager = CacheFactory.GetCacheManager("MyCacheManager");
            if (objCacheManager.Contains("CityList"))
            {
                CityList = (List<FlightCityList>)objCacheManager.GetData("CityList");

            }
            else
            {
                CityList = Data.GetCityList("I", ConnStr);
                FileDependency _objFileDependency = new FileDependency(HttpContext.Current.Server.MapPath("~/UpdateCache.XML"));
                objCacheManager.Add("CityList", CityList, CacheItemPriority.Normal, null, _objFileDependency);
            }
            if (objCacheManager.Contains("Airlist"))
            {
                Airlist = (List<AirlineList>)objCacheManager.GetData("Airlist");
            }
            else
            {
                Airlist = Data.GetAirlineList(ConnStr);
                FileDependency _objFileDependency = new FileDependency(HttpContext.Current.Server.MapPath("~/UpdateCache.XML"));
                objCacheManager.Add("Airlist", Airlist, CacheItemPriority.Normal, null, _objFileDependency);
            }

            int flight = 1;
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            ArrayList CacheList = new ArrayList(); float srvCharge = 0;
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            float srvChargeAdt = 0;
            float srvChargeChd = 0;
            //FlightCommonBAL obj = new FlightCommonBAL();

            List<BaggageList> BagInfoList = new List<BaggageList>();
            List<FlightSearchResults> FlightList = new List<FlightSearchResults>();


            BagInfoList = null; //objFltComm.GetBaggage(searchInputs.Trip.ToString(), VC, Bag);

            string strFlightNo = "";
            string strValidatingCarrier = "";
            string FlttmDur = "";//added by abhilash
            int LNO = 1;

            try
            {

                #region OutBOund -Inbound
                foreach (DataRow dr in FlightDs.Tables[0].Rows)
                {
                    string arrCity = "", depCity = "";
                    FlightSearchResults objFS = new FlightSearchResults();
                    depCity = dr["DepAirportCode"].ToString().Replace(")", "").Replace("(", "");
                    arrCity = dr["ArrAirportCode"].ToString().Replace(")", "").Replace("(", "");

                    try
                    {
                        #region Get TotalViaSector for Commission PPPSector and PPPSegment
                        int TotalViaSector = 1;
                        #endregion
                        int legs = 1;
                        for (int i = 0; i < legs; i++)
                        {

                            decimal AdtTF = 0, ChdTF = 0, InfTF = 0;//For Total Fare Per Pax Type
                            #region Flight Details
                            objFS.OrgDestFrom = depCity;
                            objFS.OrgDestTo = arrCity;
                            objFS.Flight = flight.ToString();
                            objFS.DepartureLocation = depCity;
                            objFS.DepAirportCode = depCity;
                            objFS.DepartureTerminal = dr["DepartureTerminal"].ToString();
                            objFS.DepartureCityName = getCityName(objFS.DepartureLocation, CityList);
                            objFS.ArrivalLocation = arrCity;
                            objFS.ArrAirportCode = arrCity;
                            objFS.ArrivalTerminal = dr["ArrivalTerminal"].ToString();
                            objFS.ArrivalCityName = getCityName(objFS.ArrivalLocation, CityList);
                            try
                            {
                                objFS.DepartureAirportName = ((from ct in CityList where ct.AirportCode == objFS.DepartureLocation select ct).ToList())[0].AirportName;
                                objFS.ArrivalAirportName = ((from ct in CityList where ct.AirportCode == objFS.ArrivalLocation select ct).ToList())[0].AirportName;
                            }
                            catch (Exception ex)
                            {
                                objFS.DepartureAirportName = depCity;
                                objFS.ArrivalAirportName = arrCity;
                            }
                            string[] Dep = Utility.Split(dr["Departure_Date"].ToString(), "/");
                            objFS.DepartureDate = Dep[0] + Dep[1] + Utility.Right(Dep[2], 2);
                            objFS.Departure_Date = Dep[0] + " " + Utility.datecon(Dep[1]);
                            objFS.DepartureTime = dr["DepartureTime"].ToString();//.Replace(":", "");

                            string[] Arr = Utility.Split(dr["Arrival_Date"].ToString(), "/");
                            objFS.ArrivalDate = Arr[0] + Arr[1] + Utility.Right(Arr[2], 2);
                            objFS.Arrival_Date = Arr[0] + " " + Utility.datecon(Arr[1]);
                            objFS.ArrivalTime = dr["ArrivalTime"].ToString();//.Replace(":", "");
                            try
                            {

                                DateTime DDTime = DateTime.Parse(objFS.DepartureTime);
                                DateTime ADTime = DateTime.Parse(objFS.ArrivalTime);
                                TimeSpan value = ADTime - DDTime;
                                string label = string.Format("{0:hh\\:mm\\:ss}", value);
                                objFS.TotDur = Utility.Left(label, 5);
                                // objFS.TotDur = objFS.TotDur = Utility.Left(Convert.ToDateTime(t.Leg[i]["STA"]).Subtract(Convert.ToDateTime(t.Leg[i]["STD"])).ToString(), 5);


                            }
                            catch { objFS.TotDur = ""; }
                            objFS.Adult = searchInputs.Adult;
                            objFS.Child = searchInputs.Child;
                            objFS.Infant = searchInputs.Infant;
                            objFS.TotPax = searchInputs.Adult + searchInputs.Child;

                            string VC = dr["MarketingCarrier"].ToString().Replace(")", "").Replace("(", "");
                            objFS.MarketingCarrier = VC;
                            objFS.OperatingCarrier = VC;
                            objFS.FlightIdentification = dr["FlightNo"].ToString();

                            objFS.ValiDatingCarrier = VC;

                            objFS.AirLineName = dr["AirLineName"].ToString();
                            objFS.AvailableSeats = dr["Avl_Seat"].ToString();
                            objFS.AvailableSeats1 = dr["Avl_Seat"].ToString();

                            objFS.BagInfo = dr["BagInfo"].ToString();


                            #region Baggage
                            objFS.IsBagFare = false;
                            //if (objFareTypeSettings != null && objFareTypeSettings.Count > 0 && objFareTypeSettings[0].SSRCode != null)
                            objFS.SSRCode = "";
                            #endregion
                            #region SME FARE        
                            objFS.IsSMEFare = false;
                            objFS.IsLTC = false;
                            #endregion
                            //objFS.TotDur = "";
                            string[] Dep1 = Utility.Split(dr["Departure_Date"].ToString(), "/");
                            string[] Arr1 = Utility.Split(dr["Arrival_Date"].ToString(), "/");
                            objFS.depdatelcc = Dep1[2] + '-' + Dep1[1] + '-' + Dep1[0] + "T" + dr["DepartureTime"].ToString() + ":00";
                            objFS.arrdatelcc = Arr1[2] + '-' + Arr1[1] + '-' + Arr1[0] + "T" + dr["ArrivalTime"].ToString() + ":00";

                            int schd = 0;
                            if (searchInputs.Trip == Trip.I)
                            {
                                if (schd == 0)
                                {
                                    if (searchInputs.TripType == STD.Shared.TripType.RoundTrip)
                                        objFS.Sector = depCity + ":" + arrCity + ":" + depCity;
                                    else
                                        objFS.Sector = depCity + ":" + arrCity;
                                }
                                else
                                {
                                    if (searchInputs.TripType == STD.Shared.TripType.RoundTrip)
                                        objFS.Sector = arrCity + ":" + depCity + ":" + arrCity;
                                    else
                                        objFS.Sector = depCity + ":" + arrCity;
                                }
                            }
                            else
                            {
                                if (schd == 0)
                                {

                                    if (searchInputs.RTF == true)
                                    { objFS.Sector = depCity + ":" + arrCity + ":" + depCity; }
                                    else
                                    { objFS.Sector = depCity + ":" + arrCity; }
                                }
                                else
                                {
                                    if (searchInputs.RTF == true)
                                        objFS.Sector = arrCity + ":" + depCity + ":" + arrCity;
                                    else
                                        objFS.Sector = depCity + ":" + arrCity;
                                }
                            }

                            objFS.EQ = dr["FlightNo"].ToString();
                            objFS.LineNumber = LNO;
                            // objFS.Ln = LNO;
                            objFS.Leg = (i + 1);
                            objFS.fareBasis = dr["fareBasis"].ToString();
                            objFS.RBD = dr["RBD"].ToString();
                            objFS.AdtRbd = dr["RBD"].ToString();
                            objFS.ChdRbd = dr["RBD"].ToString();
                            objFS.InfRbd = dr["RBD"].ToString();
                            objFS.ChdCabin = "E";
                            objFS.InfCabin = "E";
                            objFS.FareRule = dr["FareRule"].ToString(); //Fare Sell Key
                            objFS.Searchvalue = "";//t.FSK; //Fare Sell Key
                            objFS.sno = ""; //t.JSK; //Journey Sell Key
                            objFS.SubKey = "";
                            objFS.Track_id = "";
                            objFS.ProviderUserID = "";

                            objFS.Leg = (i + 1);
                            objFS.fareBasis = dr["fareBasis"].ToString();
                            objFS.MainKey = "";

                            objFS.AdtAvlStatus = "";
                            objFS.ChdBreakPoint = "";
                            objFS.ChdAvlStatus = "";
                            objFS.InfBreakPoint = "";
                            objFS.InfAvlStatus = "";
                            objFS.TotalTripDur = objFS.TotDur;
                            objFS.TripCnt = objFS.TotDur;
                            objFS.FBPaxType = "";
                            objFS.InfFar = "";
                            objFS.ChdFar = "";
                            objFS.getVia = "";
                            objFS.ProductDetailQualifier = "";


                            #endregion

                            ////SMS charge calc
                            //float srvCharge = 0;
                            //float srvChargeAdt = 0;
                            //float srvChargeChd = 0;

                            //float srvCharge = 0;
                            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID);
                            ////SMS charge calc end


                            #region Adult
                            //objFS.AdtFar = IdType;
                            objFS.AdtFar = "FDD";// CrdType;
                            objFS.AdtCabin = "E";// "E";// t.COS;
                            objFS.AdtFarebasis = dr["fareBasis"].ToString();
                            objFS.AdtRbd = dr["RBD"].ToString();
                            #region ADTFR

                            objFS.AdtFareType = "Non Refundable";


                            //FareSellKey , JSK
                            #endregion

                            float PROMODISCHD = 0;
                            float PROMODIS = 0;
                            //Adt Fare Details 
                            if (searchInputs.Adult > 0)
                            {
                                int m = 0;
                                var AdtCharges = 0;



                                // navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge adtd = new navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.BookingServiceCharge();
                                // adtd = svc;



                                objFS.AdtFSur = float.Parse(Math.Round(decimal.Parse(dr["YQ"].ToString()), 0).ToString());
                                objFS.AdtWO = float.Parse(Math.Round(decimal.Parse(dr["WO"].ToString()), 0).ToString());
                                objFS.AdtYR = float.Parse(Math.Round(decimal.Parse(dr["YR"].ToString()), 0).ToString());
                                objFS.AdtBfare = float.Parse(Math.Round(decimal.Parse(dr["Basicfare"].ToString()), 0).ToString());
                                objFS.AdtOT = float.Parse(Math.Round(decimal.Parse(dr["OT"].ToString()), 0).ToString());

                                objFS.AdtFare = float.Parse(Math.Round(decimal.Parse(dr["GROSS_Total"].ToString()), 0).ToString());
                                AdtTF = 0;


                                PROMODIS = 0;


                                objFS.AdtBfare = objFS.AdtBfare - PROMODIS;
                                objFS.AdtFare = objFS.AdtFare - PROMODIS;
                                AdtTF = AdtTF - decimal.Parse(PROMODIS.ToString());

                                objFS.AdtTax = objFS.AdtFare - objFS.AdtBfare;

                                // float srvChargeAdt = 0;
                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    srvChargeAdt = 0;//objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType, Convert.ToString(objFS.AdtBfare), Convert.ToString(objFS.AdtFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                }
                                catch { srvChargeAdt = 0; }
                                #endregion


                                //SMS charge add 
                                objFS.AdtOT = objFS.AdtOT + srvChargeAdt;//srvCharge;
                                objFS.AdtTax = objFS.AdtTax + srvChargeAdt;//srvCharge;
                                objFS.AdtFare = objFS.AdtFare + srvChargeAdt; //srvCharge;
                                                                              //SMS charge add end

                                if (dr["Markup_Type"].ToString() == "Flat")
                                {

                                    objFS.ADTAdminMrk = float.Parse(dr["Admin_Markup"].ToString() == "" ? "0" : dr["Admin_Markup"].ToString()); // CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                }
                                else
                                {
                                    objFS.ADTAdminMrk = (objFS.AdtBfare * Convert.ToInt32(dr["Admin_Markup"].ToString() == "" ? "0" : dr["Admin_Markup"].ToString())) / 100;
                                }

                                objFS.ADTAgentMrk = float.Parse(dr["Markup"].ToString() == "" ? "0" : dr["Markup"].ToString());
                                //objFS.ADTAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.AdtFare, searchInputs.Trip.ToString());
                                //   objFS.ADTAgentMrk = float.Parse(dr["Markup"].ToString() == "" ? "0" : dr["Markup"].ToString());  //CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                                if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                {
                                    //objFS.ADTAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier + CrdType, objFS.AdtFare, searchInputs.Trip.ToString());
                                    objFS.ADTAdminMrk = float.Parse(dr["Admin_Markup"].ToString());
                                    objFS.AdtBfare = objFS.AdtBfare + objFS.ADTAdminMrk;
                                    objFS.AdtFare = objFS.AdtFare + objFS.ADTAdminMrk;
                                }
                                CommDt.Clear();
                                STTFTDS.Clear();
                                if ((searchInputs.Trip.ToString() == JourneyType.D.ToString()) && (searchInputs.RTF != true)) //&& (searchInputs.RTF != true)
                                {
                                    //if ((IdType != "SGTBF") && (IdType != "SGSTR"))
                                    //{
                                    try
                                    {
                                        //CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), 1, objFS.AdtRbd, objFS.AdtCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, CrdType, Convert.ToString(TotalViaSector));
                                        //objFS.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                        //objFS.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                        //STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objFS.ValiDatingCarrier, objFS.AdtDiscount1, objFS.AdtBfare, objFS.AdtFSur, searchInputs.TDS);
                                        //objFS.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                        //objFS.AdtDiscount = objFS.AdtDiscount1 - objFS.AdtSrvTax1;
                                        //objFS.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                        //objFS.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                        //objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());
                                        objFS.AdtDiscount1 = 0;
                                        objFS.AdtDiscount = 0;
                                        objFS.AdtCB = 0;
                                        objFS.AdtSrvTax = 0;
                                        objFS.AdtTF = 0;
                                        objFS.AdtTds = 0;
                                        objFS.IATAComm = 0;
                                    }
                                    catch (Exception ex)
                                    {
                                        objFS.AdtDiscount1 = 0;
                                        objFS.AdtDiscount = 0;
                                        objFS.AdtCB = 0;
                                        objFS.AdtSrvTax = 0;
                                        objFS.AdtTF = 0;
                                        objFS.AdtTds = 0;
                                        objFS.IATAComm = 0;
                                    }
                                    //}
                                }
                                else
                                {
                                    objFS.AdtDiscount = 0;
                                    objFS.AdtCB = 0;
                                    objFS.AdtSrvTax = 0;
                                    objFS.AdtTF = 0;
                                    objFS.AdtTds = 0;
                                    objFS.IATAComm = 0;
                                }
                            }
                            if ((searchInputs.IsCorp == true) && (searchInputs.RTF != true))
                            {
                                try
                                {
                                    //DataTable MGDT = new DataTable();
                                    //MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.AdtFare.ToString()));
                                    objFS.AdtMgtFee = 0;
                                    objFS.AdtSrvTax = 0;
                                }
                                catch { }
                            }
                            objFS.AdtEduCess = 0;
                            objFS.AdtHighEduCess = 0;
                            #endregion

                            //#region child
                            objFS.ChdTax = 0;
                            if (searchInputs.Child > 0)
                            {
                                objFS.ChdFar = "";
                                objFS.ChdCabin = "E";// "E";// t.COS;
                                objFS.ChdFarebasis = dr["fareBasis"].ToString();
                                objFS.AdtRbd = dr["RBD"].ToString();
                                objFS.ChdfareType = "";
                                objFS.ChdTax = 0;

                                var ChdCharges = 0;
                                #region CHDFR


                                objFS.ChdFSur = float.Parse(Math.Round(decimal.Parse(dr["YQ"].ToString()), 0).ToString());
                                objFS.ChdWO = float.Parse(Math.Round(decimal.Parse(dr["WO"].ToString()), 0).ToString());
                                objFS.ChdYR = float.Parse(Math.Round(decimal.Parse(dr["YR"].ToString()), 0).ToString());
                                objFS.ChdBFare = float.Parse(Math.Round(decimal.Parse(dr["Basicfare"].ToString()), 0).ToString());
                                objFS.ChdOT = float.Parse(Math.Round(decimal.Parse(dr["OT"].ToString()), 0).ToString());
                                objFS.ChdFare = float.Parse(Math.Round(decimal.Parse(dr["GROSS_Total"].ToString()), 0).ToString());
                                ChdTF = 0;
                                PROMODISCHD = 0;

                                #endregion
                                objFS.ChdBFare = objFS.ChdBFare - PROMODISCHD;
                                objFS.ChdFare = objFS.ChdFare - PROMODISCHD;
                                ChdTF = ChdTF - decimal.Parse(PROMODISCHD.ToString());


                                objFS.ChdTax = objFS.ChdFare - objFS.ChdBFare;
                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    srvChargeChd = 0;
                                }
                                catch { srvChargeChd = 0; }
                                #endregion



                                objFS.ChdOT = objFS.ChdOT + srvChargeChd;//srvCharge;
                                objFS.ChdTax = objFS.ChdTax + srvChargeChd;//srvCharge;
                                objFS.ChdFare = objFS.ChdFare + srvChargeChd; //srvCharge;








                                if (dr["Markup_Type"].ToString() == "Flat")
                                {
                                    objFS.CHDAdminMrk = float.Parse(dr["Admin_Markup"].ToString() == "" ? "0" : dr["Admin_Markup"].ToString());

                                }
                                else
                                {
                                    objFS.CHDAdminMrk = (objFS.ChdBFare * Convert.ToInt32(dr["Admin_Markup"].ToString() == "" ? "0" : dr["Admin_Markup"].ToString())) / 100;
                                }

                                objFS.CHDAgentMrk = float.Parse(dr["Markup"].ToString() == "" ? "0" : dr["Markup"].ToString());
                                // CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                // objFS.CHDAgentMrk = float.Parse(dr["Markup"].ToString() == "" ? "0" : dr["Markup"].ToString());   //CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                                CommDt.Clear();
                                STTFTDS.Clear();
                                if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                {
                                    //objFS.CHDAdminMrk = float.Parse(dr["Admin_Markup"].ToString() == "" ? "0" : dr["Admin_Markup"].ToString());

                                    objFS.ChdBFare = objFS.ChdBFare + objFS.CHDAdminMrk;
                                    objFS.ChdFare = objFS.ChdFare + objFS.CHDAdminMrk;
                                }
                                if ((searchInputs.Trip.ToString() == JourneyType.D.ToString()) && (searchInputs.RTF != true))
                                {
                                    try
                                    {
                                        objFS.ChdDiscount1 = 0;
                                        objFS.ChdDiscount = 0;
                                        objFS.ChdCB = 0;
                                        objFS.ChdSrvTax = 0;
                                        objFS.ChdTF = 0;
                                        objFS.ChdTds = 0;
                                    }
                                    catch (Exception ex)
                                    {
                                        objFS.ChdDiscount1 = 0;
                                        objFS.ChdDiscount = 0;
                                        objFS.ChdCB = 0;
                                        objFS.ChdSrvTax = 0;
                                        objFS.ChdTF = 0;
                                        objFS.ChdTds = 0;
                                    }
                                }
                                else
                                {
                                    objFS.ChdDiscount1 = 0;
                                    objFS.ChdDiscount = 0;
                                    objFS.ChdCB = 0;
                                    objFS.ChdSrvTax = 0;
                                    objFS.ChdTF = 0;
                                    objFS.ChdTds = 0;
                                }
                                if ((searchInputs.IsCorp == true) && (searchInputs.RTF != true))
                                {
                                    try
                                    {

                                        objFS.ChdMgtFee = 0;
                                        objFS.ChdSrvTax = 0;
                                    }
                                    catch { }
                                }
                                objFS.ChdEduCess = 0;
                                objFS.ChdHighEduCess = 0;
                                objFS.IATAComm = 0;
                            }
                            else
                            {
                                objFS.ChdBFare = 0;
                                objFS.ChdFare = 0;
                                objFS.ChdFSur = 0;
                                objFS.ChdWO = 0;
                                objFS.ChdIN = 0;
                                objFS.ChdJN = 0;
                                objFS.ChdYR = 0;
                                objFS.ChdOT = 0;
                                objFS.ChdTax = 0;
                                objFS.ChdDiscount = 0;
                                objFS.ChdCB = 0;
                                objFS.ChdSrvTax = 0;
                                objFS.ChdTF = 0;
                                objFS.ChdTds = 0;
                                objFS.ChdEduCess = 0;
                                objFS.ChdHighEduCess = 0;

                            }

                            #endregion



                            objFS.ElectronicTicketing = "";

                            objFS.InfCabin = "";
                            objFS.InfFarebasis = "";
                            objFS.InfRbd = "";
                            objFS.InfFareType = "";
                            objFS.InfTax = 0;
                            #region Infant
                            if (searchInputs.Infant > 0)
                            {


                                objFS.InfBfare = float.Parse(Math.Round(decimal.Parse(dr["Basicfare"].ToString()), 0).ToString());
                                objFS.InfOT = float.Parse(Math.Round(decimal.Parse(dr["OT"].ToString()), 0).ToString());
                                objFS.InfFare = float.Parse(Math.Round(decimal.Parse(dr["GROSS_Total"].ToString()), 0).ToString());

                                objFS.InfTax = objFS.InfFare - objFS.InfBfare;

                                if (objFS.InfBfare > 0)
                                {
                                    if (objFS.InfFare > objFS.InfTax)
                                        objFS.InfBfare = float.Parse(Math.Round(objFS.InfFare - objFS.InfTax).ToString());
                                    else
                                        objFS.InfBfare = float.Parse(Math.Round(objFS.InfTax - objFS.InfFare).ToString());
                                    objFS.InfFare = float.Parse(Math.Round(objFS.InfFare).ToString());
                                    objFS.InfTax = float.Parse(Math.Round(objFS.InfTax).ToString());
                                    objFS.InfOT = objFS.InfTax;
                                    InfTF = decimal.Parse(Math.Round(objFS.InfFare).ToString());
                                }
                                else
                                {
                                    objFS.InfFare = float.Parse(Math.Round(objFS.InfBfare + objFS.InfTax).ToString());
                                    objFS.InfBfare = float.Parse(Math.Round(objFS.InfBfare).ToString());
                                    objFS.InfTax = float.Parse(Math.Round(objFS.InfTax).ToString());
                                    objFS.InfOT = objFS.InfTax;
                                    InfTF = decimal.Parse(Math.Round(objFS.InfFare).ToString());
                                }
                                objFS.InfFSur = 0;
                                objFS.InfIN = 0;
                                objFS.InfJN = 0;
                                //objFS.InfOT = 0;
                                objFS.InfQ = 0;

                                if (searchInputs.IsCorp == true)
                                {
                                    try
                                    {

                                        objFS.InfMgtFee = 0;
                                        objFS.InfSrvTax = 0;
                                    }
                                    catch { }
                                }
                            }
                            else
                            {
                                objFS.InfFare = 0;
                                objFS.InfBfare = 0;
                                objFS.InfFSur = 0;
                                objFS.InfIN = 0;
                                objFS.InfJN = 0;
                                objFS.InfOT = 0;
                                objFS.InfQ = 0;
                            }
                            #endregion

                            objFS.OriginalTF = float.Parse((AdtTF * objFS.Adult + ChdTF * objFS.Child).ToString());//Without Infant Fare
                            objFS.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
                            objFS.TotalTax = (objFS.AdtTax * objFS.Adult) + (objFS.ChdTax * objFS.Child) + (objFS.InfTax * objFS.Infant);
                            objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
                            objFS.TotalFare = (objFS.AdtFare * objFS.Adult) + (objFS.ChdFare * objFS.Child) + (objFS.InfFare * objFS.Infant);
                            objFS.STax = (objFS.AdtSrvTax * objFS.Adult) + (objFS.ChdSrvTax * objFS.Child) + (objFS.InfSrvTax * objFS.Infant);
                            objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
                            objFS.TotDis = (objFS.AdtDiscount * objFS.Adult) + (objFS.ChdDiscount * objFS.Child);
                            objFS.TotCB = (objFS.AdtCB * objFS.Adult) + (objFS.ChdCB * objFS.Child);
                            objFS.TotTds = (objFS.AdtTds * objFS.Adult) + (objFS.ChdTds * objFS.Child);// +objFS.InfTds;
                            objFS.TotMrkUp = (objFS.ADTAdminMrk * objFS.Adult) + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAdminMrk * objFS.Child) + (objFS.CHDAgentMrk * objFS.Child);
                            objFS.TotMgtFee = (objFS.AdtMgtFee * objFS.Adult) + (objFS.ChdMgtFee * objFS.Child) + (objFS.InfMgtFee * objFS.Infant);
                            if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                objFS.TotalFare = objFS.TotalFare + objFS.STax + objFS.TFee + objFS.TotMgtFee + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child);
                            else
                                objFS.TotalFare = objFS.TotalFare + objFS.TotMrkUp + objFS.STax + objFS.TFee + objFS.TotMgtFee;
                            objFS.NetFare = (objFS.TotalFare + objFS.TotTds) - (objFS.TotDis + objFS.TotCB + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child));
                            //objFS.OperatingCarrier = p.opreatingcarrier;
                            objFS.NetFareSRTF = objFS.NetFare;
                            //int hr = int.Parse(p.jrnyTime) / 60;
                            //int min = int.Parse(p.jrnyTime) % 60;
                            //objFS.TotDur = hr + " hrs " + min + " min";

                            objFS.Stops = legs - 1 + "-Stop";
                            objFS.FareDet = "";
                            objFS.Trip = searchInputs.Trip.ToString();
                            // objFS.FType = searchInputs.TripType.ToString();
                            if ((searchInputs.RTF == true) || (searchInputs.Trip == Trip.I))
                            {
                                if (schd == 0)
                                {
                                    objFS.Flight = "1";
                                    if (searchInputs.RTF == true)
                                        objFS.FType = "RTF";
                                    else
                                        objFS.FType = "OutBound";
                                    objFS.TripType = "O";
                                }
                                else
                                {
                                    objFS.Flight = "2";
                                    if (searchInputs.RTF == true)
                                        objFS.FType = "RTF";
                                    else
                                        objFS.FType = "InBound";
                                    objFS.TripType = "R";
                                }
                            }
                            else
                            {
                                objFS.Flight = "1";
                                objFS.FType = "OutBound";
                                objFS.TripType = "O";
                            }
                            objFS.IsCorp = searchInputs.IsCorp;
                            objFS.Provider = "FDD";
                            srvCharge = srvChargeAdt + srvChargeChd;  //06-03-2018  Add MISC CHARGES PAX WISE
                            objFS.OriginalTT = srvCharge;
                            objFS.SearchId = "FDD";
                            FlightList.Add(objFS);
                        }
                        LNO = LNO + 1;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


            CacheList.Add(FlightList);
            //CacheList.Add(objFS);

            return CacheList;
        }

        private ArrayList Avilability(FlightSearch searchInputs, DataSet FltList, string CrdType)
        {
            ArrayList ReturnList = new ArrayList();
            try
            {
                FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
                List<FltSrvChargeList> SrvChargeList; List<MISCCharges> MiscList = new List<MISCCharges>();
                DataTable dtAgentMarkup = new DataTable();
                DataSet MarkupDs = new DataSet();
                SrvChargeList = Data.GetSrvChargeInfo(searchInputs.Trip.ToString(), ConnStr);// Get Data From DB or Cache
                dtAgentMarkup = Data.GetMarkup(ConnStr, searchInputs.UID.ToString(), searchInputs.DISTRID.ToString(), searchInputs.Trip.ToString(), "TA");
                dtAgentMarkup.TableName = "AgentMarkUp";
                MarkupDs.Tables.Add(dtAgentMarkup);
                try
                {
                    MiscList = objFltComm.GetMiscCharges(searchInputs.Trip.ToString(), "ALL", searchInputs.UID.ToString(), searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                }
                catch (Exception ex2)
                { }


                ReturnList = AvilabilityList(FltList, SrvChargeList, MarkupDs, MiscList, searchInputs.AgentType, searchInputs.TDS, searchInputs.RTF, searchInputs, CrdType);

            }
            catch (Exception ex) { }
            return ReturnList;

        }


        private ArrayList AvilabilityR(FlightSearch searchInputs, DataSet FltList, string CrdType)
        {
            ArrayList ReturnList = new ArrayList();
            try
            {
                FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
                List<FltSrvChargeList> SrvChargeList; List<MISCCharges> MiscList = new List<MISCCharges>();
                DataTable dtAgentMarkup = new DataTable();
                DataSet MarkupDs = new DataSet();
                SrvChargeList = Data.GetSrvChargeInfo(searchInputs.Trip.ToString(), ConnStr);// Get Data From DB or Cache
                dtAgentMarkup = Data.GetMarkup(ConnStr, searchInputs.UID.ToString(), searchInputs.DISTRID.ToString(), searchInputs.Trip.ToString(), "TA");
                dtAgentMarkup.TableName = "AgentMarkUp";
                MarkupDs.Tables.Add(dtAgentMarkup);
                try
                {
                    MiscList = objFltComm.GetMiscCharges(searchInputs.Trip.ToString(), "ALL", searchInputs.UID.ToString(), searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                }
                catch (Exception ex2)
                { }

                ReturnList = AvilabilityListSpl(FltList, SrvChargeList, MarkupDs, MiscList, searchInputs.AgentType, searchInputs.TDS, searchInputs.RTF, searchInputs, CrdType);
            }
            catch (Exception ex) { }
            return ReturnList;

        }
        private string SequenceSearchKey(string Org, string Dest, string Depdate, string RetDate, int Adt, int Chd, int Inf, bool GdsRtf, bool LccRtf, string Cabin)
        {
            string SKey = "";
            if (GdsRtf == true || LccRtf == true)
            {

                SKey = Utility.Left(Org.Trim(), 3) + "/" + Utility.Left(Dest.Trim(), 3) + "/" + Utility.Left(Org.Trim(), 3) + "/" + Depdate.Trim() + "/" + RetDate.Trim() + "/" + Adt.ToString() + "/" + Chd.ToString() + "/" + Inf.ToString() + "/" + GdsRtf + "/" + LccRtf + "/" + Cabin;
            }
            else
            {
                SKey = Utility.Left(Org.Trim(), 3) + "/" + Utility.Left(Dest.Trim(), 3) + "/" + Depdate.Trim() + "/" + RetDate.Trim() + "/" + Adt.ToString() + "/" + Chd.ToString() + "/" + Inf.ToString() + "/" + GdsRtf + "/" + LccRtf + "/" + Cabin;
            }
            return SKey;
        }
        public ArrayList GetSequenceFareList(FlightSearch searchInputs, string CrdType, string ac = "")
        {
            DataSet Cacheds = new DataSet();
            DataSet RCacheds = new DataSet();
            ArrayList CList = new ArrayList();
            FlightCommonBAL objFCBAL = new FlightCommonBAL(ConnStr);
            string Org = searchInputs.HidTxtDepCity.Trim();
            string Dest = searchInputs.HidTxtArrCity.Trim();
            string Sector = Utility.Left(Org.Trim(), 3) + ":" + Utility.Left(Dest.Trim(), 3);
            string RSector = Utility.Left(Dest.Trim(), 3) + ":" + Utility.Left(Org.Trim(), 3);
            string SearchDate = Utility.Right(searchInputs.DepDate.Trim(), 4) + "/" + Utility.Mid(searchInputs.DepDate.Trim(), 3, 2) + "/" + Utility.Left(searchInputs.DepDate.Trim(), 2);
            string RetDate = Utility.Right(searchInputs.RetDate.Trim(), 4) + "/" + Utility.Mid(searchInputs.RetDate.Trim(), 3, 2) + "/" + Utility.Left(searchInputs.RetDate.Trim(), 2);
            string UrlO = ""; string UrlR = ""; bool RoundTrip = false; string AirlineCode = "";
            try
            {

                AirlineCode = ac;


                if (searchInputs.TripType.ToString() == "RoundTrip" && searchInputs.Trip.ToString() == "D" && searchInputs.RTF == false && searchInputs.GDSRTF == false)
                {
                    Cacheds = objFCBAL.GetSequenceTable(Sector, SearchDate, AirlineCode, RetDate, RoundTrip);
                    RCacheds = objFCBAL.GetSequenceTable(RSector, RetDate, AirlineCode, RetDate, RoundTrip);
                    RoundTrip = true;
                }
                else
                {
                    Cacheds = objFCBAL.GetSequenceTable(Sector, SearchDate, AirlineCode, RetDate, RoundTrip);
                }

                if (Cacheds.Tables.Count > 0)
                {
                    ArrayList OBJFS_O = new ArrayList();
                    ArrayList OBJFS_R = new ArrayList();

                    string CacheJson = "";
                    ArrayList ChacheList = new ArrayList();
                    List<FlightSearchResults> OBJFS = new List<FlightSearchResults>();
                    ArrayList objFltResultList = new ArrayList();
                    CacheFinalList CacheFinalList = new CacheFinalList();
                    List<CacheList> clsRetList = new List<CacheList>();
                    //if (RoundTrip == true && searchInputs.Trip == Trip.D)
                    //{

                    //        //OBJFS_O = AvilabilityR(searchInputs, Cacheds, CrdType);
                    //        //objFltResultList.Add(AddFlightKey((List<FlightSearchResults>)OBJFS_O[0], true));
                    //        //CList = objFltResultList;

                    //        //OBJFS_O = Avilability(searchInputs, Cacheds, CrdType);
                    //        //OBJFS_R = Avilability(searchInputs, RCacheds, CrdType);
                    //        //objFltResultList.Add(AddFlightKey((List<FlightSearchResults>)OBJFS_O[0], false));
                    //        //objFltResultList.Add(AddFlightKey((List<FlightSearchResults>)OBJFS_R[0], false));

                    //       // CList = objFltResultList;

                    //    OBJFS_O = AvilabilityR(searchInputs, Cacheds, CrdType);
                    //    objFltResultList.Add(AddFlightKey((List<FlightSearchResults>)OBJFS_O[0], true));
                    //    CList = objFltResultList;
                    //}
                    if (searchInputs.TripType.ToString() == "RoundTrip" && (searchInputs.Trip == Trip.I))
                    {
                        Thread.Sleep(200);
                        OBJFS_O = AvilabilityR(searchInputs, Cacheds, CrdType);
                        objFltResultList.Add(AddFlightKey((List<FlightSearchResults>)OBJFS_O[0], true));
                        CList = objFltResultList;


                    }
                    else
                    {
                        if (searchInputs.TripType.ToString() != "RoundTrip")
                        {
                            Thread.Sleep(200);
                            OBJFS_O = Avilability(searchInputs, Cacheds, CrdType);
                            // CList.Add(OBJFS_O);
                            //  CList = OBJFS_O;
                            CList.Add(AddFlightKey((List<FlightSearchResults>)OBJFS_O[0], false));
                        }
                    }

                }
            }

            catch (Exception ex) { }

            return CList;
        }


        public ArrayList GetSequenceFareListonResult(FlightSearch searchInputs, string CrdType, string ac = "")
        {
            DataSet Cacheds = new DataSet();
            DataSet RCacheds = new DataSet();
            ArrayList CList = new ArrayList();
            FlightCommonBAL objFCBAL = new FlightCommonBAL(ConnStr);
            string Org = searchInputs.HidTxtDepCity.Trim();
            string Dest = searchInputs.HidTxtArrCity.Trim();
            string Sector = Utility.Left(Org.Trim(), 3) + ":" + Utility.Left(Dest.Trim(), 3);
            string RSector = Utility.Left(Dest.Trim(), 3) + ":" + Utility.Left(Org.Trim(), 3);
            string SearchDate = Utility.Right(searchInputs.DepDate.Trim(), 4) + "/" + Utility.Mid(searchInputs.DepDate.Trim(), 3, 2) + "/" + Utility.Left(searchInputs.DepDate.Trim(), 2);
            string RetDate = Utility.Right(searchInputs.RetDate.Trim(), 4) + "/" + Utility.Mid(searchInputs.RetDate.Trim(), 3, 2) + "/" + Utility.Left(searchInputs.RetDate.Trim(), 2);
            string UrlO = ""; string UrlR = ""; bool RoundTrip = false; string AirlineCode = "";
            try
            {

                AirlineCode = ac;


                if (searchInputs.TripType.ToString() == "RoundTrip" && searchInputs.Trip.ToString() == "D" && searchInputs.RTF == false && searchInputs.GDSRTF == false)
                {
                    Cacheds = objFCBAL.GetSequenceTablebydate(Sector, SearchDate, AirlineCode, RetDate, RoundTrip, searchInputs.TripType.ToString(), searchInputs.Trip.ToString());
                    RCacheds = objFCBAL.GetSequenceTablebydate(RSector, RetDate, AirlineCode, RetDate, RoundTrip,searchInputs.TripType.ToString(), searchInputs.Trip.ToString());
                    RoundTrip = true;
                }
                else
                {
                    Cacheds = objFCBAL.GetSequenceTablebydate(Sector, SearchDate, AirlineCode, RetDate, RoundTrip, searchInputs.TripType.ToString(), searchInputs.Trip.ToString());
                }

                if (Cacheds.Tables.Count > 0)
                {
                    ArrayList OBJFS_O = new ArrayList();
                    ArrayList OBJFS_R = new ArrayList();

                    string CacheJson = "";
                    ArrayList ChacheList = new ArrayList();
                    List<FlightSearchResults> OBJFS = new List<FlightSearchResults>();
                    ArrayList objFltResultList = new ArrayList();
                    CacheFinalList CacheFinalList = new CacheFinalList();
                    List<CacheList> clsRetList = new List<CacheList>();
                    if (RoundTrip == true && searchInputs.Trip == Trip.D)
                    {

                        //OBJFS_O = AvilabilityR(searchInputs, Cacheds, CrdType);
                        //objFltResultList.Add(AddFlightKey((List<FlightSearchResults>)OBJFS_O[0], true));
                        //CList = objFltResultList;

                        OBJFS_O = Avilability(searchInputs, Cacheds, CrdType);
                        OBJFS_R = Avilability(searchInputs, RCacheds, CrdType);
                        objFltResultList.Add(AddFlightKey((List<FlightSearchResults>)OBJFS_O[0], false));
                        objFltResultList.Add(AddFlightKey((List<FlightSearchResults>)OBJFS_R[0], false));

                        CList = objFltResultList;

                        // OBJFS_O = AvilabilityR(searchInputs, Cacheds, CrdType);
                        // objFltResultList.Add(AddFlightKey((List<FlightSearchResults>)OBJFS_O[0], true));
                        // CList = objFltResultList;
                    }
                    if (searchInputs.TripType.ToString() == "RoundTrip" && (searchInputs.Trip == Trip.I))
                    {
                        Thread.Sleep(200);
                        OBJFS_O = AvilabilityR(searchInputs, Cacheds, CrdType);
                        objFltResultList.Add(AddFlightKey((List<FlightSearchResults>)OBJFS_O[0], true));
                        CList = objFltResultList;


                    }
                    else
                    {
                        if (searchInputs.TripType.ToString() != "RoundTrip")
                        {
                            Thread.Sleep(200);
                            OBJFS_O = Avilability(searchInputs, Cacheds, CrdType);
                            // CList.Add(OBJFS_O);
                            //  CList = OBJFS_O;
                            CList.Add(AddFlightKey((List<FlightSearchResults>)OBJFS_O[0], false));
                        }
                    }

                }
            }

            catch (Exception ex) { }

            return CList;
        }

        public List<FlightSearchResults> AddFlightKey(List<FlightSearchResults> FltSearchResult, bool isSplFare)
        {

            if (FltSearchResult.Count > 0)
            {
                List<int> linnum = FltSearchResult.Select(x => x.LineNumber).Distinct().OrderBy(x => x).ToList();

                if (isSplFare)
                {

                    for (int i = 0; i < linnum.Count; i++)
                    {
                        StringBuilder key1 = new StringBuilder();
                        StringBuilder key2 = new StringBuilder();
                        FltSearchResult.Where(x => x.LineNumber == linnum[i] && x.Flight == "1").ToList().ForEach(x =>
                        {
                            key1.Append("FDD");
                            key1.Append(x.FlightIdentification.ToString().Trim());
                            key1.Append(x.AdtCabin.ToString().Trim());
                            key1.Append(x.ValiDatingCarrier.ToString().Trim());
                            key1.Append(x.Stops.ToString().Trim());
                            key1.Append(x.DepartureDate.ToString().Trim());
                            key1.Append(x.ArrivalDate.ToString().Trim());
                            key1.Append(x.DepartureTime.ToString().Trim());
                            key1.Append(x.ArrivalTime.ToString().Trim());
                            key1.Append(x.AdtFar.ToString().Trim());
                            key1.Append(x.IsSMEFare.ToString().Trim());
                            key1.Append(x.IsBagFare.ToString().Trim());

                        });

                        FltSearchResult.Where(x => x.LineNumber == linnum[i] && x.Flight == "1").ToList().ForEach(x =>
                        {
                            x.SubKey = key1.ToString();
                        });


                        FltSearchResult.Where(x => x.LineNumber == linnum[i] && x.Flight == "2").ToList().ForEach(x =>
                        {
                            key2.Append("FDD");
                            key2.Append(x.FlightIdentification.ToString().Trim());
                            key2.Append(x.AdtCabin.ToString().Trim());
                            key2.Append(x.ValiDatingCarrier.ToString().Trim());
                            key2.Append(x.Stops.ToString().Trim());
                            key2.Append(x.DepartureDate.ToString().Trim());
                            key2.Append(x.ArrivalDate.ToString().Trim());
                            key2.Append(x.DepartureTime.ToString().Trim());
                            key2.Append(x.ArrivalTime.ToString().Trim());
                            key2.Append(x.AdtFar.ToString().Trim());
                            key2.Append(x.IsSMEFare.ToString().Trim());
                            key2.Append(x.IsBagFare.ToString().Trim());

                        });

                        FltSearchResult.Where(x => x.LineNumber == linnum[i] && x.Flight == "2").ToList().ForEach(x =>
                        {
                            x.SubKey = key2.ToString();
                        });



                        StringBuilder key = new StringBuilder();

                        FltSearchResult.Where(x => x.LineNumber == linnum[i]).ToList().ForEach(x =>
                        {
                            key.Append("FDD");
                            key.Append(x.FlightIdentification.ToString().Trim());
                            key.Append(x.AdtCabin.ToString().Trim());
                            key.Append(x.ValiDatingCarrier.ToString().Trim());
                            key.Append(x.Stops.ToString().Trim());
                            key.Append(x.DepartureDate.ToString().Trim());
                            key.Append(x.ArrivalDate.ToString().Trim());
                            key.Append(x.DepartureTime.ToString().Trim());
                            key.Append(x.ArrivalTime.ToString().Trim());
                            key.Append(x.AdtFar.ToString().Trim());
                            key.Append(x.IsSMEFare.ToString().Trim());
                            key.Append(x.IsBagFare.ToString().Trim());

                        });

                        FltSearchResult.Where(x => x.LineNumber == linnum[i]).ToList().ForEach(x =>
                        {
                            x.MainKey = key.ToString();
                        });

                    }


                }
                else
                {

                    for (int i = 0; i < linnum.Count; i++)
                    {
                        StringBuilder key = new StringBuilder();

                        FltSearchResult.Where(x => x.LineNumber == linnum[i]).ToList().ForEach(x =>
                        {
                            key.Append("FDD");
                            key.Append(x.FlightIdentification.ToString().Trim());
                            key.Append(x.AdtCabin.ToString().Trim());
                            key.Append(x.ValiDatingCarrier.ToString().Trim());
                            key.Append(x.Stops.ToString().Trim());
                            key.Append(x.DepartureDate.ToString().Trim());
                            key.Append(x.ArrivalDate.ToString().Trim());
                            key.Append(x.DepartureTime.ToString().Trim());
                            key.Append(x.ArrivalTime.ToString().Trim());
                            key.Append(x.AdtFar.ToString().Trim());
                            key.Append(x.IsSMEFare.ToString().Trim());
                            key.Append(x.IsBagFare.ToString().Trim());
                        });

                        FltSearchResult.Where(x => x.LineNumber == linnum[i]).ToList().ForEach(x =>
                        {
                            x.MainKey = key.ToString();
                            x.SubKey = key.ToString();
                        });
                    }


                }

            }


            SetNullValueToEmptyString(FltSearchResult);

            return FltSearchResult;
        }
        public void SetNullValueToEmptyString<T>(IList<T> data)
        {
            System.ComponentModel.PropertyDescriptorCollection properties =
              System.ComponentModel.TypeDescriptor.GetProperties(typeof(T));

            foreach (T item in data)
            {

                foreach (System.ComponentModel.PropertyDescriptor prop in properties)
                {
                    if (prop.GetValue(item) == null)
                    {
                        prop.SetValue(item, "");
                    }
                    #region Fare Rule
                    if (prop.Name == "FareRule" || prop.Name == "FareDet" || prop.Name == "AdtBreakPoint" || prop.Name == "ChdBreakPoint" || prop.Name == "InfBreakPoint")
                        prop.SetValue(item, "");
                    #endregion
                }


            }
        }

        private string getCityName(string city, List<FlightCityList> CityList)
        {
            string city1 = "";
            try
            {
                city1 = ((from ct in CityList where ct.AirportCode == city select ct).ToList())[0].City;
            }
            catch { city1 = city; }
            return city1;
        }

        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;
            decimal originalDis = 0;
            Hashtable STHT = new Hashtable();
            try
            {
                List<FltSrvChargeList> StNew = (from st in SrvchargeList where st.AirlineCode == VC select st).ToList();
                if (StNew.Count > 0)
                {
                    STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                    TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                    IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                }
                //STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                //TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                //IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                originalDis = decimal.Parse(Dis.ToString()) - decimal.Parse(STHT["TFee"].ToString());
                STHT.Add("STax", Math.Round(((originalDis * STaxP) / 100), 0));
                //STHT.Add("Tds", Math.Round(((originalDis * decimal.Parse(TDS)) / 100), 0));
                STHT.Add("Tds", Math.Round(((double.Parse(Dis.ToString()) - double.Parse(STHT["STax"].ToString())) * double.Parse(TDS)) / 100, 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }
        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray;
            double mrkamt = 0;
            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");

                if (!(airMrkArray != null && airMrkArray.Length > 0))
                {
                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

                }

                if (airMrkArray.Length > 0)
                {

                    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    {
                        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    }
                    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    }

                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return float.Parse(mrkamt.ToString());
        }
    }
}