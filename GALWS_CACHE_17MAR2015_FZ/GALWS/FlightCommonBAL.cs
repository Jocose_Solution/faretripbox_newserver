﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using STD.Shared;
using STD.DAL;
using System.Data;
using System.Collections;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Net.Cache;
using ITZERRORLOG;
using GALWS;
using System.Web;
using BAL;

namespace STD.BAL
{
    public class FlightCommonBAL
    {
        string ConnStr = "";
        public FlightCommonBAL()
        { }
        public FlightCommonBAL(string ConnectionString)
        {
            ConnStr = ConnectionString;
        }
        /// <summary>
        /// Fetches Datatables Based on OrderId and tablename(FLT_Details,PaxDetails,etc)
        /// These Datatables are needed to Genretae Report(HTML table)
        /// <param name="OrderId"></param>
        /// <param name="table"></param>
        /// <returns></returns>

        //        public string Create_Flt_Html_table(DataSet obj, string OrderId, int PaxID, string OWNER_ID)
        //        {
        //            string SearchVal = "";
        //            string my_table = "";
        //            try
        //            {
        //                            LoginBAL objLoginBAL = new LoginBAL();
        //            //SP_DIST_FLT_GETMARKUP - Get Rows from Markup Table where APPLIEDON in (@DistrID,'ALL')
        //                #region DEFINE USERTYPE

        //                SearchVal = objLoginBAL.getLoginType(OWNER_ID);

        //                #endregion
        //                //CREATE HEADER
        //                my_table = "<table width='100%' border='0'  cellspacing='0'  cellpadding='0'>";//MAIN TABLE
        //                #region HeaderDetails  - COMMON
        //                my_table += "<tr>";
        //                my_table += "<td colspan='2' align='center'   style='background-color: #1C589D ;  color: #FFFFFF; font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; height: 25px;padding-left:10px;border: thin solid #70A0CD'>Booking Reference No. " + OrderId + "</td>";
        //                my_table += "</tr>";
        //                my_table += "<tr>";
        //                my_table += "<td style='border: thin solid #999999;padding-left:20px;'>";
        //                my_table += "<table border='0' cellpadding='0' cellspacing='0' width='100%'  >";//TABLE 1
        //                my_table += "<tr>";
        //                my_table += "&nbsp;&nbsp;<td valign='top' background='~/images/nologo.png' style='background-repeat: no-repeat;width:100px;height:70px;'><img width='100' height='70' src='" + obj.Tables[0].Rows[0]["AGENCYLOGO"].ToString() + "' alt='' /> ";
        //                //For agency logo
        //                my_table += "<td style='padding:10px' align='right' >";
        //                my_table += "<table border='0' cellpadding='0' cellspacing='0' width='100%' >";//TABLE 2
        //                my_table += "<tr>";
        //                my_table += "<td  align='right' style='font-family: arial, Helvetica, sans-serif; font-size: 12px;font-weight:bold; color: #000000; height: 20px;padding-left:20px'>Agency Information</td>";
        //                //my_table += "<td> <img src='CSS/images/logo.png' /></td>";
        //                my_table += "</tr>";
        //                my_table += "<tr>";
        //                my_table += "<td align='right' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:20px'>" + obj.Tables[0].Rows[0]["AGENCYNAME"].ToString() + "</td>";
        //                //my_table += "<td> <img src='CSS/images/logo.png' /></td>";
        //                my_table += "</tr>";
        //                my_table += "<tr>";
        //                my_table += "<td align='right' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:20px'>" + obj.Tables[0].Rows[0]["ADDRESS"].ToString() + "</td>";
        //                my_table += "</tr>";
        //                my_table += "<td align='right' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:20px'>" + obj.Tables[0].Rows[0]["MOBILE"].ToString() + "</td>";
        //                my_table += "</tr>";
        //                my_table += "<tr>";
        //                my_table += "<td align='right' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:20px'>" + obj.Tables[0].Rows[0]["EMAIL"].ToString() + "</td>";
        //                my_table += "</tr>";
        //                my_table += "</table>"; //TABLE 2 END
        //                my_table += "</td>";
        //                my_table += "</tr>";
        //                my_table += "</table>"; //TABLE 1 END
        //                //HEADER COMPLETED
        //                #endregion
        //                #region PNRDETAILS  - COMMON
        //                //START PNR DETAILS
        //                //my_table += "</td>";
        //                //my_table += "</tr>";
        //                my_table += "<tr>";
        //                my_table += "<td style='padding: 10px;border: thin solid #999999'>";
        //                my_table += "<table border='0' cellpadding='0' cellspacing='0' width='100%' align='center' >";//TABLE 3
        //                my_table += "<tr>";
        //                my_table += "<td  style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'>GDSPNR</td>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + obj.Tables[1].Rows[0]["GDSPNR"].ToString() + "</td>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'>Booking Date</td>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + obj.Tables[1].Rows[0]["CREATEDATE"].ToString() + "</td>";
        //                my_table += "</tr>";
        //                my_table += "<tr>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'>AirlinePNR</td>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + obj.Tables[1].Rows[0]["AIRLINEPNR"].ToString() + " </td>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'>Status</td>";
        //                if (obj.Tables[1].Rows[0]["STATUS"].ToString().Trim().ToUpper() == "CONFIRM")
        //                {
        //                    my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>PNR ON HOLD</td>";
        //                }
        //                else
        //                {
        //                    my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + obj.Tables[1].Rows[0]["STATUS"].ToString() + "</td>";
        //                }
        //                my_table += "</tr>";
        //                my_table += "</table>";//TABLE 3 END
        //                //////////////////////////////////////HEADER FOR TABLE COMPLETED//////////////////////////////////////////////
        //                #endregion
        //                #region TRAVELLER INFORMATION  - COMMON
        //                my_table += "</td>";
        //                my_table += "</tr>";
        //                my_table += "<tr>";
        //                my_table += "<td style='border: thin solid #999999;' >";
        //                my_table += "<table border='0' cellpadding='0' cellspacing='0' width='100%' align='center' >"; //TABLE 4 
        //                my_table += "<tr>";
        //                my_table += "<td colspan='3'  style='background-color: #1C589D; color: #ffffff; font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; height: 25px;padding-left:10px'>Traveller Information</td>";
        //                my_table += "</tr>";
        //                my_table += "<tr >";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;padding-left:10px;width:250px'>Passenger Name</td>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:250px'>Type</td>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'>Ticket No</td>";
        //                my_table += "</tr>";
        //                //SHOW REPORT 1 IF PAXID=(0 OR NOT PROVIDED)
        //                if (string.IsNullOrEmpty(PaxID.ToString()) || PaxID == 0)
        //                {
        //                    //if (OrderId == null)
        //                    //{
        //                    double GrandTotal = 0;
        //                    foreach (DataRow dr in obj.Tables[2].Rows)//FOR EACH PAX SHOW PAX DETAILS
        //                    {
        //                        my_table += "<tr>";
        //                        my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:10px;'>" + dr["NAME"].ToString() + "</td>";
        //                        my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + dr["PAXTYPE"].ToString() + "</td>";
        //                        if (obj.Tables[1].Rows[0]["STATUS"].ToString().Trim().ToUpper() != "CONFIRM")
        //                        {
        //                            my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + dr["TICKETNUMBER"].ToString() + "</td>";
        //                        }
        //                        else
        //                        {
        //                            //if (obj.Tables[1].Rows[0]["GDSPNR"].ToString().IndexOf("-FQ") > 0)
        //                            //{
        //                            if (obj.Tables[1].Rows[0]["GDSPNR"].ToString().Contains("-FQ"))
        //                            {
        //                                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>Pnr On Hold, Due to dynamic updation of fare and inventory class at airline’s end, the fare may have changed. Pls contact our call centre.</td>";
        //                            }
        //                            //else if (Strings.InStr(obj.Tables[1].Rows[0]["GdsPnr"].ToString(), "-BLOCK") > 0)
        //                            else if (obj.Tables[1].Rows[0]["GDSPNR"].ToString().Contains("-BLOCK"))
        //                            {
        //                                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>Pnr On Hold, Due to dynamic updation of fare and inventory class at airline’s end, the fare may have changed. Pls contact our call centre.</td>";
        //                            }
        //                            //else if (Strings.InStr(dtpnr.Rows[0]["GdsPnr"].ToString(), "-SPR") > 0)
        //                            else if (obj.Tables[1].Rows[0]["GDSPNR"].ToString().Contains("-SPR"))
        //                            {
        //                                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>Pnr On Hold, Pls contact our call centre.</td>";
        //                            }
        //                            else
        //                            {
        //                                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>Pnr On Hold, We are processing your ticket details.</td>";
        //                            }
        //                        }
        //                        my_table += "</tr>";
        //                    }
        //                }
        //                //SHOW REPORT 2 IF PAXID=(IS PROVIDED)
        //                else
        //                {
        //                    foreach (DataRow dr in obj.Tables[2].Rows)
        //                    {
        //                        my_table += "<tr>";
        //                        my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:10px;'>" + dr["NAME"].ToString() + "</td>";
        //                        my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + dr["PAXTYPE"].ToString() + "</td>";
        //                        my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + dr["TICKETNUMBER"].ToString() + "</td>";
        //                        my_table += "</tr>";
        //                    }
        //                }
        //                my_table += "</table>";//TABLE 4  END
        //                //END PAX DETAILS
        //                #endregion          
        //                #region Flight Information  - COMMON
        //                //START FLIGHT DETAILS
        //                my_table += "</td>";
        //                my_table += "</tr>";
        //                my_table += "<tr>";
        //                my_table += "<td style='border: thin solid #999999' >";
        //                my_table += "<table border='0' cellpadding='0' cellspacing='0' width='100%' align='center' >";
        //                my_table += "<tr>";
        //                my_table += "<td colspan='6'  style='background-color: #1C589D; color: #ffffff; font-family: arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; height: 25px;padding-left:10px'>Flight Information</td>";
        //                my_table += "</tr>";
        //                my_table += "<tr>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;padding-left:10px'>From - To</td>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px; '></td>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'>Depart Date</td>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;'>DepTime</td>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;'>ArrTime</td>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;'>Aircraft</td>";
        //                my_table += "</tr>";
        //                foreach (DataRow dr1 in obj.Tables[4].Rows) //TBL_FLTDETAILS
        //                {
        //                    my_table += "<tr>";
        //                    my_table += "<td valign='bottom' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px;padding-left:10px'>" + dr1["DepAirName"].ToString() + " (" + dr1["DFROM"].ToString() + " ) - " + dr1["ARRAIRNAME"].ToString() + " (" + (dr1["ATO"].ToString()) + ")</td>";
        //                    my_table += "<td valign='bottom'>";
        //                    my_table += "<table>";
        //                    my_table += "<tr>";
        //                    my_table += "<td> <img src='http://www.springtravels.com/AirLogo/sm" + dr1["AIRCRAFT"].ToString() + ".gif' /></td>";
        //                    my_table += "</tr>";
        //                    my_table += "<tr>";
        //                    my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + dr1["AIRLINENAME"].ToString() + "(" + dr1["AIRLINECODE"].ToString() + "-" + dr1["FLTNUMBER"].ToString() + ")</td>";
        //                    my_table += "</tr>";
        //                    my_table += "</table>";
        //                    my_table += "</td>";
        //                    string depDate = "";
        //                    depDate = Utility.Left(dr1["DEPDATE"].ToString(), 2) + " " + Utility.datecon(Utility.Mid(dr1["DEPDATE"].ToString(), 3, 2)) + ", " + Utility.Right(dr1["DEPDATE"].ToString(), 2);
        //                    //depDate = dr1["DEPDATE"].ToString();
        //                    my_table += "<td valign='bottom' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + depDate + "</td>";
        //                    my_table += "<td valign='bottom' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + dr1["DEPTIME"].ToString() + "Hrs</td>";
        //                    my_table += "<td valign='bottom' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + dr1["ARRTIME"].ToString() + "Hrs</td>";
        //                    my_table += "<td valign='bottom' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + dr1["AIRCRAFT"].ToString() + "</td>";
        //                    my_table += "</tr>";
        //                }
        //                my_table += "</table>";
        //                #endregion 
        //                #region FARE INFORMATION- VARYING PART FOR 2 TYPE OF REPORTS
        //                my_table += "</td>";
        //                my_table += "</tr>";
        //                #region REPORT1- GENERAL
        //                //REPORT 1 FOR ALL PAX TYPE DETAILS
        //                if (string.IsNullOrEmpty(PaxID.ToString()) || PaxID == 0)
        //                {
        //                    double GrandTotal = 0;
        //                    my_table += "<tr>";
        //                    my_table += "<td colspan='3' style='background-color: #1C589D; color: #ffffff; font-family: arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; height: 25px;padding-left:10px'>Fare Information</td>";
        //                    my_table += "</tr>";
        //                    my_table += "<tr>";
        //                    my_table += "<td style='border: thin solid #999999'>";
        //                    my_table += "<table border='0' cellpadding='0' cellspacing='0' width='100%' align='center' >";
        //                    my_table += "<tr>";
        //                    my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:100px'>Pax Detail</td>";
        //                    my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:100px'>Base Fare</td>";
        //                    my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:100px'>Fuel Surcharge</td>";
        //                    my_table += "<td  align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:60px'>Tax</td>";
        //                    my_table += "<td  align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:60px'>STax</td>";
        //                    my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:80px'>Trans Fee</td>";
        //                    my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:80px'>Trans Charge</td>";
        //                    my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:80px''>TOTAL</td>";
        //                    my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:80px''></td>";
        //                    my_table += "</tr>";
        //                    DataTable dt = obj.Tables[5];
        //                    //int Adt = ;
        //                    foreach (DataRow dr2 in obj.Tables[5].Rows)
        //                    {

        //                        if (dr2["PAXTYPE"].ToString() == "ADT")
        //                        {
        //                            int Count = Convert.ToInt32(dt.Compute("Count(PAXTYPE)", ""));
        //                            double BaseFare = Convert.ToDouble(dr2["BaseFare"].ToString()) * Count;//Convert.ToDouble(dtadtcount.Rows[0][0].ToString()
        //                            double Fuel = Convert.ToDouble(dr2["Fuel"].ToString()) * Count ;
        //                            double Tax = Convert.ToDouble(dr2["Tax"].ToString()) * Count; //Convert.ToDouble(dtadtcount.Rows[0][0].ToString());
        //                            double ServiceTax = 0; 
        //                            if (SearchVal == "TA")
        //                            {
        //                                ServiceTax = Convert.ToDouble(dr2["TA_SERVICETAX"].ToString()) * Count;
        //                            }
        //                            else if (SearchVal == "DI")
        //                            {
        //                                ServiceTax = Convert.ToDouble(dr2["DI_SERVICETAX"].ToString()) * Count;
        //                            }
        //                            double TFee = Convert.ToDouble(dr2["TFee"].ToString()) * Count;// Convert.ToDouble(dtadtcount.Rows[0][0].ToString());
        //                            double TCharge = Convert.ToDouble(dr2["TCharge"].ToString()) * Count; // Convert.ToDouble(dtadtcount.Rows[0][0].ToString());
        //                            double Total = BaseFare + Fuel + Tax + ServiceTax + TFee + TCharge;// Convert.ToDouble(dtadtcount.Rows[0][0].ToString());
        //                            my_table += "<tr>";
        //                            my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + dr2["PAXTYPE"].ToString() + "</td>";
        //                            my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + BaseFare + "</td>";
        //                            my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + Fuel + "</td>";
        //                            my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + Tax + "</td>";
        //                            my_table += "<td  align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + ServiceTax + "</td>";
        //                            my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + TFee + "</td>";
        //                            my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + TCharge + "</td>";

        //                            my_table += "<td  align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #1C589D; height: 20px;font-weight:Bold'>" + Total + "</td>";
        //                            GrandTotal += Total * Count; // Convert.ToDouble(dtadtcount.Rows[0][0].ToString()
        //                            my_table += "</tr>";
        //                        }
        //                        if (dr2["PaxType"].ToString() == "CHD")
        //                        {
        //                            int Count = Convert.ToInt32(dt.Compute("Count(PAXTYPE)", ""));
        //                            double BaseFare = Convert.ToDouble(dr2["BaseFare"].ToString()) * Count;// Convert.ToDouble(dtchdcount.Rows[0][0].ToString());
        //                            double Fuel = Convert.ToDouble(dr2["Fuel"].ToString()) * Count;// Convert.ToDouble(dtchdcount.Rows[0][0].ToString());
        //                            double Tax = Convert.ToDouble(dr2["Tax"].ToString()) * Count; // Convert.ToDouble(dtchdcount.Rows[0][0].ToString());
        //                            double ServiceTax = 0;
        //                            if (SearchVal == "TA")
        //                            {
        //                                ServiceTax = Convert.ToDouble(dr2["TA_SERVICETAX"].ToString()) * Count;
        //                            }
        //                            else if (SearchVal == "DI")
        //                            {
        //                                ServiceTax = Convert.ToDouble(dr2["DI_SERVICETAX"].ToString()) * Count;
        //                            }
        //                            double TFee = Convert.ToDouble(dr2["TFee"].ToString()) * Count; // Convert.ToDouble(dtchdcount.Rows[0][0].ToString());
        //                            double TCharge = Convert.ToDouble(dr2["TCharge"].ToString()) * Count; // Convert.ToDouble(dtchdcount.Rows[0][0].ToString());
        //                            double Total = BaseFare + Fuel + Tax + ServiceTax + TFee + TCharge; //Convert.ToDouble(dtchdcount.Rows[0][0].ToString());
        //                            my_table += "<tr>";
        //                            my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + dr2["PAXTYPE"].ToString() + "</td>";
        //                            my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + BaseFare + "</td>";
        //                            my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + Fuel + "</td>";
        //                            my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + Tax + "</td>";
        //                            my_table += "<td  align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + ServiceTax + "</td>";
        //                            my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + TFee + "</td>";
        //                            my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + TCharge + "</td>";

        //                            my_table += "<td  align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #1C589D; height: 20px;font-weight:Bold'>" + Total + "</td>";
        //                            GrandTotal += Total * Count; //Convert.ToDouble(dtchdcount.Rows[0][0].ToString());
        //                            my_table += "</tr>";
        //                        }
        //                        if (dr2["PaxType"].ToString() == "INF")
        //                        {
        //                            int Count = Convert.ToInt32(dt.Compute("Count(PAXTYPE)", ""));
        //                            double BaseFare = Convert.ToDouble(dr2["BaseFare"].ToString()) * Count;// Convert.ToDouble(dtinfcount.Rows[0][0].ToString());
        //                            double Fuel = Convert.ToDouble(dr2["Fuel"].ToString()) * Count;// Convert.ToDouble(dtinfcount.Rows[0][0].ToString());
        //                            double Tax = Convert.ToDouble(dr2["Tax"].ToString()) * Count;  //Convert.ToDouble(dtinfcount.Rows[0][0].ToString());
        //                            double ServiceTax = 0;
        //                            if (SearchVal == "TA")
        //                            {
        //                                ServiceTax = Convert.ToDouble(dr2["TA_SERVICETAX"].ToString()) * Count;
        //                            }
        //                            else if (SearchVal == "DI")
        //                            {
        //                                ServiceTax = Convert.ToDouble(dr2["DI_SERVICETAX"].ToString()) * Count;
        //                            }
        //                            double TFee = 0; //Convert.ToDouble(dr2["TFee"].ToString()) * Convert.ToDouble(dtinfcount.Rows[0][0].ToString());
        //                            double TCharge = 0; //Convert.ToDouble(dr2["TCharge"].ToString()) * Convert.ToDouble(dtinfcount.Rows[0][0].ToString());
        //                            double Total = 0; // Convert.ToDouble(dr2["Total"].ToString()) * Convert.ToDouble(dtinfcount.Rows[0][0].ToString());
        //                            my_table += "<tr>";
        //                            my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + dr2["PaxType"].ToString() + "</td>";
        //                            my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + BaseFare + "</td>";
        //                            my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + Fuel + "</td>";
        //                            my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + Tax + "</td>";
        //                            my_table += "<td  align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + ServiceTax + "</td>";
        //                            my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + TFee + "</td>";
        //                            my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + TCharge + "</td>";

        //                            my_table += "<td  align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #1C589D; height: 20px;font-weight:Bold'>" + Total + "</td>";
        //                            GrandTotal += Total * Count; //Convert.ToDouble(dtinfcount.Rows[0][0].ToString())
        //                            my_table += "</tr>";

        //                        }
        //                    }
        //                    my_table += "<tr style='background-color: gray;'>";
        //                    my_table += "<td colspan='6' align='right' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'></td>";
        //                    my_table += "<td  align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #FFFF99; height: 20px'>GRAND TOTAL</td>";
        //                    my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #FFFF99; height: 20px'>" + GrandTotal + "</td>";
        //                    my_table += "<td align='center' style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'></td>";
        //                    my_table += "</tr>";
        //                    my_table += "</table>";
        //                    my_table += "</td>";
        //                    my_table += "</tr>";

        //                    my_table += "</table>";
        //                    my_table += "</td>";
        //                    my_table += "</tr>";

        //                }
        //                #endregion
        //                #region REPORT2 - PER PAX
        //                //REPORT 2 FOR PER PAX
        //                else
        //                {
        //                    //Fare Calculation
        //                    double BFare = Convert.ToDouble(obj.Tables[5].Rows[0]["BASEFARE"].ToString());
        //                    double Fuel = Convert.ToDouble(obj.Tables[5].Rows[0]["FUEL"].ToString());
        //                    double Tax = Convert.ToDouble(obj.Tables[5].Rows[0]["TAX"].ToString());
        //                    double TC = Convert.ToDouble(obj.Tables[5].Rows[0]["TCHARGE"].ToString());
        //                    double TFee = Convert.ToDouble(obj.Tables[5].Rows[0]["TFEE"].ToString());
        //                    double ServiceTax = 0;
        //                    if (SearchVal == "TA")
        //                    {
        //                        ServiceTax = Convert.ToDouble(obj.Tables[5].Rows[0]["TA_SERVICETAX"].ToString());
        //                    }
        //                    else if (SearchVal == "DI")
        //                    {
        //                        ServiceTax = Convert.ToDouble(obj.Tables[5].Rows[0]["DI_SERVICETAX"].ToString());
        //                    }
        //                    double Total = BFare + Fuel + Tax + TC + TFee + ServiceTax;
        //                    my_table += "<tr>";
        //                    my_table += "<td style='border: thin solid #999999' >";
        //                    my_table += "<table border='0' cellpadding='0' cellspacing='0' width='100%' align='center' >";
        //                    my_table += "<tr>";
        //                    my_table += "<td colspan='3' style='background-color: #1C589D; color: #ffffff; font-family: arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; height: 25px;padding-left:10px'>Fare Information</td>";
        //                    my_table += "</tr>";
        //                    my_table += "<tr>";
        //                    my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;width:250px;padding-left:10px'>Base Fare</td>";
        //                    my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + obj.Tables[5].Rows[0]["BASEFARE"].ToString() + "</td>";
        //                    my_table += "</tr>";
        //                    my_table += "<tr>";
        //                    my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;padding-left:10px'>Fuel Surcharge</td>";
        //                    my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + obj.Tables[5].Rows[0]["FUEL"].ToString() + "</td>";
        //                    my_table += "</tr>";
        //                    my_table += "<tr>";
        //                    my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;padding-left:10px'>Tax</td>";
        //                    my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + obj.Tables[5].Rows[0]["TAX"].ToString() + "</td>";
        //                    my_table += "</tr>";
        //                    my_table += "<tr>";
        //                    my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;padding-left:10px'>Transaction Fee</td>";
        //                    my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + obj.Tables[5].Rows[0]["TFEE"].ToString() + "</td>";
        //                    my_table += "</tr>";
        //                    my_table += "<tr>";
        //                    my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;padding-left:10px'>Transaction Charge</td>";
        //                    my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + obj.Tables[5].Rows[0]["TCHARGE"].ToString() + "</td>";
        //                    my_table += "</tr>";
        //                    my_table += "<tr>";
        //                    my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;padding-left:10px'>Service Tax</td>";
        //                    my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + ServiceTax + "</td>";
        //                    my_table += "</tr>";
        //                    my_table += "<tr>";
        //                    my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #1C589D; height: 20px;padding-left:10px'>TOTAL</td>";
        //                    my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #1C589D; height: 20px;font-weight:Bold'>" + Total + "</td>";
        //                    my_table += "</tr>";

        //                    double ResuCharge = 0;
        //                    double ResuServiseCharge = 0;
        //                    double ResuFareDiff = 0;

        //                    if (!string.IsNullOrEmpty(obj.Tables[1].Rows[0]["RESUID"].ToString()) && obj.Tables[1].Rows[0]["RESUID"].ToString() != null)
        //                    {
        //                        ResuCharge = Convert.ToDouble(obj.Tables[1].Rows[0]["RESUCHARGE"].ToString());
        //                        ResuServiseCharge = Convert.ToDouble(obj.Tables[1].Rows[0]["RESUSERVICECHARGE"].ToString());
        //                        ResuFareDiff = Convert.ToDouble(obj.Tables[1].Rows[0]["RESUFAREDIFF"].ToString());
        //                        my_table += "<tr>";
        //                        my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;padding-left:10px'>Reissue Charge</td>";
        //                        my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + ResuCharge + "</td>";
        //                        my_table += "</tr>";

        //                        my_table += "<tr>";
        //                        my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;padding-left:10px'>Reissue Srv. Charge</td>";
        //                        my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + ResuServiseCharge + "</td>";
        //                        my_table += "</tr>";

        //                        my_table += "<tr>";
        //                        my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px;padding-left:10px'>Reissue Fare Diff.</td>";
        //                        my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #000000; height: 20px'>" + ResuFareDiff + "</td>";
        //                        my_table += "</tr>";

        //                    }

        //                    double GTotal = 0;
        //                    if (!string.IsNullOrEmpty(obj.Tables[1].Rows[0]["RESUID"].ToString()) && obj.Tables[1].Rows[0]["RESUID"].ToString() != null)
        //                    {
        //                        GTotal = Total + ResuCharge + ResuServiseCharge + ResuFareDiff;
        //                    }
        //                    else
        //                    {
        //                        GTotal = Total;
        //                    }
        //                    my_table += "<tr>";
        //                    my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #1C589D; height: 20px;padding-left:10px'>GRAND TOTAL</td>";
        //                    my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #1C589D; height: 20px;font-weight:Bold'>" + GTotal + "</td>";
        //                    my_table += "</tr>";

        //                    my_table += "<tr>";
        //                    if (string.IsNullOrEmpty(PaxID.ToString()) || PaxID == 0)
        //                    {
        //                        my_table += "<td style='padding: 10px;border: thin solid #999999'>";
        //                    }
        //                    else
        //                    {
        //                        my_table += "<td colspan='2' style='padding: 10px;border: thin solid #999999'>";
        //                    }
        //                    #endregion
        //                }
        //                #endregion
        //                #region TERMS CONDITIONS - COMMON
        //                my_table += "<table border='0' cellpadding='0' cellspacing='0' width='100%' align='center' >";
        //                my_table += "<tr>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;color: #000000; height: 20px'> Kindly confirm the status of your PNR within 24 hrs of booking, as at times the same may fail on account of payment failure, internet connectivity, booking engine or due to any other reason beyond our control.</br>For Customers who book their flights well in advance of the scheduled departure date it is necessary that you re-confirm the departure time of your flight between 72 and 24 hours before the Scheduled Departure Time.</td>";
        //                my_table += "</tr>";

        //                my_table += "<tr>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #1C589D;font-weight:bold;padding-top:10px'>TERMS AND CONDITIONS :</td>";
        //                my_table += "</tr>";

        //                my_table += "<tr>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px;color: #000000; height: 20px'><b>1.</b> Guests are requested to carry their valid photo identification for all guests, including children</td>";
        //                my_table += "</tr>";

        //                my_table += "<tr>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px;color: #000000; height: 20px'><b>2.</b> We recommend check-in at least 2 hours prior to departure.</td>";

        //                my_table += "</tr>";
        //                my_table += "<tr>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px;color: #000000; height: 20px'><b>3.</b>  Boarding gates close 45 minutes prior to the scheduled time of departure. Please report at your departure gate at the indicated boarding time. Any passenger failing to report in time may be refused boarding privileges.</td>";
        //                my_table += "</tr>";

        //                my_table += "<tr>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px;color: #000000; height: 20px'><b>4.</b> Cancellations and Changes permitted more than two (2) hours prior to departure with payment of change fee and difference in fare if applicable only in working hours (10:00 am to 06:00 pm) except Sundays and Holidays.</td>";
        //                my_table += "</tr>";

        //                my_table += "<tr>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px;color: #000000; height: 20px'><b>5.</b>  Flight schedules are subject to change and approval by authorities.</td>";
        //                my_table += "</tr>";

        //                my_table += "<tr>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px;color: #000000; height: 20px'><b>6.</b> In case of ticket Cancellation the transaction fee will not be refunded.</td>";
        //                my_table += "</tr>";

        //                my_table += "<tr>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px;color: #000000; height: 20px'><b>7.</b> Name Changes on a confirmed booking are strictly prohibited. Please ensure that the name given at the time of booking matches as mentioned on the traveling Guests valid photo ID Proof.</td>";
        //                my_table += "</tr>";

        //                my_table += "<tr>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px;color: #000000; height: 20px'><b>8.</b>  Travel Agent does not provide compensation for travel on other airlines, meals, lodging or ground transportation.</td>";
        //                my_table += "</tr>";

        //                my_table += "<tr>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px;color: #000000; height: 20px'><b>9.</b> Bookings made under the Armed Forces quota are non cancelable and non- changeable.</td>";
        //                my_table += "</tr>";

        //                my_table += "<tr>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px;color: #000000; height: 20px'><b>10.</b> Guests are advised to check their all flight details (including their Name, Flight numbers, Date of Departure, Sectors) before leaving the Agent Counter.</td>";
        //                my_table += "</tr>";

        //                my_table += "<tr>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px;color: #000000; height: 20px'><b>11.</b> Cancellation amount will be charged as per airline rule.</td>";
        //                my_table += "</tr>";

        //                my_table += "<tr>";
        //                my_table += "<td style='font-family: arial, Helvetica, sans-serif; font-size: 12px;color: #000000; height: 20px'><b>12.</b> Guests requiring wheelchair assistance, stretcher, Guests traveling with infants and unaccompanied minors need to be booked in advance since the inventory for these special service requests are limited per flight.</td>";
        //                my_table += "</tr>";
        //                my_table += "</table>";

        //                my_table += "</td>";
        //                my_table += "</tr>";
        //                my_table += "</table>";
        //#endregion
        //                my_table += "</td>";
        //                my_table += "</tr>";
        //                my_table += "</table>";

        //            }
        //            catch (Exception ex)
        //            {
        //                ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightBookingReport");
        //            }
        //            return my_table;
        //        }

        //public int insertFltHeader(FltHeaderDetails objFH)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return Convert.ToInt32(objFCDAL.InsertFltHeaderDetails(objFH));
        //}

        //public int insertFltDetails(FltDetails objFD)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return Convert.ToInt32(objFCDAL.InsertFltDetails(objFD));
        //}

        //public int insertPax(FltPaxDetails objFPD)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return Convert.ToInt32(objFCDAL.InsertFltPxDetails(objFPD));
        //}

        //public int insertFltFareDetails(FltFareDetails objFFD)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return Convert.ToInt32(objFCDAL.InsertFltFareDetails(objFFD));
        //}

        //public int Insert_GDS_BKG_LOG(string VC, string ORDERID, string SELL_REQ, string SELL_RES, string ADDMULTI_REQ, string ADDMULTI_RES, string PRC_REQ, string PRC_RES, string TST_REQ, string TST_RES, string PNR_REQ, string PNR_RES, string OTHERS)
        //{
        //    try
        //    {
        //        FlightCommonDAL ObjDal = new FlightCommonDAL();
        //        ObjDal.Insert_GDS_LOG(VC, ORDERID, SELL_REQ, SELL_RES, ADDMULTI_REQ, ADDMULTI_RES, PRC_REQ, PRC_RES, TST_REQ, TST_RES, PNR_REQ, PNR_RES, OTHERS);
        //    }
        //    catch (Exception ex)
        //    {
        //        //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightBooking");
        //    }
        //    return 0;
        //}

        //public int Insert_LCC_BKG_LOG(string VC, string ORDERID, string GDSPNR, string BOOKREQ, string BOOKRES, string ADDPAYREQ, string ADDPAYRES, string PRC_RES, string CONFIRMPAYRES)
        //{
        //    try
        //    {
        //        FlightCommonDAL ObjDal = new FlightCommonDAL();
        //        ObjDal.Insert_LCC_LOG(VC, ORDERID, GDSPNR, BOOKREQ, BOOKRES, ADDPAYREQ, ADDPAYRES, PRC_RES, CONFIRMPAYRES);
        //    }
        //    catch (Exception ex)
        //    {
        //        //Console.WriteLine(ex.Message);
        //    }
        //    return 0;
        //}

        //public int Update_Flt_Header(string OrderId, string OwnerID, string GdsPNR, String AirlinePnr, string Status)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return (objFCDAL.Update_FltHeader(OrderId, OwnerID, GdsPNR, AirlinePnr, Status));
        //}

        //public int Update_Flt_PaxDetails(string OrderId, int Paxid, string TicketNo)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return (objFCDAL.Update_Flt_Pax(OrderId, Paxid, TicketNo));
        //}

        //public DataTable Update_Dis_Agnt_Limit(string OwnerId, double TAamount, double DIamount)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return (objFCDAL.Update_Dis_Agent_Limit(OwnerId, TAamount, DIamount));
        //}

        //public DataSet Select_Flt_Details(string UserID, string OrderId)//string TableName
        //{//Call a table method or PDF or Word Accordingly
        //    //Orderid is TrackId 
        //    //UserID is AgentID
        //    DataSet Flt = new DataSet();
        //    DataTable dt = new DataTable();
        //    if (!String.IsNullOrEmpty(OrderId) && !String.IsNullOrEmpty(UserID))
        //    {
        //        FlightCommonDAL dtagency = new FlightCommonDAL();
        //        dt = dtagency.SelectAgencyDetail(UserID, OrderId);
        //    }
        //    else if (String.IsNullOrEmpty(UserID) && !String.IsNullOrEmpty(OrderId))
        //    {
        //        FlightCommonDAL dtagency = new FlightCommonDAL();
        //        dt = dtagency.SelectAgencyDetail("", OrderId);
        //    }
        //    else
        //    {
        //        FlightCommonDAL dtagency = new FlightCommonDAL();
        //        dt = dtagency.SelectAgencyDetail(UserID, OrderId);
        //    }
        //    Flt.Tables.Add(dt);

        //    FlightCommonDAL dtpnr = new FlightCommonDAL();
        //    Flt.Tables.Add(dtpnr.SelectFlight_Details(OrderId, "FLT_HEADER"));//Table1- Index 0

        //    FlightCommonDAL dtpax = new FlightCommonDAL();
        //    Flt.Tables.Add(dtpax.SelectFlight_Details(OrderId, "PAX_DETAIL"));//Table2- Index 1

        //    FlightCommonDAL dtagentid = new FlightCommonDAL();
        //    Flt.Tables.Add(dtagentid.SelectFlight_Details(OrderId, "AGENTID"));//Table3- Index 2

        //    FlightCommonDAL dtflight = new FlightCommonDAL();
        //    Flt.Tables.Add(dtflight.SelectFlight_Details(OrderId, "FLT_DETAIL"));//Table4- Index 3

        //    FlightCommonDAL dtfare = new FlightCommonDAL();
        //    Flt.Tables.Add(dtfare.SelectFlight_Details(OrderId, "FLT_FAREDETAILS"));//Table5- Index 4

        //    return Flt;
        //}

        //public DataTable Select_ALL_FLT(string Tid, string TableName)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return (objFCDAL.Fetch_ALL_FltDetails(Tid, TableName));
        //}

        //public List<STD.Shared.FlightCityList> FetchCity(string jrnyType, string City)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return objFCDAL.FetchCityName(jrnyType, City);

        //}

        //public List<STD.Shared.AirlineList> FetchAirline(string Airline)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return objFCDAL.FetchAirlineName(Airline);

        //}

        //public bool Check_Agt_Distr_Credit(string AgentId, string DistrId, double Netfare)
        //{
        //    DAL.FlightCommonDAL obj = new FlightCommonDAL();
        //    bool status = obj.CHECK_AGENT_DISTR_CLIMIT(AgentId, DistrId, Netfare);
        //    return status;
        //}

        //public void Split_Tax(string[] tax, out double YQ, out double YR, out double WO, out double OT)
        //{
        //    string[] tax1 = null;
        //    YQ = 0;
        //    YR = 0;
        //    WO = 0;
        //    OT = 0;

        //    try
        //    {
        //        for (int i = 0; i <= tax.Length - 2; i++)
        //        {
        //            if (tax[i].Contains("YQ"))//Strings.InStr(tax[i], "YQ")
        //            {
        //                tax1 = tax[i].Split(':');
        //                YQ = YQ + Convert.ToInt32(tax1[1]);
        //            }
        //            else if (tax[i].Contains("YR"))
        //            {
        //                tax1 = tax[i].Split(':');
        //                YR = YR + Convert.ToInt32(tax1[1]);
        //            }
        //            else if (tax[i].Contains("WO"))
        //            {
        //                tax1 = tax[i].Split(':');
        //                WO = WO + Convert.ToInt32(tax1[1]);
        //            }
        //            else
        //            {
        //                tax1 = tax[i].Split(':');
        //                OT = OT + Convert.ToInt32(tax1[1]);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }


        //    //totTax = YQ + YR + WO + OT;
        //    //totFare = basefare + totTax + SrvTax + TF + admrk;
        //    //netFare = (totFare + tds) - comm;
        //}

        /// <summary>
        /// Function That Merges GDS,LCC Results - Created By Manish Sharma
        /// </summary>
        /// <param name="objFltList"></param>
        /// <returns></returns>
        //public ArrayList MergeResultList(ArrayList objFltList)
        //{
        //    List<FlightSearchResults> ListRes = new List<FlightSearchResults>();
        //    int TotLineOW = 0;
        //    int TotLineRT = 0;
        //    ArrayList Final = new ArrayList();
        //    try
        //    {
        //        //ArrayList OneWay = new ArrayList();
        //        List<FlightSearchResults> OneWay = new List<FlightSearchResults>();
        //        List<FlightSearchResults> RoundTrip = new List<FlightSearchResults>();
        //        for (int i = 0; i < objFltList.Count; i++) //For Total no. of Carrier Lists 
        //        {
        //            ArrayList list = new ArrayList();
        //            //Fetch Each List
        //            list = (ArrayList)objFltList[i];
        //            for (int trip = 0; trip < list.Count; trip++) //trip<2 after type safety
        //            {
        //                //Fetch Records List for OneWay or RoundTrip
        //                ListRes = (List<FlightSearchResults>)list[trip]; //Type Mismatch List[0] = ResListItem and List[1] = ArrayList
        //                //TotallineNo = ListRes[ListRes.Count - 1].LineNumber; //Line No. of Last Record
        //                if (i == 0) //If This is First Airline List
        //                {   //Add Records in Final List (Because First List would be Directly Added)
        //                    if (trip == 0)
        //                    {
        //                        if (ListRes.Count >= 1)
        //                        {
        //                            for (int j = 0; j < ListRes.Count; j++)
        //                            {
        //                                FlightSearchResults objOW = new FlightSearchResults();
        //                                objOW = ListRes[j];
        //                                OneWay.Add(objOW);// OneWay.Add(ListRes[j]);
        //                            }
        //                            TotLineOW = TotLineOW + OneWay[OneWay.Count - 1].LineNumber;
        //                        }
        //                    }
        //                    else if (trip == 1)
        //                    {
        //                        if (ListRes.Count >= 1)
        //                        {
        //                            for (int j = 0; j < ListRes.Count; j++)
        //                            {
        //                                FlightSearchResults objRT = new FlightSearchResults();
        //                                objRT = ListRes[j];
        //                                RoundTrip.Add(objRT);
        //                            }
        //                            TotLineRT = TotLineRT + RoundTrip[RoundTrip.Count - 1].LineNumber;
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    //Add Records in this List 
        //                    if (trip == 0)
        //                    {
        //                        if (ListRes.Count >= 1)
        //                        {
        //                            for (int j = 0; j < ListRes.Count; j++)
        //                            {//Set Values in New List 
        //                                //Change Line No. of Each Record (By Adding last Factor)
        //                                FlightSearchResults obj1 = new FlightSearchResults();
        //                                obj1 = (FlightSearchResults)ListRes[j].Clone(); //Pointing to Same Memory Location //Changed to Structure
        //                                obj1.LineNumber = ListRes[j].LineNumber + TotLineOW;
        //                                OneWay.Add(obj1);
        //                            }
        //                            TotLineOW = OneWay[OneWay.Count - 1].LineNumber;//Line No. of Last Record //TotLineOW + 
        //                        }
        //                    }
        //                    else if (trip == 1)
        //                    {
        //                        if (ListRes.Count >= 1)
        //                        {
        //                            for (int j = 0; j < ListRes.Count; j++)
        //                            {//Set Values in New List 
        //                                //Change Line No. of Each Record (By Adding last Factor)
        //                                FlightSearchResults obj2 = new FlightSearchResults();
        //                                obj2 = (FlightSearchResults)ListRes[j].Clone(); //Pointing to Same Memory Location //Changed to Structure
        //                                obj2.LineNumber = ListRes[j].LineNumber + TotLineRT;
        //                                RoundTrip.Add(obj2);
        //                            }
        //                            TotLineRT = RoundTrip[RoundTrip.Count - 1].LineNumber; //TotLineRT + 
        //                        }
        //                    }
        //                }
        //                //TotallineNo = TotallineNo+ ListRes[ListRes.Count - 1].LineNumber;//Line No. of Last Record
        //            }
        //        }
        //        Final.Add(OneWay);
        //        if (RoundTrip.Count > 0)
        //        {
        //            Final.Add(RoundTrip);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightBookingReport");
        //    }
        //    return Final;
        //}
        public ArrayList MergeResultList(ArrayList objFltList)//, string tm
        {
            List<FlightSearchResults> ListRes = new List<FlightSearchResults>();
            int TotLineOW = 0;
            int TotLineRT = 0;
            ArrayList Final = new ArrayList();
            try
            {
                //ArrayList OneWay = new ArrayList();
                List<FlightSearchResults> OneWay = new List<FlightSearchResults>();
                List<FlightSearchResults> RoundTrip = new List<FlightSearchResults>();
                for (int trip = 0; trip < objFltList.Count; trip++) //trip<2 after type safety
                {
                    ArrayList list = new ArrayList();
                    //Fetch Records List for OneWay or RoundTrip
                    list = (ArrayList)objFltList[trip];
                    int rec = 1;
                    #region Process
                    for (int i = 0; i < list.Count; i++) // 
                    {
                        //Fetch Each List for Total no. of Carriers
                        ListRes = (List<FlightSearchResults>)list[i];
                        //TotallineNo = ListRes[ListRes.Count - 1].LineNumber; //Line No. of Last Record
                        if (ListRes.Count >= 1) //If This is First Airline List
                        {   //Add Records in Final List (Because First List would be Directly Added)
                            if (trip == 0)
                            {
                                if (rec == 1)
                                {
                                    for (int j = 0; j < ListRes.Count; j++)
                                    {
                                        FlightSearchResults objOW = new FlightSearchResults();
                                        objOW = ListRes[j];
                                        OneWay.Add(objOW);// OneWay.Add(ListRes[j]);
                                    }
                                    TotLineOW = TotLineOW + OneWay[OneWay.Count - 1].LineNumber;
                                    rec++;
                                }
                                else
                                {
                                    for (int j = 0; j < ListRes.Count; j++)
                                    {//Set Values in New List 
                                        //Change Line No. of Each Record (By Adding last Factor)
                                        FlightSearchResults obj1 = new FlightSearchResults();
                                        obj1 = (FlightSearchResults)ListRes[j].Clone(); //Pointing to Same Memory Location //Changed to Structure
                                        obj1.LineNumber = ListRes[j].LineNumber + TotLineOW;
                                        OneWay.Add(obj1);
                                    }
                                    TotLineOW = OneWay[OneWay.Count - 1].LineNumber;//Line No. of Last Record //TotLineOW + 
                                }
                            }
                            else if (trip == 1)
                            {
                                if (rec == 1)
                                {
                                    for (int j = 0; j < ListRes.Count; j++)
                                    {
                                        FlightSearchResults objRT = new FlightSearchResults();
                                        objRT = ListRes[j];
                                        RoundTrip.Add(objRT);
                                    }
                                    TotLineRT = TotLineRT + RoundTrip[RoundTrip.Count - 1].LineNumber;
                                    rec++;
                                }
                                else
                                {
                                    for (int j = 0; j < ListRes.Count; j++)
                                    {//Set Values in New List 
                                        //Change Line No. of Each Record (By Adding last Factor)
                                        FlightSearchResults obj2 = new FlightSearchResults();
                                        obj2 = (FlightSearchResults)ListRes[j].Clone(); //Pointing to Same Memory Location //Changed to Structure
                                        obj2.LineNumber = ListRes[j].LineNumber + TotLineRT;
                                        RoundTrip.Add(obj2);
                                    }
                                    TotLineRT = RoundTrip[RoundTrip.Count - 1].LineNumber; //TotLineRT + 
                                }
                            }
                        }
                    }
                    #endregion
                }
                // OneWay[OneWay.Count - 1].TimeSpent = OneWay[OneWay.Count - 1].TimeSpent + "#" + tm + "#" + DateTime.Now.ToString("h:mm:ss.ff tt") + "AFT_Merge#";//After Merge
                Final.Add(OneWay);
                if ((RoundTrip.Count > 0) || (objFltList.Count == 2))
                {
                    Final.Add(RoundTrip);
                }
            }
            catch (Exception ex)
            {
                throw ex;//ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightBookingReport");
            }
            return Final;
        }

        public DataTable GetFltComm_Gal(string GPType, string VC, decimal BaseFare, decimal YQ, int PaxCnt, string RBD, string cabinClass, string depdate, string sector
                              , string returnDate, string farebasis, string orgCountry, string destCountry, string fltNum, string OPC, string MktC, string fareType, string bkgChannel)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.Cal_Comm_cb_Gal(GPType, VC, BaseFare, YQ, PaxCnt, RBD, cabinClass, depdate, sector, returnDate, farebasis, orgCountry, destCountry, fltNum, OPC, MktC, fareType, bkgChannel);
        }

        public DataTable GetFltComm_WithouDB(string GPType, string VC, decimal BaseFare, decimal YQ, int PaxCnt, string RBD, string cabinClass, string depdate, string sector
                               , string returnDate, string farebasis, string orgCountry, string destCountry, string fltNum, string OPC, string MktC, string fareType, string bkgChannel)
        {
            DataTable commDT = new DataTable();
            DataTable table = new DataTable();
            try
            {           
            //DataTable dtn = (DataTable)HttpContext.Current.Session["CommissionNew"];
            DataTable dtn = (DataTable)System.Web.HttpContext.Current.Session["CommissionNew"];
            string expression = "";
            string BookClassin = "";
            string RUN = "(AirlineCode = 'VVX')";
            bool NR = false;
                //    expression = " (AirlineCode = '" + VC + "' or AirlineCode='ALL') and (CabinClassInclude = '" + cabinClass + "' or CabinClassInclude='ALL') and (GroupType = '" + GPType + "' or GroupType='ALL')  and (fareType = '" + fareType + "')  and (BookingClassInclude like '%" + RBD + "%' or BookingClassInclude = '') and (BookingClassExclude not like '%" + RBD + "%' or BookingClassExclude = '') ";

                DataRow[] CheckAirline = dtn.Select("AirlineCode = '" + VC + "'");
                if (CheckAirline.Count() == 0)
                {
                    table.Columns.Add("Dis", typeof(decimal));
                    table.Columns.Add("CB", typeof(decimal));
                    table.Columns.Add("PPPType", typeof(string));
                    table.Columns.Add("CashBack", typeof(decimal));
                    table.Rows.Add(0, 0, "PPPType", 0);
                }
                else
                {
                    //CHECK FARETYPE
                    //expression = " (AirlineCode = '" + VC + "' or AirlineCode='ALL') and (CabinClassInclude = '" + cabinClass + "' or CabinClassInclude='ALL') and (GroupType = '" + GPType + "' or GroupType='ALL')  and (fareType = 'All')  and (BookingClassInclude like '%" + RBD + "%') and (BookingClassExclude not like '%" + RBD + "%') ";
                    expression = " (AirlineCode = '" + VC + "' or AirlineCode='ALL') and (CabinClassInclude = '" + cabinClass + "' or CabinClassInclude='ALL') and (GroupType = '" + GPType + "' or GroupType='ALL')  and (fareType = '" + fareType + "')  and (BookingClassInclude like '%" + RBD + "%') and (BookingClassExclude not like '%" + RBD + "%') ";

                    DataRow[] foundRows = dtn.Select(expression);

                    DataRow[] foundRows1 = dtn.Select(RUN);   // NO Record
                                                              //BOOKINGClassin
                    if (foundRows.Count() == 0)
                    {
                       // BookClassin = " (AirlineCode = '" + VC + "' or AirlineCode='ALL') and (CabinClassInclude = '" + cabinClass + "' or CabinClassInclude='ALL') and (GroupType = '" + GPType + "' or GroupType='ALL')  and (fareType = 'All') and (BookingClassInclude = ' ') and (BookingClassExclude =' ') ";
                        BookClassin = " (AirlineCode = '" + VC + "' or AirlineCode='ALL') and (CabinClassInclude = '" + cabinClass + "' or CabinClassInclude='ALL') and (GroupType = '" + GPType + "' or GroupType='ALL')  and (fareType = '" + fareType + "') and (BookingClassInclude = ' ') and (BookingClassExclude =' ') ";
                        foundRows1 = dtn.Select(BookClassin);
                    }


                    if (foundRows.Count() == 0 && foundRows1.Count() == 0)
                    {
                        expression = " (AirlineCode = '" + VC + "' or AirlineCode='ALL') and (CabinClassInclude = '" + cabinClass + "' or CabinClassInclude='ALL') and (GroupType = '" + GPType + "' or GroupType='ALL')  and (fareType = 'ALL')  and (BookingClassInclude like '%" + RBD + "%') and (BookingClassExclude not like '%" + RBD + "%') ";
                        DataRow[] foundRowsType = dtn.Select(expression);

                        if (foundRowsType.Count() > 0)
                        {
                            commDT = dtn.Select(expression).CopyToDataTable();
                        }
                        else
                        {
                            expression = " (AirlineCode = '" + VC + "' or AirlineCode='ALL') and (CabinClassInclude = '" + cabinClass + "' or CabinClassInclude='ALL') and (GroupType = '" + GPType + "' or GroupType='ALL')  and (fareType ='ALL')  and (BookingClassInclude = '') and (BookingClassExclude = '') ";
                            DataRow[] foundRowsType2 = dtn.Select(expression);
                            if (foundRowsType2.Count() > 0)
                            {
                                commDT = dtn.Select(expression).CopyToDataTable();
                            }
                        }
                    }
                    else
                    {
                        if (foundRows.Count() > 0)
                            commDT = dtn.Select(expression).CopyToDataTable();

                        if (foundRows1.Count() > 0)
                            commDT = dtn.Select(BookClassin).CopyToDataTable();
                    }


                    string PPPType = "";
                    // DataTable table = new DataTable();
                    if (commDT != null && commDT.Rows.Count > 0)
                    {
                        /*calculate commision start*/

                        decimal totComm = (BaseFare * Convert.ToDecimal(commDT.Rows[0]["CommisionOnBasic"])) / 100;
                        totComm = totComm + ((YQ * Convert.ToDecimal(commDT.Rows[0]["CommissionOnYq"])) / 100);
                        totComm = totComm + (((BaseFare + YQ) * Convert.ToDecimal(commDT.Rows[0]["CommisionOnBasicYq"])) / 100);

                        //decimal totComm = (BaseFare * @CommisionOnBasic) / 100;
                        //totComm = totComm + ((YQ * @CommissionOnYq) / 100);
                        //totComm = totComm + (((BaseFare + YQ) * @CommisionOnBasicYq) / 100);

                        decimal CashBack = Convert.ToDecimal(commDT.Rows[0]["CashBackAmt"]) * 1;//@PaxCnt;
                                                                                                /*calculate commision end*/

                        decimal totPlb = (((BaseFare - totComm) * Convert.ToDecimal(commDT.Rows[0]["PlbOnBasic"])) / 100);
                        totPlb = totPlb + (((YQ - (totComm + totPlb)) * 0) / 100);
                        //  totPlb = totPlb + (((YQ - (totComm + totPlb)) * Convert.ToDecimal(commDT.Rows[0]["PlbOnYq"])) / 100);
                        totPlb = totPlb + ((((BaseFare + YQ) - (totComm + totPlb)) * Convert.ToDecimal(commDT.Rows[0]["PlbOnBasicYq"])) / 100);


                        table.Columns.Add("Dis", typeof(decimal));
                        table.Columns.Add("CB", typeof(decimal));
                        table.Columns.Add("PPPType", typeof(string));
                        table.Columns.Add("CashBack", typeof(decimal));

                        PPPType = Convert.ToString(commDT.Rows[0]["PPPType"]);
                        // Here we add five DataRows.



                        if (PPPType == "PPPSECTOR")
                        {
                            totComm = totComm + totPlb + (CashBack * Convert.ToInt32(bkgChannel));
                            table.Rows.Add(Math.Round(totComm, MidpointRounding.AwayFromZero), 0, PPPType, CashBack);
                            //  select @totComm+@totPlb+(@CashBack * @SectorCnt) as Dis,0 as CB , @PPPType as PPPType,(@CashBack * @SectorCnt) as CashBack
                        }
                        else
                        {
                            totComm = totComm + totPlb + CashBack;
                            table.Rows.Add(Math.Round(totComm, MidpointRounding.AwayFromZero), 0, PPPType, CashBack);
                            //'select @totComm+@totPlb+@CashBack as Dis,0 as CB , @PPPType as PPPType,@CashBack as CashBack
                        }
                    }
                    else
                    {
                        table.Columns.Add("Dis", typeof(decimal));
                        table.Columns.Add("CB", typeof(decimal));
                        table.Columns.Add("PPPType", typeof(string));
                        table.Columns.Add("CashBack", typeof(decimal));
                        table.Rows.Add(0, 0, "PPPType", 0);
                    }
                }

        }

            catch (Exception ex)
            {

            }
            //if (table == null && table.Rows.Count == 0)
            //{
            //    table.Rows.Add(0, 0, "PPPType", 0);

            //}
          return table;

        }


        //public List<STD.Shared.GetFtlCommission> GetFtlCommission(GetFtlCommission objGFC)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return objFCDAL.GetFtlCommission(objGFC);

        //}

        //public List<STD.Shared.GetFtlServiceTaxTFee> GetFtlSrvTaxTFee(GetFtlServiceTaxTFee objGFSTT)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return objFCDAL.GetFtlSrvTaxTFee(objGFSTT);

        //}

        //public List<STD.Shared.GetTDS> GetTDS(GetTDS objGetTDS)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return objFCDAL.GetTDS(objGetTDS);

        //}

        //public List<STD.Shared.GetFareDetailsByPaxId> GetFareDetailsByPaxId(int PaxId)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return objFCDAL.GetFareDetailsByPaxId(PaxId);

        //}

        //public Hashtable Chck_Bkng_Tktng_Status(string UserId, string UserType, string OwnerId, double DI_Netfare, double TA_Netfare)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return (objFCDAL.Check_Bkng_Tktng_Status(UserId, UserType, OwnerId, DI_Netfare, TA_Netfare));
        //}

        //public bool check_trackid(string ORDERID)
        //{
        //    bool stat = true;
        //    try
        //    {
        //        FlightCommonDAL ObjFltDal = new FlightCommonDAL();
        //        stat= ObjFltDal.CHECK_ORDERID(ORDERID);
        //    }
        //    catch (Exception ex)
        //    {
        //        //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightBookingReport");
        //    }
        //    return stat;
        //}

        //public DataTable GetTktCredentials(string VC)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return (objFCDAL.Get_TktCredentials(VC));
        //}

        //public DataTable GET_FLTHEADERDETAILSBYSTATUS(string STATUS, string TRIP, string MULTISTATUS, string EXECID)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return (objFCDAL.GET_FLTHEADERDETAILSBYSTATUS(STATUS, TRIP, MULTISTATUS, EXECID));
        //}

        //public int UpdateHeaderDetails(string ORDERID, string EXECID, string STATUS, string REJECTRM, string GDSPNR, string AIRLINEPNR)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return Convert.ToInt32(objFCDAL.UpdateFltDetails(ORDERID, EXECID, STATUS, REJECTRM, GDSPNR, AIRLINEPNR));
        //}

        //public void MessageForInfantPax(string Pnr, string VC, DataTable PaxDs)
        //{
        //    if (VC == "SG" | VC == "6E")
        //    {
        //        string pgstr = "";
        //        DataRow[] PaxArray = null;
        //        PaxArray = PaxDs.Select("PaxType='INF'", "");
        //        try
        //        {
        //            if (PaxArray.Length > 0)
        //            {
        //                pgstr = "Dear Team,<br /><br />";
        //                pgstr = pgstr + "Please Contact LCC Airline For Infant Passenger(PNR : " + Pnr + " And Airline : " + VC + ").<br /><br />";
        //                try
        //                {
        //                    for (int k = 0; k <= PaxArray.Length - 1; k++)
        //                    {
        //                        string name = (PaxArray[k]["Title"].ToString()) + " " + (PaxArray[k]["FName"].ToString()) + " " + (PaxArray[k]["MName"].ToString()) + " " + (PaxArray[k]["LName"].ToString());
        //                        pgstr = pgstr + "Infant Name : " + name.Trim() + " dateOfBirth(DD/MM/YYYY) : " + (PaxArray[k]["DOB"].ToString());
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightBookingReport");
        //                }
        //                pgstr = pgstr + "<br />Regards,<br />Spring Travels";
        //                System.Net.Mail.SmtpClient objMail = new System.Net.Mail.SmtpClient();
        //                System.Net.Mail.MailMessage msgMail = new System.Net.Mail.MailMessage();
        //                msgMail.To.Clear();
        //                msgMail.To.Add(new System.Net.Mail.MailAddress("b2badmin@springtravels.com"));
        //                msgMail.From = new System.Net.Mail.MailAddress("SPRING TRAVELS(B2B Ticketing)<b2bticketing@springtravels.com>");
        //                msgMail.Subject = "LCC Pnr with Infant Passenger";
        //                msgMail.IsBodyHtml = true;
        //                msgMail.Body = pgstr;
        //                try
        //                {
        //                    objMail.Credentials = new System.Net.NetworkCredential("b2bticketing", "america");
        //                    objMail.Host = "shekhal.springtravels.com";
        //                    objMail.Send(msgMail);
        //                }
        //                catch (Exception ex)
        //                {
        //                    //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightBookingReport");
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightBookingReport");
        //        }
        //    }
        //}

        //public string mailTktCopy(string VC, string FltNo, string Sector, string DepDate, string FT, string AirlinePnr, string GdsPnr, string BkgStatus, string OrderId, string EmailId, int PaxId, DataSet AgencyDs, string OwnerId)
        //{

        //    string strTktCopy = "";
        //    string strHTML = "";
        //    string strFileName = "";
        //    string strMailMsg = "";
        //    string strErrorMsg = "";
        //    bool rightHTML = false;
        //    strFileName = "D:\\SPR_TicketCopy\\" + GdsPnr + "-" + FT + " Flight details-" + DateTime.Now.ToString().Replace(":", "").Trim() + ".html";
        //    strFileName = strFileName.Replace("/", "-");
        //    strTktCopy = "";// Create_Flt_Html_table(AgencyDs, OrderId, PaxId, OwnerId);
        //    strHTML = "<html><head><title>Booking Details</title><style type='text/css'> .maindiv{border: #1C589D 1px solid; margin: 10px auto 10px auto; width: 650px; font-size:12px; font-family:tahoma,Arial;}\t .text1{color:#333333; font-weight:bold;}\t .pnrdtls{font-size:12px; color:#333333; text-align:left;font-weight:bold;}\t .pnrdtls1{font-size:12px; color:#333333; text-align:left;}\t .bookdate{font-size:11px; color:#CC6600; text-align:left}\t .flthdr{font-size:11px; color:#CC6600; text-align:left; font-weight:bold}\t .fltdtls{font-size:11px; color:#333333; text-align:left;}\t.text3{font-size:11px; padding:5px;color:#333333; text-align:right}\t .hdrtext{padding-left:5px; font-size:14px; font-weight:bold; color:#FFFFFF;}\t .hdrtd{background-color:#333333;}\t  .lnk{color:#333333;text-decoration:underline;}\t  .lnk:hover{color:#333333;text-decoration:none;}\t  .contdtls{font-size:12px; padding-top:8px; padding-bottom:3px; color:#333333; font-weight:bold}\t  .hrcss{color:#CC6600; height:1px; text-align:left; width:450px;}\t </style></head><body>" + strTktCopy + "</body></html>";
        //    rightHTML = Utility.SaveTextToFile(strHTML, strFileName, ref strErrorMsg);
        //    strMailMsg = "<p style='font-family:verdana; font-size:12px'>Dear Customer<br /><br />";
        //    strMailMsg = strMailMsg + "Greetings of the day !!!!<br /><br />";
        //    strMailMsg = strMailMsg + "Please find an attachment for your E-ticket, kindly carry the print out of the same for hassle-free travel. Your onward booking for " + Sector + " is confirmed on " + VC + "-" + FltNo + " for " + DepDate + ". Your airline  booking reference no is " + AirlinePnr + ". <br /><br />";
        //    strMailMsg = strMailMsg + "Have a nice &amp; wonderful trip.<br /><br />";
        //    if (BkgStatus == "Ticketed")
        //    {
        //        System.Net.Mail.SmtpClient objMail = new System.Net.Mail.SmtpClient();
        //        System.Net.Mail.MailMessage msgMail = new System.Net.Mail.MailMessage();
        //        msgMail.To.Clear();
        //        msgMail.To.Add(new System.Net.Mail.MailAddress(EmailId));
        //        msgMail.CC.Add(new System.Net.Mail.MailAddress("b2badmin@springtravels.com"));
        //        msgMail.From = new System.Net.Mail.MailAddress("SPRING TRAVELS(B2B Ticketing)<b2bticketing@springtravels.com>");
        //        msgMail.Subject = FT + " Flight Booking Details";
        //        msgMail.IsBodyHtml = true;
        //        msgMail.Body = strMailMsg;
        //        if (rightHTML)
        //        {
        //            msgMail.Attachments.Add(new System.Net.Mail.Attachment(strFileName));
        //        }
        //        try
        //        {
        //            objMail.Credentials = new System.Net.NetworkCredential("b2bticketing", "america");
        //            objMail.Host = "shekhal.springtravels.com";
        //            objMail.Send(msgMail);
        //        }
        //        catch (Exception ex)
        //        {
        //            //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightSearch");
        //        }
        //    }
        //    return strTktCopy;
        //}

        //public DataTable GetPaxDetailsForHoldPNR(string ORDERID)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return (objFCDAL.GetPaxDetailsForHoldPNR(ORDERID));

        //}

        //public int UpdateLadgerByPaxId(string ORDERID, int PAXID, string TICKETNO)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return Convert.ToInt32(objFCDAL.UpdateLadgerByPaxId(ORDERID, PAXID, TICKETNO));
        //}

        //public DataTable GetHoldPNRReport(string LOGINID, string OWNERID, string DISTRID, string USERTYPE, string STATUS, string MULTISTATUS)
        //{
        //    FlightCommonDAL objFCDAL = new FlightCommonDAL();
        //    return (objFCDAL.GetHoldPNRReport(LOGINID, OWNERID, DISTRID, USERTYPE, STATUS, MULTISTATUS));
        //}
        /// <summary>
        /// Commission for International
        /// </summary>
        /// <param name="GPType">Agency Type</param>
        /// <param name="VC">validating Carrier</param>
        /// <param name="BaseFare"></param>
        /// <param name="YQ"></param>
        /// <param name="Qtax"></param>
        /// <param name="Org">Origin</param>
        /// <param name="Dest">Destination</param>
        /// <param name="Cls">RBD</param>
        /// <param name="DDate">DDMMYY</param>
        /// <param name="RDate">DDMMYY</param>
        /// <param name="PaxCnt">1</param>
        /// <returns></returns>
        public DataTable GetFltComm_Gal_Intl(string GPType, string VC, decimal BaseFare, decimal YQ, decimal Qtax, string Org, string Dest, string Cls, string DDate, string RDate, string cabinClass, string farebasis, string fltNum, string OPC, string MktC, string fareType, string bkgChannel)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.Cal_Comm_cb_Gal_Intl(GPType, VC, BaseFare, YQ, Qtax, Org, Dest, Cls, DDate, RDate, cabinClass,farebasis,fltNum,OPC,MktC,fareType,bkgChannel);
        }
        public string PostXml(string url, string xml, string Userid, string Pwd, string SOAPAction,int optional = 1,string optair = "GDS",string fareType="")
        {
            string MailBody = "";
            string MailSubject = "";
            string ServiceType = "1G";            
            StringBuilder sbResult = new StringBuilder();
            try
            {
                Uri ur = new Uri(url);
                // Galileo.Web.GZipHttpWebRequest Http = new Galileo.Web.GZipHttpWebRequest(ur);

                HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(url);
                if (!string.IsNullOrEmpty(xml))
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                    Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip");
                    // Http.Headers.Add(HttpRequestHeader.ContentEncoding, "gzip");
                    Http.Method = "POST";
                    byte[] lbPostBuffer = Encoding.UTF8.GetBytes(xml);

                    
                    Http.ContentLength = lbPostBuffer.Length;
                    Http.ContentType = @"text/xml";
                    Http.KeepAlive = false;
                    // Http.Credentials = new NetworkCredential(Userid, Pwd);
                    string auth = Userid + ":" + Pwd;
                    auth = Convert.ToBase64String(Encoding.Default.GetBytes(auth));
                    Http.Headers["Authorization"] = "Basic " + auth;
                    Http.Headers.Add(String.Format("SOAPAction: \"{0}\"", SOAPAction));
                    // Http.PreAuthenticate = true;
                    //Http.AllowAutoRedirect = false;
                    //Stream postStream = Http.GetRequestStream();
                    //GZipStream zipStream = new GZipStream(postStream, CompressionMode.Compress, false);
                    //zipStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                    //zipStream.Close();
                    //postStream.Close();


                    //using (Stream postStream = Http.GetRequestStream())
                    //{
                    //    using (Stream zipStream = new GZipStream(postStream, CompressionMode.Compress, false))
                    //    {

                    //        //int nRead;
                    //        //while ((nRead = postStream.Read(lbPostBuffer, 0, lbPostBuffer.Length)) > 0)
                    //        //{
                    //        //    zipStream.Write(lbPostBuffer, 0, nRead);
                    //        //}


                    //       zipStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                    //    }
                    //} 


                    //using (Stream PostStream = new GZipStream(Http.GetRequestStream(), CompressionMode.Compress, true))
                    using (Stream PostStream = Http.GetRequestStream())
                    {

                        PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                    }
                    //using (Stream PostStream = new GZipStream(Http.GetRequestStream(), CompressionMode.Compress, true))
                    //{ }
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        //else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        //{
                        //    responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        //}
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        responseStream.Close();
                    }
                }
            }

            catch (WebException ex)
            {
                string ApiResponse = Convert.ToString(sbResult);
                sbResult.Append("");
                #region Write Error Log Date:21-01-2018
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonBAL(PostXml)", "Error_001", ex, "PostXml-Issue in 1G Web Service");
                File.AppendAllText("C:\\\\CPN_SP\\\\1G_PostXml_WebException_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine +
                    "URL: " + url + Environment.NewLine +
                    "Userid: " + Userid + Environment.NewLine +
                    "Pwd: " + Pwd + Environment.NewLine +
                    "SOAPAction: " + SOAPAction + Environment.NewLine +
                    "CRDTYPE: " + fareType + Environment.NewLine +
                    "WebExceptionMessage: " + Environment.NewLine + ex.Message + Environment.NewLine + Environment.NewLine +
                    "WebExceptionStackTrace: " + Environment.NewLine + Convert.ToString(ex.StackTrace) + Environment.NewLine + Environment.NewLine +
                    "1G_XML_REQUEST: " + Environment.NewLine + xml + Environment.NewLine + Environment.NewLine +
                    "1G_XML_RESPONSE:" + Environment.NewLine + ApiResponse + Environment.NewLine + Environment.NewLine + Environment.NewLine);
                #region Send Service Down Mail
                try
                {
                    MailBody = "<p>Unable to get the result from 1G<br />" +
                        "<br />DateTime: " + System.DateTime.Now.ToString() +
                        "<br />FareType: " + fareType +
                        "<br />UserId: " + Userid +
                        "<br />Pwd: " + Pwd +
                         "<br />URL: " + url +
                         "<br />SOAPAction: " + SOAPAction +
                          "<br /><br />ExceptionMessage:<br />" + ex.Message +
                           "<br /><br />ExceptionStackTrace:<br />" + Convert.ToString(ex.StackTrace) +
                        "<br /><br />1G-Request:<br />" + xml +
                        "<br /><br />1G-Response:<br />" + ApiResponse+
                         "<br /></p>";
                    MailSubject = "PostXml in 1G Web Service-WebException DateTime: " + System.DateTime.Now.ToString();
                    MailNotification(MailBody, MailSubject, "", ServiceType);

                }
                catch (Exception ex1)
                { }
                #endregion
               
                #endregion
            }
            catch (Exception ex)
            {
                string ApiResponse = Convert.ToString(sbResult);
                sbResult.Append("");
                #region Write Error Log Date:21-01-2018
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonBAL(PostXml)", "Error_001", ex, "PostXml-Issue in 1G Web Servicel");
                File.AppendAllText("C:\\\\CPN_SP\\\\1G_PostXml_Exception_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine +
                   "URL: " + url + Environment.NewLine +
                   "Userid: " + Userid + Environment.NewLine +
                   "Pwd: " + Pwd + Environment.NewLine +
                   "SOAPAction: " + SOAPAction + Environment.NewLine +
                   "CRDTYPE: " + fareType + Environment.NewLine +
                   "ExceptionMessage: " + Environment.NewLine + ex.Message + Environment.NewLine + Environment.NewLine +
                   "ExceptionStackTrace: " + Environment.NewLine + Convert.ToString(ex.StackTrace) + Environment.NewLine + Environment.NewLine +
                   "1G_XML_REQUEST: " + Environment.NewLine + xml + Environment.NewLine + Environment.NewLine +
                   "1G_XML_RESPONSE:" + Environment.NewLine + ApiResponse + Environment.NewLine + Environment.NewLine + Environment.NewLine);
                #region Send Service Down Mail
                try
                {
                    MailBody = "Unable to get the result from 1G<br />" +
                        "<br />DateTime: " + System.DateTime.Now.ToString() +
                        "<br />FareType:" + fareType +
                        "<br />UserId: " + Userid +
                        "<br />Pwd: " + Pwd +
                         "<br />URL: " + url +
                         "<br />SOAPAction: " + SOAPAction +
                          "<br />ExceptionMessage:<br />" + ex.Message +
                           "<br />ExceptionStackTrace:<br />" + Convert.ToString(ex.StackTrace) +
                        "<br /><br />1G-Request:<br />" + xml +
                        "<br /><br />1G-Response:<br />" + Convert.ToString(ApiResponse);
                    MailSubject = "PostXml in 1G Web Service-Exception DateTime: " + System.DateTime.Now.ToString();
                    MailNotification(MailBody, MailSubject, "", ServiceType);
                    #endregion
                }
                catch (Exception ex1)
                { }
                #endregion
                
            }
            finally
            {
                if (optional == 0)
                {
                    try
                    {
                    SaveResponse.SAVElOGFILE(xml, "REQ", "XML", "GDS", optair,fareType);
                    SaveResponse.SAVElOGFILE(sbResult.ToString(), "RES", "XML", "GDS", optair,fareType);
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            //try
            //{
            //    string strFileName = @"D:\GALXMLLOGS\Req" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss").Replace(":", ".").Trim() + ".xml";
            //    string strFileName1 = @"D:\GALXMLLOGS\Res" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss").Replace(":", ".").Trim() + ".xml";
            //    SaveTextToFile(xml, strFileName);
            //    SaveTextToFile(sbResult.ToString(), strFileName1);
            //}
            //catch { }
           // if (!string.IsNullOrEmpty(sbResult.ToString()) && sbResult.ToString().Contains("<ErrorCode>") && sbResult.ToString().Contains("NO AVAILABILITY FOR THIS REQUEST") == false)
            if (!string.IsNullOrEmpty(sbResult.ToString()) && (sbResult.ToString().Contains("<ErrorCode>") || sbResult.ToString().Contains("<Text>UNABLE TO FARE QUOTE</Text>")))
            {
                string strDateTime = ((System.DateTime.Now.ToString().Replace('-', ' ')).Replace(':', ' ')).Replace('/', ' ');
                string TxtFileName = string.Concat(strDateTime.Where(c => !char.IsWhiteSpace(c)));
                if (sbResult.ToString().Contains("UNAVAILABLE") && sbResult.ToString().Contains("NO AVAILABILITY FOR THIS REQUEST") == false)
                {
                    try
                    {
                        File.AppendAllText("C:\\\\1G_UNAVAILABLE_LOG\\\\1G_PostXml_UNAVAILABLE_" + TxtFileName + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + "URL: " + url + Environment.NewLine + "Userid: " + Userid + Environment.NewLine + "Pwd: " + Pwd + Environment.NewLine + "SOAPAction: " + SOAPAction + Environment.NewLine + "CRDTYPE: " + fareType + Environment.NewLine + "1G_XML_REQUEST: " + Environment.NewLine + xml + Environment.NewLine + "1G_XML_RESPONSE:" + Environment.NewLine + sbResult.ToString() + Environment.NewLine + Environment.NewLine + Environment.NewLine);
                        #region Send Service Down Mail
                        MailBody = "<p>Unable to get the result from 1G - UNAVAILABLE<br />" +                            
                            "<br />DateTime: " + System.DateTime.Now.ToString() +
                            "<br />TxtFileRef: " + TxtFileName +
                            "<br />FareType: " + fareType +
                            "<br />UserId: " + Userid +
                            "<br />Pwd: " + Pwd +
                             "<br />URL: " + url +
                             "<br />SOAPAction: " + SOAPAction +
                            "<br /><br />1G-Request:<br />" + xml +
                            "<br /><br />1G-Response:<br />" + Convert.ToString(sbResult) +
                            "<br /></p>";
                        MailSubject = "Issue in 1G Web Service-UNAVAILABLE, Date-Time:" + System.DateTime.Now.ToString();
                        MailNotification(MailBody, MailSubject, "", ServiceType);
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonBAL(PostXml)", "Error_002", ex, "unable to get the result from 1G" + xml);
                    }
                   
                }
                else if(sbResult.ToString().Contains("<Text>UNABLE TO FARE QUOTE</Text>"))
                {
                    try
                    {
                        File.AppendAllText("C:\\\\1G_UNAVAILABLE_LOG\\\\1G_UNABLE_TO_FARE_QUOTE_" + TxtFileName + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + "URL: " + url + Environment.NewLine + "Userid: " + Userid + Environment.NewLine + "Pwd: " + Pwd + Environment.NewLine + "SOAPAction: " + SOAPAction + Environment.NewLine + "CRDTYPE: " + fareType + Environment.NewLine + "1G_XML_REQUEST: " + Environment.NewLine + xml + Environment.NewLine + "1G_XML_RESPONSE:" + Environment.NewLine + sbResult.ToString() + Environment.NewLine + Environment.NewLine + Environment.NewLine);
                        #region Send Service Down Mail
                        MailBody = "<p>UNABLE TO FARE QUOTE - 1G SERVICE<br />" +
                            "<br />DateTime: " + System.DateTime.Now.ToString() +
                            "<br />TxtFileRef: " + TxtFileName +
                            "<br />FareType: " + fareType +
                            "<br />UserId: " + Userid +
                            "<br />Pwd: " + Pwd +
                             "<br />URL: " + url +
                             "<br />SOAPAction: " + SOAPAction +
                            "<br /><br />1G-Request:<br />" + xml +
                            "<br /><br />1G-Response:<br />" + Convert.ToString(sbResult) +
                            "<br /></p>";
                        MailSubject = "UNABLE TO FARE QUOTE - 1G Service, Date-Time:" + System.DateTime.Now.ToString();
                        MailNotification(MailBody, MailSubject, "", "1G-FARE-QUOTE");
                        #endregion
                    }
                    catch (Exception ex)
                    { }
                    
                }
                else
                {
                    try
                    {
                        File.AppendAllText("C:\\\\1G_UNAVAILABLE_LOG\\\\1G_PostXml_NO_AVAILABILITY_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + "URL: " + url + Environment.NewLine + "Userid: " + Userid + Environment.NewLine + "Pwd: " + Pwd + Environment.NewLine + "SOAPAction: " + SOAPAction + Environment.NewLine + "CRDTYPE: " + fareType + Environment.NewLine + "1G_XML_REQUEST: " + Environment.NewLine + xml + Environment.NewLine + "1G_XML_RESPONSE:" + Environment.NewLine + sbResult.ToString() + Environment.NewLine + Environment.NewLine + Environment.NewLine);
                        #region Send Service Down Mail
                        MailBody = "<p>Unable to get the result from 1G<br />" +
                            "<br />DateTime: " + System.DateTime.Now.ToString() +
                            "<br />TxtFileRef: " + TxtFileName +
                            "<br />FareType: " + fareType +
                            "<br />UserId: " + Userid +
                            "<br />Pwd: " + Pwd +
                             "<br />URL: " + url +
                             "<br />SOAPAction: " + SOAPAction +
                            "<br /><br />1G-Request:<br />" + xml +
                            "<br /><br />1G-Response:<br />" + Convert.ToString(sbResult) +
                            "<br /></p>";
                        MailSubject = "Issue in 1G Web Service Date-Time:" + System.DateTime.Now.ToString();
                        MailNotification(MailBody, MailSubject, "", ServiceType);
                        #endregion
                    }
                    catch (Exception ex)
                    {
                    }                    
                }



            }
            #region Send Mail 1G Request Response XML
            //try
            //{
            //    MailBody = "<p><b>1G Request Response</b><br />" +
            //                   "<br />DateTime: " + System.DateTime.Now.ToString() + "<br />FareType: " + fareType + "<br />UserId: " + Userid + "<br />Pwd: " + Pwd +
            //                   "<br />URL: " + url + "<br />SOAPAction:" + SOAPAction + "<br /><br />1G-Request:<br />" + xml +
            //                   "<br /><br />1G-Response:<br />" + Convert.ToString(sbResult) + "<br /></p>";
            //    MailSubject = "1G Request Response Post Xml  Details, Date-Time:" + System.DateTime.Now.ToString();
            //    MailNotification(MailBody, MailSubject, "", "1GPOSTXML");
            //}
            //catch(Exception)
            //{ }           
            #endregion

            return sbResult.ToString();
        }

        public int MailNotification(string Emailbody, string subject, string AttachmentFile, string ServiceType)
        {
            int flag = 0;
            try
            {
                //body = "Test Mail Service Api";
                #region Send Mail               
                string body = "<html><body><p>" + Emailbody + "</p></body></html>";
                DataSet ds = GetMailNotificationCredential(ServiceType);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    

                    #region Set value Credential 
                    //string SMTPCLIENT = "relay.galileo.co.in";
                    //string UserID = "auth@relay.galileo.co.in";
                    //string Password = "Z@14$>8*&}d#";
                    ////string SMTPCLIENT = "smtpbp.falconide.com";
                    ////string UserID = "flywidusfal";
                    ////string Password = "KieG@89LE";
                    //string from = "noreply@flywidus.com";
                    //string To = "Mohit.Kumar1@galileo.co.in";
                    //string cc = "devesh.mailme@gmail.com";// "Ajit.Kumar@galileo.co.in";
                    //string bcc = "Devesh.Singh@galileo.co.in";
                    #endregion

                    if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["SMTPCLIENT"])) && !string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["UserID"])) && !string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["Pass"])) && !string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["MAILFROM"])) && !string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["MAILTO"])))
                    {
                      #region Sending 
                    string SMTPCLIENT = Convert.ToString(ds.Tables[0].Rows[0]["SMTPCLIENT"]);
                    string UserID = Convert.ToString(ds.Tables[0].Rows[0]["UserID"]);
                    string Password = Convert.ToString(ds.Tables[0].Rows[0]["Pass"]);
                    string from = Convert.ToString(ds.Tables[0].Rows[0]["MAILFROM"]);
                    string To = Convert.ToString(ds.Tables[0].Rows[0]["MAILTO"]);
                    string cc = "";// "Ajit.Kumar@galileo.co.in";
                    string bcc = "";//true
                    if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["CCStatus"])) && !string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["CC"])) && Convert.ToString(ds.Tables[0].Rows[0]["CCStatus"]).ToLower() == "true")
                    {
                        cc = Convert.ToString(ds.Tables[0].Rows[0]["CC"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["BCCStatus"])) && !string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["BCC"])) && Convert.ToString(ds.Tables[0].Rows[0]["BCCStatus"]).ToLower() == "true")
                    {
                        bcc = Convert.ToString(ds.Tables[0].Rows[0]["BCC"]);
                    }
                    System.Net.Mail.SmtpClient objMail = new System.Net.Mail.SmtpClient();
                    System.Net.Mail.MailMessage msgMail = new System.Net.Mail.MailMessage();
                    msgMail.To.Clear();
                    msgMail.To.Add(new System.Net.Mail.MailAddress(To));
                    msgMail.From = new System.Net.Mail.MailAddress(from);
                    if (!string.IsNullOrEmpty(bcc))
                    {
                        msgMail.Bcc.Add(new System.Net.Mail.MailAddress(bcc));
                    }
                    if (!string.IsNullOrEmpty(cc))
                    {
                        msgMail.CC.Add(new System.Net.Mail.MailAddress(cc));
                    }
                    //if (!string.IsNullOrEmpty(AttachmentFile))
                    //{
                    //    msgMail.Attachments.Add(new System.Net.Mail.Attachment(AttachmentFile));
                    //}

                    msgMail.Subject = subject;
                    msgMail.IsBodyHtml = true;
                    msgMail.Body = body;
                    try
                    {
                        objMail.Credentials = new System.Net.NetworkCredential(UserID, Password);
                        objMail.Host = SMTPCLIENT;
                        objMail.Send(msgMail);
                        flag = 1;

                    }
                    catch (Exception ex)
                    {
                        ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonBAL(MailNotification)", "Error_001", ex, "MailNotification-SendMail");
                        flag = 0;
                    }

                    #endregion
                    }                 
                }
                #endregion
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("FlightCommonBAL(MailNotification)", "Error_001", ex, "MailNotification");
                flag = 0;
            }
            return flag;
        }


        public bool SaveTextToFile(string strData, string FullPath)
        {
            string Contents = null;
            bool Saved = false;
            System.IO.StreamWriter objReader = null;
            try
            {
                objReader = new System.IO.StreamWriter(FullPath);
                objReader.Write(strData);
                objReader.Close();
                Saved = true;
            }
            catch (Exception Ex)
            {
                //ErrInfo = Ex.Message;
            }
            return Saved;
        }

        public Dictionary<string, object> GetIntlCommGal(string GType, ArrayList AirArray, string TDS)
        {
            Dictionary<string, object> IntFareDetails = new Dictionary<string, object>();
            try
            {
                DataTable CommDt = new DataTable();
                Hashtable STTFTDS = new Hashtable();
                List<FltSrvChargeList> SrvChargeList;
                SrvChargeList = Data.GetSrvChargeInfo(JourneyType.I.ToString(), ConnStr);// Get Data From DB or Cache
                int adt = 0, chd = 0, inf = 0;
                float adtComm = 0, adtComm1 = 0, adtCB = 0, adtSrvtax = 0, adtSrvtax1 = 0, adtTFee = 0, adtTds = 0;
                float chdComm = 0, chdComm1 = 0, chdCB = 0, chdSrvtax = 0, chdSrvtax1 = 0, chdTFee = 0, chdTds = 0;
                float infSrvTax = 0;
                float IATAComm = 0;
                double tdsPrcnt = 0, totalbFare = 0;
                double SrvTaxP = 0;
                string adtrbd = "", chdrbd = "", infrbd = "";
                string ddate = "", rdate = "";
                
                Dictionary<string, object> a = new Dictionary<string, object>();
                a = (Dictionary<string, object>)AirArray[0];

                adt = int.Parse(a["Adult"].ToString());
                chd = int.Parse(a["Child"].ToString());
                inf = int.Parse(a["Infant"].ToString());
                string AllFltNo = "";
                for (int i = 0; i < AirArray.Count; i++)
                {
                    Dictionary<string, object> b = new Dictionary<string, object>();
                    b = (Dictionary<string, object>)AirArray[i];
                    adtrbd += b["AdtRbd"].ToString() + ":";
                    //chdrbd += b["ChdRbd"].ToString() + ":";
                    chdrbd += chd > 0 ? b["ChdRbd"].ToString() : "" + ":";
                    AllFltNo += b["FlightIdentification"].ToString() + ",";
                    if (b["Flight"].ToString() == "1")
                    {
                        if (b["Provider"].ToString() == "1G")
                            ddate = Utility.Right(b["DepartureDate"].ToString(), 2) + Utility.Mid(b["DepartureDate"].ToString(), 4, 2) + Utility.Mid(b["DepartureDate"].ToString(), 2, 2);
                        else
                            ddate = b["DepartureDate"].ToString();
                    }
                    else if (b["Flight"].ToString() == "2")
                    {
                        if (b["Provider"].ToString() == "1G")
                            rdate = Utility.Right(b["DepartureDate"].ToString(), 2) + Utility.Mid(b["DepartureDate"].ToString(), 4, 2) + Utility.Mid(b["DepartureDate"].ToString(), 2, 2);
                        else
                            rdate = b["DepartureDate"].ToString();
                    }
                }

                List<string> uniquesFltNo = AllFltNo.Split(',').Distinct().ToList();
                int TotalSector = uniquesFltNo.Count - 1;
                if (string.IsNullOrEmpty(Convert.ToString(TotalSector)) || TotalSector == 0)
                {
                    TotalSector = 1;
                }
                if (adt > 0)
                {
                    string AdtFareType = "";
                    if (!string.IsNullOrEmpty(Convert.ToString(a["AdtFar"])))
                    {
                        AdtFareType = Convert.ToString(a["AdtFar"]);
                    }
                    else
                    {
                        AdtFareType = "NRM";
                    }


                    //CommDt = GetFltComm_Gal_Intl(GType, a["ValiDatingCarrier"].ToString(), a["Provider"].ToString() == "OF" ? 0 : decimal.Parse(a["AdtBfare"].ToString()), decimal.Parse(a["AdtFSur"].ToString()), decimal.Parse(a["AdtQ"].ToString()), a["OrgDestFrom"].ToString(), a["OrgDestTo"].ToString(), adtrbd, ddate, rdate, Convert.ToString(a["AdtCabin"]));
                    CommDt = GetFltComm_Gal_Intl(GType, a["ValiDatingCarrier"].ToString(), a["Provider"].ToString() == "OF" ? 0 : decimal.Parse(a["AdtBfare"].ToString()), decimal.Parse(a["AdtFSur"].ToString()), decimal.Parse(a["AdtQ"].ToString()), a["OrgDestFrom"].ToString(), a["OrgDestTo"].ToString(), adtrbd, ddate, rdate, Convert.ToString(a["AdtCabin"]), Convert.ToString(a["AdtFarebasis"]), Convert.ToString(a["FlightIdentification"]), Convert.ToString(a["OperatingCarrier"]), Convert.ToString(a["MarketingCarrier"]), AdtFareType, Convert.ToString(TotalSector));
                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, a["ValiDatingCarrier"].ToString(), float.Parse(CommDt.Rows[0][0].ToString()), float.Parse(a["AdtBfare"].ToString()), float.Parse(a["AdtFSur"].ToString()), TDS);

                    //IntFareDetails.Add("AdtBFare", Convert.ToDouble(a["AdtBfare"].ToString()));
                    //IntFareDetails.Add("AdtTax", Convert.ToDouble(a["AdtTax"].ToString()));
                    IntFareDetails.Add("AdtTotal", Convert.ToDouble(a["AdtFare"].ToString()));

                    adtComm1 = float.Parse(CommDt.Rows[0][0].ToString());
                    adtCB = float.Parse(CommDt.Rows[0][1].ToString());
                    //adtCB = int.Parse(CommDt.Rows[0]["CB"].ToString());
                    //adtSrvtax = float.Parse(STTFTDS["STax"].ToString());
                    if (Boolean.Parse(a["IsCorp"].ToString()))
                    { adtSrvtax = float.Parse(a["AdtSrvTax"].ToString()); }
                    else
                    {
                        adtSrvtax1 = float.Parse(STTFTDS["STax"].ToString());
                        adtSrvtax = 0;
                        adtComm = adtComm1 - adtSrvtax1;
                    }


                    adtTFee = float.Parse(STTFTDS["TFee"].ToString());
                    adtTds = float.Parse(STTFTDS["Tds"].ToString());
                    IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());
                    IntFareDetails.Add("IATAComm", IATAComm);
                }
                else
                {
                    //IntFareDetails.Add("AdtBFare", 0);
                    //IntFareDetails.Add("AdtTax", 0);
                    IntFareDetails.Add("AdtTotal", 0);
                    adtComm = 0;
                    adtCB = 0;
                    adtSrvtax = 0;
                    adtTFee = 0;
                    adtTds = 0;
                    IATAComm = 0;
                }
                if (chd > 0)
                {
                    CommDt.Clear();
                    STTFTDS.Clear();
                    string ChdFareType = "";
                    if (!string.IsNullOrEmpty(Convert.ToString(a["AdtFar"])))
                    {
                        ChdFareType = Convert.ToString(a["AdtFar"]);
                    }
                    else
                    {
                        ChdFareType = "NRM";
                    }
                    //CommDt = GetFltComm_Gal_Intl(GType, a["ValiDatingCarrier"].ToString(), a["Provider"].ToString() == "OF" ? 0 : decimal.Parse(a["ChdBFare"].ToString()), decimal.Parse(a["ChdFSur"].ToString()), decimal.Parse(a["ChdQ"].ToString()), a["OrgDestFrom"].ToString(), a["OrgDestTo"].ToString(), chdrbd, ddate, rdate, Convert.ToString(a["AdtCabin"]));
                    CommDt = GetFltComm_Gal_Intl(GType, a["ValiDatingCarrier"].ToString(), a["Provider"].ToString() == "OF" ? 0 : decimal.Parse(a["ChdBFare"].ToString()), decimal.Parse(a["ChdFSur"].ToString()), decimal.Parse(a["ChdQ"].ToString()), a["OrgDestFrom"].ToString(), a["OrgDestTo"].ToString(), chdrbd, ddate, rdate, Convert.ToString(a["AdtCabin"]), Convert.ToString(a["ChdFarebasis"]), Convert.ToString(a["FlightIdentification"]), Convert.ToString(a["OperatingCarrier"]), Convert.ToString(a["MarketingCarrier"]), ChdFareType, Convert.ToString(TotalSector));
                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, a["ValiDatingCarrier"].ToString(), float.Parse(CommDt.Rows[0][0].ToString()), float.Parse(a["ChdBFare"].ToString()), float.Parse(a["ChdFSur"].ToString()), TDS);

                    //IntFareDetails.Add("ChdBFare", Convert.ToDouble(a["ChdBFare"].ToString()));
                    //IntFareDetails.Add("ChdTax", Convert.ToDouble(a["ChdTax"].ToString()));
                    IntFareDetails.Add("ChdTotal", Convert.ToDouble(a["ChdFare"].ToString()));

                    chdComm1 = float.Parse(CommDt.Rows[0][0].ToString());
                    chdCB = float.Parse(CommDt.Rows[0][1].ToString());
                    //chdCB = int.Parse(CommDt.Rows[0]["CB"].ToString());
                    //chdSrvtax = float.Parse(STTFTDS["STax"].ToString());
                    if (Boolean.Parse(a["IsCorp"].ToString()))
                    { chdSrvtax = float.Parse(a["ChdSrvTax"].ToString()); }
                    else
                    {
                        chdSrvtax1 = float.Parse(STTFTDS["STax"].ToString());
                        chdSrvtax = 0;
                        chdComm = chdComm1 - chdSrvtax1;
                    }
                    chdTFee = float.Parse(STTFTDS["TFee"].ToString());
                    chdTds = float.Parse(STTFTDS["Tds"].ToString());
                    //IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());
                }
                else
                {
                    //IntFareDetails.Add("ChdBFare", 0);
                    //IntFareDetails.Add("ChdTax", "");
                    IntFareDetails.Add("ChdTotal", 0);

                    chdComm = 0;
                    chdCB = 0;
                    chdSrvtax = 0;
                    chdTFee = 0;
                    chdTds = 0;
                    IATAComm = 0;
                }


                // Hashtable

                if (inf > 0)
                {
                    //IntFareDetails.Add("InfBFare", Convert.ToDouble(a["InfBfare"].ToString()));
                    //IntFareDetails.Add("InfTax", Convert.ToDouble(a["InfTax"].ToString()));
                    IntFareDetails.Add("InfTotal", Convert.ToDouble(a["InfFare"].ToString()));
                    if (Boolean.Parse(a["IsCorp"].ToString()))
                        infSrvTax = float.Parse(a["InfSrvTax"].ToString());
                }
                else
                {
                    //IntFareDetails.Add("InfBFare", 0);
                    //IntFareDetails.Add("InfTax", "");
                    IntFareDetails.Add("InfTotal", 0);
                }
                //totalbFare = (Convert.ToDouble(a["AdtBfare"].ToString()) * adt + Convert.ToDouble(a["ChdBFare"].ToString()) * chd + Convert.ToDouble(a["InfBfare"].ToString()) * inf);

                //IntFareDetails.Add("TotalBfare", totalbFare);
                IntFareDetails.Add("SrvTax", ((adtSrvtax * adt) + (chdSrvtax * chd) + (infSrvTax * inf)));
                IntFareDetails.Add("TFee", (adtTFee * adt + chdTFee * chd));

                IntFareDetails.Add("adtSrvtax", adtSrvtax);
                IntFareDetails.Add("adtSrvtax1", adtSrvtax1);
                IntFareDetails.Add("adtComm", adtComm);
                IntFareDetails.Add("adtComm1", adtComm1);
                IntFareDetails.Add("chdSrvtax", chdSrvtax);
                IntFareDetails.Add("chdSrvtax1", chdSrvtax1);
                IntFareDetails.Add("chdComm", chdComm);
                IntFareDetails.Add("chdComm1", chdComm1);

                IntFareDetails.Add("adtCB", adtCB);
                IntFareDetails.Add("chdCB", chdCB);

                IntFareDetails.Add("adtTds", adtTds);
                IntFareDetails.Add("chdTds", chdTds);

                IntFareDetails.Add("totComm", (adtComm * adt) + (chdComm * chd));
                IntFareDetails.Add("totCB", (adtCB * adt) + (chdCB * chd));
                IntFareDetails.Add("totTds", (adtTds * adt) + (chdTds * chd));

                int TC = 0;
                int AgentTC = 0;
                TC = (Convert.ToInt32(a["ADTAdminMrk"].ToString()) * adt) + (Convert.ToInt32(a["CHDAdminMrk"].ToString()) * chd) + (Convert.ToInt32(a["ADTAgentMrk"].ToString()) * adt) + (Convert.ToInt32(a["CHDAgentMrk"].ToString()) * chd);
                AgentTC = (Convert.ToInt32(a["ADTAgentMrk"].ToString()) * adt) + (Convert.ToInt32(a["CHDAgentMrk"].ToString()) * chd);
                IntFareDetails.Add("TC", TC);
                int totFare = 0;
                int netFare = 0;

                if (Boolean.Parse(a["IsCorp"].ToString()))
                {
                    totFare = Convert.ToInt32((Convert.ToDouble(IntFareDetails["AdtTotal"]) * adt) + (Convert.ToDouble(IntFareDetails["ChdTotal"]) * chd) + (Convert.ToDouble(IntFareDetails["InfTotal"]) * inf));
                    totFare = totFare + Convert.ToInt32(IntFareDetails["SrvTax"]) + Convert.ToInt32(IntFareDetails["TFee"]) + AgentTC + Convert.ToInt32(a["TotMgtFee"].ToString());
                    netFare = (totFare + Convert.ToInt32(IntFareDetails["totTds"])) - (Convert.ToInt32(IntFareDetails["totComm"]) + AgentTC + Convert.ToInt32(IntFareDetails["totCB"]));

                }
                else
                {
                    totFare = Convert.ToInt32((Convert.ToDouble(IntFareDetails["AdtTotal"]) * adt) + (Convert.ToDouble(IntFareDetails["ChdTotal"]) * chd) + (Convert.ToDouble(IntFareDetails["InfTotal"]) * inf));
                    totFare = totFare + Convert.ToInt32(IntFareDetails["SrvTax"]) + Convert.ToInt32(IntFareDetails["TFee"]) + Convert.ToInt32(IntFareDetails["TC"]);
                    netFare = (totFare + Convert.ToInt32(IntFareDetails["totTds"])) - (Convert.ToInt32(IntFareDetails["totComm"]) + AgentTC + Convert.ToInt32(IntFareDetails["totCB"]));
                }
                //totFare = Convert.ToInt32((Convert.ToDouble(IntFareDetails["AdtTotal"]) * adt) + (Convert.ToDouble(IntFareDetails["ChdTotal"]) * chd) + (Convert.ToDouble(IntFareDetails["InfTotal"]) * inf));
                //totFare = totFare + Convert.ToInt32(IntFareDetails["SrvTax"]) + Convert.ToInt32(IntFareDetails["TFee"]) + Convert.ToInt32(IntFareDetails["TC"]);
                //netFare = (totFare + Convert.ToInt32(IntFareDetails["totTds"])) - (Convert.ToInt32(IntFareDetails["totComm"]) + AgentTC);
                IntFareDetails.Add("totFare", totFare);
                IntFareDetails.Add("netFare", netFare);
                
            }
            catch(Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("Error_log", "GetIntlCommGal", ex, "Flight");
                
                IntFareDetails.Add("error", ex.Message);
                
            }
            return IntFareDetails;
        }




        public Dictionary<string, object> GetDomCommGal(string GType, ArrayList AirArray, string TDS)
        {
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            List<FltSrvChargeList> SrvChargeList;
            SrvChargeList = Data.GetSrvChargeInfo(JourneyType.D.ToString(), ConnStr);// Get Data From DB or Cache
            int adt = 0, chd = 0, inf = 0;
            float adtComm = 0, adtComm1 = 0, adtCB = 0, adtSrvtax = 0, adtSrvtax1 = 0, adtTFee = 0, adtTds = 0;
            float chdComm = 0, chdComm1 = 0, chdCB = 0, chdSrvtax = 0, chdSrvtax1 = 0, chdTFee = 0, chdTds = 0;
            float infSrvTax = 0;
            float IATAComm = 0;
            double tdsPrcnt = 0, totalbFare = 0;
            double SrvTaxP = 0;
            string adtrbd = "", chdrbd = "", infrbd = "";
            string ddate = "", rdate = "";
            string  AllFltNo = "";
            Dictionary<string, object> IntFareDetails = new Dictionary<string, object>();
            Dictionary<string, object> a = new Dictionary<string, object>();
            a = (Dictionary<string, object>)AirArray[0];

            adt = int.Parse(a["Adult"].ToString());
            chd = int.Parse(a["Child"].ToString());
            inf = int.Parse(a["Infant"].ToString());

            for (int i = 0; i < AirArray.Count; i++)
            {
                Dictionary<string, object> b = new Dictionary<string, object>();
                b = (Dictionary<string, object>)AirArray[i];
                adtrbd += b["AdtRbd"].ToString() + ":";
                //chdrbd += b["ChdRbd"].ToString() + ":";
                chdrbd += chd > 0 ? b["ChdRbd"].ToString() : "" + ":";
                AllFltNo += b["FlightIdentification"].ToString() + ",";
                //if (AirArray.Count>1)
                //{                    
                //    AllFltNo += b["FlightIdentification"].ToString() + ",";
                //}                
                //else
                //{
                //    AllFltNo = b["FlightIdentification"].ToString(); 
                //}
                    


                if (b["Flight"].ToString() == "1")
                {
                    if (b["Provider"].ToString() == "1G")
                        ddate = Utility.Right(b["DepartureDate"].ToString(), 2) + Utility.Mid(b["DepartureDate"].ToString(), 4, 2) + Utility.Mid(b["DepartureDate"].ToString(), 2, 2);
                    else
                        ddate = b["DepartureDate"].ToString();
                }
                else if (b["Flight"].ToString() == "2")
                {
                    if (b["Provider"].ToString() == "1G")
                        rdate = Utility.Right(b["DepartureDate"].ToString(), 2) + Utility.Mid(b["DepartureDate"].ToString(), 4, 2) + Utility.Mid(b["DepartureDate"].ToString(), 2, 2);
                    else
                        rdate = b["DepartureDate"].ToString();
                }
            }
            //List<string> uniquesFltNo = AllFltNo.Split(',').Reverse().Distinct().Take(2).Reverse().ToList();
            List<string> uniquesFltNo = AllFltNo.Split(',').Distinct().ToList();
            int TotalSector = uniquesFltNo.Count-1;
            if (string.IsNullOrEmpty(Convert.ToString(TotalSector)) || TotalSector==0)
            {
                TotalSector = 1;
            }
            if (adt > 0)
            {
                string AdtFareType = "";
                if (!string.IsNullOrEmpty(Convert.ToString(a["AdtFar"])))
                {
                    AdtFareType = Convert.ToString(a["AdtFar"]);
                }
                else
                {
                    AdtFareType = "NRM";
                }


                //CommDt = GetFltComm_Gal_Intl(GType, a["ValiDatingCarrier"].ToString(), a["Provider"].ToString() == "OF" ? 0 : decimal.Parse(a["AdtBfare"].ToString()), decimal.Parse(a["AdtFSur"].ToString()), decimal.Parse(a["AdtQ"].ToString()), a["OrgDestFrom"].ToString(), a["OrgDestTo"].ToString(), adtrbd, ddate, rdate, Convert.ToString(a["AdtCabin"]));
               // CommDt = GetFltComm_Gal_Intl(GType, a["ValiDatingCarrier"].ToString(), a["Provider"].ToString() == "OF" ? 0 : decimal.Parse(a["AdtBfare"].ToString()), decimal.Parse(a["AdtFSur"].ToString()), decimal.Parse(a["AdtQ"].ToString()), a["OrgDestFrom"].ToString(), a["OrgDestTo"].ToString(), adtrbd, ddate, rdate, Convert.ToString(a["AdtCabin"]), Convert.ToString(a["AdtFarebasis"]), Convert.ToString(a["FlightIdentification"]), Convert.ToString(a["OperatingCarrier"]), Convert.ToString(a["MarketingCarrier"]), AdtFareType, "");
                //CommDt = GetFltComm_Gal(GType, a["ValiDatingCarrier"].ToString() + AdtFareType, (a["Provider"].ToString() == "OF" ? 0 : decimal.Parse(a["AdtBfare"].ToString())), decimal.Parse(a["AdtFSur"].ToString()), 1, adtrbd, Convert.ToString(a["AdtCabin"].ToString()), ddate, a["OrgDestFrom"].ToString() + "-" + a["OrgDestTo"].ToString(), rdate, Convert.ToString(a["AdtFarebasis"]), "", "", Convert.ToString(a["FlightIdentification"]), Convert.ToString(a["OperatingCarrier"]), Convert.ToString(a["MarketingCarrier"]), AdtFareType, "");
                CommDt = GetFltComm_Gal(GType, a["ValiDatingCarrier"].ToString(), (a["Provider"].ToString() == "OF" ? 0 : decimal.Parse(a["AdtBfare"].ToString())), decimal.Parse(a["AdtFSur"].ToString()), 1, adtrbd, Convert.ToString(a["AdtCabin"].ToString()), ddate, a["OrgDestFrom"].ToString() + "-" + a["OrgDestTo"].ToString(), rdate, Convert.ToString(a["AdtFarebasis"]), "", "", Convert.ToString(a["FlightIdentification"]), Convert.ToString(a["OperatingCarrier"]), Convert.ToString(a["MarketingCarrier"]), AdtFareType,  Convert.ToString(TotalSector));                
                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, a["ValiDatingCarrier"].ToString(), float.Parse(CommDt.Rows[0][0].ToString()), float.Parse(a["AdtBfare"].ToString()), float.Parse(a["AdtFSur"].ToString()), TDS);

                //IntFareDetails.Add("AdtBFare", Convert.ToDouble(a["AdtBfare"].ToString()));
                //IntFareDetails.Add("AdtTax", Convert.ToDouble(a["AdtTax"].ToString()));
                IntFareDetails.Add("AdtTotal", Convert.ToDouble(a["AdtFare"].ToString()));

                adtComm1 = float.Parse(CommDt.Rows[0][0].ToString());
                adtCB = float.Parse(CommDt.Rows[0][1].ToString());
                //adtCB = int.Parse(CommDt.Rows[0]["CB"].ToString());
                //adtSrvtax = float.Parse(STTFTDS["STax"].ToString());
                if (Boolean.Parse(a["IsCorp"].ToString()))
                { adtSrvtax = float.Parse(a["AdtSrvTax"].ToString()); }
                else
                {
                    adtSrvtax1 = float.Parse(STTFTDS["STax"].ToString());
                    adtSrvtax = 0;
                    adtComm = adtComm1 - adtSrvtax1;
                }


                adtTFee = float.Parse(STTFTDS["TFee"].ToString());
                adtTds = float.Parse(STTFTDS["Tds"].ToString());
                IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());               
                IntFareDetails.Add("IATAComm", IATAComm);
            }
            else
            {
                //IntFareDetails.Add("AdtBFare", 0);
                //IntFareDetails.Add("AdtTax", 0);
                IntFareDetails.Add("AdtTotal", 0);
                adtComm = 0;
                adtCB = 0;
                adtSrvtax = 0;
                adtTFee = 0;
                adtTds = 0;
                IATAComm = 0;
            }
            if (chd > 0)
            {
                CommDt.Clear();
                STTFTDS.Clear();
                string ChdFareType = "";
                if (!string.IsNullOrEmpty(Convert.ToString(a["AdtFar"])))
                {
                    ChdFareType = Convert.ToString(a["AdtFar"]);
                }
                else
                {
                    ChdFareType = "NRM";
                }
                //CommDt = GetFltComm_Gal_Intl(GType, a["ValiDatingCarrier"].ToString(), a["Provider"].ToString() == "OF" ? 0 : decimal.Parse(a["ChdBFare"].ToString()), decimal.Parse(a["ChdFSur"].ToString()), decimal.Parse(a["ChdQ"].ToString()), a["OrgDestFrom"].ToString(), a["OrgDestTo"].ToString(), chdrbd, ddate, rdate, Convert.ToString(a["AdtCabin"]));
                //CommDt = GetFltComm_Gal_Intl(GType, a["ValiDatingCarrier"].ToString(), a["Provider"].ToString() == "OF" ? 0 : decimal.Parse(a["ChdBFare"].ToString()), decimal.Parse(a["ChdFSur"].ToString()), decimal.Parse(a["ChdQ"].ToString()), a["OrgDestFrom"].ToString(), a["OrgDestTo"].ToString(), chdrbd, ddate, rdate, Convert.ToString(a["AdtCabin"]), Convert.ToString(a["ChdFarebasis"]), Convert.ToString(a["FlightIdentification"]), Convert.ToString(a["OperatingCarrier"]), Convert.ToString(a["MarketingCarrier"]), ChdFareType, "");
                //CommDt = GetFltComm_Gal(GType, a["ValiDatingCarrier"].ToString() + ChdFareType, (a["Provider"].ToString() == "OF" ? 0 : decimal.Parse(a["ChdBFare"].ToString())), decimal.Parse(a["ChdFSur"].ToString()), 1, chdrbd, Convert.ToString(a["ChdCabin"].ToString()), ddate, a["OrgDestFrom"].ToString() + "-" + a["OrgDestTo"].ToString(), rdate, Convert.ToString(a["ChdFarebasis"]), "", "", Convert.ToString(a["FlightIdentification"]), Convert.ToString(a["OperatingCarrier"]), Convert.ToString(a["MarketingCarrier"]), ChdFareType, "");
                CommDt = GetFltComm_Gal(GType, a["ValiDatingCarrier"].ToString(), (a["Provider"].ToString() == "OF" ? 0 : decimal.Parse(a["ChdBFare"].ToString())), decimal.Parse(a["ChdFSur"].ToString()), 1, chdrbd, Convert.ToString(a["ChdCabin"].ToString()), ddate, a["OrgDestFrom"].ToString() + "-" + a["OrgDestTo"].ToString(), rdate, Convert.ToString(a["ChdFarebasis"]), "", "", Convert.ToString(a["FlightIdentification"]), Convert.ToString(a["OperatingCarrier"]), Convert.ToString(a["MarketingCarrier"]), ChdFareType, Convert.ToString(TotalSector));
                
                STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, a["ValiDatingCarrier"].ToString(), float.Parse(CommDt.Rows[0][0].ToString()), float.Parse(a["ChdBFare"].ToString()), float.Parse(a["ChdFSur"].ToString()), TDS);

                //IntFareDetails.Add("ChdBFare", Convert.ToDouble(a["ChdBFare"].ToString()));
                //IntFareDetails.Add("ChdTax", Convert.ToDouble(a["ChdTax"].ToString()));
                IntFareDetails.Add("ChdTotal", Convert.ToDouble(a["ChdFare"].ToString()));

                chdComm1 = float.Parse(CommDt.Rows[0][0].ToString());
                chdCB = float.Parse(CommDt.Rows[0][1].ToString());
                //chdCB = int.Parse(CommDt.Rows[0]["CB"].ToString());
                //chdSrvtax = float.Parse(STTFTDS["STax"].ToString());
                if (Boolean.Parse(a["IsCorp"].ToString()))
                { chdSrvtax = float.Parse(a["ChdSrvTax"].ToString()); }
                else
                {
                    chdSrvtax1 = float.Parse(STTFTDS["STax"].ToString());
                    chdSrvtax = 0;
                    chdComm = chdComm1 - chdSrvtax1;
                }
                chdTFee = float.Parse(STTFTDS["TFee"].ToString());
                chdTds = float.Parse(STTFTDS["Tds"].ToString());
                //IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());
            }
            else
            {
                //IntFareDetails.Add("ChdBFare", 0);
                //IntFareDetails.Add("ChdTax", "");
                IntFareDetails.Add("ChdTotal", 0);

                chdComm = 0;
                chdCB = 0;
                chdSrvtax = 0;
                chdTFee = 0;
                chdTds = 0;
                IATAComm = 0;
            }


            // Hashtable

            if (inf > 0)
            {
                //IntFareDetails.Add("InfBFare", Convert.ToDouble(a["InfBfare"].ToString()));
                //IntFareDetails.Add("InfTax", Convert.ToDouble(a["InfTax"].ToString()));
                IntFareDetails.Add("InfTotal", Convert.ToDouble(a["InfFare"].ToString()));
                if (Boolean.Parse(a["IsCorp"].ToString()))
                    infSrvTax = float.Parse(a["InfSrvTax"].ToString());
            }
            else
            {
                //IntFareDetails.Add("InfBFare", 0);
                //IntFareDetails.Add("InfTax", "");
                IntFareDetails.Add("InfTotal", 0);
            }
            //totalbFare = (Convert.ToDouble(a["AdtBfare"].ToString()) * adt + Convert.ToDouble(a["ChdBFare"].ToString()) * chd + Convert.ToDouble(a["InfBfare"].ToString()) * inf);

            //IntFareDetails.Add("TotalBfare", totalbFare);
            IntFareDetails.Add("SrvTax", ((adtSrvtax * adt) + (chdSrvtax * chd) + (infSrvTax * inf)));
            IntFareDetails.Add("TFee", (adtTFee * adt + chdTFee * chd));

            IntFareDetails.Add("adtSrvtax", adtSrvtax);
            IntFareDetails.Add("adtSrvtax1", adtSrvtax1);
            IntFareDetails.Add("adtComm", adtComm);
            IntFareDetails.Add("adtComm1", adtComm1);
            IntFareDetails.Add("chdSrvtax", chdSrvtax);
            IntFareDetails.Add("chdSrvtax1", chdSrvtax1);
            IntFareDetails.Add("chdComm", chdComm);
            IntFareDetails.Add("chdComm1", chdComm1);

            IntFareDetails.Add("adtCB", adtCB);
            IntFareDetails.Add("chdCB", chdCB);

            IntFareDetails.Add("adtTds", adtTds);
            IntFareDetails.Add("chdTds", chdTds);

            IntFareDetails.Add("totComm", (adtComm * adt) + (chdComm * chd));
            IntFareDetails.Add("totCB", (adtCB * adt) + (chdCB * chd));
            IntFareDetails.Add("totTds", (adtTds * adt) + (chdTds * chd));

            int TC = 0;
            int AgentTC = 0;
            TC = (Convert.ToInt32(a["ADTAdminMrk"].ToString()) * adt) + (Convert.ToInt32(a["CHDAdminMrk"].ToString()) * chd) + (Convert.ToInt32(a["ADTAgentMrk"].ToString()) * adt) + (Convert.ToInt32(a["CHDAgentMrk"].ToString()) * chd);
            AgentTC = (Convert.ToInt32(a["ADTAgentMrk"].ToString()) * adt) + (Convert.ToInt32(a["CHDAgentMrk"].ToString()) * chd);
            IntFareDetails.Add("TC", TC);
            int totFare = 0;
            int netFare = 0;

            if (Boolean.Parse(a["IsCorp"].ToString()))
            {
                totFare = Convert.ToInt32((Convert.ToDouble(IntFareDetails["AdtTotal"]) * adt) + (Convert.ToDouble(IntFareDetails["ChdTotal"]) * chd) + (Convert.ToDouble(IntFareDetails["InfTotal"]) * inf));
                totFare = totFare + Convert.ToInt32(IntFareDetails["SrvTax"]) + Convert.ToInt32(IntFareDetails["TFee"]) + AgentTC + Convert.ToInt32(a["TotMgtFee"].ToString());
                netFare = (totFare + Convert.ToInt32(IntFareDetails["totTds"])) - (Convert.ToInt32(IntFareDetails["totComm"]) + AgentTC + Convert.ToInt32(IntFareDetails["totCB"]));
            }
            else
            {
                totFare = Convert.ToInt32((Convert.ToDouble(IntFareDetails["AdtTotal"]) * adt) + (Convert.ToDouble(IntFareDetails["ChdTotal"]) * chd) + (Convert.ToDouble(IntFareDetails["InfTotal"]) * inf));
                totFare = totFare + Convert.ToInt32(IntFareDetails["SrvTax"]) + Convert.ToInt32(IntFareDetails["TFee"]) + Convert.ToInt32(IntFareDetails["TC"]);
                netFare = (totFare + Convert.ToInt32(IntFareDetails["totTds"])) - (Convert.ToInt32(IntFareDetails["totComm"]) + AgentTC + Convert.ToInt32(IntFareDetails["totCB"]));
            }
            //totFare = Convert.ToInt32((Convert.ToDouble(IntFareDetails["AdtTotal"]) * adt) + (Convert.ToDouble(IntFareDetails["ChdTotal"]) * chd) + (Convert.ToDouble(IntFareDetails["InfTotal"]) * inf));
            //totFare = totFare + Convert.ToInt32(IntFareDetails["SrvTax"]) + Convert.ToInt32(IntFareDetails["TFee"]) + Convert.ToInt32(IntFareDetails["TC"]);
            //netFare = (totFare + Convert.ToInt32(IntFareDetails["totTds"])) - (Convert.ToInt32(IntFareDetails["totComm"]) + AgentTC);
            IntFareDetails.Add("totFare", totFare);
            IntFareDetails.Add("netFare", netFare);
            return IntFareDetails;

        }

        public ArrayList MereFare_GalIntl(ArrayList AirArray, Dictionary<string, object> FareData)
        {
            ArrayList Final = new ArrayList();
            for (int i = 0; i < AirArray.Count; i++)
            {
                Dictionary<string, object> Rec = new Dictionary<string, object>();
                Rec = (Dictionary<string, object>)AirArray[i];
                //Fields For Fare BreakUp
                Rec["STax"] = FareData["SrvTax"];
                Rec["TFee"] = FareData["TFee"];
                Rec["AdtSrvTax"] = FareData["adtSrvtax"];
                Rec["ChdSrvTax"] = FareData["chdSrvtax"];
                Rec["AdtDiscount"] = FareData["adtComm"];
                Rec["ChdDiscount"] = FareData["chdComm"];
                Rec["AdtSrvTax1"] = FareData["adtSrvtax1"];
                Rec["ChdSrvTax1"] = FareData["chdSrvtax1"];
                Rec["AdtDiscount1"] = FareData["adtComm1"];
                Rec["ChdDiscount1"] = FareData["chdComm1"];
                Rec["AdtCB"] = FareData["adtCB"];
                Rec["ChdCB"] = FareData["chdCB"];
                Rec["AdtTds"] = FareData["adtTds"];
                Rec["ChdTds"] = FareData["chdTds"];
                Rec["TotDis"] = FareData["totComm"];
                Rec["TotCB"] = FareData["totCB"];
                Rec["TotTds"] = FareData["totTds"];
                Rec["TotMrkUp"] = FareData["TC"];
                Rec["TotalFare"] = FareData["totFare"];
                Rec["NetFare"] = FareData["netFare"];
                Rec["IATAComm"] = FareData["IATAComm"];
                Final.Add(Rec);
            }
            return Final;
        }

        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;
           // int IATAComm = 0;
            Hashtable STHT = new Hashtable();
            try
            {
                List<FltSrvChargeList> StNew =(from st in SrvchargeList where st.AirlineCode == VC select st).ToList();
                if (StNew.Count > 0)
                {
                    STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                    TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                    IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                }
                STHT.Add("STax", Math.Round(((decimal.Parse(Dis.ToString()) * STaxP) / 100), 0));
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                //STHT.Add("Tds", Math.Round((double.Parse(Dis.ToString()) * double.Parse(TDS)) / 100, 0));
                STHT.Add("Tds", Math.Round(((double.Parse(Dis.ToString()) - double.Parse(STHT["STax"].ToString())) * double.Parse(TDS)) / 100, 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }

        public DataTable clac_MgtFee(string GPType, string VC, decimal BaseFare, decimal YQ, string Trip, decimal Total)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.clac_MgtFeeFromDB(GPType, VC, BaseFare, YQ, Trip, Total);
        }

        public float GetMiscServiceCharge(string Trip, string VC, string Agentid, string type, string org, string dest)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.GetMiscServiceChargeFromDB(Trip, VC, Agentid, type, org, dest);

        }
        public DataTable GetFQCSDetails()
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.GetFQCSDeatailsFromDB();
        }

        public List<BaggageList> GetBaggage(string Trip, string Airline, bool Bag)
        {
            FlightCommonDAL objFltCommon = new FlightCommonDAL(ConnStr);
            return objFltCommon.GetBaggage(Trip, Airline, Bag);
        }
        public List<BaggageList> GetBaggages(string Trip, string Airline, bool Bag, string CountryFromCode, string CountryToCode)
        {
            FlightCommonDAL objFltCommon = new FlightCommonDAL(ConnStr);
            return objFltCommon.GetBaggages(Trip, Airline, Bag, CountryFromCode, CountryToCode);
        }


        public DataTable GetofflineResult(string Org, string Dest, string depDate, string vc, string Trip)
        {
            FlightCommonDAL objFltCommon = new FlightCommonDAL(ConnStr);
            return objFltCommon.GetofflineResultDal(Org, Dest, depDate, vc, Trip);
        }
        //Cache Update
        public List<MISCCharges> GetMiscCharges(string Trip, string VC, string Agentid, string type, string org, string dest)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.GetMiscCharges(Trip, VC, Agentid, type, org, dest);

        }
        public float MISCServiceFee(List<MISCCharges> MiscList, string Airline,string FareType, string BaseFareAmt, string YQAmt)
        {
            float srvCharge = 0; 
            try
            {
                List<MISCCharges> MarkupList = null;
                if (MiscList.Count > 0)
                {

                    bool AirExist = MiscList.Any(Air => Air.Airline.ToUpper() == Airline);
                    if (MiscList.Any(Air => Air.Airline.ToUpper() == Airline && Air.FareType.ToUpper() == FareType.ToUpper()))
                    {
                        MarkupList = MiscList.Where(Air => Air.Airline.ToUpper() == Airline && Air.FareType.ToUpper() == FareType.ToUpper()).ToList();
                        if (MarkupList.Count > 0 && MarkupList[0].MarkupType == "P")
                        {
                            srvCharge = MISCChargePercentage(float.Parse(BaseFareAmt), float.Parse(YQAmt), float.Parse(MarkupList[0].CommisionOnBasic.ToString()), float.Parse(MarkupList[0].CommissionOnYq.ToString()), float.Parse(MarkupList[0].CommisionOnBasicYq.ToString()));
                        }
                        else if (MarkupList.Count > 0 && MarkupList[0].MarkupType == "F")
                        {
                            srvCharge = float.Parse(MarkupList[0].Amount.ToString());
                        }
                        else
                        {
                            srvCharge = 0;
                        }

                    }
                    else if (MiscList.Any(Air => Air.Airline.ToUpper() == "ALL" && Air.FareType.ToUpper() == FareType.ToUpper()))
                    {
                        MarkupList = null;
                        MarkupList = MiscList.Where(Air => Air.Airline.ToUpper() == "ALL" && Air.FareType.ToUpper() == FareType.ToUpper()).ToList();
                        if (MarkupList.Count > 0 && MarkupList[0].MarkupType == "P")
                        {
                            srvCharge = MISCChargePercentage(float.Parse(BaseFareAmt), float.Parse(YQAmt), float.Parse(MarkupList[0].CommisionOnBasic.ToString()), float.Parse(MarkupList[0].CommissionOnYq.ToString()), float.Parse(MarkupList[0].CommisionOnBasicYq.ToString()));
                        }
                        else if (MarkupList.Count > 0 && MarkupList[0].MarkupType == "F")
                        {
                            srvCharge = float.Parse(MarkupList[0].Amount.ToString());
                        }
                        else
                        {
                            srvCharge = 0;
                        }
                    }
                    else if (MiscList.Any(Air => Air.Airline.ToUpper() == Airline && Air.FareType.ToUpper() == "ALL"))
                    {
                        MarkupList = null;
                        MarkupList = MiscList.Where(Air => Air.Airline.ToUpper() == Airline && Air.FareType.ToUpper() == "ALL").ToList();
                        if (MarkupList.Count > 0 && MarkupList[0].MarkupType == "P")
                        {
                            srvCharge = MISCChargePercentage(float.Parse(BaseFareAmt), float.Parse(YQAmt), float.Parse(MarkupList[0].CommisionOnBasic.ToString()), float.Parse(MarkupList[0].CommissionOnYq.ToString()), float.Parse(MarkupList[0].CommisionOnBasicYq.ToString()));
                        }
                        else if (MarkupList.Count > 0 && MarkupList[0].MarkupType == "F")
                        {
                            srvCharge = float.Parse(MarkupList[0].Amount.ToString());
                        }
                        else
                        {
                            srvCharge = 0;
                        }
                    }
                    else if (MiscList.Any(Air => Air.Airline.ToUpper() == "ALL" && Air.FareType.ToUpper() == "ALL"))
                    {
                        MarkupList = null;
                        MarkupList = MiscList.Where(Air => Air.Airline.ToUpper() == "ALL" && Air.FareType.ToUpper() == "ALL").ToList();
                        if (MarkupList.Count > 0 && MarkupList[0].MarkupType == "P")
                        {
                            srvCharge = MISCChargePercentage(float.Parse(BaseFareAmt), float.Parse(YQAmt), float.Parse(MarkupList[0].CommisionOnBasic.ToString()), float.Parse(MarkupList[0].CommissionOnYq.ToString()), float.Parse(MarkupList[0].CommisionOnBasicYq.ToString()));
                        }
                        else if (MarkupList.Count > 0 && MarkupList[0].MarkupType == "F")
                        {
                            srvCharge = float.Parse(MarkupList[0].Amount.ToString());
                        }
                        else
                        {
                            srvCharge = 0;
                        }
                    }
                    else
                    {
                        srvCharge = 0;// MiscList.Any(Air => Air.Airline.ToUpper() == "ALL") == true ? ((from misc in MiscList where misc.Airline.ToUpper() == "ALL" select misc).ToList())[0].Amount : 0;
                    }
                    srvCharge = float.Parse(Math.Round(srvCharge).ToString());
                    //bool AirExist = MiscList.Any(Air => Air.Airline.ToUpper() == Airline);
                    //if (AirExist == true)
                    //    srvCharge = ((from misc in MiscList where misc.Airline.ToUpper() == Airline select misc).ToList())[0].Amount;
                    //else
                    //    srvCharge = MiscList.Any(Air => Air.Airline.ToUpper() == "ALL") == true ? ((from misc in MiscList where misc.Airline.ToUpper() == "ALL" select misc).ToList())[0].Amount : 0;
                }
            }
            catch(Exception ex)
            {
                srvCharge = 0;
            }
             
            
            return srvCharge;
        }

        public float MISCChargePercentage(float BasicFareAmt, float YQAmt, float MarkupBasic, float MarkupYQ, float MarkupBasicYQ)
        {
            float SrvCharge = 0;
            float SrvChargeBasic = 0;
            float SrvChargeYQ = 0;
            float SrvChargeBasicYQ = 0;
            try
            {
                if (MarkupBasic > 0)
                {
                    SrvChargeBasic = (BasicFareAmt * MarkupBasic) / 100;
                }
                if (MarkupYQ > 0)
                {
                    SrvChargeYQ =( YQAmt * MarkupYQ) / 100;
                }
                if (MarkupYQ > 0)
                {
                    SrvChargeBasicYQ = ((BasicFareAmt + YQAmt) * (MarkupBasicYQ ))/ 100;
                }
            }
            catch(Exception ex)
            {

            }
            SrvCharge = SrvChargeBasic + SrvChargeYQ + SrvChargeBasicYQ;
            return SrvCharge;

        }
        public DataSet GetCacheTable(string Sector, string Searchdate, string Url, string UrlR, string Airline, string RetDate, bool IsRoundTrip)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.GetCacheTable(Sector, Searchdate, Url, UrlR, Airline, RetDate, IsRoundTrip);
        }
        public string GetActiveAirlineProvider(string org, string dest, string airline, string rtf, string trip, string agentID, DateTime DepDate, DateTime RetDate)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.GetActiveAirlineProvider(org, dest, airline, rtf, trip, agentID, DepDate, RetDate);
        }
        public List<UserFlightSearch> GetUserFlightSearch(string UserId, string UserType)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.GetUserFlightSearch(UserId, UserType);

        }


        public int InsertTBOBookingLog(string airline, string orderId, string pnr, string bookReq, string bookResp, string RePriceReq, string RePriceRes, string exep)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.InsertTBOBookingLog(airline, orderId, pnr, bookReq, bookResp, RePriceReq, RePriceRes, exep);
        }
        public DataSet GetSequenceTable(string Sector, string Searchdate, string Airline, string RetDate, bool IsRoundTrip)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.GetSequenceTable(Sector, Searchdate, Airline, RetDate, IsRoundTrip);
        }

        public DataSet GetSequenceTablebydate(string Sector, string Searchdate, string Airline, string RetDate, bool IsRoundTrip,string triptype,string trip)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.GetSequenceTablebydate(Sector, Searchdate, Airline, RetDate, IsRoundTrip, triptype, trip);
        }
        

        public int InsertYABookingLog(string airline, string orderId, string pnr, string bookReq, string bookResp, string RePriceReq, string RePriceRes, string exep)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.InsertYABookingLog(airline, orderId, pnr, bookReq, bookResp, RePriceReq, RePriceRes, exep);
        }
        public DataSet GetSearchCredentilas(string Provider)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.GetSearchCredentilas(Provider);
        }        
        public List<FlightSearchResults> AddFlightKey(List<FlightSearchResults> FltSearchResult, bool isSplFare)
        {

            if (FltSearchResult.Count > 0)
            {
                List<int> linnum = FltSearchResult.Select(x => x.LineNumber).Distinct().OrderBy(x => x).ToList();

                if (isSplFare)
                {

                    for (int i = 0; i < linnum.Count; i++)
                    {
                        StringBuilder key1 = new StringBuilder();
                        StringBuilder key2 = new StringBuilder();

                        StringBuilder superkey1 = new StringBuilder();
                        StringBuilder superkey2 = new StringBuilder();

                        FltSearchResult.Where(x => x.LineNumber == linnum[i] && x.Flight == "1").ToList().ForEach(x =>
                        {

                            key1.Append(x.FlightIdentification.ToString().Trim());
                            key1.Append(x.AdtCabin.ToString().Trim());
                            key1.Append(x.ValiDatingCarrier.ToString().Trim());
                            key1.Append(x.Stops.ToString().Trim());
                            key1.Append(x.DepartureDate.ToString().Trim());
                            key1.Append(x.ArrivalDate.ToString().Trim());
                            key1.Append(x.DepartureTime.ToString().Trim());
                            key1.Append(x.ArrivalTime.ToString().Trim());
                            key1.Append(x.AdtFar.ToString().Trim());
                            key1.Append(x.IsSMEFare.ToString().Trim());
                            key1.Append(x.IsBagFare.ToString().Trim());

                            #region Add for SLineNumber Date:05-02-2019
                            superkey1.Append(x.FlightIdentification.ToString().Trim());
                            superkey1.Append(x.AdtCabin.ToString().Trim());
                            superkey1.Append(x.ValiDatingCarrier.ToString().Trim());
                            superkey1.Append(x.Stops.ToString().Trim());
                            superkey1.Append(x.DepartureDate.ToString().Trim());
                            superkey1.Append(x.ArrivalDate.ToString().Trim());
                            superkey1.Append(x.DepartureTime.ToString().Trim());
                            superkey1.Append(x.ArrivalTime.ToString().Trim());
                            #endregion

                        });

                        FltSearchResult.Where(x => x.LineNumber == linnum[i] && x.Flight == "1").ToList().ForEach(x =>
                        {
                            x.SubKey = key1.ToString();
                            x.SubSLineNumber = superkey1.ToString();
                        });


                        FltSearchResult.Where(x => x.LineNumber == linnum[i] && x.Flight == "2").ToList().ForEach(x =>
                        {

                            key2.Append(x.FlightIdentification.ToString().Trim());
                            key2.Append(x.AdtCabin.ToString().Trim());
                            key2.Append(x.ValiDatingCarrier.ToString().Trim());
                            key2.Append(x.Stops.ToString().Trim());
                            key2.Append(x.DepartureDate.ToString().Trim());
                            key2.Append(x.ArrivalDate.ToString().Trim());
                            key2.Append(x.DepartureTime.ToString().Trim());
                            key2.Append(x.ArrivalTime.ToString().Trim());
                            key2.Append(x.AdtFar.ToString().Trim());
                            key2.Append(x.IsSMEFare.ToString().Trim());
                            key2.Append(x.IsBagFare.ToString().Trim());

                            #region Add for SLineNumber Date:05-02-2019
                            superkey2.Append(x.FlightIdentification.ToString().Trim());
                            superkey2.Append(x.AdtCabin.ToString().Trim());
                            superkey2.Append(x.ValiDatingCarrier.ToString().Trim());
                            superkey2.Append(x.Stops.ToString().Trim());
                            superkey2.Append(x.DepartureDate.ToString().Trim());
                            superkey2.Append(x.ArrivalDate.ToString().Trim());
                            superkey2.Append(x.DepartureTime.ToString().Trim());
                            superkey2.Append(x.ArrivalTime.ToString().Trim());
                            #endregion
                        });

                        FltSearchResult.Where(x => x.LineNumber == linnum[i] && x.Flight == "2").ToList().ForEach(x =>
                        {
                            x.SubKey = key2.ToString();
                            x.SubSLineNumber = superkey2.ToString();
                        });



                        StringBuilder key = new StringBuilder();
                        StringBuilder superkey = new StringBuilder();
                        FltSearchResult.Where(x => x.LineNumber == linnum[i]).ToList().ForEach(x =>
                        {

                            key.Append(x.FlightIdentification.ToString().Trim());
                            key.Append(x.AdtCabin.ToString().Trim());
                            key.Append(x.ValiDatingCarrier.ToString().Trim());
                            key.Append(x.Stops.ToString().Trim());
                            key.Append(x.DepartureDate.ToString().Trim());
                            key.Append(x.ArrivalDate.ToString().Trim());
                            key.Append(x.DepartureTime.ToString().Trim());
                            key.Append(x.ArrivalTime.ToString().Trim());
                            key.Append(x.AdtFar.ToString().Trim());
                            key.Append(x.IsSMEFare.ToString().Trim());
                            key.Append(x.IsBagFare.ToString().Trim());

                            #region Add for SLineNumber Date:05-02-2019
                            //superkey.Append(x.FlightIdentification.ToString().Trim());
                            //superkey.Append(x.AdtCabin.ToString().Trim());
                            //superkey.Append(x.ValiDatingCarrier.ToString().Trim());
                            //superkey.Append(x.Stops.ToString().Trim());
                            //superkey.Append(x.DepartureDate.ToString().Trim());
                            //superkey.Append(x.ArrivalDate.ToString().Trim());
                            //superkey.Append(x.DepartureTime.ToString().Trim());
                            //superkey.Append(x.ArrivalTime.ToString().Trim());
                            #endregion
                        });

                        FltSearchResult.Where(x => x.LineNumber == linnum[i]).ToList().ForEach(x =>
                        {
                            x.MainKey = key.ToString();
                            //x.SLineNumber= superkey.ToString();
                        });

                    }


                }
                else
                {

                    for (int i = 0; i < linnum.Count; i++)
                    {
                        StringBuilder key = new StringBuilder();
                        StringBuilder superkey = new StringBuilder();
                        FltSearchResult.Where(x => x.LineNumber == linnum[i]).ToList().ForEach(x =>
                        {
                            key.Append(x.FlightIdentification.ToString().Trim());
                            key.Append(x.AdtCabin.ToString().Trim());
                            key.Append(x.ValiDatingCarrier.ToString().Trim());
                            key.Append(x.Stops.ToString().Trim());
                            key.Append(x.DepartureDate.ToString().Trim());
                            key.Append(x.ArrivalDate.ToString().Trim());
                            key.Append(x.DepartureTime.ToString().Trim());
                            key.Append(x.ArrivalTime.ToString().Trim());
                            key.Append(x.AdtFar.ToString().Trim());
                            key.Append(x.IsSMEFare.ToString().Trim());
                            key.Append(x.IsBagFare.ToString().Trim());

                            superkey.Append(x.FlightIdentification.ToString().Trim());
                            superkey.Append(x.AdtCabin.ToString().Trim());
                            superkey.Append(x.ValiDatingCarrier.ToString().Trim());
                            superkey.Append(x.Stops.ToString().Trim());
                            superkey.Append(x.DepartureDate.ToString().Trim());
                            superkey.Append(x.ArrivalDate.ToString().Trim());
                            superkey.Append(x.DepartureTime.ToString().Trim());
                            superkey.Append(x.ArrivalTime.ToString().Trim());
                           
                        });

                        FltSearchResult.Where(x => x.LineNumber == linnum[i]).ToList().ForEach(x =>
                        {
                            x.MainKey = key.ToString();
                            x.SubKey = key.ToString();

                           // x.SLineNumber = superkey.ToString();
                            x.SubSLineNumber = superkey.ToString();
                        });
                    }


                }

            }


            SetNullValueToEmptyString(FltSearchResult);

            return FltSearchResult;
        }


        public DataSet InserAndGetYAPricingLog(string orderId, string priceReq, string priceRes, string cmdType)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.InserAndGetYAPricingLog(orderId, priceReq, priceRes, cmdType);


        }


        public int AddFareRule(string orderId, string fareRule)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.AddFareRule(orderId, fareRule);
        }

        public void SetNullValueToEmptyString<T>(IList<T> data)
        {
            System.ComponentModel.PropertyDescriptorCollection properties =
              System.ComponentModel.TypeDescriptor.GetProperties(typeof(T));

            foreach (T item in data)
            {

                foreach (System.ComponentModel.PropertyDescriptor prop in properties)
                {
                    if (prop.GetValue(item) == null)
                    {
                        prop.SetValue(item, "");
                    }
                    #region Fare Rule
                    if (prop.Name == "FareRule" || prop.Name == "FareDet" || prop.Name == "AdtBreakPoint" || prop.Name == "ChdBreakPoint" || prop.Name == "InfBreakPoint")
                        prop.SetValue(item, "");
                    #endregion
                }


            }
        }



        public int InserScrapBookingLog(string orderId, string VC, string Pnr, string request, string response, string other)
        {
             FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
             return objFCDAL.InserScrapBookingLog(orderId, VC, Pnr, request, response, other);
        }

        public List<FareTypeSettings> GetFareTypeSettings(string aircode, string trip, string provider)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.GetFareTypeSettings(aircode, trip, provider);
        }

        #region GST
        public int UpdateGSTTax(string OrderId, double GSTDIFF)
        {

            FlightCommonDAL objFltCommon = new FlightCommonDAL(ConnStr);
            return objFltCommon.UpdateGSTTax(OrderId, GSTDIFF);

        }
        #endregion

        public List<SearchResultBlock> GetFlightResultBlockList(string Trip, string Provider, string Airline, string GroupType, string AgentId, string FareType, string Exclude)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.GetFlightResultBlockList(Trip, Provider, Airline, GroupType, AgentId, FareType, Exclude);
        }
        public DataSet GetMailNotificationCredential(string ServiceType)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            return objFCDAL.GetMailNotificationCredential(ServiceType);
        }
	 #region Booking LCC

        public DataSet GetAllFlightDetailsByOrderId(string orderId)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.GetAllFlightDetailsByOrderId(orderId);
        }

        #endregion
        public string GetServiceBundleCode(string OrderId)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.GetServiceBundleCode(OrderId);
        }
        public BookingResource getAddress(string TCCode)
        {
            FlightCommonDAL obj = new FlightCommonDAL(ConnStr);
            return obj.getAddress(TCCode);
        }
        public List<TDPaxInfo> TDPaxInformation(DataSet ds)
        {
            List<TDPaxInfo> pax = new List<TDPaxInfo>();

            for (int i = 0; i < ds.Tables[0].Rows.Count;i++ )
            {
                pax.Add(new TDPaxInfo { PaxType = Convert.ToString(ds.Tables[0].Rows[i]["PaxType"]) });
            }
                return pax;
        }
        #region Seat       
		 public int insertSeat(Seat SeatDetails,string Fullname)
        {
            FlightCommonDAL objfltdal = new FlightCommonDAL(ConnStr);
            return objfltdal.InsertSeat(SeatDetails, Fullname);
        }
        public List<Seat> SeatDetails(string OrderId)
        {
            FlightCommonDAL objfltdal = new FlightCommonDAL(ConnStr);
            return objfltdal.SeatDetails(OrderId);
        }
        public int Update_NET_TOT_Fare_Seat(string TrackId, string totmealBagPrice)
        {
            FlightCommonDAL objfltdal = new FlightCommonDAL(ConnStr);
            return objfltdal.Update_NET_TOT_Fare_Seat(TrackId, totmealBagPrice);
        }
        //public string GetServiceBundleCode(string OrderId)
        //{
        //    FlightCommonDAL objfltdal = new FlightCommonDAL(ConnStr);
        //    return objfltdal.GetServiceBundleCode(OrderId);
        //}
        public int Update_Seat_Details(int Counter, string SeatStatus, bool IsSeatAssignment)
        {
            FlightCommonDAL objfltdal = new FlightCommonDAL(ConnStr);
            return objfltdal.Update_Seat_Details(Counter, SeatStatus, IsSeatAssignment);
        }
        #endregion

        public List<FlightFareType> GetFlightFareTypeMaster(string FareType, string ActionType)
        {
            FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
            return objFCDAL.GetFlightFareTypeMaster(FareType, ActionType);
        }
    }
}
