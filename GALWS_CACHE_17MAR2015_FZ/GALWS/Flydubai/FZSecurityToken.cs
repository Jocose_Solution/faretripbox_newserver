﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;

namespace STD.BAL
{
  public  class FZSecurityToken
    {

      public string LoginID = "";
      public string LoginPass = "";
      public FZSecurityToken(string loginID, string loginPass)//(string methodUrl, string serviceUrl)
      {
          LoginID = loginID;
          LoginPass = loginPass;
      }
      public string GetSecurityToken(string methodUrl, string serviceUrl, ref string exep) 
      {

          string token = "";

          string ReqXml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:rad=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request\" xmlns:rad1=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Security.Request\"> <soapenv:Header/> <soapenv:Body> <tem:RetrieveSecurityToken> <!--Optional:--> <tem:RetrieveSecurityTokenRequest> <rad:CarrierCodes> <!--Zero or more repetitions:--> <rad:CarrierCode> <rad:AccessibleCarrierCode>FZ</rad:AccessibleCarrierCode> </rad:CarrierCode> </rad:CarrierCodes>";
          ReqXml += "<rad1:LogonID>" + LoginID + "</rad1:LogonID> <rad1:Password>" + LoginPass + "</rad1:Password> </tem:RetrieveSecurityTokenRequest> </tem:RetrieveSecurityToken> </soapenv:Body> </soapenv:Envelope>";
                 string resXml = FZUtility.PostXml(ReqXml, methodUrl, serviceUrl,ref exep);

                 XDocument xmlDoc = XDocument.Parse(resXml);                
                 XNamespace myNamespace = "http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Security.Response";

                 //var results = from result in xmlDoc.Descendants(myNamespace + "SecurityToken")
                 //              select result;//.Element("SecurityToken").Value;
                  token = xmlDoc.Descendants(myNamespace + "SecurityToken").First().Value;

                  FZUtility.SaveXml(ReqXml, token, "SecurityToken_Req");
                  FZUtility.SaveXml(resXml, token, "SecurityToken_Res");

                 return token;
      
      
      }


    



    }
}
