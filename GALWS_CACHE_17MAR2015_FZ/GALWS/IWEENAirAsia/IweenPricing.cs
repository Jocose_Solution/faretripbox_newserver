﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GALWS.IWEENAirAsia
{
  public   class IweenPricing
    {
      public ArrayList GetFareQuote(DataSet Crd, DataSet dsO, ref Dictionary<string, string> Log, ref string exep, string securityGUID,ref string statusmessage)
      {
          ArrayList result = new ArrayList();
          float TotalAmount = 0;
          string responseXml = "";
          try
          {
              string username = "", password = "", serviceUrl = "", targetCode = "";
              //username = Crd.Tables[0].Rows[0]["UserID"].ToString();
              //password = Crd.Tables[0].Rows[0]["Password"].ToString();
              //double INFBasic = Convert.ToDouble(Crd.Tables[0].Rows[0]["INFBasic"]);
              //double INFTax = Convert.ToDouble(Crd.Tables[0].Rows[0]["INFTax"]);
              //IweenRequest obj = new IweenRequest(username, password, targetCode, serviceUrl, securityGUID);
              IweenRequest obj = new IweenRequest();
              responseXml = obj.GetFareQuoteRequest(dsO, ref exep);
              //DataTable dto = new DataTable();
              //dto = dsO.Tables[0];
              //responseXml = obj.ReGetFareQuoteRequest(dto, ref exep, ref Log);

              #region Pricing
              if (responseXml.Contains("Error") == false)
              {




                  XDocument xd2 = XDocument.Parse(responseXml);
                  string statuscode = "";
                  string statusmessagecode = "";
                  xd2.Descendants("return").ToList().ForEach(Each => 
                  { 
                      statuscode = Each.Element("statuscode").Value;
                      statusmessagecode = Each.Element("statusmessage").Value;

                  });
                  statusmessage = statuscode + ":" + statusmessagecode;
                  //foreach (var AvlR in xd2.Descendants("SellResponse").Descendants("ItinearyDetails"))
                  //{
                  //    foreach (var Items in AvlR.Descendants("Segments"))
                  //    {
                  //        var FlightDetails = Items.Descendants("FlightDetails");
                  //        var FareDescription = Items.Descendants("FareDescription");
                  //        var ADTFARELIST = FareDescription.Descendants("PaxFareDetails").Where(x => x.Element("PaxType").Value == "ADT");
                  //        TotalAmount += (float)Math.Ceiling(float.Parse(ADTFARELIST.First().Element("OtherInfo").Element("GrossAmount").Value.ToString())) * float.Parse(dsO.Tables[0].Rows[0]["Adult"].ToString());
                  //        if (int.Parse(dsO.Tables[0].Rows[0]["Child"].ToString()) > 0)
                  //        {
                  //            var CHDFARELIST = FareDescription.Descendants("PaxFareDetails").Where(x => x.Element("PaxType").Value == "CHD");
                  //            TotalAmount += (float)Math.Ceiling(float.Parse(CHDFARELIST.First().Element("OtherInfo").Element("GrossAmount").Value.ToString())) * float.Parse(dsO.Tables[0].Rows[0]["Child"].ToString());
                  //        }
                  //        if (int.Parse(dsO.Tables[0].Rows[0]["Infant"].ToString()) > 0)
                  //        {
                  //            // var INFARELIST = FareDescription.Descendants("PaxFareDetails").Where(x => x.Element("PaxType").Value == "INF");
                  //            // TotalAmount += float.Parse(INFARELIST.First().Element("OtherInfo").Element("GrossAmount").Value.ToString()) * float.Parse(dsO.Tables[0].Rows[0]["Infant"].ToString());
                  //            TotalAmount += float.Parse((INFBasic + INFTax).ToString()) * float.Parse(dsO.Tables[0].Rows[0]["Infant"].ToString());
                  //        }
                  //    }
                  //}
              }

              #endregion
             // result.Add(TotalAmount);
          }
          catch (Exception ex)
          {
              //result.Add(TotalAmount);
              exep = exep + ex.Message + ex.StackTrace.ToString();
          }
          return result;
      }

      

     
    }
}
