﻿using STD.BAL;
using STD.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GALWS.IWEENAirAsia
{

    public class IweenRequest
    {
        string UserName;
        string Password;
        string ServiceUrl;
        string TargetCode;
        string SecurityGUID;


        public IweenRequest()
        {
            //UserName = username;
            //Password = password;
            //ServiceUrl = serviceUrl;
            //TargetCode = targetCode;
            //SecurityGUID = securityGUID;
        }
        public string GetLowFareSearch(FlightSearch f, ref string exep, string TTrip, string TTripType, bool RTSF, bool faretype)
        {
            string strRes = "";
            try
            {

                StringBuilder LowFareSearchReq = new StringBuilder();
                string randomkey = Utility.GetRndm();
                LowFareSearchReq.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:mer='http://mercuryws.nextra.com/'>");
                LowFareSearchReq.Append("<soapenv:Header/>");
                LowFareSearchReq.Append("<soapenv:Body>");
                LowFareSearchReq.Append("<mer:SearchFlights>");
                LowFareSearchReq.Append("<flightsearchrequest>");
                LowFareSearchReq.Append("<actionname>FLIGHTSEARCH</actionname>");
                LowFareSearchReq.Append("<credentials>");
                //LowFareSearchReq.Append("<agentid>DM24614</agentid>");
                //LowFareSearchReq.Append("<officeid>DM24614</officeid>");
                ////LowFareSearchReq.Append("<!--Optional:-->");
                //LowFareSearchReq.Append("<username>testcustomer</username>");
                //LowFareSearchReq.Append("<password>Test@321</password>");
                LowFareSearchReq.Append("<agentid>AAA16445</agentid>");
                LowFareSearchReq.Append("<officeid>AAA16445</officeid>");
                //LowFareSearchReq.Append("<!--Optional:-->");
                LowFareSearchReq.Append("<username>RICHA</username>");
                LowFareSearchReq.Append("<password>1-TRVL!9102!~</password>");
                LowFareSearchReq.Append("</credentials>");

                //LowFareSearchReq.Append("<!--Optional:-->");
                LowFareSearchReq.Append("<destination>" + IweenAirAsiaAuth.Left(f.HidTxtArrCity, 3) + "</destination>");
                //LowFareSearchReq.Append("<!--Optional:-->");
                //LowFareSearchReq.Append("<excludecarriers>?</excludecarriers>");
                //LowFareSearchReq.Append("<!--Optional:-->");
                LowFareSearchReq.Append("<journeytype>OneWay</journeytype>");
                LowFareSearchReq.Append("<numadults>"+f.Adult.ToString()+"</numadults>");
                LowFareSearchReq.Append("<numchildren>"+f.Child.ToString()+"</numchildren>");
                LowFareSearchReq.Append("<numinfants>"+f.Infant.ToString()+"</numinfants>");
                LowFareSearchReq.Append("<numresults/>");

                LowFareSearchReq.Append("<onwarddate>" + Utility.Right(f.DepDate, 4) + "-" + Utility.Mid(f.DepDate, 3, 2) + "-" + Utility.Left(f.DepDate, 2) + "</onwarddate>");
                LowFareSearchReq.Append("<origin>" + IweenAirAsiaAuth.Left(f.HidTxtDepCity, 3) + "</origin>");
                //LowFareSearchReq.Append("<preddeptimewindow>?</preddeptimewindow>");
                //LowFareSearchReq.Append("<prefarrtimewindow>?</prefarrtimewindow>");
                LowFareSearchReq.Append("<prefcarrier>I5</prefcarrier>");
                LowFareSearchReq.Append("<prefclass>All</prefclass>");
                //LowFareSearchReq.Append("<promocode>?</promocode>");
                LowFareSearchReq.Append("<requestformat>XML</requestformat>");
                LowFareSearchReq.Append("<resultformat>XML</resultformat>");
                LowFareSearchReq.Append("<searchmode>LIVE</searchmode>");
                LowFareSearchReq.Append("<searchtype>normal</searchtype>");
                LowFareSearchReq.Append("<sortkey>Default</sortkey>");
                LowFareSearchReq.Append("</flightsearchrequest>");
                LowFareSearchReq.Append("</mer:SearchFlights>");
                LowFareSearchReq.Append("</soapenv:Body>");
                LowFareSearchReq.Append("</soapenv:Envelope>");
                //strRes = IweenAirAsiaAuth.GetResponse("http://modemov2.iweensoft.com:80/flights/flights?wsdl", LowFareSearchReq.ToString());
                strRes = IweenAirAsiaAuth.GetResponse("http://www.airasiaapi.com:8080/flights/flights?wsdl", LowFareSearchReq.ToString());
                IweenAirAsiaAuth.SaveXml(LowFareSearchReq.ToString(), SecurityGUID, "Availability_" + TTripType + "_" + TTrip + RTSF.ToString() + "_Req" + randomkey);
                IweenAirAsiaAuth.SaveXml(strRes, SecurityGUID, "Availability_" + TTripType + "_" + TTrip + RTSF.ToString() + "_Res" + randomkey);



            }
            catch (Exception ex)
            {

            }
            return strRes;
        }

        public string GetFareQuoteRequest(DataSet dsO, ref string exep)
        {
            string strRes = "";
            string OrgDest = dsO.Tables[0].Rows[0]["DepAirportCode"].ToString() + "_" + dsO.Tables[0].Rows[0]["ArrAirportCode"].ToString();
            string Trip = dsO.Tables[0].Rows[0]["Trip"].ToString();
            string Sno = dsO.Tables[0].Rows[0]["sno"].ToString();
            string nextraflightkey= Sno.Split(':')[0].ToString();
            try
            {
                StringBuilder LowFareSearchReq = new StringBuilder();
                LowFareSearchReq.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:mer='http://mercuryws.nextra.com/'>");
                LowFareSearchReq.Append("<soapenv:Header/>");
                LowFareSearchReq.Append("<soapenv:Body>");
                LowFareSearchReq.Append("<mer:ValidateFlightPrice>");
                LowFareSearchReq.Append("<pricingrequest>");
                LowFareSearchReq.Append("<credentials>");
                //LowFareSearchReq.Append("<agentid>DM24614</agentid>");
                //LowFareSearchReq.Append("<officeid>DM24614</officeid>");
                //LowFareSearchReq.Append("<username>testcustomer</username>");
                //LowFareSearchReq.Append("<password>Test@321</password>");
                LowFareSearchReq.Append("<agentid>AAA16445</agentid>");
                LowFareSearchReq.Append("<officeid>AAA16445</officeid>");
                LowFareSearchReq.Append("<username>RICHA</username>");
                LowFareSearchReq.Append("<password>1-TRVL!9102!~</password>");
                LowFareSearchReq.Append("</credentials>");
                LowFareSearchReq.Append("<domint>domestic</domint>");
                LowFareSearchReq.Append("<selectedflight>" + nextraflightkey + "</selectedflight>");
                LowFareSearchReq.Append("<selectedflighttw>?</selectedflighttw>");
                LowFareSearchReq.Append("<wsmode>LIVE</wsmode>");
                LowFareSearchReq.Append("</pricingrequest>");
                LowFareSearchReq.Append("</mer:ValidateFlightPrice>");
                LowFareSearchReq.Append("</soapenv:Body>");
                LowFareSearchReq.Append("</soapenv:Envelope>");
                
                string randomkey1 = Utility.GetRndm();

                //strRes = IweenAirAsiaAuth.GetResponse("http://modemov2.iweensoft.com:80/flights/flights?wsdl", LowFareSearchReq.ToString());
                strRes = IweenAirAsiaAuth.GetResponse("http://www.airasiaapi.com:8080/flights/flights?wsdl", LowFareSearchReq.ToString());
                IweenAirAsiaAuth.SaveXml(LowFareSearchReq.ToString(), SecurityGUID, "Repricing_" + Trip + "_Req" + randomkey1);
                IweenAirAsiaAuth.SaveXml(strRes, SecurityGUID, "Repricing_" + Trip +"_Res" + randomkey1);


            }
            catch (Exception ex)
            {

            }
            return strRes;
        }
    }
}
