﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Collections;
using System.IO;
using ITZERRORLOG;

namespace GALWS.AirAsia
{
    public class AirAsiaSellSSR_AssignSeat
    {
        public decimal GETSELL(DataTable fltdt, DataSet Credencilas, string RTFS, string Constr, ref string sessionids)
        {
            decimal TotalCost = 0;
            try
            {
                string[] sno = fltdt.Rows[0]["sno"].ToString().Split(':');
                decimal ExchangeRates = Convert.ToDecimal(sno[8]);
                XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/"; // XNamespace a = "http://schemas.datacontract.org/2004/07/ACE.Entities"; XNamespace i = "http://www.w3.org/2001/XMLSchema-instance";
                AirAsiaLogon objlogindtl = new AirAsiaLogon();
                sessionids = objlogindtl.GetSessionID(Credencilas.Tables[0].Rows[0]["UserID"].ToString(), Credencilas.Tables[0].Rows[0]["Password"].ToString(), Credencilas.Tables[0].Rows[0]["LoginID"].ToString());
                AirAsiaUtility objupdatedb = new AirAsiaUtility();
                objupdatedb.UpdateAirAsiaSessionId(sessionids, fltdt.Rows[0]["Track_id"].ToString(), Constr);

                string SellRequest2 = SetSellRequest(sessionids, fltdt, RTFS, sno[7], Constr, Credencilas.Tables[0].Rows[0]["UserID"].ToString());
                AirAsiaUtility.SaveFile(SellRequest2, "SellReq_" + fltdt.Rows[0]["Track_id"].ToString());
                string SellResponse2 = AirAsiaUtility.AirAsiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), "http://tempuri.org/IBookingService/Sell", SellRequest2);// objfsr.sno = Fares.Element("RuleNumber").Value + ":" + Fares.Element("CarrierCode").Value + ":" + Fares.Element("ClassOfService").Value + ":" + Fares.Element("FareApplicationType").Value + ":" + Fares.Element("FareSequence").Value + ":" + Fares.Element("ProductClass").Value;//flt.ResultIndex + ":" + items.Response.TraceId + ":" + flt.IsLCC + ":" + flt.Source;
                AirAsiaUtility.SaveFile(SellResponse2, "SellRes_" + fltdt.Rows[0]["Track_id"].ToString());

                XDocument AirAsiaDocument2 = XDocument.Parse(SellResponse2.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                XElement SellResult = AirAsiaDocument2.Element(s + "Envelope").Element(s + "Body").Element("SellResponse").Element("SellResult");
                if (SellResult.Element("Success") != null)
                {
                    if (SellResult.Element("Success").Element("PNRAmount") != null)
                        TotalCost = Convert.ToDecimal(SellResult.Element("Success").Element("PNRAmount").Element("TotalCost").Value.Trim()) * ExchangeRates;
                }
            }
            catch (Exception ex)
            { ExecptionLogger.FileHandling("GETSELL", "Error_001", ex, "AirAsiaFlight"); }
            return TotalCost;
        }
        public List<AirAsiaSSR> GetSSRAvailabilityForBooking(List<AirAsiaSSR> ssrlists, DataSet Credencilas, DataTable fltdt, string ServerPath, string RTF)
        {
            try
            {
                if (fltdt.Rows.Count > 0)
                {
                    XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/"; // XNamespace a = "http://schemas.datacontract.org/2004/07/ACE.Entities"; XNamespace i = "http://www.w3.org/2001/XMLSchema-instance";

                    string[] sno = fltdt.Rows[0]["sno"].ToString().Split(':');
                    decimal ExchangeRates = Convert.ToDecimal(sno[8]);
                    int segmentcount = Convert.ToInt32(sno[6]);
                    if (segmentcount == 1)
                    {
                        string SSRAvailabilityRequest = SSRAvailabilityRequestS(fltdt.Rows[0]["Searchvalue"].ToString(), fltdt, RTF, sno[7]);
                        AirAsiaUtility.SaveFile(SSRAvailabilityRequest, "SSRAvailabilityReq_" + fltdt.Rows[0]["Track_id"].ToString());
                        string SSRAvailabilityResponse = AirAsiaUtility.AirAsiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), "http://tempuri.org/IBookingService/GetSSRAvailabilityForBooking", SSRAvailabilityRequest);
                        AirAsiaUtility.SaveFile(SSRAvailabilityResponse, "SSRAvailabilityRes_" + fltdt.Rows[0]["Track_id"].ToString());

                        IEnumerable<XElement> SSRSegmentList = null;

                        XDocument AirAsiaDocument = XDocument.Parse(SSRAvailabilityResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                        XElement SSRAvailabilityResult = AirAsiaDocument.Element(s + "Envelope").Element(s + "Body").Element("GetSSRAvailabilityForBookingResponse").Element("GetSSRAvailabilityForBookingResult");
                        if (SSRAvailabilityResult.Element("ExceptionMessage") == null)
                        {
                            SSRSegmentList = SSRAvailabilityResult.Element("SSRSegmentList").Elements("SSRSegment");
                        }
                        if (SSRSegmentList != null)
                        {
                            ssrlists = SSRAvailabilityList(ssrlists, fltdt, ServerPath, segmentcount, SSRSegmentList, ExchangeRates);
                        }
                    }
                    else if (segmentcount > 1)
                    {
                        DataRow[] newFltDs;

                        string sessionids = fltdt.Rows[0]["Searchvalue"].ToString();
                        string counterstr = fltdt.Rows[0]["counter"].ToString();
                        for (int lg = 0; lg < segmentcount; lg++)
                        {
                            if (lg == 0)
                            {
                                newFltDs = fltdt.Select("1=1");
                                string SSRAvailabilityRequest = MultipleSegmentSSRAvailabilityRequest(sessionids, newFltDs, RTF, sno[7]);

                                AirAsiaUtility.SaveFile(SSRAvailabilityRequest, "SSRAvailabilityReq_" + newFltDs[0]["Track_id"].ToString() + "_" + newFltDs[0]["DepartureLocation"].ToString() + "_" + newFltDs[0]["ArrivalLocation"].ToString());
                                string SSRAvailabilityResponse = AirAsiaUtility.AirAsiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), "http://tempuri.org/IBookingService/GetSSRAvailabilityForBooking", SSRAvailabilityRequest);
                                AirAsiaUtility.SaveFile(SSRAvailabilityResponse, "SSRAvailabilityRes_" + newFltDs[0]["Track_id"].ToString() + "_" + newFltDs[0]["DepartureLocation"].ToString() + "_" + newFltDs[0]["ArrivalLocation"].ToString());

                                IEnumerable<XElement> SSRSegmentList = null;

                                XDocument AirAsiaDocument = XDocument.Parse(SSRAvailabilityResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                                XElement SSRAvailabilityResult = AirAsiaDocument.Element(s + "Envelope").Element(s + "Body").Element("GetSSRAvailabilityForBookingResponse").Element("GetSSRAvailabilityForBookingResult");
                                if (SSRAvailabilityResult.Element("ExceptionMessage") == null)
                                {
                                    SSRSegmentList = SSRAvailabilityResult.Element("SSRSegmentList").Elements("SSRSegment");
                                }
                                if (SSRSegmentList != null)
                                {
                                    ssrlists = SSRAvailabilityList(ssrlists, fltdt, ServerPath, segmentcount, SSRSegmentList, ExchangeRates);
                                }
                            }
                            else
                            {
                                newFltDs = fltdt.Select("counter NOT IN (" + counterstr + ")");
                                string SSRAvailabilityRequest = MultipleSegmentSSRAvailabilityRequest(sessionids, newFltDs, RTF, sno[7]);

                                AirAsiaUtility.SaveFile(SSRAvailabilityRequest, "SSRAvailabilityReq_" + newFltDs[0]["Track_id"].ToString() + "_" + newFltDs[0]["DepartureLocation"].ToString() + "_" + newFltDs[0]["ArrivalLocation"].ToString());
                                string SSRAvailabilityResponse = AirAsiaUtility.AirAsiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), "http://tempuri.org/IBookingService/GetSSRAvailabilityForBooking", SSRAvailabilityRequest);
                                AirAsiaUtility.SaveFile(SSRAvailabilityResponse, "SSRAvailabilityRes_" + newFltDs[0]["Track_id"].ToString() + "_" + newFltDs[0]["DepartureLocation"].ToString() + "_" + newFltDs[0]["ArrivalLocation"].ToString());

                                IEnumerable<XElement> SSRSegmentList = null;

                                XDocument AirAsiaDocument = XDocument.Parse(SSRAvailabilityResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                                XElement SSRAvailabilityResult = AirAsiaDocument.Element(s + "Envelope").Element(s + "Body").Element("GetSSRAvailabilityForBookingResponse").Element("GetSSRAvailabilityForBookingResult");
                                if (SSRAvailabilityResult.Element("ExceptionMessage") == null)
                                {
                                    SSRSegmentList = SSRAvailabilityResult.Element("SSRSegmentList").Elements("SSRSegment");
                                }
                                if (SSRSegmentList != null)
                                {
                                    ssrlists = SSRAvailabilityList(ssrlists, fltdt, ServerPath, segmentcount, SSRSegmentList, ExchangeRates);
                                }
                                counterstr += "," + fltdt.Rows[0]["counter"].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("GetSSRAvailabilityForBooking", "Error_001", ex, "AirAsiaFlight");
            }
            return ssrlists;
        }
        public List<AirAsiaSSR> SSRAvailabilityList(List<AirAsiaSSR> ssrlists, DataTable fltdt, string ServerPath, int segmentcount, IEnumerable<XElement> SSRSegmentList, decimal ExchangeRates)
        {
            try
            {
                string ft = fltdt.Rows[0]["AdtRbd"].ToString();

                XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/";  XNamespace a = "http://schemas.datacontract.org/2004/07/ACE.Entities"; //XNamespace i = "http://www.w3.org/2001/XMLSchema-instance";
                IEnumerable<XElement> CodeNames = null;
                if (File.Exists(ServerPath + "\\SSRNameDetails.xml"))
                {
                    XDocument document = XDocument.Load(ServerPath + "SSRNameDetails.xml");
                    XElement GetCodeNameResult = document.Element(s + "Envelope").Element(s + "Body").Element("GetCodeNameResponse").Element("GetCodeNameResult");
                    CodeNames = GetCodeNameResult.Element(a + "CodeNames").Elements(a + "CodeName");
                }
                foreach (var SSRSegment in SSRSegmentList)
                {
                    //IEnumerable<XElement> AvailablePaxSSR = SSRSegment.Element("AvailablePaxSSRList").Elements("AvailablePaxSSR").Where(x => x.Element("PaxSSRPriceList").Element("PaxSSRPrice") != null);
                    IEnumerable<XElement> AvailablePaxSSRNew = SSRSegment.Element("AvailablePaxSSRList").Elements("AvailablePaxSSR").Where(x => x.Element("PaxSSRPriceList") != null);                  
                    XElement LegKey = SSRSegment.Element("LegKey");

                    #region Meal Info
                    //IEnumerable<XElement> AvailableMEAL = SSRSegment.Element("AvailablePaxSSRList").Elements("AvailablePaxSSR").Where(x => x.Element("SSRLegList").Element("SSRLeg") != null && Convert.ToInt32(x.Element("Available").Value) > 0);
                    IEnumerable<XElement> AvailableMEALNew = SSRSegment.Element("AvailablePaxSSRList").Elements("AvailablePaxSSR").Where(x => x.Element("SSRLegList") != null);
                    if (AvailableMEALNew.Count() > 0)
                    {
                        IEnumerable<XElement> AvailableMEAL = AvailableMEALNew.Where(x => x.Element("SSRLegList").Element("SSRLeg") != null && Convert.ToInt32(x.Element("Available").Value) > 0);
                        foreach (var SSRNames in AvailableMEAL)
                        {
                            IEnumerable<XElement> CodeName = CodeNames.Where(x => x.Element(a + "Code").Value.Trim() == SSRNames.Element("SSRCode").Value.Trim());

                            IEnumerable<XElement> SSRLeg = SSRNames.Element("SSRLegList").Elements("SSRLeg");
                            XElement PaxFee = SSRNames.Element("PaxSSRPriceList").Element("PaxSSRPrice").Element("PaxFee");
                            IEnumerable<XElement> BookingServiceCharge = PaxFee.Element("ServiceCharges").Elements("BookingServiceCharge");//.Element( a +"")
                            decimal Amount = 0;
                            foreach (var seviceAmt in BookingServiceCharge)
                            {
                                Amount += Convert.ToDecimal(seviceAmt.Element("Amount").Value.ToString());
                            }
                            foreach (var SSRL in SSRLeg)
                            {
                                try
                                {


                                    ssrlists.Add(new AirAsiaSSR
                                    {
                                        Code = SSRNames.Element("SSRCode").Value.Trim(),
                                        Description = CodeName.ElementAt(0).Element(a + "Name").Value,
                                        Destination = SSRL.Element("LegKey").Element("ArrivalStation").Value.Trim(),
                                        Origin = SSRL.Element("LegKey").Element("DepartureStation").Value.Trim(),
                                        Amount = Math.Round(Amount * ExchangeRates, 2),
                                        SSRType = "Meal"//fltdt.Rows[0]["ArrivalLocation"].ToString(), fltdt.Rows[0]["DepartureLocation"].ToString()
                                    });
                                }
                                catch (Exception srex)
                                { }
                            }
                        }
                        if (ft == "D" || ft == "C" || ft == "J" || ft == "EF" || ft == "OF" || ft == "KF" || ft == "ZF" || ft == "IF" || ft == "AF" || ft == "VF" || ft == "PF" || ft == "LF" || ft == "UF" || ft == "TF" || ft == "QF" || ft == "MF" || ft == "YF")
                        {
                            if (ssrlists.Where(x => x.Amount == 0).ToList().Count == 0)
                                ssrlists.Add(new AirAsiaSSR { Code = "CPML", Description = "Complimentary Meal", Amount = 0, SSRType = "Meal", Destination = fltdt.Rows[0]["ArrivalLocation"].ToString(), Origin = fltdt.Rows[0]["DepartureLocation"].ToString() });
                        }
                    }
                    #endregion
                    #region Add Baggage
                    if (AvailablePaxSSRNew.Count() > 0)
                    {
                        IEnumerable<XElement> AvailablePaxSSR = AvailablePaxSSRNew.Where(x => x.Element("PaxSSRPriceList").Element("PaxSSRPrice") != null);
                        IEnumerable<XElement> AvailableBagage = AvailablePaxSSR.Where(x => x.Element("SSRCode").Value == "PBAA" || x.Element("SSRCode").Value == "PBAB" || x.Element("SSRCode").Value == "PBAC" || x.Element("SSRCode").Value == "PBAD" || x.Element("SSRCode").Value == "PBAF" || x.Element("SSRCode").Value == "PBPA" || x.Element("SSRCode").Value == "PBPB");
                        foreach (var SSRNames in AvailableBagage)
                        {
                            try
                            {
                                IEnumerable<XElement> CodeName = CodeNames.Where(x => x.Element(a + "Code").Value.Trim() == SSRNames.Element("SSRCode").Value.Trim());
                                XElement PaxFee = SSRNames.Element("PaxSSRPriceList").Element("PaxSSRPrice").Element("PaxFee");
                                IEnumerable<XElement> BookingServiceCharge = PaxFee.Element("ServiceCharges").Elements("BookingServiceCharge");//.Element( a +"")
                                decimal Amount = 0;
                                foreach (var seviceAmt in BookingServiceCharge)
                                {
                                    Amount += Convert.ToDecimal(seviceAmt.Element("Amount").Value.ToString());
                                }
                                ssrlists.Add(new AirAsiaSSR
                                {
                                    Code = SSRNames.Element("SSRCode").Value.Trim(),
                                    Description = CodeName.ElementAt(0).Element(a + "Name").Value,
                                    Amount = Math.Round(Amount * ExchangeRates, 2),
                                    SSRType = "Baggage",
                                    Destination = LegKey.Element("ArrivalStation").Value.Trim(),
                                    Origin = LegKey.Element("DepartureStation").Value.Trim()
                                });
                            }
                            catch (Exception bagex) { }
                        }
                        if (ft == "EF" || ft == "OF" || ft == "KF" || ft == "ZF" || ft == "IF" || ft == "AF" || ft == "VF" || ft == "PF" || ft == "LF" || ft == "UF" || ft == "TF" || ft == "QF" || ft == "MF" || ft == "YF")
                        {
                            if (ssrlists.Where(x => x.Amount == 0 && x.SSRType == "Baggage").ToList().Count == 0)
                            {
                                ssrlists.Add(new AirAsiaSSR { Code = "PBAB", Description = "Checked baggage 20kg", Amount = 0, SSRType = "Baggage", Destination = fltdt.Rows[0]["ArrivalLocation"].ToString(), Origin = fltdt.Rows[0]["DepartureLocation"].ToString() });
                                if (fltdt.Rows[0]["Trip"].ToString() == "D")
                                    ssrlists.Add(new AirAsiaSSR { Code = "PBAA", Description = "Checked baggage 15kg", Amount = 0, SSRType = "Baggage", Destination = fltdt.Rows[0]["ArrivalLocation"].ToString(), Origin = fltdt.Rows[0]["DepartureLocation"].ToString() });
                            }
                        }
                        else if (ft == "D" || ft == "C" || ft == "J")
                        {
                            if (ssrlists.Where(x => x.Amount == 0 && x.SSRType == "Baggage").ToList().Count == 0)
                            {
                                ssrlists.Add(new AirAsiaSSR { Code = "PBAB", Description = "Checked baggage 20kg", Amount = 0, SSRType = "Baggage", Destination = fltdt.Rows[0]["ArrivalLocation"].ToString(), Origin = fltdt.Rows[0]["DepartureLocation"].ToString() });
                                ssrlists.Add(new AirAsiaSSR { Code = "PBAC", Description = "Checked baggage 25kg", Amount = 0, SSRType = "Baggage", Destination = fltdt.Rows[0]["ArrivalLocation"].ToString(), Origin = fltdt.Rows[0]["DepartureLocation"].ToString() });
                                ssrlists.Add(new AirAsiaSSR { Code = "PBAD", Description = "Checked baggage 30kg", Amount = 0, SSRType = "Baggage", Destination = fltdt.Rows[0]["ArrivalLocation"].ToString(), Origin = fltdt.Rows[0]["DepartureLocation"].ToString() });
                                ssrlists.Add(new AirAsiaSSR { Code = "PBAF", Description = "Checked baggage 40kg", Amount = 0, SSRType = "Baggage", Destination = fltdt.Rows[0]["ArrivalLocation"].ToString(), Origin = fltdt.Rows[0]["DepartureLocation"].ToString() });
                                if (fltdt.Rows[0]["Trip"].ToString() == "D")
                                    ssrlists.Add(new AirAsiaSSR { Code = "PBAA", Description = "Checked baggage 15kg", Amount = 0, SSRType = "Baggage", Destination = fltdt.Rows[0]["ArrivalLocation"].ToString(), Origin = fltdt.Rows[0]["DepartureLocation"].ToString() });
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("SSRAvailabilityList", "Error_001", ex, "AirAsiaFlight");
            }
            return ssrlists;
        }
        public List<AirAsiaSeat> GetSeatAvailability(List<AirAsiaSeat> ssrlists, DataSet Credencilas, DataTable fltdt)
        {
            try
            {
                //DataTable fltdt = Fltdtlds.Tables[0];
                if (fltdt.Rows.Count > 0)
                {
                    XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/"; // XNamespace a = "http://schemas.datacontract.org/2004/07/ACE.Entities"; XNamespace i = "http://www.w3.org/2001/XMLSchema-instance";

                    string[] sno = fltdt.Rows[0]["sno"].ToString().Split(':');
                    decimal ExchangeRates = Convert.ToDecimal(sno[8]);
                    int segmentcount = Convert.ToInt32(sno[6]);
                    if (segmentcount == 1)
                    {
                        string SeatAvailabilityRequest = GetSeatAvailabilityRequest(fltdt.Rows[0]["Searchvalue"].ToString(), fltdt.Rows[0]["OperatingCarrier"].ToString(), fltdt.Rows[0]["FlightIdentification"].ToString(), fltdt.Rows[0]["ArrivalLocation"].ToString(), fltdt.Rows[0]["DepartureLocation"].ToString(), Convert.ToDateTime(fltdt.Rows[0]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss"), Convert.ToDateTime(fltdt.Rows[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss"), sno[7]);
                        AirAsiaUtility.SaveFile(SeatAvailabilityRequest, "SeatAvailabilityReq_" + fltdt.Rows[0]["Track_id"].ToString());
                        string SeatAvailabilityResponse = AirAsiaUtility.AirAsiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), "http://tempuri.org/IBookingService/GetSeatAvailability", SeatAvailabilityRequest);
                        AirAsiaUtility.SaveFile(SeatAvailabilityResponse, "SeatAvailabilityRes_" + fltdt.Rows[0]["Track_id"].ToString());

                        IEnumerable<XElement> EquipmentInfo = null;
                        IEnumerable<XElement> SeatGroupPassengerFee = null;

                        XDocument AirAsiaDocument = XDocument.Parse(SeatAvailabilityResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                        XElement SeatAvailabilityResult = AirAsiaDocument.Element(s + "Envelope").Element(s + "Body").Element("GetSeatAvailabilityResponse").Element("GetSeatAvailabilityResult");
                        if (SeatAvailabilityResult.Element("ExceptionMessage") == null)
                        {
                            EquipmentInfo = SeatAvailabilityResult.Element("EquipmentInfos").Elements("EquipmentInfo");
                            SeatGroupPassengerFee = SeatAvailabilityResult.Element("SeatGroupPassengerFees").Elements("SeatGroupPassengerFee");
                        }
                        //else if (SeatAvailabilityResult.Element("ExceptionMessage").Value.Trim().Contains("Invalid Login.") || SeatAvailabilityResult.Element("ExceptionMessage").Value.Trim().Contains("Session token authentication failure. : No such session:"))
                        //{
                        //    AirAsiaLogon objlogindtl = new AirAsiaLogon();
                        //    string sessionids = objlogindtl.GetSessionID(Credencilas.Tables[0].Rows[0]["UserID"].ToString(), Credencilas.Tables[0].Rows[0]["Password"].ToString(), Credencilas.Tables[0].Rows[0]["LoginID"].ToString());

                        //    string SeatAvailabilityResult2 = GetSeatAvailabilityRequest(sessionids, fltdt.Rows[0]["ValiDatingCarrier"].ToString(), fltdt.Rows[0]["FlightIdentification"].ToString(), fltdt.Rows[0]["ArrivalLocation"].ToString(), fltdt.Rows[0]["DepartureLocation"].ToString(), Convert.ToDateTime(fltdt.Rows[0]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss"), Convert.ToDateTime(fltdt.Rows[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss"));
                        //    AirAsiaUtility.SaveFile(SeatAvailabilityResult2, "SSRAvailabilityReq2_" + fltdt.Rows[0]["Track_id"].ToString());
                        //    string SeatAvailabilityResponse2 = AirAsiaUtility.AirAsiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), "http://tempuri.org/IBookingService/GetSeatAvailability", SeatAvailabilityResult2);
                        //    AirAsiaUtility.SaveFile(SeatAvailabilityResponse2, "SSRAvailabilityRes2_" + fltdt.Rows[0]["Track_id"].ToString());

                        //    XDocument AirAsiaDocument2 = XDocument.Parse(SeatAvailabilityResponse2.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                        //    XElement SSRAvailabilityResult2 = AirAsiaDocument2.Element(s + "Envelope").Element(s + "Body").Element("GetSeatAvailabilityResponse").Element("GetSeatAvailabilityResult");
                        //    EquipmentInfo = SSRAvailabilityResult2.Element("EquipmentInfos").Elements("EquipmentInfo");
                        //    SeatGroupPassengerFee = SSRAvailabilityResult2.Element("SeatGroupPassengerFees").Elements("SeatGroupPassengerFee");

                        //}
                        if (EquipmentInfo != null && SeatGroupPassengerFee != null)
                        {
                            ssrlists = GetSeatAvailabilityList(ssrlists, fltdt, segmentcount, EquipmentInfo, SeatGroupPassengerFee, ExchangeRates);
                        }
                    }
                    else if (segmentcount > 1)
                    {
                        DataRow[] newFltDs;

                        string sessionids = fltdt.Rows[0]["Searchvalue"].ToString();
                        string counterstr = fltdt.Rows[0]["counter"].ToString();
                        for (int lg = 0; lg < segmentcount; lg++)
                        {
                            if (lg == 0)
                            {
                                newFltDs = fltdt.Select("1=1");
                                string SeatAvailabilityRequest = MultipleSegmentSeatAvailabilityRequest(sessionids, newFltDs, sno[7]);
                                AirAsiaUtility.SaveFile(SeatAvailabilityRequest, "SeatAvailabilityReq_" + fltdt.Rows[0]["Track_id"].ToString() + "_" + newFltDs[0]["DepartureLocation"].ToString() + "_" + newFltDs[0]["ArrivalLocation"].ToString());
                                string SeatAvailabilityResponse = AirAsiaUtility.AirAsiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), "http://tempuri.org/IBookingService/GetSeatAvailability", SeatAvailabilityRequest);
                                AirAsiaUtility.SaveFile(SeatAvailabilityResponse, "SeatAvailabilityRes_" + fltdt.Rows[0]["Track_id"].ToString() + "_" + newFltDs[0]["DepartureLocation"].ToString() + "_" + newFltDs[0]["ArrivalLocation"].ToString());

                                IEnumerable<XElement> EquipmentInfo = null;
                                IEnumerable<XElement> SeatGroupPassengerFee = null;

                                XDocument AirAsiaDocument = XDocument.Parse(SeatAvailabilityResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                                XElement SeatAvailabilityResult = AirAsiaDocument.Element(s + "Envelope").Element(s + "Body").Element("GetSeatAvailabilityResponse").Element("GetSeatAvailabilityResult");
                                if (SeatAvailabilityResult.Element("ExceptionMessage") == null)
                                {
                                    EquipmentInfo = SeatAvailabilityResult.Element("EquipmentInfos").Elements("EquipmentInfo");
                                    SeatGroupPassengerFee = SeatAvailabilityResult.Element("SeatGroupPassengerFees").Elements("SeatGroupPassengerFee");
                                }
                                if (EquipmentInfo != null && SeatGroupPassengerFee != null)
                                {
                                    ssrlists = GetSeatAvailabilityList(ssrlists, fltdt, segmentcount, EquipmentInfo, SeatGroupPassengerFee, ExchangeRates);
                                }
                            }
                            else
                            {
                                newFltDs = fltdt.Select("counter NOT IN (" + counterstr + ")");
                                string SeatAvailabilityRequest = MultipleSegmentSeatAvailabilityRequest(sessionids, newFltDs, sno[7]);

                                AirAsiaUtility.SaveFile(SeatAvailabilityRequest, "SeatAvailabilityReq_" + newFltDs[0]["Track_id"].ToString() + "_" + newFltDs[0]["DepartureLocation"].ToString() + "_" + newFltDs[0]["ArrivalLocation"].ToString());
                                string SeatAvailabilityResponse = AirAsiaUtility.AirAsiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), "http://tempuri.org/IBookingService/GetSeatAvailability", SeatAvailabilityRequest);
                                AirAsiaUtility.SaveFile(SeatAvailabilityResponse, "SeatAvailabilityRes_" + newFltDs[0]["Track_id"].ToString() + "_" + newFltDs[0]["DepartureLocation"].ToString() + "_" + newFltDs[0]["ArrivalLocation"].ToString());

                                IEnumerable<XElement> EquipmentInfo = null;
                                IEnumerable<XElement> SeatGroupPassengerFee = null;

                                XDocument AirAsiaDocument = XDocument.Parse(SeatAvailabilityResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                                XElement SeatAvailabilityResult = AirAsiaDocument.Element(s + "Envelope").Element(s + "Body").Element("GetSeatAvailabilityResponse").Element("GetSeatAvailabilityResult");
                                if (SeatAvailabilityResult.Element("ExceptionMessage") == null)
                                {
                                    EquipmentInfo = SeatAvailabilityResult.Element("EquipmentInfos").Elements("EquipmentInfo");
                                    SeatGroupPassengerFee = SeatAvailabilityResult.Element("SeatGroupPassengerFees").Elements("SeatGroupPassengerFee");
                                }
                                if (EquipmentInfo != null && SeatGroupPassengerFee != null)
                                {
                                    ssrlists = GetSeatAvailabilityList(ssrlists, fltdt, segmentcount, EquipmentInfo, SeatGroupPassengerFee, ExchangeRates);
                                }
                                counterstr += "," + fltdt.Rows[0]["counter"].ToString();
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            { ExecptionLogger.FileHandling("GetSeatAvailability", "Error_001", ex, "AirAsiaFlight"); }
            return ssrlists;
        }
        public List<AirAsiaSeat> GetSeatAvailabilityList(List<AirAsiaSeat> ssrlists, DataTable fltdt, int segmentcount, IEnumerable<XElement> EquipmentInfo, IEnumerable<XElement> SeatGroupPassengerFee, decimal ExchangeRates)
        {
            try
            {
                XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/"; // XNamespace a = "http://schemas.datacontract.org/2004/07/ACE.Entities"; XNamespace i = "http://www.w3.org/2001/XMLSchema-instance";

                if (EquipmentInfo != null && SeatGroupPassengerFee != null)
                {
                    foreach (var Compartment in EquipmentInfo)
                    {
                        XElement CompartmentInfo = Compartment.Element("Compartments").Element("CompartmentInfo").Element("Seats");
                        foreach (var Seats in CompartmentInfo.Elements("SeatInfo").Where(x => x.Element("Assignable").Value == "true" && x.Element("SeatAvailability").Value == "Open"))
                        {
                            IEnumerable<XElement> PassengerFee = SeatGroupPassengerFee.Where(x => x.Element("SeatGroup").Value.Trim() == Seats.Element("SeatGroup").Value.Trim());
                            decimal Amount = 0;
                            if (PassengerFee.Count() > 0)
                            {
                                foreach (var ServiceCharge in PassengerFee.ElementAt(0).Element("PassengerFee").Element("ServiceCharges").Elements("BookingServiceCharge"))
                                {
                                    Amount += Convert.ToDecimal(ServiceCharge.Element("Amount").Value);
                                }
                                IEnumerable<XElement> EquipmentProperty = Seats.Element("PropertyList").Elements("EquipmentProperty").Where(x => x.Element("TypeCode").Value == "WINDOW" || x.Element("TypeCode").Value == "AISLE");
                                ssrlists.Add(new AirAsiaSeat
                                {
                                    SeatGroup = Seats.Element("SeatGroup").Value.Trim(),
                                    SeatDesignator = Seats.Element("SeatDesignator").Value.Trim(),
                                    Amount = Math.Round(Amount * ExchangeRates, 2),
                                    SeatType = EquipmentProperty.Count() > 0 ? EquipmentProperty.ElementAt(0).Element("TypeCode").Value : "MIDDLE"
                                });
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("GetSeatAvailabilityList", "Error_001", ex, "AirAsiaFlight");
            }
            return ssrlists;
        }
        public string SellSSR(DataSet Credencilas, DataTable fltdt, string ConStr, DataTable MealBages, ref Dictionary<string, string> xml)
        {
            string TotalCost = "FAILURE";
            string Reqxml = "", Resxml = "";
            try
            {
                if (fltdt.Rows.Count > 0)
                {
                    string[] sno = fltdt.Rows[0]["sno"].ToString().Split(':');
                    decimal ExchangeRates = Convert.ToDecimal(sno[8]);

                    int segmentcount = Convert.ToInt32(sno[6]);

                    if (segmentcount == 1)
                        Reqxml = SellSSRRequest(fltdt.Rows[0]["Searchvalue"].ToString(), fltdt, MealBages, sno[7]);
                    if (segmentcount > 1)
                        Reqxml = MultipleSegmentSellSSRRequest(fltdt.Rows[0]["Searchvalue"].ToString(), fltdt, MealBages, sno[7]);

                    AirAsiaUtility.SaveFile(Reqxml, "SellSSRReq_" + fltdt.Rows[0]["Track_id"].ToString());
                    Resxml = AirAsiaUtility.AirAsiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), "http://tempuri.org/IBookingService/Sell", Reqxml);
                    AirAsiaUtility.SaveFile(Resxml, "SellSSRRes_" + fltdt.Rows[0]["Track_id"].ToString());

                    XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/"; // XNamespace a = "http://schemas.datacontract.org/2004/07/ACE.Entities"; XNamespace i = "http://www.w3.org/2001/XMLSchema-instance";
                    XDocument AirAsiaDocument = XDocument.Parse(Resxml.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                    XElement SellSSRResult = AirAsiaDocument.Element(s + "Envelope").Element(s + "Body").Element("SellResponse").Element("SellResult");
                    if (SellSSRResult.Element("ExceptionMessage") == null)
                    {
                        if (SellSSRResult.Element("Success") != null)
                        {
                            if (SellSSRResult.Element("Success").Element("PNRAmount") != null)
                            {
                                TotalCost = (Convert.ToDecimal(SellSSRResult.Element("Success").Element("PNRAmount").Element("TotalCost").Value.Trim()) * ExchangeRates).ToString();
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("SellSSR", "Error_001", ex, "AirAsiaFlight");
                xml.Add("UPPAXREQ", "");
                xml.Add("UPPAXRES", "");
                xml.Add("UCCONREQ", "");
                xml.Add("UCCONRES", "");
                xml.Add("STATEREQ", "");
                xml.Add("STATERES", "");
                xml.Add("APBREQ", "");
                xml.Add("APBRES", "");
                xml.Add("BC-REQ", "");
                xml.Add("BC-RES", "");
                xml.Add("OTHER", ex.Message);
            }
            finally
            {
                xml.Add("SSR", Reqxml + "<br>" + Resxml);
            }
            return TotalCost;
        }
        public decimal AssignSeats(DataSet Credencilas, DataTable fltdt)
        {
            decimal TotalCost = 0;
            try
            {
                if (fltdt.Rows.Count > 0)
                {
                    string[] sno = fltdt.Rows[0]["sno"].ToString().Split(':');
                    decimal ExchangeRates = Convert.ToDecimal(sno[8]);

                    string AssignSeatsReq = AssignSeatsRequest(fltdt.Rows[0]["Searchvalue"].ToString(), fltdt.Rows[0]["OperatingCarrier"].ToString(), fltdt.Rows[0]["FlightIdentification"].ToString(), fltdt.Rows[0]["ArrivalLocation"].ToString(), fltdt.Rows[0]["DepartureLocation"].ToString(), Convert.ToDateTime(fltdt.Rows[0]["arrdatelcc"]).ToString("yyyy-MM-ddThh:mm:ss"), Convert.ToDateTime(fltdt.Rows[0]["depdatelcc"]).ToString("yyyy-MM-ddThh:mm:ss"), "", sno[7]);
                    AirAsiaUtility.SaveFile(AssignSeatsReq, "SSRAvailabilityReq_" + fltdt.Rows[0]["Track_id"].ToString());
                    string AssignSeatsResponse = AirAsiaUtility.AirAsiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), "http://tempuri.org/IBookingService/AssignSeats", AssignSeatsReq);
                    AirAsiaUtility.SaveFile(AssignSeatsResponse, "SSRAvailabilityRes_" + fltdt.Rows[0]["Track_id"].ToString());

                    XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/"; //Namespace a = "http://schemas.datacontract.org/2004/07/ACE.Entities"; XNamespace i = "http://www.w3.org/2001/XMLSchema-instance";
                    IEnumerable<XElement> SSRSegmentList = null;

                    XDocument AirAsiaDocument = XDocument.Parse(AssignSeatsResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                    XElement AssignSeatsResult = AirAsiaDocument.Element(s + "Envelope").Element(s + "Body").Element("AssignSeatsResponse").Element("AssignSeatsResult");
                    if (AssignSeatsResult.Element("ExceptionMessage") == null)
                    {
                        if (AssignSeatsResult.Element("Success") != null)
                        {
                            TotalCost = Math.Round(Convert.ToDecimal(AssignSeatsResult.Element("Success").Element("PNRAmount").Element("TotalCost").Value.Trim()) * ExchangeRates, 2);
                        }
                    }
                    else if (AssignSeatsResult.Element("ExceptionMessage").Value.Trim().Contains("Invalid Login.") || AssignSeatsResult.Element("ExceptionMessage").Value.Trim().Contains("Session token authentication failure. : No such session:"))
                    {
                        AirAsiaLogon objlogindtl = new AirAsiaLogon();
                        string sessionids = objlogindtl.GetSessionID(Credencilas.Tables[0].Rows[0]["UserID"].ToString(), Credencilas.Tables[0].Rows[0]["Password"].ToString(), Credencilas.Tables[0].Rows[0]["LoginID"].ToString());

                        string SellRequest2 = AssignSeatsRequest(sessionids, fltdt.Rows[0]["ValiDatingCarrier"].ToString(), fltdt.Rows[0]["FlightIdentification"].ToString(), fltdt.Rows[0]["ArrivalLocation"].ToString(), fltdt.Rows[0]["DepartureLocation"].ToString(), Convert.ToDateTime(fltdt.Rows[0]["arrdatelcc"]).ToString("yyyy-MM-ddThh:mm:ss"), Convert.ToDateTime(fltdt.Rows[0]["depdatelcc"]).ToString("yyyy-MM-ddThh:mm:ss"), "", sno[7]);
                        AirAsiaUtility.SaveFile(SellRequest2, "SellReq_" + fltdt.Rows[0]["Track_id"].ToString());
                        string SellResponse2 = AirAsiaUtility.AirAsiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), "http://tempuri.org/IBookingService/Sell", SellRequest2);// objfsr.sno = Fares.Element("RuleNumber").Value + ":" + Fares.Element("CarrierCode").Value + ":" + Fares.Element("ClassOfService").Value + ":" + Fares.Element("FareApplicationType").Value + ":" + Fares.Element("FareSequence").Value + ":" + Fares.Element("ProductClass").Value;//flt.ResultIndex + ":" + items.Response.TraceId + ":" + flt.IsLCC + ":" + flt.Source;
                        AirAsiaUtility.SaveFile(SellResponse2, "SellRes_" + fltdt.Rows[0]["Track_id"].ToString());
                        AssignSeatsResult = null;
                        XDocument AirAsiaDocument2 = XDocument.Parse(SellResponse2.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                        AssignSeatsResult = AirAsiaDocument2.Element(s + "Envelope").Element(s + "Body").Element("SellResponse").Element("SellResult");
                        if (AssignSeatsResult.Element("Success") != null)
                        {
                            TotalCost = Math.Round(Convert.ToDecimal(AssignSeatsResult.Element("Success").Element("PNRAmount").Element("TotalCost").Value.Trim()) * ExchangeRates, 2);
                        }
                    }
                }
            }
            catch (Exception ex)
            { ExecptionLogger.FileHandling("AssignSeats", "Error_001", ex, "AirAsiaFlight"); }
            return TotalCost;
        }
        public string UpdateContactsGST(DataSet Credencilas, string sessionvalue, string Track_id, string CompanyName, string GSTNumber, string EmailAddress, string Phone, string Address, string Constr, string CurrancyCode, string ExchangeRate)
        {
            decimal TotalCost = 0;
            try
            {
                string GSTUpdateRequest = SetGSTUpdateRequest(sessionvalue, EmailAddress, CompanyName, GSTNumber, Phone, Address, CurrancyCode);
                AirAsiaUtility.SaveFile(GSTUpdateRequest, "GSTUpdateReq_" + Track_id);
                string GSTUpdateResponse = AirAsiaUtility.AirAsiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), "http://tempuri.org/IBookingService/UpdateContacts", GSTUpdateRequest);
                AirAsiaUtility.SaveFile(GSTUpdateResponse, "GSTUpdateRes_" + Track_id);

                XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/"; // XNamespace a = "http://schemas.datacontract.org/2004/07/ACE.Entities"; XNamespace i = "http://www.w3.org/2001/XMLSchema-instance";

                XDocument AirAsiaDocument = XDocument.Parse(GSTUpdateResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                XElement UpdateContactsResult = AirAsiaDocument.Element(s + "Envelope").Element(s + "Body").Element("UpdateContactsResponse").Element("UpdateContactsResult");
                if (UpdateContactsResult.Element("ExceptionMessage") == null)
                {
                    if (UpdateContactsResult.Element("Success") != null)
                    {
                        if (UpdateContactsResult.Element("Success").Element("PNRAmount") != null)
                            TotalCost = Convert.ToDecimal(UpdateContactsResult.Element("Success").Element("PNRAmount").Element("TotalCost").Value.Trim()) * Convert.ToDecimal(ExchangeRate);
                    }
                }
            }
            catch (Exception ex)
            { ExecptionLogger.FileHandling("UpdateContactsGST", "Error_001", ex, "AirAsiaFlight"); }
            return sessionvalue;
        }

        public string SetSellRequest(string SessionID, DataTable fltdt, string RTFS, string CurrancyCode, string constr, string userid)
        {
            StringBuilder objreqXml = new StringBuilder();
            try
            {
                DataRow[] newFltDsIN = fltdt.Select("TripType='R'");
                DataRow[] newFltDsOut = fltdt.Select("TripType='O'");

                objreqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:ace=\"http://schemas.datacontract.org/2004/07/ACE.Entities\" xmlns:arr=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">");
                objreqXml.Append("<soapenv:Header/><soapenv:Body><tem:Sell>");
                objreqXml.Append("<tem:strSessionID>" + SessionID + "</tem:strSessionID>");
                objreqXml.Append("<tem:objSellRequestData><ace:SellBy>Journey</ace:SellBy><ace:SellJourneyRequest><ace:SellJourneyRequestData>");
                objreqXml.Append("<ace:CurrencyCode>" + CurrancyCode + "</ace:CurrencyCode><ace:Journeys>");
                if (RTFS == "" && newFltDsIN.Length == 0)
                {
                    #region One way 
                    objreqXml.Append("<ace:SellJourney><ace:Segments>");
                    string[] sno = fltdt.Rows[0]["sno"].ToString().Split(':');
                    int segmentcount = Convert.ToInt32(sno[6]);
                    if (segmentcount == 1)
                    {
                        objreqXml.Append("<ace:SellSegment>");
                        if (fltdt.Rows.Count == 1)
                        {
                            objreqXml.Append("<ace:ArrivalStation>" + fltdt.Rows[0]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + fltdt.Rows[0]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                            objreqXml.Append("<ace:Fare>");
                            objreqXml.Append("<ace:ClassOfService>" + fltdt.Rows[0]["AdtRbd"].ToString() + "</ace:ClassOfService><ace:CarrierCode>" + sno[1] + "</ace:CarrierCode>");
                            objreqXml.Append("<ace:RuleNumber>" + sno[0] + "</ace:RuleNumber><ace:FareBasisCode>" + fltdt.Rows[0]["FareBasis"].ToString() + "</ace:FareBasisCode><ace:FareSequence>" + sno[4] + "</ace:FareSequence>");
                            objreqXml.Append("<ace:FareClassOfService>" + fltdt.Rows[0]["AdtRbd"].ToString() + "</ace:FareClassOfService><ace:FareApplicationType>" + sno[3] + "</ace:FareApplicationType>");
                            objreqXml.Append("<ace:ProductClass>" + sno[5] + "</ace:ProductClass>");
                            objreqXml.Append("</ace:Fare>");
                            objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + fltdt.Rows[0]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + fltdt.Rows[0]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");
                            objreqXml.Append("<ace:STA>" + Convert.ToDateTime(fltdt.Rows[0]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STA><ace:STD>" + Convert.ToDateTime(fltdt.Rows[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");
                        }
                        else if (fltdt.Rows.Count > 1)
                        {
                            objreqXml.Append("<ace:ArrivalStation>" + fltdt.Rows[fltdt.Rows.Count - 1]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + fltdt.Rows[0]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                            objreqXml.Append("<ace:Fare>");
                            objreqXml.Append("<ace:ClassOfService>" + fltdt.Rows[0]["AdtRbd"].ToString() + "</ace:ClassOfService><ace:CarrierCode>" + sno[1] + "</ace:CarrierCode>");
                            objreqXml.Append("<ace:RuleNumber>" + sno[0] + "</ace:RuleNumber><ace:FareBasisCode>" + fltdt.Rows[0]["FareBasis"].ToString() + "</ace:FareBasisCode><ace:FareSequence>" + sno[4] + "</ace:FareSequence>");
                            objreqXml.Append("<ace:FareClassOfService>" + fltdt.Rows[0]["AdtRbd"].ToString() + "</ace:FareClassOfService><ace:FareApplicationType>" + sno[3] + "</ace:FareApplicationType>");
                            objreqXml.Append("<ace:ProductClass>" + sno[5] + "</ace:ProductClass>");
                            objreqXml.Append("</ace:Fare>");
                            objreqXml.Append("<ace:Legs>");
                            for (int lg = 0; lg < fltdt.Rows.Count; lg++)
                            {
                                objreqXml.Append("<ace:Leg>");
                                objreqXml.Append("<ace:ArrivalStation>" + fltdt.Rows[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + fltdt.Rows[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                                objreqXml.Append("<ace:STA>" + Convert.ToDateTime(fltdt.Rows[lg]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STA><ace:STD>" + Convert.ToDateTime(fltdt.Rows[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");
                                objreqXml.Append("</ace:Leg>");
                            }
                            objreqXml.Append("</ace:Legs>");
                            objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + fltdt.Rows[0]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + fltdt.Rows[0]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");
                            objreqXml.Append("<ace:STA>" + Convert.ToDateTime(fltdt.Rows[fltdt.Rows.Count - 1]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STA><ace:STD>" + Convert.ToDateTime(fltdt.Rows[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");

                        }
                        objreqXml.Append("</ace:SellSegment>");
                    }
                    else if (segmentcount > 1)
                    {
                        for (int sg = 0; sg < fltdt.Rows.Count; sg++)
                        {
                            string[] SgSno = fltdt.Rows[sg]["sno"].ToString().Split(':');
                            objreqXml.Append("<ace:SellSegment>");
                            objreqXml.Append("<ace:ArrivalStation>" + fltdt.Rows[sg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + fltdt.Rows[sg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");

                            objreqXml.Append("<ace:Fare>");
                            objreqXml.Append("<ace:ClassOfService>" + fltdt.Rows[sg]["AdtRbd"].ToString() + "</ace:ClassOfService><ace:CarrierCode>" + SgSno[1] + "</ace:CarrierCode>");
                            objreqXml.Append("<ace:RuleNumber>" + SgSno[0] + "</ace:RuleNumber><ace:FareBasisCode>" + fltdt.Rows[sg]["FareBasis"].ToString() + "</ace:FareBasisCode><ace:FareSequence>" + SgSno[4] + "</ace:FareSequence>");
                            objreqXml.Append("<ace:FareClassOfService>" + fltdt.Rows[sg]["AdtRbd"].ToString() + "</ace:FareClassOfService><ace:FareApplicationType>" + SgSno[3] + "</ace:FareApplicationType>");
                            objreqXml.Append("<ace:ProductClass>" + SgSno[5] + "</ace:ProductClass>");
                            objreqXml.Append("</ace:Fare>");
                            objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + fltdt.Rows[sg]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + fltdt.Rows[sg]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");
                            objreqXml.Append("<ace:STA>" + Convert.ToDateTime(fltdt.Rows[sg]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STA><ace:STD>" + Convert.ToDateTime(fltdt.Rows[sg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");

                            objreqXml.Append("</ace:SellSegment>");
                        }
                    }
                    objreqXml.Append("</ace:Segments></ace:SellJourney>");
                    #endregion
                }
                else
                {
                    #region RTF Fare Sell Request
                    string[] sno = newFltDsOut[0]["sno"].ToString().Split(':');
                    int segmentcount = Convert.ToInt32(sno[6]);
                    objreqXml.Append("<ace:SellJourney><ace:Segments>");
                    if (segmentcount == 1)
                    {
                        objreqXml.Append("<ace:SellSegment>");
                        if (newFltDsOut.Length == 1)
                        {
                            objreqXml.Append("<ace:ArrivalStation>" + newFltDsOut[0]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + newFltDsOut[0]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                            objreqXml.Append("<ace:Fare>");
                            objreqXml.Append("<ace:ClassOfService>" + newFltDsOut[0]["AdtRbd"].ToString() + "</ace:ClassOfService><ace:CarrierCode>" + sno[1] + "</ace:CarrierCode>");
                            objreqXml.Append("<ace:RuleNumber>" + sno[0] + "</ace:RuleNumber><ace:FareBasisCode>" + newFltDsOut[0]["FareBasis"].ToString() + "</ace:FareBasisCode><ace:FareSequence>" + sno[4] + "</ace:FareSequence>");
                            objreqXml.Append("<ace:FareClassOfService>" + newFltDsOut[0]["AdtRbd"].ToString() + "</ace:FareClassOfService><ace:FareApplicationType>" + sno[3] + "</ace:FareApplicationType>");
                            objreqXml.Append("<ace:ProductClass>" + sno[5] + "</ace:ProductClass>");
                            objreqXml.Append("</ace:Fare>");
                            objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + newFltDsOut[0]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + newFltDsOut[0]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");
                            objreqXml.Append("<ace:STA>" + Convert.ToDateTime(newFltDsOut[0]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STA><ace:STD>" + Convert.ToDateTime(newFltDsOut[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");
                        }
                        else if (newFltDsOut.Length > 1)
                        {
                            objreqXml.Append("<ace:ArrivalStation>" + newFltDsOut[newFltDsOut.Length - 1]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + newFltDsOut[0]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                            objreqXml.Append("<ace:Fare>");
                            objreqXml.Append("<ace:ClassOfService>" + newFltDsOut[0]["AdtRbd"].ToString() + "</ace:ClassOfService><ace:CarrierCode>" + sno[1] + "</ace:CarrierCode>");
                            objreqXml.Append("<ace:RuleNumber>" + sno[0] + "</ace:RuleNumber><ace:FareBasisCode>" + newFltDsOut[0]["FareBasis"].ToString() + "</ace:FareBasisCode><ace:FareSequence>" + sno[4] + "</ace:FareSequence>");
                            objreqXml.Append("<ace:FareClassOfService>" + newFltDsOut[0]["AdtRbd"].ToString() + "</ace:FareClassOfService><ace:FareApplicationType>" + sno[3] + "</ace:FareApplicationType>");
                            objreqXml.Append("<ace:ProductClass>" + sno[5] + "</ace:ProductClass>");
                            objreqXml.Append("</ace:Fare>");
                            objreqXml.Append("<ace:Legs>");
                            for (int lg = 0; lg < newFltDsOut.Length; lg++)
                            {
                                objreqXml.Append("<ace:Leg>");
                                objreqXml.Append("<ace:ArrivalStation>" + newFltDsOut[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + newFltDsOut[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                                objreqXml.Append("<ace:STA>" + Convert.ToDateTime(newFltDsOut[lg]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STA><ace:STD>" + Convert.ToDateTime(newFltDsOut[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");
                                objreqXml.Append("</ace:Leg>");
                            }
                            objreqXml.Append("</ace:Legs>");
                            objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + newFltDsOut[0]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + newFltDsOut[0]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");
                            objreqXml.Append("<ace:STA>" + Convert.ToDateTime(newFltDsOut[newFltDsOut.Length - 1]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STA><ace:STD>" + Convert.ToDateTime(newFltDsOut[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");

                        }
                        objreqXml.Append("</ace:SellSegment>");
                    }
                    else if (segmentcount > 1)
                    {
                        for (int sg = 0; sg < newFltDsOut.Length; sg++)
                        {
                            string[] SgSno = newFltDsOut[sg]["sno"].ToString().Split(':');
                            objreqXml.Append("<ace:SellSegment>");
                            objreqXml.Append("<ace:ArrivalStation>" + newFltDsOut[sg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + newFltDsOut[sg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");

                            objreqXml.Append("<ace:Fare>");
                            objreqXml.Append("<ace:ClassOfService>" + newFltDsOut[sg]["AdtRbd"].ToString() + "</ace:ClassOfService><ace:CarrierCode>" + SgSno[1] + "</ace:CarrierCode>");
                            objreqXml.Append("<ace:RuleNumber>" + SgSno[0] + "</ace:RuleNumber><ace:FareBasisCode>" + newFltDsOut[sg]["FareBasis"].ToString() + "</ace:FareBasisCode><ace:FareSequence>" + SgSno[4] + "</ace:FareSequence>");
                            objreqXml.Append("<ace:FareClassOfService>" + newFltDsOut[sg]["AdtRbd"].ToString() + "</ace:FareClassOfService><ace:FareApplicationType>" + SgSno[3] + "</ace:FareApplicationType>");
                            objreqXml.Append("<ace:ProductClass>" + SgSno[5] + "</ace:ProductClass>");
                            objreqXml.Append("</ace:Fare>");
                            objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + newFltDsOut[sg]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + newFltDsOut[sg]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");
                            objreqXml.Append("<ace:STA>" + Convert.ToDateTime(newFltDsOut[sg]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STA><ace:STD>" + Convert.ToDateTime(newFltDsOut[sg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");

                            objreqXml.Append("</ace:SellSegment>");
                        }
                    }
                    objreqXml.Append("</ace:Segments></ace:SellJourney>");

                    if (newFltDsIN.Length > 0)
                    {
                        string[] snoIN = newFltDsIN[0]["sno"].ToString().Split(':');
                        int segmentcountIN = Convert.ToInt32(snoIN[6]);
                        objreqXml.Append("<ace:SellJourney><ace:Segments>");
                        if (segmentcountIN == 1)
                        {
                            objreqXml.Append("<ace:SellSegment>");
                            if (newFltDsIN.Length == 1)
                            {
                                objreqXml.Append("<ace:ArrivalStation>" + newFltDsIN[0]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + newFltDsIN[0]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                                objreqXml.Append("<ace:Fare>");
                                objreqXml.Append("<ace:ClassOfService>" + newFltDsIN[0]["AdtRbd"].ToString() + "</ace:ClassOfService><ace:CarrierCode>" + snoIN[1] + "</ace:CarrierCode>");
                                objreqXml.Append("<ace:RuleNumber>" + snoIN[0] + "</ace:RuleNumber><ace:FareBasisCode>" + newFltDsIN[0]["FareBasis"].ToString() + "</ace:FareBasisCode><ace:FareSequence>" + snoIN[4] + "</ace:FareSequence>");
                                objreqXml.Append("<ace:FareClassOfService>" + newFltDsIN[0]["AdtRbd"].ToString() + "</ace:FareClassOfService><ace:FareApplicationType>" + snoIN[3] + "</ace:FareApplicationType>");
                                objreqXml.Append("<ace:ProductClass>" + snoIN[5] + "</ace:ProductClass>");
                                objreqXml.Append("</ace:Fare>");
                                objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + newFltDsIN[0]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + newFltDsIN[0]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");
                                objreqXml.Append("<ace:STA>" + Convert.ToDateTime(newFltDsIN[0]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STA><ace:STD>" + Convert.ToDateTime(newFltDsIN[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");
                            }
                            else if (newFltDsIN.Length > 1)
                            {
                                objreqXml.Append("<ace:ArrivalStation>" + newFltDsIN[newFltDsIN.Length - 1]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + newFltDsIN[0]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                                objreqXml.Append("<ace:Fare>");
                                objreqXml.Append("<ace:ClassOfService>" + newFltDsIN[0]["AdtRbd"].ToString() + "</ace:ClassOfService><ace:CarrierCode>" + snoIN[1] + "</ace:CarrierCode>");
                                objreqXml.Append("<ace:RuleNumber>" + snoIN[0] + "</ace:RuleNumber><ace:FareBasisCode>" + newFltDsIN[0]["FareBasis"].ToString() + "</ace:FareBasisCode><ace:FareSequence>" + snoIN[4] + "</ace:FareSequence>");
                                objreqXml.Append("<ace:FareClassOfService>" + newFltDsIN[0]["AdtRbd"].ToString() + "</ace:FareClassOfService><ace:FareApplicationType>" + snoIN[3] + "</ace:FareApplicationType>");
                                objreqXml.Append("<ace:ProductClass>" + snoIN[5] + "</ace:ProductClass>");
                                objreqXml.Append("</ace:Fare>");
                                objreqXml.Append("<ace:Legs>");
                                for (int lg = 0; lg < newFltDsIN.Length; lg++)
                                {
                                    objreqXml.Append("<ace:Leg>");
                                    objreqXml.Append("<ace:ArrivalStation>" + newFltDsIN[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + newFltDsIN[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                                    objreqXml.Append("<ace:STA>" + Convert.ToDateTime(newFltDsIN[lg]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STA><ace:STD>" + Convert.ToDateTime(newFltDsIN[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");
                                    objreqXml.Append("</ace:Leg>");
                                }
                                objreqXml.Append("</ace:Legs>");
                                objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + newFltDsIN[0]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + newFltDsIN[0]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");
                                objreqXml.Append("<ace:STA>" + Convert.ToDateTime(newFltDsIN[newFltDsIN.Length - 1]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STA><ace:STD>" + Convert.ToDateTime(newFltDsIN[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");

                            }
                            objreqXml.Append("</ace:SellSegment>");
                        }
                        else if (segmentcountIN > 1)
                        {
                            for (int sg = 0; sg < newFltDsIN.Length; sg++)
                            {
                                string[] SgSno = newFltDsIN[sg]["sno"].ToString().Split(':');
                                objreqXml.Append("<ace:SellSegment>");
                                objreqXml.Append("<ace:ArrivalStation>" + newFltDsIN[sg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + newFltDsIN[sg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");

                                objreqXml.Append("<ace:Fare>");
                                objreqXml.Append("<ace:ClassOfService>" + newFltDsIN[sg]["AdtRbd"].ToString() + "</ace:ClassOfService><ace:CarrierCode>" + SgSno[1] + "</ace:CarrierCode>");
                                objreqXml.Append("<ace:RuleNumber>" + SgSno[0] + "</ace:RuleNumber><ace:FareBasisCode>" + newFltDsIN[sg]["FareBasis"].ToString() + "</ace:FareBasisCode><ace:FareSequence>" + SgSno[4] + "</ace:FareSequence>");
                                objreqXml.Append("<ace:FareClassOfService>" + newFltDsIN[sg]["AdtRbd"].ToString() + "</ace:FareClassOfService><ace:FareApplicationType>" + SgSno[3] + "</ace:FareApplicationType>");
                                objreqXml.Append("<ace:ProductClass>" + SgSno[5] + "</ace:ProductClass>");
                                objreqXml.Append("</ace:Fare>");
                                objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + newFltDsIN[sg]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + newFltDsIN[sg]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");
                                objreqXml.Append("<ace:STA>" + Convert.ToDateTime(newFltDsIN[sg]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STA><ace:STD>" + Convert.ToDateTime(newFltDsIN[sg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");

                                objreqXml.Append("</ace:SellSegment>");
                            }
                        }
                        objreqXml.Append("</ace:Segments></ace:SellJourney>");
                    }
                    #endregion
                }
                objreqXml.Append("</ace:Journeys>");
                //SetSellRequest(fltdt.Rows[0]["Searchvalue"].ToString(), fltdt.Rows[0]["ElectronicTicketing"].ToString(), fltdt.Rows[0]["ArrivalLocation"].ToString(), fltdt.Rows[0]["DepartureLocation"].ToString(), fltdt.Rows[0]["AdtRbd"].ToString(),
                //sno[1], sno[0], fltdt.Rows[0]["FareBasis"].ToString(), sno[4], sno[3], fltdt.Rows[0]["OperatingCarrier"].ToString(), fltdt.Rows[0]["FlightIdentification"].ToString(),
                //Convert.ToDateTime(fltdt.Rows[0]["arrdatelcc"]).ToString("yyyy-MM-ddThh:mm:ss"), Convert.ToDateTime(fltdt.Rows[0]["depdatelcc"]).ToString("yyyy-MM-ddThh:mm:ss"), Convert.ToInt32(fltdt.Rows[0]["Adult"]), Convert.ToInt32(fltdt.Rows[0]["Child"]));
                objreqXml.Append("<ace:Passengers>");
                for (int ad = 0; ad < Convert.ToInt32(fltdt.Rows[0]["Adult"]); ad++)
                {
                    objreqXml.Append("<ace:Passenger>");
                    objreqXml.Append("<ace:PassengerNumber>" + ad + "</ace:PassengerNumber><ace:PassengerTypeInfo><ace:PaxType>ADT</ace:PaxType></ace:PassengerTypeInfo>");
                    objreqXml.Append("</ace:Passenger>");
                }
                for (int ad = 0; ad < Convert.ToInt32(fltdt.Rows[0]["Child"]); ad++)
                {
                    objreqXml.Append("<ace:Passenger>");
                    objreqXml.Append("<ace:PassengerNumber>" + (Convert.ToInt32(fltdt.Rows[0]["Adult"]) + ad).ToString() + "</ace:PassengerNumber><ace:PassengerTypeInfo><ace:PaxType>CHD</ace:PaxType></ace:PassengerTypeInfo>");
                    objreqXml.Append("</ace:Passenger>");
                }
                objreqXml.Append("</ace:Passengers>");
                objreqXml.Append("<ace:PaxCount>" + (Convert.ToInt32(fltdt.Rows[0]["Adult"]) + Convert.ToInt32(fltdt.Rows[0]["Child"])).ToString() + "</ace:PaxCount>");

                //string PromoCode = AirAsiaUtility.HaveAnyPromoCodeAplicable(fltdt.Rows[0]["DepartureLocation"].ToString().Trim(), newFltDsOut[newFltDsOut.Length - 1]["ArrivalLocation"].ToString().Trim(), Convert.ToDateTime(fltdt.Rows[0]["depdatelcc"]).ToString("yyyy-MM-dd") + "T00:00:00", constr, "AK");
                string PromoCode = newFltDsOut[0]["sno"].ToString().Split(':')[9];
                if (PromoCode != "")
                {
                    objreqXml.Append("<ace:SourcePOS><ace:State>New</ace:State><ace:OrganizationCode>" + userid + "</ace:OrganizationCode></ace:SourcePOS>");
                    objreqXml.Append("<ace:TypeOfSale><ace:State>New</ace:State><ace:PromotionCode>" + PromoCode + "</ace:PromotionCode></ace:TypeOfSale>");
                }
                else
                    objreqXml.Append("<LoyaltyFilter>MonetaryOnly</LoyaltyFilter><SuppressPaxAgeValidation>false</SuppressPaxAgeValidation>");
                objreqXml.Append("</ace:SellJourneyRequestData></ace:SellJourneyRequest></tem:objSellRequestData></tem:Sell></soapenv:Body></soapenv:Envelope>");

            }
            catch (Exception ex)
            { ExecptionLogger.FileHandling("SELLRequest", "Error_001", ex, "AirAsiaFlight"); }

            return objreqXml.ToString();
        }
        public string SSRAvailabilityRequestS(string SessionID, DataTable fltdt, string RTF, string CurrancyCode)
        {
            StringBuilder objreqXml = new StringBuilder();
            try
            {
                objreqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:ace=\"http://schemas.datacontract.org/2004/07/ACE.Entities\" xmlns:arr=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">");
                objreqXml.Append("<soapenv:Header/><soapenv:Body><tem:GetSSRAvailabilityForBooking>");
                objreqXml.Append("<tem:strSessionID>" + SessionID + "</tem:strSessionID>");
                objreqXml.Append("<tem:objSSRAvailabilityRequest>");
                objreqXml.Append("<ace:SegmentKeyList>");
                if (fltdt.Rows[0]["FlightStatus"].ToString() == "RTF")
                {
                    DataRow[] newFltDsIN = fltdt.Select("TripType='R'");
                    DataRow[] newFltDsOut = fltdt.Select("TripType='O'");
                    if (RTF == "RTF")
                    {
                        for (int lg = 0; lg < newFltDsIN.Length; lg++)
                        {
                            objreqXml.Append("<ace:LegKey>");
                            objreqXml.Append("<ace:CarrierCode>" + newFltDsIN[lg]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + newFltDsIN[lg]["FlightIdentification"].ToString() + "</ace:FlightNumber>");
                            objreqXml.Append("<ace:DepartureDate>" + Convert.ToDateTime(newFltDsIN[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:DepartureDate><ace:ArrivalDate>" + Convert.ToDateTime(newFltDsIN[lg]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:ArrivalDate>");
                            objreqXml.Append("<ace:DepartureStation>" + newFltDsIN[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation><ace:ArrivalStation>" + newFltDsIN[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation>");
                            objreqXml.Append("</ace:LegKey>");
                        }
                    }
                    else
                    {
                        for (int lg = 0; lg < newFltDsOut.Length; lg++)
                        {
                            objreqXml.Append("<ace:LegKey>");
                            objreqXml.Append("<ace:CarrierCode>" + newFltDsOut[lg]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + newFltDsOut[lg]["FlightIdentification"].ToString() + "</ace:FlightNumber>");
                            objreqXml.Append("<ace:DepartureDate>" + Convert.ToDateTime(newFltDsOut[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:DepartureDate><ace:ArrivalDate>" + Convert.ToDateTime(newFltDsOut[lg]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:ArrivalDate>");
                            objreqXml.Append("<ace:DepartureStation>" + newFltDsOut[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation><ace:ArrivalStation>" + newFltDsOut[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation>");
                            objreqXml.Append("</ace:LegKey>");
                        }
                    }
                }
                else
                {
                    for (int lg = 0; lg < fltdt.Rows.Count; lg++)
                    {
                        objreqXml.Append("<ace:LegKey>");
                        objreqXml.Append("<ace:CarrierCode>" + fltdt.Rows[lg]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + fltdt.Rows[lg]["FlightIdentification"].ToString() + "</ace:FlightNumber>");
                        objreqXml.Append("<ace:DepartureDate>" + Convert.ToDateTime(fltdt.Rows[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:DepartureDate><ace:ArrivalDate>" + Convert.ToDateTime(fltdt.Rows[lg]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:ArrivalDate>");
                        objreqXml.Append("<ace:DepartureStation>" + fltdt.Rows[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation><ace:ArrivalStation>" + fltdt.Rows[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation>");
                        objreqXml.Append("</ace:LegKey>");
                    }
                }
                objreqXml.Append("</ace:SegmentKeyList>");
                objreqXml.Append("<ace:InventoryControlled>true</ace:InventoryControlled><ace:NonInventoryControlled>true</ace:NonInventoryControlled><ace:SeatDependent>true</ace:SeatDependent><ace:NonSeatDependent>true</ace:NonSeatDependent>");
                objreqXml.Append("<ace:CurrencyCode>" + CurrancyCode + "</ace:CurrencyCode></tem:objSSRAvailabilityRequest></tem:GetSSRAvailabilityForBooking></soapenv:Body></soapenv:Envelope>");
                //SSRAvailabilityRequestS(fltdt.Rows[0]["Searchvalue"].ToString(), fltdt.Rows[0]["OperatingCarrier"].ToString(), fltdt.Rows[0]["FlightIdentification"].ToString(), fltdt.Rows[0]["ArrivalLocation"].ToString(), fltdt.Rows[0]["DepartureLocation"].ToString(), Convert.ToDateTime(fltdt.Rows[0]["arrdatelcc"]).ToString("yyyy-MM-ddThh:mm:ss"), Convert.ToDateTime(fltdt.Rows[0]["depdatelcc"]).ToString("yyyy-MM-ddThh:mm:ss"));
            }
            catch (Exception ex)
            { ExecptionLogger.FileHandling("SSRAvailabilityRequestS", "Error_001", ex, "AirAsiaFlight"); }
            return objreqXml.ToString();
        }

        public string SetGSTUpdateRequest(string Sessionids, string EmailAddress, string CompanyName, string GSTNumber, string Phone, string Address, string CurrancyCode)
        {
            StringBuilder objreqXml = new StringBuilder();
            objreqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:ace=\"http://schemas.datacontract.org/2004/07/ACE.Entities\" xmlns:arr=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">");
            objreqXml.Append("<soapenv:Header/><soapenv:Body><tem:UpdateContacts>");
            objreqXml.Append("<tem:strSessionID>" + Sessionids + "</tem:strSessionID>");
            objreqXml.Append("<tem:objUpdateContactsRequestData><ace:BookingContactList><ace:BookingContact>");
            objreqXml.Append("<ace:State>New</ace:State><ace:TypeCode>G</ace:TypeCode>");
            objreqXml.Append("<ace:Names><ace:BookingName><ace:State>New</ace:State><ace:FirstName/><ace:MiddleName/><ace:LastName/><ace:Title/></ace:BookingName></ace:Names>");
            objreqXml.Append("<ace:EmailAddress>" + EmailAddress + "</ace:EmailAddress>");
            objreqXml.Append("<ace:WorkPhone/>");
            objreqXml.Append("<ace:CompanyName>" + CompanyName + "</ace:CompanyName><ace:City/>");
            objreqXml.Append("<ace:DistributionOption>None</ace:DistributionOption><ace:CustomerNumber>" + GSTNumber + "</ace:CustomerNumber>");
            objreqXml.Append("<ace:NotificationPreference>None</ace:NotificationPreference>");
            objreqXml.Append("</ace:BookingContact></ace:BookingContactList></tem:objUpdateContactsRequestData></tem:UpdateContacts></soapenv:Body></soapenv:Envelope>");
            return objreqXml.ToString();
        }

        public string GetSeatAvailabilityRequest(string Sessionids, string FlightCarrierCode, string FlightNumber, string ArrivalStation, string DepartureStation, string STA, string STD, string CurrancyCode)
        {
            StringBuilder objreqXml = new StringBuilder();
            try
            {
                objreqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:ace=\"http://schemas.datacontract.org/2004/07/ACE.Entities\" xmlns:arr=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">");
                objreqXml.Append("<soapenv:Header/><soapenv:Body><tem:GetSeatAvailability>");
                objreqXml.Append("<tem:strSessionID>" + Sessionids + "</tem:strSessionID>");
                objreqXml.Append("<tem:objSeatAvailabilityRequest>");
                objreqXml.Append("<ace:STD>" + STD + "</ace:STD>");
                objreqXml.Append("<ace:DepartureStation>" + DepartureStation + "</ace:DepartureStation><ace:ArrivalStation>" + ArrivalStation + "</ace:ArrivalStation>");
                objreqXml.Append("<ace:IncludeSeatFees>true</ace:IncludeSeatFees><ace:SeatAssignmentMode>PreSeatAssignment</ace:SeatAssignmentMode>");
                objreqXml.Append("<ace:FlightNumber>" + FlightNumber + "</ace:FlightNumber><ace:CarrierCode>" + FlightCarrierCode + "</ace:CarrierCode><ace:OpSuffix/>");
                objreqXml.Append("<ace:CompressProperties>false</ace:CompressProperties><ace:EnforceSeatGroupRestrictions>false</ace:EnforceSeatGroupRestrictions>");
                objreqXml.Append("<ace:CollectedCurrencyCode>" + CurrancyCode + "</ace:CollectedCurrencyCode>");
                objreqXml.Append("</tem:objSeatAvailabilityRequest></tem:GetSeatAvailability></soapenv:Body></soapenv:Envelope>");
            }
            catch (Exception ex)
            { ExecptionLogger.FileHandling("GetSeatAvailabilityRequest", "Error_001", ex, "AirAsiaFlight"); }

            return objreqXml.ToString();
        }
        public string SellSSRRequest(string Sessionids, DataTable fltdt, DataTable MealBages, string CurrancyCode)
        {
            StringBuilder objreqXml = new StringBuilder();
            try
            {
                DataRow[] newFltDsIN = fltdt.Select("TripType='R'");

                objreqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:ace=\"http://schemas.datacontract.org/2004/07/ACE.Entities\" xmlns:arr=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">");
                objreqXml.Append("<soapenv:Header/><soapenv:Body><tem:Sell>");
                objreqXml.Append("<tem:strSessionID>" + Sessionids + "</tem:strSessionID>");
                objreqXml.Append("<tem:objSellRequestData><ace:SellBy>SSR</ace:SellBy><ace:SellSSR><ace:SSRRequest>");
                objreqXml.Append("<ace:SegmentSSRRequests>");


                if (newFltDsIN.Length == 0)
                {
                    #region normal request
                    objreqXml.Append("<ace:SegmentSSRRequest>");
                    objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + fltdt.Rows[0]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + fltdt.Rows[0]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");
                    objreqXml.Append("<ace:STD>" + Convert.ToDateTime(fltdt.Rows[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");
                    objreqXml.Append("<ace:DepartureStation>" + fltdt.Rows[0]["DepartureLocation"].ToString() + "</ace:DepartureStation><ace:ArrivalStation>" + fltdt.Rows[fltdt.Rows.Count - 1]["ArrivalLocation"].ToString() + "</ace:ArrivalStation>");

                    string ft = fltdt.Rows[0]["AdtRbd"].ToString();
                    if (Convert.ToInt32(fltdt.Rows[0]["Infant"]) > 0 || MealBages.Rows.Count > 0 || ft == "EF" || ft == "OF" || ft == "KF" || ft == "ZF" || ft == "IF" || ft == "AF" || ft == "VF" || ft == "PF" || ft == "LF" || ft == "UF" || ft == "TF" || ft == "QF" || ft == "MF" || ft == "YF")
                    {
                        objreqXml.Append("<ace:PaxSSRs>");
                        //SellSSRRequest(fltdt.Rows[0]["Searchvalue"].ToString(), fltdt.Rows[0]["ElectronicTicketing"].ToString(), fltdt.Rows[0]["OperatingCarrier"].ToString(), fltdt.Rows[0]["FlightIdentification"].ToString(), fltdt.Rows[0]["ArrivalLocation"].ToString(), fltdt.Rows[0]["DepartureLocation"].ToString(), Convert.ToDateTime(fltdt.Rows[0]["arrdatelcc"]).ToString("yyyy-MM-ddThh:mm:ss"), Convert.ToDateTime(fltdt.Rows[0]["depdatelcc"]).ToString("yyyy-MM-ddThh:mm:ss"), fltdt.Rows[0]["AdtRbd"].ToString(), Convert.ToInt32(fltdt.Rows[0]["Infant"]), MealBages);
                        for (int ad = 0; ad < Convert.ToInt32(fltdt.Rows[0]["Infant"]); ad++)
                        {
                            for (int lg = 0; lg < fltdt.Rows.Count; lg++)
                            {
                                objreqXml.Append("<ace:PaxSSR>");
                                objreqXml.Append("<ace:ArrivalStation>" + fltdt.Rows[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + fltdt.Rows[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                                objreqXml.Append("<ace:PassengerNumber>" + ad + "</ace:PassengerNumber><ace:SSRCode>INFT</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                                objreqXml.Append("</ace:PaxSSR>");
                            }
                        }

                        for (int lg = 0; lg < fltdt.Rows.Count; lg++)
                        {
                            int MealPaxNo = 0, BaggagePaxNo = 0;
                            for (int ssr = 0; ssr < MealBages.Rows.Count; ssr++)
                            {
                                if (MealBages.Rows[ssr]["MealCode"].ToString() != "")
                                {
                                    string[] MealDiscription = MealBages.Rows[ssr]["MealDesc"].ToString().Split('_');
                                    if (fltdt.Rows[lg]["ArrivalLocation"].ToString().Trim() == MealDiscription[2].Trim() && fltdt.Rows[lg]["DepartureLocation"].ToString().Trim() == MealDiscription[1].Trim())
                                    {

                                        objreqXml.Append("<ace:PaxSSR>");
                                        objreqXml.Append("<ace:ArrivalStation>" + MealDiscription[2] + "</ace:ArrivalStation><ace:DepartureStation>" + MealDiscription[1] + "</ace:DepartureStation>");
                                        objreqXml.Append("<ace:PassengerNumber>" + MealPaxNo.ToString() + "</ace:PassengerNumber><ace:SSRCode>" + MealBages.Rows[ssr]["MealCode"].ToString() + "</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                                        objreqXml.Append("</ace:PaxSSR>");
                                        MealPaxNo++;
                                    }
                                }
                                if (MealBages.Rows[ssr]["BaggageCode"].ToString() != "")
                                {
                                    string[] BaggageDiscription = MealBages.Rows[ssr]["BaggageDesc"].ToString().ToString().Split('_'); ;
                                    if (fltdt.Rows[lg]["ArrivalLocation"].ToString().Trim() == BaggageDiscription[2].Trim() && fltdt.Rows[lg]["DepartureLocation"].ToString().Trim() == BaggageDiscription[1].Trim())
                                    {

                                        objreqXml.Append("<ace:PaxSSR>");
                                        objreqXml.Append("<ace:ArrivalStation>" + fltdt.Rows[fltdt.Rows.Count - 1]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + fltdt.Rows[0]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                                        objreqXml.Append("<ace:PassengerNumber>" + BaggagePaxNo.ToString() + "</ace:PassengerNumber><ace:SSRCode>" + MealBages.Rows[ssr]["BaggageCode"].ToString() + "</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                                        objreqXml.Append("</ace:PaxSSR>"); BaggagePaxNo++;
                                    }
                                }
                            }
                        }

                        if (ft == "EF" || ft == "OF" || ft == "KF" || ft == "ZF" || ft == "IF" || ft == "AF" || ft == "VF" || ft == "PF" || ft == "LF" || ft == "UF" || ft == "TF" || ft == "QF" || ft == "MF" || ft == "YF")
                        {
                            for (int lg = 0; lg < fltdt.Rows.Count; lg++)
                            {
                                objreqXml.Append("<ace:PaxSSR>");
                                objreqXml.Append("<ace:ArrivalStation>" + fltdt.Rows[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + fltdt.Rows[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                                objreqXml.Append("<ace:SSRCode>HFL</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                                objreqXml.Append("</ace:PaxSSR>");
                            }
                        }

                        if ((ft == "EF" || ft == "OF" || ft == "KF" || ft == "ZF" || ft == "IF" || ft == "AF" || ft == "VF" || ft == "PF" || ft == "LF" || ft == "UF" || ft == "TF" || ft == "QF" || ft == "MF" || ft == "YF" || ft == "D" || ft == "C" || ft == "J"))
                        {
                            for (int lg = 0; lg < fltdt.Rows.Count; lg++)
                            {
                                DateTime dtdt = Convert.ToDateTime(fltdt.Rows[lg]["depdatelcc"]);
                                double tothours = dtdt.Subtract(DateTime.Now).TotalHours;
                                if (tothours < 22)
                                {
                                    objreqXml.Append("<ace:PaxSSR>");
                                    objreqXml.Append("<ace:ArrivalStation>" + fltdt.Rows[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + fltdt.Rows[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                                    objreqXml.Append("<ace:SSRCode>LTML</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                                    objreqXml.Append("</ace:PaxSSR>");
                                }
                            }
                        }
                        objreqXml.Append("</ace:PaxSSRs>");
                    }
                    else
                        objreqXml.Append("<ace:PaxSSRs/>");
                    objreqXml.Append("</ace:SegmentSSRRequest>");
                    #endregion
                }
                else
                {
                    DataRow[] newFltDsOut = fltdt.Select("TripType='O'");

                    #region SRF request outBond
                    objreqXml.Append("<ace:SegmentSSRRequest>");
                    objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + newFltDsOut[0]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + newFltDsOut[0]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");
                    objreqXml.Append("<ace:STD>" + Convert.ToDateTime(newFltDsOut[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");
                    objreqXml.Append("<ace:DepartureStation>" + newFltDsOut[0]["DepartureLocation"].ToString() + "</ace:DepartureStation><ace:ArrivalStation>" + newFltDsOut[newFltDsOut.Length - 1]["ArrivalLocation"].ToString() + "</ace:ArrivalStation>");

                    string ft = newFltDsOut[0]["AdtRbd"].ToString();
                    if (Convert.ToInt32(newFltDsOut[0]["Infant"]) > 0 || MealBages.Rows.Count > 0 || ft == "EF" || ft == "OF" || ft == "KF" || ft == "ZF" || ft == "IF" || ft == "AF" || ft == "VF" || ft == "PF" || ft == "LF" || ft == "UF" || ft == "TF" || ft == "QF" || ft == "MF" || ft == "YF")
                    {
                        objreqXml.Append("<ace:PaxSSRs>");
                        //SellSSRRequest(fltdt.Rows[0]["Searchvalue"].ToString(), fltdt.Rows[0]["ElectronicTicketing"].ToString(), fltdt.Rows[0]["OperatingCarrier"].ToString(), fltdt.Rows[0]["FlightIdentification"].ToString(), fltdt.Rows[0]["ArrivalLocation"].ToString(), fltdt.Rows[0]["DepartureLocation"].ToString(), Convert.ToDateTime(fltdt.Rows[0]["arrdatelcc"]).ToString("yyyy-MM-ddThh:mm:ss"), Convert.ToDateTime(fltdt.Rows[0]["depdatelcc"]).ToString("yyyy-MM-ddThh:mm:ss"), fltdt.Rows[0]["AdtRbd"].ToString(), Convert.ToInt32(fltdt.Rows[0]["Infant"]), MealBages);
                        for (int ad = 0; ad < Convert.ToInt32(newFltDsOut[0]["Infant"]); ad++)
                        {
                            for (int lg = 0; lg < newFltDsOut.Length; lg++)
                            {
                                objreqXml.Append("<ace:PaxSSR>");
                                // objreqXml.Append("<ace:ActionStatusCode>" + ActionStatusCode + "</ace:ActionStatusCode><ace:ArrivalStation>" + ArrivalStation + "</ace:ArrivalStation><ace:DepartureStation>" + DepartureStation + "</ace:DepartureStation>");
                                objreqXml.Append("<ace:ArrivalStation>" + newFltDsOut[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + newFltDsOut[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                                objreqXml.Append("<ace:PassengerNumber>" + ad + "</ace:PassengerNumber><ace:SSRCode>INFT</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                                objreqXml.Append("</ace:PaxSSR>");
                            }
                        }
                        int mcount = 0, bcount = 0;
                        for (int ssr = 0; ssr < MealBages.Rows.Count; ssr++)
                        {
                            for (int lg = 0; lg < newFltDsOut.Length; lg++)
                            {
                                if (MealBages.Rows[ssr]["MealCode"].ToString() != "")
                                {
                                    string[] MealDiscription = MealBages.Rows[ssr]["MealDesc"].ToString().Split('_');
                                    if (newFltDsOut[lg]["ArrivalLocation"].ToString().Trim() == MealDiscription[2].Trim() && newFltDsOut[lg]["DepartureLocation"].ToString().Trim() == MealDiscription[1].Trim())
                                    {
                                        objreqXml.Append("<ace:PaxSSR>");
                                        // objreqXml.Append("<ace:ActionStatusCode>" + ActionStatusCode + "</ace:ActionStatusCode><ace:ArrivalStation>" + ArrivalStation + "</ace:ArrivalStation><ace:DepartureStation>" + DepartureStation + "</ace:DepartureStation>");
                                        objreqXml.Append("<ace:ArrivalStation>" + MealDiscription[2] + "</ace:ArrivalStation><ace:DepartureStation>" + MealDiscription[1] + "</ace:DepartureStation>");
                                        objreqXml.Append("<ace:PassengerNumber>" + mcount.ToString() + "</ace:PassengerNumber><ace:SSRCode>" + MealBages.Rows[ssr]["MealCode"].ToString() + "</ace:SSRCode>");
                                        //objreqXml.Append("<ace:PassengerNumber>0</ace:PassengerNumber><ace:SSRCode>" + MealBages.Rows[ssr]["MealCode"].ToString() + "</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                                        objreqXml.Append("</ace:PaxSSR>");
                                        mcount++;
                                    }
                                }
                                if (MealBages.Rows[ssr]["BaggageCode"].ToString() != "")
                                {
                                    string[] BaggageDiscription = MealBages.Rows[ssr]["BaggageDesc"].ToString().ToString().Split('_'); ;
                                    if (newFltDsOut[lg]["ArrivalLocation"].ToString().Trim() == BaggageDiscription[2].Trim() && newFltDsOut[lg]["DepartureLocation"].ToString().Trim() == BaggageDiscription[1].Trim())
                                    {
                                        objreqXml.Append("<ace:PaxSSR>");
                                        objreqXml.Append("<ace:ArrivalStation>" + newFltDsOut[newFltDsOut.Length - 1]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + newFltDsOut[0]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                                        objreqXml.Append("<ace:PassengerNumber>" + bcount.ToString() + "</ace:PassengerNumber><ace:SSRCode>" + MealBages.Rows[ssr]["BaggageCode"].ToString() + "</ace:SSRCode>");
                                        // objreqXml.Append("<ace:PassengerNumber>0</ace:PassengerNumber><ace:SSRCode>" + MealBages.Rows[ssr]["BaggageCode"].ToString() + "</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                                        objreqXml.Append("</ace:PaxSSR>");
                                        bcount++;
                                    }
                                }
                            }                         
                        }

                        if (ft == "EF" || ft == "OF" || ft == "KF" || ft == "ZF" || ft == "IF" || ft == "AF" || ft == "VF" || ft == "PF" || ft == "LF" || ft == "UF" || ft == "TF" || ft == "QF" || ft == "MF" || ft == "YF")
                        {
                            for (int lg = 0; lg < newFltDsOut.Length; lg++)
                            {
                                objreqXml.Append("<ace:PaxSSR>");
                                objreqXml.Append("<ace:ArrivalStation>" + newFltDsOut[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + newFltDsOut[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                                objreqXml.Append("<ace:SSRCode>HFL</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                                objreqXml.Append("</ace:PaxSSR>");
                            }
                        }
                        if (ft == "EF" || ft == "OF" || ft == "KF" || ft == "ZF" || ft == "IF" || ft == "AF" || ft == "VF" || ft == "PF" || ft == "LF" || ft == "UF" || ft == "TF" || ft == "QF" || ft == "MF" || ft == "YF" || ft == "D" || ft == "C" || ft == "J")
                        {
                            for (int lg = 0; lg < newFltDsOut.Length; lg++)
                            {
                                DateTime dtdt = Convert.ToDateTime(newFltDsOut[lg]["depdatelcc"]);
                                double tothours = dtdt.Subtract(DateTime.Now).TotalHours;//dtdt.Subtract(dtdt1).TotalHours
                                if (tothours < 22)
                                {
                                    objreqXml.Append("<ace:PaxSSR>");
                                    objreqXml.Append("<ace:ArrivalStation>" + newFltDsOut[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + newFltDsOut[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                                    objreqXml.Append("<ace:SSRCode>LTML</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                                    objreqXml.Append("</ace:PaxSSR>");
                                }
                            }
                        }
                        objreqXml.Append("</ace:PaxSSRs>");
                    }
                    else
                        objreqXml.Append("<ace:PaxSSRs/>");
                    objreqXml.Append("</ace:SegmentSSRRequest>");
                    #endregion
                    #region SRF request Inbond
                    objreqXml.Append("<ace:SegmentSSRRequest>");
                    objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + newFltDsIN[0]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + newFltDsIN[0]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");
                    objreqXml.Append("<ace:STD>" + Convert.ToDateTime(newFltDsIN[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");
                    objreqXml.Append("<ace:DepartureStation>" + newFltDsIN[0]["DepartureLocation"].ToString() + "</ace:DepartureStation><ace:ArrivalStation>" + newFltDsIN[newFltDsIN.Length - 1]["ArrivalLocation"].ToString() + "</ace:ArrivalStation>");

                    string ftIN = newFltDsIN[0]["AdtRbd"].ToString();
                    if (Convert.ToInt32(newFltDsIN[0]["Infant"]) > 0 || MealBages.Rows.Count > 0 || ft == "EF" || ft == "OF" || ft == "KF" || ft == "ZF" || ft == "IF" || ft == "AF" || ft == "VF" || ft == "PF" || ft == "LF" || ft == "UF" || ft == "TF" || ft == "QF" || ft == "MF" || ft == "YF")
                    {
                        objreqXml.Append("<ace:PaxSSRs>");
                        //SellSSRRequest(fltdt.Rows[0]["Searchvalue"].ToString(), fltdt.Rows[0]["ElectronicTicketing"].ToString(), fltdt.Rows[0]["OperatingCarrier"].ToString(), fltdt.Rows[0]["FlightIdentification"].ToString(), fltdt.Rows[0]["ArrivalLocation"].ToString(), fltdt.Rows[0]["DepartureLocation"].ToString(), Convert.ToDateTime(fltdt.Rows[0]["arrdatelcc"]).ToString("yyyy-MM-ddThh:mm:ss"), Convert.ToDateTime(fltdt.Rows[0]["depdatelcc"]).ToString("yyyy-MM-ddThh:mm:ss"), fltdt.Rows[0]["AdtRbd"].ToString(), Convert.ToInt32(fltdt.Rows[0]["Infant"]), MealBages);
                        for (int ad = 0; ad < Convert.ToInt32(newFltDsIN[0]["Infant"]); ad++)
                        {
                            for (int lg = 0; lg < newFltDsIN.Length; lg++)
                            {
                                objreqXml.Append("<ace:PaxSSR>");
                                // objreqXml.Append("<ace:ActionStatusCode>" + ActionStatusCode + "</ace:ActionStatusCode><ace:ArrivalStation>" + ArrivalStation + "</ace:ArrivalStation><ace:DepartureStation>" + DepartureStation + "</ace:DepartureStation>");
                                objreqXml.Append("<ace:ArrivalStation>" + newFltDsIN[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + newFltDsIN[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                                objreqXml.Append("<ace:PassengerNumber>" + ad + "</ace:PassengerNumber><ace:SSRCode>INFT</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                                objreqXml.Append("</ace:PaxSSR>");
                            }
                        }
                        int mcount = 0, bcount = 0;
                        for (int ssr = 0; ssr < MealBages.Rows.Count; ssr++)
                        {
                            for (int lg = 0; lg < newFltDsIN.Length; lg++)
                            {
                                if (MealBages.Rows[ssr]["MealCode"].ToString() != "")
                                {
                                    string[] MealDiscription = MealBages.Rows[ssr]["MealDesc"].ToString().Split('_');
                                    if (newFltDsIN[lg]["ArrivalLocation"].ToString().Trim() == MealDiscription[2].Trim() && newFltDsIN[lg]["DepartureLocation"].ToString().Trim() == MealDiscription[1].Trim())
                                    {
                                        objreqXml.Append("<ace:PaxSSR>");
                                        // objreqXml.Append("<ace:ActionStatusCode>" + ActionStatusCode + "</ace:ActionStatusCode><ace:ArrivalStation>" + ArrivalStation + "</ace:ArrivalStation><ace:DepartureStation>" + DepartureStation + "</ace:DepartureStation>");
                                        objreqXml.Append("<ace:ArrivalStation>" + MealDiscription[2] + "</ace:ArrivalStation><ace:DepartureStation>" + MealDiscription[1] + "</ace:DepartureStation>");
                                        objreqXml.Append("<ace:PassengerNumber>" + mcount.ToString() + "</ace:PassengerNumber><ace:SSRCode>" + MealBages.Rows[ssr]["MealCode"].ToString() + "</ace:SSRCode>");
                                      // objreqXml.Append("<ace:PassengerNumber>0</ace:PassengerNumber><ace:SSRCode>" + MealBages.Rows[ssr]["MealCode"].ToString() + "</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                                        objreqXml.Append("</ace:PaxSSR>");
                                        mcount++;
                                    }
                                }
                                if (MealBages.Rows[ssr]["BaggageCode"].ToString() != "")
                                {
                                    string[] BaggageDiscription = MealBages.Rows[ssr]["BaggageDesc"].ToString().ToString().Split('_'); ;
                                    if (newFltDsIN[lg]["ArrivalLocation"].ToString().Trim() == BaggageDiscription[2].Trim() && newFltDsIN[lg]["DepartureLocation"].ToString().Trim() == BaggageDiscription[1].Trim())
                                    {
                                        objreqXml.Append("<ace:PaxSSR>");
                                        objreqXml.Append("<ace:ArrivalStation>" + newFltDsIN[newFltDsIN.Length - 1]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + newFltDsIN[0]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                                        objreqXml.Append("<ace:PassengerNumber>" + bcount.ToString() + "</ace:PassengerNumber><ace:SSRCode>" + MealBages.Rows[ssr]["BaggageCode"].ToString() + "</ace:SSRCode>");
                                       // objreqXml.Append("<ace:PassengerNumber>0</ace:PassengerNumber><ace:SSRCode>" + MealBages.Rows[ssr]["BaggageCode"].ToString() + "</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                                        objreqXml.Append("</ace:PaxSSR>");
                                        bcount++;
                                    }
                                }
                            }
                           // mbcount++;
                        }

                        if (ft == "EF" || ft == "OF" || ft == "KF" || ft == "ZF" || ft == "IF" || ft == "AF" || ft == "VF" || ft == "PF" || ft == "LF" || ft == "UF" || ft == "TF" || ft == "QF" || ft == "MF" || ft == "YF")
                        {
                            for (int lg = 0; lg < newFltDsIN.Length; lg++)
                            {
                                objreqXml.Append("<ace:PaxSSR>");
                                objreqXml.Append("<ace:ArrivalStation>" + newFltDsIN[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + newFltDsIN[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                                objreqXml.Append("<ace:SSRCode>HFL</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                                objreqXml.Append("</ace:PaxSSR>");
                            }
                        }
                        objreqXml.Append("</ace:PaxSSRs>");
                    }
                    else
                        objreqXml.Append("<ace:PaxSSRs/>");
                    objreqXml.Append("</ace:SegmentSSRRequest>");
                    #endregion
                }
                objreqXml.Append("</ace:SegmentSSRRequests>");
                objreqXml.Append("<ace:CurrencyCode>" + CurrancyCode + "</ace:CurrencyCode><ace:CancelFirstSSR>false</ace:CancelFirstSSR><ace:SSRFeeForceWaiveOnSell>false</ace:SSRFeeForceWaiveOnSell>");
                objreqXml.Append("</ace:SSRRequest></ace:SellSSR></tem:objSellRequestData></tem:Sell></soapenv:Body></soapenv:Envelope>");

            }
            catch (Exception ex)
            { ExecptionLogger.FileHandling("SellSSRRequest", "Error_001", ex, "AirAsiaFlight"); }
            return objreqXml.ToString();
        }
        public string MultipleSegmentSellSSRRequest(string Sessionids, DataTable fltdt, DataTable MealBages, string CurrancyCode)
        {
            StringBuilder objreqXml = new StringBuilder();
            try
            {
                objreqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:ace=\"http://schemas.datacontract.org/2004/07/ACE.Entities\" xmlns:arr=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">");
                objreqXml.Append("<soapenv:Header/><soapenv:Body><tem:Sell>");
                objreqXml.Append("<tem:strSessionID>" + Sessionids + "</tem:strSessionID>");
                objreqXml.Append("<tem:objSellRequestData><ace:SellBy>SSR</ace:SellBy><ace:SellSSR><ace:SSRRequest>");
                objreqXml.Append("<ace:SegmentSSRRequests>");
                for (int lg = 0; lg < fltdt.Rows.Count; lg++)
                {
                    int meal = 0, bagg = 0;
                    objreqXml.Append("<ace:SegmentSSRRequest>");
                    objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + fltdt.Rows[lg]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + fltdt.Rows[lg]["FlightIdentification"].ToString() + "</ace:FlightNumber></ace:FlightDesignator>");
                    objreqXml.Append("<ace:STD>" + Convert.ToDateTime(fltdt.Rows[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");
                    objreqXml.Append("<ace:DepartureStation>" + fltdt.Rows[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation><ace:ArrivalStation>" + fltdt.Rows[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation>");
                    objreqXml.Append("<ace:PaxSSRs>");
                    for (int ad = 0; ad < Convert.ToInt32(fltdt.Rows[0]["Infant"]); ad++)
                    {
                        objreqXml.Append("<ace:PaxSSR>");
                        objreqXml.Append("<ace:ArrivalStation>" + fltdt.Rows[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + fltdt.Rows[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                        objreqXml.Append("<ace:PassengerNumber>" + ad + "</ace:PassengerNumber><ace:SSRCode>INFT</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                        objreqXml.Append("</ace:PaxSSR>");
                    }
                    for (int ssr = 0; ssr < MealBages.Rows.Count; ssr++)
                    {
                        if (MealBages.Rows[ssr]["MealCode"].ToString() != "")
                        {
                            string[] MealDiscription = MealBages.Rows[ssr]["MealDesc"].ToString().Split('_');
                            if (fltdt.Rows[lg]["DepartureLocation"].ToString().Trim() == MealDiscription[1].Trim() && fltdt.Rows[lg]["ArrivalLocation"].ToString().Trim() == MealDiscription[2].Trim())
                            {
                                objreqXml.Append("<ace:PaxSSR>");
                                // objreqXml.Append("<ace:ActionStatusCode>" + ActionStatusCode + "</ace:ActionStatusCode><ace:ArrivalStation>" + ArrivalStation + "</ace:ArrivalStation><ace:DepartureStation>" + DepartureStation + "</ace:DepartureStation>");
                                objreqXml.Append("<ace:ArrivalStation>" + MealDiscription[2] + "</ace:ArrivalStation><ace:DepartureStation>" + MealDiscription[1] + "</ace:DepartureStation>");
                                // objreqXml.Append("<ace:PassengerNumber>" + ssr + "</ace:PassengerNumber><ace:SSRCode>" + MealBages.Rows[ssr]["MealCode"].ToString() + "</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                                objreqXml.Append("<ace:PassengerNumber>" + meal.ToString() + "</ace:PassengerNumber><ace:SSRCode>" + MealBages.Rows[ssr]["MealCode"].ToString() + "</ace:SSRCode>");
                                objreqXml.Append("</ace:PaxSSR>");
                                meal++;
                            }
                        }
                        if (MealBages.Rows[ssr]["BaggageCode"].ToString() != "")
                        {
                            string[] BaggageDiscription = MealBages.Rows[ssr]["BaggageDesc"].ToString().ToString().Split('_'); ;
                            if (fltdt.Rows[lg]["DepartureLocation"].ToString().Trim() == BaggageDiscription[1].Trim() && fltdt.Rows[lg]["ArrivalLocation"].ToString().Trim() == BaggageDiscription[2].Trim())
                            {
                                objreqXml.Append("<ace:PaxSSR>");
                                objreqXml.Append("<ace:ArrivalStation>" + fltdt.Rows[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + fltdt.Rows[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                                //  objreqXml.Append("<ace:PassengerNumber>" + ssr + "</ace:PassengerNumber><ace:SSRCode>" + MealBages.Rows[ssr]["BaggageCode"].ToString() + "</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                                objreqXml.Append("<ace:PassengerNumber>" + bagg + "</ace:PassengerNumber><ace:SSRCode>" + MealBages.Rows[ssr]["BaggageCode"].ToString() + "</ace:SSRCode>");
                                objreqXml.Append("</ace:PaxSSR>");
                                bagg++;
                            }
                        }
                    }
                    string ft = fltdt.Rows[lg]["AdtRbd"].ToString();
                    if (ft == "EF" || ft == "OF" || ft == "KF" || ft == "ZF" || ft == "IF" || ft == "AF" || ft == "VF" || ft == "PF" || ft == "LF" || ft == "UF" || ft == "TF" || ft == "QF" || ft == "MF" || ft == "YF")
                    {
                        objreqXml.Append("<ace:PaxSSR>");
                        objreqXml.Append("<ace:ArrivalStation>" + fltdt.Rows[lg]["ArrivalLocation"].ToString() + "</ace:ArrivalStation><ace:DepartureStation>" + fltdt.Rows[lg]["DepartureLocation"].ToString() + "</ace:DepartureStation>");
                        objreqXml.Append("<ace:SSRCode>HFL</ace:SSRCode><ace:SSRNumber>0</ace:SSRNumber>");
                        objreqXml.Append("</ace:PaxSSR>");
                    }
                    objreqXml.Append("</ace:PaxSSRs>");
                    objreqXml.Append("</ace:SegmentSSRRequest>");
                }
                objreqXml.Append("</ace:SegmentSSRRequests>");
                objreqXml.Append("<ace:CurrencyCode>" + CurrancyCode + "</ace:CurrencyCode><ace:CancelFirstSSR>false</ace:CancelFirstSSR><ace:SSRFeeForceWaiveOnSell>false</ace:SSRFeeForceWaiveOnSell>");
                objreqXml.Append("</ace:SSRRequest></ace:SellSSR></tem:objSellRequestData></tem:Sell></soapenv:Body></soapenv:Envelope>");

            }
            catch (Exception ex)
            { ExecptionLogger.FileHandling("MultipleSegmentSellSSRRequest", "Error_001", ex, "AirAsiaFlight"); }
            return objreqXml.ToString();
        }
        public string AssignSeatsRequest(string Sessionids, string FlightCarrierCode, string FlightNumber, string ArrivalStation, string DepartureStation, string STA, string STD, string SeatNo, string CurrancyCode)
        {
            StringBuilder objreqXml = new StringBuilder();
            try
            {
                objreqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:ace=\"http://schemas.datacontract.org/2004/07/ACE.Entities\" xmlns:arr=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">");
                objreqXml.Append("<soapenv:Header/><soapenv:Body><tem:AssignSeats>");
                objreqXml.Append("<tem:strSessionID>" + Sessionids + "</tem:strSessionID>");
                objreqXml.Append("<tem:objSeatSellRequest><ace:BlockType>Session</ace:BlockType>");
                objreqXml.Append("<ace:SameSeatRequiredOnThruLegs>false</ace:SameSeatRequiredOnThruLegs><ace:AssignNoSeatIfAlreadyTaken>false</ace:AssignNoSeatIfAlreadyTaken>");
                objreqXml.Append("<ace:AllowSeatSwappingInPNR>true</ace:AllowSeatSwappingInPNR><ace:WaiveFee>false</ace:WaiveFee> <ace:ReplaceSpecificSeatRequest>false</ace:ReplaceSpecificSeatRequest>");
                objreqXml.Append(" <ace:SeatAssignmentMode>PreSeatAssignment</ace:SeatAssignmentMode><ace:IgnoreSeatSSRs>false</ace:IgnoreSeatSSRs>");
                objreqXml.Append("<ace:SegmentSeatRequests>");
                objreqXml.Append("<ace:SegmentSeatRequest>");
                objreqXml.Append("<ace:FlightDesignator><ace:CarrierCode>" + FlightCarrierCode + "</ace:CarrierCode><ace:FlightNumber>" + FlightNumber + "</ace:FlightNumber></ace:FlightDesignator>");
                objreqXml.Append("<ace:STD>" + STD + "</ace:STD>");
                objreqXml.Append("<ace:DepartureStation>" + DepartureStation + "</ace:DepartureStation><ace:ArrivalStation>" + ArrivalStation + "</ace:ArrivalStation>");
                objreqXml.Append("<ace:PassengerNumbers><arr:short>0</arr:short></ace:PassengerNumbers>");
                objreqXml.Append("<ace:UnitDesignator>" + SeatNo + "</ace:UnitDesignator> <ace:CompartmentDesignator/>");
                objreqXml.Append("</ace:SegmentSeatRequest>");
                objreqXml.Append("</ace:SegmentSeatRequests><ace:CollectedCurrencyCode>" + CurrancyCode + "</ace:CollectedCurrencyCode>");
                objreqXml.Append(" </tem:objSeatSellRequest> </tem:AssignSeats></soapenv:Body></soapenv:Envelope>");
            }
            catch (Exception ex)
            { ExecptionLogger.FileHandling("AssignSeatsRequest", "Error_001", ex, "AirAsiaFlight"); }
            return objreqXml.ToString();
        }
        public string MultipleSegmentSSRAvailabilityRequest(string SessionID, DataRow[] fltdt, string RTF, string CurrancyCode)
        {
            StringBuilder objreqXml = new StringBuilder();
            try
            {
                objreqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:ace=\"http://schemas.datacontract.org/2004/07/ACE.Entities\" xmlns:arr=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">");
                objreqXml.Append("<soapenv:Header/><soapenv:Body><tem:GetSSRAvailabilityForBooking>");
                objreqXml.Append("<tem:strSessionID>" + SessionID + "</tem:strSessionID>");
                objreqXml.Append("<tem:objSSRAvailabilityRequest>");
                objreqXml.Append("<ace:SegmentKeyList>");
                objreqXml.Append("<ace:LegKey>");
                objreqXml.Append("<ace:CarrierCode>" + fltdt[0]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:FlightNumber>" + fltdt[0]["FlightIdentification"].ToString() + "</ace:FlightNumber>");
                objreqXml.Append("<ace:DepartureDate>" + Convert.ToDateTime(fltdt[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:DepartureDate><ace:ArrivalDate>" + Convert.ToDateTime(fltdt[0]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:ArrivalDate>");
                objreqXml.Append("<ace:DepartureStation>" + fltdt[0]["DepartureLocation"].ToString() + "</ace:DepartureStation><ace:ArrivalStation>" + fltdt[0]["ArrivalLocation"].ToString() + "</ace:ArrivalStation>");
                objreqXml.Append("</ace:LegKey>");
                objreqXml.Append("</ace:SegmentKeyList>");
                objreqXml.Append("<ace:InventoryControlled>true</ace:InventoryControlled><ace:NonInventoryControlled>true</ace:NonInventoryControlled><ace:SeatDependent>true</ace:SeatDependent><ace:NonSeatDependent>true</ace:NonSeatDependent>");
                objreqXml.Append("<ace:CurrencyCode>" + CurrancyCode + "</ace:CurrencyCode></tem:objSSRAvailabilityRequest></tem:GetSSRAvailabilityForBooking></soapenv:Body></soapenv:Envelope>");
            }
            catch (Exception ex)
            { ExecptionLogger.FileHandling("MultipleSegmentSSRAvailabilityRequest", "Error_001", ex, "AirAsiaFlight"); }
            return objreqXml.ToString();
        }   
        public string MultipleSegmentSeatAvailabilityRequest(string Sessionids, DataRow[] fltdt, string CurrancyCode)
        {
            StringBuilder objreqXml = new StringBuilder();
            try
            {
                objreqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:ace=\"http://schemas.datacontract.org/2004/07/ACE.Entities\" xmlns:arr=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">");
                objreqXml.Append("<soapenv:Header/><soapenv:Body><tem:GetSeatAvailability>");
                objreqXml.Append("<tem:strSessionID>" + Sessionids + "</tem:strSessionID>");
                objreqXml.Append("<tem:objSeatAvailabilityRequest>");
                objreqXml.Append("<ace:STD>" + Convert.ToDateTime(fltdt[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "</ace:STD>");
                objreqXml.Append("<ace:DepartureStation>" + fltdt[0]["DepartureLocation"].ToString() + "</ace:DepartureStation><ace:ArrivalStation>" + fltdt[0]["ArrivalLocation"].ToString() + "</ace:ArrivalStation>");
                objreqXml.Append("<ace:IncludeSeatFees>true</ace:IncludeSeatFees><ace:SeatAssignmentMode>PreSeatAssignment</ace:SeatAssignmentMode>");
                objreqXml.Append("<ace:FlightNumber>" + fltdt[0]["FlightIdentification"].ToString() + "</ace:FlightNumber><ace:CarrierCode>" + fltdt[0]["OperatingCarrier"].ToString() + "</ace:CarrierCode><ace:OpSuffix/>");
                objreqXml.Append("<ace:CompressProperties>false</ace:CompressProperties><ace:EnforceSeatGroupRestrictions>false</ace:EnforceSeatGroupRestrictions>");
                objreqXml.Append("<ace:CollectedCurrencyCode>" + CurrancyCode + "</ace:CollectedCurrencyCode>");
                objreqXml.Append("</tem:objSeatAvailabilityRequest></tem:GetSeatAvailability></soapenv:Body></soapenv:Envelope>");
            }
            catch (Exception ex)
            { ExecptionLogger.FileHandling("MultipleSegmentSeatAvailabilityRequest", "Error_001", ex, "AirAsiaFlight"); }

            return objreqXml.ToString();
        }
    }
}
