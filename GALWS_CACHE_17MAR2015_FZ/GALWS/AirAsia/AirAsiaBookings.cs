﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using ITZERRORLOG;

namespace GALWS.AirAsia
{
   public class AirAsiaBookings
    {
       public string AirAsiaBookingDetails(DataSet Credencilas, DataTable Fltdtlds, DataSet PaxDs, DataSet HeaderDs, ref ArrayList TktNoArray, DataSet AgencyDs, string ConStr, decimal SSRPrice, string CurrancyCode, decimal OriginalFare)
       {
           string PNR = "";
           try
           {
               if (OriginalFare > 0)
               {
                   decimal PaymentedAmt = AddPaymentToBooking(Credencilas, Fltdtlds.Rows[0]["Searchvalue"].ToString(), Fltdtlds.Rows[0]["Track_id"].ToString(), OriginalFare.ToString(), ConStr, CurrancyCode);
                   if (PaymentedAmt > 0)
                   {
                       PNR = Commit(Credencilas, PaxDs, HeaderDs, AgencyDs, Fltdtlds.Rows[0]["Searchvalue"].ToString(), ConStr, Fltdtlds.Rows[0]["Track_id"].ToString());
                       if (PNR != "")
                       {
                           string GetBookingReq = GetBookingRequest(Fltdtlds.Rows[0]["Searchvalue"].ToString(), PNR,CurrancyCode);
                           AirAsiaUtility.SaveFile(GetBookingReq, "GetBookingReq_" + Fltdtlds.Rows[0]["Track_id"].ToString());
                           string GetBookingResponse = AirAsiaUtility.AirAsiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), "http://tempuri.org/IBookingService/GetBooking", GetBookingReq);
                           AirAsiaUtility.SaveFile(GetBookingResponse, "GetBookingRes_" + Fltdtlds.Rows[0]["Track_id"].ToString());

                           XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/"; // XNamespace a = "http://schemas.datacontract.org/2004/07/ACE.Entities";

                           XDocument AirAsiaDocument2 = XDocument.Parse(GetBookingResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                           XElement GetBookingResult = AirAsiaDocument2.Element(s + "Envelope").Element(s + "Body").Element("GetBookingResponse").Element("GetBookingResult");

                           if (GetBookingResult.Element("ExceptionMessage") == null)
                           {
                               PNR = GetBookingResult.Element("RecordLocator").Value.Trim();

                               TktNoArray = new ArrayList();

                               IEnumerable<XElement> Passengers = GetBookingResult.Element("Passengers").Elements("Passenger");
                               foreach (var Passenger in Passengers)
                               {
                                   TktNoArray.Add(Passenger.Element("PassengerID").Value);
                               }
                           }
                           else
                               PNR =  "-FQ";
                       }
                       else
                           PNR = PNR + "AirAsia-FQ";
                   }
                   else
                       PNR = PNR + "AirAsiaPayment-FQ";
               }
           }
           catch(Exception ex)
           {
               PNR = PNR + "-FQ-EXP";
           }
           return PNR;
       }
       public decimal GetAGAPIBalance(DataSet Credencilas, string Sessonid, string Track_id, string SellSsrRate, string ConStr, string CurrancyCode)
       {
            decimal TotalCost = 0;
            try
            {
                        string GetAGPosAmtReq = GetAGPosAmtRequest(Sessonid,  SellSsrRate,  CurrancyCode);
                        AirAsiaUtility.SaveFile(GetAGPosAmtReq, "AGPosAmtReq_" + Track_id);
                        string GetAGPosAmtResponse = AirAsiaUtility.AirAsiaPostXML(Credencilas.Tables[0].Rows[0]["BkgServerUrlOrIP"].ToString(), "http://tempuri.org/ILookupService/GetAGPosAmt", GetAGPosAmtReq);
                        AirAsiaUtility.SaveFile(GetAGPosAmtResponse, "AGPosAmtRes_" + Track_id);

                        XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/"; //XNamespace a = "http://schemas.datacontract.org/2004/07/ACE.Entities"; XNamespace i = "http://www.w3.org/2001/XMLSchema-instance";
                        XDocument AirAsiaDocument = XDocument.Parse(GetAGPosAmtResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                        XElement AGPosAmtResult = AirAsiaDocument.Element(s + "Envelope").Element(s + "Body").Element("GetAGPosAmtResponse").Element("GetAGPosAmtResult");
                        if (AGPosAmtResult.Element("ExceptionMessage") == null)
                        {
                                TotalCost = Convert.ToDecimal(AGPosAmtResult.Element("DisplayAmt").Value.Trim());
                        }
                        else if (AGPosAmtResult.Element("ExceptionMessage").Value.Trim().Contains("Invalid Login.") || AGPosAmtResult.Element("ExceptionMessage").Value.Trim().Contains("Session token authentication failure. : No such session:"))
                        {
                            AirAsiaLogon objlogindtl = new AirAsiaLogon();
                            Sessonid = objlogindtl.GetSessionID(Credencilas.Tables[0].Rows[0]["UserID"].ToString(), Credencilas.Tables[0].Rows[0]["Password"].ToString(), Credencilas.Tables[0].Rows[0]["LoginID"].ToString());

                            AirAsiaUtility objupdatedb = new AirAsiaUtility();
                            objupdatedb.UpdateAirAsiaSessionId(Sessonid, Track_id, ConStr);

                            string GetAGPosAmtReq2 = GetAGPosAmtRequest(Sessonid, SellSsrRate, CurrancyCode);
                            AirAsiaUtility.SaveFile(GetAGPosAmtReq2, "AGPosAmtReq2_" + Track_id);
                            string GetAGPosAmtResponse2 = AirAsiaUtility.AirAsiaPostXML(Credencilas.Tables[0].Rows[0]["BkgServerUrlOrIP"].ToString(), "http://tempuri.org/ILookupService/GetAGPosAmt", GetAGPosAmtReq2);
                            AirAsiaUtility.SaveFile(GetAGPosAmtResponse, "AGPosAmtRes_" + Track_id);

                            XDocument AirAsiaDocument2 = XDocument.Parse(GetAGPosAmtResponse2.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                            XElement AGPosAmtResult2 = AirAsiaDocument.Element(s + "Envelope").Element(s + "Body").Element("GetAGPosAmtResponse").Element("GetAGPosAmtResult");
                            TotalCost = Convert.ToDecimal(AGPosAmtResult2.Element("DisplayAmt").Value.Trim());
                        }
            }
           catch (Exception ex)
           { 
                ExecptionLogger.FileHandling("GetAGAPIBalance", "Error_001", ex, "AirAsiaFlight");
           }
           return TotalCost;
       }
     
       public decimal AddPaymentToBooking(DataSet Credencilas, string Sessonid, string Track_id, string SellSsrRate, string ConStr, string CurrancyCode)
       {
           decimal TotalCost = 0;
           try
           {
               string AddPaymentToBookingReq = AddPaymentToBookingRequest(Sessonid, Credencilas.Tables[0].Rows[0]["UserID"].ToString(), SellSsrRate, CurrancyCode);
               AirAsiaUtility.SaveFile(AddPaymentToBookingReq, "AddPaymentToBookingReq_" + Track_id);
               string AddPaymentToBookingResponse = AirAsiaUtility.AirAsiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), "http://tempuri.org/IBookingService/AddPaymentToBooking", AddPaymentToBookingReq);
               AirAsiaUtility.SaveFile(AddPaymentToBookingResponse, "AddPaymentToBookingRes_" + Track_id);

               XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/"; // XNamespace a = "http://schemas.datacontract.org/2004/07/ACE.Entities"; XNamespace i = "http://www.w3.org/2001/XMLSchema-instance";
               XDocument AirAsiaDocument = XDocument.Parse(AddPaymentToBookingResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
               XElement AddPaymentToBookingResult = AirAsiaDocument.Element(s + "Envelope").Element(s + "Body").Element("AddPaymentToBookingResponse").Element("AddPaymentToBookingResult");
               if (AddPaymentToBookingResult.Element("ExceptionMessage") == null)
               {
                   TotalCost = Convert.ToDecimal(AddPaymentToBookingResult.Element("ValidationPayment").Element("Payment").Element("PaymentAmount").Value.Trim());
               }
               
           }
           catch (Exception ex)
           {
               ExecptionLogger.FileHandling("AddPaymentToBooking", "Error_001", ex, "AirAsiaFlight");
           }
           return TotalCost;
       }

       public string Commit(DataSet Credencilas, DataSet PaxDs, DataSet HeaderDs, DataSet AgencyDs, string Sessonid, string ConStr, string Track_id)
       {
           string PNR = "";

           try
           {
               string CommitReq = CommitRequest(Sessonid, Credencilas.Tables[0].Rows[0]["UserID"].ToString(), PaxDs, HeaderDs, AgencyDs, Credencilas.Tables[0].Rows[0]["CorporateID"].ToString());
               AirAsiaUtility.SaveFile(CommitReq, "CommitReq_" + Track_id);
               string CommitBookingResponse = AirAsiaUtility.AirAsiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), "http://tempuri.org/IBookingService/Commit", CommitReq);
               AirAsiaUtility.SaveFile(CommitBookingResponse, "CommitRes_" + Track_id);

               XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/"; // XNamespace a = "http://schemas.datacontract.org/2004/07/ACE.Entities"; XNamespace i = "http://www.w3.org/2001/XMLSchema-instance";
               XDocument AirAsiaDocument = XDocument.Parse(CommitBookingResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
               XElement CommitResult = AirAsiaDocument.Element(s + "Envelope").Element(s + "Body").Element("CommitResponse").Element("CommitResult");
               if (CommitResult.Element("ExceptionMessage") == null)
               {
                   PNR = CommitResult.Element("Success").Element("RecordLocator").Value.Trim();
               }
           }
           catch(Exception ex)
           {
                ExecptionLogger.FileHandling("GetAGAPIBalance", "Error_001", ex, "AirAsiaFlight");
           }
           return PNR;
       }
       public string GetAGPosAmtRequest(string Sessionids, string SellSsrRate, string CurrancyCode)
       {
           StringBuilder objreqXml = new StringBuilder();
           try
           {
               objreqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:ace=\"http://schemas.datacontract.org/2004/07/ACE.Entities\">");
               objreqXml.Append("<soapenv:Header/><soapenv:Body><tem:GetAGPosAmt>");
               objreqXml.Append("<tem:agPosAmtRequest>");
               objreqXml.Append("<ace:AGPosCurr>USD</ace:AGPosCurr><ace:DisplayCurr>" + CurrancyCode + "</ace:DisplayCurr>");
               objreqXml.Append("<ace:DisplayAmt>" + SellSsrRate + "</ace:DisplayAmt>");
               objreqXml.Append("</tem:agPosAmtRequest>");
               objreqXml.Append("<tem:sessionID>" + Sessionids + "</tem:sessionID>");
               objreqXml.Append("</tem:GetAGPosAmt></soapenv:Body></soapenv:Envelope>");
           }
           catch (Exception ex)
           { ExecptionLogger.FileHandling("GetAGPosAmtRequest", "Error_001", ex, "AirAsiaFlight"); }

           return objreqXml.ToString();
       }
       public string AddPaymentToBookingRequest(string Sessionids, string userid, string SellSsrRate, string CurrancyCode)
       {
           StringBuilder objreqXml = new StringBuilder();
           try
           {
               objreqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:ace=\"http://schemas.datacontract.org/2004/07/ACE.Entities\">");
               objreqXml.Append("<soapenv:Header/><soapenv:Body><tem:AddPaymentToBooking>");
               objreqXml.Append("<tem:strSessionID>" + Sessionids + "</tem:strSessionID>");
               objreqXml.Append("<tem:objAddPaymentToBookingRequestData>");
               objreqXml.Append("<ace:MessageState>New</ace:MessageState><ace:WaiveFee>false</ace:WaiveFee><ace:ReferenceType>Default</ace:ReferenceType><ace:PaymentMethodType>AgencyAccount</ace:PaymentMethodType>");
               objreqXml.Append("<ace:PaymentMethodCode>AG</ace:PaymentMethodCode><ace:QuotedCurrencyCode>" + CurrancyCode + "</ace:QuotedCurrencyCode>");
               objreqXml.Append("<ace:QuotedAmount>" + SellSsrRate + "</ace:QuotedAmount>");
               objreqXml.Append("<ace:AccountNumber>" + userid + "</ace:AccountNumber><ace:Expiration>9999-12-31T23:59:59</ace:Expiration>");
               objreqXml.Append("<ace:ParentPaymentID>0</ace:ParentPaymentID><ace:Installments>1</ace:Installments><ace:PaymentText>PAY BY AG</ace:PaymentText><ace:Deposit>false</ace:Deposit>");
               objreqXml.Append("</tem:objAddPaymentToBookingRequestData></tem:AddPaymentToBooking></soapenv:Body></soapenv:Envelope>");
           }
           catch (Exception ex)
           { ExecptionLogger.FileHandling("AddPaymentToBookingRequest", "Error_001", ex, "AirAsiaFlight"); }

           return objreqXml.ToString();
       }
       public string CommitRequest(string Sessionids, string userid, DataSet PaxDs, DataSet HeaderDs, DataSet AgencyDs, string organisationid)
       {
           StringBuilder objreqXml = new StringBuilder();
           try
           {
               DataRow[] ADTPax = PaxDs.Tables[0].Select("PaxType='ADT'", "PaxId ASC");
               DataRow[] CHDPax = PaxDs.Tables[0].Select("PaxType='CHD'", "PaxId ASC");
               DataRow[] INFPax = PaxDs.Tables[0].Select("PaxType='INF'", "PaxId ASC");

               objreqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:ace=\"http://schemas.datacontract.org/2004/07/ACE.Entities\">");
               objreqXml.Append("<soapenv:Header/><soapenv:Body><tem:Commit>");
               objreqXml.Append("<tem:strSessionID>" + Sessionids + "</tem:strSessionID>");
               objreqXml.Append("<tem:objCommitRequestData><ace:Booking>");
               objreqXml.Append("<ace:State>Clean</ace:State><ace:PaxCount>" + (ADTPax.Count() + CHDPax.Count()).ToString() + "</ace:PaxCount>");
               objreqXml.Append("<ace:Passengers>");
               #region Adult and Infant
               bool Infantover = true;
               for (int i = 0; i < ADTPax.Count(); i++)
               {
                   objreqXml.Append("<ace:Passenger>");
                   objreqXml.Append("<ace:State>Modified</ace:State><ace:PassengerNumber>"+ i.ToString() +"</ace:PassengerNumber>");
                   objreqXml.Append("<ace:Names><ace:BookingName>");
                   string title = ADTPax[i]["Title"].ToString() ;
                   if (ADTPax[i]["Title"].ToString() == "Miss")
                       title = "Ms";
                   else  if (ADTPax[i]["Title"].ToString() == "Mstr")
                       title = "Mr";
                   objreqXml.Append("<ace:State>New</ace:State><ace:FirstName>" + ADTPax[i]["FName"].ToString());
                   if (ADTPax[i]["MName"].ToString() != "")
                       objreqXml.Append(" " + ADTPax[i]["MName"].ToString());
                   objreqXml.Append("</ace:FirstName><ace:LastName>" + ADTPax[i]["LName"].ToString() + "</ace:LastName><ace:Title>" + title + "</ace:Title>");
                   objreqXml.Append("</ace:BookingName></ace:Names>");
                   #region Infant
                   if (INFPax.Count() > 0)
                   {
                       if (Infantover && INFPax.Count() > i )
                       {
                           objreqXml.Append("<ace:Infant>");
                           objreqXml.Append("<ace:State>New</ace:State>");
                           if (!string.IsNullOrEmpty(Convert.ToString(INFPax[i]["DOB"])) && Convert.ToString(INFPax[i]["DOB"]).Trim().Length > 7)
                           {
                               objreqXml.Append("<ace:DOB>");
                               objreqXml.Append(INFPax[i]["DOB"].ToString().Split('/')[2] + "-" + INFPax[i]["DOB"].ToString().Split('/')[1] + "-" + INFPax[i]["DOB"].ToString().Split('/')[0] + "T00:00:00");
                               objreqXml.Append("</ace:DOB>");//
                           }
                           if (INFPax[i]["Gender"].ToString() == "M")
                                objreqXml.Append("<ace:Gender>Male</ace:Gender>");
                           else if (INFPax[i]["Gender"].ToString() == "F")
                               objreqXml.Append("<ace:Gender>Female</ace:Gender>");
                           if (!string.IsNullOrEmpty(Convert.ToString(INFPax[i]["NationalityCode"])))
                               objreqXml.Append("<ace:Nationality>" + INFPax[i]["NationalityCode"].ToString() + "</ace:Nationality><ace:ResidentCountry>" + INFPax[i]["NationalityCode"].ToString() + "</ace:ResidentCountry>");
                           else
                               objreqXml.Append("<ace:Nationality>IN</ace:Nationality><ace:ResidentCountry>IN</ace:ResidentCountry>");
                           //string inftitle = INFPax[i]["Title"].ToString();
                           //if (INFPax[i]["Title"].ToString() == "Miss")
                           //    inftitle = "Ms";
                           //else if (INFPax[i]["Title"].ToString() == "Mstr")
                           //    inftitle = "Mr";
                           objreqXml.Append("<ace:Names><ace:BookingName>");
                           //objreqXml.Append("<ace:State>New</ace:State><ace:FirstName>" + INFPax[i]["FName"] + "</ace:FirstName><ace:LastName>" + INFPax[i]["LName"].ToString() + "</ace:LastName><ace:Title>" + inftitle + "</ace:Title>");
                           objreqXml.Append("<ace:State>New</ace:State><ace:FirstName>" + INFPax[i]["FName"].ToString());
                           if (INFPax[i]["MName"].ToString() != "")
                               objreqXml.Append(" " + INFPax[i]["MName"].ToString());
                           objreqXml.Append("</ace:FirstName><ace:LastName>" + INFPax[i]["LName"].ToString() + "</ace:LastName><ace:Title></ace:Title>");
                           objreqXml.Append("</ace:BookingName></ace:Names>");
                           #region Infant Passport details
                           if (!string.IsNullOrEmpty(Convert.ToString(INFPax[i]["PassportNo"])))
                           {
                               if (INFPax[i]["PassportNo"].ToString() != "Passport No")
                               {
                                   objreqXml.Append("<ace:PassengerTravelDocuments>");
                                   objreqXml.Append("<ace:PassengerTravelDocument>");
                                   objreqXml.Append("<ace:DocTypeCode>P</ace:DocTypeCode>");
                                   if (!string.IsNullOrEmpty(Convert.ToString(INFPax[i]["IssueCountryCode"])))
                                       objreqXml.Append("<ace:IssuedByCode>" + INFPax[i]["IssueCountryCode"].ToString() + "</ace:IssuedByCode>");
                                   objreqXml.Append("<ace:DocNumber>" + INFPax[i]["PassportNo"].ToString() + "</ace:DocNumber>");
                                   if (!string.IsNullOrEmpty(Convert.ToString(INFPax[i]["DOB"])) && Convert.ToString(INFPax[i]["DOB"]).Trim().Length > 7)
                                   {
                                       objreqXml.Append("<ace:DOB>");
                                       objreqXml.Append(INFPax[i]["DOB"].ToString().Split('/')[2] + "-" + INFPax[i]["DOB"].ToString().Split('/')[1] + "-" + INFPax[i]["DOB"].ToString().Split('/')[0] + "T00:00:00");
                                       objreqXml.Append("</ace:DOB>");//
                                   }
                                   if (INFPax[i]["Gender"].ToString() == "M")
                                       objreqXml.Append("<ace:Gender>Male</ace:Gender>");
                                   else if (INFPax[i]["Gender"].ToString() == "F")
                                       objreqXml.Append("<ace:Gender>Female</ace:Gender>");
                                   if (!string.IsNullOrEmpty(Convert.ToString(INFPax[i]["NationalityCode"])))
                                       objreqXml.Append("<ace:Nationality>" + INFPax[i]["NationalityCode"].ToString() + "</ace:Nationality>");
                                   else
                                       objreqXml.Append("<ace:Nationality>IN</ace:Nationality><ace:BirthCountry>IN</ace:BirthCountry>");
                                   if (!string.IsNullOrEmpty(Convert.ToString(INFPax[i]["PassportExpireDate"])) && Convert.ToString(INFPax[i]["PassportExpireDate"]).Trim().Length > 7)
                                   {
                                       string[] ExpiryDateSTR = INFPax[i]["PassportExpireDate"].ToString().Split('/');
                                       int Issueyear = Convert.ToInt32(ExpiryDateSTR[2].Trim());
                                       objreqXml.Append("<ace:ExpirationDate>" + ExpiryDateSTR[2] + "-" + ExpiryDateSTR[1] + "-" + ExpiryDateSTR[0] + "T00:00:00</ace:ExpirationDate>");

                                       objreqXml.Append("<ace:Names><ace:BookingName>");
                                       objreqXml.Append("<ace:State>New</ace:State><ace:FirstName>" + INFPax[i]["FName"].ToString());
                                       if (INFPax[i]["MName"].ToString() != "")
                                           objreqXml.Append(" " + INFPax[i]["MName"].ToString());
                                       objreqXml.Append("</ace:FirstName><ace:LastName>" + INFPax[i]["LName"].ToString() + "</ace:LastName><ace:Title></ace:Title>");
                                       objreqXml.Append("</ace:BookingName></ace:Names>");

                                       objreqXml.Append("<ace:IssuedDate>" + (Issueyear - 10).ToString() + "-" + ExpiryDateSTR[1] + "-" + ExpiryDateSTR[0] + "T00:00:00</ace:IssuedDate>");//
                                   }
                                   objreqXml.Append("</ace:PassengerTravelDocument>");
                                   objreqXml.Append("</ace:PassengerTravelDocuments>");
                               }
                           }
                           #endregion
                           objreqXml.Append("</ace:Infant>");
                           if (INFPax.Count() == i + 1)
                               Infantover = false;
                       }
                   }
                   #endregion
                   objreqXml.Append("<ace:PassengerInfo>");
                   if (ADTPax[i]["Gender"].ToString() == "M")
                       objreqXml.Append("<ace:Gender>Male</ace:Gender>");
                   else if (ADTPax[i]["Gender"].ToString() == "F")
                       objreqXml.Append("<ace:Gender>Female</ace:Gender>");
                   if (!string.IsNullOrEmpty(Convert.ToString(ADTPax[i]["NationalityCode"])))
                       objreqXml.Append("<ace:Nationality>" + ADTPax[i]["NationalityCode"].ToString() + "</ace:Nationality><ace:ResidentCountry>" + ADTPax[i]["NationalityCode"].ToString() + "</ace:ResidentCountry>");
                   else
                       objreqXml.Append("<ace:Nationality>IN</ace:Nationality><ace:ResidentCountry>IN</ace:ResidentCountry>");
                   if (ADTPax[i]["Gender"].ToString() == "M")
                       objreqXml.Append("<ace:WeightCategory>Male</ace:WeightCategory>");
                   else if (ADTPax[i]["Gender"].ToString() == "F")
                       objreqXml.Append("<ace:WeightCategory>Female</ace:WeightCategory>");
                   objreqXml.Append("</ace:PassengerInfo>");

                   #region Adult Passport details
                   if (!string.IsNullOrEmpty(Convert.ToString(ADTPax[i]["PassportNo"])))
                   {
                       if (ADTPax[i]["PassportNo"].ToString() != "Passport No")
                       {
                           objreqXml.Append("<ace:PassengerTravelDocuments>");
                           objreqXml.Append("<ace:PassengerTravelDocument>");
                           objreqXml.Append("<ace:DocTypeCode>P</ace:DocTypeCode>");
                           if (!string.IsNullOrEmpty(Convert.ToString(ADTPax[i]["IssueCountryCode"])))
                               objreqXml.Append("<ace:IssuedByCode>" + ADTPax[i]["IssueCountryCode"].ToString() + "</ace:IssuedByCode>");
                           objreqXml.Append("<ace:DocNumber>" + ADTPax[i]["PassportNo"].ToString() + "</ace:DocNumber>");
                           if (!string.IsNullOrEmpty(Convert.ToString(ADTPax[i]["DOB"])) && Convert.ToString(ADTPax[i]["DOB"]).Trim().Length > 7)
                           {
                               objreqXml.Append("<ace:DOB>");
                               objreqXml.Append(ADTPax[i]["DOB"].ToString().Split('/')[2] + "-" + ADTPax[i]["DOB"].ToString().Split('/')[1] + "-" + ADTPax[i]["DOB"].ToString().Split('/')[0] + "T00:00:00");
                               objreqXml.Append("</ace:DOB>");//
                           }
                           if (ADTPax[i]["Gender"].ToString() == "M")
                               objreqXml.Append("<ace:Gender>Male</ace:Gender>");
                           else if (ADTPax[i]["Gender"].ToString() == "F")
                               objreqXml.Append("<ace:Gender>Female</ace:Gender>");
                           if (!string.IsNullOrEmpty(Convert.ToString(ADTPax[i]["NationalityCode"])))
                               objreqXml.Append("<ace:Nationality>" + ADTPax[i]["NationalityCode"].ToString() + "</ace:Nationality>");
                           else
                               objreqXml.Append("<ace:Nationality>IN</ace:Nationality>");
                           if (!string.IsNullOrEmpty(Convert.ToString(ADTPax[i]["PassportExpireDate"])) && Convert.ToString(ADTPax[i]["PassportExpireDate"]).Trim().Length > 7)
                           {
                               string[] ExpiryDateSTR= ADTPax[i]["PassportExpireDate"].ToString().Split('/');
                               int Issueyear = Convert.ToInt32(ExpiryDateSTR[2].Trim());
                               objreqXml.Append("<ace:ExpirationDate>" + ExpiryDateSTR[2] + "-" + ExpiryDateSTR[1] + "-" + ExpiryDateSTR[0] + "T00:00:00</ace:ExpirationDate>");

                               objreqXml.Append("<ace:Names><ace:BookingName>");
                               objreqXml.Append("<ace:State>New</ace:State><ace:FirstName>" + ADTPax[i]["FName"].ToString() );
                               if (ADTPax[i]["MName"].ToString() != "")
                                   objreqXml.Append(" " + ADTPax[i]["MName"].ToString());
                               objreqXml.Append("</ace:FirstName><ace:LastName>" + ADTPax[i]["LName"].ToString() + "</ace:LastName><ace:Title>" + title + "</ace:Title>");
                               objreqXml.Append("</ace:BookingName></ace:Names>");
                               if (!string.IsNullOrEmpty(Convert.ToString(ADTPax[i]["NationalityCode"])))
                                   objreqXml.Append("<ace:BirthCountry>" + ADTPax[i]["NationalityCode"].ToString() + "</ace:BirthCountry>");
                               else
                                   objreqXml.Append("<ace:BirthCountry>IN</ace:BirthCountry>");
                               objreqXml.Append("<ace:IssuedDate>" + (Issueyear-10).ToString() + "-" + ExpiryDateSTR[1] + "-" + ExpiryDateSTR[0] + "T00:00:00</ace:IssuedDate>");//
                           } 
                           objreqXml.Append("</ace:PassengerTravelDocument>");
                           objreqXml.Append("</ace:PassengerTravelDocuments>");
                       }
                   }
                   #endregion

                   objreqXml.Append("<ace:PassengerTypeInfos><ace:PassengerTypeInfo>");
                   objreqXml.Append("<ace:State>New</ace:State>");
                   if (!string.IsNullOrEmpty(Convert.ToString(ADTPax[i]["DOB"])) && Convert.ToString(ADTPax[i]["DOB"]).Trim().Length > 7)
                   {
                       objreqXml.Append("<ace:DOB>");
                       objreqXml.Append(ADTPax[i]["DOB"].ToString().Split('/')[2] + "-" + ADTPax[i]["DOB"].ToString().Split('/')[1] + "-" + ADTPax[i]["DOB"].ToString().Split('/')[0] + "T00:00:00");
                       objreqXml.Append("</ace:DOB>");//
                   }
                   objreqXml.Append("<ace:PaxType>ADT</ace:PaxType>");
                   objreqXml.Append("</ace:PassengerTypeInfo></ace:PassengerTypeInfos><ace:PseudoPassenger>false</ace:PseudoPassenger>");
                   //if(i==0)
                   //     objreqXml.Append("<ace:CustomerNumber>" + HeaderDs.Tables[0].Rows[0]["PgMobile"].ToString() + "</ace:CustomerNumber>");
                   objreqXml.Append("</ace:Passenger>");
               }
               #endregion
               #region Child
               for (int i = 0; i < CHDPax.Count(); i++)
               {
                   objreqXml.Append("<ace:Passenger>");
                   objreqXml.Append("<ace:State>Modified</ace:State><ace:PassengerNumber>" + (ADTPax.Count() + i).ToString() + "</ace:PassengerNumber>");
                   objreqXml.Append("<ace:Names><ace:BookingName>");
                   objreqXml.Append("<ace:State>New</ace:State><ace:FirstName>" + CHDPax[i]["FName"].ToString());
                   if (CHDPax[i]["MName"].ToString() != "")
                       objreqXml.Append(" " + CHDPax[i]["MName"].ToString());
                   objreqXml.Append("</ace:FirstName><ace:LastName>" + CHDPax[i]["LName"].ToString() + "</ace:LastName><ace:Title>CHD</ace:Title>");
                   objreqXml.Append("</ace:BookingName></ace:Names>");
                 
                   objreqXml.Append("<ace:PassengerInfo>");
                   if (CHDPax[i]["Gender"].ToString() == "M")
                       objreqXml.Append("<ace:Gender>Male</ace:Gender>");
                   else if (CHDPax[i]["Gender"].ToString() == "F")
                       objreqXml.Append("<ace:Gender>Female</ace:Gender>");
                   if (!string.IsNullOrEmpty(Convert.ToString(CHDPax[i]["NationalityCode"])))
                       objreqXml.Append("<ace:Nationality>" + CHDPax[i]["NationalityCode"].ToString() + "</ace:Nationality><ace:ResidentCountry>" + CHDPax[i]["NationalityCode"].ToString() + "</ace:ResidentCountry>");
                   else
                       objreqXml.Append("<ace:Nationality>IN</ace:Nationality><ace:ResidentCountry>IN</ace:ResidentCountry>");
                   objreqXml.Append("<ace:BalanceDue>0</ace:BalanceDue><ace:TotalCost>0</ace:TotalCost><ace:WeightCategory>Child</ace:WeightCategory>");
                   objreqXml.Append("</ace:PassengerInfo>");
                   #region Child Passport details
                   if (!string.IsNullOrEmpty(Convert.ToString(CHDPax[i]["PassportNo"])))
                   {
                       if (CHDPax[i]["PassportNo"].ToString() != "Passport No")
                       {
                           objreqXml.Append("<ace:PassengerTravelDocuments>");
                           objreqXml.Append("<ace:PassengerTravelDocument>");
                           objreqXml.Append("<ace:DocTypeCode>P</ace:DocTypeCode>");
                           if (!string.IsNullOrEmpty(Convert.ToString(CHDPax[i]["IssueCountryCode"])))
                               objreqXml.Append("<ace:IssuedByCode>" + CHDPax[i]["IssueCountryCode"].ToString() + "</ace:IssuedByCode>");
                           objreqXml.Append("<ace:DocNumber>" + CHDPax[i]["PassportNo"].ToString() + "</ace:DocNumber>");
                           if (!string.IsNullOrEmpty(Convert.ToString(CHDPax[i]["DOB"])) && Convert.ToString(CHDPax[i]["DOB"]).Trim().Length > 7)
                           {
                               objreqXml.Append("<ace:DOB>");
                               objreqXml.Append(CHDPax[i]["DOB"].ToString().Split('/')[2] + "-" + CHDPax[i]["DOB"].ToString().Split('/')[1] + "-" + CHDPax[i]["DOB"].ToString().Split('/')[0] + "T00:00:00");
                               objreqXml.Append("</ace:DOB>");//
                           }
                           if (CHDPax[i]["Gender"].ToString() == "M")
                               objreqXml.Append("<ace:Gender>Male</ace:Gender>");
                           else if (CHDPax[i]["Gender"].ToString() == "F")
                               objreqXml.Append("<ace:Gender>Female</ace:Gender>");
                           if (!string.IsNullOrEmpty(Convert.ToString(CHDPax[i]["NationalityCode"])))
                               objreqXml.Append("<ace:Nationality>" + CHDPax[i]["NationalityCode"].ToString() + "</ace:Nationality><ace:BirthCountry>" + CHDPax[i]["NationalityCode"].ToString() + "</ace:BirthCountry>");
                           else
                               objreqXml.Append("<ace:Nationality>IN</ace:Nationality><ace:BirthCountry>IN</ace:BirthCountry>");
                           if (!string.IsNullOrEmpty(Convert.ToString(CHDPax[i]["PassportExpireDate"])) && Convert.ToString(ADTPax[i]["PassportExpireDate"]).Trim().Length > 7)
                           {
                               string[] ExpiryDateSTR = CHDPax[i]["PassportExpireDate"].ToString().Split('/');
                               int Issueyear = Convert.ToInt32(ExpiryDateSTR[2].Trim());
                               objreqXml.Append("<ace:ExpirationDate>" + ExpiryDateSTR[2] + "-" + ExpiryDateSTR[1] + "-" + ExpiryDateSTR[0] + "T00:00:00</ace:ExpirationDate>");

                               objreqXml.Append("<ace:Names><ace:BookingName>");
                               objreqXml.Append("<ace:State>New</ace:State><ace:FirstName>" + CHDPax[i]["FName"].ToString() );
                               if (CHDPax[i]["MName"].ToString() != "")
                                   objreqXml.Append(" " + CHDPax[i]["MName"].ToString());
                               objreqXml.Append("</ace:FirstName><ace:LastName>" + CHDPax[i]["LName"].ToString() + "</ace:LastName><ace:Title>CHD</ace:Title>");
                               objreqXml.Append("</ace:BookingName></ace:Names>");

                               objreqXml.Append("<ace:IssuedDate>" + (Issueyear - 10).ToString() + "-" + ExpiryDateSTR[1] + "-" + ExpiryDateSTR[0] + "T00:00:00</ace:IssuedDate>");//
                           }
                           objreqXml.Append("</ace:PassengerTravelDocument>");
                           objreqXml.Append("</ace:PassengerTravelDocuments>");
                       }
                   }
                   #endregion

                   objreqXml.Append("<ace:PassengerTypeInfos><ace:PassengerTypeInfo>");
                   objreqXml.Append("<ace:State>New</ace:State>");
                   if (!string.IsNullOrEmpty(Convert.ToString(CHDPax[i]["DOB"])) && Convert.ToString(CHDPax[i]["DOB"]).Trim().Length > 7)
                   {
                       objreqXml.Append("<ace:DOB>");
                       objreqXml.Append(CHDPax[i]["DOB"].ToString().Split('/')[2] + "-" + CHDPax[i]["DOB"].ToString().Split('/')[1] + "-" + CHDPax[i]["DOB"].ToString().Split('/')[0] + "T00:00:00");
                       objreqXml.Append("</ace:DOB>");//
                   }
                   objreqXml.Append("<ace:PaxType>CHD</ace:PaxType>");
                   objreqXml.Append("</ace:PassengerTypeInfo></ace:PassengerTypeInfos><ace:PseudoPassenger>false</ace:PseudoPassenger>");

                   objreqXml.Append("</ace:Passenger>");
               }
               #endregion
               objreqXml.Append("</ace:Passengers>");
               #region Agency Details 
               objreqXml.Append("<ace:BookingContacts><ace:BookingContact>");
               objreqXml.Append(" <ace:State>New</ace:State><ace:TypeCode>P</ace:TypeCode>");
               objreqXml.Append("<ace:Names><ace:BookingName><ace:State>New</ace:State><ace:FirstName>" + AgencyDs.Tables[0].Rows[0]["Fname"].ToString().Replace("&", "") + "</ace:FirstName><ace:LastName>" + AgencyDs.Tables[0].Rows[0]["Lname"].ToString().Replace("&", "") + "</ace:LastName><ace:Title>" + AgencyDs.Tables[0].Rows[0]["Title"].ToString().Replace(".", "").Trim() + "</ace:Title></ace:BookingName></ace:Names>");
         
              // objreqXml.Append("<ace:EmailAddress>07seaz@gmail.com</ace:EmailAddress>");
               if (HeaderDs.Tables[0].Rows[0]["PgEmail"].ToString().Length > 11)
                   objreqXml.Append("<ace:EmailAddress>" + HeaderDs.Tables[0].Rows[0]["PgEmail"].ToString() + "</ace:EmailAddress>");
               else
                   objreqXml.Append("<ace:EmailAddress>07seaz@gmail.com</ace:EmailAddress>");
              
               if (HeaderDs.Tables[0].Rows[0]["PgMobile"].ToString().Length > 4)
                   objreqXml.Append("<ace:HomePhone>" + HeaderDs.Tables[0].Rows[0]["PgMobile"].ToString() + "</ace:HomePhone>");
               else if (AgencyDs.Tables[0].Rows[0]["Mobile"].ToString().Length > 4 && HeaderDs.Tables[0].Rows[0]["PgMobile"].ToString().Length < 5)
                   objreqXml.Append("<ace:HomePhone>" + AgencyDs.Tables[0].Rows[0]["Mobile"].ToString() + "</ace:HomePhone>");

               if (AgencyDs.Tables[0].Rows[0]["Mobile"].ToString().Length > 4 )
                   objreqXml.Append("<ace:OtherPhone>" + AgencyDs.Tables[0].Rows[0]["Mobile"].ToString() + "</ace:OtherPhone>");
              
               //objreqXml.Append("<ace:HomePhone>" + AgencyDs.Tables[0].Rows[0]["Mobile"].ToString() + "</ace:HomePhone>");
               //objreqXml.Append("<ace:OtherPhone>" + HeaderDs.Tables[0].Rows[0]["PgMobile"].ToString() + "</ace:OtherPhone>");
               objreqXml.Append("<ace:CompanyName>SEVEN SEAZ VACATION PVT LTD</ace:CompanyName>");
               objreqXml.Append("<ace:AddressLine1>H-5, South Extension Part 1</ace:AddressLine1>");
               objreqXml.Append("<ace:City>New Delhi</ace:City> <ace:ProvinceState>DL</ace:ProvinceState>");
              
               //string agencyaddress = AgencyDs.Tables[0].Rows[0]["Address"].ToString();
               //if (agencyaddress.Length < 53)
               //    objreqXml.Append("<ace:AddressLine1>" + agencyaddress + "</ace:AddressLine1>");
               //else
               //{
               //    objreqXml.Append("<ace:AddressLine1>" + agencyaddress.Substring(0,52) + "</ace:AddressLine1>");                 
               //    if (agencyaddress.Length < 105)
               //        objreqXml.Append("<ace:AddressLine2>" + agencyaddress.Substring(52, agencyaddress.Length - 52) + "</ace:AddressLine2>");
               //    else
               //    {
               //        objreqXml.Append("<ace:AddressLine2>" + agencyaddress.Substring(52, 52) + "</ace:AddressLine2>");
               //        objreqXml.Append("<ace:AddressLine3>" + agencyaddress.Substring(104, agencyaddress.Length - 104) + "</ace:AddressLine3>");
               //    }
               //}
              
               //objreqXml.Append("<ace:City>" + AgencyDs.Tables[0].Rows[0]["City"].ToString() + "</ace:City> <ace:ProvinceState>" + AgencyDs.Tables[0].Rows[0]["State"].ToString().Substring(0,2) + "</ace:ProvinceState>");
               objreqXml.Append("<ace:PostalCode>110049</ace:PostalCode> <ace:CountryCode>IN</ace:CountryCode> <ace:CultureCode>en-GB</ace:CultureCode><ace:DistributionOption>Email</ace:DistributionOption>");
              // objreqXml.Append("<ace:CustomerNumber>" + HeaderDs.Tables[0].Rows[0]["PgMobile"].ToString() + "</ace:CustomerNumber>");
               //objreqXml.Append("<ace:PostalCode><ace:PostalCode>");
               objreqXml.Append("<ace:NotificationPreference>None</ace:NotificationPreference><ace:SourceOrganization>" + organisationid + "</ace:SourceOrganization>");
               objreqXml.Append("</ace:BookingContact></ace:BookingContacts>");
               #endregion
               objreqXml.Append("</ace:Booking>");
               objreqXml.Append("<ace:RestrictionOverride>true</ace:RestrictionOverride><ace:ChangeHoldDateTime>false</ace:ChangeHoldDateTime><ace:WaiveNameChangeFee>false</ace:WaiveNameChangeFee>");
               objreqXml.Append("<ace:WaivePenaltyFee>false</ace:WaivePenaltyFee> <ace:WaiveSpoilageFee>false</ace:WaiveSpoilageFee><ace:DistributeToContacts>true</ace:DistributeToContacts>");
               objreqXml.Append("</tem:objCommitRequestData></tem:Commit></soapenv:Body></soapenv:Envelope>");
           }
           catch (Exception ex)
           { ExecptionLogger.FileHandling("AddPaymentToBookingRequest", "Error_001", ex, "AirAsiaFlight"); }

           return objreqXml.ToString();
       }
  
       public string GetBookingRequest(string Sessionids, string PNR, string CurrancyCode)
       {
           StringBuilder objreqXml = new StringBuilder();
           try
           {
               objreqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:ace=\"http://schemas.datacontract.org/2004/07/ACE.Entities\">");
               objreqXml.Append("<soapenv:Header/><soapenv:Body><tem:GetBooking>");
               objreqXml.Append("<tem:strSessionID>" + Sessionids + "</tem:strSessionID>");
               objreqXml.Append("<tem:objGetBookingRequestData>");
               objreqXml.Append("<ace:GetByRecordLocator><ace:RecordLocator>" + PNR + "</ace:RecordLocator></ace:GetByRecordLocator>");
               objreqXml.Append("</tem:objGetBookingRequestData>");
               objreqXml.Append("</tem:GetBooking></soapenv:Body></soapenv:Envelope>");
           }
           catch (Exception ex)
           { ExecptionLogger.FileHandling("GetBookingRequest", "Error_001", ex, "AirAsiaFlight"); }

           return objreqXml.ToString();
       }
    }
}
