﻿using STD.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace OnlineCancellation
{
    
   public  class PNRCancellationBAL
    {
        string connStr = "";
        public PNRCancellationBAL(string connectionString)
        {
            connStr = connectionString;
        }

        public List<SegmentDetail> GetCancellationSegmentDetails(string OrderID, string PaxID, string Action)
        {
            List<SegmentDetail> obj = new List<SegmentDetail>();
            int legCount = 0;
            int TotPax = 0;
            int TotPaxLive = 0;
            string Msg = "";
            try
            {
                PNRCancellationDAL objDal = new PNRCancellationDAL(connStr.Trim());
                obj = objDal.GetCancellationSegmentDetails(OrderID, PaxID, Action);
                string VC = obj.Select(x => x.VC).Distinct().First();
                string Provider = obj.Select(x => x.Provider).Distinct().First();
                //TotPax = obj.Select(x => x.PaxId).Distinct().ToList().Count();
                //if (Provider == "1G")
                //{
                //    TotPax = obj.Select(x => x.PaxId).Distinct().ToList().Count();
                //    string GDSPNR = obj.Select(x => x.GDSPNR).Distinct().First();
                //    string Trip = obj.Select(x => x.Trip).Distinct().First();
                //    string CorpId = obj.Select(x => x.CorpId).Distinct().First();
                //    UAPITrans uAPITrans = new UAPITrans(connStr);
                //    Dictionary<string, string> xmlo = new Dictionary<string, string>();
                //    string BkgRes = uAPITrans.RetriveBooking(Provider, GDSPNR, false, VC, Trip, CorpId, ref xmlo);
                //    Msg = xmlo.ContainsKey("ERROR") ? xmlo["ERROR"] : ((string.IsNullOrEmpty(BkgRes) || BkgRes.Contains("faultcode")) ? "Unable to retrieve pnr from host." : "");
                //    //BkgRes = System.IO.File.ReadAllText(@"C:\Users\manish.gupta\Desktop\test.xml");
                //    XDocument xd = XDocument.Parse(BkgRes);
                //    System.Xml.Linq.XNamespace Pns = "http://www.travelport.com/schema/air_" + VersionUAPI.APIVersion + "";
                //    System.Xml.Linq.XNamespace universal = "http://www.travelport.com/schema/universal_" + VersionUAPI.APIVersion + "";
                //    System.Xml.Linq.XNamespace common = "http://www.travelport.com/schema/common_" + VersionUAPI.APIVersion + "";
                //    var AirSegment = xd.Descendants(universal + "UniversalRecordRetrieveRsp").Descendants(universal + "UniversalRecord").Descendants(Pns + "AirReservation").Descendants(Pns + "AirSegment");
                //    var AirPricingInfo = xd.Descendants(universal + "UniversalRecordRetrieveRsp").Descendants(universal + "UniversalRecord").Descendants(Pns + "AirReservation").Descendants(Pns + "AirPricingInfo");
                //    var TicketInfo = xd.Descendants(universal + "UniversalRecordRetrieveRsp").Descendants(universal + "UniversalRecord").Descendants(Pns + "AirReservation").Descendants(Pns + "DocumentInfo").Descendants(Pns + "TicketInfo");
                //    var BookingTraveler = xd.Descendants(universal + "UniversalRecordRetrieveRsp").Descendants(universal + "UniversalRecord").Descendants(common + "BookingTraveler");
                //    TotPaxLive = BookingTraveler.Count();
                //    foreach (var airSegment in AirSegment)
                //    {
                //        string ArrCode = "";
                //        string DepCode = "";
                //        string ArrivalStation = airSegment.Attributes("Destination").Any() == true ? airSegment.Attributes("Destination").First().Value : "";
                //        string DepartureStation = airSegment.Attributes("Origin").Any() == true ? airSegment.Attributes("Origin").First().Value : "";
                //        string DeptDate = airSegment.Attributes("DepartureTime").Any() == true ? Convert.ToDateTime(airSegment.Attributes("DepartureTime").First().Value).ToString("dd MMM yyyy") : "";
                //        string DeptTime = airSegment.Attributes("DepartureTime").Any() == true ? Convert.ToDateTime(airSegment.Attributes("DepartureTime").First().Value).ToString("HH:mm") : "";
                //        string ArrDate = airSegment.Attributes("ArrivalTime").Any() == true ? Convert.ToDateTime(airSegment.Attributes("ArrivalTime").First().Value).ToString("dd MMM yyyy") : "";
                //        string ArrTime = airSegment.Attributes("ArrivalTime").Any() == true ? Convert.ToDateTime(airSegment.Attributes("ArrivalTime").First().Value).ToString("HH:mm") : "";
                //        string FltNo = airSegment.Attributes("FlightNumber").Any() == true ? airSegment.Attributes("FlightNumber").First().Value.Trim() : "";
                //        string Key = airSegment.Attributes("Key").Any() == true ? airSegment.Attributes("Key").First().Value.Trim() : "";

                //        for (int i = 0; i < obj.Count; i++)
                //        {
                //            if (ArrivalStation.Trim() == obj[i].ArrAirCode.Trim() && DepartureStation.Trim() == obj[i].DepAirCode.Trim() && obj[i].DeptDate == Convert.ToDateTime(DeptDate).ToString("ddMMyy") && obj[i].FltNo.Trim() == FltNo)
                //            {
                //                try
                //                {
                //                    obj[i].DeptDate = DeptDate;
                //                    obj[i].DeptTime = DeptTime;
                //                    obj[i].ArrDate = ArrDate;
                //                    obj[i].ArrTime = ArrTime;
                //                }
                //                catch (Exception ex) { }
                //                ArrCode = ArrivalStation;
                //                DepCode = DepartureStation;
                //                obj[i].Segment = DepCode + ":" + ArrCode;
                //                obj[i].SegmentStatus = "Live";
                //                obj[i].root = DepCode + ArrCode;
                //                obj[i].Key = Key;
                //                try
                //                {
                //                    if (obj[i].TicketNumber == "" && TicketInfo.Count() > 0)
                //                    {
                //                        var AirPricingInfo_Key = AirPricingInfo.Where(x => x.Descendants(Pns + "BookingInfo").Select(y => y.Attribute("SegmentRef").Value).Where(z => z.ToString() == Key).Any()).Attributes("Key").ToList(); // ? AirPricingInfo.Where(x => x.Descendants(Pns + "BookingInfo").Attributes("SegmentRef").First().Value == Key).Attributes("Key").ToList() : null;
                //                        if (AirPricingInfo_Key != null)
                //                        {
                //                            foreach (var APK in AirPricingInfo_Key)
                //                            {
                //                                try
                //                                {
                //                                    var TicketInfoalt = TicketInfo.Where(x => x.Attributes("AirPricingInfoRef").Any());
                //                                    obj[i].TicketNumber = TicketInfoalt.Where(x => x.Descendants(common + "Name").Attributes("First").First().Value == obj[i].PaxFName && x.Descendants(common + "Name").Attributes("Last").First().Value == obj[i].PaxLName && x.Attributes("AirPricingInfoRef").First().Value == APK.Value).Any() ? TicketInfoalt.Where(x => x.Descendants(common + "Name").Attributes("First").First().Value == obj[i].PaxFName && x.Descendants(common + "Name").Attributes("Last").First().Value == obj[i].PaxLName && x.Attributes("AirPricingInfoRef").First().Value == APK.Value).Attributes("Number").First().Value : "";
                //                                }
                //                                catch { }
                //                                if (obj[i].TicketNumber != "") break;
                //                            }
                //                        }

                //                        // obj[i].TicketNumber = TicketInfo.Where(x => x.Descendants(common + "Name").Attributes("First").First().Value == obj[i].PaxFName && x.Descendants(common + "Name").Attributes("Last").First().Value == obj[i].PaxLName).Attributes("Number").First().Value;
                //                    }
                //                }
                //                catch (Exception ex) { }
                //            }
                //        }
                //    }
                //}
                //else
                //{
                #region 6E and SG V4
                //List<CredentialList> CrdList;

               
               //DAL.Credentials objCrd = new DAL.Credentials(connStr);
               //FlightCommonBAL objFleSBal = new FlightCommonBAL(connStr);
                    string Trip = obj.Select(x => x.Trip).Distinct().First();
                    string CorpId = obj.Select(x => x.CorpId).Distinct().First();
                //DataSet ds = objFleSBal.GetAllFlightDetailsByOrderId(OrderID);
                PNRCancellationDAL objDA = new PNRCancellationDAL(connStr);
                    DataSet FltHdrDs = objDA.GetHdrDetails(OrderID);

                //DataTable FltDT = ds.Tables[0];
                //string idType = Convert.ToString(FltDT.Rows[0]["TripCnt"]);

                STD.DAL.Credentials objCrd = new STD.DAL.Credentials(connStr);
                List<STD.Shared.CredentialList> CrdList = objCrd.GetServiceCredentials("LCC");
                CrdList = CrdList.Where(X => X.AirlineCode == VC && X.LoginID== Convert.ToString(FltHdrDs.Tables[0].Rows[0]["TicketId"]).ToUpper().Trim()).ToList();

                //CrdList = objCrd.GetGALBookingCredentials(Trip, CorpId, VC.ToUpper(), VC, idType, "LCC");
                    #endregion
                    if (CrdList != null && CrdList.Count > 0 && CrdList[0].ServerIP.ToUpper().Trim() == "V4")
                    {

                        //if (VC == "6E")
                        //{
                        //    TotPax = obj.Where(x => x.PaxType != "INF").Select(x => x.PaxId).Distinct().ToList().Count();
                        //    navitaire.indigo.bm.ver4.GetBookingResponse BkgRes = BAL.Flight.INDIGONAV.INDIGONAV4.RetriveBooking(OrderID, out Msg);
                        //    TotPaxLive = BkgRes.Booking.Passengers.Count();
                        //    foreach (var j in BkgRes.Booking.Journeys)
                        //    {
                        //        #region Get root
                        //        int TSeg = j.Segments.Count();
                        //        int n = 0;
                        //        string root = "";
                        //        foreach (var seg in j.Segments)
                        //        {
                        //            if (n == 0)
                        //            {
                        //                root = seg.DepartureStation;
                        //            }
                        //            if (TSeg == (n + 1))
                        //            {
                        //                root += seg.ArrivalStation;
                        //            }
                        //            n = n + 1;
                        //        }
                        //        #endregion
                        //        #region SegmentUpdate
                        //        foreach (var seg in j.Segments)
                        //        {
                        //            legCount += seg.Legs.Count();
                        //            for (int i = 0; i < obj.Count; i++)
                        //            {
                        //                string ArrCode = "";
                        //                string DepCode = "";
                        //                foreach (var leg in seg.Legs)
                        //                {
                        //                    if (leg.ArrivalStation.Trim() == obj[i].ArrAirCode.Trim() && leg.DepartureStation.Trim() == obj[i].DepAirCode.Trim())
                        //                    {
                        //                        try
                        //                        {
                        //                            obj[i].DeptDate = leg.STD.ToString("dd MMM yyyy");
                        //                            obj[i].DeptTime = leg.STD.ToString("HH:mm");
                        //                            obj[i].ArrDate = leg.STA.ToString("dd MMM yyyy");
                        //                            obj[i].ArrTime = leg.STA.ToString("HH:mm");
                        //                        }
                        //                        catch (Exception ex) { }
                        //                        ArrCode = seg.ArrivalStation;
                        //                        DepCode = seg.DepartureStation;
                        //                        obj[i].Segment = DepCode + ":" + ArrCode;
                        //                        obj[i].SegmentStatus = "Live";
                        //                        obj[i].root = root;
                        //                        break;
                        //                    }
                        //                }
                        //            }
                        //        }
                        //        #endregion
                        //    }

                        //}
                        if (VC == "SG")
                        {
                            TotPax = obj.Where(x => x.PaxType != "INF").Select(x => x.PaxId).Distinct().ToList().Count();
                        navitaire.SG.ver4.GetBookingResponse BkgRes = null;//= BAL.Flight.SGNAV.SGNAV4.RetriveBooking(OrderID, out Msg);
                            TotPaxLive = BkgRes.Booking.Passengers.Count();
                            foreach (var j in BkgRes.Booking.Journeys)
                            {
                                #region Get root
                                int TSeg = j.Segments.Count();
                                int n = 0;
                                string root = "";
                                foreach (var seg in j.Segments)
                                {
                                    if (n == 0)
                                    {
                                        root = seg.DepartureStation;
                                    }
                                    if (TSeg == (n + 1))
                                    {
                                        root += seg.ArrivalStation;
                                    }
                                    n = n + 1;
                                }
                                #endregion
                                #region SegmentUpdate
                                foreach (var seg in j.Segments)
                                {
                                    legCount += seg.Legs.Count();
                                    for (int i = 0; i < obj.Count; i++)
                                    {
                                        string ArrCode = "";
                                        string DepCode = "";
                                        foreach (var leg in seg.Legs)
                                        {
                                            if (leg.ArrivalStation.Trim() == obj[i].ArrAirCode.Trim() && leg.DepartureStation.Trim() == obj[i].DepAirCode.Trim())
                                            {
                                                try
                                                {
                                                    obj[i].DeptDate = leg.STD.ToString("dd MMM yyyy");
                                                    obj[i].DeptTime = leg.STD.ToString("HH:mm");
                                                    obj[i].ArrDate = leg.STA.ToString("dd MMM yyyy");
                                                    obj[i].ArrTime = leg.STA.ToString("HH:mm");
                                                }
                                                catch (Exception ex) { }
                                                ArrCode = seg.ArrivalStation;
                                                DepCode = seg.DepartureStation;
                                                obj[i].Segment = DepCode + ":" + ArrCode;
                                                obj[i].SegmentStatus = "Live";
                                                obj[i].root = root;
                                                break;
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                        }
                    }

                    //if (VC == "G8")
                    //{
                    //    TotPax = obj.Where(x => x.PaxType != "INF").Select(x => x.PaxId).Distinct().ToList().Count();
                    //    navitaire.bm.ver4.GetBookingResponse BkgRes = BAL.Flight.G8NAV.G8NAV4.RetriveBooking(OrderID, out Msg);
                    //    TotPaxLive = BkgRes.Booking.Passengers.Count();
                    //    foreach (var j in BkgRes.Booking.Journeys)
                    //    {
                    //        #region Get root
                    //        int TSeg = j.Segments.Count();
                    //        int n = 0;
                    //        string root = "";
                    //        foreach (var seg in j.Segments)
                    //        {
                    //            if (n == 0)
                    //            {
                    //                root = seg.DepartureStation;
                    //            }
                    //            if (TSeg == (n + 1))
                    //            {
                    //                root += seg.ArrivalStation;
                    //            }
                    //            n = n + 1;
                    //        }
                    //        #endregion
                    //        #region SegmentUpdate
                    //        foreach (var seg in j.Segments)
                    //        {

                    //            legCount += seg.Legs.Count();
                    //            for (int i = 0; i < obj.Count; i++)
                    //            {
                    //                string ArrCode = "";
                    //                string DepCode = "";
                    //                foreach (var leg in seg.Legs)
                    //                {
                    //                    if (leg.ArrivalStation.Trim() == obj[i].ArrAirCode.Trim() && leg.DepartureStation.Trim() == obj[i].DepAirCode.Trim())
                    //                    {
                    //                        try
                    //                        {
                    //                            obj[i].DeptDate = leg.STD.ToString("dd MMM yyyy");
                    //                            obj[i].DeptTime = leg.STD.ToString("HH:mm");
                    //                            obj[i].ArrDate = leg.STA.ToString("dd MMM yyyy");
                    //                            obj[i].ArrTime = leg.STA.ToString("HH:mm");
                    //                        }
                    //                        catch (Exception ex) { }
                    //                        ArrCode = seg.ArrivalStation;
                    //                        DepCode = seg.DepartureStation;
                    //                        obj[i].root = root;
                    //                        obj[i].Segment = DepCode + ":" + ArrCode;
                    //                        obj[i].SegmentStatus = "Live";
                    //                        break;
                    //                    }
                    //                }
                    //            }
                    //        }
                    //        #endregion
                    //    }
                    //}
                //}
                if ((obj.Count < legCount) || (TotPax != TotPaxLive && TotPax != 0))
                {
                    obj = new List<SegmentDetail>();
                    obj.Add(new SegmentDetail
                    {
                        Error = Msg != "" ? Msg : "Mismatch in Booking details.",
                    });
                }
            }
            catch (Exception ex)
            {
                obj = new List<SegmentDetail>();
                obj.Add(new SegmentDetail
                {
                    Error = Msg != "" ? Msg : "Mismatch in Booking details.",
                });
            }
            return obj;
        }

      

    }

}

   
